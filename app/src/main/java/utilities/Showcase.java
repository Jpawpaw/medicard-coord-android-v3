package utilities;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.Layout;
import android.text.TextPaint;
import android.util.Log;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.SimpleShowcaseEventListener;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import coord.medicard.com.medicardcoordinator.R;

/**
 * Created by mpx-pawpaw on 6/9/17.
 */

public class Showcase {




    public static void showCase(ViewTarget viewTarget, String title, String message, Activity activity
    , final ShowCaseCallBack callBack , final String ORIGIN) {
        try {

            TextPaint paint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
            paint.setTextSize(activity.getResources().getDimension(R.dimen.abc_text_size_body_1_material));
            paint.setColor(Color.WHITE);
            paint.setFakeBoldText(true);


            TextPaint titlePaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
            titlePaint.setTextSize(activity.getResources().getDimension(R.dimen.abc_text_size_headline_material));
            titlePaint.setColor(Color.WHITE);
            titlePaint.setFakeBoldText(true);



            ShowcaseView showcaseView = new ShowcaseView.Builder(activity)
                    .withNewStyleShowcase()
                    .setTarget(viewTarget)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setContentTextPaint(paint)
                    .setContentTitlePaint(titlePaint)
                    .hideOnTouchOutside()
                    .setStyle(R.style.CustomShowcaseTheme2)
                    .blockAllTouches()
                    .setShowcaseEventListener(new SimpleShowcaseEventListener() {
                                                  @Override
                                                  public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                                                      Log.d("MEMO", "MEMO");
                                                      callBack.onClickedShowCaseClose(ORIGIN);
                                                  }
                                              }


                    )
                    .build();

            //showcaseView.setDetailTextAlignment(Layout.Alignment.ALIGN_CENTER);
            //showcaseView.setTitleTextAlignment(Layout.Alignment.ALIGN_CENTER);
           // showcaseView.forceTextPosition(ShowcaseView.BELOW_SHOWCASE);

        } catch (Exception e) {

        }
    }




    public interface  ShowCaseCallBack{

        void onClickedShowCaseClose(String ORIGIN);

    }

}
