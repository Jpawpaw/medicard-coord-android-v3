package utilities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * Created by mpx-pawpaw on 5/11/17.
 */

public class CreateList {


    public static List<String> getYearList() {

        List<String> yearList = new ArrayList<String>();
        int startYEAR = 1900;

        Calendar now = Calendar.getInstance(); // Gets the current date and time
        int year = now.get(Calendar.YEAR) + 1; // The current year

        for (int x = startYEAR; x < year; x++) {


            yearList.add(startYEAR + "");
            startYEAR += 1;
        }

        Collections.reverse(yearList);

        return yearList;
    }

    public static List<String> getMonthList() {

        List<String> month = new ArrayList<String>();


        month.add("  Jan  ");
        month.add("  Feb  ");
        month.add("  Mar  ");
        month.add("  Apr  ");
        month.add("  May  ");
        month.add("  Jun  ");
        month.add("  Jul  ");
        month.add("  Aug  ");
        month.add("  Sept  ");
        month.add("  Oct  ");
        month.add("  Nov  ");
        month.add("  Dec  ");

        return month;
    }

    public static List<String> getDayList(int toaddYear) {

        List<String> day = new ArrayList<String>();

        int startDay = 1;


        for (int x = 0; x < toaddYear; x++) {


            day.add("    " + startDay + "    ");
            startDay += 1;
        }


        return day;
    }
}
