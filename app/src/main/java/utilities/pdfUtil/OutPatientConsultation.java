package utilities.pdfUtil;

/**
 * Created by mpx-pawpaw on 5/3/17.
 */

public interface OutPatientConsultation {


    String VALID_FROM = "validFrom";
    String VALID_UNTIL = "validUntil";

    String DATE_OF_CONSULT = "date";
    String REFERENCE_NUMBER = "referenceNumber";
    String DOCTOR = "doctor";
    String HOSPITAL = "hospital";
    String MEMBER_NAME = "memberName";
    String AGE = "age";
    String GENDER = "gender";
    String MEMBER_ID = "memberId";
    String COMPANY = "company";
    String REMARKS = "remarks";
    String DATE_EFFECTIVITY = "dateEffectivity";
    String VALIDITY_DATE = "validityDate";
    String LIMITS = "limits";

    String CHIEF_COMPLAINT = "chiefComplaint";
    String HISTORY_oF_PERSENT_ILLNESS = "historyOfPresentIllness";
    String PAST_OR_FAMILY_HISTORY = "pastOrFamilyHistory";
    String BATCH_CODE = "batchCode" ;
    String IMG_APPROVAL = "imgApproval";
    String DIAGNOSIS = "diagnosis";
    String PRIMARYDIAGNOSISWORKINGIMPRESSION = "primaryDianosisWorkingImpression";
    String PROCEDURES = "procedure";
    String PROC_APPROVAL_NO = "procedureDoneInClinicApprovalNo";
    String PRESCRIBEDTESTFORPRIMARYDIAG = "prescribedTestForPrimaryDiagnosis";
    String ICDCODE ="ICDCODE";

}
