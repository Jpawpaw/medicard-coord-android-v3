package utilities.pdfUtil;

import android.graphics.Bitmap;

/**
 * Created by mpx-pawpaw on 5/3/17.
 */

public class OutPatientConsultationForm implements OutPatientConsultation {

    private String validFrom;
    private String validUntil;

    private String date;
    private String referenceNumber;
    private String doctor;
    private String hospital;
    private String memberName;
    private String age;
    private String gender;
    private String memberId;
    private String company;
    private String remarks;
    private String dateEffectivity;
    private String validityDate;
    private String limit;
    private String procedureDoneInClinicApprovalNo;

    private String chiefComplaint;
    private String historyOfPresentIllness;
    private String pastOrFamilyHistory;
    private String batchCode;
    private String serviceType;
    private Bitmap imgApproval;
    private String diagnosis;
    private String primaryDianosisWorkingImpression;
    private String procedure;
    private String  prescribedTestForPrimaryDiagnosis;
    private String  icd10Code;



    public String getProcedureDoneInClinicApprovalNo() {
        return procedureDoneInClinicApprovalNo;
    }

    public void setProcedureDoneInClinicApprovalNo(String procedureDoneInClinicApprovalNo) {
        this.procedureDoneInClinicApprovalNo = procedureDoneInClinicApprovalNo;
    }

    public String getProcedure() {
        return procedure;
    }

    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }


    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getIcd10Code() {
        return icd10Code;
    }

    public void setIcd10Code(String icd10Code) {
        this.icd10Code = icd10Code;
    }

    public String getPrimaryDianosisWorkingImpression() {
        return primaryDianosisWorkingImpression;
    }

    public String getPrescribedTestForPrimaryDiagnosis() {
        return prescribedTestForPrimaryDiagnosis;
    }

    public void setPrescribedTestForPrimaryDiagnosis(String prescribedTestForPrimaryDiagnosis) {
        this.prescribedTestForPrimaryDiagnosis = prescribedTestForPrimaryDiagnosis;
    }

    public void setPrimaryDianosisWorkingImpression(String primaryDianosisWorkingImpression) {
        this.primaryDianosisWorkingImpression = primaryDianosisWorkingImpression;
    }

    public String getLimit() { return limit; }

    public void setLimit(String limit) { this.limit = limit; }

    public Bitmap getImgApproval() { return imgApproval; }

    public void setImgApproval(Bitmap imgApproval) { this.imgApproval = imgApproval; }

    public String getValidFrom() { return validFrom; }

    public void setValidFrom(String validFrom) { this.validFrom = validFrom; }

    public String getValidUntil() { return validUntil; }

    public void setValidUntil(String validUntil) { this.validUntil = validUntil; }

    public String getDateOfConsult() { return date; }

    public void setDateOfConsult(String dateOfConsult) { this.date = dateOfConsult; }

    public String getReferenceNumber() { return referenceNumber; }

    public void setReferenceNumber(String referenceNumber) { this.referenceNumber = referenceNumber; }

    public String getDoctor() { return doctor; }

    public void setDoctor(String doctor) { this.doctor = doctor; }

    public String getHospital() { return hospital; }

    public void setHospital(String hospital) { this.hospital = hospital; }

    public String getMemberName() { return memberName; }

    public void setMemberName(String memberName) { this.memberName = memberName; }

    public String getAge() { return age; }

    public void setAge(String age) { this.age = age; }

    public String getGender() { return gender; }

    public void setGender(String gender) { this.gender = gender; }

    public String getMemberId() { return memberId; }

    public void setMemberId(String memberId) { this.memberId = memberId; }

    public String getCompany() { return company; }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDateEffectivity() {
        return dateEffectivity;
    }

    public void setDateEffectivity(String dateEffectivity) {
        this.dateEffectivity = dateEffectivity;
    }

    public String getValidityDate() {
        return validityDate;
    }

    public void setValidityDate(String validityDate) {
        this.validityDate = validityDate;
    }

    public String getChiefComplaint() {
        return chiefComplaint;
    }

    public void setChiefComplaint(String chiefComplaint) {
        this.chiefComplaint = chiefComplaint;
    }

    public String getHistoryOfPresentIllness() {
        return historyOfPresentIllness;
    }

    public void setHistoryOfPresentIllness(String historyOfPresentIllness) {
        this.historyOfPresentIllness = historyOfPresentIllness;
    }

    public String getPastOrFamilyHistory() {
        return pastOrFamilyHistory;
    }

    public void setPastOrFamilyHistory(String pastOrFamilyHistory) {
        this.pastOrFamilyHistory = pastOrFamilyHistory;
    }

    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public OutPatientConsultationForm(Builder builder) {
        validFrom = builder.validFrom;
        validUntil = builder.validUntil;
        date = builder.date;
        referenceNumber = builder.referenceNumber;
        doctor = builder.doctor;
        hospital = builder.hospital;
        memberName = builder.memberName;
        age = builder.age;
        gender = builder.gender;
        memberId = builder.memberId;
        remarks = builder.remarks;
        company = builder.company;
        dateEffectivity = builder.dateEffectivity;
        validityDate = builder.validityDate;
        chiefComplaint = builder.chiefComplaint;
        historyOfPresentIllness = builder.historyOfPresentIllness;
        pastOrFamilyHistory = builder.pastOrFamilyHistory;
        serviceType = builder.serviceType;
        batchCode = builder.batchCode;
        imgApproval = builder.imgApproval;
        diagnosis  = builder.diagnosis;
        primaryDianosisWorkingImpression = builder.primaryDianosisWorkingImpression;
        procedure = builder.procedure;
        procedureDoneInClinicApprovalNo = builder.procedureDoneInClinicApprovalNo;
        prescribedTestForPrimaryDiagnosis = builder.prescribedTestForPrimaryDiagnosis;
        icd10Code = builder.icd10Code;
    }



    public static class Builder {


        private String validFrom;//
        private String validUntil;//

        private String date;
        private String referenceNumber;
        private String doctor;
        private String hospital;
        private String memberName;
        private String age;
        private String gender;
        private String memberId;
        private String company;
        private String remarks;
        private String dateEffectivity;
        private String validityDate;
        private String limits;

        private String chiefComplaint;
        private String historyOfPresentIllness;
        private String pastOrFamilyHistory;
        private String serviceType;
        private String batchCode;
        private Bitmap imgApproval;
        private String primaryDianosisWorkingImpression;
        private String procedure;
        private String procedureDoneInClinicApprovalNo;
        public String diagnosis;
        public String prescribedTestForPrimaryDiagnosis;
        private String icd10Code;

        public Builder() {
        }

        public Builder limits(String limits) {
            this.limits = limits;
            return this;
        }

        public Builder diagnosis(String diagnosis) {
            this.diagnosis = diagnosis;
            return this;
        }
        public Builder icd10Code(String icd10Code) {
            this.icd10Code = icd10Code;
            return this;
        }

        public Builder primaryDianosisWorkingImpression(String primaryDianosisWorkingImpression) {
            this.primaryDianosisWorkingImpression = primaryDianosisWorkingImpression;
            return this;
        }

        public Builder prescribedTestForPrimaryDiagnosis(String prescribedTestForPrimaryDiagnosis) {
            this.prescribedTestForPrimaryDiagnosis = prescribedTestForPrimaryDiagnosis;
            return this;
        }

        public Builder procedureDoneInClinicApprovalNo(String procedureDoneInClinicApprovalNo) {
            this.procedureDoneInClinicApprovalNo = procedureDoneInClinicApprovalNo;
            return this;
        }


        public Builder procedure(String procedure) {
            this.procedure = procedure;
            return this;
        }


        public Builder imgApproval(Bitmap imgApproval) {
            this.imgApproval = imgApproval;
            return this;
        }
        public Builder serviceType(String serviceType) {
            this.serviceType = serviceType;
            return this;
        }

        public Builder batchCode(String batchCode) {
            this.batchCode = batchCode;
            return this;
        }

        public Builder validFrom(String validFrom) {
            this.validFrom = validFrom;
            return this;
        }

        public Builder validUntil(String validUntil) {
            this.validUntil = validUntil;
            return this;
        }

        public Builder date(String dateOfConsult) {
            this.date = dateOfConsult;
            return this;
        }

        public Builder referenceNumber(String referenceNumber) {
            this.referenceNumber = referenceNumber;
            return this;
        }

        public Builder doctor(String doctor) {
            this.doctor = doctor;
            return this;
        }

        public Builder hospital(String hospital) {
            this.hospital = hospital;
            return this;
        }

        public Builder memberName(String memberName) {
            this.memberName = memberName;
            return this;
        }

        public Builder age(String age) {
            this.age = age;
            return this;
        }

        public Builder gender(String gender) {
            this.gender = gender;
            return this;
        }

        public Builder memberId(String memberId) {
            this.memberId = memberId;
            return this;
        }

        public Builder company(String company) {
            this.company = company;
            return this;
        }

        public Builder remarks(String remarks) {
            this.remarks = remarks;
            return this;
        }

        public Builder dateEffectivity(String dateEffectivity) {
            this.dateEffectivity = dateEffectivity;
            return this;
        }

        public Builder validityDate(String validityDate) {
            this.validityDate = validityDate;
            return this;
        }

        public Builder chiefComplaint(String chiefComplaint) {
            this.chiefComplaint = chiefComplaint;
            return this;
        }

        public Builder historyOfPresentIllness(String historyOfPresentIllness) {
            this.historyOfPresentIllness = historyOfPresentIllness;
            return this;
        }

        public Builder pastOrFamilyHistory(String pastOrFamilyHistory) {
            this.pastOrFamilyHistory = pastOrFamilyHistory;
            return this;
        }

        public OutPatientConsultationForm build() {
            return new OutPatientConsultationForm(this);
        }

    }

}

