package utilities.pdfUtil;

import android.util.Log;

import java.io.InputStream;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mpx-pawpaw on 5/3/17.
 */

public class GenerateForm {


    public static void generateLoaForm(final OutPatientConsultationForm build, final InputStream inputStream
            , final PdfCallback callback) {

        Observable.create(new Observable.OnSubscribe<Boolean>() {

            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                try {
                    PdfGenerator.generatePdfLoaConsultationForm(
                            build, inputStream);
                    subscriber.onNext(Boolean.TRUE);
                } catch (Exception e) {
                    Log.e("ERROR_gen" , e.getMessage());
                    // Timber.d("error %s", e.toString());
                    subscriber.onNext(Boolean.FALSE);
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onCompleted() {
                        // Timber.d("completed process");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("ERROR_gen2" , e.getMessage());
                        //  Timber.d("error message %s", e.toString());
                        callback.onGenerateLoaFormError();
                    }

                    @Override
                    public void onNext(Boolean success) {
                        //  Timber.d("onNext");
                        if (success) {
                            callback.onGenerateLoaFormSuccess( build.getServiceType() , build.getReferenceNumber());
                        } else {
                            callback.onGenerateLoaFormError();
                        }
                    }
                });

    }


    public interface PdfCallback {
        void onGenerateLoaFormSuccess(String serviceType, String referenceNumber);

        void onGenerateLoaFormError();
    }
}
