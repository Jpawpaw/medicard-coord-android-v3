package utilities.pdfUtil;

import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.media.Image;
import android.os.Environment;
import android.util.Log;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.BaseField;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.qrcode.EncodeHintType;
import com.itextpdf.text.pdf.qrcode.ErrorCorrectionLevel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mpx-pawpaw on 5/3/17.
 */

public class PdfGenerator {

    public static final String TAG = "xyz";


    public static final void generatePdfLoaConsultationForm(OutPatientConsultationForm patientForm, InputStream stream) {

        try {

            String loaFileName = StatusType.genFileName(patientForm.getServiceType(), patientForm.getReferenceNumber());


            File pdfFolder = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DCIM), "MediCard");
            if (!pdfFolder.exists()) {
                pdfFolder.mkdir();
                //  Timber.d("generate folder name loaConsultation...");
            }

            //Create time stamp
            Date date = new Date();
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date);

            File loaFormFile = new File(pdfFolder, loaFileName + ".pdf");
            //  Timber.d("folder path %s", loaFormFile.toString());

            OutputStream output = new FileOutputStream(loaFormFile);

            PdfReader reader = new PdfReader(stream);
            PdfStamper stamper = new PdfStamper(reader, output);
            AcroFields fields = stamper.getAcroFields();


            try {
                System.out.println("reqeest no. " + patientForm.getReferenceNumber() + "date " + patientForm.getDateOfConsult() + "hospital no. " + patientForm.getHospital());
                fields.setField(OutPatientConsultationForm.VALID_FROM, patientForm.getValidFrom());
                fields.setField(OutPatientConsultationForm.VALID_UNTIL, patientForm.getValidUntil());
                fields.setField(OutPatientConsultationForm.DATE_OF_CONSULT, patientForm.getDateOfConsult());
                try {
                    fields.setField(OutPatientConsultationForm.REFERENCE_NUMBER, patientForm.getReferenceNumber());
                }catch (Exception e){
                    e.printStackTrace();
                }
                fields.setField(OutPatientConsultationForm.DOCTOR, patientForm.getDoctor());
                fields.setField(OutPatientConsultationForm.HOSPITAL, patientForm.getHospital());
                fields.setField(OutPatientConsultationForm.MEMBER_NAME, patientForm.getMemberName());
                fields.setField(OutPatientConsultationForm.AGE, patientForm.getAge());
                fields.setField(OutPatientConsultationForm.GENDER, patientForm.getGender());
                fields.setField(OutPatientConsultationForm.MEMBER_ID, patientForm.getMemberId());
                fields.setField(OutPatientConsultationForm.COMPANY, patientForm.getCompany());
                fields.setField(OutPatientConsultationForm.REMARKS, testRemarks(trimRemarks(patientForm.getRemarks())));
                fields.setField(OutPatientConsultationForm.DATE_EFFECTIVITY, patientForm.getDateEffectivity());
                fields.setField(OutPatientConsultationForm.VALIDITY_DATE, patientForm.getValidityDate());
                fields.setField(OutPatientConsultationForm.CHIEF_COMPLAINT, patientForm.getChiefComplaint());
                fields.setField(OutPatientConsultationForm.HISTORY_oF_PERSENT_ILLNESS, patientForm.getHistoryOfPresentIllness());
                fields.setField(OutPatientConsultationForm.PAST_OR_FAMILY_HISTORY, patientForm.getPastOrFamilyHistory());
                ////adding font size to the field
                fields.setFieldProperty(OutPatientConsultationForm.PRESCRIBEDTESTFORPRIMARYDIAG, "textSize", new Float(30), null);
                fields.setField(OutPatientConsultationForm.PRESCRIBEDTESTFORPRIMARYDIAG, patientForm.getPrescribedTestForPrimaryDiagnosis());

                ////adding font size to the field
                fields.setFieldProperty(OutPatientConsultationForm.ICDCODE, "textSize", new Float(30), null);
                fields.setField(OutPatientConsultationForm.ICDCODE, patientForm.getIcd10Code());
                //adding font size to the field
                fields.setFieldProperty(OutPatientConsultationForm.PRIMARYDIAGNOSISWORKINGIMPRESSION, "textSize", new Float(30), null);
                fields.setField(OutPatientConsultationForm.PRIMARYDIAGNOSISWORKINGIMPRESSION, patientForm.getPrimaryDianosisWorkingImpression());

                fields.setField(OutPatientConsultationForm.DIAGNOSIS, patientForm.getDiagnosis());
                fields.setField(OutPatientConsultationForm.PROCEDURES, patientForm.getProcedure());
                //adding font size to the field
                fields.setFieldProperty(OutPatientConsultationForm.PROC_APPROVAL_NO, "textSize", new Float(30), null);
                fields.setField(OutPatientConsultationForm.PROC_APPROVAL_NO, patientForm.getProcedureDoneInClinicApprovalNo());
                fields.setField(OutPatientConsultationForm.BATCH_CODE, patientForm.getBatchCode());
                PushbuttonField pf = fields.getNewPushbuttonFromField(OutPatientConsultationForm.IMG_APPROVAL);
                Map<EncodeHintType, Object> hints = new HashMap<>();
                hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
                hints.put(EncodeHintType.CHARACTER_SET, "ISO-8859-1");

                BarcodeQRCode qrcode = new BarcodeQRCode(patientForm.getReferenceNumber(), 0, 0, hints);
                pf.setLayout(PushbuttonField.LAYOUT_ICON_ONLY);
                pf.setProportionalIcon(true);
                pf.setBorderWidth(0);
                pf.setVisibility(BaseField.VISIBLE);
                pf.setImage(qrcode.getImage());
                fields.replacePushbuttonField(OutPatientConsultationForm.IMG_APPROVAL, pf.getField());
            }catch (Exception e){
                e.printStackTrace();
            }


//            pf.setLayout(PushbuttonField.LAYOUT_ICON_ONLY);
//            pf.setProportionalIcon(true);
//            //
//            Bitmap b = patientForm.getImgApproval();
//            b.compress(Bitmap.CompressFormat.JPEG, 100, new Image());
//            Image i = (Image) b;
//            pf.setImage(i);
//
//            fields.replacePushbuttonField(OutPatientConsultationForm.IMG_APPROVAL, pf.getField());
//
            Log.d("SENTENCE",patientForm.getBatchCode() != null? patientForm.getBatchCode(): "");
            // remove the field to make the pdf look like clean
            stamper.setFormFlattening(true);
            stamper.close();

        } catch (IOException e) {
            // Timber.d("error message %s", e.toString());
        } catch (DocumentException e) {
            // Timber.d("error message %s", e.toString());
        }

    }

    public static final String trimRemarks(String remarks) {
        return remarks.replace("\n", ", ").replace("\r", "");
    }

    private static String testRemarks(String sentence) {

        return sentence.replace(",,", "").replace("''", "").replace(", , ", "");
    }


}