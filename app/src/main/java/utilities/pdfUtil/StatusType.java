package utilities.pdfUtil;

/**
 * Created by mpx-pawpaw on 5/4/17.
 */

public class StatusType {


    public static final String CONSULTATION = "CONSULTATION";

    public static final String MATERNITY_CONSULTATION = "MATERNITY";

    public static final String OTHER_TEST = "OTHER TEST";

    public static final String BASIC_TEST = "BASIC TEST";


    public static final String PROCEDURES = "PROCEDURES";


    //sa loalist to ng clinic procedure
    public static final String PROCEDURE = "PROCEDURE";







    // file name
    public static final String FILE_NAME_CONSULTATION = "CONSULT_";

    public static final String FILE_NAME_MATERNITY_CONSULTATION = "MATERNITY_";

    public static final String FILE_NAME_OTHER_TEST = "OTHERTEST_";

    public static final String FILE_NAME_BASIC_TEST = "BASICTEST_";

    public static final String FILE_NAME_PROCEDURE = "CLINICPROCEDURE_";

    public static final String genFileName(String serviceType, String approvalCode) {

        StringBuilder fileName = new StringBuilder();

        if (serviceType.contains(MATERNITY_CONSULTATION)) {
            fileName.append(FILE_NAME_MATERNITY_CONSULTATION);
        } else if (serviceType.contains(CONSULTATION)) {
            fileName.append(FILE_NAME_CONSULTATION);
        } else if (serviceType.contains(OTHER_TEST)) {
            fileName.append(FILE_NAME_OTHER_TEST);
        } else if (serviceType.contains(BASIC_TEST)) {
            fileName.append(FILE_NAME_BASIC_TEST);
        } else if(serviceType.contains(PROCEDURES)){
            fileName.append(FILE_NAME_PROCEDURE);
        }
        else if(serviceType.contains(PROCEDURE)){
            fileName.append(FILE_NAME_PROCEDURE);
        }

        fileName.append(approvalCode);

        return fileName.toString();
    }


}

