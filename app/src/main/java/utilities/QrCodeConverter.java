package utilities;

/**
 * Created by mpx-pawpaw on 11/10/16.
 */

public class QrCodeConverter {

    public String NOT_AVAILABLE = "NOT_AVAILABLE";

    public String[] convertQr(String Qrcode) {


        String[] data = new String[2];
        data[0] = NOT_AVAILABLE;
        data[1] = NOT_AVAILABLE;

        if (!Qrcode.equals("")) {

            for (int x = 0; x < Qrcode.length(); x++) {
                if (String.valueOf(Qrcode.charAt(x)).equals(":")) {

                    if (Qrcode.substring(0, x).contains(" ")) {
                        data[0] = "NOT_AVAILABLE";
                    } else
                        data[0] = Qrcode.substring(0, x);

                    if (Qrcode.substring((x + 1), Qrcode.length()).contains(" ")) {
                        data[1] = "NOT_AVAILABLE";
                    } else
                        data[1] = Qrcode.substring((x + 1), Qrcode.length());

                }
            }
        } else {
            data[0] = NOT_AVAILABLE;
            data[1] = NOT_AVAILABLE;
        }


        if (data[0].equals("") || data[1].equals("") || data[0].equals(NOT_AVAILABLE) || data[1].equals(NOT_AVAILABLE)) {
            data[0] = NOT_AVAILABLE;
            data[1] = NOT_AVAILABLE;
        }


        return data;
    }
}
