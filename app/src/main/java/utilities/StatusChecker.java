package utilities;

import java.text.SimpleDateFormat;

import static coord.medicard.com.medicardcoordinator.LoaDisplay.LoaDisplayRetieve.convertDate2;
import static coord.medicard.com.medicardcoordinator.LoaDisplay.LoaDisplayRetieve.testExpiration;

/**
 * Created by mpx-pawpaw on 4/3/17.
 */

public class StatusChecker {


        public static String setStatusHeader(String requestType, String status, String requestDatetime) {

        String data = "";
            System.out.println("requestdate "+ requestDatetime);
        if (status.equalsIgnoreCase("AVAILED")) {
            data = "REQUEST AVAILED";
        } else if (null==requestDatetime ) {
            data = "APPROVED";
        } else if (testExpiration(convertDate2(requestDatetime, new SimpleDateFormat("yyyy-MM-dd HH:mm")))) {
            if (requestType.equalsIgnoreCase("CONSULTATION") && status.equalsIgnoreCase("APPROVED")) {
                data = "CONFIRMED";
            } else {
                data = status;
            }
        } else {
            data = "REQUEST EXPIRED";
        }


        return data;


    }
}
