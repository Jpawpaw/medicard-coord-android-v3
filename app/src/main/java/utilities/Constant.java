package utilities;

/**
 * Created by mpx-pawpaw on 3/30/17.
 */

public class Constant {

    //TABLE TYPE
    public static String HOSPITAL_TT = "HOSPITAL_TT";
    public static String DIAGNOSIS_TT = "DIAGNOSIS_TT";
    public static String CONSULTATION = "CONSULTATION";
    public static String MATERNITY = "MATERNITY CONSULTATION";
    public static String OTHER_TEST = "OTHER TEST";
    public static String PROCEDURES = "PROCEDURES";
    public static String PROCEDURE = "PROCEDURE";
    public static String BASIC_TEST = "BASIC TEST";
    public static String TEST = "TEST";
    public static String CONSULT = "CONSULT";
    public static String PROCEDURE_DESC = "PROCEDURE DESC";
    public static String ADAPTER_INPATIENT_DOCTOR = "ADAPTER_INPATIENT_DOCTOR";
    public static String ADAPTER_INPATIENT_DIAGNOSIS = "ADAPTER_INPATIENT_DIAGNOSIS";
    public static String ADAPTER_INPATIENT_PROCEDURE = "ADAPTER_INPATIENT_PROCEDURE";

    public static String ADAPTER_PROCEDURE = "ADAPTER_PROCEDURE";
    public static String ADAPTER_PRIMARY = "ADAPTER_PRIMARY";
    public static String ADAPTER_OTHER = "ADAPTER_OTHER";
    public static String ADAPTER_OTHER_2 = "ADAPTER_OTHER_2";

    public static String ADAPTER_OTHER_BASIC = "ADAPTER_OTHER_BASIC";

    public static String ADAPTER_OTHER_DIAG = "ADAPTER_OTHER_DIAG";
    public static String ADAPTER_OTHER_DIAG_SECOND = "ADAPTER_OTHER_DIAG_SECOND";
    public static String ADAPTER_PRIME_DIAG = "ADAPTER_PRIME_DIAG";
    public static String ADAPTER_DIAGNOSIS = "ADAPTER_DIAGNOSIS";
    public static String ADAPTER_DOCTORS = "ADAPTER_DOCTORS";
    public static String ADAPTER_DOCTORS_IN_PATIENT = "ADAPTER_DOCTORS_IN_PATIENT";
    public static String ADAPTER_DIAG_IN_PATIENT = "ADAPTER_DIAG_IN_PATIENT";
    public static String ADAPTER_DOCTOR_IN_PATIENT = "ADAPTER_DOCTOR_IN_PATIENT";
    public static String ADAPTER_DIAG_ADMITTED = "ADAPTER_DIAG_ADMITTED";
    public static String ADAPTER_DOCTORS_IN_PATIENT_MAIN = "ADAPTER_DOCTORS_IN_PATIENT_MAIN";
    public static String ADAPTER_PROC_OTHER_SERVICES = "ADAPTER_PROC_OTHER_SERVICES";
    public static String ADAPTER_ROOM_PLANS = "ADAPTER_ROOM_PLANS";
    public static String ADAPTER_PRE_ROOM_PLANS = "ADAPTER_PRE_ROOM_PLANS";
    public static String ADAPTER_ROOM_CATEGORY = "ADAPTER_ROOM_CATEGORY";
    public static String ADAPTER_UNFILTERED_DOCTORS = "ADAPTER_UNFILTERED_DOCTORS";
    public static String ADAPTER_UNFILTERED_HOSP = "ADAPTER_UNFILTERED_HOSP";


    public static String ADAPTER_PROC_IN_PATIENT = "ADAPTER_PROC_IN_PATIENT";

    public static String FROM_PRIMARY_PROC = "FROM_PRIMARY_PROC";
    public static String FROM_OTHER_PROC = "FROM_OTHER_PROC";
    public static String FROM_OTHER_PROC_2 = "FROM_OTHER_PROC_2";
    public static String FROM_PRIMARY_PROC_2 = "FROM_PRIMARY_PROC_2";
    public static String FROM_OTHER_TEST = "FROM_OTHER_TEST";
    public static String FROM_PROCEDURES = "FROM_PROCEDURES";
    public static String FROM_BASIC_TEST = "FROM_BASIC_TEST";
    public static String FROM_ADMITTED_IN_PATIENT = "FROM_ADMITTED_IN_PATIENT";
    public static String FROM_AVIALED = "FROM_AVIALED";
    public static String FROM_IN_PATIENT_AVAIL = "FROM_IN_PATIENT_AVAIL";
    public static String FROM_OTHER_DIAGNOSIS = "OTHER_DIAGNOSIS";

    public static String IS_NOT_MATERNITY = "IS_NOT_MATERNITY";

    public static String PROCEDURES_TO_SEND = "3";
    public static String BASIC_TEST_TO_SEND = "3";
    public static String OTHER_TEST_TO_SEND = "4";
    public static String PENDING = "PENDING";

    public static String BASIC_TEST_SERVICE_TYPE = "2";
    public static String OTHER_TEST_SERVICE_TYPE = "2";
    public static String DIAGNOSIS_OTHER_IN_PATIENT = "DIAGNOSIS_OTHER_IN_PATIENT";
    public static String DIAGNOSIS_OTHER_IN_PATIENT_CHANGE = "DIAGNOSIS_OTHER_IN_PATIENT_CHANGE";
    public static String DIAGNOSIS_OTHER_IN_PATIENT_ADD = "DIAGNOSIS_OTHER_IN_PATIENT_ADD";
    public static String DIAGNOSIS_OTHER_IN_PATIENT_REMOVE = "DIAGNOSIS_OTHER_IN_PATIENT_REMOVE";
    public static String IN_PATIENT_PROC = "IN_PATIENT_PROC";
    public static String GETDATE = "GETDATE";
    public static String GETTIME = "GETTIME";
    public static String NONE = "NONE";
    public static String FROM_ADMITTED_IN_PATIENT_CHANGE = "FROM_ADMITTED_IN_PATIENT_CHANGE";
    public static String TIME_ADMITTED = "TIME_ADMITTED";
    public static String DATE_ADMITTED = "DATE_ADMITTED";
    public static String ROOM_CATEGORY = "ROOM_CATEGORY";


    //SHOWCASE ORIGINS
    public static final String DONE = "DONE";
    public static final String UNDONE = "UNDONE";
    public static final String FROM_QR_SEARCH = "1";
    public static final String FROM_MANUAL_SEARCH = "2";
    public static final String FROM_EDIT_IMAGE_ICON = "3";
    public static final String FROM_VIEW_LOA_LIST = "4";
    public static final String FROM_OUT_PATIENT = "5";
    public static final String FROM_IN_PATIENT = "6";
    public static final String FROM_ER = "7";


    //
    public static final String ZIP_NAME = "MaceListing.zip";
    public static final String DIAGNOSIS_CSV = "Diagnosis.csv";
    public static final String HOSPITAL_CSV = "Hospitals.csv";
    public static final String DOC_HOSP_CSV = "DocHosp.csv";
    public static final String ROOM_CAT_CSV = "RoomCategories.csv";
    public static final String ROOM_PLANS_CSV = "RoomPlans.csv";
    public static final String TEST_PROC_LIST_CSV = "TestProcList.csv";

    public static final String IMAGE_DIRECTORY_NAME = "Android File Upload";

    public static final String REQUEST_ORIGIN = "Coordinator";
}
