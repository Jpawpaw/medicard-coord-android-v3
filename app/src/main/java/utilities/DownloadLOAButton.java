package utilities;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by mpx-pawpaw on 5/8/17.
 */

public class DownloadLOAButton {


    public static void updateDownloadButton(Button btn_download, Button btn_download_test, TextView tv_status) {

        String AVAILED = "AVAILED";
        String EXPIRED = "EXPIRED";
        String CANCEL = "CANCELLED";
        String PENDING = "PENDING";
        String DISAPPROVED = "DISAPPROVED";




        String status = tv_status.getText().toString().trim().toLowerCase();



        if (status.contains(AVAILED.toLowerCase()) || status.contains(CANCEL.toLowerCase())
                || status.contains(EXPIRED.toLowerCase()) || status.contains(PENDING.toLowerCase()) || status.contains(DISAPPROVED.toLowerCase())   ) {
            btn_download.setVisibility(View.GONE);
        } else {
            btn_download.setVisibility(View.VISIBLE);
        }


        if (status.contains(AVAILED.toLowerCase()) || status.contains(CANCEL.toLowerCase())
                || status.contains(EXPIRED.toLowerCase()) || status.contains(PENDING.toLowerCase()) || status.contains(DISAPPROVED.toLowerCase())) {
            btn_download_test.setVisibility(View.GONE);
        } else {
            btn_download_test.setVisibility(View.VISIBLE);
        }




    }
}
