package utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;

import model.BasicTests;
import model.DiagnosisList;
import model.DoctorModelInsertion;
import model.DoctorModelMultipleSelection;
import model.ExclusionModel;
import model.Hospital;
import model.ProceduresListLocal;
import model.Rooms;
import model.Services;
import model.TestsAndProcedures;

/**
 * Created by mpx-pawpaw on 11/17/16.
 */

public class DataBaseHandler extends SQLiteOpenHelper {
    Context context;


    String medicard_one = "MEDICARD PHILIPPINES, INC.";
    String medicard_two = "MEDICard LIFESTYLE CENTER";

    //  private static final int DATABASE_VERSION = 1;
    //private static final int DATABASE_VERSION = 2;  //ADDED A FIELD FOR PROCEDURES
    //  private static final int DATABASE_VERSION = 3;  //ADDED BASIC TEST  >>>>>TABLE UPLOAD START HERE
    // private static final int DATABASE_VERSION = 4;  //ADDED isSelectedProcInPatient
    //   private static final int DATABASE_VERSION = 5;  //ADDED isSelected on doctorTable
    //   private static final int DATABASE_VERSION = 6;  //ADDED isSelectedAsMain on doctorTable
    //  private static final int DATABASE_VERSION = 7;  //ADDED isSelectedDiagOther on diagnosisTable
    //private static final int DATABASE_VERSION = 8;  //ADDED isSelectedAsMain on doctorTable
    // private static final int DATABASE_VERSION = 9;  //ADDED otherServiceTable
    //  private static final int DATABASE_VERSION = 10;  //ALTERED procedureTable
    // private static final int DATABASE_VERSION = 11;  //ADDED roomAvailTable
    // private static final int DATABASE_VERSION = 11;  //ADDED isSelected ON OTHERSERVICES
    private static final int DATABASE_VERSION = 12;  //ADDED roomCategoryTable
    protected static final String DATABASE_NAME = "DOCTORLIST";

    private static final String TAG = "DatabaseHandler.java";

    private String otherServiceTable = "otherServiceTable";
    // public String id = "id" ;
    private String serviceCode = "serviceCode";
    private String serviceDesc = "serviceDesc";
    private String createdOn = "createdOn";
    private String createdBy = "createdBy";
    private String lastUpdateOn = "lastUpdateOn";
    private String lastUpdateBy = "lastUpdateBy";


    private String roomsTable = "roomsTable";
    private String classCategory = "classCategory";
    private String monthlyPremium = "monthlyPremium";
    private String updatedDate = "updatedDate";
    private String planCode = "planCode";
    private String annualPremium = "annualPremium";
    private String grpType = "grpType";
    private String ddl = "ddl";
    private String planDesc = "planDesc";
    private String semiAnnualPremium = "semiAnnualPremium";
    private String wHosp = "wHosp";
    private String quarterlyPremium = "quarterlyPremium";
    private String updatedBy = "updatedBy";


    public String doctorTable = "doctorTable";
    public String procedureTable = "procedureTable";
    public String diagnosisTable = "diagnosisTable";
    public String basicTestTable = "basicTestTable";
    public String maceServiceType = "maceServiceType";
    public String costCenter = "costCenter";
    public String hospitalTable = "hospitalTable";
    public String diagnosis_Table = "diagnosis_Table";

    //Adding hospital objects
    public String HOSPITAL_CODE = "HOSPITAL_CODE";
    public String HOSPITAL_NAME = "HOSPITAL_NAME";
    public String STREET = "STREET";
    public String HOSPITAL_ABBRE = "HOSPITAL_ABBRE";
    public String CITY_NAME = "CITY_NAME";
    public String PROVINCE_NAME = "PROVINCE_NAME";
    public String REGION_NAME = "REGION_NAME";
    public String REGION_DESC = "REGION_DESC";
    public String PHONE_NO = "PHONE_NO";
    public String CLASS = "CLASS";
    public String CATEGORY = "CATEGORY";
    public String ENTRY_TYPE = "ENTRY_TYPE";
    public String ISACCREDITED = "ISACCREDITED";
    public String ISEXCLUDED = "ISEXCLUDED";


    //Adding diagnosis objects
    public String DIAG_CODE = "DIAG_CODE";
    public String DIAG_DESC = "DIAG_DESC";
    public String DIAG_REMARKS = "DIAG_REMARKS";
    public String DIAG_TYPE = "DIAG_TYPE";
    public String DIAG_TYPE_OLD = "DIAG_TYPE_OLD";
    public String TYPE_DESC = "TYPE_DESC";
    public String GROUP_DESC = "GROUP_DESC";
    public String ICD10_CODE = "ICD10_CODE";
    public String ICD10_DESC = "ICD10_DESC";
    public String ICD10_4C = "ICD10_4C";
    public String STATUS = "STATUS";


    public String fieldObjectId = "id";
    public String isSelected = "isSelected";
    public String isSelectedProc = "isSelectedProc";
    public String isSelectedProcPrime = "isSelectedProcPrime";
    public String isSelectedProcOther = "isSelectedProcOther";
    public String isSelectedProcInPatient = "isSelectedProcInPatient";
    public String isSelectedDiagOther = "isSelectedDiagOther";
    public String isSelectedAsMain = "isSelectedAsMain";

    public String doctorCode = "doctorCode";
    public String docLname = "docLname";
    public String docFname = "docFname";
    public String docMname = "docMname";
    public String hospitalCode = "hospitalCode";
    public String hospitalName = "hospitalName";
    public String specDesc = "specDesc";
    public String specCode = "specCode";
    public String schedule = "schedule";
    public String room = "room";
    public String wtax = "wtax";
    public String gracePeriod = "gracePeriod";
    public String vat = "vat";
    public String specialRem = "specialRem";
    public String hospRemarks = "hospRemarks";
    public String roomBoard = "roomBoard";
    public String remarks = "remarks";
    public String remarks2 = "remarks2";


    //PROCEDURE OBJECTS
    private String id = "id";
    private String serviceClassCode = "serviceClassCode";
    private String procedureCode = "procedureCode";
    private String procedureDesc = "procedureDesc";
    private String procedureAmount = "procedureAmount";

//
//    private String diagProcCode = "diagProcCode";
//    private String diagProcGroupCode = "diagProcGroupCode";
//    private String diagProcDesc = "diagProcDesc";
//    private String diagProcGroupDesc = "diagProcGroupDesc";

    //DIAGNOSIS OBJECTS
    private String status = "status";
    private String icd10Desc = "icd10Desc";
    private String diagCode = "diagCode";
    private String typeDesc = "typeDesc";
    private String type = "type";
    private String diagDesc = "diagDesc";
    private String icd10Code = "icd10Code";
    private String diagRemarks = "diagRemarks";

    private String roomCategoryTable = "roomCategoryTable";
    private String category = "category";


    private String referral_desk = "REFERRAL DESK";




    public DataBaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {


        String sql = "";

        sql += "CREATE TABLE " + doctorTable;
        sql += " ( ";
        sql += fieldObjectId + " INTEGER PRIMARY KEY AUTOINCREMENT, ";
        sql += doctorCode + " TEXT ,";
        sql += docLname + " TEXT ,";
        sql += docFname + " TEXT ,";
        sql += docMname + " TEXT ,";
        sql += hospitalCode + " TEXT ,";
        sql += hospitalName + " TEXT ,";
        sql += specDesc + " TEXT ,";
        sql += specCode + " TEXT ,";
        sql += schedule + " TEXT ,";
        sql += room + " TEXT ,";
        sql += wtax + " TEXT ,";
        sql += gracePeriod + " TEXT ,";
        sql += vat + " TEXT ,";
        sql += specialRem + " TEXT ,";
        sql += hospRemarks + " TEXT ,";
        sql += roomBoard + " TEXT ,";
        sql += remarks + " TEXT ,";
        sql += remarks2 + " TEXT ,";
        sql += isSelectedAsMain + " TEXT ,";
        sql += isSelected + " TEXT ";
        sql += " ) ";

        db.execSQL(sql);
        //Log.e(TAG, sql);


        String sqlProcedures = "";


        sqlProcedures += "CREATE TABLE " + procedureTable;
        sqlProcedures += " ( ";
        sqlProcedures += fieldObjectId + " INTEGER PRIMARY KEY AUTOINCREMENT, ";
        sqlProcedures += isSelected + " TEXT ,";
        sqlProcedures += isSelectedProc + " TEXT ,";
        sqlProcedures += isSelectedProcPrime + " TEXT ,";
        sqlProcedures += isSelectedProcOther + " TEXT ,";
        sqlProcedures += isSelectedProcInPatient + " TEXT ,";
        sqlProcedures += costCenter + " TEXT ,";
        sqlProcedures += procedureCode + " TEXT ,";
        sqlProcedures += procedureDesc + " TEXT ,";
        sqlProcedures += procedureAmount + " TEXT ,";
        sqlProcedures += maceServiceType + " TEXT ";
        sqlProcedures += " ) ";

        db.execSQL(sqlProcedures);
        //Log.e(TAG, sqlProcedures);

        //old diagTable
        String sqlDiagnosis = "";
        sqlDiagnosis += "CREATE TABLE " + diagnosisTable;
        sqlDiagnosis += " ( ";
        sqlDiagnosis += fieldObjectId + " INTEGER PRIMARY KEY AUTOINCREMENT, ";
        sqlDiagnosis += isSelected + " TEXT ,";
        sqlDiagnosis += isSelectedDiagOther + " TEXT ,";
        sqlDiagnosis += status + " TEXT ,";
        sqlDiagnosis += icd10Desc + " TEXT ,";
        sqlDiagnosis += diagCode + " TEXT ,";
        sqlDiagnosis += typeDesc + " TEXT ,";
        sqlDiagnosis += type + " TEXT ,";
        sqlDiagnosis += diagDesc + " TEXT ,";
        sqlDiagnosis += icd10Code + " TEXT ,";
        sqlDiagnosis += diagRemarks + " TEXT ";
        sqlDiagnosis += " ) ";
        db.execSQL(sqlDiagnosis);
        Log.e(TAG, sqlDiagnosis);

        String sqlRooms = "";
        sqlRooms += "CREATE TABLE " + roomsTable;
        sqlRooms += " ( ";
        sqlRooms += classCategory + " TEXT ,";
        sqlRooms += monthlyPremium + " TEXT ,";
        sqlRooms += updatedDate + " TEXT ,";
        sqlRooms += planCode + " TEXT ,";
        sqlRooms += annualPremium + " TEXT ,";
        sqlRooms += grpType + " TEXT ,";
        sqlRooms += ddl + " TEXT ,";
        sqlRooms += planDesc + " TEXT ,";
        sqlRooms += semiAnnualPremium + " TEXT ,";
        sqlRooms += wHosp + " TEXT , ";
        sqlRooms += quarterlyPremium + " TEXT , ";
        sqlRooms += updatedBy + " TEXT  ";
        sqlRooms += " ) ";

        db.execSQL(sqlRooms);
        //Log.e(TAG, sqlRooms);


        String sqlOtherServices = "";
        sqlOtherServices += "CREATE TABLE " + otherServiceTable;
        sqlOtherServices += " ( ";
        sqlOtherServices += id + " TEXT ,";
        sqlOtherServices += serviceCode + " TEXT ,";
        sqlOtherServices += serviceDesc + " TEXT ,";
        sqlOtherServices += createdOn + " TEXT ,";
        sqlOtherServices += createdBy + " TEXT ,";
        sqlOtherServices += lastUpdateOn + " TEXT ,";
        sqlOtherServices += lastUpdateBy + " TEXT ,";
        sqlOtherServices += isSelected + " TEXT ";
        sqlOtherServices += " ) ";

        db.execSQL(sqlOtherServices);
        //Log.e(TAG, sqlOtherServices);


        String sqlRoomCategory = "";
        sqlRoomCategory += "CREATE TABLE " + roomCategoryTable;
        sqlRoomCategory += " ( ";
        sqlRoomCategory += category + " TEXT ";
        sqlRoomCategory += " ) ";
        db.execSQL(sqlRoomCategory);
        //Log.e(TAG, sqlRoomCategory);

        String sqlHospitalTbl = "CREATE TABLE " + hospitalTable;
        sqlHospitalTbl += " ( ";
        sqlHospitalTbl += HOSPITAL_CODE + " TEXT ,";
        sqlHospitalTbl += HOSPITAL_NAME + " TEXT ,";
        sqlHospitalTbl += STREET + " TEXT ,";
        sqlHospitalTbl += HOSPITAL_ABBRE + " TEXT ,";
        sqlHospitalTbl += CITY_NAME + " TEXT ,";
        sqlHospitalTbl += PROVINCE_NAME + " TEXT ,";
        sqlHospitalTbl += REGION_NAME + " TEXT ,";
        sqlHospitalTbl += REGION_DESC + " TEXT ,";
        sqlHospitalTbl += PHONE_NO + " TEXT ,";
        sqlHospitalTbl += CLASS + " TEXT ,";
        sqlHospitalTbl += CATEGORY + " TEXT ,";
        sqlHospitalTbl += ENTRY_TYPE + " TEXT ,";
        sqlHospitalTbl += ISACCREDITED + " TEXT";
        sqlHospitalTbl += ISEXCLUDED + " TEXT";
        sqlHospitalTbl += " ) ";
        db.execSQL(sqlHospitalTbl);
        Log.e(TAG, sqlHospitalTbl);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


        if (newVersion > oldVersion) {
            Log.d("DATABASE", "new version");
            String sql3 = "DROP TABLE IF EXISTS " + doctorTable;
            String sql4 = "DROP TABLE IF EXISTS " + procedureTable;
            String sql5 = "DROP TABLE IF EXISTS " + diagnosisTable;
            String sql6 = "DROP TABLE IF EXISTS " + basicTestTable;
            String sql7 = "DROP TABLE IF EXISTS " + otherServiceTable;
            String sql8 = "DROP TABLE IF EXISTS " + roomsTable;
            String sql9 = "DROP TABLE IF EXISTS " + roomCategoryTable;
            db.execSQL(sql3);
            db.execSQL(sql4);
            db.execSQL(sql5);
            db.execSQL(sql6);
            db.execSQL(sql7);
            db.execSQL(sql8);
            db.execSQL(sql9);
            onCreate(db);
        } else
            Log.d("DATABASE", "old version");
    }

    public void insertToHospital(String[] rows) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(HOSPITAL_CODE, rows.length < 1 ? "" : rows[0].trim());
        cv.put(HOSPITAL_NAME, rows.length < 2 ? "" : rows[1].trim());
        cv.put(STREET, rows.length < 3 ? "" : rows[2].trim());
        cv.put(HOSPITAL_ABBRE, "");
        cv.put(CITY_NAME, rows.length < 4 ? "" : rows[3].trim());
        cv.put(PROVINCE_NAME, rows.length < 5 ? "" : rows[4].trim());
        cv.put(REGION_NAME, rows.length < 6 ? "" : rows[5].trim());
        cv.put(REGION_DESC, rows.length < 7 ? "" : rows[6].trim());
        cv.put(PHONE_NO, rows.length < 8 ? "" : rows[7].trim());
        cv.put(CLASS, rows.length < 9 ? "" : rows[8].trim());
        cv.put(CATEGORY, rows.length < 10 ? "" : rows[9].trim());
        cv.put(ENTRY_TYPE, rows.length < 11 ? "" : rows[10].trim());
        cv.put(ISACCREDITED, rows.length < 12 ? "" : rows[11].trim());
        cv.put(ISEXCLUDED, "false");
        db.insert(hospitalTable, null, cv);
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public void insertToTestProcList(String[] rows) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(procedureDesc, rows.length < 1 ? "" : rows[0].trim());
        cv.put(procedureCode, rows.length < 2 ? "" : rows[1].trim());
        cv.put(maceServiceType, rows.length < 3 ? "" : rows[2].trim());
        cv.put(costCenter, rows.length < 4 ? "" : rows[3].trim());
        cv.put(procedureAmount, rows.length < 5 ? "" : rows[4].trim());
        cv.put(isSelected, "false");
        cv.put(isSelectedProc, "false");
        cv.put(isSelectedProcPrime, "false");
        cv.put(isSelectedProcOther, "false");
        cv.put(isSelectedProcInPatient, "false");
        db.insert(procedureTable, null, cv);
        db.setTransactionSuccessful();
        db.endTransaction();


    }

    public void insertToDiagnosis(String[] rows) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ContentValues cv = new ContentValues();
//        cv.put(fieldObjectId, "");
//        cv.put(fieldObjectId, "");
        cv.put(isSelected, "false");
        cv.put(isSelectedDiagOther, "false");
        cv.put(diagCode, rows.length < 1 ? "" : rows[0].trim());
        cv.put(diagDesc, rows.length < 2 ? "" : rows[1].trim());
        cv.put(diagRemarks, rows.length < 3 ? "" : rows[2].trim());
        cv.put(type, rows.length < 4 ? "" : rows[3].trim());
        cv.put(typeDesc, rows.length < 5 ? "" : rows[4].trim());
        cv.put(icd10Code, rows.length < 6 ? "" : rows[5].trim());
        cv.put(icd10Desc, rows.length < 7 ? "" : rows[6].trim());
        cv.put(status, rows.length < 8 ? "" : rows[7].trim());
        db.insert(diagnosisTable, null, cv);
        db.setTransactionSuccessful();
        db.endTransaction();

    }

    public void insertToRoomCat(String[] rows) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(category, rows.length < 1 ? "" : rows[0].trim());
        db.insert(roomCategoryTable, null, cv);
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public void insertToRoomPlan(String[] rows) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ContentValues cv = new ContentValues();
//        cv.put(category, rows.length < 1? "":rows[0].trim());
        cv.put(planCode, rows.length < 1 ? "" : rows[0].trim());
        cv.put(planDesc, rows.length < 2 ? "" : rows[1].trim());
        cv.put(classCategory, rows.length < 3 ? "" : rows[2].trim());
        cv.put(monthlyPremium, rows.length < 4 ? "" : rows[3].trim());
        cv.put(quarterlyPremium, rows.length < 5 ? "" : rows[4].trim());
        cv.put(semiAnnualPremium, rows.length < 6 ? "" : rows[5].trim());
        cv.put(annualPremium, rows.length < 7 ? "" : rows[6].trim());
        cv.put(grpType, rows.length < 8 ? "" : rows[7].trim());
        cv.put(ddl, rows.length < 9 ? "" : rows[8].trim());
        cv.put(wHosp, rows.length < 10 ? "" : rows[9].trim());
        cv.put(updatedBy, rows.length < 11 ? "" : rows[10].trim());
        cv.put(updatedDate, "");
        db.insert(roomsTable, null, cv);
        db.setTransactionSuccessful();
        db.endTransaction();

    }

    public void insertToDocHosp(String[] rows) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ContentValues cv = new ContentValues();
        cv.put(doctorCode, rows.length < 1 ? "" : rows[0].trim());
        cv.put(docLname, rows.length < 2 ? "" : rows[1].trim());
        cv.put(docFname, rows.length < 3 ? "" : rows[2].trim());
        cv.put(docMname, rows.length < 4 ? "" : rows[3].trim());
        cv.put(hospitalCode, rows.length < 5 ? "" : rows[4].trim());
        cv.put(hospitalName, rows.length < 6 ? "" : rows[5].trim());
        cv.put(specCode, rows.length < 7 ? "" : rows[6].trim());
        cv.put(specDesc, rows.length < 8 ? "" : rows[7].trim());
        cv.put(schedule, rows.length < 9 ? "" : rows[8].trim());
        cv.put(room, rows.length < 10 ? "" : rows[9].trim());
        cv.put(wtax, rows.length < 11 ? "" : rows[10].trim());
        cv.put(gracePeriod, rows.length < 12 ? "" : rows[11].trim());
        cv.put(vat, rows.length < 13 ? "" : rows[12].trim());
        cv.put(specialRem, rows.length < 14 ? "" : rows[13].trim());
        cv.put(hospRemarks, rows.length < 15 ? "" : rows[14].trim());
        cv.put(roomBoard, rows.length < 16 ? "" : rows[15].trim());
        cv.put(remarks, rows.length < 17 ? "" : rows[16].trim());
        cv.put(remarks2, rows.length < 18 ? "" : rows[17].trim());
        cv.put(isSelected, "false");
        cv.put(isSelectedAsMain, "false");
//        Log.e("hospitalCode: ", String.valueOf(cv.get(hospitalCode)));
        db.insert(doctorTable, null, cv);
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public void createHospitals(Hospital hosp) {

        boolean createSuccessful = false;

        if (!checkIfExists(hosp.getHospitalCode())) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(HOSPITAL_CODE, hosp.getHospitalCode());
            values.put(HOSPITAL_NAME, hosp.getHospitalName());
            values.put(STREET, hosp.getStreetAddress());
            values.put(HOSPITAL_ABBRE, hosp.getHospitalAbbre());
            values.put(CITY_NAME, hosp.getCity());
            values.put(PROVINCE_NAME, hosp.getProvince());
            values.put(REGION_NAME, hosp.getRegion());
            values.put(REGION_DESC, hosp.getRegionDesc());
            values.put(PHONE_NO, hosp.getPhoneNo());
            values.put(CLASS, hosp.getCclass());
            values.put(CATEGORY, hosp.getCategory());
            values.put(ENTRY_TYPE, hosp.getEntryType());
            values.put(ISACCREDITED, hosp.getIsAccredited());
            createSuccessful = db.insert(hospitalTable, null, values) > 0;
            db.close();
            if (createSuccessful) {
                Log.e(TAG, hosp.getHospitalCode() + " created.");
            }
        }
    }


    public void createRoomCategory(String s) {


        boolean createSuccessful = false;

        if (!checkIfExists(s)) {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(category, s);

            createSuccessful = db.insert(roomCategoryTable, null, values) > 0;
            db.close();
            if (createSuccessful) {
                //Log.e(TAG, s + " created.");
            }
        }
    }


    public void createProcedureLIst(TestsAndProcedures obj) {


        boolean createSuccessful = false;

        if (!checkIfExists(obj.getProcedureCode())) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(isSelected, "false");
            values.put(isSelectedProc, "false");
            values.put(isSelectedProcPrime, "false");
            values.put(isSelectedProcOther, "false");
            values.put(isSelectedProcInPatient, "false");
            values.put(maceServiceType, obj.getMaceServiceType());
            values.put(procedureCode, obj.getProcedureCode());
            values.put(procedureDesc, obj.getProcedureDesc());
            values.put(procedureAmount, obj.getProcedureAmount());
            values.put(costCenter, obj.getCostCenter());

            createSuccessful = db.insert(procedureTable, null, values) > 0;
            db.close();
            if (createSuccessful) {
                //Log.e(TAG, obj.getProcedureDesc() + " created.");
            }

        }

    }


    public void createDiagnosis(DiagnosisList obj) {

        boolean createSuccessful = false;

        if (!checkIfExists(obj.getDiagCode())) {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(isSelected, "false");
            values.put(isSelectedDiagOther, "false");
            values.put(status, obj.getStatus());
            values.put(icd10Desc, obj.getIcd10Desc());
            values.put(diagCode, obj.getDiagCode());
            values.put(typeDesc, obj.getTypeDesc());
            values.put(type, obj.getType());
            values.put(diagDesc, obj.getDiagDesc());
            values.put(icd10Code, obj.getIcd10Code());
            values.put(diagRemarks, obj.getDiagRemarks());

            createSuccessful = db.insert(diagnosisTable, null, values) > 0;
            db.close();
            if (createSuccessful) {
                //Log.e(TAG, obj.getDiagCode() + " created.");
            }

        }

    }

    public void createOtherServices(Services obj) {

        boolean createSuccessful = false;

        if (!checkIfExists(obj.getId())) {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(isSelected, "false");
            values.put(id, obj.getId());
            values.put(serviceCode, "");
            values.put(serviceDesc, obj.getServiceDesc());
            values.put(createdOn, "");
            values.put(createdBy, "");
            values.put(lastUpdateOn, "");
            values.put(lastUpdateBy, "");

            createSuccessful = db.insert(otherServiceTable, null, values) > 0;
            db.close();
            if (createSuccessful) {
                //Log.e(TAG, obj.getId() + " created.");
            }

        }


    }


    public void createRoomAvail(Rooms obj) {


        boolean createSuccessful = false;

        if (!checkIfExists(obj.getPlanCode())) {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(classCategory, obj.getClassCategory());
            values.put(monthlyPremium, obj.getMonthlyPremium());
            values.put(updatedDate, obj.getUpdatedDate());
            values.put(planCode, obj.getPlanCode());
            values.put(annualPremium, obj.getAnnualPremium());
            values.put(grpType, obj.getGrpType());
            values.put(ddl, obj.getDdl());
            values.put(planDesc, obj.getPlanDesc());
            values.put(semiAnnualPremium, obj.getSemiAnnualPremium());
            values.put(wHosp, obj.getWHosp());
            values.put(quarterlyPremium, obj.getQuarterlyPremium());
            values.put(updatedBy, obj.getUpdatedBy());

            createSuccessful = db.insert(roomsTable, null, values) > 0;
            db.close();
            if (createSuccessful) {
                //Log.e(TAG, obj.getClassCategory() + " created.");
            }

        }


    }

    public boolean createDoctorsList(DoctorModelInsertion myObj) {

        boolean createSuccessful = false;

        if (!checkIfExists(myObj.getDoctorCode())) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(doctorCode, myObj.getDoctorCode());
            values.put(docLname, myObj.getDocLname());
            values.put(docFname, myObj.getDocFname());
            values.put(docMname, myObj.getDocMname());
            values.put(hospitalCode, myObj.getHospitalCode());
            values.put(hospitalName, myObj.getHospitalName());
            values.put(specDesc, myObj.getSpecDesc());
            values.put(specCode, myObj.getSpecCode());
            values.put(schedule, myObj.getSchedule());
            values.put(room, myObj.getRoom());
            values.put(wtax, myObj.getWtax());
            values.put(gracePeriod, myObj.getGracePeriod());
            values.put(vat, myObj.getVat());
            values.put(specialRem, myObj.getSpecialRem());
            values.put(hospRemarks, myObj.getHospRemarks());
            values.put(roomBoard, myObj.getRoomBoard());
            values.put(remarks, myObj.getRemarks());
            values.put(remarks2, myObj.getRemarks2());
            values.put(isSelected, "false");
            values.put(isSelectedAsMain, "false");
            createSuccessful = db.insert(doctorTable, null, values) > 0;

            db.close();

            if (createSuccessful) {
                //Log.e(TAG, myObj.getDocLname() + " created.");
            }
        }

        return createSuccessful;
    }

    /**
     * procedures
     *
     * @return
     */

    public void setALLProcedureToFalse() {


        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT * FROM " + procedureTable;
        sql += " WHERE " + isSelected + " = " + "\'true\'";
        sql += " OR " + isSelectedProcOther + " = " + "\'true\'";
        sql += " OR " + isSelectedProcPrime + " = " + "\'true\'";
        sql += " OR " + isSelectedProcInPatient + " = " + "\"true\"";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        int x = 0;

        if (cursor.moveToFirst()) {
            do {

                String objectProcCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                //Log.e(TAG, "objectName: " + objectProcCode);

                setDataProceduretoFALSE(objectProcCode);
                setDataProcPrimarytoFALSE(objectProcCode);
                setDataProcOthertoFALSE(objectProcCode);
                setDataProcInPatienttoFalse(objectProcCode);
                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();


    }

    //used for inpatient doctor select
    public void setDataInPatientDoctortoTRUE(String ID) {

        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        sql = doctorCode + " = '" + ID + "'";
        ContentValues con = new ContentValues();
        con.put(isSelected, "true");
        db.update(doctorTable, con, sql, null);
        Log.d("ID", ID);
    }

    //used for inpatient doctor select
    public void setDataInPatientDoctortoFALSE(String ID) {

        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        sql = doctorCode + " = '" + ID + "'";
        ContentValues con = new ContentValues();
        con.put(isSelected, "false");
        db.update(doctorTable, con, sql, null);
        Log.d("ID", ID);
    }


    public void setDataProceduretoTRUE(String ID) {

        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        sql = procedureCode + " = '" + ID + "'";
        ContentValues con = new ContentValues();
        con.put(isSelected, "true");
        db.update(procedureTable, con, sql, null);
        Log.d("ID", ID);
    }

    public void setDataProcPrimarytoTRUE(String ID) {

        String sql = "";

        SQLiteDatabase db = this.getWritableDatabase();


        sql = procedureCode + " = '" + ID + "'";
        ContentValues con = new ContentValues();
        con.put(isSelectedProcPrime, "true");
        db.update(procedureTable, con, sql, null);
        Log.d("ID", ID);
    }


    public void setDataProcOthertoTRUE(String ID) {

        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        sql = procedureCode + " = '" + ID + "'";
        ContentValues con = new ContentValues();
        con.put(isSelectedProcOther, "true");
        db.update(procedureTable, con, sql, null);
        Log.d("ID", ID);
    }


    public void setDataProceduretoFALSE(String ID) {

        String sql = "";

        SQLiteDatabase db = this.getWritableDatabase();


        sql = procedureCode + " = '" + ID + "'";
        ContentValues con = new ContentValues();
        con.put(isSelected, "false");
        db.update(procedureTable, con, sql, null);


    }

    public void setDataProcPrimarytoFALSE(String ID) {

        String sql = "";

        SQLiteDatabase db = this.getWritableDatabase();


        sql = procedureCode + " = '" + ID + "'";
        ContentValues con = new ContentValues();
        con.put(isSelectedProcPrime, "false");
        db.update(procedureTable, con, sql, null);
        Log.d("ID", ID);
    }

    public void setDataProcOthertoFALSE(String ID) {

        String sql = "";

        SQLiteDatabase db = this.getWritableDatabase();


        sql = procedureCode + " = '" + ID + "'";
        ContentValues con = new ContentValues();
        con.put(isSelectedProcOther, "false");
        db.update(procedureTable, con, sql, null);


        Log.d("ID", ID);
    }

    //used for inpatient doctor select
    public void updateAllExcludedHospToFalse() {

        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        sql = ISEXCLUDED + " = 'true'";
        ContentValues con = new ContentValues();
        con.put(ISEXCLUDED, "false");
        db.update(hospitalTable, con, sql, null);

    }


    public boolean getDataProcedureFromTrue(String ID) {

        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT " + isSelected + " FROM " + procedureTable;
        sql += " WHERE " + procedureCode + "='" + ID + "'";
        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();
        // execute the query
        Cursor cursor = db.rawQuery(sql, null);
        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                objectIsTrue = cursor.getString(cursor.getColumnIndex(isSelected));
                //Log.e(TAG, "objectName: " + objectIsTrue);
                x++;
            } while (cursor.moveToNext());
        }


        if (objectIsTrue.equals("false")) {
            return false;
        } else return true;
    }

    public boolean getDataProcInPatientFromTrue(String ID) {


        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT " + isSelectedProcInPatient + " FROM " + procedureTable;
        sql += " WHERE " + procedureCode + "='" + ID + "'";
        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();


        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                objectIsTrue = cursor.getString(cursor.getColumnIndex(isSelectedProcInPatient));

                Log.e("e_", "objectName: " + objectIsTrue);

                x++;


            } while (cursor.moveToNext());
        }


        if (objectIsTrue.equals("false")) {
            return false;
        } else return true;


    }

    public boolean getDataProcOtherFromTrue(String ID) {


        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT " + isSelectedProcOther + " FROM " + procedureTable;
        sql += " WHERE " + procedureCode + "='" + ID + "'";
        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();


        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                objectIsTrue = cursor.getString(cursor.getColumnIndex(isSelectedProcOther));

                //Log.e(TAG, "objectName: " + objectIsTrue);

                x++;


            } while (cursor.moveToNext());
        }


        if (objectIsTrue.equals("false")) {
            return false;
        } else return true;


    }


    public void setDataProcInPatienttoTRUE(String ID) {
        Log.d("TO_TRUE", "TO_TRUE");
        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        sql = procedureCode + " = '" + ID + "'";
        ContentValues con = new ContentValues();
        con.put(isSelectedProcInPatient, "true");
        db.update(procedureTable, con, sql, null);


        Log.d("ID", ID);
    }

    public void setDataProcInPatienttoFalse(String ID) {
        Log.d("TO_TRUE", "TO_FALSE");
        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        sql = procedureCode + " = '" + ID + "'";
        ContentValues con = new ContentValues();
        con.put(isSelectedProcInPatient, "false");
        db.update(procedureTable, con, sql, null);


        Log.d("db_ID", ID);

    }

    public boolean getDataProcPrimaryFromTrue(String ID) {


        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT " + isSelectedProcPrime + " FROM " + procedureTable;
        sql += " WHERE " + procedureCode + " = '" + ID + "'";
        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();


        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                objectIsTrue = cursor.getString(cursor.getColumnIndex(isSelectedProcPrime));

                //Log.e(TAG, "objectName: " + objectIsTrue);

                x++;


            } while (cursor.moveToNext());
        }


        if (objectIsTrue.equals("false")) {
            return false;
        } else return true;


    }


    public ArrayList<String> retrieveRoomCategory(String searchTerm) {
        ArrayList<String> arrayList = new ArrayList<>();
        Cursor cursor;
        SQLiteDatabase db = this.getWritableDatabase();

        String sql = "";
        sql += "SELECT * FROM " + roomCategoryTable;
        sql += " WHERE " + "UPPER(" + category + ") LIKE '%" + searchTerm + "%' ";

        cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                String getCategory = getCursorData(cursor, category);
                arrayList.add(getCategory);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return arrayList;
    }


    public ArrayList<TestsAndProcedures> retrieveProcedure(String searchTerm, String[] procedures, Integer serviceType) {
        ArrayList<TestsAndProcedures> arrayList = new ArrayList<>();
        Cursor cursor;
        SQLiteDatabase db = this.getWritableDatabase();


        String sql = "";
        sql += "SELECT * FROM " + procedureTable;
        sql += " WHERE " + "UPPER(" + procedureDesc + ") = '" + searchTerm + "'";
        sql += addMultipleProcCodes(procedures);
        sql += filterServiceType(serviceType);

//        sql += " ORDER BY " + procedureDesc + " ASC";
        //Log.e(TAG, "objectName: " + sql);

        // execute the query
        cursor = db.rawQuery(sql, null);


        if (cursor.moveToFirst()) {
            do {

                String objserviceClassCode = cursor.getString(cursor.getColumnIndex(costCenter));
                String objectprocedureCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                String objectprocedureDesc = cursor.getString(cursor.getColumnIndex(procedureDesc));
                String objectprocedureAmount = cursor.getString(cursor.getColumnIndex(procedureAmount));
                String objectid = cursor.getString(cursor.getColumnIndex(maceServiceType));
                //Log.e(TAG, "objectName: " + objectprocedureDesc);

                TestsAndProcedures insertion = new TestsAndProcedures();
                insertion.setProcedureAmount(objectprocedureAmount);
                insertion.setProcedureCode(objectprocedureCode);
                insertion.setProcedureDesc(objectprocedureDesc);
                insertion.setCostCenter(objserviceClassCode);
                insertion.setMaceServiceType(objectid);

                arrayList.add(insertion);

            } while (cursor.moveToNext());
        }

        cursor.close();
        sql = "";
        sql += "SELECT * FROM " + procedureTable;
        sql += " WHERE " + "UPPER(" + procedureDesc + ") LIKE '%" + searchTerm + "%' ";
        sql += addMultipleProcCodes(procedures);
        sql += filterServiceType(serviceType);

        if (!searchTerm.equals("")) {
            sql += "AND " + "UPPER(" + procedureDesc + ") <> '" + searchTerm + "'";
        }

//        sql += " ORDER BY " + procedureDesc + " ASC";
        //Log.e(TAG, "objectName: " + sql);

        // execute the query
        cursor = db.rawQuery(sql, null);


        if (cursor.moveToFirst()) {
            do {

                String objserviceClassCode = cursor.getString(cursor.getColumnIndex(costCenter));
                String objectprocedureCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                String objectprocedureDesc = cursor.getString(cursor.getColumnIndex(procedureDesc));
                String objectprocedureAmount = cursor.getString(cursor.getColumnIndex(procedureAmount));
                String objectid = cursor.getString(cursor.getColumnIndex(maceServiceType));
                //Log.e(TAG, "objectName: " + objectprocedureDesc);

                TestsAndProcedures insertion = new TestsAndProcedures();
                insertion.setProcedureAmount(objectprocedureAmount);
                insertion.setProcedureCode(objectprocedureCode);
                insertion.setProcedureDesc(objectprocedureDesc);
                insertion.setCostCenter(objserviceClassCode);
                insertion.setMaceServiceType(objectid);

                arrayList.add(insertion);


            } while (cursor.moveToNext());
        }

        cursor.close();


        db.close();
        Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;

    }

    private String filterServiceType(Integer serviceType) {
        String sql = " AND ";
        if (serviceType == 2)
            sql += maceServiceType + " != 'Procedure' ";
        else if (serviceType == 3)
            sql += maceServiceType + " = 'Procedure' ";
        else
            sql = "";
        return sql;
    }

    public ArrayList<TestsAndProcedures> retrieveProcedureAllWithExceptions(String searchTerm, String[] exceptions, Integer serviceType) {
        ArrayList<TestsAndProcedures> arrayList = new ArrayList<>();
        Cursor cursor;
        SQLiteDatabase db = this.getWritableDatabase();


        String sql = "";
        sql += "SELECT * FROM " + procedureTable;
        sql += " WHERE " + "UPPER(" + procedureDesc + ") = '" + searchTerm + "'";
        sql += removeMultipleProcCodes(exceptions);
        sql += filterServiceType(serviceType);
//        sql += " ORDER BY " + procedureDesc + " ASC";
        //Log.e(TAG, "objectName: " + sql);


        // execute the query
        cursor = db.rawQuery(sql, null);


        if (cursor.moveToFirst()) {
            do {

                String objserviceClassCode = cursor.getString(cursor.getColumnIndex(costCenter));
                String objectprocedureCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                String objectprocedureDesc = cursor.getString(cursor.getColumnIndex(procedureDesc));
                String objectprocedureAmount = cursor.getString(cursor.getColumnIndex(procedureAmount));
                String objectid = cursor.getString(cursor.getColumnIndex(maceServiceType));
                //Log.e(TAG, "objectName: " + objectprocedureDesc);

                TestsAndProcedures insertion = new TestsAndProcedures();
                insertion.setProcedureAmount(objectprocedureAmount);
                insertion.setProcedureCode(objectprocedureCode);
                insertion.setProcedureDesc(objectprocedureDesc);
                insertion.setCostCenter(objserviceClassCode);
                insertion.setMaceServiceType(objectid);

                arrayList.add(insertion);

            } while (cursor.moveToNext());
        }

        cursor.close();
        sql = "";
        sql += "SELECT * FROM " + procedureTable;
        sql += " WHERE " + "UPPER(" + procedureDesc + ") LIKE '%" + searchTerm + "%' ";
        sql += removeMultipleProcCodes(exceptions);
        sql += filterServiceType(serviceType);
        if (!searchTerm.equals("")) {
            sql += "AND " + "UPPER(" + procedureDesc + ") <> '" + searchTerm + "'";
        }

//        sql += " ORDER BY " + procedureDesc + " ASC";
//        Log.e(TAG, "objectName: " + sql);

        // execute the query
        cursor = db.rawQuery(sql, null);


        if (cursor.moveToFirst()) {
            do {

                String objserviceClassCode = cursor.getString(cursor.getColumnIndex(costCenter));
                String objectprocedureCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                String objectprocedureDesc = cursor.getString(cursor.getColumnIndex(procedureDesc));
                String objectprocedureAmount = cursor.getString(cursor.getColumnIndex(procedureAmount));
                String objectid = cursor.getString(cursor.getColumnIndex(maceServiceType));
                //Log.e(TAG, "objectName: " + objectprocedureDesc);

                TestsAndProcedures insertion = new TestsAndProcedures();
                insertion.setProcedureAmount(objectprocedureAmount);
                insertion.setProcedureCode(objectprocedureCode);
                insertion.setProcedureDesc(objectprocedureDesc);
                insertion.setCostCenter(objserviceClassCode);
                insertion.setMaceServiceType(objectid);

                arrayList.add(insertion);


            } while (cursor.moveToNext());
        }

        cursor.close();


        db.close();
        Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;

    }

    public ArrayList<TestsAndProcedures> retrieveProcedureAll(String searchTerm) {
        ArrayList<TestsAndProcedures> arrayList = new ArrayList<>();
        Cursor cursor;
        SQLiteDatabase db = this.getWritableDatabase();


        String sql = "";
        sql += "SELECT * FROM " + procedureTable;
        sql += " WHERE " + "UPPER(" + procedureDesc + ") = '" + searchTerm + "'";
//        sql += " ORDER BY " + procedureDesc + " ASC";
        //Log.e(TAG, "objectName: " + sql);

        // execute the query
        cursor = db.rawQuery(sql, null);


        if (cursor.moveToFirst()) {
            do {

                String objserviceClassCode = cursor.getString(cursor.getColumnIndex(costCenter));
                String objectprocedureCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                String objectprocedureDesc = cursor.getString(cursor.getColumnIndex(procedureDesc));
                String objectprocedureAmount = cursor.getString(cursor.getColumnIndex(procedureAmount));
                String objectid = cursor.getString(cursor.getColumnIndex(maceServiceType));
                //Log.e(TAG, "objectName: " + objectprocedureDesc);

                TestsAndProcedures insertion = new TestsAndProcedures();
                insertion.setProcedureAmount(objectprocedureAmount);
                insertion.setProcedureCode(objectprocedureCode);
                insertion.setProcedureDesc(objectprocedureDesc);
                insertion.setCostCenter(objserviceClassCode);
                insertion.setMaceServiceType(objectid);

                arrayList.add(insertion);

            } while (cursor.moveToNext());
        }

        cursor.close();
        sql = "";
        sql += "SELECT * FROM " + procedureTable;
        sql += " WHERE " + "UPPER(" + procedureDesc + ") LIKE '%" + searchTerm + "%' ";

        if (!searchTerm.equals("")) {
            sql += "AND " + "UPPER(" + procedureDesc + ") <> '" + searchTerm + "'";
        }
        sql += " ORDER BY " + isSelected + " DESC";
        sql += " LIMIT 50";

//        sql += " ORDER BY " + procedureDesc + " ASC";
        Log.e(TAG, "objectName: " + sql);


        // execute the query
        cursor = db.rawQuery(sql, null);


        if (cursor.moveToFirst()) {
            do {

                String objserviceClassCode = cursor.getString(cursor.getColumnIndex(costCenter));
                String objectprocedureCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                String objectprocedureDesc = cursor.getString(cursor.getColumnIndex(procedureDesc));
                String objectprocedureAmount = cursor.getString(cursor.getColumnIndex(procedureAmount));
                String objectid = cursor.getString(cursor.getColumnIndex(maceServiceType));
                //Log.e(TAG, "objectName: " + objectprocedureDesc);

                TestsAndProcedures insertion = new TestsAndProcedures();
                insertion.setProcedureAmount(objectprocedureAmount);
                insertion.setProcedureCode(objectprocedureCode);
                insertion.setProcedureDesc(objectprocedureDesc);
                insertion.setCostCenter(objserviceClassCode);
                insertion.setMaceServiceType(objectid);

                arrayList.add(insertion);


            } while (cursor.moveToNext());
        }

        cursor.close();


        db.close();
        Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;

    }


    private String addMultipleProcCodes(String[] procedures) {
        String data = "";


        if (procedures.length != 0) {
            data += " AND ( ";
            for (int x = 0; x < procedures.length; x++) {
                data += procedureCode + " = '" + procedures[x] + "' OR ";
            }

            data = data.substring(0, data.length() - 3);
            data += " ) ";
        } else {
            data += " AND ";
            data += procedureCode + " = " + "'NONE'";
        }
        return data;

    }

    private String removeMultipleProcCodes(String[] procedures) {
        String data = "";


        if (procedures.length > 0) {
            data += " AND " + procedureCode + " NOT IN (";
            for (int x = 0; x < procedures.length; x++) {
                if (x == 0)
                    data += "'" + procedures[x] + "'";
                else
                    data += ", '" + procedures[x] + "'";
            }
            data += " ) ";
        } else {
            data += " AND ";
            data += procedureCode + " != '" + procedures[0] + "'";
        }
        return data;

    }


    public ArrayList<TestsAndProcedures> retrieveProcPrimary(String searchTerm) {
        ArrayList<TestsAndProcedures> arrayList = new ArrayList<>();
        Cursor cursor;
        SQLiteDatabase db = this.getWritableDatabase();


        String sql = "";
        sql += "SELECT * FROM " + procedureTable;
        sql += " WHERE " + "UPPER(" + procedureDesc + ") = '" + searchTerm + "'";
//        sql += " ORDER BY " + procedureDesc + " ASC";
        //Log.e(TAG, "objectName: " + sql);


        // execute the query
        cursor = db.rawQuery(sql, null);


        if (cursor.moveToFirst()) {
            do {


                String objserviceClassCode = cursor.getString(cursor.getColumnIndex(costCenter));
                String objectprocedureCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                String objectprocedureDesc = cursor.getString(cursor.getColumnIndex(procedureDesc));
                String objectprocedureAmount = cursor.getString(cursor.getColumnIndex(procedureAmount));
                String objectid = cursor.getString(cursor.getColumnIndex(maceServiceType));
                //Log.e(TAG, "objectName: " + objectprocedureDesc);

                TestsAndProcedures insertion = new TestsAndProcedures();
                insertion.setProcedureAmount(objectprocedureAmount);
                insertion.setProcedureCode(objectprocedureCode);
                insertion.setProcedureDesc(objectprocedureDesc);
                insertion.setCostCenter(objserviceClassCode);
                insertion.setMaceServiceType(objectid);

                arrayList.add(insertion);

            } while (cursor.moveToNext());
        }

        cursor.close();
        sql = "";
        sql += "SELECT * FROM " + procedureTable;
        sql += " WHERE " + "UPPER(" + procedureDesc + ") LIKE '%" + searchTerm + "%' ";

        if (!searchTerm.equals("")) {
            sql += "AND " + "UPPER(" + procedureDesc + ") <> '" + searchTerm + "'";
        }

//        sql += " ORDER BY " + procedureDesc + " ASC";
        //Log.e(TAG, "objectName: " + sql);


        // execute the query
        cursor = db.rawQuery(sql, null);


        if (cursor.moveToFirst()) {
            do {


                String objserviceClassCode = cursor.getString(cursor.getColumnIndex(costCenter));
                String objectprocedureCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                String objectprocedureDesc = cursor.getString(cursor.getColumnIndex(procedureDesc));
                String objectprocedureAmount = cursor.getString(cursor.getColumnIndex(procedureAmount));
                String objectid = cursor.getString(cursor.getColumnIndex(maceServiceType));
                //Log.e(TAG, "objectName: " + objectprocedureDesc);

                TestsAndProcedures insertion = new TestsAndProcedures();
                insertion.setProcedureAmount(objectprocedureAmount);
                insertion.setProcedureCode(objectprocedureCode);
                insertion.setProcedureDesc(objectprocedureDesc);
                insertion.setCostCenter(objserviceClassCode);
                insertion.setMaceServiceType(objectid);

                arrayList.add(insertion);

            } while (cursor.moveToNext());
        }

        cursor.close();


        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;

    }


    public ArrayList<TestsAndProcedures> retrieveSelectedProceduresInPatient() {
        ArrayList<TestsAndProcedures> arrayList = new ArrayList<>();


        String sql = "";
        sql += "SELECT * FROM " + procedureTable + " WHERE " + isSelectedProcInPatient + " = \"true\"";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {


                String objserviceClassCode = cursor.getString(cursor.getColumnIndex(costCenter));
                String objectprocedureCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                String objectprocedureDesc = cursor.getString(cursor.getColumnIndex(procedureDesc));
                String objectprocedureAmount = cursor.getString(cursor.getColumnIndex(procedureAmount));
                String objectid = cursor.getString(cursor.getColumnIndex(maceServiceType));
                //Log.e(TAG, "objectName: " + objectprocedureDesc);

                TestsAndProcedures insertion = new TestsAndProcedures();
                insertion.setProcedureAmount(objectprocedureAmount);
                insertion.setProcedureCode(objectprocedureCode);
                insertion.setProcedureDesc(objectprocedureDesc);
                insertion.setCostCenter(objserviceClassCode);
                insertion.setMaceServiceType(objectid);
                insertion.setOriginalPrice(objectprocedureAmount);


                arrayList.add(insertion);
                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;

    }


    public ArrayList<TestsAndProcedures> retrieveSelectedProcedures() {
        ArrayList<TestsAndProcedures> arrayList = new ArrayList<>();

        String sql = "";
        sql += "SELECT * FROM " + procedureTable + " WHERE " + isSelected + " = \'true\'";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {


                String objserviceClassCode = cursor.getString(cursor.getColumnIndex(costCenter));
                String objectprocedureCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                String objectprocedureDesc = cursor.getString(cursor.getColumnIndex(procedureDesc));
                String objectprocedureAmount = cursor.getString(cursor.getColumnIndex(procedureAmount));
                String objectid = cursor.getString(cursor.getColumnIndex(maceServiceType));
                //Log.e(TAG, "objectName: " + objectprocedureDesc);

                TestsAndProcedures insertion = new TestsAndProcedures();
                insertion.setProcedureAmount(objectprocedureAmount);
                insertion.setProcedureCode(objectprocedureCode);
                insertion.setProcedureDesc(objectprocedureDesc);
                insertion.setCostCenter(objserviceClassCode);
                insertion.setMaceServiceType(objectid);
                insertion.setOriginalPrice(objectprocedureAmount);

                arrayList.add(insertion);
                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;

    }

    //used
    public ArrayList<DoctorModelInsertion> retrieveSelectedInpatientDoctor() {
        ArrayList<DoctorModelInsertion> arrayList = new ArrayList<>();


        String sql = "";
        sql += "SELECT * FROM " + doctorTable + " WHERE " + isSelected + " = \'true\'";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                DoctorModelInsertion insertion = new DoctorModelInsertion();
                insertion.setIsSelected(getCursorData(cursor, isSelected));
                insertion.setDocFname(getCursorData(cursor, docFname));
                insertion.setDocLname(getCursorData(cursor, docLname));
                insertion.setDocMname(getCursorData(cursor, docMname));
                insertion.setDoctorCode(getCursorData(cursor, doctorCode));
                insertion.setSpecDesc(getCursorData(cursor, specDesc));
                arrayList.add(insertion);
                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;

    }



    public ArrayList<DoctorModelMultipleSelection> retrieveSelectedDoctors() {
        ArrayList<DoctorModelMultipleSelection> arrayList = new ArrayList<>();


        String sql = "";
        sql += "SELECT * FROM " + doctorTable + " WHERE " + isSelected + " = \'true\'";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {


                DoctorModelMultipleSelection insertion = new DoctorModelMultipleSelection();
                insertion.setIsSelected(getCursorData(cursor, isSelected));
                insertion.setDocFname(getCursorData(cursor, docFname));
                insertion.setDocLname(getCursorData(cursor, docLname));
                insertion.setDocMname(getCursorData(cursor, docMname));
                insertion.setDoctorCode(getCursorData(cursor, doctorCode));
                insertion.setSpecDesc(getCursorData(cursor, specDesc));

                arrayList.add(insertion);
                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;

    }

    private String getCursorData(Cursor cursor, String key) {
        return cursor.getString(cursor.getColumnIndex(key));
    }


    public ArrayList<DiagnosisList> retrieveSelectedDiagnosis() {
        ArrayList<DiagnosisList> arrayList = new ArrayList<>();

        String sql = "";
        sql += "SELECT * FROM " + diagnosisTable + " WHERE " + isSelected + " = \'true\'";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {


                String objectdiagCode = cursor.getString(cursor.getColumnIndex(diagCode));
                String objecticd10 = cursor.getString(cursor.getColumnIndex(icd10Code));
                String objectDiagDesc = cursor.getString(cursor.getColumnIndex(diagDesc));

                //Log.e(TAG, "objectName: " + objectDiagDesc);

                DiagnosisList insertion = new DiagnosisList();
                insertion.setDiagCode(objectdiagCode);
                insertion.setDiagDesc(objectDiagDesc);
                insertion.setIcd10Code(objecticd10);


                arrayList.add(insertion);
                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;

    }


    public ArrayList<DiagnosisList> retrieveSelectedOtherDiagnosis() {
        ArrayList<DiagnosisList> arrayList = new ArrayList<>();


        String sql = "";
        sql += "SELECT * FROM " + diagnosisTable + " WHERE " + isSelectedDiagOther + " = \'true\'";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {


                String objectdiagCode = cursor.getString(cursor.getColumnIndex(diagCode));
                String objecticd10 = cursor.getString(cursor.getColumnIndex(icd10Code));
                String objectDiagDesc = cursor.getString(cursor.getColumnIndex(diagDesc));

                //Log.e(TAG, "objectName: " + objectDiagDesc);

                DiagnosisList insertion = new DiagnosisList();
                insertion.setDiagCode(objectdiagCode);
                insertion.setDiagDesc(objectDiagDesc);
                insertion.setIcd10Code(objecticd10);


                arrayList.add(insertion);
                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;

    }


    public DoctorModelInsertion retrieveDoctor(String code) {
        DoctorModelInsertion doctorModelInsertion = new DoctorModelInsertion();


        String sql = "";
        sql += "SELECT * FROM " + doctorTable + " WHERE " + doctorCode + " = '" + code + "'";
        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        if (cursor.moveToFirst()) {

            doctorModelInsertion.setDocFname(getCursorData(cursor, docFname));
            doctorModelInsertion.setDocLname(getCursorData(cursor, docLname));
            doctorModelInsertion.setDocMname(getCursorData(cursor, docMname));
            doctorModelInsertion.setDoctorCode(getCursorData(cursor, doctorCode));
            doctorModelInsertion.setSpecCode(getCursorData(cursor, specCode));
            doctorModelInsertion.setSpecDesc(getCursorData(cursor, specDesc));


        }

        return doctorModelInsertion;
    }


    public DiagnosisList retrieveOtherDiagnosis(String code) {
        DiagnosisList diagnosisList = new DiagnosisList();


        String sql = "";
        sql += "SELECT * FROM " + diagnosisTable + " WHERE " + diagCode + " = '" + code + "'";


        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        if (cursor.moveToFirst()) {


            String objectdiagCode = cursor.getString(cursor.getColumnIndex(diagCode));
            String objecticd10 = cursor.getString(cursor.getColumnIndex(icd10Code));
            String objectDiagDesc = cursor.getString(cursor.getColumnIndex(diagDesc));

            String objectStatus = cursor.getString(cursor.getColumnIndex(status));
            String objectTypeDesc = cursor.getString(cursor.getColumnIndex(typeDesc));
            String objectType = cursor.getString(cursor.getColumnIndex(type));
            String objectIcd10Desc = cursor.getString(cursor.getColumnIndex(icd10Desc));
            String objectDiagRemarks = cursor.getString(cursor.getColumnIndex(diagRemarks));

            //Log.e(TAG, "objectName: " + objectDiagDesc);

            DiagnosisList insertion = new DiagnosisList();
            insertion.setDiagCode(objectdiagCode);
            insertion.setDiagDesc(objectDiagDesc);
            insertion.setIcd10Code(objecticd10);

            insertion.setStatus(objectStatus);
            insertion.setTypeDesc(objectTypeDesc);
            insertion.setType(objectType);
            insertion.setIcd10Desc(objectIcd10Desc);
            insertion.setDiagRemarks(objectDiagRemarks);

            diagnosisList = insertion;
        }

        cursor.close();
        db.close();
//        //Log.e(TAG, "objectName: " + arrayList.toString());
        return diagnosisList;

    }


    public void setDataDiagnosetoTRUE(String ID) {

        String sql = "";

        SQLiteDatabase db = this.getWritableDatabase();
        sql = diagCode + " = \'" + ID + "\'";
        ContentValues con = new ContentValues();
        con.put(isSelected, "true");
        db.update(diagnosisTable, con, sql, null);


        Log.d("ID", ID);
    }

    public void setDataDiagnoseOthertoTRUE(String ID) {

        String sql = "";

        SQLiteDatabase db = this.getWritableDatabase();
        sql = diagCode + " = \'" + ID + "\'";
        ContentValues con = new ContentValues();
        con.put(isSelectedDiagOther, "true");
        db.update(diagnosisTable, con, sql, null);


        Log.d("ID", ID);
    }

    public void setDataDiagnosetoFALSE(String ID) {

        String sql = "";

        SQLiteDatabase db = this.getWritableDatabase();
        sql = diagCode + " =  \'" + ID + "\'";
        ContentValues con = new ContentValues();
        con.put(isSelected, "false");
        db.update(diagnosisTable, con, sql, null);

        Log.d("ID", ID);
    }

    public void setDataDiagnoseOtherToFALSE(String ID) {

        String sql = "";

        SQLiteDatabase db = this.getWritableDatabase();
        sql = diagCode + " =  \'" + ID + "\'";
        ContentValues con = new ContentValues();
        con.put(isSelectedDiagOther, "false");
        db.update(diagnosisTable, con, sql, null);

        Log.d("ID", ID);
    }


    public boolean getDataDiagnosisFromTrue(String ID) {


        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT " + isSelected + " FROM " + diagnosisTable;
        sql += " WHERE " + diagCode + "=" + "\'" + ID + "\'";
        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();


        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                objectIsTrue = cursor.getString(cursor.getColumnIndex(isSelected));

                //Log.e(TAG, "objectName: " + objectIsTrue);

                x++;


            } while (cursor.moveToNext());
        }


        if (objectIsTrue.equals("false")) {
            return false;
        } else return true;


    }


    public boolean getDataDiagnosisOtherFromTrue(String ID) {


        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT " + isSelectedDiagOther + " FROM " + diagnosisTable;
        sql += " WHERE " + diagCode + "=" + "\'" + ID + "\'";
        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();


        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                objectIsTrue = cursor.getString(cursor.getColumnIndex(isSelectedDiagOther));

                //Log.e(TAG, "objectName: " + objectIsTrue);

                x++;


            } while (cursor.moveToNext());
        }


        if (objectIsTrue.equals("false")) {
            return false;
        } else return true;


    }


    public ArrayList<DiagnosisList> retrieveDiagnose(String search) {
        ArrayList<DiagnosisList> arrayList = new ArrayList<>();
        Cursor cursor;
        SQLiteDatabase db;
        // select query
        db = this.getWritableDatabase();
        String sql1 = "";

        String searchTerm = search.toUpperCase();

        sql1 += "SELECT * FROM " + diagnosisTable;
        sql1 += " WHERE " + "UPPER(" + diagDesc + ") = '" + searchTerm + "'";

        sql1 += " ORDER BY " + diagDesc + " ASC";

        //Log.e(TAG, "objectName: " + sql1);

        // execute the query
        cursor = db.rawQuery(sql1, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                String objecIcdCode = cursor.getString(cursor.getColumnIndex(icd10Code));
                String objectDiagDesc = cursor.getString(cursor.getColumnIndex(diagDesc));
                String objectDiagCode = cursor.getString(cursor.getColumnIndex(diagCode));
                //   String objectProcGroupDesc = cursor.getString(cursor.getColumnIndex(diagProcGroupDesc));

                //Log.e(TAG, "objectName: " + objectDiagDesc);

                DiagnosisList insertion = new DiagnosisList();
                insertion.setIcd10Code(objecIcdCode);
                insertion.setDiagDesc(objectDiagDesc);
                insertion.setDiagCode(objectDiagCode);

                arrayList.add(insertion);
                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
//        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
//


        ////////
        // select query
        String sql2 = "";
        sql2 += "SELECT * FROM " + diagnosisTable;
        sql2 += " WHERE " + "UPPER(" + diagDesc + ") LIKE '%" + searchTerm + "%' ";
        if (!searchTerm.equals("")) {
            sql2 += "AND " + "UPPER(" + diagDesc + ") <> '" + searchTerm + "'";
        }
        sql2 += " ORDER BY " + isSelected + " DESC";
        sql2 += ", " + diagDesc + " ASC";
        sql2 += " LIMIT 50";
        Log.e(TAG, "diagnosisTable objectName: " + sql2);
        cursor = db.rawQuery(sql2, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                String objecIcdCode = cursor.getString(cursor.getColumnIndex(icd10Code));
                String objectDiagDesc = cursor.getString(cursor.getColumnIndex(diagDesc));
                String objectDiagCode = cursor.getString(cursor.getColumnIndex(diagCode));
                //   String objectProcGroupDesc = cursor.getString(cursor.getColumnIndex(diagProcGroupDesc));

                //Log.e(TAG, "objectName: " + objectDiagDesc);

                DiagnosisList insertion = new DiagnosisList();
                insertion.setIcd10Code(objecIcdCode);
                insertion.setDiagDesc(objectDiagDesc);
                insertion.setDiagCode(objectDiagCode);

                arrayList.add(insertion);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());


        return arrayList;

    }

    public ArrayList<DiagnosisList> retrieveDiagnoseExcluded(String search, String excludedDiag) {
        ArrayList<DiagnosisList> arrayList = new ArrayList<>();
        Cursor cursor;
        SQLiteDatabase db;
        // select query
        db = this.getWritableDatabase();
        String sql1 = "";

        String searchTerm = search.toUpperCase();

        sql1 += "SELECT * FROM " + diagnosisTable;
        sql1 += " WHERE " + "UPPER(" + diagDesc + ") = '" + searchTerm + "'";

        sql1 += " AND " + diagCode + " NOT IN (" + excludedDiag + ")";
        sql1 += " AND " + diagDesc + " !=  '1'";

        sql1 += " ORDER BY " + diagDesc + " ASC";

        //Log.e(TAG, "objectName: " + sql1);

        // execute the query
        cursor = db.rawQuery(sql1, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                String objecIcdCode = cursor.getString(cursor.getColumnIndex(icd10Code));
                String objectDiagDesc = cursor.getString(cursor.getColumnIndex(diagDesc));
                String objectDiagCode = cursor.getString(cursor.getColumnIndex(diagCode));
                //   String objectProcGroupDesc = cursor.getString(cursor.getColumnIndex(diagProcGroupDesc));

                //Log.e(TAG, "objectName: " + objectDiagDesc);

                DiagnosisList insertion = new DiagnosisList();
                insertion.setIcd10Code(objecIcdCode);
                insertion.setDiagDesc(objectDiagDesc);
                insertion.setDiagCode(objectDiagCode);

                arrayList.add(insertion);
                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
//        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
//


        ////////
        // select query
        String sql2 = "";
        sql2 += "SELECT * FROM " + diagnosisTable;
        sql2 += " WHERE " + "UPPER(" + diagDesc + ") LIKE '%" + searchTerm + "%' ";
        if (!searchTerm.equals("")) {
            sql2 += "AND " + "UPPER(" + diagDesc + ") <> '" + searchTerm + "'";
        }
        sql2 += " AND " + diagCode + " NOT IN (" + excludedDiag + ")";
        sql1 += " AND " + diagDesc + " !=  '1'";
        sql2 += " ORDER BY " + diagDesc + " ASC";
        //Log.e(TAG, "objectName: " + sql2);
        cursor = db.rawQuery(sql2, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                String objecIcdCode = cursor.getString(cursor.getColumnIndex(icd10Code));
                String objectDiagDesc = cursor.getString(cursor.getColumnIndex(diagDesc));
                String objectDiagCode = cursor.getString(cursor.getColumnIndex(diagCode));
                //   String objectProcGroupDesc = cursor.getString(cursor.getColumnIndex(diagProcGroupDesc));

                //Log.e(TAG, "objectName: " + objectDiagDesc);

                DiagnosisList insertion = new DiagnosisList();
                insertion.setIcd10Code(objecIcdCode);
                insertion.setDiagDesc(objectDiagDesc);
                insertion.setDiagCode(objectDiagCode);

                arrayList.add(insertion);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());


        return arrayList;

    }


    public ArrayList<DoctorModelInsertion> retrieveDoctorList(String searchTerm, String isFromMaternityOrigin) {
        ArrayList<DoctorModelInsertion> arrayList = new ArrayList<>();

        // select query
        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor;


        sql += "SELECT * FROM " + doctorTable;
        sql += " WHERE " + "UPPER(" + docLname + ") = '" + searchTerm + "'";
        sql += " AND " + hospitalCode + " = '" + SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context) + "'";
        sql += " AND " + isSelectedAsMain + " = \'false\' ";
        sql += " AND " + docLname + " IS NOT NULL ";
        sql += " AND " + docLname + " != ''";
        sql += " ORDER BY " + docLname + " ASC";
        sql += " LIMIT 50";


        Log.e(TAG, "objectName: " + sql);
        cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                String objectLName = cursor.getString(cursor.getColumnIndex(docLname));
                String objectMName = cursor.getString(cursor.getColumnIndex(docMname));
                String objectFName = cursor.getString(cursor.getColumnIndex(docFname));
                String objectDescription = cursor.getString(cursor.getColumnIndex(specDesc));
                String objectCode = cursor.getString(cursor.getColumnIndex(doctorCode));
                String objectHospitalCode = cursor.getString(cursor.getColumnIndex(hospitalCode));
                String objectHospitalName = cursor.getString(cursor.getColumnIndex(hospitalName));
                //Log.e(TAG, "objectName: " + objectLName);

                DoctorModelInsertion insertion = new DoctorModelInsertion();
                insertion.setDocFname(objectFName);
                insertion.setDocLname(objectLName);
                insertion.setDocMname(objectMName);
                insertion.setSpecDesc(objectDescription);
                insertion.setDoctorCode(objectCode);
                insertion.setHospitalCode(objectHospitalCode);
                insertion.setHospitalName(objectHospitalName);
                arrayList.add(insertion);


            } while (cursor.moveToNext());
        }


        cursor.close();


///2nd query

        sql = "";
        sql += "SELECT * FROM " + doctorTable;
        sql += " WHERE (" + specDesc + " LIKE '%" + searchTerm + "%' OR " + docLname + " LIKE '%" + searchTerm + "%' " +
                "OR " + docFname + " LIKE '%" + searchTerm + "%' ) ";
        sql += " AND " + hospitalCode + " = '" + SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context) + "'";
        if (!searchTerm.equals("")) {
            sql += "AND " + "UPPER(" + docLname + ") <> '" + searchTerm + "'";
        }
        sql += " AND " + isSelectedAsMain + " = \'false\' ";
        sql += " AND " + docLname + " IS NOT NULL ";
        sql += " AND " + docLname + " != ''";
        sql += " ORDER BY " + isSelected + " DESC";
        sql += ", " + docLname + " ASC";
        sql += " LIMIT 50";



        Log.e(TAG, "objectName: " + sql);
        // execute the query
        cursor = db.rawQuery(sql, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                String objectLName = cursor.getString(cursor.getColumnIndex(docLname));
                String objectMName = cursor.getString(cursor.getColumnIndex(docMname));
                String objectFName = cursor.getString(cursor.getColumnIndex(docFname));
                String objectDescription = cursor.getString(cursor.getColumnIndex(specDesc));
                String objectCode = cursor.getString(cursor.getColumnIndex(doctorCode));
                String objectHospitalCode = cursor.getString(cursor.getColumnIndex(hospitalCode));
                String objectHospitalName = cursor.getString(cursor.getColumnIndex(hospitalName));
                //Log.e(TAG, "objectName: " + objectLName);

                DoctorModelInsertion insertion = new DoctorModelInsertion();
                insertion.setDocFname(objectFName);
                insertion.setDocLname(objectLName);
                insertion.setDocMname(objectMName);
                insertion.setSpecDesc(objectDescription);
                insertion.setDoctorCode(objectCode);
                insertion.setHospitalCode(objectHospitalCode);
                insertion.setHospitalName(objectHospitalName);
                arrayList.add(insertion);


            } while (cursor.moveToNext());
        }
//        Log.e(TAG, "DocHospList: " + arrayList.size());

        cursor.close();


        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;

    }

    public ArrayList<DoctorModelInsertion> retrieveUnfilteredDoctorList(String searchTerm, String isFromMaternityOrigin) {
        ArrayList<DoctorModelInsertion> arrayList = new ArrayList<>();

        // select query
        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor;

        sql += "SELECT * FROM " + doctorTable;
        sql += " WHERE " + "UPPER(" + docLname + ") = '" + searchTerm + "'";
        sql += " AND " + hospitalCode + " = '" + SharedPref.getStringValue(SharedPref.USER, SharedPref.UNFILTERED_HOSPITAL_CODE, context) + "'";
        sql += " AND " + isSelectedAsMain + " = \'false\' ";
        sql += " AND " + docLname + " IS NOT NULL ";
        sql += " AND " + docLname + " != ''";
        sql += " ORDER BY " + docLname + " ASC";
        sql += " LIMIT 50";

        Log.e(TAG, "objectName: " + sql);
        cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                String objectLName = cursor.getString(cursor.getColumnIndex(docLname));
                String objectMName = cursor.getString(cursor.getColumnIndex(docMname));
                String objectFName = cursor.getString(cursor.getColumnIndex(docFname));
                String objectDescription = cursor.getString(cursor.getColumnIndex(specDesc));
                String objectCode = cursor.getString(cursor.getColumnIndex(doctorCode));
                String objectHospitalCode = cursor.getString(cursor.getColumnIndex(hospitalCode));
                String objectHospitalName = cursor.getString(cursor.getColumnIndex(hospitalName));

                //Log.e(TAG, "objectName: " + objectLName);

                DoctorModelInsertion insertion = new DoctorModelInsertion();
                insertion.setDocFname(objectFName);
                insertion.setDocLname(objectLName);
                insertion.setDocMname(objectMName);
                insertion.setSpecDesc(objectDescription);
                insertion.setDoctorCode(objectCode);
                insertion.setHospitalCode(objectHospitalCode);
                insertion.setHospitalName(objectHospitalName);


                arrayList.add(insertion);


            } while (cursor.moveToNext());
        }


        cursor.close();


///2nd query

        sql = "";
        sql += "SELECT * FROM " + doctorTable;
        sql += " WHERE (" + specDesc + " LIKE '%" + searchTerm + "%' OR " + docLname + " LIKE '%" + searchTerm + "%' " +
                "OR " + docFname + " LIKE '%" + searchTerm + "%' ) ";
        sql += " AND " + hospitalCode + " = '" + SharedPref.getStringValue(SharedPref.USER, SharedPref.UNFILTERED_HOSPITAL_CODE, context) + "'";
        if (!searchTerm.equals("")) {
            sql += "AND " + "UPPER(" + docLname + ") <> '" + searchTerm + "'";
        }
        sql += " AND " + isSelectedAsMain + " = \'false\' ";
        sql += " AND " + docLname + " IS NOT NULL ";
        sql += " AND " + docLname + " != ''";
        sql += " ORDER BY " + docLname + " ASC";
        sql += " LIMIT 50";

        Log.e(TAG, "objectName: " + sql);
        // execute the query
        cursor = db.rawQuery(sql, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                String objectLName = cursor.getString(cursor.getColumnIndex(docLname));
                String objectMName = cursor.getString(cursor.getColumnIndex(docMname));
                String objectFName = cursor.getString(cursor.getColumnIndex(docFname));
                String objectDescription = cursor.getString(cursor.getColumnIndex(specDesc));
                String objectCode = cursor.getString(cursor.getColumnIndex(doctorCode));
                String objectHospitalCode = cursor.getString(cursor.getColumnIndex(hospitalCode));
                String objectHospitalName = cursor.getString(cursor.getColumnIndex(hospitalName));
                //Log.e(TAG, "objectName: " + objectLName);

                DoctorModelInsertion insertion = new DoctorModelInsertion();
                insertion.setDocFname(objectFName);
                insertion.setDocLname(objectLName);
                insertion.setDocMname(objectMName);
                insertion.setSpecDesc(objectDescription);
                insertion.setDoctorCode(objectCode);
                insertion.setHospitalCode(objectHospitalCode);
                insertion.setHospitalName(objectHospitalName);
                arrayList.add(insertion);


            } while (cursor.moveToNext());
        }
//        Log.e(TAG, "DocHospList: " + arrayList.size());


        cursor.close();


        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;

    }



    public ArrayList<DoctorModelInsertion> retrieveDoctorListWithFilter(String searchTerm, String isFromMaternityOrigin,
                                                                        String getDoctorCode) {
        ArrayList<DoctorModelInsertion> arrayList = new ArrayList<>();

        // select query
        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor;

        sql += "SELECT * FROM " + doctorTable;
        sql += " WHERE " + "UPPER(" + docLname + ") = '" + searchTerm + "'";
        if (isFromMaternityOrigin.equals(Constant.MATERNITY)) {
            sql += " AND " + specDesc + " LIKE '%OB-GY%' ";
        }
        sql += " AND " + diagCode + " != '" + getDoctorCode + "' ";
        sql += " ORDER BY " + docLname + " ASC";

        //Log.e(TAG, "objectName: " + sql);
        cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {

                String objectLName = cursor.getString(cursor.getColumnIndex(docLname));
                String objectMName = cursor.getString(cursor.getColumnIndex(docMname));
                String objectFName = cursor.getString(cursor.getColumnIndex(docFname));
                String objectDescription = cursor.getString(cursor.getColumnIndex(specDesc));
                String objectCode = cursor.getString(cursor.getColumnIndex(doctorCode));
                String objectHospitalCode = cursor.getString(cursor.getColumnIndex(hospitalCode));
                String objectHospitalName = cursor.getString(cursor.getColumnIndex(hospitalName));
                //Log.e(TAG, "objectName: " + objectLName);

                DoctorModelInsertion insertion = new DoctorModelInsertion();
                insertion.setDocFname(objectFName);
                insertion.setDocLname(objectLName);
                insertion.setDocMname(objectMName);
                insertion.setSpecDesc(objectDescription);
                insertion.setDoctorCode(objectCode);
                insertion.setHospitalCode(objectHospitalCode);
                insertion.setHospitalName(objectHospitalName);
                arrayList.add(insertion);


            } while (cursor.moveToNext());
        }


        cursor.close();


///2nd query

        sql = "";
        sql += "SELECT * FROM " + doctorTable;
        sql += " WHERE (" + specDesc + " LIKE '%" + searchTerm + "%' OR " + docLname + " LIKE '%" + searchTerm + "%' " +
                "OR " + docFname + " LIKE '%" + searchTerm + "%' ) ";
        if (isFromMaternityOrigin.equals(Constant.MATERNITY)) {
            sql += " AND " + specDesc + " LIKE '%OB-GY%' ";
        }
        if (!searchTerm.equals("")) {
            sql += "AND " + "UPPER(" + docLname + ") <> '" + searchTerm + "'";
        }
        sql += " ORDER BY " + docLname + " ASC";
        //Log.e(TAG, "objectName: " + sql);
        // execute the query
        cursor = db.rawQuery(sql, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                String objectLName = cursor.getString(cursor.getColumnIndex(docLname));
                String objectMName = cursor.getString(cursor.getColumnIndex(docMname));
                String objectFName = cursor.getString(cursor.getColumnIndex(docFname));
                String objectDescription = cursor.getString(cursor.getColumnIndex(specDesc));
                String objectCode = cursor.getString(cursor.getColumnIndex(doctorCode));
                String objectHospitalCode = cursor.getString(cursor.getColumnIndex(hospitalCode));
                String objectHospitalName = cursor.getString(cursor.getColumnIndex(hospitalName));
                //Log.e(TAG, "objectName: " + objectLName);

                DoctorModelInsertion insertion = new DoctorModelInsertion();
                insertion.setDocFname(objectFName);
                insertion.setDocLname(objectLName);
                insertion.setDocMname(objectMName);
                insertion.setSpecDesc(objectDescription);
                insertion.setDoctorCode(objectCode);
                insertion.setHospitalCode(objectHospitalCode);
                insertion.setHospitalName(objectHospitalName);
                arrayList.add(insertion);


            } while (cursor.moveToNext());
        }


        cursor.close();


        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;

    }

    public ArrayList<DoctorModelMultipleSelection> retrieveDoctorListMultiSelect(String searchTerm) {
        ArrayList<DoctorModelMultipleSelection> arrayList = new ArrayList<>();

        // select query
        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor;

        sql += "SELECT * FROM " + doctorTable;
        sql += " WHERE " + "UPPER(" + docLname + ") = '" + searchTerm + "'";
        sql += " AND " + isSelectedAsMain + " = \'false\' ";
        sql += " ORDER BY " + docLname + " ASC";

        //Log.e(TAG, "objectName: " + sql);
        cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {

                String objectLName = cursor.getString(cursor.getColumnIndex(docLname));
                String objectMName = cursor.getString(cursor.getColumnIndex(docMname));
                String objectFName = cursor.getString(cursor.getColumnIndex(docFname));
                String objectDescription = cursor.getString(cursor.getColumnIndex(specDesc));
                String objectCode = cursor.getString(cursor.getColumnIndex(doctorCode));
                String objectHospitalCode = cursor.getString(cursor.getColumnIndex(hospitalCode));
                String objectHospitalName = cursor.getString(cursor.getColumnIndex(hospitalName));
                String objectIsSelected = cursor.getString(cursor.getColumnIndex(isSelected));
                //Log.e(TAG, "objectName: " + objectLName);

                DoctorModelMultipleSelection insertion = new DoctorModelMultipleSelection();
                insertion.setDocFname(objectFName);
                insertion.setDocLname(objectLName);
                insertion.setDocMname(objectMName);
                insertion.setSpecDesc(objectDescription);
                insertion.setDoctorCode(objectCode);
                insertion.setHospitalCode(objectHospitalCode);
                insertion.setHospitalName(objectHospitalName);
                insertion.setIsSelected(objectIsSelected);
                arrayList.add(insertion);


            } while (cursor.moveToNext());
        }


        cursor.close();


///2nd query

        sql = "";
        sql += "SELECT * FROM " + doctorTable;
        sql += " WHERE (" + specDesc + " LIKE '%" + searchTerm + "%' OR " + docLname + " LIKE '%" + searchTerm + "%' " +
                "OR " + docFname + " LIKE '%" + searchTerm + "%' ) ";

        if (!searchTerm.equals("")) {
            sql += "AND " + "UPPER(" + docLname + ") <> '" + searchTerm + "'";
        }
        sql += " AND " + isSelectedAsMain + " = \'false\' ";

        sql += " ORDER BY " + docLname + " ASC";
        //Log.e(TAG, "objectName: " + sql);
        // execute the query
        cursor = db.rawQuery(sql, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                String objectLName = cursor.getString(cursor.getColumnIndex(docLname));
                String objectMName = cursor.getString(cursor.getColumnIndex(docMname));
                String objectFName = cursor.getString(cursor.getColumnIndex(docFname));
                String objectDescription = cursor.getString(cursor.getColumnIndex(specDesc));
                String objectCode = cursor.getString(cursor.getColumnIndex(doctorCode));
                String objectHospitalCode = cursor.getString(cursor.getColumnIndex(hospitalCode));
                String objectHospitalName = cursor.getString(cursor.getColumnIndex(hospitalName));
                String objectIsSelected = cursor.getString(cursor.getColumnIndex(isSelected));

                //Log.e(TAG, "objectName: " + objectLName);

                DoctorModelMultipleSelection insertion = new DoctorModelMultipleSelection();
                insertion.setDocFname(objectFName);
                insertion.setDocLname(objectLName);
                insertion.setDocMname(objectMName);
                insertion.setSpecDesc(objectDescription);
                insertion.setDoctorCode(objectCode);
                insertion.setHospitalCode(objectHospitalCode);
                insertion.setHospitalName(objectHospitalName);
                insertion.setIsSelected(objectIsSelected);
                arrayList.add(insertion);


            } while (cursor.moveToNext());
        }


        cursor.close();


        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;

    }

    public void deleteTableData() {

        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "DELETE FROM " + doctorTable;
        db.execSQL(sql);

        String sql2 = "DELETE FROM " + procedureTable;
        db.execSQL(sql2);

        String sql3 = "DELETE FROM " + diagnosisTable;
        db.execSQL(sql3);

        String sql4 = "DELETE FROM " + roomsTable;
        db.execSQL(sql4);

        String sql5 = "DELETE FROM " + hospitalTable;
        db.execSQL(sql5);

        String sql6 = "DELETE FROM " + roomCategoryTable;
        db.execSQL(sql6);

        db.close();

    }

    public void deletedoctorTableData() {
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "DELETE FROM " + doctorTable;
        db.execSQL(sql);
        db.close();

    }

    public void deleteprocedureTableData() {
        SQLiteDatabase db = this.getWritableDatabase();
        String sql2 = "DELETE FROM " + procedureTable;
        db.execSQL(sql2);
        db.close();

    }

    public void deletediagnosisTableData() {
        SQLiteDatabase db = this.getWritableDatabase();
        String sql3 = "DELETE FROM " + diagnosisTable;
        db.execSQL(sql3);
        db.close();

    }


    public boolean checkIfExists(String objectName) {

        boolean recordExists = false;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * " + " FROM " + doctorTable + " WHERE " + doctorCode + " = '" + objectName + "'", null);

        if (cursor != null) {

            if (cursor.getCount() > 0) {
                recordExists = true;
            }
        }

        cursor.close();
        db.close();

        return recordExists;
    }

    public void setALLDiagnosisToFalse() {


        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT * FROM " + diagnosisTable;
        sql += " WHERE " + isSelected + " = " + "\'true\'";
        sql += " OR " + isSelectedDiagOther + " = " + "\'true\'";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        int x = 0;

        if (cursor.moveToFirst()) {
            do {

                String objectProcCode = cursor.getString(cursor.getColumnIndex(diagCode));
                //Log.e(TAG, "objectName: " + objectProcCode);


                setDataDiagnosetoFALSE(objectProcCode);
                setDataDiagnoseOtherToFALSE(objectProcCode);
                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();


    }

    public void setALLDoctorsToFalse() {
        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT * FROM " + doctorTable;
        sql += " WHERE " + isSelected + " = " + "\'true\'";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        int x = 0;

        if (cursor.moveToFirst()) {
            do {

                String objectProcCode = cursor.getString(cursor.getColumnIndex(doctorCode));
                //Log.e(TAG, "objectName: " + objectProcCode);
                setDataDoctorToFalse(objectProcCode);

                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();


    }

    public String getDoctorName(String getdoctorCode) {
        String name = "";


        String sql = " SELECT * FROM " + doctorTable +
                " WHERE " + doctorCode + " = '" + getdoctorCode + "'";
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery(sql, null);

        String Lname = "";
        String Mname = "";
        String Fname = "";
        if (cursor.moveToFirst()) {


            do {
                Lname = cursor.getString(cursor.getColumnIndex(docLname));
                Mname = cursor.getString(cursor.getColumnIndex(docMname));
                Fname = cursor.getString(cursor.getColumnIndex(docFname));
                Log.d("HOSP-ID", Lname + " , " + Mname + " , " + Fname);

                name = Fname + " " + Lname;
            } while (cursor.moveToNext());

        }

        database.close();
        cursor.close();
        return name;
    }

    public String getDoctorSpec(String getdoctorCode) {
        String name = "";


        String sql = " SELECT * FROM " + doctorTable +
                " WHERE " + doctorCode + " = '" + getdoctorCode + "'";
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery(sql, null);

        String spec = "";

        if (cursor.moveToFirst()) {


            do {
                spec = cursor.getString(cursor.getColumnIndex(specDesc));
                Log.d("HOSP-ID", specDesc);


            } while (cursor.moveToNext());

        }

        database.close();
        cursor.close();
        return spec;
    }

    public String getDoctorHosp(String getdoctorCode) {
        String name = "";


        String sql = " SELECT * FROM " + doctorTable +
                " WHERE " + doctorCode + " = '" + getdoctorCode + "'";
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery(sql, null);

        String spec = "";

        if (cursor.moveToFirst()) {


            do {
                spec = cursor.getString(cursor.getColumnIndex(hospitalName));
                Log.d("HOSP-ID", hospitalName);


            } while (cursor.moveToNext());

        }

        database.close();
        cursor.close();
        return spec;
    }

    public boolean isDatabaseEmpty() {
        return retrieveDiagnose("").size() == 0;
    }

    public ArrayList<TestsAndProcedures> retrieveSelectedProcPrimary() {
        ArrayList<TestsAndProcedures> arrayList = new ArrayList<>();


        String sql = "";
        sql += "SELECT * FROM " + procedureTable + " WHERE " + isSelectedProcPrime + " = \'true\'";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {


                String objserviceClassCode = cursor.getString(cursor.getColumnIndex(costCenter));
                String objectprocedureCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                String objectprocedureDesc = cursor.getString(cursor.getColumnIndex(procedureDesc));
                String objectprocedureAmount = cursor.getString(cursor.getColumnIndex(procedureAmount));
                String objectid = cursor.getString(cursor.getColumnIndex(maceServiceType));
                //Log.e(TAG, "objectName: " + objectprocedureDesc);

                TestsAndProcedures insertion = new TestsAndProcedures();
                insertion.setProcedureAmount(objectprocedureAmount);
                insertion.setProcedureCode(objectprocedureCode);
                insertion.setProcedureDesc(objectprocedureDesc);
                insertion.setCostCenter(objserviceClassCode);
                insertion.setMaceServiceType(objectid);
                insertion.setOriginalPrice(objectprocedureAmount);

                arrayList.add(insertion);
                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;
    }

    public ArrayList<TestsAndProcedures> retrieveSelectedProcOther() {
        ArrayList<TestsAndProcedures> arrayList = new ArrayList<>();


        String sql = "";
        sql += "SELECT * FROM " + procedureTable + " WHERE " + isSelectedProcOther + " = \'true\'";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {


                String objserviceClassCode = cursor.getString(cursor.getColumnIndex(costCenter));
                String objectprocedureCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                String objectprocedureDesc = cursor.getString(cursor.getColumnIndex(procedureDesc));
                String objectprocedureAmount = cursor.getString(cursor.getColumnIndex(procedureAmount));
                String objectid = cursor.getString(cursor.getColumnIndex(maceServiceType));
                //Log.e(TAG, "objectName: " + objectprocedureDesc);

                TestsAndProcedures insertion = new TestsAndProcedures();
                insertion.setProcedureAmount(objectprocedureAmount);
                insertion.setProcedureCode(objectprocedureCode);
                insertion.setProcedureDesc(objectprocedureDesc);
                insertion.setCostCenter(objserviceClassCode);
                insertion.setMaceServiceType(objectid);
                insertion.setOriginalPrice(objectprocedureAmount);

                arrayList.add(insertion);
                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;
    }

    public void setPrimaryProcToFalse() {


        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT * FROM " + procedureTable;
        sql += " WHERE " + isSelectedProcPrime + " = " + "\'true\'";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        int x = 0;

        if (cursor.moveToFirst()) {
            do {

                String objectProcCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                //Log.e(TAG, "objectName: " + objectProcCode);


                setDataProcPrimarytoFALSE(objectProcCode);

                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();


    }

    public void setOtherProcToFalse() {


        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT * FROM " + procedureTable;
        sql += " WHERE " + isSelectedProcOther + " = " + "\'true\'";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        int x = 0;

        if (cursor.moveToFirst()) {
            do {

                String objectProcCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                //Log.e(TAG, "objectName: " + objectProcCode);


                setDataProcOthertoFALSE(objectProcCode);

                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();


    }

    public void setPrimaryProcToFalse2() {


        String sql = "";
        sql += "SELECT * FROM " + procedureTable;
        sql += " WHERE " + isSelected + " = " + "\'true\'";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        int x = 0;

        if (cursor.moveToFirst()) {
            do {

                String objectProcCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                //Log.e(TAG, "objectName: " + objectProcCode);


                setDataProceduretoFALSE(objectProcCode);

                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();


    }

    public ArrayList<ProceduresListLocal> retrieveBasicTest(String searchTerm) {
        ArrayList<ProceduresListLocal> arrayList = new ArrayList<>();
        Cursor cursor;
        SQLiteDatabase db = this.getWritableDatabase();


        String sql = "";
        sql += "SELECT * FROM " + basicTestTable;
        sql += " WHERE " + "UPPER(" + procedureDesc + ") = '" + searchTerm + "'";
//        sql += " ORDER BY " + procedureDesc + " ASC";
        //Log.e(TAG, "objectName: " + sql);


        // execute the query
        cursor = db.rawQuery(sql, null);


        if (cursor.moveToFirst()) {
            do {

                String objserviceClassCode = cursor.getString(cursor.getColumnIndex(serviceClassCode));
                String objectprocedureCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                String objectprocedureDesc = cursor.getString(cursor.getColumnIndex(procedureDesc));
                String objectprocedureAmount = cursor.getString(cursor.getColumnIndex(procedureAmount));
                String objectid = cursor.getString(cursor.getColumnIndex(id));
                //Log.e(TAG, "objectName: " + objectprocedureDesc);

                ProceduresListLocal insertion = new ProceduresListLocal();
                insertion.setProcedureAmount(objectprocedureAmount);
                insertion.setProcedureCode(objectprocedureCode);
                insertion.setProcedureDesc(objectprocedureDesc);
                insertion.setServiceClassCode(objserviceClassCode);
                insertion.setId(objectid);

                arrayList.add(insertion);

            } while (cursor.moveToNext());
        }

        cursor.close();
        sql = "";
        sql += "SELECT * FROM " + basicTestTable;
        sql += " WHERE " + "UPPER(" + procedureDesc + ") LIKE '%" + searchTerm + "%' ";

        if (!searchTerm.equals("")) {
            sql += "AND " + "UPPER(" + procedureDesc + ") <> '" + searchTerm + "'";
        }

//        sql += " ORDER BY " + procedureDesc + " ASC";
        //Log.e(TAG, "objectName: " + sql);


        // execute the query
        cursor = db.rawQuery(sql, null);


        if (cursor.moveToFirst()) {
            do {

                String objserviceClassCode = cursor.getString(cursor.getColumnIndex(serviceClassCode));
                String objectprocedureCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                String objectprocedureDesc = cursor.getString(cursor.getColumnIndex(procedureDesc));
                String objectprocedureAmount = cursor.getString(cursor.getColumnIndex(procedureAmount));
                String objectid = cursor.getString(cursor.getColumnIndex(id));
                //Log.e(TAG, "objectName: " + objectprocedureDesc);

                ProceduresListLocal insertion = new ProceduresListLocal();
                insertion.setProcedureAmount(objectprocedureAmount);
                insertion.setProcedureCode(objectprocedureCode);
                insertion.setProcedureDesc(objectprocedureDesc);
                insertion.setServiceClassCode(objserviceClassCode);
                insertion.setId(objectid);

                arrayList.add(insertion);

            } while (cursor.moveToNext());
        }
        db.close();
        cursor.close();


        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;

    }

    public String getDiagDesc(String[] diagnosisList) {
        String name = "";


        String sql = " SELECT * FROM " + diagnosisTable +
                " WHERE " + diagCode + " LIKE '%" + diagnosisList[0] + "%'";
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery(sql, null);
        Log.d("DATA_DIAG", sql);
        String spec = "";

        if (cursor.moveToFirst()) {


            do {
                spec = cursor.getString(cursor.getColumnIndex(diagDesc));
                Log.d("DATA_DIAG", spec);


            } while (cursor.moveToNext());

        }

        database.close();
        cursor.close();
        return spec;

    }

    public String getDiagDesc(String diagnosisList) {
        String name = "";


        String sql = " SELECT * FROM " + diagnosisTable +
                " WHERE " + diagCode + " LIKE '%" + diagnosisList + "%'";

        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery(sql, null);
        Log.d("DATA_DIAG", sql);
        String spec = "";

        if (cursor.moveToFirst()) {


            do {
                spec = cursor.getString(cursor.getColumnIndex(diagDesc));
                Log.d("DATA_DIAG", spec);


            } while (cursor.moveToNext());

        }

        database.close();
        cursor.close();
        return spec;

    }

    public String getCostCenter(String procCode) {
        String name = "";


        String sql = " SELECT " + costCenter + " FROM " + procedureTable +
                " WHERE " + procedureCode + " = '" + procCode + "'";
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery(sql, null);
        Log.d("CostCenter", sql);
        String spec = "";

        if (cursor.moveToFirst()) {


            do {
                spec = cursor.getString(cursor.getColumnIndex(costCenter));
                Log.d("DATA_DIAG", spec);


            } while (cursor.moveToNext());

        }
        database.close();
        cursor.close();
        return spec;

    }

    public ArrayList<TestsAndProcedures> retrieveProcedureFromList(String[] proceduresList) {
        ArrayList<TestsAndProcedures> arrayList = new ArrayList<>();

        Cursor cursor;
        SQLiteDatabase db = this.getWritableDatabase();


        String sql = "";
        sql += "SELECT * FROM " + procedureTable;

        String data = "";


        if (proceduresList.length != 0) {
            data += " WHERE ( ";
            for (int x = 0; x < proceduresList.length; x++) {
                data += procedureCode + " = '" + proceduresList[x] + "' OR ";
            }

            data = data.substring(0, data.length() - 3);
            data += " ) ";
        } else {
            data += " WHERE ";
            data += procedureCode + " = " + "'NONE'";
        }

        sql += data;
//        sql += " ORDER BY " + procedureDesc + " ASC";
        Log.e("LIST_PROC", sql);

        // execute the query
        cursor = db.rawQuery(sql, null);


        if (cursor.moveToFirst()) {
            do {


                String objserviceClassCode = cursor.getString(cursor.getColumnIndex(costCenter));
                String objectprocedureCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                String objectprocedureDesc = cursor.getString(cursor.getColumnIndex(procedureDesc));
                String objectprocedureAmount = cursor.getString(cursor.getColumnIndex(procedureAmount));
                String objectid = cursor.getString(cursor.getColumnIndex(maceServiceType));
                //Log.e(TAG, "objectName: " + objectprocedureDesc);

                TestsAndProcedures insertion = new TestsAndProcedures();
                insertion.setProcedureAmount(objectprocedureAmount);
                insertion.setProcedureCode(objectprocedureCode);
                insertion.setProcedureDesc(objectprocedureDesc);
                insertion.setCostCenter(objserviceClassCode);
                insertion.setMaceServiceType(objectid);

                arrayList.add(insertion);

            } while (cursor.moveToNext());
        }

        db.close();
        Log.e("LIST_PROC", arrayList.toString());
        cursor.close();
        return arrayList;
    }

    public String getProcedureDesc(String s) {

        String name = "";


        String sql = " SELECT * FROM " + procedureTable +
                " WHERE " + procedureCode + " LIKE '%" + s + "%'";
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery(sql, null);
        Log.d("DATA_DIAG", sql);
        String spec = "";

        if (cursor.moveToFirst()) {


            do {
                spec = cursor.getString(cursor.getColumnIndex(procedureDesc));
                Log.d("DATA_DIAG", spec);


            } while (cursor.moveToNext());

        }

        database.close();
        cursor.close();
        return spec;

    }

    public String getProcedureServiceClassCode(String s) {

        String name = "";


        String sql = " SELECT * FROM " + procedureTable +
                " WHERE " + procedureCode + " LIKE '%" + s + "%'";
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery(sql, null);
        Log.d("DATA_DIAG", sql);
        String spec = "";

        if (cursor.moveToFirst()) {


            do {
                spec = cursor.getString(cursor.getColumnIndex(serviceClassCode));
                Log.d("DATA_DIAG", spec);


            } while (cursor.moveToNext());

        }

        database.close();
        cursor.close();
        return spec;

    }

    public boolean getDataInpatientDoctorFromTrue(String ID) {


        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT " + isSelected + " FROM " + doctorTable;
        sql += " WHERE " + doctorCode + "=" + "\'" + ID + "\'";
        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();
        // execute the query
        Cursor cursor = db.rawQuery(sql, null);
        int x = 0;
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                objectIsTrue = cursor.getString(cursor.getColumnIndex(isSelected));
//                Log.e(TAG, "objectName: " + objectIsTrue);
                x++;
            } while (cursor.moveToNext());
        }
        db.close();
        cursor.close();
        if (objectIsTrue.equals("false")) {
            return false;
        } else return true;


    }

    public boolean getDataDoctorFromTrue(String ID) {


        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT " + isSelected + " FROM " + doctorTable;
        sql += " WHERE " + doctorCode + "=" + "\'" + ID + "\'";
        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();


        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                objectIsTrue = cursor.getString(cursor.getColumnIndex(isSelected));

                //Log.e(TAG, "objectName: " + objectIsTrue);

                x++;


            } while (cursor.moveToNext());
        }

        db.close();
        cursor.close();
        if (objectIsTrue.equals("false")) {
            return false;
        } else return true;


    }

    public boolean getDataOtherServicesFromTrue(String ID) {


        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT " + isSelected + " FROM " + otherServiceTable;
        sql += " WHERE " + id + "=" + "\'" + ID + "\'";
        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();


        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                objectIsTrue = cursor.getString(cursor.getColumnIndex(isSelected));

                //Log.e(TAG, "objectName: " + objectIsTrue);

                x++;


            } while (cursor.moveToNext());
        }

        db.close();
        cursor.close();
        if (objectIsTrue.equals("false")) {
            return false;
        } else return true;


    }

    public void setDataDoctorToTrue(String ID) {

        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        sql = doctorCode + " = '" + ID + "'";
        ContentValues con = new ContentValues();
        con.put(isSelected, "true");
        db.update(doctorTable, con, sql, null);

        db.close();
        Log.d("ID", ID);
    }

    public void setDataDoctorToFalse(String ID) {


        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        sql = doctorCode + " = '" + ID + "'";
        ContentValues con = new ContentValues();
        con.put(isSelected, "false");
        db.update(doctorTable, con, sql, null);

        db.close();

        Log.d("ID", ID);
    }

    public void setInPatientMainDoctorSelected(String getDoctor) {

        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        sql = doctorCode + " = '" + getDoctor + "'";
        ContentValues con = new ContentValues();
        con.put(isSelectedAsMain, "true");
        db.update(doctorTable, con, sql, null);


        Log.d("ID", getDoctor);


        db.close();
    }

    public void setInPatientMainDoctorDeSelected(String getDoctor) {

        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        sql = doctorCode + " = '" + getDoctor + "'";
        ContentValues con = new ContentValues();
        con.put(isSelectedAsMain, "false");
        db.update(doctorTable, con, sql, null);


        Log.d("ID", getDoctor);

        db.close();
    }

    public void setDataOtherServicesToTrue(String ID) {

        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        sql = id + " = '" + ID + "'";
        ContentValues con = new ContentValues();
        con.put(isSelected, "true");
        db.update(otherServiceTable, con, sql, null);

        db.close();
        Log.d("ID", ID);
    }

    public void setDataOtherServicesToFalse(String ID) {


        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        sql = id + " = '" + ID + "'";
        ContentValues con = new ContentValues();
        con.put(isSelected, "false");
        db.update(otherServiceTable, con, sql, null);

        db.close();
        Log.d("ID", ID);
    }

    public ArrayList<Services> retrieveSelectedOtherServices() {
        ArrayList<Services> arrayList = new ArrayList<>();


        String sql = "";
        sql += "SELECT * FROM " + otherServiceTable + " WHERE " + isSelected + " = \'true\'";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {


                Services insertion = new Services();
                insertion.setIsSelected(getCursorData(cursor, isSelected));
                insertion.setId(getCursorData(cursor, id));
                insertion.setCreatedBy(getCursorData(cursor, createdBy));
                insertion.setCreatedOn(getCursorData(cursor, createdOn));
                insertion.setLastUpdateBy(getCursorData(cursor, lastUpdateBy));
                insertion.setServiceCode(getCursorData(cursor, serviceCode));
                insertion.setServiceDesc(getCursorData(cursor, serviceDesc));
                insertion.setLastUpdateOn(getCursorData(cursor, lastUpdateOn));

                arrayList.add(insertion);
                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;

    }

    public void dropTableOtherServices() {

        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "DELETE FROM " + otherServiceTable;

        db.execSQL(sql);
        db.close();
    }

    public Rooms getRoomPlan(String code) {
        Rooms getRoom = new Rooms();


        String sql = " SELECT * FROM " + roomsTable +
                " WHERE " + planCode + " = '" + code + "'";
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery(sql, null);

        String spec = "";

        if (cursor.moveToFirst()) {


            do {
                getRoom = getRoomData(cursor);
            } while (cursor.moveToNext());

        }
        database.close();
        cursor.close();
        return getRoom;
    }

    private Rooms getRoomData(Cursor cursor) {

        Rooms rooms = new Rooms();

        rooms.setClassCategory(getCursorData(cursor, classCategory));
        rooms.setMonthlyPremium(getCursorData(cursor, monthlyPremium));
        rooms.setUpdatedDate(getCursorData(cursor, updatedDate));
        rooms.setPlanCode(getCursorData(cursor, planCode));
        rooms.setAnnualPremium(getCursorData(cursor, annualPremium));
        rooms.setGrpType(getCursorData(cursor, grpType));
        rooms.setDdl(getCursorData(cursor, ddl));
        rooms.setPlanDesc(getCursorData(cursor, planDesc));
        rooms.setSemiAnnualPremium(getCursorData(cursor, semiAnnualPremium));
        rooms.setWHosp(getCursorData(cursor, wHosp));
        rooms.setQuarterlyPremium(getCursorData(cursor, quarterlyPremium));
        rooms.setUpdatedBy(getCursorData(cursor, updatedBy));

        return rooms;
    }

    public Collection<? extends Rooms> retrieveRooms(String s) {
        ArrayList<Rooms> getRooms = new ArrayList<>();
        String searchItem = s;
        Cursor cursor;
        SQLiteDatabase db = this.getWritableDatabase();

//
        String sql = "";
//        sql += "SELECT * FROM " + roomsTable;
//        sql += " WHERE " + "UPPER(" + planDesc + ") = '" + searchItem + "'";
//        sql += " OR " + "UPPER(" + classCategory + ") = '" + searchItem + "'";
//        //Log.e(TAG, "objectName: " + sql);
//
//
//        // execute the query
//        cursor = db.rawQuery(sql, null);
//
//
//        if (cursor.moveToFirst()) {
//            do {
//                getRooms.add(getRoomData(cursor));
//            } while (cursor.moveToNext());
//        }
//
//        cursor.close();
        sql = "";
        sql += "SELECT * FROM " + roomsTable;
        sql += " WHERE " + "UPPER(" + planDesc + ") LIKE '%" + searchItem + "%' ";
        sql += " OR " + "UPPER(" + classCategory + ") LIKE '%" + searchItem + "%' ";

//        if (!searchItem.equals("")) {
//            sql += "AND (" + "UPPER(" + planDesc + ") <> '" + searchItem + "'";
//            sql += " OR " + "UPPER(" + classCategory + ") = '" + searchItem + "' )";
//        }

        sql += " ORDER BY " + planDesc + " ASC";
        //Log.e(TAG, "objectName: " + sql);


        // execute the query
        cursor = db.rawQuery(sql, null);


        if (cursor.moveToFirst()) {
            do {
                getRooms.add(getRoomData(cursor));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();


        return getRooms;
    }

    public Collection<? extends Services> retrieveOtherServices(String s) {

        ArrayList<Services> arrayList = new ArrayList<>();
        Cursor cursor;
        SQLiteDatabase db = this.getWritableDatabase();
        String searchTerm = s;

        String sql = "";
        sql += "SELECT * FROM " + otherServiceTable;
        sql += " WHERE " + "UPPER(" + serviceDesc + ") = '" + searchTerm + "'";
        sql += " ORDER BY " + serviceDesc + " ASC";
        //Log.e(TAG, "objectName: " + sql);


        // execute the query
        cursor = db.rawQuery(sql, null);


        if (cursor.moveToFirst()) {
            do {

                arrayList.add(getOtherServices(cursor));


            } while (cursor.moveToNext());
        }

        cursor.close();
        sql = "";
        sql += "SELECT * FROM " + otherServiceTable;
        sql += " WHERE " + "UPPER(" + serviceDesc + ") LIKE '%" + searchTerm + "%' ";

        if (!searchTerm.equals("")) {
            sql += "AND " + "UPPER(" + serviceDesc + ") <> '" + searchTerm + "'";
        }

        sql += " ORDER BY " + serviceDesc + " ASC";
        //Log.e(TAG, "objectName: " + sql);


        // execute the query
        cursor = db.rawQuery(sql, null);


        if (cursor.moveToFirst()) {
            do {

                arrayList.add(getOtherServices(cursor));

            } while (cursor.moveToNext());
        }

        cursor.close();


        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;

    }

    private Services getOtherServices(Cursor cursor) {

        Services services = new Services();
        services.setIsSelected(getCursorData(cursor, isSelected));
        services.setLastUpdateBy(getCursorData(cursor, lastUpdateBy));
        services.setLastUpdateOn(getCursorData(cursor, lastUpdateOn));
        services.setServiceDesc(getCursorData(cursor, serviceDesc));
        services.setCreatedBy(getCursorData(cursor, createdBy));
        services.setId(getCursorData(cursor, id));
        services.setCreatedOn(getCursorData(cursor, createdOn));
        services.setServiceCode(getCursorData(cursor, serviceCode));
        return services;
    }

    private Hospital getUnfilteredHospital(Cursor cursor) {
        Hospital hospital = new Hospital();
        hospital.setHospitalCode(getCursorData(cursor, HOSPITAL_CODE));
        hospital.setHospitalName(getCursorData(cursor, HOSPITAL_NAME));
        hospital.setStreetAddress(getCursorData(cursor, STREET));
        hospital.setHospitalAbbre(getCursorData(cursor, HOSPITAL_ABBRE));
        hospital.setCity(getCursorData(cursor, CITY_NAME));
        hospital.setProvince(getCursorData(cursor, PROVINCE_NAME));
        hospital.setRegion(getCursorData(cursor, REGION_NAME));
        hospital.setRegionDesc(getCursorData(cursor, REGION_DESC));
        hospital.setPhoneNo(getCursorData(cursor, PHONE_NO));
        hospital.setCclass(getCursorData(cursor, CLASS));
        hospital.setCategory(getCursorData(cursor, CATEGORY));
        hospital.setEntryType(getCursorData(cursor, ENTRY_TYPE));
        hospital.setIsAccredited(getCursorData(cursor, ISACCREDITED));
        return hospital;
    }


    public Collection<? extends Hospital> retrieveUnfilterdHosp(String s) {
        ArrayList<Hospital> arrayList = new ArrayList<>();
        Cursor cursor;
        SQLiteDatabase db = this.getWritableDatabase();

        String sql = "";
        sql += "SELECT * FROM " + hospitalTable;
        sql += " WHERE " + "UPPER(" + HOSPITAL_NAME + ") LIKE '%" + s + "%' ";
        sql += " AND " + HOSPITAL_NAME + " NOT  LIKE '" + medicard_one + "%' ";
        sql += " AND " + HOSPITAL_NAME + " NOT  LIKE '" + medicard_two + "%' ";
        sql += " AND " + HOSPITAL_NAME + " IS NOT NULL ";
        sql += " AND " + HOSPITAL_NAME + " != ''";
        sql += " ORDER BY " + HOSPITAL_NAME + " ASC";

        cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                arrayList.add(getUnfilteredHospital(cursor));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return arrayList;
    }

    public Collection<? extends Hospital> retrieveUnfilterdHospMedicardOnly(String search) {
        ArrayList<Hospital> arrayList = new ArrayList<>();
        Cursor cursor;
        SQLiteDatabase db = this.getWritableDatabase();

        String sql = "";
        sql += "SELECT * FROM " + hospitalTable;
        sql += " WHERE " + HOSPITAL_NAME + " LIKE  '%" + search + "%' ";
        sql += " AND (" + HOSPITAL_NAME + "  LIKE '%" + medicard_one + "%' ";
        sql += " OR " + HOSPITAL_NAME + "  LIKE '%" + medicard_two + "%' )";
        sql += " AND " + HOSPITAL_NAME + " NOT LIKE '%" + referral_desk + "%' ";
        sql += " AND " + HOSPITAL_NAME + " IS NOT NULL ";
        sql += " AND " + HOSPITAL_NAME + " != ''";
        sql += " ORDER BY " + HOSPITAL_NAME + " ASC";

        cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                arrayList.add(getUnfilteredHospital(cursor));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return arrayList;
    }

    public void updateBasicTestPrice(Context context, ArrayList<BasicTests> arrayList) {

//        String sql = "";
//        SQLiteDatabase db = this.getWritableDatabase();
//        sql = procedureCode + " = '" + ID + "'";
//        ContentValues con = new ContentValues();
//        con.put(isSelected, "true");
//        db.update(procedureTable, con, sql, null);
//
//
//        Log.d("ID", ID);
//
//
//        ContentValues newValues = new ContentValues();
//        newValues.put("YOUR_COLUMN", "newValue");
//        db.update("YOUR_TABLE", newValues, "id=6", null);
    }

    public void updateTestAndProcedurePrice(String ID,String PRICE) {

        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        sql = procedureCode + " = '" + ID + "'";
        ContentValues con = new ContentValues();
        con.put(procedureAmount, PRICE);
        db.update(procedureTable, con, sql, null);
        Log.d("ID", ID);

    }


    public ArrayList<TestsAndProcedures> retrieveBasicTestList() {
        ArrayList<TestsAndProcedures> arrayList = new ArrayList<>();
        Cursor cursor;
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "";
        sql += "SELECT * FROM " + procedureTable;
        sql += " WHERE " + maceServiceType + " = 'Basic Test'";
        sql += " ORDER BY " + maceServiceType + " ASC";
        cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {

                String objserviceClassCode = cursor.getString(cursor.getColumnIndex(costCenter));
                String objectprocedureCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                String objectprocedureDesc = cursor.getString(cursor.getColumnIndex(procedureDesc));
                String objectprocedureAmount = cursor.getString(cursor.getColumnIndex(procedureAmount));
                String objectid = cursor.getString(cursor.getColumnIndex(maceServiceType));
                //Log.e(TAG, "objectName: " + objectprocedureDesc);

                TestsAndProcedures insertion = new TestsAndProcedures();
                insertion.setProcedureAmount(objectprocedureAmount);
                insertion.setProcedureCode(objectprocedureCode);
                insertion.setProcedureDesc(objectprocedureDesc);
                insertion.setCostCenter(objserviceClassCode);
                insertion.setMaceServiceType(objectid);

                arrayList.add(insertion);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        Log.e(TAG, "Basic Test: " + arrayList.toString());
        return arrayList;
    }

    public void setAllDoctorDataInpatientToFalse() {
        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT * FROM " + doctorTable;
        sql += " WHERE " + isSelected + " = " + "\'true\'";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        int x = 0;

        if (cursor.moveToFirst()) {
            do {

                String objectProcCode = cursor.getString(cursor.getColumnIndex(doctorCode));
                //Log.e(TAG, "objectName: " + objectProcCode);
                setDataDoctorToFalse(objectProcCode);

                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();


    }


    public void setDataInPatientDiagnosistoTRUE(String ID) {
        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        sql = diagCode + " = '" + ID + "'";
        ContentValues con = new ContentValues();
        con.put(isSelected, "true");
        db.update(diagnosisTable, con, sql, null);
        Log.d("ID", diagCode);
    }

    public void setDataInPatientDiagnosistoFALSE(String ID) {
        String sql = "";

        SQLiteDatabase db = this.getWritableDatabase();
        sql = diagCode + " =  \'" + ID + "\'";
        ContentValues con = new ContentValues();
        con.put(isSelected, "false");
        db.update(diagnosisTable, con, sql, null);

        Log.d("ID", diagCode);
    }


    public ArrayList<DiagnosisList>  retrieveSelectedInpatientDiagnosis() {
        ArrayList<DiagnosisList> arrayList = new ArrayList<>();

        String sql = "";
        sql += "SELECT * FROM " + diagnosisTable + " WHERE " + isSelected + " = \'true\'";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                String objectdiagIsSelected = cursor.getString(cursor.getColumnIndex(isSelected));
                String objectdiagCode = cursor.getString(cursor.getColumnIndex(diagCode));
                String objecticd10 = cursor.getString(cursor.getColumnIndex(icd10Code));
                String objectDiagDesc = cursor.getString(cursor.getColumnIndex(diagDesc));

                //Log.e(TAG, "objectName: " + objectDiagDesc);

                DiagnosisList insertion = new DiagnosisList();
                insertion.setIsSelected(objectdiagIsSelected);
                insertion.setDiagCode(objectdiagCode);
                insertion.setDiagDesc(objectDiagDesc);
                insertion.setIcd10Code(objecticd10);


                arrayList.add(insertion);
                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;
    }

    public void setAllDiagnosisDataInpatientToFalse() {
        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT * FROM " + diagnosisTable;
        sql += " WHERE " + isSelected + " = " + "\'true\'";



        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        int x = 0;

        if (cursor.moveToFirst()) {
            do {

                String objectProcCode = cursor.getString(cursor.getColumnIndex(diagCode));
                //Log.e(TAG, "objectName: " + objectProcCode);
                setDataInPatientDiagnosistoFALSE(objectProcCode);

                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();




    }

    public boolean getDataInpatientDiagnosisFromTrue(String ID) {


        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT " + isSelected + " FROM " + diagnosisTable;
        sql += " WHERE " + diagCode + "=" + "\'" + ID + "\'";
        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();
        // execute the query
        Cursor cursor = db.rawQuery(sql, null);
        int x = 0;
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                objectIsTrue = cursor.getString(cursor.getColumnIndex(isSelected));
//                Log.e(TAG, "objectName: " + objectIsTrue);
                x++;
            } while (cursor.moveToNext());
        }
        db.close();
        cursor.close();
        if (objectIsTrue.equals("false")) {
            return false;
        } else return true;


    }

    public ArrayList<TestsAndProcedures> retrieveSelectedInpatientProcedure() {
        ArrayList<TestsAndProcedures> arrayList = new ArrayList<>();

        String sql = "";
        sql += "SELECT * FROM " + procedureTable + " WHERE " + isSelected + " = \'true\'";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);


        int x = 0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {


                String objserviceClassCode = cursor.getString(cursor.getColumnIndex(costCenter));
                String objectprocedureCode = cursor.getString(cursor.getColumnIndex(procedureCode));
                String objectprocedureDesc = cursor.getString(cursor.getColumnIndex(procedureDesc));
                String objectprocedureAmount = cursor.getString(cursor.getColumnIndex(procedureAmount));
                String objectprocedureIsSelected= cursor.getString(cursor.getColumnIndex(isSelected));
                String objectid = cursor.getString(cursor.getColumnIndex(maceServiceType));
                //Log.e(TAG, "objectName: " + objectprocedureDesc);

                TestsAndProcedures insertion = new TestsAndProcedures();
                insertion.setSelected(Boolean.parseBoolean(objectprocedureIsSelected));
                insertion.setProcedureAmount(objectprocedureAmount);
                insertion.setProcedureCode(objectprocedureCode);
                insertion.setProcedureDesc(objectprocedureDesc);
                insertion.setCostCenter(objserviceClassCode);
                insertion.setMaceServiceType(objectid);
                insertion.setOriginalPrice(objectprocedureAmount);

                arrayList.add(insertion);
                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        //Log.e(TAG, "objectName: " + arrayList.toString());
        return arrayList;

    }

    public void setDataInPatientProceduretoTRUE(String ID) {
        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        sql = procedureCode + " = '" + ID + "'";
        ContentValues con = new ContentValues();
        con.put(isSelected, "true");
        db.update(procedureTable, con, sql, null);
        Log.d("ID", procedureCode);
    }

    public void setDataInPatientProceduretoFALSE(String ID) {
        String sql = "";
        SQLiteDatabase db = this.getWritableDatabase();
        sql = procedureCode + " = '" + ID + "'";
        ContentValues con = new ContentValues();
        con.put(isSelected, "false");
        db.update(procedureTable, con, sql, null);
        Log.d("ID", procedureCode);
    }

    public boolean getDataInpatientProcedureFromTrue(String ID) {
        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT " + isSelected + " FROM " + procedureTable;
        sql += " WHERE " + procedureCode + "=" + "\'" + ID + "\'";
        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();
        // execute the query
        Cursor cursor = db.rawQuery(sql, null);
        int x = 0;
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                objectIsTrue = cursor.getString(cursor.getColumnIndex(isSelected));
//                Log.e(TAG, "objectName: " + objectIsTrue);
                x++;
            } while (cursor.moveToNext());
        }
        db.close();
        cursor.close();
        if (objectIsTrue.equals("false")) {
            return false;
        } else return true;


    }

    public void setAllProcedureDataInpatientToFalse() {
        String objectIsTrue = "";
        String sql = "";
        sql += "SELECT * FROM " + diagnosisTable;
        sql += " WHERE " + isSelected + " = " + "\'true\'";

        //Log.e(TAG, "objectName: " + sql);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        int x = 0;

        if (cursor.moveToFirst()) {
            do {

                String objectProcCode = cursor.getString(cursor.getColumnIndex(diagCode));
                //Log.e(TAG, "objectName: " + objectProcCode);
                setDataInPatientProceduretoFALSE(objectProcCode);

                x++;

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();





    }

    public void updateAllExcludedHospToTrue(ExclusionModel body) {

        ArrayList<String> data = body.getExclusions();

        for (int x = 0; x > data.size() ;x++){
            String sql = "";
            SQLiteDatabase db = this.getWritableDatabase();
            sql =  hospitalCode + " = '" +data.get(x)+ "'";
            ContentValues con = new ContentValues();
            con.put(ISEXCLUDED, "true");
            db.update(hospitalTable, con, sql, null);
        }

    }


}
