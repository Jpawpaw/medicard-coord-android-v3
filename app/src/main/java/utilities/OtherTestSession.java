package utilities;

import java.util.ArrayList;

import model.DiagnosisProcedures;
import model.TestsAndProcedures;

/**
 * Created by IPC on 9/15/2017.
 */

public class OtherTestSession {


    private static ArrayList<TestsAndProcedures> arrayListPrimary = new ArrayList<>();


    public void addStringPrimary(TestsAndProcedures s){
        arrayListPrimary.add(s);
    }

    public static ArrayList<TestsAndProcedures> getAllPrimary() {
        return arrayListPrimary;
    }


    private static ArrayList<TestsAndProcedures> arrayListOther = new ArrayList<>();


    public void addStringOther(TestsAndProcedures s){
        arrayListPrimary.add(s);
    }

    public static ArrayList<TestsAndProcedures> getAllOther() {
        return arrayListOther;
    }


    public static void releaseContent() {
        arrayListPrimary.clear();
        arrayListPrimary = new ArrayList<>();
        arrayListOther.clear();
        arrayListOther = new ArrayList();
    }
}
