package utilities;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by IPC on 11/24/2017.
 */

public class AttachmentSession {

    private static ArrayList<File> fileAttachments = new ArrayList<>();


    public static void addAttachemnt(File file){
        fileAttachments.add(file);
    }

    public static void removeAttachment(File file){
        fileAttachments.remove(file);
    }

    public static ArrayList<File> getAllAttachments(){
        return fileAttachments;
    }

    public static void releaseContent() {
        fileAttachments.clear();
        fileAttachments = new ArrayList<>();
    }
}
