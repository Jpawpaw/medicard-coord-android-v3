package utilities;

import android.app.TimePickerDialog;
import android.content.Context;
import android.util.Log;
import android.widget.TextView;
import android.widget.TimePicker;

import org.greenrobot.eventbus.EventBus;

import java.util.Calendar;

import coord.medicard.com.medicardcoordinator.Navigator.NavigatorActivity;
import fragment.Avail.AvailServices;
import fragment.InPatient.InPatientAdmitted;

/**
 * Created by mpx-pawpaw on 6/3/17.
 */

public class TimepickerSet {


    public static void getTime(Context context, final TextView tv_admitted_time, final String ORIGIN) {

        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR);
        int mins = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog;
        timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int min) {

                String am_pm = (hour < 12) ? "AM" : "PM";
                String minutes = setMinutes(min);
                int hour12Format = (hour < 12) ? hour : hour - 12;
                StringBuilder time = new StringBuilder();
                time.append(setHour(hour12Format))
                        .append(":")
                        .append(minutes)
                        .append(" " + am_pm);

                tv_admitted_time.setText(time.toString());

                if (ORIGIN.equals(Constant.FROM_AVIALED)) {
                    EventBus.getDefault().post(new InPatientAdmitted.MessageEvent(Constant.GETTIME, hour + ":" + min));
                } else if (ORIGIN.equals(Constant.FROM_IN_PATIENT_AVAIL)) {
                    EventBus.getDefault().post(new AvailServices.MessageEvent(Constant.GETTIME, hour + ":" + min));
                } else if (ORIGIN.equals(Constant.TIME_ADMITTED)) {
                    EventBus.getDefault().post(new InPatientAdmitted.MessageEvent(Constant.TIME_ADMITTED, hour + ":" + min));
                } else {

                    EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.GETTIME, hour + ":" + min));
                }

            }
        }, hour, mins, false);


        timePickerDialog.show();

    }

    private static String setMinutes(int min) {
        return min <= 9 ? "0" + min : min + "";
    }

    private static int setHour(int i) {
        return i == 0 ? 12 : i;
    }
}
