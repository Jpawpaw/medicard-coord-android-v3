package utilities;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;

import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsSpinner;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import java.io.File;

import coord.medicard.com.medicardcoordinator.R;
import de.hdodenhof.circleimageview.CircleImageView;
import fragment.Avail.AvailInterface;
import fragment.Avail.AvailServices;
import fragment.Avail.AvailServicesAPICall;
import model.BasicTestOrOtherTestReturn;
import model.BasicTestResponse;
import model.BasicTests;
import model.LOAReturn;
import model.SendLOAConsultationAndMaternityModel;
import utilities.pdfUtil.StatusType;

/**
 * Created by mpx-pawpaw on 10/21/16.
 */

public class AlertDialogCustom extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public String HTTP_401 = "HTTP_401";
    public String HTTP_409 = "HTTP_409";
    public String server_down = "failed to connect to";


    public String EMAIL_message = "Email format is invalid. Please Try again.";
    public String PASSWORD_CRED_message = "Password must have at least one capitalized letter , one number and minimum of eight letters";
    public String PASSWORD_MATCH_message = "Password and Re-typed password didn't match";

    public String CONGRATULATIONS_title = "Success";
    public String CONGRATULATIONS_message = "You have successfully created an account. Please login to begin.";


    public String ALREADY_message = "This account is already created or not existing. ";
    public String ALREADY_title = "Account Error";

    public String not_message = "Member ID and Birthdate did not match.\n Please Call 841-8080.";
    public String cannot_connect_message = "Cannot connect to server";
    public String no_memberID  = "No Member ID found";
    public String warning_on_date_and_member_ID_7 = "Member ID and Birthdate did not match. You only have 7 tries left.";
    public String warning_on_date_and_member_ID_6 = "Member ID and Birthdate did not match. You only have 6 tries left.";
    public String warning_on_date_and_member_ID_5 = "Member ID and Birthdate did not match. You only have 5 tries left.";
    public String warning_on_date_and_member_ID_4 = "Member ID and Birthdate did not match. You only have 4 tries left.";
    public String warning_on_date_and_member_ID_3 = "Member ID and Birthdate did not match. You only have 3 tries left.";
    public String warning_on_date_and_member_ID_2 = "Member ID and Birthdate did not match. You only have 2 tries left.";
    public String warning_on_date_and_member_ID_1 = "Member ID and Birthdate did not match. You only have 1 try left.";
    public String not_title = "Account Error";
    public String member_is_locked = "This member has been locked. Please ask for a valid ID and call 841-8080 for verification.";

    public String no_PASS_USER = "No User Account for entered username";

    public String invalid_doc = "Please use the pre-printed LOA form";
    public String INVALID_PASS_USER = "Username and Password didn't match.\n Please Call 841-8080.";
    public String INVALID_PASS_USERP_title = "Wrong Credentials";
    public String NO_Internet = "Please Try Again and check your internet connection";
    public String NO_Internet_title = "No Internet Connection";
    public String unknown = "Hold On";

    public String hold_it = "Hold It";
    public String unknown_msg = "Please try again.";
    public String CONGRATULATIONS = "CONGRATULATIONS";
    public String success = "Success";
    public String success_msg = "Image successfully uploaded";
    public String delete_msg = "Image successfully deleted";

    public String noDoc_title = "Oops!";
    public String noDoc_msg = "No doctors registered.";
    public String noDiagList = "Error on fetching Diagnosis List. Please try again.";
    public String noProcList = "Error on fetching Procedures List. Please try again.";
    public String already_added_tolist = "Item already added to list";
    public String fill_all_fields = "Please fill up the required fields.";
    public String not_eligible = "This member is locked. Please call 841-8080.";
    public String Select_diag_first = "Please select Primary Diagnosis first.";
    public String Select_hosp_first = "Please select Hospital first.";
    public String Select_diag_second = "Please select Other Diagnosis first.";


    TextView tv_message, tv_title,tv_header,tv_content;
    CircleImageView ci_error_image;
    Button btn_accept,bt_ok;
    Spinner spinner;
    String repeat = "";


    public void showMe(Context context, String title, String message, int errorImage) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alertshow);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        ci_error_image = (CircleImageView) dialog.findViewById(R.id.ci_error_image);
        tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        btn_accept = (Button) dialog.findViewById(R.id.btn_accept);
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        setDetails(context, message, title, 1, btn_accept);


        dialog.show();


        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.40);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = width;
        dialog.getWindow().setAttributes(lp);

    }
    public void showMeValidateReq(final String response,
                                  Context context, AvailInterface getCallback,
                                  final String ORIGIN) {

        final AvailInterface callback;
        callback = getCallback;
        Button btn_accept;
        Button btn_cancel;
        TextView tv_message;

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.confirmloa);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        spinner = (Spinner) dialog.findViewById(R.id.dropStatus);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context,
                R.array.repeat, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        btn_accept = (Button) dialog.findViewById(R.id.btn_accept);
        btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        tv_message.setText(response);
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
//                Log.d("BatchCode", BatchCode);
                if("CONSULTATION".equalsIgnoreCase(ORIGIN)){
                    AvailServicesAPICall.sendConsultationRequest(ORIGIN,callback,repeat);
                }else if("MATERNITY".equalsIgnoreCase(ORIGIN)){
                    AvailServicesAPICall.sendMaternityRequest(ORIGIN,callback,repeat);
                }else if(Constant.BASIC_TEST.equalsIgnoreCase(ORIGIN)){
                    AvailServicesAPICall.sendBasicTest(ORIGIN,callback,repeat);
                }else if(Constant.PROCEDURES.equalsIgnoreCase(ORIGIN)){
                    AvailServicesAPICall.sendProcedures(ORIGIN,callback,repeat);
                }else if(Constant.OTHER_TEST.equalsIgnoreCase(ORIGIN)){
                AvailServicesAPICall.sendOtherTestRequest(ORIGIN,callback,repeat);
                }

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();


        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.80);

        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.50);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = height;
        lp.width = width;
        dialog.getWindow().setAttributes(lp);

    }




    public void showNotif(Context context, String title, String message) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.diag_notif);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        dialog.setCancelable(false);

        tv_header = (TextView) dialog.findViewById(R.id.tv_header);
        tv_content = (TextView) dialog.findViewById(R.id.tv_content);
        bt_ok = (Button) dialog.findViewById(R.id.bt_ok);
        tv_header.setText(title);


        if(message.contains("|"))
        {
            String parts[] = message.split("\\|");
            String allText = "";
            for(int i=0;i<parts.length;i++){
                if(parts[i].contains("~"))
                    allText += "\n";
                else
                    allText += parts[i] + "\n";
            }
            tv_content.setText(allText);
        }else {
            tv_content.setText(message);
        }

        bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();

//
//        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.40);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

    }

    public void showMeValidateReq(final String response,
                                  Context context, ConfirmLoa getCallback,
                                  final String ORIGIN, final String BatchCode) {



        final ConfirmLoa callback;
        callback = getCallback;
        Button btn_accept;
        Button btn_cancel;
        TextView tv_message;

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.confirmloa);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);


        spinner = (Spinner) dialog.findViewById(R.id.dropStatus);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//                android.R.layout.simple_spinner_item,getResources().getStringArray());
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.repeat, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        btn_accept = (Button) dialog.findViewById(R.id.btn_accept);
        btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        tv_message.setText(response);
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Log.d("BatchCode", BatchCode);
                callback.loaDuplicateSendApproval(ORIGIN, BatchCode);
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();


        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.40);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = width;
        dialog.getWindow().setAttributes(lp);

    }

    public void showMeValidateReqforBasicOtherTest(final BasicTestOrOtherTestReturn LOAReturn,
                                                   Context context, ConfirmLoa getCallback,
                                                   final String ORIGIN, final String BatchCode) {

        final ConfirmLoa callback;
        callback = getCallback;
        Button btn_accept;
        Button btn_cancel;
        TextView tv_message;


        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.confirmloa);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        btn_accept = (Button) dialog.findViewById(R.id.btn_accept);
        btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        tv_message.setText(LOAReturn.getResponseDesc());
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Log.d("BatchCode", BatchCode);
                callback.loaDuplicateSendApprovalforBasicTestAndOtherTest(ORIGIN, BatchCode, LOAReturn);
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();


        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.40);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = width;
        dialog.getWindow().setAttributes(lp);

    }

    public void showMeValidateReqforBasicTest(final BasicTestResponse LOAReturn,
                                                   Context context, ConfirmLoa getCallback,
                                                   final String ORIGIN, final String BatchCode) {

        final ConfirmLoa callback;
        callback = getCallback;
        Button btn_accept;
        Button btn_cancel;
        TextView tv_message;


        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.confirmloa);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        btn_accept = (Button) dialog.findViewById(R.id.btn_accept);
        btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        tv_message.setText(LOAReturn.getResponseDesc());
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Log.d("BatchCode", BatchCode);
                callback.loaDuplicateSendApprovalforBasicTest(ORIGIN, BatchCode, LOAReturn);
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();


        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.40);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = width;
        dialog.getWindow().setAttributes(lp);

    }



    public void showMePrintableLOA(final Context context, String title, String message, final String ORIGIN, final String referenceNumber) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_view_loa);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        Button btn_view_loa;

        ci_error_image = (CircleImageView) dialog.findViewById(R.id.ci_error_image);
        tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        btn_accept = (Button) dialog.findViewById(R.id.btn_accept);
        btn_view_loa = (Button) dialog.findViewById(R.id.btn_view_loa);
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_view_loa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLOA(context, ORIGIN, referenceNumber);
            }
        });

        setDetails(context, message, title, 1, btn_accept);


        dialog.show();


        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.40);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = width;
        dialog.getWindow().setAttributes(lp);

    }

    public void showLOA(Context context, String ORIGIN, String referenceNumber) {


        try {


            File pdfFolder = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DCIM), "MediCard");

            String loaFileName = StatusType.genFileName(ORIGIN, referenceNumber);
            File loaFormFile = new File(pdfFolder, loaFileName + ".pdf");


            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(loaFormFile), "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "No PDF Viewer Installed", Toast.LENGTH_LONG).show();
        }


    }


    private void setDetails(Context context, String message, String title, int errorImage, Button button) {

        tv_message.setText(message);
        tv_title.setText(title);


        switch (errorImage) {

            case 1:
//error
                ci_error_image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon));
                button.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimaryLight));
                break;


            case 2:
//congrats
                ci_error_image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.success));
                button.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));

                break;


            case 3:
//Log out
                ci_error_image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.logout));
                button.setBackgroundColor(ContextCompat.getColor(context, R.color.BLACK));

                break;


        }
    }

    public void showMeDuplicateSameDetailsReq(Context context, String responseDesc) {


        Button btn_cancel;
        TextView tv_message;

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_same_details_req);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);


        btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        tv_message = (TextView) dialog.findViewById(R.id.tv_message);

        tv_message.setText(responseDesc);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();


        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.40);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = width;
        dialog.getWindow().setAttributes(lp);

    }


    public void showWaiver(final Context context, final ConfirmLoa callback) {


        final Button btn_accept, btn_decline;
        final CheckBox checkBox;

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_waiver);
        checkBox = (CheckBox) dialog.findViewById(R.id.checkBox);
        btn_accept = (Button) dialog.findViewById(R.id.btn_accept);
        btn_decline = (Button) dialog.findViewById(R.id.btn_cancel);

        setButtonEnabled(btn_accept, false, context);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                setButtonEnabled(btn_accept, b, context);
            }
        });

        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                callback.onWaiverAcceptListener(getIsTicked(checkBox.isChecked()));
            }
        });


        btn_decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.setCancelable(false);
        dialog.show();

        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 1.0);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = width;
        dialog.getWindow().setAttributes(lp);


    }

    private String getIsTicked(boolean checked) {
        return checked ? "1" : "0";
    }

    private void setButtonEnabled(Button btn_accept, boolean isChecked, Context context) {


        if (isChecked) {
            btn_accept.setEnabled(true);
            btn_accept.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimaryLight));
        } else {
            btn_accept.setEnabled(false);
            btn_accept.setBackgroundColor(ContextCompat.getColor(context, R.color.gray));
        }


    }


    public static void showError(String message, Context context, final ConfirmationAttachment confirmationAttachment) {
        TextView tv_message;

        Button btn_accept;
        final Dialog dialog;
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alertshow);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        btn_accept = (Button) dialog.findViewById(R.id.btn_accept);
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmationAttachment.onClickOk();
                dialog.dismiss();
            }
        });


        tv_message.setText(message);
        dialog.show();


        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.40);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = width;
        dialog.getWindow().setAttributes(lp);


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        spinner.setSelection(position, false);
        repeat = spinner.getSelectedItem().toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

//    public void showMeSpashScreenError(Context context, String invalid_pass_userp_title, String s, final ConfirmationAttachment confirmationAttachment) {
//
//        TextView tv_message;
//
//        Button btn_accept;
//        final Dialog dialog;
//        dialog = new Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.alertshow);
//        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        dialog.setCancelable(false);
//
//        tv_message = (TextView) dialog.findViewById(R.id.tv_message);
//        btn_accept = (Button) dialog.findViewById(R.id.btn_accept);
//        btn_accept.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                confirmationAttachment.onClickOk();
//                dialog.dismiss();
//            }
//        });
//
//
//        tv_message.setText(message);
//        dialog.show();
//
//
//        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.40);
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.width = width;
//        dialog.getWindow().setAttributes(lp);
//
//
//    }


    public interface ConfirmLoa {

        void loaDuplicateSendApproval(String ORIGIN, String batchCode);

        void onWaiverAcceptListener(String isTicked);

        void loaDuplicateSendApprovalforBasicTestAndOtherTest(String origin, String batchCode, BasicTestOrOtherTestReturn loaReturn);

        void loaDuplicateSendApprovalforBasicTest(String origin, String batchCode, BasicTestResponse loaReturn);
    }

    public interface ConfirmationAttachment {
        void onClickOk();

        void onClickCancel();
    }
}
