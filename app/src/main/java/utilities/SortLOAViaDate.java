package utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import coord.medicard.com.medicardcoordinator.LoaDisplay.LoaListCardRetrieve;
import model.LoaList;

/**
 * Created by mpx-pawpaw on 5/8/17.
 */

public class SortLOAViaDate {



    public static ArrayList<LoaListCardRetrieve> sortData(ArrayList<LoaListCardRetrieve> arrayList){

        Collections.sort(arrayList, new Comparator<LoaListCardRetrieve>() {
            DateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

            @Override
            public int compare(LoaListCardRetrieve o1,LoaListCardRetrieve o2) {

                Date f1 = null, f2 = null;
                try {
                    f2 = f.parse(o1.getRequestDateTime());
                    f1 = f.parse(o2.getRequestDateTime());
                    return f1.compareTo(f2);

                } catch (Exception e) {
                    e.printStackTrace();
                    return 0;
                }


            }
        });


   return  arrayList;
    }
}
