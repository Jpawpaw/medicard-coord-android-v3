package utilities;

import java.util.ArrayList;

import model.BasicTests;
import model.DiagnosisList;
import model.DoctorModelInsertion;
import model.TestsAndProcedures;

/**
 * Created by IPC on 12/6/2017.
 */

public class InpatientSession {

    private static ArrayList<DoctorModelInsertion> doctorModelInsertions = new ArrayList<>();
    private static ArrayList<DiagnosisList> diagnosisModelInsertions = new ArrayList<>();
    private static ArrayList<TestsAndProcedures> procedureModelInsertions = new ArrayList<>();

    //for doctors
    public void addString(DoctorModelInsertion s){
        doctorModelInsertions.add(s);
    }

    public static ArrayList<DoctorModelInsertion> getAllSelectedDoctor() {
        return doctorModelInsertions;
    }

    public static void releaseContentDoctorModel() {
        doctorModelInsertions.clear();
        doctorModelInsertions = new ArrayList<>();
    }

    //for diagnosis
    public void addString(DiagnosisList s){
        diagnosisModelInsertions.add(s);
    }

    public static ArrayList<DiagnosisList> getAllSelectedDiagnosis() {
        return diagnosisModelInsertions;
    }

    public static void releaseContentDiagnosisModel() {
        diagnosisModelInsertions.clear();
        diagnosisModelInsertions = new ArrayList<>();
    }


    //for procedure
    public void addString(TestsAndProcedures s){
        procedureModelInsertions.add(s);
    }

    public static ArrayList<TestsAndProcedures> getAllSelectedProcedure() {
        return procedureModelInsertions;
    }

    public static void releaseContentProcedureModel() {
        procedureModelInsertions.clear();
        procedureModelInsertions = new ArrayList<>();
    }
}
