package utilities;

import android.content.Context;

/**
 * Created by mpx-pawpaw on 4/19/17.
 */

public class OriginTester {


    public static boolean isOriginFromPrimary(Context context) {
        return SharedPref.getStringValue(SharedPref.USER, SharedPref.PROCEDURE_ORIGIN, context)
                .equals(Constant.ADAPTER_PRIMARY);

    }

    public static boolean isOriginFromOther(Context context) {
        return SharedPref.getStringValue(SharedPref.USER, SharedPref.PROCEDURE_ORIGIN, context)
                .equals(Constant.ADAPTER_OTHER);
    }

    public static boolean isOriginFromProc(Context context) {
        return SharedPref.getStringValue(SharedPref.USER, SharedPref.PROCEDURE_ORIGIN, context)
                .equals(Constant.ADAPTER_PROCEDURE);

    }

    public static boolean isOriginFromOtherSecond(Context context){
        return SharedPref.getStringValue(SharedPref.USER, SharedPref.PROCEDURE_ORIGIN, context)
                .equals(Constant.ADAPTER_OTHER_2);
    }


    public static boolean isFromOriginInPatientProc(Context context) {
        return SharedPref.getStringValue(SharedPref.USER, SharedPref.PROCEDURE_ORIGIN, context)
                .equals(Constant.ADAPTER_PROC_IN_PATIENT);

    }
}
