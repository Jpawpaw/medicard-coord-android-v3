package utilities;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import coord.medicard.com.medicardcoordinator.CoordinatorMenu.CoordinatorMenuActivity;
import coord.medicard.com.medicardcoordinator.MemberVerificationAcitivty;
import model.VerifyMember;

/**
 * Created by mpx-pawpaw on 11/14/16.
 */

public class SearchToCoordinatorMenu {


    public static String NAME = "NAME";
    public static String GENDER = "GENDER";
    public static String AGE = "AGE";
    public static String COMPANY = "COMPANY";
    public static String ID = "ID";
    public static String STATUS = "STATUS";
    public static String MEMCODE = "MEMCODE";
    public static String TYPE = "TYPE";
    public static String EFFECTIVE = "EFFECTIVE";
    public static String VALID = "VALID";
    public static String DD_LIMIT = "DD_LIMIT";
    public static String PLAN = "PEC";
    public static String OTHER = "OTHER";
    public static String USERNAME = "USERNAME";
    public static String ISKIPPED = "ISKIPPED";
    public static String BDAY = "BDAY";
    public static String BDAY_RAW = "BDAY_RAW";
    public static String REQUEST_CODE_IN_PATIENT = "REQUEST_CODE_IN_PATIENT";
    public static String REQUEST_DATA_IN_PATIENT = "REQUEST_DATA_IN_PATIENT";
    public static String REQUEST_CODE_ER = "REQUEST_CODE_ER";

    public static String APPROVAL_NO = "APPROVAL_NO";
    public static String VALIDITYDATE = "VALIDITYDATE";
    public static String EFFECTIVITYDATE = "EFFECTIVITYDATE";
    public static String UTILIZATION = "UTILIZATION";
    public static String DEPENDENTS = "DEPENDENTS";
    public static String IS_IN_PATIENT = "IS_IN_PATIENT";
    public static String IS_ER ="IS_ER";

    //This variable is used for changing the text in in-patient to Discharge
    public static String DISCHARGE_TO_INPATIENT = "ENDORSE ER TO IN-PATIENT";

    //This variable is used for changing the text in out-patient to Discharge
    public static String DISCHARGE_TO_OUTPATIENT = "ENDORSE ER TO OUT-PATIENT";


    AlertDialogCustom alertDialogCustom = new AlertDialogCustom();

    public void gotoCoordinator(String dateofBirth, Context context, VerifyMember memberInfo) {

        if (memberInfo.getMemberInfo().getBDAY().equals(dateofBirth)) {


            if (memberInfo.getVerifiedMember().getSTATUS().equals("ACTIVE")) {
                Log.d("BAD", "BAD");
                Intent gotoMenu = new Intent(context, CoordinatorMenuActivity.class);
                gotoMenu.putExtra(NAME, memberInfo.getMemberInfo().getMEM_NAME());
                gotoMenu.putExtra(ID, memberInfo.getMemberInfo().getPRIN_CODE());
                gotoMenu.putExtra(STATUS, memberInfo.getMemberInfo().getMem_OStat_Code());
                gotoMenu.putExtra(MEMCODE, memberInfo.getMemberInfo().getACCOUNT_CODE());
                gotoMenu.putExtra(TYPE, memberInfo.getMemberInfo().getMEM_TYPE());
                gotoMenu.putExtra(EFFECTIVE, memberInfo.getMemberInfo().getEFF_DATE());
                gotoMenu.putExtra(VALID, memberInfo.getMemberInfo().getVAL_DATE());
                gotoMenu.putExtra(DD_LIMIT, memberInfo.getMemberInfo().getDD_Reg_Limits());
                gotoMenu.putExtra(OTHER, memberInfo.getMemberInfo().getOTHER_REM());
                gotoMenu.putExtra(PLAN, memberInfo.getMemberInfo().getPlan_Desc());

                context.startActivity(gotoMenu);
            } else if (memberInfo.getVerifiedMember().getSTATUS().equals("UNVERIFIED")) {
                Log.d("GOOD", "GOOD");
                SharedPref s = new SharedPref();
                s.setStringValue(s.USER, s.ID, memberInfo.getMemberInfo().getPRIN_CODE(), context);
                Intent gotoValidate = new Intent(context, MemberVerificationAcitivty.class);
                gotoValidate.putExtra(NAME, memberInfo.getMemberInfo().getMEM_NAME());
                gotoValidate.putExtra(COMPANY, memberInfo.getMemberInfo().getACCOUNT_NAME());
                gotoValidate.putExtra(AGE, memberInfo.getMemberInfo().getAGE());
                gotoValidate.putExtra(GENDER, memberInfo.getMemberInfo().getMEM_SEX());
                gotoValidate.putExtra(ID, memberInfo.getMemberInfo().getPRIN_CODE());
                gotoValidate.putExtra(STATUS, memberInfo.getMemberInfo().getMem_OStat_Code());


                gotoValidate.putExtra(USERNAME, memberInfo.getVerifiedMember().getAPP_USERNAME());
                gotoValidate.putExtra(ISKIPPED, memberInfo.getVerifiedMember().getIS_SKIPPED());
                gotoValidate.putExtra(NAME, memberInfo.getMemberInfo().getMEM_NAME());
                gotoValidate.putExtra(ID, memberInfo.getMemberInfo().getPRIN_CODE());
                gotoValidate.putExtra(STATUS, memberInfo.getMemberInfo().getMem_OStat_Code());
                gotoValidate.putExtra(MEMCODE, memberInfo.getMemberInfo().getACCOUNT_CODE());
                gotoValidate.putExtra(TYPE, memberInfo.getMemberInfo().getMEM_TYPE());
                gotoValidate.putExtra(EFFECTIVE, memberInfo.getMemberInfo().getEFF_DATE());
                gotoValidate.putExtra(VALID, memberInfo.getMemberInfo().getVAL_DATE());
                gotoValidate.putExtra(DD_LIMIT, memberInfo.getMemberInfo().getDD_Reg_Limits());

                gotoValidate.putExtra(PLAN, memberInfo.getMemberInfo().getPlan_Desc());
                gotoValidate.putExtra(OTHER, memberInfo.getMemberInfo().getID_REM()
                        + "/n" +
                        memberInfo.getMemberInfo().getID_REM2()
                        + "/n" +
                        memberInfo.getMemberInfo().getID_REM3()
                        + "/n" +
                        memberInfo.getMemberInfo().getID_REM4()
                        + "/n" +
                        memberInfo.getMemberInfo().getID_REM5()
                        + "/n" +
                        memberInfo.getMemberInfo().getID_REM6()
                        + "/n" +
                        memberInfo.getMemberInfo().getID_REM7());


                context.startActivity(gotoValidate);
            }
        } else {

            alertDialogCustom.showMe(context, alertDialogCustom.not_title, alertDialogCustom.not_message, 1);

        }


    }
}
