package utilities;

import android.util.Log;

import java.util.ArrayList;

import model.DiagnosisList;
import model.DoctorModelInsertion;
import model.GetDoctorsToHospital;
import model.ProceduresList;
import model.ProceduresListLocal;
import model.RoomCategory;
import model.Rooms;
import model.RoomsAvail;
import model.TestsAndProcedures;

/**
 * Created by mpx-pawpaw on 11/21/16.
 */

public class SetDatatoDatabase {

    /**
     * This method adds a List of DoctorsToHospital to database
     * */
    public static void setDoctorsDataToDatabase(ArrayList<GetDoctorsToHospital> g, DataBaseHandler databaseH) {


        for (int x = 0; x < g.size(); x++) {

            databaseH.createDoctorsList(new DoctorModelInsertion(
                    g.get(x).getSpecialRem(),
                    g.get(x).getDocFname(),
                    g.get(x).getSpecDesc(),
                    g.get(x).getHospRemarks(),
                    g.get(x).getDoctorCode(),
                    g.get(x).getDocMname(),
                    g.get(x).getVat(),
                    g.get(x).getWtax(),
                    g.get(x).getRemarks(),
                    g.get(x).getGracePeriod(),
                    g.get(x).getSpecCode(),
                    g.get(x).getSchedule(),
                    g.get(x).getRoom(),
                    g.get(x).getHospitalName(),
                    g.get(x).getDocLname(),
                    g.get(x).getHospitalCode(),
                    g.get(x).getRemarks2(),
                    g.get(x).getRoomBoard()));

        }

    }


    public static void setProcedureListToDatabase(ArrayList<TestsAndProcedures> diagnosisMode, DataBaseHandler databaseH) {


        for (int x = 0; x < diagnosisMode.size(); x++) {
            databaseH.createProcedureLIst(new TestsAndProcedures(
                    diagnosisMode.get(x).getProcedureCode(),
                    diagnosisMode.get(x).getProcedureAmount(),
                    diagnosisMode.get(x).getProcedureDesc(),
                    diagnosisMode.get(x).getCostCenter(),
                    diagnosisMode.get(x).getMaceServiceType()));


        }


    }


    public static void setDiagnosisListToDatabase(ArrayList<DiagnosisList> diagnosisMode, DataBaseHandler databaseH) {


        Log.d("COUNT", diagnosisMode.size() + "");
        for (int x = 0; x < diagnosisMode.size(); x++) {

            databaseH.createDiagnosis(new DiagnosisList(
                    diagnosisMode.get(x).getStatus(),
                    diagnosisMode.get(x).getIcd10Desc(),
                    diagnosisMode.get(x).getDiagCode(),
                    diagnosisMode.get(x).getTypeDesc(),
                    diagnosisMode.get(x).getType(),
                    diagnosisMode.get(x).getDiagDesc(),
                    diagnosisMode.get(x).getIcd10Code(),
                    diagnosisMode.get(x).getDiagRemarks()));


        }


    }

    public static void setRoomPlanToDatabase(RoomsAvail body, DataBaseHandler databaseH) {

        for (Rooms rooms : body.getRooms()) {
            databaseH.createRoomAvail(rooms);
        }
    }

    public static void setRoomCategoryToDatabase(RoomCategory body, DataBaseHandler databaseH) {

        for (int x = 0; x < body.getCategories().length; x++) {

            databaseH.createRoomCategory(body.getCategories()[x]);

        }

    }
}
