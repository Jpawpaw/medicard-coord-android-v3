package utilities;

import java.util.ArrayList;

import model.BasicTests;
import model.TestsAndProcedures;

/**
 * Created by IPC on 8/23/2017.
 */

public class ClinicProcedureSession {
    private static ArrayList<TestsAndProcedures> arrayList = new ArrayList<>();

    public void addTests(TestsAndProcedures s){
        arrayList.add(s);
    }

    public static ArrayList<TestsAndProcedures> getAllProcedureCode() {
        return arrayList;
    }


    public static void releaseContent() {
        arrayList.clear();
        arrayList = new ArrayList<>();
    }
}

