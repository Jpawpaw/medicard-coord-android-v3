package utilities.APIMultipleCalls;

import android.util.Log;

import fragment.Avail.AvailInterface;
import model.ProcedureFilter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import services.AppInterface;
import services.AppService;

/**
 * Created by mpx-pawpaw on 6/1/17.
 */

public class FilteredProcedureCall {


    public static void getProcedureFilter(final FilterProcedureCallback callback, final String code, final String ORIGIN) {

        Log.d("DIAGCODE", code);
        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.filterProcedures(code)
                .enqueue(new Callback<ProcedureFilter>() {
                    @Override
                    public void onResponse(Call<ProcedureFilter> call, Response<ProcedureFilter> response) {

                        try {
                            if (response.body() != null) {
                                callback.procedurePrimaryCollectedSuccess(code, response.body(), ORIGIN);
                            } else {
                                callback.procedurePrimaryCollectedError("", ORIGIN);
                            }
                        } catch (Exception e) {
                            callback.procedurePrimaryCollectedError("", ORIGIN);
                        }
                    }

                    @Override
                    public void onFailure(Call<ProcedureFilter> call, Throwable t) {
                        try {
                            if (t != null) {
                                callback.procedurePrimaryCollectedError(t.getMessage(), ORIGIN);
                            } else {
                                callback.procedurePrimaryCollectedError("", ORIGIN);
                            }
                        } catch (Exception e) {
                            callback.procedurePrimaryCollectedError("", ORIGIN);
                        }
                    }
                });


    }

    public static void getProceduresOnlyFilter(final FilterProcedureCallback callback, final String code, final String ORIGIN) {

        Log.d("DIAGCODE", code);
        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.filterProceduresOnly(code)
                .enqueue(new Callback<ProcedureFilter>() {
                    @Override
                    public void onResponse(Call<ProcedureFilter> call, Response<ProcedureFilter> response) {

                        try {
                            if (response.body() != null) {
                                callback.procedurePrimaryCollectedSuccess(code, response.body(), ORIGIN);
                            } else {
                                callback.procedurePrimaryCollectedError("", ORIGIN);
                            }
                        } catch (Exception e) {
                            callback.procedurePrimaryCollectedError("", ORIGIN);
                        }
                    }

                    @Override
                    public void onFailure(Call<ProcedureFilter> call, Throwable t) {
                        try {
                            if (t != null) {
                                callback.procedurePrimaryCollectedError(t.getMessage(), ORIGIN);
                            } else {
                                callback.procedurePrimaryCollectedError("", ORIGIN);
                            }
                        } catch (Exception e) {
                            callback.procedurePrimaryCollectedError("", ORIGIN);
                        }
                    }
                });


    }

    public static void getTestsFilter(final FilterProcedureCallback callback, final String code, final String ORIGIN) {

        Log.d("DIAGCODE", code);
        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.filterTests(code)
                .enqueue(new Callback<ProcedureFilter>() {
                    @Override
                    public void onResponse(Call<ProcedureFilter> call, Response<ProcedureFilter> response) {

                        try {
                            if (response.body() != null) {
                                callback.procedurePrimaryCollectedSuccess(code, response.body(), ORIGIN);
                            } else {
                                callback.procedurePrimaryCollectedError("", ORIGIN);
                            }
                        } catch (Exception e) {
                            callback.procedurePrimaryCollectedError("", ORIGIN);
                        }
                    }

                    @Override
                    public void onFailure(Call<ProcedureFilter> call, Throwable t) {
                        try {
                            if (t != null) {
                                callback.procedurePrimaryCollectedError(t.getMessage(), ORIGIN);
                            } else {
                                callback.procedurePrimaryCollectedError("", ORIGIN);
                            }
                        } catch (Exception e) {
                            callback.procedurePrimaryCollectedError("", ORIGIN);
                        }
                    }
                });


    }


    public interface FilterProcedureCallback {
        void procedurePrimaryCollectedError(String s, String ORIGIN);

        void procedurePrimaryCollectedSuccess(String code, ProcedureFilter body, String ORIGIN);
    }
}
