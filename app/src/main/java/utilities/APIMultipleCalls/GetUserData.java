package utilities.APIMultipleCalls;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import model.ExclusionModel;
import model.Services;
import model.VerifyMember;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import services.AppInterface;
import services.AppService;
import utilities.DataBaseHandler;

/**
 * Created by mpx-pawpaw on 6/19/17.
 */

public class GetUserData {

    public static String QRCODE = "qrcode";
    public static String SEARCH ="search";


    public static void getUserData(String memberId, String birthday, String hospCode, String phoneId,
                                   String username, final GetUserCallback callback, String ORIGIN, final ProgressDialog pd) {

        Log.d("memberId", memberId);
        Log.d("hospCode", hospCode);
        Log.d("phoneId", phoneId);
        Log.d("username", username);
        Log.d("birthday", birthday);
        Log.d("search", ORIGIN);

        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.getData(memberId, birthday, phoneId, username, hospCode, ORIGIN)
                .enqueue(new Callback<VerifyMember>() {
                    @Override
                    public void onResponse(Call<VerifyMember> call, Response<VerifyMember> response) {
                        if(response.body() != null){
                            getExcludedHospList(callback,pd,response.body());
//                            callback.onSuccessGetUser(response.body());
                        }else {
                            callback.onErrorGetUser("no response to server");
                        }
                    }


                    @Override
                    public void onFailure(Call<VerifyMember> call, Throwable e) {
                        try {
                            callback.onErrorGetUser(e.getMessage());
                        } catch (Exception error) {
                            callback.onErrorGetUser("");
                        }
                    }
                });

    }

    public static void getExcludedHospList(final GetUserCallback callback, final ProgressDialog pd, final VerifyMember body) {


        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.getExclusionList()
                .enqueue(new Callback<ExclusionModel>() {
                    @Override
                    public void onResponse(Call<ExclusionModel> call, Response<ExclusionModel> response) {
                        if(response.body() != null){
                            callback.onSuccessGetExcludedList(response.body(),body);
                        }else {
                            callback.onErrorGetUser("no response to server");
                        }
                    }


                    @Override
                    public void onFailure(Call<ExclusionModel> call, Throwable e) {
                        try {
                            callback.onErrorGetUser(e.getMessage());
                        } catch (Exception error) {
                            callback.onErrorGetUser("");
                        }
                    }
                });

    }


    public static void updateAllExcludedHospToFalse(final ExclusionModel body, final VerifyMember verifyMember, final DataBaseHandler dataBaseHandler, final GetUserCallback getUserCallback) {

        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                dataBaseHandler.updateAllExcludedHospToFalse();
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                updateAllExcludedHospToTrue(body,verifyMember,dataBaseHandler,getUserCallback);
            }
        };

        asyncTask.execute();
    }

    public static void updateAllExcludedHospToTrue(final ExclusionModel body, final VerifyMember verifyMember, final DataBaseHandler dataBaseHandler, final GetUserCallback getUserCallback) {

        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                dataBaseHandler.updateAllExcludedHospToTrue(body);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                getUserCallback.onSuccessGetUser(verifyMember);
            }
        };

        asyncTask.execute();
    }


    public interface GetUserCallback {
        void onSuccessGetUser(VerifyMember body);

        void onSuccessGetExcludedList(ExclusionModel body, VerifyMember verifyMember);

        void onErrorGetUser(String message);

    }

}
