package utilities.APIMultipleCalls;

import android.os.AsyncTask;
import android.util.Log;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import services.AppInterface;
import services.AppService;


/**
 * Created by mpx-pawpaw on 3/8/17.
 */

public class RequestBlockLogger {


    public static void logger(final String memberCode, final String hospitalCode, final String appUserName, final String transaction, final String deviceID) {


        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {


                AppInterface appInterface;
                appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
                appInterface.blockedRequestLogger(
                        memberCode,
                        hospitalCode,
                        appUserName,
                        transaction, deviceID)
                        .enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                Log.d("LOGGING", "SUCCESS LOGGING");
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Log.e("LOGGING", t.getMessage());
                            }
                        });
//                        .subscribeOn(Schedulers.io())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .unsubscribeOn(Schedulers.io())
//                        .subscribe(new Subscriber<ResponseBody>() {
//                            @Override
//                            public void onCompleted() {
//
//                            }
//
//                            @Override
//                            public void onError(Throwable e) {
//                                Log.e("LOGGING", e.getMessage());
//                            }
//
//                            @Override
//                            public void onNext(ResponseBody responseBody) {
//                                Log.d("LOGGING", "SUCCESS LOGGING");
//                            }
//                        });


                return null;


            }
        };
        asyncTask.execute();
    }


}
