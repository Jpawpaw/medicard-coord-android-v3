package utilities.APIMultipleCalls;

import android.util.Log;

import model.ProcedureFilter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import services.AppInterface;
import services.AppService;


/**
 * Created by IPC_Server on 8/7/2017.
 */

public class ClinicProceduresCall {
    public static void getProcedureClinicFilter(final ClinicProceduresCall.FilterClinicProcedureCallback callback, final String code, final String ORIGIN) {

        Log.d("DIAGCODE", code);
        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.filterProceduresforclinic(code)
                .enqueue(new Callback<ProcedureFilter>() {
                    @Override
                    public void onResponse(Call<ProcedureFilter> call, Response<ProcedureFilter> response) {

                        try {
                            if (response.body() != null) {
                                callback.clinicPprocedurePrimaryCollectedSuccess(code, response.body(), ORIGIN);
                            } else {
                                callback.clinicProcedurePrimaryCollectedError("", ORIGIN);
                            }
                        } catch (Exception e) {
                            callback.clinicProcedurePrimaryCollectedError("", ORIGIN);
                        }
                    }

                    @Override
                    public void onFailure(Call<ProcedureFilter> call, Throwable t) {
                        try {
                            if (t != null) {
                                callback.clinicProcedurePrimaryCollectedError(t.getMessage(), ORIGIN);
                            } else {
                                callback.clinicProcedurePrimaryCollectedError("", ORIGIN);
                            }
                        } catch (Exception e) {
                            callback.clinicProcedurePrimaryCollectedError("", ORIGIN);
                        }
                    }
                });


    }

    public interface FilterClinicProcedureCallback {
        void clinicProcedurePrimaryCollectedError(String s, String ORIGIN);

        void clinicPprocedurePrimaryCollectedSuccess(String code, ProcedureFilter body, String ORIGIN);
    }
}
