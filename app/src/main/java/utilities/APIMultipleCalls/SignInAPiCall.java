package utilities.APIMultipleCalls;

import model.AnnouncementDataJson;
import model.LogIn;
import model.SignInDetails;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import services.AppInterface;
import services.AppService;

/**
 * Created by mpx-pawpaw on 6/19/17.
 */

public class SignInAPiCall {

    public static void signIn(LogIn logIn, final SignInCallback callback) {
        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.logInUser(logIn)
                .enqueue(new Callback<SignInDetails>() {
                    @Override
                    public void onResponse(Call<SignInDetails> call, Response<SignInDetails> response) {
                        if(response.body() != null){
                            callback.onSuccessSignIn(response.body());
                        }else {
                            callback.onErrorSignIn("no response to server");
                        }
                    }

                    @Override
                    public void onFailure(Call<SignInDetails> call, Throwable e) {
                        try {
                            callback.onErrorSignIn(e.getMessage());
                        } catch (Exception error) {
                            callback.onErrorSignIn("");
                        }

                    }
                });

    }

    public static void getNotification(final SignInCallback callback) {
        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.getAnnouncements()
                .enqueue(new Callback<AnnouncementDataJson>() {
                    @Override
                    public void onResponse(Call<AnnouncementDataJson> call, Response<AnnouncementDataJson> response) {
                        if(response.body() != null){
                            if ("200".equalsIgnoreCase(response.body().getResponseCode()))
                                callback.onNotificationSuccess(response.body());
                        }else {
                            callback.onErrorSignIn("no response to server");
                        }
                    }

                    @Override
                    public void onFailure(Call<AnnouncementDataJson> call, Throwable t) {
                        try {
                            callback.onErrorSignIn(t.getMessage());
                        } catch (Exception error) {
                            callback.onErrorSignIn("");
                        }
                    }
                });
    }


    public interface SignInCallback {
        void onSuccessSignIn(SignInDetails body);

        void onErrorSignIn(String message);

        void onNotificationSuccess(AnnouncementDataJson body);
    }

}
