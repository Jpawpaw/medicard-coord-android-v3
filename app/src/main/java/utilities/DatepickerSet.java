package utilities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import coord.medicard.com.medicardcoordinator.Navigator.NavigatorActivity;
import fragment.InPatient.InPatientAdmitted;

/**
 * Created by Jpcab on 10/4/2016.
 */

public class DatepickerSet {


    static DatePickerDialog datePickerDialog;
    static String StringdatetoSend = "";
    //BIRTHDAY FORMAT 19751213

    public static void getDate(final Context context, final TextView setData, final String ORIGIN) {


        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
        int day = now.get(Calendar.DAY_OF_MONTH);


        datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(android.widget.DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {


                        java.text.DateFormat datetoShow = new SimpleDateFormat("MMM dd, yyyy");
                        java.text.DateFormat datetoSend = new SimpleDateFormat("yyyyMMdd");
                        Calendar mDate = null;


                        mDate = Calendar.getInstance();
                        mDate.set(year, monthOfYear, dayOfMonth);
                        datetoShow.format(mDate.getTime());

//                        SharedPref sharedPref = new SharedPref();
//                        sharedPref.setStringValue(sharedPref.USER, sharedPref.DAY, dayOfMonth + "", context);
//                        sharedPref.setStringValue(sharedPref.USER, sharedPref.MONTH, monthOfYear + "", context);
//                        sharedPref.setStringValue(sharedPref.USER, sharedPref.YEAR, year + "", context);

                        setData.setText(datetoShow.format(mDate.getTime()));
                        StringdatetoSend = datetoSend.format(mDate.getTime());
                        Log.d("DATE", "onDateSet: " + StringdatetoSend);

                        if (ORIGIN.equals(Constant.FROM_ADMITTED_IN_PATIENT)) {
                            EventBus.getDefault().post(new InPatientAdmitted.MessageEvent(Constant.GETDATE, StringdatetoSend));
                        } else if (ORIGIN.equals(Constant.FROM_AVIALED)) {
                            EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.GETDATE, StringdatetoSend));
                            EventBus.getDefault().post(new InPatientAdmitted.MessageEvent(Constant.DATE_ADMITTED, datetoShow.format(mDate.getTime())));

                        }

                    }
                }, year, month, day);


        datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {

//                    final Calendar c = Calendar.getInstance();
//
//                    int mYear = c.get(Calendar.YEAR); // current year
//                    int mMonth = c.get(Calendar.MONTH); // current month
//                    int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
//                    SharedPref sharedPref = new SharedPref();
//                    sharedPref.setStringValue(sharedPref.USER, sharedPref.DAY, mDay + "", context);
//                    sharedPref.setStringValue(sharedPref.USER, sharedPref.MONTH, mMonth + "", context);
//                    sharedPref.setStringValue(sharedPref.USER, sharedPref.YEAR, mYear + "", context);


                }
            }
        });


//        datePickerDialog.setButton(DialogInterf~ace.BUTTON_POSITIVE,
//                "OK", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog,
//                                        int which) {
//                        if (which == DialogInterface.BUTTON_POSITIVE) {
//
//                        }
//                    }
//                });

        if (ORIGIN.equals(Constant.FROM_AVIALED)){
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        }

        datePickerDialog.show();


    }
}
