package utilities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;

import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import coord.medicard.com.medicardcoordinator.R;

/**
 * Created by window on 10/5/2016.
 */

public class LoadCreator {


    ProgressDialog pd;

    Context context;

    public LoadCreator(Context context) {
        this.context = context;
    }

    public void setMessage(String message) {
        pd.setMessage(message);
    }

    public void startLad() {


        pd = new ProgressDialog(context, R.style.MyTheme);
        pd.setCancelable(false);
        pd.setMessage("Requesting...");
        pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pd.show();
    }


    public void stopLoad() {
        pd.dismiss();
    }
}
