package utilities;

import android.content.Context;
import android.content.SharedPreferences;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.HashMap;

import model.BasicTestOrOtherTest;
import model.Dependents;
import model.DiagnosisList;
import model.LoaList;
import model.RequestBasicTest;
import model.SendLOAConsultationAndMaternityModel;
import model.TestsAndProcedures;
import model.Utilization;

/**
 * Created by window on 9/29/2016.
 */

public class SharedPref {
    public static SendLOAConsultationAndMaternityModel sendLoaConsult = new SendLOAConsultationAndMaternityModel();
    public static RequestBasicTest requestBasicTest = new RequestBasicTest();
    public static BasicTestOrOtherTest TestOrOtherTest = new BasicTestOrOtherTest();


    public static  ArrayList<TestsAndProcedures> testsAndProcedures = new ArrayList<>();
    public static DiagnosisList primaryDiag = new DiagnosisList();

    public static String masterPASSWORD = "masterPASSWORD";
    public static String masterUSERNAME = "masterUSERNAME";
    public static String PHONE_ID = "PHONE_ID";
    public static String HOSPITALNAME = "HOSPITALNAME";
    public static String HOSPITALCODE = "HOSPITALCODE";
    public static String HOSPITALADDRESS = "HOSPITALADDRESS" ;
    public static String HAS_ER ="HAS_ER";
    public static String TYPE ="TYPE";
    public static String MEMBER_STATUS = "MEMBER_STATUS";
    public static String ISINPATIENT = "ISINPATIENT";
    public static String ISINER = "ISINER";
    public static String REQUESTING_HOSPCOODE = "REQUESTING_HOSPCOODE";
    public static String REQUESTING_HOSPNAME = "REQUESTING_HOSPNAME";
    public static String REQUESTING_HOSPADD = "REQUESTING_HOSPADD";
    public static String INPATIENT_DOCTOR_ORIGIN = "INPATIENT_DOCTOR_ORIGIN";
    public static String INPATIENT_DIAGNOSIS_ORIGIN = "INPATIENT_DIAGNOSIS_ORIGIN";
    public static String INPATIENT_PROCEDURE_ORIGIN = "INPATIENT_PROCEDURE_ORIGIN";


    public static HashMap<String, Boolean> lastUpdateTables = new HashMap<>();

    //FOR RESULTS
    public static String COMPANY_NAME = "COMPANY_NAME  ";
    public static String FULL_NAME = "FULL_NAME";
    public static String BIRTHDAY = "BIRTHDAY";
    public static String AGE = "AGE";
    public static String GENDER = "GENDER";
    public static String MEMBERCODE = "MEMBERCODE";
    public static String LIMITS = "LIMITS";

    public static String IF_PENDING = "IF_PENDING";


    public static ArrayList<Utilization> util = new ArrayList<>();
    public static ArrayList<Dependents> dependents = new ArrayList<>();


    public static String OTHER_TEST_STATUS = "OTHER_TEST_STATUS";

    public static String USER = "USER";
    public static String PROCEDURE_ORIGIN = "PROCEDURE_ORIGIN";
    public static String ROOM_ORIGIN = "ROOM_ORIGIN";
    public static String PROCEDURE_DESC = "PROCEDURE_DESC";

    public static String USERNAME = "USERNAME";
    public static String ID = "ID";
    public static String STATUSREMARKS = "STATUSREMARKS";
    public static String CODESTATUS = "CODESTATUS";
    public static String MEMBERID = "MEMBERID";
    public static String INPATIENT = "INPATIENT";
    public static String MEMBER_PLAN = "MEMBER_PLAN";
    public static String INPATIENT_ADD_DATA = "INPATIENT_ADD_DATA";
    public static String REQUEST_ID = "REQUEST_ID";
    public static String REQUEST_CODE_ER = "REQUEST_CODE_ER";
    public static String REQUEST_CODE_INPATIENT = "REQUEST_CODE_INPATIENT";

    public static String YEAR = "YEAR";
    public static String DAY = "DAY";
    public static String MONTH = "MONTH";
    public static String DIAG_ORIGIN = "DIAG_ORIGIN";

    public static String HOSP_ORIGIN = "HOSP_ORIGIN";

    public static String Consultation = "Consultation";
    public static String OtherTest = "OtherServices" ;
    public static String In_Patient = "Inpatient" ;
    public static String Out_Patient = "Outpatient" ;
    public static String Procedure = "Procedure" ;
    public static String Er = "Er";

    public static String VALID = "VALID";
    public static String EFFECTIVE = "EFFECTIVE";
    public static String REMARKS = "REMARKS";
    public static String SEARCHTYPE = "SEARCHTYPE";

    public static String HAS_MATERNITY = "HAS_MATERNITY" ;

    public static String EMAIL = "EMAIL";
    public static String CONTACT_NO = "CONTACT_NO";
    public static String COORD_NAME = "COORD_NAME";

    public static String MEMBER_SEARCH_TUTS = "MEMBER_SEARCH_TUTS" ;
    public static String COORD_MENU_TUTS = "COORD_MENU_TUTS" ;

    public static String ISKIPPED = "ISKIPPED" ;

    public static String ADD_OTHER_DIAG = "ADD_OTHER_DIAG" ;

    public static String DIAGTOEXCLUDE = "DIAGTOEXCLUDE";

    public static ArrayList<String> DIAGCODELIST = new ArrayList<>();
    public static ArrayList<String> HOSPCODELIST = new ArrayList<>();

    public static LoaList LOALIST;

    public static String UNFILTERED_HOSPITAL_CODE = "UNFILTERED_HOSPITAL_CODE";
    public static String UNFILTERED_HOSPITAL_NAME = "UNFILTERED_HOSPITAL_NAME";
    public static String UNFILTERED_HOSPITAL_ADDR = "UNFILTERED_HOSPITAL_ADDR";


    //DATA CHECKER - JJ
//    public static String INIT_COMPLETE = "NO INIT";
    public static String INIT_COMPLETE = "INIT_COMPLETE";

    public static String DOCTOR_LIST = "DOCTOR_LIST";
    public static String DOCTOR_LIST_LAST_UPDATE = "DOCTOR_LIST_LAST_UPDATE";

    public static String PROCEDURE_LIST = "PROCEDURE_LIST";
    public static String PROCEDURE_LIST_LAST_UPDATE = "PROCEDURE_LIST_LAST_UPDATE";

    public static String DIAGNOSIS_LIST = "DIAGNOSIS_LIST";
    public static String DIAGNOSIS_LIST_LAST_UPDATE = "DIAGNOSIS_LIST_LAST_UPDATE";

    public static String HOSPITAL_LIST = "HOSPITAL_LIST";
    public static String HOSPITAL_LIST_LAST_UPDATE = "HOSPITAL_LIST_LAST_UPDATE";

    public static String ROOM_PLAN_LIST = "ROOM_PLAN_LIST";
    public static String ROOM_PLAN_LIST_LAST_UPDATE = "ROOM_PLAN_LIST_LAST_UPDATE";

    public static String ROOM_CATEGORY_LIST = "ROOM_CATEGORY_LIST";
    public static String ROOM_CATEGORY_LIST_LAST_UPDATE = "ROOM_CATEGORY_LIST_LAST_UPDATE";

    public static String APPROVED_COUNT = "APPROVED_COUNT";
    public static String DISAPPROVED_COUNT = "DISAPPROVED_COUNT";
    public static String PENDING_COUNT = "PENDING_COUNT";

    public static String RECENT_TRANS_MEMCODE = "RECENT_TRANS_MEMCODE";
    public void setBoolValue(String Key, boolean value, Context context) {

        SharedPreferences prefs = context.getSharedPreferences(Key, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("LOG", value);
        editor.commit();


    }


      public static boolean getBoolValue(String Key, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Key, Context.MODE_PRIVATE);
        return preferences.getBoolean("LOG", false);
    }


    public static void setStringValue(String Key, String specID, String value, Context context) {

        SharedPreferences prefs = context.getSharedPreferences(Key, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(specID, value);
        editor.apply();


    }

    public static void setBooleanValue(String Key, String specID, boolean value, Context context) {

        SharedPreferences prefs = context.getSharedPreferences(Key, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(specID, value);
        editor.apply();
    }

    public static void setIntegerValue(String Key, String specID, int value, Context context) {

        SharedPreferences prefs = context.getSharedPreferences(Key, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(specID, value);
        editor.apply();
    }

    public static boolean getBooleanValue(String Key, String specID, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Key, Context.MODE_PRIVATE);
        return preferences.getBoolean(specID, false);
    }

    public static   String getStringValue(String Key, String specID, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Key, Context.MODE_PRIVATE);
        return preferences.getString(specID, "null");
    }

    public static int getIntegerValue(String Key, String specID, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Key, Context.MODE_PRIVATE);
        return preferences.getInt(specID, 0);
    }

}
