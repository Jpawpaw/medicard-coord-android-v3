package utilities;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by window on 12/5/2016.
 */

public class SetAvailableServices {
    //Consultation, OtherServices
    String Consultation = "Consultation";
    String OtherTest = "OtherTest";
    String In_Patient = "Inpatient";
    String Out_Patient = "Outpatient";
    String Er = "Er";


    public void setServiceType(ArrayList<String> serviceType, Context context) {


        SharedPref sharedPref = new SharedPref();
        sharedPref.setStringValue(sharedPref.USER, sharedPref.Consultation, "FALSE", context);
        sharedPref.setStringValue(sharedPref.USER, sharedPref.OtherTest, "FALSE", context);
        sharedPref.setStringValue(sharedPref.USER, sharedPref.In_Patient, "FALSE", context);
        sharedPref.setStringValue(sharedPref.USER, sharedPref.Out_Patient, "FALSE", context);
        sharedPref.setStringValue(sharedPref.USER, sharedPref.Er, "FALSE", context);
        if (serviceType.size() != 0) {


            for (int x = 0; x < serviceType.size(); x++) {
                if (serviceType.get(x).equals(Consultation)) {
                    sharedPref.setStringValue(sharedPref.USER, sharedPref.Consultation, "TRUE", context);
                }
                if (serviceType.get(x).equals(OtherTest)) {
                    sharedPref.setStringValue(sharedPref.USER, sharedPref.OtherTest, "TRUE", context);
                }
                if (serviceType.get(x).equals(In_Patient)) {
                    sharedPref.setStringValue(sharedPref.USER, sharedPref.In_Patient, "TRUE", context);
                }
                if (serviceType.get(x).equals(Out_Patient)) {
                    sharedPref.setStringValue(sharedPref.USER, sharedPref.Out_Patient, "TRUE", context);
                }
                if (serviceType.get(x).equals(Er)) {
                    sharedPref.setStringValue(sharedPref.USER, sharedPref.Er, "TRUE", context);
                }
            }


        }


    }

}
