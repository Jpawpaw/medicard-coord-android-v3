package utilities;

import java.util.ArrayList;
import java.util.List;

import model.BasicTests;
import model.TestsAndProcedures;

/**
 * Created by macbookpro on 8/12/17.
 */

public class BasicTestSession {


    private static ArrayList<BasicTests> arrayList = new ArrayList<>();


    public void addString(BasicTests s){
        arrayList.add(s);
    }

    public static ArrayList<BasicTests> getAllProcedureCode() {
        return arrayList;
    }


    public static void releaseContent() {
        arrayList.clear();
        arrayList = new ArrayList<>();
    }

    private static ArrayList<TestsAndProcedures> arrayListTestsAndProcedures = new ArrayList<>();


    public void addString(TestsAndProcedures s){
        arrayListTestsAndProcedures.add(s);
    }

    public static ArrayList<TestsAndProcedures> getAllTestAndProcedureCode() {
        return arrayListTestsAndProcedures;
    }


    public static void releaseContentTestsAndProcedures() {
        arrayListTestsAndProcedures.clear();
        arrayListTestsAndProcedures = new ArrayList<>();
    }


}
