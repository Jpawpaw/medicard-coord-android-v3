package utilities;

import android.content.Context;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindView;
import coord.medicard.com.medicardcoordinator.R;

/**
 * Created by mpx-pawpaw on 11/18/16.
 */

public class StatusSetter {

    @BindView(R.id.txtInpatient)
    LinearLayout txtInpatient;
    static String[] stringBender =

            {
                    "ACTIVE", "DISAPPROVED", "FOR ENCODING", "MEDICAL EVALUATION", "ON HOLD", "FOR APPROVAL",
                    "RESIGNED", "CANCELLED", "PENDING (E-MEDICARD)", "LAPSE (NON RENEW)", "FOR REACTIVATION", "VERIFY PAYMENT WITH RMD",
                    "VERIFY RENEWAL FROM MARKETING / SALES", "VERIFY MEMBERSHIP"
            };

    static String[] stringResult = {

            "ACTIVE", "DISAPPROVED", "PENDING", "PENDING", "PENDING", "PENDING", "INACTIVE", "INACTIVE",
            "PENDING", "INACTIVE", "INACTIVE", "PENDING", "PENDING", "PENDING"


    };


    public static String setStatus(String status, TextView tv_status) {
        String result = "";

        for (int x = 0; x < stringBender.length; x++) {

            if (stringBender[x].equals(status)) {

                result = stringResult[x];
                break;
            }

        }


        tv_status.setText(result);

        if (result.equals("ACTIVE")) {
            tv_status.setTextColor(Color.BLACK);
        } else {
            tv_status.setTextColor(Color.RED);
        }


        return result;
    }


    public static String setStatus(String status) {
        String result = "";

        for (int x = 0; x < stringBender.length; x++) {

            if (stringBender[x].equals(status)) {
                result = stringResult[x];
                break;
            }

        }

        return result;
    }


    public static String setRemarks(String status) {


        String setRemark = "";
        if (status.equals("DISAPPROVED")) {

            setRemark = "Please call 841-8080 to verify validity.";
        } else if (status.equals("FOR ENCODING")) {

            setRemark = "Please call 841-8080 to verify validity.";
        } else if (status.equals("MEDICAL EVALUATION")) {

            setRemark = "Please call 841-8080 to verify validity.";
        } else if (status.equals("FOR APPROVAL")) {

            setRemark = "Please call 841-8080 to verify validity.";
        } else if (status.equals("RESIGNED")) {

            setRemark = "Patient is no longer a MediCard member";
        } else if (status.equals("CANCELLED")) {

            setRemark = "Patient is no longer a MediCard member";
        } else if (status.equals("LAPSE (NON RENEW)")) {
            setRemark = "Please call 841-8080 to verify validity.";
        } else if (status.equals("FOR REACTIVATION")) {
            setRemark = "Please call 841-8080 to verify membership status.";

        } else if (status.equals("VERIFY MEMBERSHIP")) {
            setRemark = "Please call 841-8080 to verify membership.";
        } else if (status.equals("VERIFY PAYMENT WITH RMD")) {
            //APPROVED
            setRemark = "Please call 841-8080 to verify membership.";
        } else if (status.equals("VERIFY RENEWAL FROM MARKETING / SALES")) {
            setRemark = "Please call 841-8080 to verify validity. ";
        }


        return setRemark;
    }


    public static String inPatientStatus(Context context, TextView txtInpatient, boolean data ) {
        if(data == true){
            txtInpatient.setText("UPDATE IN-PATIENT");
            return data ? context.getString(R.string.admitted_patient) : context.getString(R.string.admitted_patient);
        }else{
            return "Member is not admitted to this hospital.";
        }

    }

    public static String erStatus(Context context, TextView txtEr, boolean data ) {
        if(data == true){
            txtEr.setText("DISCHARGE ER");
            return data ? context.getString(R.string.admitted_er) : context.getString(R.string.admitted_er);
        }else{
            return "Member is not admitted to this hospital ER.";
        }

    }

}
