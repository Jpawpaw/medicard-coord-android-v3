package utilities;

import android.app.DatePickerDialog;
import android.content.Context;

import android.widget.TextView;
/**
 * Created by mpx-pawpaw on 3/2/17.
 */

public class DateAdmitted {


    public static void getDate(final Context context, final TextView tv_admitted ,int[] mDate) {


        DatePickerDialog datePickerDialog;


        datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(android.widget.DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int updateDateMonth = monthOfYear + 1 ;
                        tv_admitted.setText(DateConverter.convertDatetoyyyyMMdd(year + "-" + updateDateMonth + "-" + dayOfMonth));
                    }
                }, mDate[0], mDate[1], mDate[2]);


        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();

    }

}
