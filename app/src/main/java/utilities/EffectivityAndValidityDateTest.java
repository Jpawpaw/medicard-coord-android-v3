package utilities;

import android.animation.FloatArrayEvaluator;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mpx-pawpaw on 12/6/16.
 */

public class EffectivityAndValidityDateTest {


    public boolean isEffective(String effectDate) {

        Date effectiveDate = null, currentDate = null;

        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
        try {
            effectiveDate = sdf.parse(effectDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            currentDate = sdf.parse(getCurrentDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.d("effectDate", effectDate);
        Log.d("getCurrentDate", getCurrentDate());
        if (effectiveDate.after(currentDate)) {
            Log.d("effectDate", "TRUE");
            return true;
        } else {
            Log.d("effectDate", "FALSE");
            return false;
        }

    }


    public boolean isValid(String validDate) {
        Date validityDate = null, currentDate = null;

        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
        try {
            validityDate = sdf.parse(validDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            currentDate = sdf.parse(getCurrentDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.d("validDate", validDate);
        Log.d("getCurrentDate", getCurrentDate());
        if (validityDate.before(currentDate)) {
            return true;
        } else
            return false;

    }


    public String getCurrentDate() {

//"Aug 01, 2015"

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("MMM dd, yyyy");
        String formattedDate = df.format(c.getTime());
        Log.d("CURRENT_DATE", formattedDate);

        return formattedDate;
    }


//    private boolean testDates(String currentDate , String ) {
//
//
//    }

}
