package utilities;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;

/**
 * Created by window on 10/4/2016.
 */

public class SnackBarSet {

    public String INCOMPLETE = "Please complete the fields.";
    public String NO_MEMCODE = "Please Enter the member code.";


    public void setSnackBar(CoordinatorLayout coordinatorLayout, String text) {

        Snackbar snackbar = Snackbar

                .make(coordinatorLayout, text, Snackbar.LENGTH_SHORT);
        snackbar.show();

    }
}
