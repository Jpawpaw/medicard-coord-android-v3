package utilities;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mpx-pawpaw on 11/10/16.
 */

public class DateConverter {


    public static String convertDatetoyyyyMMdd(String birthday) {

        /**
         CURRENT FORMAT
         1975-01-12
         to
         19751213
         */


        String date_s = birthday;
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = dt.parse(date_s);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat dt1 = new SimpleDateFormat("MMM dd, yyyy");
        System.out.println(dt1.format(date) != null? dt.format(date):"Error parsing");

        Log.d("CURRENT", dt1.format(date));
        return dt1.format(date);
    }


    public static String convertDatetoyyyy_MM_dd(String birthday) {

        /**
         CURRENT FORMAT
         1975-01-12
         to
         19751213
         */


        if (birthday.equals("")) {
            return "";
        } else {
            String date_s = birthday;
            SimpleDateFormat dt = new SimpleDateFormat("MMM dd, yyyy");
            Date date = null;
            try {
                date = dt.parse(date_s);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
            System.out.println(dt1.format(date) != null? dt.format(date):"Error parsing");

            Log.d("CURRENT", dt1.format(date));
            return dt1.format(date);
        }

    }

    public static void getDate(int[] mDate, String getdate) {

        String date_s = getdate;
        SimpleDateFormat dt = new SimpleDateFormat("MMM dd, yyyy");
        Date date = null;
        try {
            date = dt.parse(date_s);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat day = new SimpleDateFormat("dd");
        SimpleDateFormat month = new SimpleDateFormat("MM");
        SimpleDateFormat year = new SimpleDateFormat("yyyy");
        Log.d("day", day.format(date));
        Log.d("month", month.format(date));
        Log.d("year", year.format(date));

        mDate[0] = Integer.parseInt(year.format(date));
        mDate[1] = Integer.parseInt(month.format(date));
        mDate[2] = Integer.parseInt(day.format(date));


    }

    public static String convertDatetoyyyyMMdd_(String birthday) {

        /**
         CURRENT FORMAT
         1975-01-12
         to
         19751213
         */


        String date_s = birthday;
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:ss");
        Date date = null;
        try {
            date = dt.parse(date_s);
            SimpleDateFormat dt1 = new SimpleDateFormat("MMM dd , yyyy");
            System.out.println(dt1.format(date) != null? dt.format(date):"Error parsing");
            Log.d("CURRENT", dt1.format(date));
            return dt1.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }


    public static String validityDatePLusDay(String date_, int dayToADD) {

        String date_s = date_;
        SimpleDateFormat dt = new SimpleDateFormat("MMM dd , yyyy");
        Date date = null;
        try {
            date = dt.parse(date_s);

            SimpleDateFormat day = new SimpleDateFormat("d");
            SimpleDateFormat month = new SimpleDateFormat("MM");
            SimpleDateFormat year = new SimpleDateFormat("yyyy");

            int addedDay = Integer.parseInt(day.format(date)) + dayToADD;
            String dateRaw = (Integer.parseInt(month.format(date))) + " " + addedDay + " , " + year.format(date);


            Log.d("date_parse2", dateRaw);


            return convertDateToMMddyyyy(convertDate(dateRaw));

        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

    }


    public static String convertDate(String birthday) {

        /**
         CURRENT FORMAT
         02 14, 1975
         to
         FEB 14 , 2017
         */
        Log.d("date_parse", birthday);

        String date_s = birthday;
        SimpleDateFormat dt = new SimpleDateFormat("MM dd , yyyy");
        Date date = null;
        try {
            date = dt.parse(date_s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dt1 = new SimpleDateFormat("MMM dd , yyyy");

        return dt1.format(date);
    }

    public static String convertDateToMMddyyyy(String dateAdmitted) {

        String date_s = dateAdmitted;
        SimpleDateFormat dt = new SimpleDateFormat("MMM dd , yyyy");
        Date date = null;
        try {
            date = dt.parse(date_s);
            SimpleDateFormat dt1 = new SimpleDateFormat("MM/dd/yyyy");

            return dt1.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

    }


    public static String getDate() {


        SimpleDateFormat dt = new SimpleDateFormat("MMM dd , yyyy");
        Date date = new Date();
        dt.format(date);

        return dt.format(date);
    }

    public static String setDateInPatient(String s) {
        // 2017-06-09 2:39

        String date_s = s;
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = null;
        try {
            date = dt.parse(date_s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        //   2017-06-23 05:57:01.950
        //  yyyy-MM-dd HH:mm:ss.SSS
        return dt1.format(date);
    }


    public static String convertDate(SimpleDateFormat changeTo, SimpleDateFormat changeFrom, String admittedOn) {


        Date date = null;
        try {
            date = changeFrom.parse(admittedOn);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return changeTo.format(date);
    }
}
