package utilities;

/**
 * Created by mpx-pawpaw on 10/28/16.
 */

public class GenderPicker {


    public static String setGender(int gender) {
        if (gender == 0)
            return "FEMALE";
        else return "MALE";

    }

    public static String setGenderRev(String gender) {
        if (gender.equals("FEMALE"))
            return "0";
        else return "1";

    }
}
