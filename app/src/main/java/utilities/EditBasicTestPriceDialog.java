package utilities;

import android.app.Dialog;
import android.content.Context;

import android.graphics.drawable.ColorDrawable;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.util.ArrayList;

import adapter.BasictestAdapter;
import adapter.ProceduresAdapterInPatient;
import adapter.ProceduresToBeSendAdapter;
import coord.medicard.com.medicardcoordinator.R;
import model.BasicTests;
import model.ProceduresListLocal;
import model.TestsAndProcedures;

/**
 * Created by lawrence on 11/26/16.
 */

public class EditBasicTestPriceDialog {

    TextView tv_message;
    Button btn_accept, btn_cancel;
    EditText et_price;
    Dialog dialog;
    CustomToast customToastMessage = new CustomToast();
    double amount;
    double editAmount;
    DecimalFormat formatter;

    public void showMe(final Context context, final int position, final ArrayList<TestsAndProcedures> basicTestList, final BasictestAdapter basictestAdapter, final DataBaseHandler dataBaseHandler) {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_edit_amount);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);


        tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        et_price = (EditText) dialog.findViewById(R.id.et_price);
        et_price.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(8,2)});


        amount = Double.parseDouble(basicTestList.get(position).getProcedureAmount());
        editAmount = Double.parseDouble(basicTestList.get(position).getProcedureAmount());
        formatter = new DecimalFormat("#,###.##");


        tv_message.setText("Original Price is P" + formatter.format(amount) + ".\n"
                + "Procedure: " + basicTestList.get(position).getProcedureDesc());


        if (!basicTestList.get(position).getProcedureAmount().equals(basicTestList.get(position).getProcedureAmount())) {
            et_price.setHint("Current Price is P" + formatter.format(editAmount));
        }


        btn_accept = (Button) dialog.findViewById(R.id.btn_accept);
        btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_price.getText().toString().equals("")) {
                    customToastMessage.customToastMessage("Please fill up the required field", context);
                } else {
                    editPrice(et_price.getText().toString(), basicTestList, position, basictestAdapter, et_price, context);
                    basicTestList.get(position).setProcedureAmount(et_price.getText().toString());
                    dataBaseHandler.updateTestAndProcedurePrice(basicTestList.get(position).getProcedureCode(),et_price.getText().toString());
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeEditText(et_price, context);
                dialog.dismiss();
            }
        });


        dialog.show();


        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.50);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = width;
        dialog.getWindow().setAttributes(lp);

    }


    public void showMeForUpdatePrice(final Context context, final int position, final ArrayList<BasicTests> arrayList, final BasictestAdapter basictestAdapter) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_edit_amount);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);


        tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        et_price = (EditText) dialog.findViewById(R.id.et_price);

        double amount = Double.parseDouble(arrayList.get(position).getProcedureAmount());
        double editAmount = Double.parseDouble(arrayList.get(position).getProcedureAmount());
        DecimalFormat formatter = new DecimalFormat("#,###.##");

        tv_message.setText("Original Price is P" + formatter.format(amount) + ".\n"
                + "Procedure: " + arrayList.get(position).getProcedureDesc());


        if (!arrayList.get(position).getProcedureAmount().equals(arrayList.get(position).getProcedureAmount())) {
            et_price.setHint("Current Price is P" + formatter.format(editAmount));
        }


        btn_accept = (Button) dialog.findViewById(R.id.btn_accept);
        btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_price.getText().toString().equals("")) {
                    customToastMessage.customToastMessage("Please fill up the required field", context);
                } else {
                    editPrice2(et_price.getText().toString(), arrayList, position, basictestAdapter, et_price, context);
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeEditText(et_price, context);
                dialog.dismiss();
            }
        });


        dialog.show();


        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.50);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = width;
        dialog.getWindow().setAttributes(lp);

    }

    private void editPrice(String s, ArrayList<TestsAndProcedures> arrayList, int position, BasictestAdapter basictestAdapter, EditText et_price, Context context) {

        arrayList.get(position).setProcedureAmount(s);

        Gson gson = new Gson();
        basictestAdapter.notifyDataSetChanged();
        Log.d("EDITTEDpRICE", gson.toJson(arrayList));
        closeEditText(et_price, context);
        dialog.dismiss();


    }

    private void editPrice2(String s, ArrayList<BasicTests> arrayList, int position, BasictestAdapter basictestAdapter, EditText et_price, Context context) {

        arrayList.get(position).setProcedureAmount(s);

        Gson gson = new Gson();
        Log.d("EDITTEDpRICE", gson.toJson(arrayList));
        basictestAdapter.notifyDataSetChanged();
        closeEditText(et_price, context);
        dialog.dismiss();


    }


    private void closeEditText(EditText editText, Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);


    }
}
