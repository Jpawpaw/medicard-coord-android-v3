package utilities;

import android.os.Environment;

import java.io.File;

import utilities.pdfUtil.StatusType;

/**
 * Created by mpx-pawpaw on 5/25/17.
 */

public class CheckFileExistence {


    public static boolean fileExistance( String ORIGIN, String approval_no) {


        String loaFileName =
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) +
                        File.separator + "MediCard" + File.separator +
                        StatusType.genFileName(ORIGIN, approval_no) + ".pdf";


        File file = new File(loaFileName);
        return file.exists();
    }
}
