package utilities;

import java.util.ArrayList;

import model.BasicTests;
import model.DiagnosisClinicProcedures;
import model.RequestBasicTest;
import model.Test.DiagnosisProcedures;


/**
 * Created by mpx-pawpaw on 5/29/17.
 */

public class GetOnlyApprovedData {

    public static ArrayList<DiagnosisProcedures> updateGetOnlyApprovedProc(ArrayList<DiagnosisProcedures> diagnosisProcedures) {
        ArrayList<DiagnosisProcedures> data = new ArrayList<>();

        try{
            for (int x = 0; x < diagnosisProcedures.size(); x++) {
                if (!diagnosisProcedures.get(x).getMaceRequestTest().getStatus().equals(Constant.PENDING)) {
                    DiagnosisProcedures getDiag = diagnosisProcedures.get(x);
                    data.add(getDiag);
                    System.out.print("\nPENDING DAPAT:"+getDiag);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }



        return data;
    }

    //trial for pending adapter
    public static ArrayList<DiagnosisProcedures> updateGetOnlyPending(ArrayList<DiagnosisProcedures> diagnosisProceduresPending) {
        ArrayList<DiagnosisProcedures> dataPending = new ArrayList<>();

        try{
            for (int x = 0; x < diagnosisProceduresPending.size(); x++) {
                if (diagnosisProceduresPending.get(x).getMaceRequestTest().getStatus().equals(Constant.PENDING)) {
                    DiagnosisProcedures getDiagPending = diagnosisProceduresPending.get(x);
                    dataPending.add(getDiagPending);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return dataPending;
    }

    //clinic procedures
    public static ArrayList<DiagnosisClinicProcedures> updateGetOnlyApprovedProc2(ArrayList<model.DiagnosisClinicProcedures> diagnosisProcedures) {
        ArrayList<DiagnosisClinicProcedures> data = new ArrayList<>();

        try{
            for (int x = 0; x < diagnosisProcedures.size(); x++) {
                if (!diagnosisProcedures.get(x).getMaceRequestProcedure().getStatus().equals(Constant.PENDING)) {
                    DiagnosisClinicProcedures getDiag = diagnosisProcedures.get(x);
                    data.add(getDiag);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return data;
    }

    public static ArrayList<DiagnosisClinicProcedures> updateGetOnlyApprovedProc3(ArrayList<model.DiagnosisClinicProcedures> diagnosisProcedures) {
        ArrayList<DiagnosisClinicProcedures> data = new ArrayList<>();

        try{
            for (int x = 0; x < diagnosisProcedures.size(); x++) {
                if (diagnosisProcedures.get(x).getMaceRequestProcedure().getStatus().equals(Constant.PENDING)) {
                    DiagnosisClinicProcedures getDiag = diagnosisProcedures.get(x);
                    data.add(getDiag);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return data;
    }

//    public static ArrayList<RequestBasicTest> updateGetOnlyApprovedProc3(ArrayList<RequestBasicTest> basicTestsArrayList) {
//        ArrayList<RequestBasicTest> data = new ArrayList<>();
//
//        try{
//            for (int x = 0; x < basicTestsArrayList.size() ; x++) {
//                if (!basicTestsArrayList.get(x)..equals(Constant.PENDING)) {
//                    RequestBasicTest getBasicTest = basicTestsArrayList.get(x);
//                    data.add(getBasicTest);
//                }
//            }
//
//
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//
//
//
//        return data;
//    }
}
