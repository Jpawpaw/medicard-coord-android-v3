package services;

import java.util.HashMap;
import java.util.List;

import model.AnnouncementDataJson;
import model.BasicTestOrOtherTest;
import model.BasicTestOrOtherTestReturn;
import model.BasicTestReturn;
import model.Confirm;
import model.DiagnosisModel;
import model.DoctorModel;
import model.ExclusionModel;
import model.GetInPatientReturn;
import model.GetEr;
import model.InPatient;
import model.InPatientUpdate;
import model.LOAReturn;
import model.ListingUpdateReturn;
import model.Loa;
import model.LoaReq;
import model.LogIn;
import model.OtherServices;
import model.Procedure;
import model.ProcedureFilter;
import model.ProcedureModel;
import model.RecentTransactions;
import model.RequestBasicTest;
import model.RoomCategory;
import model.RoomsAvail;
import model.SendEr;
import model.SendInPatientRequest;
import model.SendLOAConsultationAndMaternityModel;
import model.SignInDetails;
import model.SkipVerification;
import model.UpdatedInfo;
import model.VerifyAccount;
import model.VerifyMember;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by John Paulo Cabrera on 10/5/2016.
 */

public interface AppInterface {

//    String ENDPOINT = "https://125.5.100.202:8443/";
//    String PHOTOLINK = "https://125.5.100.202:8443/downloadpicture/";

//    String ENDPOINT = "http://125.5.100.202:8443/";
//    String PHOTOLINK = "http://125.5.100.202:8443/downloadpicture/";

//    String ENDPOINT = "https://10.0.2.2:8443/";
//    String PHOTOLINK = "https://10.0.2.2:8443/downloadpicture/";

//    String ENDPOINT = "http://mace-public01.medicardphils.com:8080/";
//    String PHOTOLINK = "http://mace-public01.medicardphils.com:8080/downloadpicture/";


//    String ENDPOINT = "http://192.168.137.1:8083/";
//    String PHOTOLINK = "http://192.168.137.1:8083/downloadpicture/";

    String ENDPOINT = "http://macetestsvr01.medicardphils.com:8443/";
    String PHOTOLINK = "http://macetestsvr01.medicardphils.com:8443/downloadpicture/";

//    String ENDPOINT = "http://10.10.24.195:8080/";
//    String PHOTOLINK = "http://10.10.24.195:8080/downloadpicture/";

//    String ENDPOINT = "http://192.168.137.1:8080";
//    String PHOTOLINK = "http://192.168.137.1:8080/downloadpicture/";

//    String ENDPOINT = "http://macestaging.medicardphils.com:8080/";
//    String PHOTOLINK = "http://macestaging.medicardphils.com:8080/downloadpicture/";

    //original
//    @GET("listing/getDoctorsToHospital/?=")
//    Call<DoctorModel> getDoctorList(@Query(("hospitalCode")) String hospitalCode);

    @GET("/v2/listing/getDoctorsToHospital/")
    Call<DoctorModel> getDoctorList(@Query("hospitalCode") String hospitalCode,
                                    @Query("last_update_date") String last_update_date);

    @POST("app/loginAppUser/")
    Call<SignInDetails> logInUser(@Body LogIn logIn);


    @GET("v2/searchMember/?&&&&")
    Call<VerifyMember> getData(@Query("memberCode") String memberCode,
                               @Query("dob") String dob,
                               @Query("deviceID") String deviceID,
                               @Query("appUserName") String appUserName,
                               @Query("hospitalCode") String hospitalCode,
                               @Query("searchType") String searchType);

//    @GET("viewaccountinfo/{id}")
//    Observable<VerifyMember> getUserData(@Path("id") String id);

    @POST("skipVerification/")
    Call<ResponseBody> verifier(@Body SkipVerification skipVerification);

    @POST("verifyAccount/")
    Call<ResponseBody> verifyAccount(@Body VerifyAccount verifyAccount);

    @Multipart
    @POST("uploadpicture")
    Call<ResponseBody> upload(@Part("file\"; filename=\"image.png\"") RequestBody file,
                              @Part("memCode") RequestBody memCode,
                              @Part("appUsername") RequestBody appUsername,
                              @Part("userType") RequestBody userType
    );

    @DELETE("deletepicture/{id}")
    Observable<ResponseBody> deletePhoto(@Path("id") String id);

    @POST("/coordinator/v2/requestLOAForProcedures/")
    Call<LOAReturn> getProceduresResult(@Body Procedure procedure);


    @POST("/coordinator/v3/requestLOAConsult/")
    Call<LOAReturn> getConsultationResult(@Body SendLOAConsultationAndMaternityModel sendLOAConsultationAndMaternityModel);

    //Changed Oct 2 2017, -- jj
    @POST("/coordinator/v3/requestLOAMaternity/")
    Call<LOAReturn> getMaternityResult(@Body SendLOAConsultationAndMaternityModel sendLOAConsultationModel);

    @POST("/coordinator/v3/checkForDuplicateRequestConsult/")
    Call<Confirm> checkForDuplicateConsultAndMaternity(@Body SendLOAConsultationAndMaternityModel sendLOAConsultationAndMaternityModel);

    @POST("/coordinator/v3/checkForDuplicateRequestBasicTest/")
    Call<Confirm> checkForDuplicateRequestBasicTest(@Body RequestBasicTest sendBasicTest);

    @POST("/coordinator/v3/checkForDuplicateRequestTestProc/")
    Call<Confirm> checkForDuplicateRequestTestProc(@Body BasicTestOrOtherTest sendBasicTestModel);

    // Changed Aug 11, 2017 -- Lawrence Mataga for Sprint 6 part 1 SIT
//    @POST("/coordinator/v2/requestBasicOrOtherTest")
//    Call<BasicTestOrOtherTestReturn> getBasicTestResult(@Body BasicTestOrOtherTest sendBasicTestModel);

    @POST("/coordinator/v3/requestLoaForBasicTest")
    Call<BasicTestOrOtherTestReturn> getBasicTestResult(@Body BasicTestOrOtherTest sendBasicTestModel);

    @POST("/coordinator/v3/requestLoaForProcedures/")
    Call<BasicTestOrOtherTestReturn> getProcedureResult(@Body BasicTestOrOtherTest sendBasicTestModel);

    @POST("/coordinator/v3/requestBasicOrOtherTest")
    Call<BasicTestOrOtherTestReturn> getOtherTestResult(@Body BasicTestOrOtherTest procedure);

    @POST("/coordinator/v3/inquireInpatient/")
    Call<GetInPatientReturn> getInPatientResult(@Body SendInPatientRequest sendInPatientRequest);

//    @GET("/listing/getDiagnosticProceduresList/")
//    Observable<DiagnosticProcedureModel> getProcedureList();

    //original
//    @GET("/listing/getDiagnosisList/")
//    Call<DiagnosisModel> getDiagnosisList();

    @GET("/v2/listing/getDiagnosisList/")
    Call<DiagnosisModel> getDiagnosisList(@Query("last_update_date") String last_update_date);

    @GET("/v2/listing/getListingUpdateDates")
    Call<ListingUpdateReturn> getListingUpdate();

    //original
//    @GET("v2/listing/getTestsAndProceduresListByHosp/")
//    Call<ProcedureModel> getProcList(@Query("hospCode") String hospCode);

    @GET("v2/listing/getTestsAndProceduresListByHosp")
    Call<ProcedureModel> getProcList(@Query("hospCode") String hospCode,
                                     @Query("last_update_date") String last_update_date);

    @POST("/coordinator/v2/lockMember/")
    Call<ResponseBody> setLockAccount(@Query("memberCode") String memberCode);


    @POST("/coordinator/v2/inquireEmergencyRoomNoValidation/")
    Call<GetEr> inquireER(@Body SendEr sendEr);

    @GET("/coordinator/v2/getLoaByHospitalAndMemberCode/?")
    Call<LoaReq> getLoaReq(@Query("hospitalCode") String hospitalCode,
                           @Query("memberCode") String memberCode);


    @POST("/coordinator/logBlockedRequest/")
    Call<ResponseBody> blockedRequestLogger(@Query("memberCode") String memberCode,
                                            @Query("hospitalCode") String hospitalCode,
                                            @Query("appUsername") String appUsername,
                                            @Query("transaction") String transaction,
                                            @Query("deviceID") String deviceID);

    @POST("/coordinator/v2/approveLOA")
    Call<Confirm> confirmLoaConsult(@Query("batchCode") String batchCode);

    @GET("v2/listing/getTestsProceduresByDiagnosisCode/")
    Call<ProcedureFilter> filterProcedures(@Query("diagCode") String diagCode);

    @GET("v2/listing/getTestsOnlyByDiagnosisCode/")
    Call<ProcedureFilter> filterTests(@Query("diagCode") String diagCode);

    @GET("v2/listing/getProceduresOnlyByDiagnosisCode/")
    Call<ProcedureFilter> filterProceduresOnly(@Query("diagCode") String diagCode);

    @GET("/listing/getProceduresByDiagnosisCode/")
    Call<ProcedureFilter> filterProceduresforclinic(@Query("diagCode") String diagCode);

    @GET("/coordinator/v2/getFilteredLoaByMemberCode")
    Call<Loa> getConsultationList(@Query("memberCode") String memberCode,
                                  @Query("filterType") String filterType);

    @GET("/listing/getBasicTests")
    Call<BasicTestReturn> getBasicTests();

    @GET("/v2/listing/getBasicTests")
    Call<BasicTestReturn> getBasicforOutPatient();


    @GET("/v2/updateContactNo")
    Call<UpdatedInfo> updateContact(@Query("appUsername") String appUsername,
                                    @Query("contactNo") String contactNo);

    @Multipart
    @POST("/memberloa/addAttachmentByApprovalNo")
    Call<ResponseBody> sendFile(@Part("file\"; filename=\"image.png\"") RequestBody file,
                                @Part("approvalNo") RequestBody approvalNo);


    @GET("/mace/request/filtered")
    Call<ResponseBody> getLoaReqFiltered(@Query("memberCode") String memberCode,
                                         @Query("serviceType") String serviceType,
                                         @Query("serviceSubType") String serviceSubType,
                                         @Query("status") String status,
                                         @Query("page") String page,
                                         @Query("count") String count);

    @GET("/listing/getOtherServices")
    Call<OtherServices> getOtherServices();

    @GET("/listing/getRoomPlans")
    Call<RoomsAvail> getRoomPlans();

    @GET("/listing/getRoomCategories")
    Call<RoomCategory> getRoomCategory();

    @GET("/coordinator/v2/getInpatientInfo")
    Call<InPatient> getInpatientInfo(@Query("requestCode") String requestCode);

    @POST("/coordinator/v2/updateInpatient/")
    Call<InPatientUpdate> updateInPatient(@Body InPatientUpdate inPatientUpdate);


    // for discharging er
    @POST("/coordinator/v2/dischargeEr/")
    Call<GetEr> sendDischargeEr(@Query("requestId") String requestId,
                                @Query("userName") String userName);

    @POST("/coordinator/v2/dischargeErToInpatient/")
    Call<GetEr> sendDischargeErToInpatient(@Query("requestId") String requestId,
                                           @Query("userName") String userName);

    @POST("/coordinator/v2/dischargeErToOutpatient/")
    Call<GetEr> sendDischargeErToOutpatient(@Query("requestId") String requestId,
                                            @Query("userName") String userName);

    @GET("/v2/listing/getAnnouncements/")
    Call<AnnouncementDataJson> getAnnouncements();

    @GET("/coordinator/v3/getRecentTransactionsForHosp")
    Call<HashMap<String, List<RecentTransactions>>> getRecentTransactionsForHosp(@Query("hospCode") String hospCode,
                                                                                 @Query("memberCode") String memberCode);


    @POST("/listing/getOutpatientHospitalExclusionList/")
    Call<ExclusionModel> getExclusionList();

}


