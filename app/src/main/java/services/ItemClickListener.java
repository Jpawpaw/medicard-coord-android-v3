package services;

import android.view.View;

/**
 * Created by mpx-pawpaw on 11/17/16.
 */

public interface ItemClickListener {

    void onClick(View view , int position);


}
