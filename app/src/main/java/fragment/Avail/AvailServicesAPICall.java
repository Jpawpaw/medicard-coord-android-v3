package fragment.Avail;

import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import model.BasicTestOrOtherTest;
import model.BasicTestOrOtherTestReturn;
import model.BasicTestReturn;
import model.Confirm;
import model.DiagnosisList;
import model.DischargeER;
import model.DoctorModelInsertion;
import model.GetInPatientReturn;
import model.GetEr;
import model.LOAReturn;
import model.RequestBasicTest;
import model.SendEr;
import model.SendInPatientRequest;
import model.SendLOAConsultationAndMaternityModel;
import model.TestsAndProcedures;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import services.AppInterface;
import services.AppService;
import utilities.Constant;
import utilities.SharedPref;

/**
 * COde
 * Created by mpx-pawpaw on 3/30/17.
 */

public class AvailServicesAPICall {

    private static AppInterface appInterface;

    // added by jj for new testing of duplicates

    public static void checkForDuplicateConsultAndMaternity(final String ORIGIN, final AvailInterface callback,
                                                            String problem, String getDoctorCode,
                                                            String hospCode, String getMEMBER_id,
                                                            String username, String phoneID, String contactNo, String email) {

        final String getProblem = problem;

        Log.d("getDoctorCode", getDoctorCode);
        Log.d("getHospitalCode", hospCode);
        Log.d("getMEMBER_ID", getMEMBER_id);
        Log.d("username", username);
        Log.d("phoneID", phoneID);
        Log.d("problem", problem);

        SendLOAConsultationAndMaternityModel send = new SendLOAConsultationAndMaternityModel();
        send.setDoctorCode(getDoctorCode);
        send.setHospitalCode(hospCode);
        send.setMemberCode(getMEMBER_id);
        send.setUsername(username);
        send.setDeviceID(phoneID);
        send.setPrimaryComplaint(problem);
        send.setDiagnosisCode("");
        send.setLocationCode("");
        send.setProcedureAmount(0);
        send.setProcedureCode("");
        send.setProcedureDesc("");
        send.setCoorContact(contactNo);
        send.setCoorEmail(email);
        send.setConsultSubtype("CONSULTATION".equalsIgnoreCase(ORIGIN) ? 1 : 2);


        SharedPref.sendLoaConsult = send;

        Gson gson = new Gson();
        Log.d("SEND CONSULTATION", gson.toJson(send));

        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.checkForDuplicateConsultAndMaternity(send)
                .enqueue(new Callback<Confirm>() {
                    @Override
                    public void onResponse(Call<Confirm> call, Response<Confirm> response) {
                        if (response.body() != null) {
                            if ("200".equals(response.body().getResponseCode())) {
                                //callbackOnSuccess(send) -> Process
//                                callback.onSuccessConfirm(ORIGIN);
                                if (ORIGIN.equals(Constant.CONSULTATION)) {
                                    sendConsultationRequest(ORIGIN, callback, "");
                                } else if (ORIGIN.equals(Constant.MATERNITY)) {
                                    sendMaternityRequest(ORIGIN, callback, "");
                                }
                            } else {
                                if (ORIGIN.equals(Constant.CONSULTATION)) {
                                    callback.sendConsultationDuplicate(response.body(), ORIGIN);
                                } else if (ORIGIN.equals(Constant.MATERNITY)) {
                                    callback.sendMaternityDuplicate(response.body(), ORIGIN);
                                }
                            }

                        } else {
                            callback.onErrorConfirm("no response to server");

                        }
                    }

                    @Override
                    public void onFailure(Call<Confirm> call, Throwable e) {
                        if (e != null) {
                            callback.onErrorConfirm(e.getMessage());
                        } else {
                            callback.onErrorConfirm("");

                        }
                    }
                });

    }


    public static void checkForDuplicateRequestBasictest(final String ORIGIN, final AvailInterface callback, final RequestBasicTest sendBasicTest) {


        Gson gson = new Gson();
        Log.d("SEND DUPLICATE", gson.toJson(sendBasicTest));

        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.checkForDuplicateRequestBasicTest(sendBasicTest)
                .enqueue(new Callback<Confirm>() {
                    @Override
                    public void onResponse(Call<Confirm> call, Response<Confirm> response) {
                        if (response.body() != null) {
                            if ("200".equals(response.body().getResponseCode())) {
                                sendBasicTest(ORIGIN, callback, "");
                            } else {
                                callback.onBasicTestDuplicate(response.body(), ORIGIN);
                            }

                        } else {
                            callback.onErrorConfirm("no response to server");

                        }
                    }

                    @Override
                    public void onFailure(Call<Confirm> call, Throwable e) {
                        if (e != null) {
                            callback.onErrorConfirm(e.getMessage());
                            Log.e("e.getMessage():", e.getMessage());
                        } else {
                            callback.onErrorConfirm("");

                        }
                    }
                });

    }

    public static void checkForDuplicateRequestTestProc(final String ORIGIN, final AvailInterface callback, final BasicTestOrOtherTest basicTestOrOtherTest) {

        Gson gson = new Gson();
        Log.d("SEND DUPLICATE", gson.toJson(basicTestOrOtherTest));

        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.checkForDuplicateRequestTestProc(basicTestOrOtherTest)
                .enqueue(new Callback<Confirm>() {
                    @Override
                    public void onResponse(Call<Confirm> call, Response<Confirm> response) {
                        if (response.body() != null) {
                            if ("200".equals(response.body().getResponseCode())) {
                                if (Constant.PROCEDURES.equalsIgnoreCase(ORIGIN)) {
                                    sendProcedures(ORIGIN, callback, "");
                                } else if (Constant.OTHER_TEST.equalsIgnoreCase(ORIGIN)) {
                                    sendOtherTestRequest(ORIGIN, callback, "");
                                } else if (Constant.BASIC_TEST.equalsIgnoreCase(ORIGIN)) {
                                    sendBasicTest(ORIGIN, callback, "");
                                }

                            } else {
                                if (Constant.PROCEDURES.equalsIgnoreCase(ORIGIN)) {
                                    callback.onProceduresDuplicate(response.body(), ORIGIN);
                                } else if (Constant.OTHER_TEST.equalsIgnoreCase(ORIGIN)) {
                                    callback.onOtherTestDuplicate(response.body(), ORIGIN);
                                } else if (Constant.BASIC_TEST.equalsIgnoreCase(ORIGIN)) {
                                    callback.onBasicTestDuplicate(response.body(), ORIGIN);
                                }

                            }
                        } else {
                            callback.onErrorConfirm("no response to server");

                        }
                    }

                    @Override
                    public void onFailure(Call<Confirm> call, Throwable e) {
                        if (e != null) {
                            callback.onErrorConfirm(e.getMessage());
                            Log.e("e.getMessage():", e.getMessage());
                        } else {
                            callback.onErrorConfirm("");

                        }
                    }
                });

    }

    public static void sendConfirmConsult(String batchCode, final AvailInterface callback, final String ORIGIN) {

        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.confirmLoaConsult(batchCode)
                .enqueue(new Callback<Confirm>() {
                    @Override
                    public void onResponse(Call<Confirm> call, Response<Confirm> response) {
                        if (response != null) {
                            callback.onSuccessConfirm(ORIGIN);
                        } else {
                            callback.onErrorConfirm("no response to server");

                        }
                    }

                    @Override
                    public void onFailure(Call<Confirm> call, Throwable e) {
                        if (e != null) {
                            callback.onErrorConfirm(e.getMessage());
                        } else {
                            callback.onErrorConfirm("");

                        }
                    }
                });

    }

    public static void sendConfirmBasic(String batchCode, final AvailInterface callback, final String ORIGIN) {

        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.confirmLoaConsult(batchCode)
                .enqueue(new Callback<Confirm>() {
                    @Override
                    public void onResponse(Call<Confirm> call, Response<Confirm> response) {
                        if (response != null) {
                            callback.onSuccessConfirmBasic(ORIGIN);
                        } else {
                            callback.onErrorConfirm("no response to server");

                        }
                    }

                    @Override
                    public void onFailure(Call<Confirm> call, Throwable e) {
                        if (e != null) {
                            callback.onErrorConfirm(e.getMessage());
                        } else {
                            callback.onErrorConfirm("");

                        }
                    }
                });

    }

    public static void sendConsultationRequest(final String ORIGIN, final AvailInterface callback, String duplicateTag) {


//        final String getProblem = problem;
//
//        Log.d("getDoctorCode", getDoctorCode);
//        Log.d("getHospitalCode", hospCode);
//        Log.d("getMEMBER_ID", getMEMBER_id);
//        Log.d("username", username);
//        Log.d("phoneID", phoneID);
//        Log.d("problem", problem);
        SendLOAConsultationAndMaternityModel sLoa = SharedPref.sendLoaConsult;
        System.out.println("CONSULTATION ::::");
//        sLoa.setConsultSubtype("CONSULTATION".equals(ORIGIN)? 1:2);
        sLoa.setDuplicateTag(duplicateTag);
//        send.setDoctorCode(getDoctorCode);
//        send.setHospitalCode(hospCode);
//        send.setMemberCode(getMEMBER_id);
//        send.setUsername(username);
//        send.setDeviceID(phoneID);
//        send.setPrimaryComplaint(problem);
//
//        send.setDiagnosisCode("");
//        send.setLocationCode("");
//        send.setProcedureAmount(0);
//        send.setProcedureCode("");
//        send.setProcedureDesc("");
//
        Gson gson = new Gson();
        Log.d("SEND", gson.toJson(sLoa));


        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.getConsultationResult(sLoa)
                .enqueue(new Callback<LOAReturn>() {
                    @Override
                    public void onResponse(Call<LOAReturn> call, Response<LOAReturn> response) {
                        if (response.body() != null) {
                            callback.sendConsultationSuccess(response.body(), ORIGIN);
                        } else {
                            callback.sendConsultationError("no response to server");
                        }

                    }

                    @Override
                    public void onFailure(Call<LOAReturn> call, Throwable t) {
                        callback.sendConsultationError(t.getMessage());
                    }
                });
    }

    public static void sendMaternityRequest(final String ORIGIN, final AvailInterface callback, String duplicateTag) {


//        final String getProblem = problem;
//
//        Log.d("getDoctorCode", getDoctorCode);
//        Log.d("getHospitalCode", hospCode);
//        Log.d("getMEMBER_ID", getMEMBER_id);
//        Log.d("username", username);
//        Log.d("phoneID", phoneID);
//        Log.d("problem", problem);
        SendLOAConsultationAndMaternityModel sLoa = SharedPref.sendLoaConsult;
        System.out.println("MATERNITY ::::");
//        sLoa.setConsultSubtype("CONSULTATION".equals(ORIGIN)? 1:2);
        sLoa.setDuplicateTag(duplicateTag);
//        send.setDoctorCode(getDoctorCode);
//        send.setHospitalCode(hospCode);
//        send.setMemberCode(getMEMBER_id);
//        send.setUsername(username);
//        send.setDeviceID(phoneID);
//        send.setPrimaryComplaint(problem);
//
//        send.setDiagnosisCode("");
//        send.setLocationCode("");
//        send.setProcedureAmount(0);
//        send.setProcedureCode("");
//        send.setProcedureDesc("");
//
        Gson gson = new Gson();
        Log.d("SEND", gson.toJson(sLoa));


        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.getMaternityResult(sLoa)
                .enqueue(new Callback<LOAReturn>() {
                    @Override
                    public void onResponse(Call<LOAReturn> call, Response<LOAReturn> response) {
                        if (response.body() != null) {
                            callback.sendConsultationSuccess(response.body(), ORIGIN);
                        } else {
                            callback.sendConsultationError("no response to server");
                        }

                    }

                    @Override
                    public void onFailure(Call<LOAReturn> call, Throwable e) {
                        callback.sendConsultationError(e.getMessage());

                    }
                });


    }

    public static void sendBasicTest(final String ORIGIN, final AvailInterface callback, String repeat) {
        BasicTestOrOtherTest basicTestOrOtherTest = SharedPref.TestOrOtherTest;
        System.out.println("SEND BASIC :::");
        basicTestOrOtherTest.setDuplicateTag(repeat);
        Gson gson = new Gson();
        Log.d("OTHER_TEST", gson.toJson(basicTestOrOtherTest));
        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.getBasicTestResult(basicTestOrOtherTest)
                .enqueue(new Callback<BasicTestOrOtherTestReturn>() {
                    @Override
                    public void onResponse(Call<BasicTestOrOtherTestReturn> call, Response<BasicTestOrOtherTestReturn> response) {
                        if (response.body() != null) {
                            callback.onBasicTestSuccess(response.body(), ORIGIN);
                        } else {
                            callback.sendConsultationError("no response to server");
                        }
                    }

                    @Override
                    public void onFailure(Call<BasicTestOrOtherTestReturn> call, Throwable t) {
                        callback.sendConsultationError(t.getMessage());
                    }
                });

    }

    public static void sendOtherTestRequest(final String ORIGIN, final AvailInterface callback, String repeat) {

        BasicTestOrOtherTest basicTestOrOtherTest = SharedPref.TestOrOtherTest;
        System.out.println("SEND OTHER TEST :::");
        basicTestOrOtherTest.setDuplicateTag(repeat);
        Gson gson = new Gson();
        Log.d("OTHER_TEST", gson.toJson(basicTestOrOtherTest));
        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.getOtherTestResult(basicTestOrOtherTest)
                .enqueue(new Callback<BasicTestOrOtherTestReturn>() {
                    @Override
                    public void onResponse(Call<BasicTestOrOtherTestReturn> call, Response<BasicTestOrOtherTestReturn> response) {
                        if (response.body() != null) {
                            callback.onOtherTestSuccess(response.body(), ORIGIN);
                        } else {
                            callback.sendConsultationError("no response to server");
                        }
                    }

                    @Override
                    public void onFailure(Call<BasicTestOrOtherTestReturn> call, Throwable t) {
                        callback.sendConsultationError(t.getMessage());
                    }
                });
    }

    public static void sendInPatient(final AvailInterface callback,
                                     String appUsername,
                                     String memberCode,
                                     String hospitalCode,
                                     final ArrayList<DoctorModelInsertion> doctorCodes,
                                     final ArrayList<DiagnosisList> diagCodes,
                                     final ArrayList<TestsAndProcedures> procCodes,
                                     final String roomRate,
                                     final String roomNo,
                                     final String dateTimeAdmitted,
                                     String coorContact,
                                     String coorEmail,
                                     String searchType,
                                     final String category,
                                     String roomType,
                                     String deviceId)
    {


        SendInPatientRequest send = new SendInPatientRequest();
        send.setAppUsername(appUsername);
        send.setMemberCode(memberCode);
        send.setHospitalCode(hospitalCode);

        List<String> docCodes = new ArrayList<>();
        for (DoctorModelInsertion doctorCode : doctorCodes) {
            docCodes.add(doctorCode.getDoctorCode());
        }

        send.setDoctorCodes(docCodes);


        List<String> diagCode1 = new ArrayList<>();
        for (DiagnosisList diagCode2 : diagCodes) {
            diagCode1.add(diagCode2.getDiagCode());
        }

        send.setDiagCodes(diagCode1);

        List<String> procCode1 = new ArrayList<>();
        for (TestsAndProcedures procCode2 : procCodes) {
            procCode1.add(procCode2.getProcedureCode());
        }

        send.setProcCodes(procCode1);

        send.setRoomRate(roomRate);
        send.setRoomNo(roomNo);
        send.setDateTimeAdmitted(dateTimeAdmitted);
        send.setCoorContact(coorContact);
        send.setCoorEmail(coorEmail);
        send.setSearchType(searchType);
        send.setCategory(category);
        send.setRoomType(roomType);
        send.setDeviceId(deviceId);
        send.setRequestOrigin(Constant.REQUEST_ORIGIN);


        Gson gson = new Gson();
        Log.d("INPATIENT", gson.toJson(send));
        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.getInPatientResult(send)
                .enqueue(new Callback<GetInPatientReturn>() {
                    @Override
                    public void onResponse(Call<GetInPatientReturn> call, Response<GetInPatientReturn> response) {
                        try {
                            if (response != null) {
                                if(response.body().getResponseCode().equalsIgnoreCase("200")){
                                    callback.onInPatientSuccess(response.body(), doctorCodes, diagCodes, procCodes, roomRate,
                                            roomNo, dateTimeAdmitted, category);
                                }else {
                                    callback.sendConsultationError("no response to server");
                                }

                            } else {
                                callback.sendConsultationError("no response to server");
                            }
                        } catch (Exception e) {
                            callback.sendConsultationError("");
                        }
                    }

                    @Override
                    public void onFailure(Call<GetInPatientReturn> call, Throwable t) {
                        try {
                            if (t != null) {
                                callback.sendConsultationError(t.getMessage());
                            } else {
                                callback.sendConsultationError("");

                            }
                        } catch (Exception e) {
                            callback.sendConsultationError("");
                        }
                    }
                });


        //        private String appUsername;
//        private String memberCode;
//        private String hospitalCode;
//        private List<String> doctorCodes;
//        private List<String> diagCodes;
//        private List<String> procCodes;
//        private String roomRate;
//        private String roomNo;
//        private String dateTimeAdmitted;
//        private String coorContact;
//        private String coorEmail;
//        private String searchType;
//        private String category;
//        private String roomType;
//        private String deviceId;
//        sendInPatientModel.setProcedureCode("");
//        sendInPatientModel.setDoctorCode(getDoctorCode);
//        sendInPatientModel.setStatus("");
//        sendInPatientModel.setUpdatedDate("");
//        sendInPatientModel.setSearchType(searchType);
//        sendInPatientModel.setDiagnosisCode(diagnosis);
//        sendInPatientModel.setUpdatedBy("");
//        sendInPatientModel.setCompanyCode("");
//        sendInPatientModel.setId("");
//        sendInPatientModel.setUsername(username);
//        sendInPatientModel.setCategory(category);
//        sendInPatientModel.setRoomNumber(room_no);
//        sendInPatientModel.setDiagnosisDesc(diagDesc);
//        sendInPatientModel.setRoomType(room_plan);
//        sendInPatientModel.setRunningBill("");
//        sendInPatientModel.setHospitalName(hospName);
//        sendInPatientModel.setRoomPrice(room_price);
//        sendInPatientModel.setProcedureDesc("");
//        sendInPatientModel.setHospitalCode(hospCode);
//        sendInPatientModel.setMemberName(full_name);
//        sendInPatientModel.setDoctorName(doctorName);
//        sendInPatientModel.setDateAdmitted(dateAdmitted);
//        sendInPatientModel.setDeviceId(phone_id);
//        sendInPatientModel.setMemberCode(getMEMBER_id);
//        sendInPatientModel.setCoorContact(contactNo);
//        sendInPatientModel.setCoorEmail(email);




    }

    private static List<String> SetListDataProcCodes(ArrayList<TestsAndProcedures> procCodes) {
        List<String> data = new ArrayList<>();

        for(int x = 0;x >procCodes.size() ;x++){
            data.add(procCodes.get(x).getProcedureCode());
        }
        return data;
    }

    private static List<String> SetListDataDiagCodes(ArrayList<DiagnosisList> diagCodes) {
        List<String> data = new ArrayList<>();

        for(int x = 0;x >diagCodes.size() ;x++){
            data.add(diagCodes.get(x).getDiagCode());
        }
        return data;
    }

    public List<String> setListDataDocCodes(ArrayList<DoctorModelInsertion> doctorCodes) {
        List<String> data = new ArrayList<>();

        for(int x = 0;x >doctorCodes.size() ;x++){
            data.add(doctorCodes.get(x).getDoctorCode());
        }
        return data;
    }








    protected static void sendDischargeEr(final AvailInterface callback,
                                          String id,
                                          final String username) {
        DischargeER dischargeER = new DischargeER();
        dischargeER.setId(id);
        dischargeER.setUsername(username);

        Gson gson = new Gson();
        Log.d("DISCHARGE ER", gson.toJson(dischargeER));
        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.sendDischargeEr(id, username)
                .enqueue(new Callback<GetEr>() {
                    @Override
                    public void onResponse(Call<GetEr> call, Response<GetEr> response) {

                        try {
                            if (response != null) {
                                callback.dischargeERSuccess();
                                //
                            } else {
                                callback.sendConsultationError("");
                            }
                        } catch (Exception e) {
                            callback.sendConsultationError("");
                        }

                    }

                    @Override
                    public void onFailure(Call<GetEr> call, Throwable t) {
                        try {
                            if (t != null) {
                                callback.sendConsultationError(t.getMessage());
                            } else {
                                callback.sendConsultationError("");

                            }
                        } catch (Exception e) {
                            callback.sendConsultationError("");
                        }
                    }
                });


    }


    protected static void sendER(final AvailInterface callback,
                                 final String dateAdmitted,
                                 final String er_reason,
                                 String hospCode,
                                 String getMEMBER_id,
                                 String full_name,
                                 String username,
                                 String contactNo,
                                 String email) {


        SendEr sendEr = new SendEr();
        sendEr.setDateAdmitted(dateAdmitted);
        sendEr.setErReason(er_reason);
        sendEr.setHospitalCode(hospCode);
        sendEr.setMemberCode(getMEMBER_id);
        sendEr.setMemberName(full_name);
        sendEr.setUsername(username);
        sendEr.setCoorContact(contactNo);
        sendEr.setCoorEmail(email);


        Gson gson = new Gson();
        Log.d("ER", gson.toJson(sendEr));
        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.inquireER(sendEr)
                .enqueue(new Callback<GetEr>() {
                    @Override
                    public void onResponse(Call<GetEr> call, Response<GetEr> response) {
                        try {
                            if (response != null) {
                                callback.onERSuccess(response.body(), dateAdmitted, er_reason);
                            } else {
                                callback.sendConsultationError("");
                            }
                        } catch (Exception e) {
                            callback.sendConsultationError("");
                        }
                    }

                    @Override
                    public void onFailure(Call<GetEr> call, Throwable t) {
                        try {
                            if (t != null) {
                                callback.sendConsultationError(t.getMessage());
                            } else {
                                callback.sendConsultationError("");

                            }
                        } catch (Exception e) {
                            callback.sendConsultationError("");
                        }
                    }
                });

    }


    public static void sendProcedures(final String ORIGIN, final AvailInterface callback, String repeat) {

        BasicTestOrOtherTest basicTestOrOtherTest = SharedPref.TestOrOtherTest;
        System.out.println("SEND PROCEDURE ::::");
        basicTestOrOtherTest.setDuplicateTag(repeat);

        Gson gson = new Gson();
        Log.d("SEND_PROCEDURE", gson.toJson(basicTestOrOtherTest));

        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.getProcedureResult(basicTestOrOtherTest)

                .enqueue(new Callback<BasicTestOrOtherTestReturn>() {
                    @Override
                    public void onResponse(Call<BasicTestOrOtherTestReturn> call, Response<BasicTestOrOtherTestReturn> response) {
                        if (response != null) {
                            callback.onProceduresSuccess(response.body(), ORIGIN);
                        } else {
                            callback.onProceduresError("");
                        }
                    }

                    @Override
                    public void onFailure(Call<BasicTestOrOtherTestReturn> call, Throwable t) {
                        if (t != null) {
                            callback.onProceduresError(t.getMessage());
                        } else {
                            callback.onProceduresError("");
                        }
                    }
                });

    }


//    public static void getBasicTest(final AvailInterface callback) {
//
//
//        AppInterface appInterface;
//        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
//        appInterface.getBasicTests()
//                .enqueue(new Callback<BasicTestReturn>() {
//                    @Override
//                    public void onResponse(Call<BasicTestReturn> call, Response<BasicTestReturn> response) {
//                        if (response != null) {
//                            callback.onBasicTestsSuccess(response.body());
//                        } else {
//                            callback.onBasicTestError("");
//
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<BasicTestReturn> call, Throwable t) {
//                        if (t != null) {
//                            callback.onBasicTestError(t.getMessage());
//                        } else {
//                            callback.onBasicTestError("");
//
//                        }
//                    }
//                });
//
//    }

    public static void getBasicforOutPatient(final AvailInterface callback) {
        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.getBasicforOutPatient()
                .enqueue(new Callback<BasicTestReturn>() {
                    @Override
                    public void onResponse(Call<BasicTestReturn> call, Response<BasicTestReturn> response) {
                        if (response != null) {
                            callback.onBasicTestsSuccess1(response.body());
                        } else {
                            callback.onBasicTestError("");

                        }
                    }

                    @Override
                    public void onFailure(Call<BasicTestReturn> call, Throwable t) {
                        if (t != null) {
                            callback.onBasicTestError(t.getMessage());
                        } else {
                            callback.onBasicTestError("");

                        }
                    }
                });

    }

    public static void sendEr(final AvailInterface callback) {
        Callback er;
        er = new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                callback.setErUi2();
            }

            @Override
            public void onFailure(Call call, Throwable t) {

            }
        };
    }
}
