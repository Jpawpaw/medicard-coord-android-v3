package fragment.Avail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import coord.medicard.com.medicardcoordinator.R;

/**
 * Created by IPC_Server on 8/9/2017.
 */

public class AvailBasicTest extends Fragment {


    public AvailBasicTest(){

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_basictest, container, false);
        return view;
    }


    @Nullable
    @Override
    public View getView() {
        return super.getView();
    }
}
