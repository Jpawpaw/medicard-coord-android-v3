package fragment.Avail;

import java.util.ArrayList;

import model.BasicTestOrOtherTestReturn;
import model.BasicTestReturn;
import model.Confirm;
import model.DiagnosisList;
import model.DoctorModelInsertion;
import model.GetInPatientReturn;
import model.GetEr;
import model.Hospital;
import model.LOAReturn;
import model.LoaList;
import model.Rooms;
import model.TestsAndProcedures;

/**
 * Created by mpx-pawpaw on 4/12/17.
 */


public interface AvailInterface {

    void onBasicTestsSuccess1(BasicTestReturn body);

    void onSuccessConfirm(String ORIGIN);

    void onSuccessConfirmBasic(String ORIGIN);

    void onErrorConfirm(String message);

    void sendConsultationSuccess(LOAReturn LOAReturn, String ORIGIN);

    void sendConsultationDuplicate(Confirm confirm, String ORIGIN);

    void sendMaternityDuplicate(Confirm confirm, String ORIGIN);

    void sendConsultationError(String message);

    void sendProcedureError(String message);

    void sendClinicProcedureError(String message);

    void dischargeERSuccess();

    void setInpatientUI();

    void setOutpatientUI();

    void setErUi();

    void setErUi2();

    void onDoctorNotAvailable();

    void onProblemNotAvailable();

    void sendData(int i);

    void sendConsultation();

    void dischargeER();

    void dischargeERToInpatient();

    void dischargeERToOutpatient();

    void sendMaternity();

    void sendBasic();

    void sendOtherTest();

    void sendInPatient();

    void sendER();

    void onProcedureNotAvailable();

    void onDiagnosisNotAvailable();

//    void onBasicTestSuccess(BasicTestOrOtherTestReturn getBasicTestReturn, String doctor, String ORIGIN);

    void onBasicTestSuccess(BasicTestOrOtherTestReturn getOtherTestReturnModel, String ORIGIN) ;

    void onBasicTestDuplicate(Confirm confirm, String ORIGIN);

    void onProceduresDuplicate(Confirm confirm, String ORIGIN);

    void onOtherTestDuplicate(Confirm confirm, String ORIGIN);

    void onOtherTestSuccess(BasicTestOrOtherTestReturn getOtherTestReturnModel, String ORIGIN);

    void onInPatientSuccess(GetInPatientReturn body, ArrayList<DoctorModelInsertion> doctorCodes, ArrayList<DiagnosisList> diagCodes, ArrayList<TestsAndProcedures> procCodes, String roomRate, String roomNo, String dateTimeAdmitted, String category);


    void onERSuccess(GetEr GetEr, String dateAdmitted, String er_reason);

    void onDischargeErSuccess(String id, String username);


    void onAdmittedNotAvailable();


    void onPrimaryDiagNotAvailable();

    void sendProcedures();


    void onProceduresError(String message);

    void basicTestFillUpFields(LoaList loaFillUpMOdel);

    void isOtherProcedureFillUpFields(LoaList loaFillUpMOdel);

    void isProcedureFillUpFields(LoaList loaFillUpMOdel);

    void onBasicTestsSuccess(BasicTestOrOtherTestReturn getOtherTestReturnModel, String ORIGIN);

    void onBasicTestError(String message);

    void setConsultationUI();

    void setMaternityUI();

    void setBasicTestUI();

    void setOtherTestUI();

    void deleteData();

    void setDoctorSelected(String doctorCreds, String member_id, String hospitalCode, String doctorCode, String doctorName, String hospitalName);

    void setProcedureSelected(ArrayList<TestsAndProcedures> arrayProc);

    void updateDiagnosis(DiagnosisList diagnosisList);

    void updatePrimaryProcedure(ArrayList<TestsAndProcedures> arrayProc);

    void setPrimaryDiagnosis(DiagnosisList diagnosisList);

    void setOtherProcedures(ArrayList<TestsAndProcedures> arrayProc);

    void setOtherDiagnosis(DiagnosisList diagnosisList);

    void setOtherDiagnosisSecond(DiagnosisList diagnosisListSecond);

    void setProceduresUI();

    void setSelectedPreMadeConsultationData(LoaList loa);

    void onProceduresSuccess(BasicTestOrOtherTestReturn body, String ORIGIN);

    void getTime(String origin);

    void setRoomPlans(Rooms rooms);

    void setRoomCategory(String origin);

    void noReason();

    void setOtherProceduresSecond(ArrayList<TestsAndProcedures> arrayProc);

    void setUnfilteredHosp(Hospital hospital);

    void setInPatientDoctorSelected(ArrayList<DoctorModelInsertion> doctorSelected);

    void setInPatientDiagnosisSelected(ArrayList<DiagnosisList> diagnosisLists);

    void setInPatientProcedureSelected(ArrayList<TestsAndProcedures> procedureModelInsertions);

    void onAdmittedTimeNotAvailable();
}