package fragment.Avail;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;

import coord.medicard.com.medicardcoordinator.R;
import model.BasicTestOrOtherTest;
import model.BasicTests;
import model.DiagnosisList;

import model.DiagnosisProcedures;
import model.DiagnosisTest;
import model.DoctorModelInsertion;
import model.LoaList;
import model.Procedure;
import model.ProcedureList;
import model.ProceduresListLocal;


import model.RequestBasicTest;
import model.TestsAndProcedures;
import utilities.APIMultipleCalls.ClinicProceduresCall;
import utilities.Constant;
import utilities.DataBaseHandler;
import utilities.DateConverter;
import utilities.APIMultipleCalls.FilteredProcedureCall;
import utilities.SharedPref;

/**
 * Created by mpx-pawpaw on 4/12/17.
 */

public class AvailRetrieve {
    private String in_patient = "IN_PATIENT";
    private String out_patient = "OUT_PATIENT";
    private String e_r = "ER";

    private Context context;
    private AvailInterface callback;


    public AvailRetrieve(Context context, AvailInterface callback) {
        this.callback = callback;
        this.context = context;
    }


    public String getNotNull(String classCategory) {

        try {
            if (classCategory != null) {
                if (classCategory.equals("null")) {
                    return "";
                } else {
                    return classCategory;
                }
            } else {
                return "";
            }
        } catch (Exception e) {
            return "";
        }
    }

    public void setDiagnosisAvailability(ArrayList<DiagnosisList> arrayListDiagnosis,
                                         TextView tv_diagnosis, RecyclerView rv_diagnosis) {


        if (arrayListDiagnosis.size() == 0) {
            tv_diagnosis.setVisibility(View.VISIBLE);
            rv_diagnosis.setVisibility(View.GONE);
        } else {
            tv_diagnosis.setVisibility(View.GONE);
            rv_diagnosis.setVisibility(View.VISIBLE);
        }

    }

    public void setProceduresAvailability(ArrayList<TestsAndProcedures> arrayProcedure,
                                          RelativeLayout rv_noProcAvailable, RecyclerView rv_procedures
            , TextView tv_total_price, TextView addMore) {

        if (arrayProcedure.size() == 0) {
            rv_noProcAvailable.setVisibility(View.VISIBLE);
            rv_procedures.setVisibility(View.GONE);
            addMore.setVisibility(View.GONE);
        } else {
            rv_noProcAvailable.setVisibility(View.GONE);
            rv_procedures.setVisibility(View.VISIBLE);
            addMore.setVisibility(View.VISIBLE);
        }
        tv_total_price.setText(setPrice(arrayProcedure));
    }


    public String setPrice(ArrayList<TestsAndProcedures> proceduresListLocals) {


        double price = 0;

        if (proceduresListLocals.size() == 0) {
            return "Please select Test";
        } else {

            for (int x = 0; x < proceduresListLocals.size(); x++) {
                try {
                    price = price + Double.parseDouble(proceduresListLocals.get(x).getProcedureAmount());
                } catch (NumberFormatException e) {
                    price = price + Double.parseDouble(String.valueOf(proceduresListLocals.get(x).getProcedureAmount()));
                }

            }


            DecimalFormat formatter = new DecimalFormat("#,###.##");
            String formatString = "";


            Log.d("PRICE", price + "");
            return formatter.format(price) + "";

        }


    }

    public String setBasicTestPrice(ArrayList<BasicTests> proceduresListLocals) {


        double price = 0;

        if (proceduresListLocals.size() == 0) {
            return "Please select Test";
        } else {

            for (int x = 0; x < proceduresListLocals.size(); x++) {

                price = price + Double.parseDouble(proceduresListLocals.get(x).getProcedureAmount());
            }


            DecimalFormat formatter = new DecimalFormat("#,###.##");
            String formatString = "";


            Log.d("PRICE", price + "");
            return formatter.format(price) + "";

        }


    }


    public void setCheckBox(CheckBox cb_informed, Button btn_proceed) {

        if (cb_informed.isChecked()) {
            btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
            btn_proceed.setEnabled(true);
            btn_proceed.setClickable(true);
        } else {
            btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.gray));
            btn_proceed.setEnabled(false);
            btn_proceed.setClickable(false);
        }
    }

    public void setUI() {
        if (SharedPref.getStringValue(SharedPref.USER, SharedPref.INPATIENT, context).equals(in_patient)) {
            callback.setInpatientUI();
        } else if (SharedPref.getStringValue(SharedPref.USER, SharedPref.INPATIENT, context).equals(out_patient)) {
            callback.setOutpatientUI();
        } else if (SharedPref.getStringValue(SharedPref.USER, SharedPref.INPATIENT, context).equals(e_r)) {
            callback.setErUi();
        } else {
            callback.setErUi2();
        }
    }

    public void setUnCheckBox(CheckBox cb_informed, Button btn_proceed) {

        if (cb_informed.isChecked()) {
            btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
            btn_proceed.setEnabled(true);
            btn_proceed.setClickable(true);
        } else {
            btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.gray));
            btn_proceed.setEnabled(false);
            btn_proceed.setClickable(false);
        }
    }


    public boolean testField(TextView tv_doctor) {
        return tv_doctor.getText().toString().trim().equals("");
    }


    public void testConsultation(TextView tv_doctor, TextView et_problem, int i) {

        if (testField(tv_doctor)) {
            callback.onDoctorNotAvailable();
        } else {
            if (testField(et_problem))
                callback.onProblemNotAvailable();
            else
                callback.sendData(i);

        }

    }


    public void showDialog(final int position) {

        final Dialog dialog = new Dialog(context);
        final Button btn_proceed, btn_cancel, btn_dischargeER, btn_dischargeERToInPatient, btn_dischargeERToOutPatient;
        TextView tv_title;

        dialog.setContentView(R.layout.dialog_confirmloa);
        btn_proceed = (Button) dialog.findViewById(R.id.btn_proceed);
        btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        btn_dischargeER = (Button) dialog.findViewById(R.id.btn_dischargeER);
        btn_dischargeERToInPatient = (Button) dialog.findViewById(R.id.btn_dischargeERToInPatient);
        btn_dischargeERToOutPatient = (Button) dialog.findViewById(R.id.btn_dischargeERToOutPatient);
        tv_title = (TextView) dialog.findViewById(R.id.tv_title);


        if (position == 4) {
            tv_title.setText("Submit Admission details");
        } else if (position == 6) {
            tv_title.setText("Submit Request for ER");
        } else
            tv_title.setText("Submit Request for Out Patient");

        btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (position == 0) {
                    callback.sendConsultation();
                } else if (position == 1) {
                    callback.sendMaternity();
                } else if (position == 2) {
                    callback.sendBasic();
                } else if (position == 3) {
                    callback.sendOtherTest();
                } else if (position == 4) {
                    callback.sendInPatient();
                } else if (position == 5) {
                    callback.sendProcedures();
                } else if (position == 6) {
                    callback.sendER();
                }

            }
        });

//        btn_dischargeER.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//
//                callback.dischargeER();
//
//            }
//
//        });

//        btn_dischargeERToInPatient.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//
//                callback.dischargeER();
//
//            }
//
//        });
//
//        btn_dischargeERToOutPatient.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//
//                callback.dischargeER();
//
//            }
//
//        });


        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });


        dialog.show();

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        dialog.getWindow().setLayout((4 * width) / 7, Toolbar.LayoutParams.WRAP_CONTENT);
    }

    public void testProcedure(String ORIGIN, TextView tv_doctor, TextView tv_dianosis) {

        if (tv_doctor.getText().toString().trim().equals("")) {
            callback.onDoctorNotAvailable();
        } else if (tv_dianosis.getText().toString().trim().equals("")) {
            callback.onPrimaryDiagNotAvailable();
        } else {
            if (ORIGIN.equals(Constant.FROM_PROCEDURES)) {
                showDialog(5);
            } else {
                showDialog(3);
            }
        }


    }

    public void testInPatient(ArrayList<DoctorModelInsertion> getAllSelectedDoctor, ArrayList<DiagnosisList> getAllSelectedDiagnosis, TextView tv_admitted, TextView tv_admitted_time) {


        String admitted = tv_admitted.getText().toString().trim();
        String time = tv_admitted_time.getText().toString().trim();

        if (getAllSelectedDoctor.size() == 0 || getAllSelectedDiagnosis.size() == 0 || admitted.equals("") || time.equals("")) {
            if (getAllSelectedDoctor.size() == 0)
                callback.onDoctorNotAvailable();
            else if (getAllSelectedDiagnosis.size() == 0)
                callback.onDiagnosisNotAvailable();
            else if (admitted.equals(""))
                callback.onAdmittedNotAvailable();
            else
                callback.onAdmittedTimeNotAvailable();
        } else {
            showDialog(4);
        }
    }

    public void testER(TextView tv_admitted, EditText er_reason) {

        String reason = er_reason.getText().toString().trim();
        String admitted = tv_admitted.getText().toString().trim();
        if (reason.equals("") || admitted.equals("")) {
            if (reason.equals(""))
                callback.noReason();
            else
                callback.onAdmittedNotAvailable();
        } else {
            showDialog(6);
        }
    }


    protected void setInputFieldsGone(boolean b, CardView ll_setInputFields) {

        if (b) {
            ll_setInputFields.setVisibility(View.GONE);
        } else
            ll_setInputFields.setVisibility(View.VISIBLE);
    }

    protected void setOtherProcedurePanelGone(FrameLayout fr_otherProc, boolean b) {

        if (b)
            fr_otherProc.setVisibility(View.GONE);
        else
            fr_otherProc.setVisibility(View.VISIBLE);


    }

    public String testDate(String tv_admitted) {

        return tv_admitted.equals("") ? "" : DateConverter.convertDatetoyyyy_MM_dd(tv_admitted);
    }


    public void loadProcedureBasedonDiagnosis(String ORIGIN, ProgressBar pb_primary_diag, TextView tv_primary_diag) {
        pb_primary_diag.setVisibility(View.VISIBLE);
        tv_primary_diag.setText(context.getString(R.string.loading_proc));

    }

    public void loadProcedureBasedonDiagnosis(FilteredProcedureCall.FilterProcedureCallback callback, String ORIGIN, String diagCode, ProgressBar pb_primary_diag, TextView tv_primary_diag) {


        pb_primary_diag.setVisibility(View.VISIBLE);
        tv_primary_diag.setText(context.getString(R.string.loading_proc));
        System.out.println(ORIGIN);
        if (ORIGIN.equals(Constant.FROM_PRIMARY_PROC_2))//Procedures
            FilteredProcedureCall.getProceduresOnlyFilter(callback, diagCode, ORIGIN);
        else
            FilteredProcedureCall.getTestsFilter(callback, diagCode, ORIGIN);


    }

    public void loadClinicProcedureBasedonDiagnosis(ClinicProceduresCall.FilterClinicProcedureCallback callback, String ORIGIN, String diagCode, ProgressBar pb_primary_diag, TextView tv_primary_diag) {


        pb_primary_diag.setVisibility(View.VISIBLE);
        tv_primary_diag.setText(context.getString(R.string.loading_proc));
        ClinicProceduresCall.getProcedureClinicFilter(callback, diagCode, ORIGIN);


    }

    public void setLoadingPrimaryGone(TextView tv_primary_diag, ProgressBar pb_primary_diag, boolean b) {


        if (b) {
            tv_primary_diag.setText(context.getString(R.string.add_procedure));
            pb_primary_diag.setVisibility(View.GONE);
        } else {
            tv_primary_diag.setText("There is a problem retrieving procedures.");
            pb_primary_diag.setVisibility(View.GONE);
        }

    }

    public void setLoadingPrimaryGone2(TextView tv_primary_diag_2, ProgressBar pb_primary_diag_2, boolean b) {


        if (b) {
            tv_primary_diag_2.setText("Add Clinic Procedure");
            pb_primary_diag_2.setVisibility(View.GONE);
        } else {
            tv_primary_diag_2.setText("There is a problem retrieving  clinic procedures.");
            pb_primary_diag_2.setVisibility(View.GONE);
        }

    }

    public void testSelectedDiagfromExisting(String origin, DataBaseHandler db, ArrayList<TestsAndProcedures> arrayPrimary, String diagCode, String selectedDiagnosisPrimary) {

        Log.d("PREV", selectedDiagnosisPrimary);
        Log.d("PRES", diagCode);


        if (origin.equals(Constant.FROM_PRIMARY_PROC)) {
            if (!selectedDiagnosisPrimary.equals(diagCode)) {
                db.setPrimaryProcToFalse();
                arrayPrimary.clear();

            }
        } else if (origin.equals(Constant.FROM_OTHER_PROC)) {
            if (!selectedDiagnosisPrimary.equals(diagCode)) {
                db.setOtherProcToFalse();
                arrayPrimary.clear();

            }
        } else if (origin.equals(Constant.FROM_OTHER_PROC_2)) {
            if (!selectedDiagnosisPrimary.equals(diagCode)) {
                db.setOtherProcToFalse();
                arrayPrimary.clear();

            }
        } else if (origin.equals(Constant.FROM_PRIMARY_PROC_2)) {
            if (!selectedDiagnosisPrimary.equals(diagCode)) {
                db.setPrimaryProcToFalse2();
                arrayPrimary.clear();

            }
        }


    }

    public void clearData(DataBaseHandler dataBaseHandler, ArrayList<ProceduresListLocal> arrayProcedure, ArrayList<DiagnosisList> arrayListDiagnosis, ArrayList<ProceduresListLocal> arrayPrimary, ArrayList<ProceduresListLocal> arrayOther) {

        dataBaseHandler.setALLProcedureToFalse();
        dataBaseHandler.setALLDiagnosisToFalse();
        arrayProcedure.clear();
        arrayListDiagnosis.clear();
        arrayPrimary.clear();
        arrayOther.clear();
    }


    public void testOtherTest(String ORIGIN, TextView tv_doctor, ArrayList<TestsAndProcedures> arrayPrimary, ArrayList<TestsAndProcedures> arrayOther, TextView tv_primary_diag, TextView tv_other_diag, TextView tv_other_diag_second) {

        if (testField(tv_doctor)) {
            //no doc
            callback.onDoctorNotAvailable();
        } else {
            if (testField(tv_primary_diag)) {
                callback.onPrimaryDiagNotAvailable();
            } else {
                //PASS ALL AVAILABLE DATA
                if (ORIGIN.equals(Constant.FROM_BASIC_TEST)) {
                    showDialog(2);
                } else {
                    showDialog(3);
                }
            }
        }
    }

    public void testBasicTest(String ORIGIN, ArrayList<TestsAndProcedures> arrayList, RecyclerView rv_procedures) {


        if (ORIGIN.equals(Constant.FROM_BASIC_TEST)) {
            showDialog(2);
        } else {
            showDialog(3);
        }


    }

    public Procedure collectData(
            String totalPrice,
            ArrayList<ProceduresListLocal> arrayProcedure,
            DiagnosisList arrayListDiagnosis,
            String getDoctorCode,
            String hospitalCode,
            String getMEMBER_id,
            String username,
            String phoneId, String searchType) {


        Procedure procedures = new Procedure();
        procedures.setUsername(username);
        procedures.setProcedureList(getProcedures(arrayProcedure));
        procedures.setDoctorCode(getDoctorCode);
        procedures.setDiagnosisDesc(arrayListDiagnosis.getDiagDesc());
        procedures.setDeviceID(phoneId);
        procedures.setHospitalCode(hospitalCode);
        procedures.setSearchType(searchType);
        procedures.setDiagnosisCode(arrayListDiagnosis.getDiagCode());
        procedures.setMemberCode(getMEMBER_id);

        return procedures;
    }

    private ArrayList<ProcedureList> getProcedures(ArrayList<ProceduresListLocal> arrayProcedure) {
        ArrayList<ProcedureList> arrayProcList = new ArrayList<>();


        for (ProceduresListLocal procedure : arrayProcedure) {
            ProcedureList procedureList = new ProcedureList();
            procedureList.setId(procedure.getId());
            procedureList.setProcedureAmount(procedure.getProcedureAmount());
            procedureList.setProcedureCode(procedure.getProcedureCode());
            procedureList.setProcedureDesc(procedure.getProcedureDesc());
            procedureList.setServiceClassCode(procedure.getServiceClassCode());
            // procedureList.setStatus(procedure.get);
            arrayProcList.add(procedureList);
        }

        return arrayProcList;

    }

    private String[] getDiagnosisList(String diagList) {
//        String[] data = new String[2];
//        for (int x = 0 ; x < 1 ; x++){
//            data[x] = diagList ;
//        }
        return new String[]{diagList};
    }

    public String getPrice(ArrayList<ProceduresListLocal> arrayProcedure) {

        Double price = 0.0;
        for (ProceduresListLocal proc : arrayProcedure) {

            price = price + Double.parseDouble(proc.getProcedureAmount());

        }


        StringBuilder totalPrice = new StringBuilder();


        totalPrice.append(price);

        return totalPrice.toString();
    }


    public BasicTestOrOtherTest collectOtherTest(String ORIGIN, AvailInterface callback,
                                                 String PHONE_ID,
                                                 String HOSPITALCODE,
                                                 String MEMBERID,
                                                 String masterUSERNAME,
                                                 String searchType,
                                                 DiagnosisList primaryDiag,
                                                 DiagnosisList primaryOtherDiag,
                                                 DiagnosisList primaryOtherDiagSecond,
                                                 ArrayList<TestsAndProcedures> arrayPrimary,
                                                 ArrayList<TestsAndProcedures> arrayOther,
                                                 ArrayList<TestsAndProcedures> arrayOtherSecond,
                                                 String getDoctorCode, String contactNo, String email, String requestingHospCode) {


        BasicTestOrOtherTest data = new BasicTestOrOtherTest();
        data.setPrimaryDiagnosisCode(primaryDiag.getDiagCode());
        data.setDoctorCode(getDoctorCode);
        data.setIsDisclaimerTicked("");
        data.setRequestBy("");
        data.setAppUsername(masterUSERNAME);
        data.setOtherDiagnosisCode(updateDataIfNull(primaryOtherDiag));
        data.setServiceType(Constant.PROCEDURES.equals(ORIGIN) ? 3 : 2);
        data.setRequestOrigin(Constant.REQUEST_ORIGIN);
        data.setHospitalCode(HOSPITALCODE);
        data.setRequestDevice(PHONE_ID);
        data.setMemberCode(MEMBERID);
        data.setCoorContact(contactNo);
        data.setCoorEmail(email);
        data.setRequestingHospCode(Constant.PROCEDURES.equals(ORIGIN) ? "" : requestingHospCode);
        data.setDiagnosisProcedures(setDiagnosisProc(arrayPrimary, arrayOther, arrayOtherSecond, primaryDiag.getDiagCode(),
                updateDataIfNull(primaryOtherDiag), updateDataIfNull(primaryOtherDiagSecond), ORIGIN));

//        SharedPref.testsAndProcedures = arrayPrimary;
//        SharedPref.primaryDiag = primaryDiag;


        return data;
    }

    // Added Aug 11, 2017 -- Lawrence Mataga for Sprint 6 part 1 SIT
    //(((((((((((((((((((((((((((((((
    public RequestBasicTest collectRequestBasicTest(String ORIGIN, AvailInterface callback,
                                                    String MEMBERID,
                                                    String HOSPITALCODE,
                                                    String PHONE_ID,
                                                    String masterUSERNAME,
                                                    ArrayList<BasicTests> arrayPrimary, String contactNo, String email) {


        RequestBasicTest data = new RequestBasicTest();
        data.setHospitalCode(HOSPITALCODE);
        data.setMemberCode(MEMBERID);
        data.setRequestDevice(PHONE_ID);
        data.setAppUsername(masterUSERNAME);
        data.setCoorContact(contactNo);
        data.setCoorEmail(email);
        ArrayList<RequestBasicTest.BasicTestsToSend> toSend = new ArrayList<>();
        if (null != arrayPrimary && arrayPrimary.size() > 0) {
            for (BasicTests basicTests : arrayPrimary) {
                if (basicTests.isSelected()) {
                    RequestBasicTest.BasicTestsToSend btts = new RequestBasicTest.BasicTestsToSend();
                    btts.setProcCode(basicTests.getProcedureCode());
                    btts.setAmount(Double.valueOf(basicTests.getProcedureAmount()));
                    btts.setProcDesc(basicTests.getProcedureDesc());
                    toSend.add(btts);
                }
            }
        } else {

        }

        data.setTests(toSend);
        return data;
    }


    private String updateDataIfNull(DiagnosisList data) {
        return data == null ? "" : data.getDiagCode();
    }

    private ArrayList<DiagnosisProcedures> setDiagnosisProc(ArrayList<TestsAndProcedures> arrayPrimary, ArrayList<TestsAndProcedures> arrayOther, ArrayList<TestsAndProcedures> arrayOtherSecond, String primaryDiag, String otherDiag, String otherDiagSecond, String ORIGIN) {

        ArrayList<DiagnosisProcedures> fill = new ArrayList<>();
        fill.addAll(collectDiagnosis(arrayPrimary, primaryDiag, ORIGIN));
        fill.addAll(collectDiagnosis(arrayOther, otherDiag, ORIGIN));
        fill.addAll(collectDiagnosis(arrayOtherSecond, otherDiagSecond, ORIGIN));
        return fill;
    }

    // Added Aug 11, 2017 -- Lawrence Mataga for Sprint 6 part 1 SIT


    private ArrayList<BasicTests> setBasictest(ArrayList<BasicTests> arrayPrimary, String ORIGIN) {

        ArrayList<BasicTests> fill = new ArrayList<>();
        fill.addAll(collectOtherTest(arrayPrimary, ORIGIN));
        return fill;
    }

    // Added Aug 11, 2017 -- Lawrence Mataga for Sprint 6 part 1 SIT

    private Collection<? extends BasicTests> collectOtherTest(ArrayList<BasicTests> arrayPrimary, String ORIGIN) {
        ArrayList<BasicTests> fill = new ArrayList<>();


        //     String json = "{\"diagnosisProcedures\":[{\"amount\":\"340.0000\",\"diagnosisCode\":\"Dx2822\",\"procedureCode\":\"DP0000404\",\"serviceType\":\"2\"},{\"amount\":\"275.0000\",\"diagnosisCode\":\"Dx2822\",\"procedureCode\":\"DP0000406\",\"serviceType\":\"2\"},{\"amount\":\"275.0000\",\"diagnosisCode\":\"Dx2674\",\"procedureCode\":\"DP0000186\",\"serviceType\":\"2\"}],\"doctorCode\":\"006612\",\"hospitalCode\":\"000018\",\"isDisclaimerTicked\":\"\",\"memberCode\":\"2621763\",\"otherDiagnosisCode\":\"Dx2674\",\"primaryDiagnosisCode\":\"Dx2822\",\"requestBy\":\"\",\"requestDevice\":\"000000000000000\",\"requestOrigin\":\"coordinator\",\"serviceSubtype\":\"3\"}" ;


        for (BasicTests basicTests : arrayPrimary) {

//            DiagnosisProcedures diagnosisProcedures = new DiagnosisProcedures();
//            diagnosisProcedures.setAmount(basicTests.getProcedureAmount());
//            diagnosisProcedures.setProcedureCode(basicTests.getProcedureCode());
//            diagnosisProcedures.setServiceType(testOriginToServiceType(ORIGIN));
            fill.add(basicTests);
        }


        return fill;
    }

    private Collection<? extends DiagnosisProcedures> collectDiagnosis(ArrayList<TestsAndProcedures> arrayPrimary, String primaryDiag, String ORIGIN) {
        ArrayList<DiagnosisProcedures> fill = new ArrayList<>();


        //     String json = "{\"diagnosisProcedures\":[{\"amount\":\"340.0000\",\"diagnosisCode\":\"Dx2822\",\"procedureCode\":\"DP0000404\",\"serviceType\":\"2\"},{\"amount\":\"275.0000\",\"diagnosisCode\":\"Dx2822\",\"procedureCode\":\"DP0000406\",\"serviceType\":\"2\"},{\"amount\":\"275.0000\",\"diagnosisCode\":\"Dx2674\",\"procedureCode\":\"DP0000186\",\"serviceType\":\"2\"}],\"doctorCode\":\"006612\",\"hospitalCode\":\"000018\",\"isDisclaimerTicked\":\"\",\"memberCode\":\"2621763\",\"otherDiagnosisCode\":\"Dx2674\",\"primaryDiagnosisCode\":\"Dx2822\",\"requestBy\":\"\",\"requestDevice\":\"000000000000000\",\"requestOrigin\":\"coordinator\",\"serviceSubtype\":\"3\"}" ;


        for (TestsAndProcedures proceduresListLocal : arrayPrimary) {

            DiagnosisProcedures diagnosisProcedures = new DiagnosisProcedures();
            diagnosisProcedures.setAmount(proceduresListLocal.getProcedureAmount());
            diagnosisProcedures.setDiagnosisCode(primaryDiag);
            diagnosisProcedures.setProcedureCode(proceduresListLocal.getProcedureCode());
            diagnosisProcedures.setServiceType(testOriginToServiceType(ORIGIN));
            fill.add(diagnosisProcedures);
        }


        return fill;
    }

    private String testOriginToServiceType(String origin) {
        return origin.equals(Constant.BASIC_TEST_TO_SEND) ? Constant.BASIC_TEST_SERVICE_TYPE
                : Constant.OTHER_TEST_SERVICE_TYPE;
    }


    public ArrayList<DiagnosisTest> getDataDiagnosis(ArrayList<TestsAndProcedures> primaryOtherDiag) {
        ArrayList<DiagnosisTest> data = new ArrayList<>();

        for (TestsAndProcedures proc : primaryOtherDiag) {

            DiagnosisTest otherDiag = new DiagnosisTest();
            otherDiag.setPROCEDURE_ID("");
            otherDiag.setPROCEDURE_CODE(proc.getProcedureCode());
            otherDiag.setPROCLASS_CODE(proc.getMaceServiceType());
            otherDiag.setPROCEDURE_DESC(proc.getProcedureDesc());
            otherDiag.setPROCEDURE_RATE(proc.getProcedureAmount());

            data.add(otherDiag);
        }

        return data;
    }


    private String getTotal(ArrayList<ProceduresListLocal> arrayPrimary, ArrayList<ProceduresListLocal> arrayOther) {
        double totalPrice = Double.parseDouble(getPrice(arrayPrimary)) + Double.parseDouble(getPrice(arrayOther));
        return String.valueOf(totalPrice);
    }


    /**
     * @param tv_approvalNo textviw approval No
     * @param s             approval No
     * @param ll_reference
     */
    public void setApprovalNoField(TextView tv_approvalNo, String s, LinearLayout ll_reference) {

        if (s.equals("")) {
            ll_reference.setVisibility(View.GONE);
        } else {
            ll_reference.setVisibility(View.VISIBLE);
            tv_approvalNo.setText(context.getString(R.string.consultation_no) + " " + s);
        }
    }

    public void setRequestOrigin(boolean isOtherProcedure, boolean isBasicTest, boolean isProcedure, AvailInterface callback, LoaList loaFillUpMOdel) {

        if (isBasicTest)
            callback.basicTestFillUpFields(loaFillUpMOdel);
        else if (isOtherProcedure)
            callback.isOtherProcedureFillUpFields(loaFillUpMOdel);
        else if (isProcedure)
            callback.isProcedureFillUpFields(loaFillUpMOdel);
    }

    public ArrayList<ProceduresListLocal> getProcedurefromList(String procedureCode, DataBaseHandler dataBaseHandler) {
        ArrayList<ProceduresListLocal> data = new ArrayList<>();

        // dataBaseHandler.getSelectedProcedureList(parseDataProcedures(procedureCode));


        return data;
    }


    public String getOrigin(boolean isMaternity) {
        return isMaternity ? Constant.MATERNITY : Constant.IS_NOT_MATERNITY;
    }


    public String[] getProcList(ArrayList<BasicTests> arrayBasicTest) {
        String[] data = new String[arrayBasicTest.size()];

        for (int x = 0; x < arrayBasicTest.size(); x++) {


            data[x] = arrayBasicTest.get(x).getProcedureCode();

        }


        return data;
    }


    public void loadProcedureBasicTest(ProgressBar pb_primary_diag, TextView tv_primary_diag, boolean b) {

        if (b) {
            pb_primary_diag.setVisibility(View.VISIBLE);
            tv_primary_diag.setText(context.getString(R.string.loading_proc));

        } else {
            tv_primary_diag.setText(context.getString(R.string.add_procedure));
            pb_primary_diag.setVisibility(View.GONE);
        }

    }

    public void setEventTrigger(AvailServices.MessageEvent event, boolean isFilledUpAndCannotbeEdited) {
        //ll_consult
        if (event.getMessage().equals("0")) {
            callback.setConsultationUI();
        } else if (event.getMessage().equals("1")) {
            //ll_maternity
            callback.setMaternityUI();
        } else if (event.getMessage().equals("2")) {
            //ll_basicsicTestUI
            callback.setBasicTestUI();
        } else if (event.getMessage().equals("3")) {
            //ll_other
            callback.setOtherTestUI();
        } else if (event.getMessage().equals("4") ||
                event.getMessage().equals("14")) {
            if (isFilledUpAndCannotbeEdited) {
                callback.deleteData();
            }
            callback.setDoctorSelected(event.getDoctorCreds(), event.getMEMBER_ID(),
                    event.getHospitalCode(),
                    event.getDoctorCode(),
                    event.getDoctorName(),
                    event.getHospitalName());
        } else if (event.getMessage().equals("5")) {
            callback.setProcedureSelected(event.getArrayProc());
        } else if (event.getMessage().equals("6")) {
            callback.updateDiagnosis(event.getDiagnosisList());
        } else if (event.getMessage().equals("8")) {
            callback.updatePrimaryProcedure(event.getArrayProc());
        } else if (event.getMessage().equals("9")) {
            if (isFilledUpAndCannotbeEdited) {
                callback.deleteData();
            }
            callback.setPrimaryDiagnosis(event.getDiagnosisList());
        } else if (event.getMessage().equals("10")) {
            callback.setOtherProcedures(event.getArrayProc());
        } else if (event.getMessage().equals("11")) {
            if (isFilledUpAndCannotbeEdited) {
                callback.deleteData();
            }
            callback.setOtherDiagnosis(event.getDiagnosisList());
        } else if (event.getMessage().equals("12")) {
            callback.setProceduresUI();
        } else if (event.getMessage().equals("13")) {
            callback.deleteData();
            callback.setSelectedPreMadeConsultationData(event.getLoa());
        } else if (event.getMessage().equals("15")) {
            if (isFilledUpAndCannotbeEdited) {
                callback.deleteData();
            }
            callback.setOtherDiagnosisSecond(event.getDiagnosisList());
        } else if (event.getMessage().equals("18")) {
            callback.setOtherProceduresSecond(event.getArrayProc());
        } else if (event.getMessage().equals(Constant.GETTIME)) {
            callback.getTime(event.getOrigin());
        } else if (event.getMessage().equals(Constant.ADAPTER_ROOM_PLANS)) {
            callback.setRoomPlans(event.getRooms());
        } else if (event.getMessage().equals(Constant.ROOM_CATEGORY)) {
            callback.setRoomCategory(event.getOrigin());
        } else if (event.getMessage().equals("19")) {
            //used for outpatient other test -jj
            callback.setUnfilteredHosp(event.getHospitalList());
        } else if (event.getMessage().equals("20")) {
            //used for inpatient doctor select -jj
            callback.setInPatientDoctorSelected(event.getDoctorModelMultipleSelections());
        } else if (event.getMessage().equals("21")) {
            //used for inpatient diagnosis -jj
            callback.setInPatientDiagnosisSelected(event.getDiagnosisModelInsertions());
        } else if (event.getMessage().equals("22")) {
            //used for inpatient procedure -jj
            callback.setInPatientProcedureSelected(event.getProcedureModelInsertions());
        }


    }

    public BasicTestOrOtherTest collectProcedures(String ORIGIN,
                                                  String PHONE_ID,
                                                  String HOSPITALCODE,
                                                  String MEMBERID,
                                                  String masterUSERNAME,
                                                  String SEARCHTYPE,
                                                  ArrayList<TestsAndProcedures> arrayProcedure,
                                                  DiagnosisList primaryDiag2,
                                                  String getDoctorCode,
                                                  String remarks,
                                                  String contactNo,
                                                  String email) {


        BasicTestOrOtherTest data = new BasicTestOrOtherTest();
        data.setPrimaryDiagnosisCode(primaryDiag2.getDiagCode());
        data.setDoctorCode(getDoctorCode);
        data.setIsDisclaimerTicked("");
        data.setRequestBy("");
        data.setOtherDiagnosisCode("");
        data.setServiceType(Constant.PROCEDURES.equalsIgnoreCase(ORIGIN) ? 3 : 2);
        data.setRequestOrigin(Constant.REQUEST_ORIGIN);
        data.setHospitalCode(HOSPITALCODE);
        data.setRequestDevice(PHONE_ID);
        data.setMemberCode(MEMBERID);
        data.setAppUsername(masterUSERNAME);
        data.setDiagnosisProcedures(setDiagnosisProc(arrayProcedure, primaryDiag2.getDiagCode(), ORIGIN));
        data.setRemarks(remarks);
        data.setCoorContact(contactNo);
        data.setCoorEmail(email);
//      data.setDiagnosisProcedures(setDiagnosisProc(arrayPrimary, arrayOther, primaryDiag.getDiagCode(),
//                updateDataIfNull(primaryOtherDiag), ORIGIN));

        return data;
    }


    private ArrayList<DiagnosisProcedures> setDiagnosisProc(ArrayList<TestsAndProcedures> arrayProcedure, String diagCode, String origin) {

        ArrayList<DiagnosisProcedures> data = new ArrayList<>();


        data.addAll(collectDiagnosis(arrayProcedure, diagCode, origin));
//        for (int x = 0; x < arrayProcedure.size(); x++) {
//            TestsAndProcedures proceduresListLocal = arrayProcedure.get(x);
//
//            DiagnosisProcedures diagnosisProcedures = new DiagnosisProcedures();
//            diagnosisProcedures.setAmount(proceduresListLocal.getProcedureAmount());
//            diagnosisProcedures.setServiceType(origin);
//            diagnosisProcedures.setProcedureCode(proceduresListLocal.getProcedureCode());
//            diagnosisProcedures.setDiagnosisCode(diagCode);
//        }


        return data;
    }

    public String getRemarks(int approved, int procedures) {

        return approved == procedures ? "REQUEST APPROVED" : "REQUEST PENDING";

    }

    public BasicTestOrOtherTest collectBasicTest(String ORIGIN,
                                                 String PHONE_ID,
                                                 String HOSPITALCODE,
                                                 String MEMBERID,
                                                 String masterUSERNAME,
                                                 String SEARCHTYPE,
                                                 ArrayList<TestsAndProcedures> arrayProcedure,
                                                 DiagnosisList primaryDiag2,
                                                 String getDoctorCode, String contact, String email) {


        BasicTestOrOtherTest data = new BasicTestOrOtherTest();
        data.setDoctorCode(getDoctorCode);
        data.setIsDisclaimerTicked("");
        data.setRequestBy("");
        data.setOtherDiagnosisCode("");
        data.setServiceType(Constant.PROCEDURES.equalsIgnoreCase(ORIGIN) ? 3 : 2);
        data.setRequestOrigin(Constant.REQUEST_ORIGIN);
        data.setHospitalCode(HOSPITALCODE);
        data.setRequestDevice(PHONE_ID);
        data.setAppUsername(masterUSERNAME);
        data.setMemberCode(MEMBERID);
        data.setCoorContact(contact);
        data.setCoorEmail(email);
        data.setDiagnosisProcedures(setDiagnosisProc(arrayProcedure, "", ORIGIN));

        return data;


    }

    private ArrayList<BasicTestOrOtherTest.TestObj> setTests() {
        ArrayList<BasicTestOrOtherTest.TestObj> objs = new ArrayList<>();

        return objs;
    }

    public void setAllDoctorDataInpatientToFalse(DataBaseHandler dataBaseHandler) {
        dataBaseHandler.setAllDoctorDataInpatientToFalse();
    }

    public void setAllDiagnosisDataInpatientToFalse(DataBaseHandler dataBaseHandler) {
        dataBaseHandler.setAllDiagnosisDataInpatientToFalse();
    }

    public void setAllProcedureDataInpatientToFalse(DataBaseHandler dataBaseHandler) {
        dataBaseHandler.setAllProcedureDataInpatientToFalse();
    }
}
