package fragment.Avail;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Calendar;

import adapter.BasictestAdapter;
import adapter.InpatientDiagnosisSelectAdapter_Availservice;
import adapter.InpatientDoctorSelectAdapter_Availservice;
import adapter.InpatientProcedureSelectAdapter_Availservice;
import adapter.ProceduresToBeSendAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import coord.medicard.com.medicardcoordinator.ApprovalScreen.ApprovalActivity;
import coord.medicard.com.medicardcoordinator.LoaDisplay.LoaDisplayActivity;
import coord.medicard.com.medicardcoordinator.Navigator.NavigatorActivity;
import coord.medicard.com.medicardcoordinator.R;
import coord.medicard.com.medicardcoordinator.Result.ResultActivity;
import de.hdodenhof.circleimageview.CircleImageView;
import fragment.CoordinatorMenuButton;
import model.BasicTestOrOtherTest;
import model.BasicTestOrOtherTestReturn;
import model.BasicTestResponse;
import model.BasicTestReturn;
import model.BasicTests;
import model.Confirm;
import model.DiagnosisClinicProcedures;
import model.DiagnosisList;
import model.DoctorModelInsertion;
import model.GetEr;
import model.Hospital;
import model.RequestBasicTest;
import model.Test.DiagnosisProcedures;
import model.GetInPatientReturn;
import model.LOAReturn;
import model.LoaList;
import model.ProcedureFilter;
import model.Rooms;
import model.TestsAndProcedures;
import utilities.APIMultipleCalls.FilteredProcedureCall;
import utilities.AlertDialogCustom;
import utilities.BasicTestSession;
import utilities.Constant;
import utilities.DataBaseHandler;
import utilities.DateAdmitted;
import utilities.DateConverter;
import utilities.ErrorMessage;
import utilities.GetOnlyApprovedData;
import utilities.InpatientSession;
import utilities.LoadCreator;
import utilities.OtherTestSession;
import utilities.SharedPref;
import utilities.TimepickerSet;
import utilities.ClinicProcedureSession;


public class AvailServices extends Fragment implements View.OnClickListener, AlertDialogCustom.ConfirmLoa, AvailInterface, FilteredProcedureCall.FilterProcedureCallback {

    public AvailServices() {
        // Required empty public constructor
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {

        }
    }

    public static final String panelER = "UPDATEER";

    boolean isFilledUpAndCannotbeEdited = false;
    boolean isPreventingDeleteFromOnStop = false;

    //BASIC TESTS
    ArrayList<BasicTests> arrayBasicTest = new ArrayList<>();

    //OTHER DIAGNOSIS AND PROCEDURES UTILS ,test if diagnosis already selected
    String selectedDiagnosisOther = "";
    boolean isOtherAvailable = false;
    boolean isOtherSelected = false;
    boolean isOtherSelectedSecond = false;


    //PRIMARY DIAGNOSIS AND PROCEDURES UTILS
    String selectedDiagnosisPrimary = "";
    boolean isPrimaryAvailable = false;
    boolean isPrimarySelected = false;

    //PRIMARY 2 DIAGNOSIS ANF PROCEDURES UTILS
    String selectedDignosisPrimary_2 = "";
    boolean isPrimary_2_Available = false;
    boolean isPrimary_2_Selected = false;

    //BASIC TEST UTILS
    boolean isBasicTestAvailable = false;

    //Added BasicTestsSession for temp storage of List BasicTests
    BasicTestSession session;
    ClinicProcedureSession clinicProcedureSession;
    OtherTestSession otherTestSession;
    InpatientSession inpatientSession;
    //

    String primaryDiagOtherTest;

    private String[] proceduresPrimaryList;
    private String[] proceduresOtherList;
    private String[] proceduresPrimary2List;
    String dateSubmitted;

    AlertDialogCustom dialogCustom = new AlertDialogCustom();
    Fragment fragment;
    FragmentTransaction fragmentTransaction;
    ProceduresToBeSendAdapter proceduresToBeSendAdapter, primaryProcedures, otherProcedure, otherProcedureSecond;

    ArrayList<TestsAndProcedures> arrayPrimary = new ArrayList<>();
    ArrayList<TestsAndProcedures> arrayOther = new ArrayList<>();

    ArrayList<TestsAndProcedures> arrayOtherSecond = new ArrayList<>();
    ArrayList<TestsAndProcedures> arrayProcedure = new ArrayList<>();

    InpatientDoctorSelectAdapter_Availservice inpatientDoctorSelectAdapterAvailservice;
    InpatientDiagnosisSelectAdapter_Availservice inpatientDiagnosisSelectAdapterAvailservice;
    InpatientProcedureSelectAdapter_Availservice inpatientProcedureSelectAdapterAvailservice;


    ArrayList<DoctorModelInsertion> doctorModelInsertions = new ArrayList<>();
    ArrayList<DiagnosisList> diagnosisModelInsertions = new ArrayList<>();
    ArrayList<TestsAndProcedures> procedureModelInsertions = new ArrayList<>();


    //  ArrayList<DiagnosisList> arrayListDiagnosis = new ArrayList<>();
    ArrayList<String> proceduresToDisplay = new ArrayList<>();
    ArrayList<String> otherTestToDisplay = new ArrayList<>();
    //    ArrayList<TestsAndProcedures> testsAndProcedures = new ArrayList<>();
    ArrayList<BasicTests> testsArrayList = new ArrayList<>();
    ArrayList<TestsAndProcedures> basicTestList = new ArrayList<>();
    ArrayList<TestsAndProcedures> basicTestListSelected = new ArrayList<>();


    String OTHER_SELECTED_DIAGNOSIS = "";
    DiagnosisList primaryDiag2 = null;
    DiagnosisList primaryDiag = null;
    DiagnosisList primaryOtherDiag = null;
    DiagnosisList primaryOtherDiagSecond = null;

    ProgressDialog pd;
    String FROM_FRAG = "";
    DataBaseHandler dataBaseHandler;

    @BindView(R.id.ll_setInputFields)
    CardView ll_setInputFields;
    @BindView(R.id.ll_loa_form)
    LinearLayout ll_loa_form;
    @BindView(R.id.ll_er)
    LinearLayout ll_er;


    @BindView(R.id.rv_diagnosis)
    RecyclerView rv_diagnosis;
    @BindView(R.id.rv_procedures)
    RecyclerView rv_procedures;

    @BindView(R.id.et_problem)
    EditText et_problem;
    //er reason
    @BindView(R.id.et_er_reason)
    EditText et_er_reason;
    @BindView(R.id.tv_er_reason_label)
    TextView tv_er_reason_label;
    @BindView(R.id.ll_validate)
    LinearLayout ll_validate;
    @BindView(R.id.fr_otherProc)
    FrameLayout fr_otherProc;
    @BindView(R.id.btn_proceed)
    Button btn_proceed;
    @BindView(R.id.btn_cancel)
    Button btn_cancel;
    @BindView(R.id.btn_back)
    Button btn_back;
    @BindView(R.id.btn_dischargeER)
    Button btn_dischargeER;
    @BindView(R.id.btn_dischargeERToInPatient)
    Button btn_dischargeERToInPatient;
    @BindView(R.id.btn_dischargeERToOutPatient)
    Button btn_dischargeERToOutPatient;

    @BindView(R.id.tv_problem_tag)
    TextView tv_problem_tag;

    @BindView(R.id.tv_primary)
    TextView tv_primary;

    @BindView(R.id.tv_admitted)
    TextView tv_admitted;
    @BindView(R.id.tv_admitted_time)
    TextView tv_admitted_time;
    @BindView(R.id.tv_admitteddateer)
    TextView tv_admitteddateer;
    @BindView(R.id.tv_admitteddate)
    TextView tv_admitteddate;


    @BindView(R.id.et_inputdoctor)
    TextView et_inputdoctor;
    @BindView(R.id.tv_doctor_tag)
    TextView tv_doctor_tag;
    @BindView(R.id.tv_doctor)
    TextView tv_doctor;
    @BindView(R.id.tv_hospital)
    TextView tv_hospital;
    @BindView(R.id.tv_hospital_tag)
    TextView tv_hospital_tag;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_diagnosis)
    TextView tv_diagnosis;
    @BindView(R.id.ll_diag_test)
    LinearLayout ll_diag_test;
    @BindView(R.id.tv_total_price)
    TextView tv_total_price;

    @BindView(R.id.rl_procDiag)
    RelativeLayout rv_noProcAvailable;

    @BindView(R.id.cb_informed)
    CheckBox cb_informed;
    @BindView(R.id.tv_informed)
    TextView tv_informed;


    @BindView(R.id.et_input_room_type)
    EditText et_input_room_type;

    @BindView(R.id.ll_primary_ip_label)
    LinearLayout ll_primary_ip_label;

    //room plan
    @BindView(R.id.et_input_room_number)
    TextView et_input_room_number;
    @BindView(R.id.et_input_room_number_label)
    TextView et_input_room_number_label;
    @BindView(R.id.et_input_room_price)
    EditText et_input_room_price;
    @BindView(R.id.et_input_category)
    TextView et_input_category;
    @BindView(R.id.et_input_category_label)
    TextView et_input_category_label;
    //room number
    @BindView(R.id.et_input_number)
    EditText et_input_number;
    @BindView(R.id.et_input_number_label)
    TextView et_input_number_label;
    @BindView(R.id.tv_er_sent)
    TextView tv_er_sent;

    @BindView(R.id.rv_basicTest)
    RecyclerView rv_basicTest;

    @BindView(R.id.rv_primary)
    RecyclerView rv_primary;
    @BindView(R.id.rl_primary)
    RelativeLayout rl_primary;
    @BindView(R.id.tv_primary_diag)
    TextView tv_primary_diag;
    @BindView(R.id.tv_primary_diag_label)
    TextView tv_primary_diag_label;
    @BindView(R.id.ll_attending_doc)
    LinearLayout ll_attending_doc;
    @BindView(R.id.textView10)
    TextView textView10;
    @BindView(R.id.rl_basic_test)
    CardView rl_basic_test;

    @BindView(R.id.tv_other_diag)
    TextView tv_other_diag;
    //this is the second other diag textView
    @BindView(R.id.tv_other_diag_second)
    TextView tv_other_diag_second;


    @BindView(R.id.cv_othertest_request)
    CardView cv_othertest_request;
    @BindView(R.id.tv_othertest_request_hosp)
    TextView tv_othertest_request_hosp;
    @BindView(R.id.tv_othertest_request_doc)
    TextView tv_othertest_request_doc;


    @BindView(R.id.rv_other_diag)
    RecyclerView rv_other_diag;

    //this is the second Recyclerview for other Diag
    @BindView(R.id.rv_other_diag_second)
    RecyclerView rv_other_diag_second;
    @BindView(R.id.imgBtn_delete_diagnosis)
    ImageButton imgBtn_delete_diagnosis;

    //this is the second Delete for the CardView
    @BindView(R.id.imgBtn_delete_diagnosis_second)
    ImageButton imgBtn_delete_diagnosis_second;


    @BindView(R.id.rl_other_diag)
    RelativeLayout rl_other_diag;
    //this is the second Relative layout for other proc
    @BindView(R.id.rl_other_diag_second)
    RelativeLayout rl_other_diag_second;

    @BindView(R.id.tv_proc_remarks_tag)
    TextView tv_proc_remarks_tag;

    @BindView(R.id.tv_proc_remarks)
    TextView tv_proc_remarks;


    @BindView(R.id.cv_other)
    CardView cv_other;
    //this is the second layout for other diagnosis
    @BindView(R.id.cv_other_second)
    CardView cv_other_second;

    @BindView(R.id.cv_prime)
    CardView cv_prime;


    @BindView(R.id.diag_procedure_tag)
    TextView diag_procedure_tag;

    @BindView(R.id.ll_reference)
    LinearLayout ll_reference;
    @BindView(R.id.tv_diag_type)
    TextView tv_diag_type;

    @BindView(R.id.pb_primary_diag)
    ProgressBar pb_primary_diag;
    @BindView(R.id.tv_other_proc)
    TextView tv_other_proc;
    @BindView(R.id.tv_other_proc_second)
    TextView tv_other_proc_second;
    @BindView(R.id.pb_other_diag)
    ProgressBar pb_other_diag;
    @BindView(R.id.pb_other_diag_second)
    ProgressBar pb_other_diag_second;

    @BindView(R.id.tv_primary_diag_2)
    TextView tv_primary_diag_2;
    @BindView(R.id.pb_primary_diag_2)
    ProgressBar pb_primary_diag_2;
    @BindView(R.id.add_diagnosis)
    Button add_diagnosis;
    @BindView(R.id.add_diagnosis2)
    Button add_diagnosis2;
    @BindView(R.id.ll_OtherDiagnosis)
    LinearLayout ll_OtherDiagnosis;
    @BindView(R.id.ll_OtherDiagnosis_second)
    LinearLayout ll_OtherDiagnosis_second;


    @BindView(R.id.imgBtn_delete_data)
    ImageButton imgBtn_delete_data;

    @BindView(R.id.imgBtn_viewLoa)
    ImageButton imgBtn_viewLoa;

    @BindView(R.id.tv_approvalNo)
    TextView tv_approvalNo;

    @BindView(R.id.tv_inpatient_doctor)
    TextView tv_inpatient_doctor;
    @BindView(R.id.rv_inpatient_doctor)
    RecyclerView rv_inpatient_doctor;
    @BindView(R.id.btn_inpatient_doctor)
    Button btn_inpatient_doctor;

    @BindView(R.id.tv_inpatient_procedure)
    TextView tv_inpatient_procedure;
    @BindView(R.id.rv_inpatient_procedure)
    RecyclerView rv_inpatient_procedure;
    @BindView(R.id.ll2_inpatient_procedure)
    LinearLayout ll2_inpatient_procedure;


    @BindView(R.id.btn_inpatient_procedure)
    Button btn_inpatient_procedure;

    @BindView(R.id.tv_inpatient_diagnosis)
    TextView tv_inpatient_diagnosis;
    @BindView(R.id.rv_inpatient_diagnosis)
    RecyclerView rv_inpatient_diagnosis;
    @BindView(R.id.btn_inpatient_diagnosis)
    Button btn_inpatient_diagnosis;


    @BindView(R.id.tv_other_proc_Add)
    TextView tv_other_proc_Add;
    //this is the second other proc add second
    @BindView(R.id.tv_other_proc_Add_second)
    TextView tv_other_proc_Add_second;
    @BindView(R.id.tv_primary_diag_2_add)
    TextView tv_primary_diag_2_add;
    @BindView(R.id.tv_primary_add)
    TextView tv_primary_add;

    @BindView(R.id.cv_request)
    CardView cv_request;


    @BindView(R.id.contact12)
    EditText contact12;
    @BindView(R.id.contact14)
    EditText contact14;


//    @BindView(R.id.ll_request_details)
//    LinearLayout ll_request_details;
//
//    @BindView(R.id.tv_primary_diagnosis)
//    TextView tv_primary_diagnosis;
//
//    @BindView(R.id.tv_doc_name)
//    TextView tv_doc_name;
//
//    @BindView(R.id.tv_doc_spec)
//    TextView tv_doc_spec;

//    @BindView(R.id.tv_type_of_Admission)
//    TextView tv_type_of_Admission;


    boolean isMaternity = false;
    boolean isConsultation = false;
    boolean isOtherTest = false;
    boolean isInPatient = false;
    boolean isBasicTest = false;
    boolean isProcedure = false;
    boolean isER = false;


    String getDoctorCode = "";
    String getProblemConsultation = "";
    String getErReason = "";
    String getHospitalCode = "";
    String getMEMBER_ID = "";
    String getDoctor = "";
    String getDoctorSpec = "";
    String getDiagnosis = "";
    String REQUEST = "";
    String rawTimeInPatient = "";
    String inPatientRoomType = "";
    String inPatientRoomCategory = "";


    AlertDialogCustom.ConfirmLoa callbackConfirm;
    FilteredProcedureCall.FilterProcedureCallback filterProcCallback;
    AvailInterface callback;
    AvailRetrieve implement;
    LoadCreator loader;
    BasictestAdapter basictestAdapter;
    Context context;

    LoaList loaFillUpMOdel;
    LOAReturn getLoaReturnRequests;
    BasicTestOrOtherTestReturn otherTestBasicTestRequest;
    BasicTestResponse basicTestResponse;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        callbackConfirm = this;
        callback = this;
        filterProcCallback = this;
        loader = new LoadCreator(getContext());
        context = getContext();
        implement = new AvailRetrieve(context, callback);
        dataBaseHandler = new DataBaseHandler(context);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_avail_service, container, false);
        ButterKnife.bind(this, view);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.ADD_OTHER_DIAG, "", context);
        rv_primary.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_other_diag.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_other_diag_second.setLayoutManager(new LinearLayoutManager(getContext()));

        rv_inpatient_doctor.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_inpatient_diagnosis.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_inpatient_procedure.setLayoutManager(new LinearLayoutManager(getContext()));

        rv_procedures.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_basicTest.setLayoutManager(new LinearLayoutManager(getContext()));
        basictestAdapter = new BasictestAdapter(context, basicTestList, dataBaseHandler);
        rv_basicTest.setAdapter(basictestAdapter);
        //  rv_diagnosis.setLayoutManager(new LinearLayoutManager(getContext()));
        primaryProcedures = new ProceduresToBeSendAdapter(tv_primary_add, implement, rv_primary, rl_primary, getContext(), arrayPrimary, tv_total_price, Constant.ADAPTER_PRIMARY, callback, dataBaseHandler);
        otherProcedure = new ProceduresToBeSendAdapter(tv_other_proc_Add, implement, rv_other_diag, rl_other_diag, getContext(), arrayOther, tv_total_price, Constant.ADAPTER_OTHER, callback, dataBaseHandler);
        otherProcedureSecond = new ProceduresToBeSendAdapter(tv_other_proc_Add_second, implement, rv_other_diag_second, rl_other_diag_second, getContext(), arrayOtherSecond, tv_total_price, Constant.ADAPTER_OTHER, callback, dataBaseHandler);
        proceduresToBeSendAdapter = new ProceduresToBeSendAdapter(tv_primary_diag_2_add, implement, rv_procedures, rv_noProcAvailable, getContext(), arrayProcedure, tv_total_price, Constant.ADAPTER_PROCEDURE, callback, dataBaseHandler);
        inpatientDoctorSelectAdapterAvailservice = new InpatientDoctorSelectAdapter_Availservice(dataBaseHandler, getContext(), doctorModelInsertions, rv_inpatient_doctor, btn_inpatient_doctor, tv_inpatient_doctor);
        inpatientDiagnosisSelectAdapterAvailservice = new InpatientDiagnosisSelectAdapter_Availservice(dataBaseHandler, getContext(), diagnosisModelInsertions, rv_inpatient_diagnosis, btn_inpatient_diagnosis, tv_inpatient_diagnosis);
        inpatientProcedureSelectAdapterAvailservice = new InpatientProcedureSelectAdapter_Availservice(dataBaseHandler, getContext(), procedureModelInsertions, ll2_inpatient_procedure, btn_inpatient_procedure, tv_inpatient_procedure, implement);

        //    diagnosisToBeSendAdapter = new DiagnosisToBeSendAdapter(tv_diagnosis, rv_diagnosis, getContext(), arrayListDiagnosis);
        rv_procedures.setAdapter(proceduresToBeSendAdapter);
        rv_procedures.setNestedScrollingEnabled(false);
        rv_inpatient_doctor.setAdapter(inpatientDoctorSelectAdapterAvailservice);
        rv_inpatient_doctor.setNestedScrollingEnabled(false);
        rv_inpatient_diagnosis.setAdapter(inpatientDiagnosisSelectAdapterAvailservice);
        rv_inpatient_diagnosis.setNestedScrollingEnabled(false);
        rv_inpatient_procedure.setAdapter(inpatientProcedureSelectAdapterAvailservice);
        rv_inpatient_procedure.setNestedScrollingEnabled(false);

        //   rv_diagnosis.setAdapter(diagnosisToBeSendAdapter);
        //  rv_diagnosis.setNestedScrollingEnabled(false);
        rv_primary.setAdapter(primaryProcedures);
        rv_primary.setNestedScrollingEnabled(false);
        rv_other_diag.setAdapter(otherProcedure);
        rv_other_diag.setNestedScrollingEnabled(false);
        rv_other_diag_second.setAdapter(otherProcedureSecond);
        rv_other_diag_second.setNestedScrollingEnabled(false);
        proceduresToBeSendAdapter.notifyDataSetChanged();

        inpatientDoctorSelectAdapterAvailservice.notifyDataSetChanged();
        inpatientDiagnosisSelectAdapterAvailservice.notifyDataSetChanged();
        inpatientProcedureSelectAdapterAvailservice.notifyDataSetChanged();

        //  diagnosisToBeSendAdapter.notifyDataSetChanged();
        primaryProcedures.notifyDataSetChanged();
        otherProcedure.notifyDataSetChanged();
        otherProcedureSecond.notifyDataSetChanged();

        session = new BasicTestSession();
        clinicProcedureSession = new ClinicProcedureSession();
        otherTestSession = new OtherTestSession();
        inpatientSession = new InpatientSession();


        //   implement.setDiagnosisAvailability(arrayListDiagnosis, tv_diagnosis, rv_diagnosis);
        implement.setProceduresAvailability(arrayProcedure, rv_noProcAvailable, rv_procedures
                , tv_total_price, tv_primary_diag_2_add);

        implement.setCheckBox(cb_informed, btn_proceed);
        implement.setUI();
        implement.setApprovalNoField(tv_approvalNo, "", ll_reference);

        tv_hospital.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALNAME, getContext()));


        et_problem.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                return (event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER);
            }
        });

        add_diagnosis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cv_other.setVisibility(View.VISIBLE);
                add_diagnosis.setVisibility(View.GONE);
                add_diagnosis2.setVisibility(View.GONE);
            }
        });
        add_diagnosis2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cv_other_second.setVisibility(View.VISIBLE);
                add_diagnosis.setVisibility(View.GONE);
                add_diagnosis2.setVisibility(View.GONE);
            }
        });

        imgBtn_delete_diagnosis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cv_other.setVisibility(View.GONE);
                add_diagnosis.setVisibility(View.VISIBLE);
                add_diagnosis2.setVisibility(View.GONE);
                tv_other_diag.setText("");
                SharedPref.setStringValue(SharedPref.USER, SharedPref.ADD_OTHER_DIAG, "", context);
            }
        });

        imgBtn_delete_diagnosis_second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cv_other_second.setVisibility(View.GONE);
                tv_other_diag.setText("");
            }
        });

        cb_informed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isBasicTest) {
                    if (basicTestList.isEmpty())
                        cb_informed.setChecked(false);
                    implement.setCheckBox(cb_informed, btn_proceed);
                } else if (isProcedure) {
                    if (getSelectedProcedures().isEmpty())
                        cb_informed.setChecked(false);
                    implement.setCheckBox(cb_informed, btn_proceed);
                } else if (isOtherTest) {
                    OTHER_SELECTED_DIAGNOSIS = SharedPref.getStringValue(SharedPref.USER, SharedPref.ADD_OTHER_DIAG, context);
                    if (arrayOther.isEmpty() && !OTHER_SELECTED_DIAGNOSIS.equals("")) {
                        cb_informed.setChecked(false);
                        implement.setCheckBox(cb_informed, btn_proceed);
                    } else if (getSelectedPrimary().isEmpty()) {
                        cb_informed.setChecked(false);
                        implement.setCheckBox(cb_informed, btn_proceed);
                    } else {
                        implement.setCheckBox(cb_informed, btn_proceed);
                    }
                } else {
                    implement.setCheckBox(cb_informed, btn_proceed);
                }
            }
        });

        // implement.loadProcedureBasedonDiagnosis(Constant.ADAPTER_PROC_IN_PATIENT, "Dx1374", pb_in_patient_procedure, tv_in_patient_procedure);

        implement.setLoadingPrimaryGone(tv_other_proc, pb_other_diag, true);
        implement.setLoadingPrimaryGone(tv_primary, pb_primary_diag, true);
        implement.setLoadingPrimaryGone2(tv_primary_diag_2, pb_primary_diag_2, true);

        //display for current coord account
        contact12.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.CONTACT_NO, context));
        contact14.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.EMAIL, context));
        return view;
    }


    @OnClick({R.id.imgBtn_viewLoa, R.id.imgBtn_delete_data, R.id.tv_admitted, R.id.btn_cancel, R.id.btn_proceed, R.id.tv_doctor, R.id.tv_diagnosis
            , R.id.rl_procDiag, R.id.btn_back, R.id.rl_primary, R.id.tv_primary_diag, R.id.rl_other_diag, R.id.rl_other_diag_second, R.id.tv_other_diag, R.id.tv_other_diag_second, R.id.tv_inpatient_doctor, R.id.tv_inpatient_procedure,
            R.id.tv_inpatient_diagnosis, R.id.tv_admitted_time, R.id.et_input_room_number, R.id.tv_primary_add,
            R.id.tv_other_proc_Add, R.id.tv_primary_diag_2_add, R.id.et_input_category, R.id.tv_othertest_request_hosp, R.id.tv_othertest_request_doc, R.id.btn_inpatient_doctor, R.id.btn_inpatient_diagnosis, R.id.btn_inpatient_procedure})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.et_input_category:
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_ROOM_CATEGORY));
                break;

            case R.id.tv_primary_diag_2_add:
                SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_ORIGIN, Constant.ADAPTER_PROCEDURE, context);
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_PROCEDURE, proceduresPrimary2List));
                break;
            case R.id.tv_other_proc_Add:

                SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_ORIGIN, Constant.ADAPTER_OTHER, context);
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_OTHER, proceduresOtherList));

                break;
            case R.id.tv_primary_add:

                SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_ORIGIN, Constant.ADAPTER_PRIMARY, context);
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_PRIMARY, proceduresPrimaryList));

                break;
            case R.id.btn_inpatient_doctor:

                SharedPref.setStringValue(SharedPref.USER, SharedPref.INPATIENT_DOCTOR_ORIGIN, Constant.ADAPTER_INPATIENT_DOCTOR, context);
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_INPATIENT_DOCTOR, doctorModelInsertions, "", ""));

                break;
            case R.id.btn_inpatient_diagnosis:

                SharedPref.setStringValue(SharedPref.USER, SharedPref.INPATIENT_DIAGNOSIS_ORIGIN, Constant.ADAPTER_INPATIENT_DIAGNOSIS, context);
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_INPATIENT_DIAGNOSIS, diagnosisModelInsertions, "", "", ""));

                break;
            case R.id.btn_inpatient_procedure:

                SharedPref.setStringValue(SharedPref.USER, SharedPref.INPATIENT_PROCEDURE_ORIGIN, Constant.ADAPTER_INPATIENT_PROCEDURE, context);
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_INPATIENT_PROCEDURE, procedureModelInsertions, "", "", "", ""));

                break;
            case R.id.et_input_room_number:
                SharedPref.setStringValue(SharedPref.USER, SharedPref.ROOM_ORIGIN, Constant.ADAPTER_ROOM_PLANS, context);
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_ROOM_PLANS));

                break;
//            case R.id.cb_informed:
//                if(isBasicTest){
//                    if(getSelectedBasicTests().isEmpty())
//                        cb_informed.setChecked(false);
//                        implement.setCheckBox(cb_informed, btn_proceed);
//                }else if(isProcedure){
//                    if(getSelectedProcedures().isEmpty())
//                        cb_informed.setChecked(false);
//                        implement.setCheckBox(cb_informed, btn_proceed);
//                }else if(isOtherTest) {
//                    OTHER_SELECTED_DIAGNOSIS = SharedPref.getStringValue(SharedPref.USER,SharedPref.ADD_OTHER_DIAG,context);
//                    if(arrayOther.isEmpty() && !OTHER_SELECTED_DIAGNOSIS.equals("") ){
//                        cb_informed.setChecked(false);
//                        implement.setCheckBox(cb_informed, btn_proceed);
//                    }else if (getSelectedPrimary().isEmpty()) {
//                        cb_informed.setChecked(false);
//                        implement.setCheckBox(cb_informed, btn_proceed);
//                    }else {
//                        implement.setCheckBox(cb_informed, btn_proceed);
//                    }
//                }else {
//                    implement.setCheckBox(cb_informed, btn_proceed);
//                }
//                break;
            case R.id.tv_admitted:
                int mDate[] = new int[3];
                final Calendar c = Calendar.getInstance();
                mDate[0] = c.get(Calendar.YEAR); // current year
                mDate[1] = c.get(Calendar.MONTH); // current month
                mDate[2] = c.get(Calendar.DAY_OF_MONTH); // current day

                DateAdmitted.getDate(getContext(), tv_admitted, mDate);
                break;

            case R.id.btn_cancel:
                fragment = new CoordinatorMenuButton();
                fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.left_to_right, R.anim.right_to_left);
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent("Close_consult"));
                break;

            case R.id.btn_proceed:
                if (isConsultation) {
                    implement.testConsultation(tv_doctor, et_problem, 0);
                } else if (isMaternity) {
                    implement.testConsultation(tv_doctor, et_problem, 1);
                } else if (isBasicTest) {
                    implement.testBasicTest(Constant.FROM_BASIC_TEST, getAllTestAndProcedureCode(), rv_procedures);
                } else if (isOtherTest) {
                    implement.testOtherTest(Constant.FROM_OTHER_TEST, tv_othertest_request_doc, arrayPrimary, arrayOther, tv_primary_diag, tv_other_diag, tv_other_diag_second);
                } else if (isInPatient) {
                    //todo: part of inpatient
                    getAllSelectedDoctor().size();
                    getAllSelectedDiagnosis().size();
                    getAllSelectedProcedure().size();
                    implement.testInPatient(getAllSelectedDoctor(), getAllSelectedDiagnosis(), tv_admitted, tv_admitted_time);
                } else if (isProcedure) {
                    implement.testProcedure(Constant.FROM_PROCEDURES, tv_doctor, tv_diagnosis);
                } else if (isER) {
                    implement.testER(tv_admitted, et_er_reason);
                }
                break;

            case R.id.tv_doctor:
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_DOCTORS,
                        implement.getOrigin(isMaternity)));
                break;

            case R.id.tv_othertest_request_doc:
                if (!tv_othertest_request_hosp.getText().toString().isEmpty()) {
                    EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_UNFILTERED_DOCTORS,
                            implement.getOrigin(isMaternity)));
                } else {
                    dialogCustom.showMe(context, "", dialogCustom.Select_hosp_first, 1);
                }


                break;
            case R.id.tv_diagnosis:
                SharedPref.setStringValue(SharedPref.USER, SharedPref.DIAG_ORIGIN, Constant.ADAPTER_DIAGNOSIS, context);
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_DIAGNOSIS));

                break;
            case R.id.rl_procDiag:
                if (isPrimary_2_Selected) {
                    if (isPrimary_2_Available) {
                        SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_ORIGIN, Constant.ADAPTER_PROCEDURE, context);
                        EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_PROCEDURE, proceduresPrimary2List));
                    }
                } else {
                    dialogCustom.showMe(context, "", dialogCustom.Select_diag_first, 1);
                }
                break;

            case R.id.rl_primary:
                // test if coming from basic test
                Log.d("DATA_1", isBasicTest + "");
                Log.d("DATA_2", arrayBasicTest.size() + "");
                if (isBasicTest && arrayBasicTest.size() != 0) {
                    proceduresPrimaryList = implement.getProcList(arrayBasicTest);
                    isPrimarySelected = true;
                    isPrimaryAvailable = true;
                    cb_informed.setClickable(true);
                }
                //test if diagnosis is already selected
                if (isPrimarySelected) {
                    if (isPrimaryAvailable) {
                        SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_ORIGIN, Constant.ADAPTER_PRIMARY, context);
                        EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_PRIMARY, proceduresPrimaryList));
                    }
                } else {
                    dialogCustom.showMe(context, "", dialogCustom.Select_diag_first, 1);
                }

                break;
            case R.id.btn_back:
                getActivity().finish();
                break;
            case R.id.rl_other_diag:

                Log.d("BASIC_TEST_SIZE", arrayBasicTest.size() + "");
                if (isBasicTest && arrayBasicTest.size() != 0) {
                    proceduresOtherList = implement.getProcList(arrayBasicTest);
                    isOtherSelected = true;
                    isOtherAvailable = true;
                }
                if (isOtherSelected) {
                    if (isOtherAvailable) {
                        SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_ORIGIN, Constant.ADAPTER_OTHER, context);
                        EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_OTHER, proceduresOtherList));
                    }
                } else {
                    dialogCustom.showMe(context, "", dialogCustom.Select_diag_second, 1);
                }

                break;

            case R.id.rl_other_diag_second:
                Log.d("BASIC_TEST_SIZE", arrayBasicTest.size() + "");
                if (isBasicTest && arrayBasicTest.size() != 0) {
                    proceduresOtherList = implement.getProcList(arrayBasicTest);
                    isOtherSelectedSecond = true;
                    isOtherAvailable = true;
                }
                if (isOtherSelectedSecond) {
                    if (isOtherAvailable) {
                        SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_ORIGIN, Constant.ADAPTER_OTHER_2, context);
                        EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_OTHER_2, proceduresOtherList));
                    }
                } else {
                    dialogCustom.showMe(context, "", dialogCustom.Select_diag_second, 1);
                }
                break;

            case R.id.tv_primary_diag:
                SharedPref.setStringValue(SharedPref.USER, SharedPref.DIAG_ORIGIN, Constant.ADAPTER_PRIME_DIAG, context);
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_PRIME_DIAG));
                break;
            case R.id.tv_othertest_request_hosp:
                SharedPref.setStringValue(SharedPref.USER, SharedPref.HOSP_ORIGIN, Constant.ADAPTER_UNFILTERED_HOSP, context);
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_UNFILTERED_HOSP));
                break;
            case R.id.tv_other_diag:

                SharedPref.setStringValue(SharedPref.USER, SharedPref.DIAG_ORIGIN, Constant.ADAPTER_OTHER_DIAG, context);
                SharedPref.setStringValue(SharedPref.USER, SharedPref.ADD_OTHER_DIAG, "TRUE", context);
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_OTHER_DIAG));
                break;

            case R.id.tv_other_diag_second:

                SharedPref.setStringValue(SharedPref.USER, SharedPref.DIAG_ORIGIN, Constant.ADAPTER_OTHER_DIAG_SECOND, context);
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_OTHER_DIAG_SECOND));
                break;

            case R.id.imgBtn_delete_data:
                ll_reference.setVisibility(View.GONE);
                deleteData();
                break;

            case R.id.imgBtn_viewLoa:

                isPreventingDeleteFromOnStop = true;
                Intent gotoReq = new Intent(getContext(), LoaDisplayActivity.class);
                gotoReq.putExtra("LOALIST", loaFillUpMOdel);
//                gotoReq.putExtra(SearchToCoordinatorMenu.GENDER, GenderPicker.setGender(Integer.valueOf(SharedPref.getStringValue(SharedPref.USER, SharedPref.GENDER, context))));
//                gotoReq.putExtra(SearchToCoordinatorMenu.AGE, SharedPref.getStringValue(SharedPref.USER, SharedPref.AGE, context));


                startActivity(gotoReq);
                break;

            case R.id.tv_inpatient_doctor:
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_INPATIENT_DOCTOR,
                        implement.getOrigin(isMaternity)));
                break;
            case R.id.tv_inpatient_procedure:
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_INPATIENT_PROCEDURE,
                        implement.getOrigin(isMaternity)));
                break;

            case R.id.tv_inpatient_diagnosis:
//                SharedPref.setStringValue(SharedPref.USER, SharedPref.DIAG_ORIGIN, Constant.ADAPTER_DIAG_IN_PATIENT, context);
//                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_DIAG_IN_PATIENT));
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_INPATIENT_DIAGNOSIS,
                        implement.getOrigin(isMaternity)));

                break;

            case R.id.tv_admitted_time:
                Log.d("trigger", "trigger");
                TimepickerSet.getTime(context, tv_admitted_time, Constant.FROM_IN_PATIENT_AVAIL);
                break;


        }
    }

    private void setProcPrimary2Selected(ArrayList<TestsAndProcedures> arrayPrimary_2) {

        cb_informed.setChecked(false);
        implement.setCheckBox(cb_informed, btn_proceed);

        arrayProcedure.clear();
        arrayProcedure.addAll(arrayPrimary_2);
        primaryProcedures.notifyDataSetChanged();
        implement.setProceduresAvailability(arrayProcedure, rv_noProcAvailable, rv_procedures
                , tv_total_price, tv_primary_diag_2_add);
    }


    private void setOtherDiagSelected(DiagnosisList diagnosisList) {
        tv_other_diag.setText(diagnosisList.getDiagDesc());
    }

    private void setOtherDiagSelectedSecond(DiagnosisList diagSelectedSecond) {
        tv_other_diag_second.setText(diagSelectedSecond.getDiagDesc());
    }

    private void setPrimaryDiagSelected(DiagnosisList diagnosisList) {
        tv_inpatient_diagnosis.setText(diagnosisList.getDiagDesc());
        // to edit er
//        tv_primary_diag_in_patient.setText("ABDOMINAL BLUNT INJURY");
//        tv_primary_diag.setText("ABDOMINAL BLUNT INJURY");
        tv_primary_diag.setText(diagnosisList.getDiagDesc());
    }

    private void setDiagnosisSelected(DiagnosisList diagnosisList) {
        tv_diagnosis.setText(diagnosisList.getDiagDesc());
    }

    private void setUnfilteredHospital(Hospital hospital) {
        tv_othertest_request_hosp.setText(hospital.getHospitalName());
    }

    private void setProcPrimarySelected(ArrayList<TestsAndProcedures> arrayProc) {
        cb_informed.setChecked(false);
        implement.setCheckBox(cb_informed, btn_proceed);
        arrayPrimary.clear();
        arrayPrimary.addAll(arrayProc);
        primaryProcedures.notifyDataSetChanged();
        implement.setProceduresAvailability(arrayPrimary, rl_primary, rv_primary
                , tv_total_price, tv_primary_add);
    }

//    public void setBasicTest(ArrayList<BasicTests> arrayTest){
//        cb_informed.setChecked(false);
//        implement.setCheckBox(cb_informed, btn_proceed);
//        testsArrayList.clear();
//        testsArrayList.addAll(arrayTest);
//        basictestAdapter.notifyDataSetChanged();
//    }

    private void setProcOtherSelected(ArrayList<TestsAndProcedures> arrayProc) {
        cb_informed.setChecked(false);
        implement.setCheckBox(cb_informed, btn_proceed);
        arrayOther.clear();
        arrayOther.addAll(arrayProc);
        otherProcedure.notifyDataSetChanged();
        implement.setProceduresAvailability(arrayOther, rl_other_diag, rv_other_diag
                , tv_total_price, tv_other_proc_Add);
    }

    private void setProcOtherSelectedSecond(ArrayList<TestsAndProcedures> arrayProc) {
        cb_informed.setChecked(false);
        implement.setCheckBox(cb_informed, btn_proceed);
        arrayOtherSecond.clear();
        arrayOtherSecond.addAll(arrayProc);
        otherProcedureSecond.notifyDataSetChanged();
        implement.setProceduresAvailability(arrayOtherSecond, rl_other_diag_second, rv_other_diag_second
                , tv_total_price, tv_other_proc_Add_second);
    }


    private void setProcedures() {
        tv_diag_type.setText(getString(R.string.procedures));
        tv_hospital.setVisibility(View.GONE);
        tv_hospital_tag.setVisibility(View.GONE);
        cv_other.setVisibility(View.GONE);
        cv_other_second.setVisibility(View.GONE);
        cv_prime.setVisibility(View.GONE);
        tv_title.setText(R.string.procedures);
        et_problem.setVisibility(View.GONE);
        tv_problem_tag.setVisibility(View.GONE);
        isMaternity = false;
        isConsultation = false;
        isOtherTest = false;
        isBasicTest = false;
        isInPatient = false;
        isProcedure = true;
        implement.setOtherProcedurePanelGone(fr_otherProc, false);
        implement.setInputFieldsGone(true, ll_setInputFields);
        EventBus.getDefault().post(new NavigatorActivity.MessageEvent("PROCEDURE"));
    }

    private void fillUpDoctorDataandMemberID(LoaList loaFillUpMOdel) {
        String doctorName = dataBaseHandler.getDoctorName(loaFillUpMOdel.getDoctorCode());
        String doctorSpec = dataBaseHandler.getDoctorSpec(loaFillUpMOdel.getDoctorCode());
        String doctorHospital = dataBaseHandler.getDoctorHosp(loaFillUpMOdel.getDoctorCode());

        setDoctorSelected(doctorSpec, loaFillUpMOdel.getMemberCode(),
                loaFillUpMOdel.getHospitalCode(),
                loaFillUpMOdel.getDoctorCode(),
                doctorName,
                doctorHospital);
    }

    private void testDuplicateRequestProcedures(BasicTestOrOtherTestReturn loaReturn, String ORIGIN) {
//        System.out.println("response codeeee" + loaReturn.getResponseCode());
        if (loaReturn.getResponseCode().equals("210")) {
            Log.d("BATCHCODE", loaReturn.toString());
            dialogCustom.showMeValidateReqforBasicOtherTest(loaReturn, getContext()
                    , callbackConfirm, ORIGIN, loaReturn.getBatchCode());
        } else if (loaReturn.getResponseCode().equals("220")) {
            dialogCustom.showMeValidateReqforBasicOtherTest(loaReturn, getContext()
                    , callbackConfirm, ORIGIN, loaReturn.getBatchCode());
            //REQUEST DENIED
//            dialogCustom.showMeDuplicateSameDetailsReq(context, loaReturn.getResponseDesc());
        } else {
            dataToDisplay(loaReturn, ORIGIN, 2);
        }
    }

    private void testDuplicateRequestBasicOrOtherTest(BasicTestOrOtherTestReturn loaReturn, String ORIGIN) {
//        System.out.println("response codeeee" + loaReturn.getResponseCode());
        if (loaReturn.getResponseCode().equals("210")) {
            Log.d("BATCHCODE", loaReturn.toString());
            dialogCustom.showMeValidateReqforBasicOtherTest(loaReturn, getContext()
                    , callbackConfirm, ORIGIN, loaReturn.getBatchCode());
        } else if (loaReturn.getResponseCode().equals("220")) {
            dialogCustom.showMeValidateReqforBasicOtherTest(loaReturn, getContext()
                    , callbackConfirm, ORIGIN, loaReturn.getBatchCode());
// request denied
//            dialogCustom.showMeDuplicateSameDetailsReq(context, loaReturn.getResponseDesc());
        } else {
            dataToDisplay(loaReturn, ORIGIN, 0);
        }


    }

    // Added Aug 11, 2017 -- Lawrence Mataga for Sprint 6 part 1 SIT
//    private void testDuplicateRequestBasic(BasicTestResponse loaReturn, String ORIGIN) {
//        System.out.println("response codeeee" + loaReturn.getResponseCode());
//        if (loaReturn.getResponseCode().equals("210")) {
//            Log.d("BATCHCODE", loaReturn.toString());
//            dialogCustom.showMeValidateReqforBasicTest(loaReturn, getContext()
//                    , callbackConfirm, ORIGIN, loaReturn.getApprovalNo());
//        } else if (loaReturn.getResponseCode().equals("220")) {
//            dialogCustom.showMeValidateReqforBasicTest(loaReturn, getContext()
//                    , callbackConfirm, ORIGIN, loaReturn.getApprovalNo());
////            dialogCustom.showMeDuplicateSameDetailsReq(context, loaReturn.getResponseDesc());
//        } else {
//            dataToDisplayBasicTest(loaReturn, ORIGIN, 3);
//        }
//    }

    private void dataToDisplayBasicTest(BasicTestOrOtherTestReturn getOtherTestReturnModel, String ORIGIN, Integer marker) {

        for (BasicTests basicTest : testsArrayList) {
            if (basicTest.isSelected()) {

            }
        }

        ArrayList<RequestBasicTest> basictest = new ArrayList<>();
//            basictest.addAll(GetOnlyApprovedData.updateGetOnlyApprovedProc3(loaReturn.getData().getRequestBasicTest()));
//            String header = implement.getRemarks(basictest.size(), loaReturn.getData().getDiagnosisClinicProcedures().size());

        sentDatatoResultfromBasicTest(getOtherTestReturnModel, ORIGIN);
//            } else {

//
//                fragment = ApprovalActivity.newInstance2(basictest, ORIGIN, header);
//                fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.setCustomAnimations(R.anim.left_to_right, R.anim.right_to_left);
//                fragmentTransaction.replace(R.id.container, fragment);
//                fragmentTransaction.commit();
//
//            }
    }


    private void dataToDisplay(BasicTestOrOtherTestReturn loaReturn, String ORIGIN, Integer marker) {
        // FOR OTHER TEST TESTING DUPLICATE
        if (marker == 0) {
            loaReturn.getLoaList();
            ArrayList<DiagnosisProcedures> procedures = new ArrayList<>();
            procedures.addAll(GetOnlyApprovedData.updateGetOnlyApprovedProc(loaReturn.getData().getDiagnosisProcedures()));
            String header = implement.getRemarks(procedures.size(), loaReturn.getData().getDiagnosisProcedures().size());
//            primaryDiagOtherTest = loaReturn.getData().getDiagnosisProcedures().get(loaReturn.getData().getDiagnosisProcedures().size()-1).getMaceRequestTest().getPrimaryDiagnosisDesc();
            System.out.println("eto primary" + primaryDiag);

            ArrayList<DiagnosisProcedures> proceduresPending = new ArrayList<>();
            proceduresPending.addAll(GetOnlyApprovedData.updateGetOnlyPending(loaReturn.getData().getDiagnosisProcedures()));
            Intent gotoResult = new Intent(getContext(), ResultActivity.class);
            gotoResult.putExtra("DOCTOR", getDoctor);
            gotoResult.putExtra("DOCTOR_SPEC", getDoctorSpec);
            gotoResult.putExtra("REQUEST", Constant.OTHER_TEST);
            gotoResult.putExtra("RESPONCE_CODE", loaReturn.getResponseCode());
            gotoResult.putExtra("APPROVAL_NO", loaReturn.getApprovalNo());
            gotoResult.putExtra("RESPONCE_DESC", loaReturn.getResponseDesc());
//            gotoResult.putExtra("REQUESTED_HOSP",loaReturn.getLoaList().get);
            gotoResult.putExtra("LOALIST", loaReturn.getLoaList());
            gotoResult.putExtra("PRIMARY_ARRAY_LIST", procedures);
            gotoResult.putExtra("OTHER_ARRAY_LIST", proceduresPending);
            gotoResult.putExtra("REMARKS", loaReturn.getLoaList().getRemarks());
            gotoResult.putExtra("WITH_PROVIDER", "");
            gotoResult.putExtra("PRIMARY_DIAGNOSIS", primaryDiagOtherTest);
            gotoResult.putExtra("REQUEST_STATUS", loaReturn.getStatus());
            startActivity(gotoResult);
            loader.stopLoad();

            //PROCEDURE TESTING DUPLICATE
        } else if (marker == 2) {
            ArrayList<DiagnosisClinicProcedures> clinicProceduresApproved = new ArrayList<>();
            clinicProceduresApproved.addAll(GetOnlyApprovedData.updateGetOnlyApprovedProc2(loaReturn.getData().getDiagnosisClinicProcedures()));
            String header = implement.getRemarks(clinicProceduresApproved.size(), loaReturn.getData().getDiagnosisClinicProcedures().size());

            ArrayList<DiagnosisClinicProcedures> clinicProceduresPending = new ArrayList<>();
            clinicProceduresPending.addAll(GetOnlyApprovedData.updateGetOnlyApprovedProc3(loaReturn.getData().getDiagnosisClinicProcedures()));

            if (clinicProceduresApproved.size() != 0) {
                header = "APPROVED";
                sentDatatoResultfromBasicTestOrOtherTest(loaReturn, ORIGIN, header);
            } else if (clinicProceduresPending.size() != 0) {
                header = "PENDING";
                sentDatatoResultfromBasicTestOrOtherTest(loaReturn, ORIGIN, header);
            }

            // TODO: 8 23 17 removed the fragment, split screen for clinic procedure is not needed - jj
//            } else {

//                fragment = ApprovalActivity.newInstance2(procedures, ORIGIN, header);
//                fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.setCustomAnimations(R.anim.left_to_right, R.anim.right_to_left);
//                fragmentTransaction.replace(R.id.container, fragment);
//                fragmentTransaction.commit();
//
//            }
            //for BASIC TEST TESTING DUPLICATE
//        } else if (marker == 3) {
//            ArrayList<RequestBasicTest> basictest = new ArrayList<>();
////            basictest.addAll(GetOnlyApprovedData.updateGetOnlyApprovedProc3(loaReturn.getData().getRequestBasicTest()));
////            String header = implement.getRemarks(basictest.size(), loaReturn.getData().getDiagnosisClinicProcedures().size());
//
//            if (basictest.size() <= 0) {
//                sentDatatoResultfromBasicTestOrOtherTest(loaReturn, ORIGIN);
////            } else {
////
////
////                fragment = ApprovalActivity.newInstance2(basictest, ORIGIN, header);
////                fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
////                fragmentTransaction.setCustomAnimations(R.anim.left_to_right, R.anim.right_to_left);
////                fragmentTransaction.replace(R.id.container, fragment);
////                fragmentTransaction.commit();
////
////            }
        } else {
            ArrayList<DiagnosisClinicProcedures> procedures = new ArrayList<>();
            procedures.addAll(GetOnlyApprovedData.updateGetOnlyApprovedProc2(loaReturn.getData().getDiagnosisClinicProcedures()));
            String header = implement.getRemarks(procedures.size(), loaReturn.getData().getDiagnosisClinicProcedures().size());

            if (procedures.size() <= 0) {
                sentDatatoResultfromBasicTestOrOtherTest(loaReturn, ORIGIN, "");
            } else {

//            ll_request_details.setVisibility(View.VISIBLE);
                fragment = ApprovalActivity.newInstance2(procedures, ORIGIN, header);
                fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.left_to_right, R.anim.right_to_left);
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
            }
        }
    }


    private void testDuplicateRequest(LOAReturn loaReturn, String ORIGIN) {

        if (loaReturn.getResponseCode().equals("210")) {
            Log.d("BATCHCODE", loaReturn.toString());
            getLoaReturnRequests = loaReturn;
            dialogCustom.showMeValidateReq(loaReturn.getResponseDesc(), getContext()
                    , callbackConfirm, ORIGIN, loaReturn.getBatchCode());
        } else if (loaReturn.getResponseCode().equals("220")) {
            dialogCustom.showMeValidateReq(loaReturn.getResponseDesc(), getContext()
                    , callbackConfirm, ORIGIN, loaReturn.getBatchCode());
        } else {
            sentDatatoResult(loaReturn, ORIGIN);
        }
    }


    private void sentDatatoResultfromBasicTest(BasicTestOrOtherTestReturn getOtherTestReturnModel, String ORIGIN) {
        et_problem.setText("");
        tv_doctor.setText("");
        tv_primary_diag.setText("");
        cb_informed.setChecked(false);
        implement.setCheckBox(cb_informed, btn_proceed);
        Intent gotoResult = new Intent(getContext(), ResultActivity.class);
        gotoResult.putExtra("REQUEST", ORIGIN);
        gotoResult.putExtra("RESPONCE_CODE", getOtherTestReturnModel.getResponseCode());
        gotoResult.putExtra("APPROVAL_NO", getOtherTestReturnModel.getApprovalNo());
        gotoResult.putExtra("RESPONCE_DESC", getOtherTestReturnModel.getResponseDesc());
//        gotoResult.putExtra("PROCEDURE_DESC",loaReturn.get)
        gotoResult.putExtra("WITH_PROVIDER", "");
        gotoResult.putExtra("TOTALTESTAMOUNT", getOtherTestReturnModel.getTotalAmount());
        gotoResult.putExtra("REQUEST_STATUS", getOtherTestReturnModel.getStatus());
        startActivity(gotoResult);
        loader.stopLoad();
    }


    private void sentDatatoResultfromBasicTestOrOtherTest(BasicTestOrOtherTestReturn loaReturn, String ORIGIN, String clinicFilter) {

        et_problem.setText("");
        tv_doctor.setText("");
        tv_primary_diag.setText("");
        cb_informed.setChecked(false);
        implement.setCheckBox(cb_informed, btn_proceed);
        Intent gotoResult = new Intent(getActivity(), ResultActivity.class);

        if (ORIGIN.equals(Constant.OTHER_TEST)) {
            gotoResult.putExtra("DOCTOR", getDoctor);
            gotoResult.putExtra("DOCTOR_SPEC", getDoctorSpec);
            gotoResult.putExtra("REQUEST", Constant.OTHER_TEST);
            gotoResult.putExtra("RESPONCE_CODE", loaReturn.getResponseCode());
            gotoResult.putExtra("APPROVAL_NO", loaReturn.getApprovalNo());
            gotoResult.putExtra("RESPONCE_DESC", loaReturn.getResponseDesc());
//            gotoResult.putStringArrayListExtra("PRIMARY_ARRAY_LIST", procedures);
//            gotoResult.putStringArrayListExtra("OTHER_ARRAY_LIST", proceduresPending);
            gotoResult.putExtra("REMARKS", "");
            gotoResult.putExtra("WITH_PROVIDER", "");
            gotoResult.putExtra("PRIMARY_DIAGNOSIS", primaryDiagOtherTest);
            gotoResult.putExtra("DIAGNOSIS", loaReturn.getData().getDiagnosisProcedures().get(0).getMaceRequestOpDiag().getDiagDesc());

        } else if (ORIGIN.equals(Constant.BASIC_TEST)) {
            gotoResult.putExtra("DOCTOR", getDoctor);
            gotoResult.putExtra("DOCTOR_SPEC", getDoctorSpec);
            gotoResult.putExtra("REQUEST", Constant.BASIC_TEST);
            gotoResult.putExtra("RESPONCE_CODE", "201");
            gotoResult.putExtra("APPROVAL_NO", "");
            gotoResult.putExtra("RESPONCE_DESC", loaReturn.getResponseDesc());
            gotoResult.putExtra("RESPONCE_DESC", "");
            gotoResult.putExtra("REMARKS", "");
            gotoResult.putExtra("WITH_PROVIDER", "");
            gotoResult.putExtra("DIAGNOSIS", loaReturn.getData().getBasicTests().get(0).getProcedureDesc());
            gotoResult.putExtra("TOTALTESTAMOUNT", loaReturn.getTotalAmount());

        } else if (ORIGIN.equals(Constant.PROCEDURES)) {
            gotoResult.putExtra("DOCTOR", getDoctor);
            gotoResult.putExtra("DOCTOR_SPEC", getDoctorSpec);
            gotoResult.putExtra("REQUEST", Constant.PROCEDURES);
            gotoResult.putExtra("RESPONCE_CODE", loaReturn.getResponseCode());
            gotoResult.putExtra("APPROVAL_NO", loaReturn.getApprovalNo());
            gotoResult.putExtra("CLINIC_FILTER", clinicFilter);
            gotoResult.putExtra("RESPONCE_DESC", loaReturn.getResponseDesc());
            gotoResult.putExtra("REMARKS", loaReturn.getData().getDiagnosisClinicProcedures().get(0).getMaceRequestProcedure().getNotes());
            gotoResult.putExtra("WITH_PROVIDER", "");
            gotoResult.putExtra("DIAGNOSIS", loaReturn.getData().getDiagnosisClinicProcedures().get(0).getMaceRequestOpDiag().getDiagDesc());
            gotoResult.putExtra("ICD10CODE", loaReturn.getIcd10Code());
            gotoResult.putExtra("TOTALTESTAMOUNT", loaReturn.getTotalAmount());
            gotoResult.putExtra("REQUEST_STATUS", loaReturn.getStatus());

        }

        startActivity(gotoResult);
        loader.stopLoad();
    }

    private void sentDatatoResult(LOAReturn loaReturn, String ORIGIN) {
        Log.d("ORIGIN_END", ORIGIN);
        et_problem.setText("");
        tv_doctor.setText("");
        tv_primary_diag.setText("");
        cb_informed.setChecked(false);
        implement.setCheckBox(cb_informed, btn_proceed);
        Intent gotoResult = new Intent(getContext(), ResultActivity.class);

        if (ORIGIN.equals(Constant.CONSULTATION) ||
                ORIGIN.equals(Constant.MATERNITY)) {

            gotoResult.putExtra("REQUEST", REQUEST);
            gotoResult.putExtra("DOCTOR", getDoctor);
            gotoResult.putExtra("DOCTOR_SPEC", getDoctorSpec);
            gotoResult.putExtra("PROBLEM", getProblemConsultation);
            gotoResult.putExtra("RESPONCE_CODE", "200");
            gotoResult.putExtra("APPROVAL_NO", loaReturn.getApprovalNo());
            gotoResult.putExtra("RESPONCE_DESC", loaReturn.getResponseDesc());
            gotoResult.putExtra("REMARKS", loaReturn.getRemarks());
            gotoResult.putExtra("WITH_PROVIDER", loaReturn.getWithProvider());
            gotoResult.putExtra("BATCH", loaReturn.getBatchCode());
            gotoResult.putExtra("DIAGNOSIS", getDiagnosis);
//            gotoResult.putExtra("DATEADMITTED", DateConverter.convertDatetoyyyyMMdd(dateAdmitted));


        } else if (ORIGIN.equals(Constant.PROCEDURES)) {
            Log.d("PROCEDURES", loaReturn.getResponseCode());
            Log.d("PROCEDURES", loaReturn.getResponseDesc());
            Log.d("PROCEDURES", loaReturn.getRemarks());
            gotoResult.putExtra("DOCTOR", getDoctor);
            gotoResult.putExtra("DOCTOR_SPEC", getDoctorSpec);
            gotoResult.putExtra("REQUEST", "OTHER_TEST");
            gotoResult.putExtra("RESPONCE_CODE", loaReturn.getResponseCode());
            gotoResult.putExtra("APPROVAL_NO", loaReturn.getApprovalNo());
            gotoResult.putStringArrayListExtra("PROCEDURE_LIST", proceduresToDisplay);
            gotoResult.putExtra("RESPONCE_DESC", loaReturn.getResponseDesc());
            gotoResult.putExtra("REMARKS", loaReturn.getRemarks());
            gotoResult.putExtra("WITH_PROVIDER", loaReturn.getWithProvider());
            gotoResult.putExtra("DIAGNOSIS", getDiagnosis);

        } else if (ORIGIN.equals(Constant.OTHER_TEST)) {
            Log.d("OTHER_TEST", loaReturn.getResponseCode());
            Log.d("OTHER_TEST", loaReturn.getResponseDesc());
            Log.d("OTHER_TEST", loaReturn.getRemarks());

            gotoResult.putExtra("DOCTOR", getDoctor);
            gotoResult.putExtra("DOCTOR_SPEC", getDoctorSpec);
            gotoResult.putExtra("REQUEST", "OTHER_TEST");
            gotoResult.putExtra("RESPONCE_CODE", loaReturn.getResponseCode());
            gotoResult.putExtra("APPROVAL_NO", loaReturn.getApprovalNo());
            gotoResult.putStringArrayListExtra("PROCEDURE_LIST", proceduresToDisplay);
            gotoResult.putExtra("RESPONCE_DESC", loaReturn.getResponseDesc());
            gotoResult.putExtra("REMARKS", loaReturn.getRemarks());
            gotoResult.putExtra("WITH_PROVIDER", loaReturn.getWithProvider());
            gotoResult.putExtra("DIAGNOSIS", getDiagnosis);

        } else if (ORIGIN.equals(Constant.BASIC_TEST)) {
            gotoResult.putExtra("DOCTOR", getDoctor);
            gotoResult.putExtra("DOCTOR_SPEC", getDoctorSpec);
            gotoResult.putExtra("REQUEST", "BASIC_TEST");
            gotoResult.putExtra("RESPONCE_CODE", loaReturn.getResponseCode());
            gotoResult.putExtra("APPROVAL_NO", loaReturn.getApprovalNo());
            gotoResult.putExtra("RESPONCE_DESC", loaReturn.getResponseDesc());
            gotoResult.putExtra("REMARKS", loaReturn.getRemarks());
            gotoResult.putExtra("WITH_PROVIDER", loaReturn.getWithProvider());
            gotoResult.putExtra("DIAGNOSIS", getDiagnosis);
        }


        startActivity(gotoResult);
        loader.stopLoad();

    }


    //DUPLICATE REQUEST SEND REQUEST
    @Override
    public void loaDuplicateSendApproval(String ORIGIN, String BatchCode) {
        loader.startLad();
        AvailServicesAPICall.sendConfirmConsult(BatchCode, callback, ORIGIN);
    }

    @Override
    public void onWaiverAcceptListener(String isTicked) {

    }

    @Override
    public void loaDuplicateSendApprovalforBasicTestAndOtherTest(String origin, String batchCode, BasicTestOrOtherTestReturn loaReturn) {
        loader.startLad();
        otherTestBasicTestRequest = loaReturn;
        AvailServicesAPICall.sendConfirmConsult(batchCode, callback, origin);
    }


    @Override
    public void loaDuplicateSendApprovalforBasicTest(String origin, String batchCode, BasicTestResponse loaReturn) {
        loader.startLad();
        basicTestResponse = loaReturn;
        AvailServicesAPICall.sendConfirmBasic(batchCode, callback, origin);
    }


    // TESTED IF DUPLICATE DATA STILL ACCEPTED
    @Override
    public void onSuccessConfirm(String ORIGIN) {
        loader.stopLoad();

        if (ORIGIN.equals(Constant.CONSULTATION) || ORIGIN.equals(Constant.MATERNITY)) {
            getLoaReturnRequests.setResponseCode("200");
            sentDatatoResult(getLoaReturnRequests, ORIGIN);
        } else {
            otherTestBasicTestRequest.setResponseCode("200");
            dataToDisplay(otherTestBasicTestRequest, ORIGIN, 1);
        }


    }

    @Override
    public void onSuccessConfirmBasic(String ORIGIN) {
        loader.stopLoad();
        basicTestResponse.setResponseCode("200");
//        dataToDisplayBasicTest(basicTestResponse, ORIGIN, 3);
    }


    @Override
    public void onErrorConfirm(String message) {
        loader.stopLoad();
        dialogCustom.showMe(getContext(), dialogCustom.unknown, ErrorMessage.setErrorMessage(message), 1);
    }

    @Override
    public void setInpatientUI() {

        getMEMBER_ID = SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, context);
        tv_title.setText(getString(R.string.admission_det));
        isMaternity = false;
        isConsultation = false;
        isOtherTest = false;
        isInPatient = true;
        isBasicTest = false;
        isProcedure = false;

        implement.setOtherProcedurePanelGone(fr_otherProc, true);
        implement.setInputFieldsGone(false, ll_setInputFields);
        implement.setAllDoctorDataInpatientToFalse(dataBaseHandler);
        implement.setAllDiagnosisDataInpatientToFalse(dataBaseHandler);
        implement.setAllProcedureDataInpatientToFalse(dataBaseHandler);

        doctorModelInsertions.clear();
        diagnosisModelInsertions.clear();
        doctorModelInsertions.clear();

        tv_doctor_tag.setVisibility(View.GONE);
        tv_doctor.setVisibility(View.GONE);
        tv_problem_tag.setVisibility(View.GONE);
        et_problem.setVisibility(View.GONE);
        btn_cancel.setVisibility(View.GONE);
        btn_proceed.setText("Submit");

        //ll_validate.setVisibility(View.GONE);
        //btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        //btn_proceed.setEnabled(true);
        //btn_proceed.setClickable(true);
        ll_er.setVisibility(View.GONE);
        ll_loa_form.setVisibility(View.VISIBLE);
        cv_other.setVisibility(View.GONE);
        cv_other_second.setVisibility(View.GONE);
        cv_prime.setVisibility(View.GONE);
        ll_reference.setVisibility(View.GONE);


        //added functions -jhay jhay
        tv_inpatient_doctor.setVisibility(View.VISIBLE);
        rv_inpatient_doctor.setVisibility(View.GONE);
        btn_inpatient_doctor.setVisibility(View.GONE);


        tv_inpatient_diagnosis.setVisibility(View.VISIBLE);
        rv_inpatient_diagnosis.setVisibility(View.GONE);
        btn_inpatient_diagnosis.setVisibility(View.GONE);

        tv_inpatient_procedure.setVisibility(View.VISIBLE);
        ll2_inpatient_procedure.setVisibility(View.GONE);
        btn_inpatient_procedure.setVisibility(View.GONE);

    }

    @Override
    public void setOutpatientUI() {
        implement.loadProcedureBasedonDiagnosis(Constant.FROM_OTHER_PROC, pb_other_diag, tv_other_proc);
        implement.loadProcedureBasedonDiagnosis(Constant.FROM_PRIMARY_PROC, pb_primary_diag, tv_primary);

        tv_doctor_tag.setVisibility(View.VISIBLE);
        tv_doctor.setVisibility(View.VISIBLE);
        btn_cancel.setVisibility(View.VISIBLE);
        btn_proceed.setText(getString(R.string.request));
        ll_validate.setVisibility(View.VISIBLE);
        ll_er.setVisibility(View.GONE);
        ll_loa_form.setVisibility(View.VISIBLE);
    }

    @Override
    public void setErUi() {

        getMEMBER_ID = SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, context);
        tv_title.setText(getString(R.string.admission_det));
        isMaternity = false;
        isConsultation = false;
        isOtherTest = false;
        isInPatient = false;
        isBasicTest = false;
        isProcedure = false;
        isER = true;
        implement.setOtherProcedurePanelGone(fr_otherProc, true);
        implement.setInputFieldsGone(false, ll_setInputFields);
        tv_doctor_tag.setVisibility(View.GONE);
        tv_doctor.setVisibility(View.GONE);
        tv_problem_tag.setVisibility(View.GONE);
        et_problem.setVisibility(View.GONE);
        btn_cancel.setVisibility(View.GONE);
        btn_proceed.setText("Submit");
        tv_inpatient_diagnosis.setText("ABDOMINAL BLUNT INJURY");
        tv_primary_diag.setText("ABDOMINAL BLUNT INJURY");

        tv_inpatient_doctor.setText("ABAD STABTOS, SALVADOR");

        //ll_validate.setVisibility(View.GONE);
        //btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        //btn_proceed.setEnabled(true);
        //btn_proceed.setClickable(true);
        ll_er.setVisibility(View.GONE);
        ll_loa_form.setVisibility(View.VISIBLE);
        et_input_room_number.setVisibility(View.GONE);
        et_input_category.setVisibility(View.GONE);
        et_input_number.setVisibility(View.GONE);
        et_input_room_number_label.setVisibility(View.GONE);
        et_input_category_label.setVisibility(View.GONE);
        et_input_number_label.setVisibility(View.GONE);
        tv_er_reason_label.setVisibility(View.VISIBLE);
        et_er_reason.setVisibility(View.VISIBLE);
        tv_inpatient_diagnosis.setVisibility(View.GONE);
        ll_primary_ip_label.setVisibility(View.GONE);
        ll_attending_doc.setVisibility(View.GONE);
        tv_inpatient_doctor.setVisibility(View.GONE);
        cv_other.setVisibility(View.GONE);
        cv_prime.setVisibility(View.GONE);
        ll_reference.setVisibility(View.GONE);
        tv_admitteddate.setVisibility(View.GONE);
        tv_admitteddateer.setVisibility(View.VISIBLE);
        btn_proceed.setText("Submit ER");
//        btn_dischargeER.setVisibility(View.VISIBLE);
//        btn_dischargeERToInPatient.setVisibility(View.VISIBLE);
//        btn_dischargeERToOutPatient.setVisibility(View.VISIBLE);


    }

    @Override
    public void setErUi2() {
        getMEMBER_ID = SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, context);
        tv_title.setText(getString(R.string.admission_det));
        isMaternity = false;
        isConsultation = false;
        isOtherTest = false;
        isInPatient = false;
        isBasicTest = false;
        isProcedure = false;
        isER = true;
        implement.setOtherProcedurePanelGone(fr_otherProc, true);
        implement.setInputFieldsGone(false, ll_setInputFields);
        tv_doctor_tag.setVisibility(View.GONE);
        tv_doctor.setVisibility(View.GONE);
        tv_problem_tag.setVisibility(View.GONE);
        et_problem.setVisibility(View.GONE);
        btn_cancel.setVisibility(View.GONE);
        btn_proceed.setText("Submit");

        //ll_validate.setVisibility(View.GONE);
        //btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        //btn_proceed.setEnabled(true);
        //btn_proceed.setClickable(true);
        ll_er.setVisibility(View.GONE);
        ll_loa_form.setVisibility(View.VISIBLE);
        et_input_room_number.setVisibility(View.GONE);
        et_input_category.setVisibility(View.GONE);
        et_input_number.setVisibility(View.GONE);
        et_input_room_number_label.setVisibility(View.GONE);
        et_input_category_label.setVisibility(View.GONE);
        et_input_number_label.setVisibility(View.GONE);
        cv_other.setVisibility(View.GONE);
        cv_other_second.setVisibility(View.GONE);
        cv_prime.setVisibility(View.GONE);
        ll_reference.setVisibility(View.GONE);
    }


    @Override
    public void onDoctorNotAvailable() {
        dialogCustom.showMe(getContext(), dialogCustom.unknown, getString(R.string.no_doctor), 1);

    }

    @Override
    public void noReason() {
        dialogCustom.showMe(getContext(), dialogCustom.unknown, getString(R.string.no_reason), 1);

    }


    @Override
    public void onProblemNotAvailable() {
        dialogCustom.showMe(getContext(), dialogCustom.unknown, getString(R.string.no_problem), 1);
    }


    // FILL UP DATA
    @Override
    public void basicTestFillUpFields(LoaList loaFillUpMOdel) {
        /** TODO
         WAIT FOR BACKEND
         FOR LISTING
         **/
        fillUpDoctorDataandMemberID(loaFillUpMOdel);
    }


    @Override
    public void isOtherProcedureFillUpFields(LoaList loaFillUpMOdel) {

        isFilledUpAndCannotbeEdited = true;

        fillUpDoctorDataandMemberID(loaFillUpMOdel);
        tv_diagnosis.setText(dataBaseHandler.getDiagDesc(loaFillUpMOdel.getDiagnosisList()));
        arrayProcedure.clear();
        arrayProcedure.addAll(dataBaseHandler.retrieveProcedureFromList(loaFillUpMOdel.getProceduresList()));
        Log.d("Proc_data", arrayProcedure.size() + "");

        if (arrayProcedure.size() != 0) {
            rv_noProcAvailable.setVisibility(View.GONE);
            rv_procedures.setVisibility(View.VISIBLE);
        }


    }


    @Override
    public void isProcedureFillUpFields(LoaList loaFillUpMOdel) {
        /** TODO
         * //todo here
         WAIT FOR BACKEND
         FOR LISTING
         **/

        isFilledUpAndCannotbeEdited = true;
        fillUpDoctorDataandMemberID(loaFillUpMOdel);
        tv_diagnosis.setText(dataBaseHandler.getDiagDesc(loaFillUpMOdel.getDiagnosisList()));
        arrayProcedure.clear();
        arrayProcedure.addAll(dataBaseHandler.retrieveProcedureFromList(loaFillUpMOdel.getProceduresList()));
        Log.d("Proc_data", arrayProcedure.size() + "");

        if (arrayProcedure.size() != 0) {
            rv_noProcAvailable.setVisibility(View.GONE);
            rv_procedures.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onBasicTestsSuccess(BasicTestOrOtherTestReturn getOtherTestReturnModel, String ORIGIN) {

    }


//    @Override
//    public void onBasicTestsSuccess(BasicTestReturn body) {
//
//        implement.loadProcedureBasicTest(pb_other_diag, tv_other_proc, true);
//        implement.loadProcedureBasicTest(pb_primary_diag, tv_primary, true);
//
//        implement.setLoadingPrimaryGone(tv_other_proc, pb_other_diag, true);
//        implement.setLoadingPrimaryGone(tv_primary, pb_primary_diag, true);
//
//        isBasicTestAvailable = true;
//        arrayBasicTest.addAll(body.getBasicTests());
//        proceduresPrimaryList = implement.getProcList(arrayBasicTest);
//
//        if (body.getBasicTests().size() != 0) {
//            for (BasicTests basicTests : body.getBasicTests()) {
//                Log.d("DATA_BASICS", basicTests.getId());
//            }
//        } else {
//            Log.d("DATA_ZERO", body.getBasicTests().size() + "");
//        }
//    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onBasicTestsSuccess1(BasicTestReturn body) {
//        testsArrayList = body.getBasicTests();//TODO DB Helper Get Basic Test
//        basicTestList.clear();
//        basicTestList.addAll(dataBaseHandler.retrieveBasicTestList());
//        basictestAdapter = new BasictestAdapter(context, testsArrayList, basicTestList, dataBaseHandler);
//        rv_basicTest.setAdapter(basictestAdapter);
//        basictestAdapter.notifyDataSetChanged();

    }

    @Override
    public void onBasicTestError(String message) {
        implement.setLoadingPrimaryGone(tv_other_proc, pb_other_diag, false);
        implement.setLoadingPrimaryGone(tv_primary, pb_primary_diag, false);
        Log.d("ERROR", "BASICTES_LIST");
    }

    /*
     This methid is used to get all Basic test selected
     @params List of BasicTests
     @return List of BasicTest selected
     */
    public ArrayList<BasicTests> getSelectedBasicTests() {
        session.releaseContent();
        for (BasicTests basicTest : testsArrayList) {
            if (basicTest.isSelected()) {
                session.addString(basicTest);
            }
        }

        //Display the selected tests Return value
        System.out.println("Return selected Basic Tests" + session.getAllProcedureCode().toString());
        return session.getAllProcedureCode();
    }

    public ArrayList<DoctorModelInsertion> getAllSelectedDoctor() {
        inpatientSession.releaseContentDoctorModel();
        for (DoctorModelInsertion doctorModelInsertion : doctorModelInsertions) {
            if (doctorModelInsertion.isSelected()) {
                inpatientSession.addString(doctorModelInsertion);
            }
        }

        //Display the selected tests Return value
        System.out.println("Return selected getAllSelectedDoctor" + inpatientSession.getAllSelectedDoctor().toString());
        return inpatientSession.getAllSelectedDoctor();
    }

    public ArrayList<DiagnosisList> getAllSelectedDiagnosis() {
        inpatientSession.releaseContentDiagnosisModel();
        for (DiagnosisList doctorModelInsertion : diagnosisModelInsertions) {
            if (doctorModelInsertion.isSelected()) {
                inpatientSession.addString(doctorModelInsertion);
            }
        }

        //Display the selected tests Return value
        System.out.println("Return selected getAllSelectedDiagnosis" + inpatientSession.getAllSelectedDiagnosis().toString());
        return inpatientSession.getAllSelectedDiagnosis();
    }


    public ArrayList<TestsAndProcedures> getAllSelectedProcedure() {
        inpatientSession.releaseContentProcedureModel();
        for (TestsAndProcedures doctorModelInsertion : procedureModelInsertions) {
            if (doctorModelInsertion.isSelected()) {
                inpatientSession.addString(doctorModelInsertion);
            }
        }

        //Display the selected tests Return value
        System.out.println("Return selected getAllSelectedProcedure" + inpatientSession.getAllSelectedProcedure().toString());
        return inpatientSession.getAllSelectedProcedure();
    }


    public ArrayList<TestsAndProcedures> getAllTestAndProcedureCode() {
        session.releaseContentTestsAndProcedures();
        for (TestsAndProcedures basicTest : basicTestList) {
            if (basicTest.isSelected()) {
                session.addString(basicTest);
            }
        }

        //Display the selected tests Return value
        System.out.println("Return selected Basic Tests" + session.getAllTestAndProcedureCode().toString());
        return session.getAllTestAndProcedureCode();
    }

    public ArrayList<TestsAndProcedures> getSelectedProcedures() {
        clinicProcedureSession.releaseContent();
        for (TestsAndProcedures testsAndProcedure : arrayProcedure) {
            clinicProcedureSession.addTests(testsAndProcedure);
        }


        //Display the selected tests Return value
        System.out.println("Return selected procedures" + clinicProcedureSession.getAllProcedureCode().toString());
        return clinicProcedureSession.getAllProcedureCode();
    }

    public ArrayList<TestsAndProcedures> getSelectedPrimary() {
        otherTestSession.releaseContent();
        for (TestsAndProcedures testsAndProcedure : arrayPrimary) {
            otherTestSession.addStringPrimary(testsAndProcedure);
        }

        //Display the selected tests Return value
        System.out.println("Return selected primary" + otherTestSession.getAllPrimary().toString());
        return otherTestSession.getAllPrimary();
    }


    @Override
    public void setConsultationUI() {
        cv_other.setVisibility(View.GONE);
        cv_other_second.setVisibility(View.GONE);
        cv_prime.setVisibility(View.GONE);
        et_problem.setVisibility(View.VISIBLE);
        tv_problem_tag.setVisibility(View.VISIBLE);
        ll_reference.setVisibility(View.GONE);
        tv_title.setText(getString(R.string.consultation));
        isMaternity = false;
        isConsultation = true;
        isOtherTest = false;
        isBasicTest = false;
        isInPatient = false;
        isProcedure = false;
        implement.setOtherProcedurePanelGone(fr_otherProc, true);
        implement.setInputFieldsGone(true, ll_setInputFields);
        EventBus.getDefault().post(new NavigatorActivity.MessageEvent("Consultation"));
    }

    @Override
    public void setMaternityUI() {
        ll_reference.setVisibility(View.GONE);
        cv_other.setVisibility(View.GONE);
        cv_other_second.setVisibility(View.GONE);
        cv_prime.setVisibility(View.GONE);
        tv_title.setText(getString(R.string.maternity));
        isMaternity = true;
        isConsultation = false;
        isOtherTest = false;
        isBasicTest = false;
        isInPatient = false;
        isProcedure = false;
        implement.setOtherProcedurePanelGone(fr_otherProc, true);
        implement.setInputFieldsGone(true, ll_setInputFields);
        et_problem.setVisibility(View.VISIBLE);
        tv_problem_tag.setVisibility(View.VISIBLE);
        EventBus.getDefault().post(new NavigatorActivity.MessageEvent("Maternity"));
    }

    @Override
    public void setBasicTestUI() {
//        AvailServicesAPICall.getBasicforOutPatient(callback);
        basicTestList.clear();
        basicTestList.addAll(dataBaseHandler.retrieveBasicTestList());
        basictestAdapter = new BasictestAdapter(context, basicTestList, dataBaseHandler);
        rv_basicTest.setAdapter(basictestAdapter);
        basictestAdapter.notifyDataSetChanged();
        implement.loadProcedureBasicTest(pb_other_diag, tv_other_proc, true);
        implement.loadProcedureBasicTest(pb_primary_diag, tv_primary, true);
        pb_primary_diag.setVisibility(View.GONE);
        rl_basic_test.setVisibility(View.VISIBLE);
        tv_diag_type.setText(getString(R.string.basic_test));
        et_inputdoctor.setVisibility(View.GONE);
        et_problem.setVisibility(View.GONE);
        tv_problem_tag.setVisibility(View.GONE);
        tv_doctor_tag.setVisibility(View.GONE);
        tv_doctor.setVisibility(View.GONE);
        tv_primary.setVisibility(View.GONE);
        tv_primary_diag_label.setVisibility(View.GONE);
        tv_primary_diag.setVisibility(View.GONE);
        ll_diag_test.setVisibility(View.GONE);
        cv_other.setVisibility(View.GONE);
        cv_other_second.setVisibility(View.GONE);
        cv_prime.setVisibility(View.GONE);
        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText("Basic Tests");
        isMaternity = false;
        isConsultation = false;
        isOtherTest = false;
        isBasicTest = true;
        isInPatient = false;
        isProcedure = false;
        implement.setOtherProcedurePanelGone(fr_otherProc, true);
        implement.setInputFieldsGone(true, ll_setInputFields);
        FROM_FRAG = "BASIC_TEST";
        EventBus.getDefault().post(new NavigatorActivity.MessageEvent("Basic_Test"));
    }

    @Override
    public void setOtherTestUI() {
        cv_request.setVisibility(View.GONE);
        tv_hospital.setVisibility(View.GONE);
        tv_hospital_tag.setVisibility(View.GONE);
        tv_doctor_tag.setVisibility(View.VISIBLE);
        tv_doctor.setVisibility(View.VISIBLE);

        cv_othertest_request.setVisibility(View.VISIBLE);
        tv_othertest_request_hosp.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALNAME, context));
        SharedPref.setStringValue(SharedPref.USER,SharedPref.UNFILTERED_HOSPITAL_CODE,
                SharedPref.getStringValue(SharedPref.USER,SharedPref.HOSPITALCODE,context),context);

        // cv_other.setVisibility(View.GONE);
        // cv_prime.setVisibility(View.GONE);
        tv_title.setText(R.string.other_test);
        et_problem.setVisibility(View.GONE);
        tv_problem_tag.setVisibility(View.GONE);
        et_inputdoctor.setVisibility(View.GONE);
        add_diagnosis.setVisibility(View.VISIBLE);
        isMaternity = false;
        isConsultation = false;
        isOtherTest = true;
        isBasicTest = false;
        isInPatient = false;
        isProcedure = false;
        implement.setOtherProcedurePanelGone(fr_otherProc, true);
        implement.setInputFieldsGone(true, ll_setInputFields);
        EventBus.getDefault().post(new NavigatorActivity.MessageEvent("Other_Test"));
    }

    @Override
    public void setUnfilteredHosp(Hospital unfilteredHosp) {
        tv_othertest_request_hosp.setText(unfilteredHosp.getHospitalName());
        tv_othertest_request_doc.setText("");
    }


    @Override
    public void deleteData() {

//
//        isFilledUpAndCannotbeEdited = false;
//        isPreventingDeleteFromOnStop = false;
//        //OTHER DIAGNOSIS AND PROCEDURES UTILS
//        selectedDiagnosisOther = "";
//        isOtherAvailable = false;
//        isOtherSelected = false;
//
//        //PRIMARY DIAGNOSIS AND PROCEDURES UTILS
//        selectedDiagnosisPrimary = "";
//        isPrimaryAvailable = false;
//        isPrimarySelected = false;
//
//        //PRIMARY 2 DIAGNOSIS ANF PROCEDURES UTILS
//        selectedDignosisPrimary_2 = "";
//        isPrimary_2_Available = false;
//        isPrimary_2_Selected = false;
//
//        isBasicTestAvailable = false;
//
//        proceduresPrimaryList = null;
//        proceduresOtherList = null;
//        proceduresPrimary2List = null;
//
//
//        tv_diagnosis.setText("");
//        tv_primary_diag.setText("");
//        tv_other_diag.setText("");
//        tv_other_diag_second.setText("");
//        tv_doctor.setText("");
////        implement.clearData(dataBaseHandler, arrayProcedure, arrayListDiagnosis, arrayPrimary,
////                arrayOther);
//        implement.setProceduresAvailability(arrayPrimary, rl_primary, rv_primary
//                , tv_total_price, tv_primary_add);
//        implement.setProceduresAvailability(arrayOther, rl_other_diag, rv_other_diag
//                , tv_total_price, tv_other_proc_Add);
//        implement.setProceduresAvailability(arrayProcedure, rv_noProcAvailable, rv_procedures
//                , tv_total_price, tv_primary_diag_2_add);
//
//        tv_total_price.setText(implement.setPrice(arrayProcedure));
//
////        if (arrayListDiagnosis.size() == 0) {
////            tv_diagnosis.setVisibility(View.VISIBLE);
////            rv_diagnosis.setVisibility(View.GONE);
////        } else {
////            tv_diagnosis.setVisibility(View.GONE);
////            rv_diagnosis.setVisibility(View.VISIBLE);
////        }
//
//        ll_reference.setVisibility(View.GONE);

    }

    @Override
    public void setDoctorSelected(String doctorCreds, String member_id, String hospitalCode, String doctorCode, String doctorName, String hospitalName) {
        getMEMBER_ID = member_id;
        getHospitalCode = hospitalCode;
        getDoctorCode = doctorCode;
        tv_doctor.setText(doctorName + " - " + doctorCreds + "\n" + hospitalName);
        tv_inpatient_doctor.setText(doctorName + " - " + doctorCreds + "\n" + hospitalName);
        tv_othertest_request_doc.setText(doctorName + "\n" + doctorCreds);
        getDoctor = doctorName;
        getDoctorSpec = doctorCreds;
        cb_informed.setChecked(false);
        implement.setCheckBox(cb_informed, btn_proceed);
    }


    @Override
    public void setProcedureSelected(ArrayList<TestsAndProcedures> arrayProc) {
        cb_informed.setChecked(false);
        implement.setCheckBox(cb_informed, btn_proceed);
        //For adding of new TestAndProc
        for (TestsAndProcedures testProcSelected : arrayProc) {
            boolean hit = false;
            for (TestsAndProcedures testProcToPass : arrayProcedure) {
                if (testProcSelected.getProcedureCode().equals(testProcToPass.getProcedureCode())) {
                    hit = true;
                    break;
                }
            }
            if (!hit)
                arrayProcedure.add(testProcSelected);
        }
        //For removing of missing TestProc
        for (TestsAndProcedures testsAndProcedures : arrayProcedure) {
            boolean hit = false;
            for (TestsAndProcedures andProcedures : arrayProc) {
                if (testsAndProcedures.getProcedureCode().equals(andProcedures.getProcedureCode())) {
                    hit = true;
                    break;
                }
            }
            if (!hit)
                arrayProcedure.remove(testsAndProcedures);
        }

//        arrayProcedure.clear();
//        arrayProcedure.addAll(arrayProc);
        proceduresToBeSendAdapter.notifyDataSetChanged();
        implement.setProceduresAvailability(arrayProcedure, rv_noProcAvailable, rv_procedures
                , tv_total_price, tv_primary_diag_2_add);


    }


    @Override
    public void setInPatientDoctorSelected(ArrayList<DoctorModelInsertion> inPatientDoctorSelected) {
        cb_informed.setChecked(false);
        implement.setCheckBox(cb_informed, btn_proceed);
        //For adding of new TestAndProc
        for (DoctorModelInsertion testProcSelected : inPatientDoctorSelected) {
            boolean hit = false;
            for (DoctorModelInsertion testProcToPass : doctorModelInsertions) {
                if (testProcSelected.getDoctorCode().equals(testProcToPass.getDoctorCode())) {
                    hit = true;
                    break;
                }
            }
            if (!hit)
                doctorModelInsertions.add(testProcSelected);
        }
        //For removing of missing TestProc
        for (DoctorModelInsertion testsAndProcedures : doctorModelInsertions) {
            boolean hit = false;
            for (DoctorModelInsertion andProcedures : inPatientDoctorSelected) {
                if (testsAndProcedures.getDoctorCode().equals(andProcedures.getDoctorCode())) {
                    hit = true;
                    break;
                }
            }
            if (!hit)
                doctorModelInsertions.remove(testsAndProcedures);
        }

        inpatientDoctorSelectAdapterAvailservice.notifyDataSetChanged();


        if (doctorModelInsertions.size() == 0) {
            btn_inpatient_doctor.setVisibility(View.GONE);
            tv_inpatient_doctor.setVisibility(View.VISIBLE);
            rv_inpatient_doctor.setVisibility(View.GONE);
        } else {
            tv_inpatient_doctor.setVisibility(View.GONE);
            rv_inpatient_doctor.setVisibility(View.VISIBLE);
            btn_inpatient_doctor.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setInPatientDiagnosisSelected(ArrayList<DiagnosisList> diagnosisLists) {
        cb_informed.setChecked(false);
        implement.setCheckBox(cb_informed, btn_proceed);
        //For adding of new TestAndProc
        for (DiagnosisList testProcSelected : diagnosisLists) {
            boolean hit = false;
            for (DiagnosisList testProcToPass : diagnosisModelInsertions) {
                if (testProcSelected.getDiagCode().equals(testProcToPass.getDiagCode())) {
                    hit = true;
                    break;
                }
            }
            if (!hit)
                diagnosisModelInsertions.add(testProcSelected);
        }
        //For removing of missing TestProc
        for (DiagnosisList testsAndProcedures : diagnosisModelInsertions) {
            boolean hit = false;
            for (DiagnosisList andProcedures : diagnosisLists) {
                if (testsAndProcedures.getDiagCode().equals(andProcedures.getDiagCode())) {
                    hit = true;
                    break;
                }
            }
            if (!hit)
                diagnosisModelInsertions.remove(testsAndProcedures);
        }

        inpatientDiagnosisSelectAdapterAvailservice.notifyDataSetChanged();


        if (diagnosisModelInsertions.size() == 0) {
            btn_inpatient_diagnosis.setVisibility(View.GONE);
            tv_inpatient_diagnosis.setVisibility(View.VISIBLE);
            rv_inpatient_diagnosis.setVisibility(View.GONE);
        } else {
            tv_inpatient_diagnosis.setVisibility(View.GONE);
            rv_inpatient_diagnosis.setVisibility(View.VISIBLE);
            btn_inpatient_diagnosis.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setInPatientProcedureSelected(ArrayList<TestsAndProcedures> procedureLists) {
        cb_informed.setChecked(false);
        implement.setCheckBox(cb_informed, btn_proceed);
        //For adding of new TestAndProc
        for (TestsAndProcedures testProcSelected : procedureLists) {
            boolean hit = false;
            for (TestsAndProcedures testProcToPass : procedureModelInsertions) {
                if (testProcSelected.getProcedureCode().equals(testProcToPass.getProcedureCode())) {
                    hit = true;
                    break;
                }
            }
            if (!hit)
                procedureModelInsertions.add(testProcSelected);
        }
        //For removing of missing TestProc
        for (TestsAndProcedures testsAndProcedures : procedureModelInsertions) {
            boolean hit = false;
            for (TestsAndProcedures andProcedures : procedureLists) {
                if (testsAndProcedures.getProcedureCode().equals(andProcedures.getProcedureCode())) {
                    hit = true;
                    break;
                }
            }
            if (!hit)
                procedureModelInsertions.remove(testsAndProcedures);
        }

        inpatientProcedureSelectAdapterAvailservice.notifyDataSetChanged();


        if (procedureModelInsertions.size() == 0) {
            btn_inpatient_procedure.setVisibility(View.GONE);
            tv_inpatient_procedure.setVisibility(View.VISIBLE);
            ll2_inpatient_procedure.setVisibility(View.GONE);
        } else {
            tv_inpatient_procedure.setVisibility(View.GONE);
            ll2_inpatient_procedure.setVisibility(View.VISIBLE);
            btn_inpatient_procedure.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void updateDiagnosis(DiagnosisList diagnosisList) {

        if (isFilledUpAndCannotbeEdited) {
            callback.deleteData();
        }
        implement.testSelectedDiagfromExisting(Constant.FROM_PRIMARY_PROC_2, dataBaseHandler, arrayProcedure, diagnosisList.getDiagCode(), selectedDignosisPrimary_2);
        setProcPrimary2Selected(arrayProcedure);
        setDiagnosisSelected(diagnosisList);
        primaryDiag2 = diagnosisList;
        isPrimary_2_Available = false;
        isPrimary_2_Selected = true;
        selectedDignosisPrimary_2 = diagnosisList.getDiagCode();
        if (!isBasicTest) {
            implement.loadProcedureBasedonDiagnosis(filterProcCallback, Constant.FROM_PRIMARY_PROC_2, diagnosisList.getDiagCode(), pb_primary_diag_2, tv_primary_diag_2);
        }
    }

    @Override
    public void updatePrimaryProcedure(ArrayList<TestsAndProcedures> arrayProc) {
        cb_informed.setChecked(false);
        implement.setCheckBox(cb_informed, btn_proceed);
        for (TestsAndProcedures testProcSelected : arrayProc) {
            boolean hit = false;
            for (TestsAndProcedures testProcToPass : arrayPrimary) {
                if (testProcSelected.getProcedureCode().equals(testProcToPass.getProcedureCode())) {
                    hit = true;
                    break;
                }
            }
            if (!hit)
                arrayPrimary.add(testProcSelected);
        }
        //For removing of missing TestProc
        for (TestsAndProcedures testsAndProcedures : arrayPrimary) {
            boolean hit = false;
            for (TestsAndProcedures andProcedures : arrayProc) {
                if (testsAndProcedures.getProcedureCode().equals(andProcedures.getProcedureCode())) {
                    hit = true;
                    break;
                }
            }
            if (!hit)
                arrayPrimary.remove(testsAndProcedures);
        }
//        arrayPrimary.clear();
//        arrayPrimary.addAll(arrayProc);
        primaryProcedures.notifyDataSetChanged();
        implement.setProceduresAvailability(arrayPrimary, rl_primary, rv_primary
                , tv_total_price, tv_primary_add);

    }

    @Override
    public void setPrimaryDiagnosis(DiagnosisList diagnosisList) {
        implement.testSelectedDiagfromExisting(Constant.FROM_PRIMARY_PROC, dataBaseHandler, arrayPrimary, diagnosisList.getDiagCode(), selectedDiagnosisPrimary);
        setProcPrimarySelected(arrayPrimary);
//        SharedPref.setStringValue();
        setPrimaryDiagSelected(diagnosisList);
        primaryDiag = diagnosisList;
        isPrimaryAvailable = false;
        isPrimarySelected = true;
        selectedDiagnosisPrimary = diagnosisList.getDiagCode();
        if (!isBasicTest) {
            implement.loadProcedureBasedonDiagnosis(filterProcCallback, Constant.FROM_PRIMARY_PROC, diagnosisList.getDiagCode(), pb_primary_diag, tv_primary);
        }
    }

//    @Override
//    public void setUnfillteredHospital(Hospital hospital) {
//        setUnfillteredHospitals(hospital);
//    }


    @Override
    public void setOtherProcedures(ArrayList<TestsAndProcedures> arrayProc) {
        cb_informed.setChecked(false);
        implement.setCheckBox(cb_informed, btn_proceed);

        for (TestsAndProcedures testProcSelected : arrayProc) {
            boolean hit = false;
            for (TestsAndProcedures testProcToPass : arrayOther) {
                if (testProcSelected.getProcedureCode().equals(testProcToPass.getProcedureCode())) {
                    hit = true;
                    break;
                }
            }
            if (!hit)
                arrayOther.add(testProcSelected);
        }
        //For removing of missing TestProc
        for (TestsAndProcedures testsAndProcedures : arrayOther) {
            boolean hit = false;
            for (TestsAndProcedures andProcedures : arrayProc) {
                if (testsAndProcedures.getProcedureCode().equals(andProcedures.getProcedureCode())) {
                    hit = true;
                    break;
                }
            }
            if (!hit)
                arrayOther.remove(testsAndProcedures);
        }
//        arrayOther.clear();
//        arrayOther.addAll(arrayProc);
        otherProcedure.notifyDataSetChanged();
        implement.setProceduresAvailability(arrayOther, rl_other_diag, rv_other_diag
                , tv_total_price, tv_other_proc_Add);
    }

    @Override
    public void setOtherProceduresSecond(ArrayList<TestsAndProcedures> arrayProc) {
        cb_informed.setChecked(false);
        implement.setCheckBox(cb_informed, btn_proceed);
        arrayOtherSecond.clear();
        arrayOtherSecond.addAll(arrayProc);
        otherProcedureSecond.notifyDataSetChanged();
        implement.setProceduresAvailability(arrayOtherSecond, rl_other_diag_second, rv_other_diag_second
                , tv_total_price, tv_other_proc_Add_second);
    }

    @Override
    public void setOtherDiagnosis(DiagnosisList diagnosisList) {
        implement.testSelectedDiagfromExisting(Constant.FROM_OTHER_PROC, dataBaseHandler, arrayOther, diagnosisList.getDiagCode(), selectedDiagnosisOther);
        setProcOtherSelected(arrayOther);
        setOtherDiagSelected(diagnosisList);
        primaryOtherDiag = diagnosisList;
        isOtherAvailable = false;
        isOtherSelected = true;
        selectedDiagnosisOther = diagnosisList.getDiagCode();
        if (!isBasicTest) {
            implement.loadProcedureBasedonDiagnosis(filterProcCallback, Constant.FROM_OTHER_PROC, diagnosisList.getDiagCode(), pb_other_diag, tv_other_proc);
        }
    }

    @Override
    public void setOtherDiagnosisSecond(DiagnosisList diagnosisListSecond) {
        implement.testSelectedDiagfromExisting(Constant.FROM_OTHER_PROC_2, dataBaseHandler, arrayOther, diagnosisListSecond.getDiagCode(), selectedDiagnosisOther);
        setProcOtherSelectedSecond(arrayOtherSecond);
        setOtherDiagSelectedSecond(diagnosisListSecond);
        primaryOtherDiagSecond = diagnosisListSecond;
        isOtherAvailable = false;
        isOtherSelectedSecond = true;
        selectedDiagnosisOther = diagnosisListSecond.getDiagCode();
        if (!isBasicTest) {
            implement.loadProcedureBasedonDiagnosis(filterProcCallback, Constant.FROM_OTHER_PROC_2, diagnosisListSecond.getDiagCode(), pb_other_diag_second, tv_other_proc_second);
        }
    }

    @Override
    public void setProceduresUI() {
        tv_diag_type.setText(getString(R.string.procedures));
        tv_hospital.setVisibility(View.GONE);
        tv_hospital_tag.setVisibility(View.GONE);
        cv_other.setVisibility(View.GONE);
        cv_other_second.setVisibility(View.GONE);
        cv_prime.setVisibility(View.GONE);
        diag_procedure_tag.setVisibility(View.VISIBLE);
        tv_title.setText("Procedures Done Inside Doctor's Clinic");
        tv_title.setTextSize(20);
        tv_primary_diag_2.setText("Add Clinic Procedure");
        tv_primary_diag_2_add.setText("Add Clinic Procedure");
        tv_primary.setText("Add Clinic Procedures");
        tv_other_proc.setText("Add Clinic Procedures");
        et_problem.setVisibility(View.GONE);
        tv_problem_tag.setVisibility(View.GONE);
        ll_primary_ip_label.setVisibility(View.VISIBLE);
        textView10.setVisibility(View.VISIBLE);
        //proc remarks
        tv_proc_remarks_tag.setVisibility(View.VISIBLE);
        tv_proc_remarks.setVisibility(View.VISIBLE);
        isMaternity = false;
        isConsultation = false;
        isOtherTest = false;
        isBasicTest = false;
        isInPatient = false;
        isProcedure = true;
        implement.setOtherProcedurePanelGone(fr_otherProc, false);
        implement.setInputFieldsGone(true, ll_setInputFields);
        EventBus.getDefault().post(new NavigatorActivity.MessageEvent("PROCEDURE"));
    }

    @Override
    public void setSelectedPreMadeConsultationData(LoaList loa) {
        loaFillUpMOdel = loa;
        implement.setApprovalNoField(tv_approvalNo, loaFillUpMOdel.getApprovalNo(), ll_reference);
        implement.setRequestOrigin(isOtherTest, isBasicTest, isProcedure, callback, loaFillUpMOdel);
    }

    @Override
    public void onProceduresSuccess(BasicTestOrOtherTestReturn body, String ORIGIN) {
        loader.stopLoad();
//        arrayPrimary.clear();
//        arrayOther.clear();
//        arrayProcedure.clear();
        System.out.println("arrayPrimary: " + arrayPrimary);
        System.out.println("arrayOther: " + arrayOther);
        System.out.println("arrayProcedure: " + arrayProcedure);
//        testDuplicateRequestProcedures(body, ORIGIN);
//        testDuplicateRequestBasicOrOtherTest(body, ORIGIN);
//         proceduresToDisplay.addAll(procedure);
        dataToDisplay(body, ORIGIN, 2);
    }

    public void onProceduresDuplicate(Confirm confirm, String ORIGIN) {
//        session.releaseContent();
        if (confirm.getResponseCode() != null) {
            et_problem.setText("");
            dialogCustom.showMeValidateReq(confirm.getResponseDesc(), getContext()
                    , callback, ORIGIN);
            loader.stopLoad();
        } else {
            dialogCustom.showMe(getContext(), dialogCustom.unknown, ErrorMessage.setErrorMessage("500"), 1);
        }
    }


    @Override
    public void getTime(String origin) {
        Log.d("GET_TIME", origin);
        rawTimeInPatient = origin;
    }

    @Override
    public void setRoomPlans(Rooms rooms) {
        et_input_room_number.setText(rooms.getPlanDesc() + "\n" +
                implement.getNotNull(rooms.getClassCategory()));

        inPatientRoomType = rooms.getPlanCode();

    }

    @Override
    public void setRoomCategory(String origin) {
        inPatientRoomCategory = origin;
        et_input_category.setText(origin);
    }


    // DATA SENDING
    @Override
    public void sendData(int i) {
        implement.showDialog(i);
    }

    @Override
    public void sendInPatient() {
        loader.startLad();


        String room_no = et_input_number.getText().toString().trim();
        String category = inPatientRoomCategory;
        String room_plan = inPatientRoomType;
        String room_price = et_input_room_price.getText().toString().trim();
        String diagnosis = selectedDiagnosisPrimary;
        String doctor = getDoctorCode;
        String doctorName = getDoctor;
        //for edit er
//        String diagDesc = primaryDiag.getDiagDesc();
        //     String diagDesc = "ABDOMINAL BLUNT INJURY";
        Log.d("rawTimeInPatient", rawTimeInPatient);
        Log.d("tv_admitted", DateConverter.convertDatetoyyyy_MM_dd(tv_admitted.getText().toString().trim()));
        Log.d("tv_admitted", DateConverter.convertDatetoyyyy_MM_dd(tv_admitted.getText().toString().trim()
                + " " + rawTimeInPatient));

        String dateAdmitted = DateConverter.setDateInPatient(DateConverter.convertDatetoyyyy_MM_dd(tv_admitted.getText().toString().trim())
                + " " + rawTimeInPatient);

        AvailServicesAPICall.sendInPatient(callback,
                SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, getContext()),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, getContext()),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, getContext()),
                getAllSelectedDoctor(),
                getAllSelectedDiagnosis(),
                getAllSelectedProcedure(),
                room_price,
                room_no,
                dateAdmitted,
                contact12.getText().toString(),
                contact14.getText().toString(),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.SEARCHTYPE, context),
                category,
                room_plan,
                SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, getContext())
        );

        SharedPref.setStringValue(SharedPref.USER, SharedPref.CONTACT_NO, contact12.getText().toString(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.EMAIL, contact14.getText().toString(), context);

    }

    @Override
    public void sendER() {

        loader.startLad();
        //TODO ER Enhancement


        getErReason = et_er_reason.getText().toString().trim();
        String dateAdmitted = DateConverter.setDateInPatient(DateConverter.convertDatetoyyyy_MM_dd(tv_admitted.getText().toString().trim())
                + " " + rawTimeInPatient);


        AvailServicesAPICall.sendER(
                callback,
                dateAdmitted,
                getErReason,
                SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, getContext()),
                getMEMBER_ID,
                SharedPref.getStringValue(SharedPref.USER, SharedPref.FULL_NAME, getContext()),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, getContext()),
                contact12.getText().toString(),
                contact14.getText().toString()
        );
    }

    @Override
    public void dischargeER() {

        AvailServicesAPICall.sendDischargeEr(
                callback,
                SharedPref.getStringValue(SharedPref.USER, SharedPref.HAS_ER, getContext()),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, getContext())
        );

    }


    @Override
    public void dischargeERToInpatient() {

        AvailServicesAPICall.sendDischargeEr(
                callback,
                SharedPref.getStringValue(SharedPref.USER, SharedPref.HAS_ER, getContext()),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, getContext())
        );

    }

    @Override
    public void dischargeERToOutpatient() {

        AvailServicesAPICall.sendDischargeEr(
                callback,
                SharedPref.getStringValue(SharedPref.USER, SharedPref.HAS_ER, getContext()),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, getContext())
        );
    }


    @Override
    public void sendConsultation() {
        REQUEST = Constant.CONSULTATION;
        getProblemConsultation = et_problem.getText().toString().trim();
        loader.startLad();

        AvailServicesAPICall.checkForDuplicateConsultAndMaternity(
                Constant.CONSULTATION,
                callback,
                getProblemConsultation,
                getDoctorCode,
                SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, getContext()),
                getMEMBER_ID,
                SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, getContext()),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, getContext()),
                contact12.getText().toString(),
                contact14.getText().toString());

        SharedPref.setStringValue(SharedPref.USER, SharedPref.CONTACT_NO, contact12.getText().toString(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.EMAIL, contact14.getText().toString(), context);


    }

    //original consultation send
//    @Override
//    public void sendConsultation() {
//        REQUEST = Constant.CONSULTATION;
//        getProblemConsultation = et_problem.getText().toString().trim();
//        loader.startLad();
//        AvailServicesAPICall.sendConsultationRequest(
//                Constant.CONSULTATION,
//                callback,
//                getProblemConsultation,
//                getDoctorCode,
//                SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, getContext()),
//                getMEMBER_ID,
//                SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, getContext()),
//                SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, getContext()));
//
//    }

    @Override
    public void sendMaternity() {
        REQUEST = Constant.MATERNITY;
        getProblemConsultation = et_problem.getText().toString().trim();
        loader.startLad();
        AvailServicesAPICall.checkForDuplicateConsultAndMaternity(
                Constant.CONSULTATION,
                callback,
                getProblemConsultation,
                getDoctorCode,
                SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, getContext()),
                getMEMBER_ID,
                SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, getContext()),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, getContext()),
                contact12.getText().toString(),
                contact14.getText().toString());

        SharedPref.setStringValue(SharedPref.USER, SharedPref.CONTACT_NO, contact12.getText().toString(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.EMAIL, contact14.getText().toString(), context);

    }

    // Changed Aug 11, 2017 -- Lawrence Mataga for Sprint 6 SIT

//    @Override
//    public void sendBasic() {
//        loader.startLad();
//        BasicTestOrOtherTest sendBasicTestModel = implement.collectOtherTest(
//                "2",
//                callback,
//                SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, getContext()),
//                SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, getContext()),
//                SharedPref.getStringValue(SharedPref.USER, SharedPref.MEMBERID, getContext()),
//                SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, getContext()),
//                SharedPref.getStringValue(SharedPref.USER, SharedPref.SEARCHTYPE, context),
//                primaryDiag,
//                primaryOtherDiag,
//                arrayPrimary,
//                arrayOther,
//                getDoctorCode
//        );
//
//        Gson gson = new Gson();
//        Log.d("BASIC_TEST", gson.toJson(sendBasicTestModel));
//        AvailServicesAPICall.sendBasicTest(Constant.BASIC_TEST, callback, sendBasicTestModel);
//
//
//    }


    @Override
    public void sendBasic() {
        loader.startLad();

//        RequestBasicTest sendBasicTest = implement.collectRequestBasicTest(
//                "2", callback,
//                SharedPref.getStringValue(SharedPref.USER, SharedPref.MEMBERID, getContext()),
//                SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, getContext()),
//                SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, getContext()),
//                SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME,getContext() ),
//                session.getAllProcedureCode()
//
//        );
        if (basicTestList.isEmpty()) {
            dialogCustom.showMe(getContext(), dialogCustom.unknown, "Please Select Basic Test", 1);
            loader.stopLoad();
        } else {
            BasicTestOrOtherTest sendBasicTestModel = implement.collectBasicTest(
                    Constant.BASIC_TEST,
                    SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, getContext()),
                    SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, getContext()),
                    SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, getContext()),
                    SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, getContext()),
                    SharedPref.getStringValue(SharedPref.USER, SharedPref.SEARCHTYPE, context),
                    session.getAllTestAndProcedureCode(),
                    primaryDiag2,
                    getDoctorCode,
                    contact12.getText().toString(),
                    contact14.getText().toString());

            SharedPref.setStringValue(SharedPref.USER, SharedPref.CONTACT_NO, contact12.getText().toString(), context);
            SharedPref.setStringValue(SharedPref.USER, SharedPref.EMAIL, contact14.getText().toString(), context);

            SharedPref.TestOrOtherTest = sendBasicTestModel;

            Gson gson = new Gson();
            Log.d("BASIC_TEST", gson.toJson(sendBasicTestModel));

            AvailServicesAPICall.checkForDuplicateRequestTestProc(Constant.BASIC_TEST, callback, sendBasicTestModel);


            //original call
//            RequestBasicTest sendBasicTest = implement.collectRequestBasicTest(
//                    "2", callback,
//                    SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, getContext()),
//                    SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, getContext()),
//                    SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, getContext()),
//                    SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, getContext()),
//                    session.getAllProcedureCode(),
//                    SharedPref.getStringValue(SharedPref.USER, SharedPref.CONTACT_NO, getContext()),
//                    SharedPref.getStringValue(SharedPref.USER, SharedPref.EMAIL, getContext())
//
//            );
//
//            SharedPref.requestBasicTest = sendBasicTest;
//
//            Gson gson = new Gson();
//            Log.d("BASIC_TEST", gson.toJson(sendBasicTest));
//            AvailServicesAPICall.checkForDuplicateRequestBasictest(Constant.BASIC_TEST, callback, sendBasicTest);
            //original call


        }
    }

    @Override
    public void sendOtherTest() {

        loader.startLad();

        BasicTestOrOtherTest sendBasicTestModel = implement.collectOtherTest(
                Constant.OTHER_TEST, callback,
                SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, getContext()),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, getContext()),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, getContext()),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, getContext()),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.SEARCHTYPE, context),
                primaryDiag,
                primaryOtherDiag,
                primaryOtherDiagSecond,
                arrayPrimary,
                arrayOther,
                arrayOtherSecond,
                getDoctorCode,
                contact12.getText().toString(),
                contact14.getText().toString(),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.UNFILTERED_HOSPITAL_CODE, getContext())

        );

        SharedPref.setStringValue(SharedPref.USER, SharedPref.CONTACT_NO, contact12.getText().toString(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.EMAIL, contact14.getText().toString(), context);

        SharedPref.TestOrOtherTest = sendBasicTestModel;

        Gson gson = new Gson();
        Log.d("OTHER_TEST_TO_SEND", gson.toJson(sendBasicTestModel));

        AvailServicesAPICall.checkForDuplicateRequestTestProc(Constant.OTHER_TEST, callback, sendBasicTestModel);
//        AvailServicesAPICall.sendOtherTestRequest(implement.getDataDiagnosis(arrayPrimary), callback, sendBasicTestModel, Constant.OTHER_TEST);

    }

    //send procedures )))))))))))))))))))))))
    @Override
    public void sendProcedures() {
        loader.startLad();
        BasicTestOrOtherTest sendBasicTestModel = implement.collectProcedures(
                Constant.PROCEDURES,
                SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, getContext()),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, getContext()),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, getContext()),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, getContext()),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.SEARCHTYPE, context),
                arrayProcedure,
                primaryDiag2,
                getDoctorCode,
                tv_proc_remarks.getText().toString(),
                contact12.getText().toString(),
                contact14.getText().toString()
        );

        SharedPref.setStringValue(SharedPref.USER, SharedPref.CONTACT_NO, contact12.getText().toString(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.EMAIL, contact14.getText().toString(), context);

        SharedPref.TestOrOtherTest = sendBasicTestModel;

        Gson gson = new Gson();
        Log.d("BASIC_TEST", gson.toJson(sendBasicTestModel));

        AvailServicesAPICall.checkForDuplicateRequestTestProc(Constant.PROCEDURES, callback, sendBasicTestModel);
    }


    @Override
    public void sendConsultationError(String message) {
        dialogCustom.showMe(getContext(), dialogCustom.unknown, ErrorMessage.setErrorMessage(message), 1);
        session.releaseContent();
        loader.stopLoad();
    }

    @Override
    public void sendProcedureError(String message) {
        Log.d("ERROR", message);
        dialogCustom.showMe(getContext(), dialogCustom.unknown, "Please Select Test", 1);
        session.releaseContent();
        loader.stopLoad();
    }

    @Override
    public void sendClinicProcedureError(String message) {
        Log.d("ERROR", message);
        dialogCustom.showMe(getContext(), dialogCustom.unknown, "Please Clinic Procedure", 1);
        session.releaseContent();
        loader.stopLoad();
    }


    @Override
    public void dischargeERSuccess() {

        dialogCustom.showMe(getContext(), dialogCustom.CONGRATULATIONS, "The Member is Discharge from ER", 1);
        loader.stopLoad();
    }


    @Override
    public void onProceduresError(String message) {
        arrayPrimary.clear();
        arrayOther.clear();
        arrayProcedure.clear();
        loader.stopLoad();
        dialogCustom.showMe(getContext(), dialogCustom.unknown, ErrorMessage.setErrorMessage(message), 1);
    }

    @Override
    public void sendConsultationSuccess(LOAReturn loaReturn, String ORIGIN) {

        if (loaReturn.getResponseCode() != null) {
            loader.stopLoad();
            et_problem.setText("");
//            testDuplicateRequest(loaReturn, ORIGIN);
            sentDatatoResult(loaReturn, ORIGIN);
            loader.stopLoad();
        } else {
            dialogCustom.showMe(getContext(), dialogCustom.unknown, ErrorMessage.setErrorMessage("500"), 1);
        }

    }

    @Override
    public void sendConsultationDuplicate(Confirm loaReturn, String ORIGIN) {

        //Show Dialog, then pass "send" object

        if (loaReturn.getResponseCode() != null) {
            et_problem.setText("");
            dialogCustom.showMeValidateReq(loaReturn.getResponseDesc(), getContext()
                    , callback, ORIGIN);
            loader.stopLoad();
        } else {
            dialogCustom.showMe(getContext(), dialogCustom.unknown, ErrorMessage.setErrorMessage("500"), 1);
        }
    }


    @Override
    public void sendMaternityDuplicate(Confirm loaReturn, String ORIGIN) {

        //Show Dialog, then pass "send" object

        if (loaReturn.getResponseCode() != null) {
            et_problem.setText("");
            dialogCustom.showMeValidateReq(loaReturn.getResponseDesc(), getContext()
                    , callback, ORIGIN);
            loader.stopLoad();
        } else {
            dialogCustom.showMe(getContext(), dialogCustom.unknown, ErrorMessage.setErrorMessage("500"), 1);
        }
    }


    @Override
    public void onBasicTestSuccess(BasicTestOrOtherTestReturn getOtherTestReturnModel, String ORIGIN) {
        loader.stopLoad();
//        session.releaseContent();
        dataToDisplayBasicTest(getOtherTestReturnModel, ORIGIN, 3);

    }

    @Override
    public void onBasicTestDuplicate(Confirm confirm, String ORIGIN) {
//        session.releaseContent();
        if (confirm.getResponseCode() != null) {
            et_problem.setText("");
            dialogCustom.showMeValidateReq(confirm.getResponseDesc(), getContext()
                    , callback, ORIGIN);
            loader.stopLoad();
        } else {
            dialogCustom.showMe(getContext(), dialogCustom.unknown, ErrorMessage.setErrorMessage("500"), 1);
        }
    }


    @Override
    public void onOtherTestSuccess(BasicTestOrOtherTestReturn body, String ORIGIN) {
        loader.stopLoad();
        arrayPrimary.clear();
        arrayOther.clear();

//        dataToDisplay(body, ORIGIN, 0);
        testDuplicateRequestBasicOrOtherTest(body, ORIGIN);
//        otherTestToDisplay.addAll(body);

    }

    @Override
    public void onOtherTestDuplicate(Confirm confirm, String ORIGIN) {

//        arrayPrimary.clear();
//        arrayOther.clear();
//        testDuplicateRequestBasicOrOtherTest(body, ORIGIN);
//        proceduresToDisplay.addAll(body);
        if (confirm.getResponseCode() != null) {
            et_problem.setText("");
            dialogCustom.showMeValidateReq(confirm.getResponseDesc(), getContext()
                    , callback, ORIGIN);
            loader.stopLoad();
        } else {
            dialogCustom.showMe(getContext(), dialogCustom.unknown, ErrorMessage.setErrorMessage("500"), 1);
        }
    }


    @Override
    public void onInPatientSuccess(GetInPatientReturn body, ArrayList<DoctorModelInsertion> doctorCodes, ArrayList<DiagnosisList> diagCodes, ArrayList<TestsAndProcedures> procCodes, String roomRate, String roomNo, String dateTimeAdmitted, String category) {

        et_input_category.setText("");
        et_input_room_number.setText("");
        et_input_room_type.setText("");
        et_input_room_price.setText("");
        tv_inpatient_diagnosis.setText("");
        tv_inpatient_doctor.setText("");
        tv_admitted.setText("");

        Intent gotoResult = new Intent(getContext(), ResultActivity.class);
        gotoResult.putExtra("REQUEST", "IN_PATIENT");
        //TODO: FIX THIS EXTRAS AFTER THE SERVICE IS FIX
//        gotoResult.putExtra("RESPONCE_CODE", getOtherTestReturnModel.getResponseCode());
//        // gotoResult.putExtra("APPROVAL_NO", getOtherTestReturnModel.getApprovalNo());
//        gotoResult.putExtra("RESPONCE_DESC", getOtherTestReturnModel.getResponseDesc());
//        gotoResult.putExtra("REMARKS", getOtherTestReturnModel.getRemarks());

        gotoResult.putExtra("ROOM_PRICE", roomRate);
        gotoResult.putExtra("ROOM_PLAN", "");
        gotoResult.putExtra("ROOM_NUMBER", roomNo);
        gotoResult.putExtra("CATEGORY", category);
        gotoResult.putExtra("DATEADMITTED", DateConverter.convertDatetoyyyyMMdd(dateTimeAdmitted));
        startActivity(gotoResult);

        loader.stopLoad();
    }
//ER EDIT

    TextView tv_message, tv_title1;
    CircleImageView ci_error_image;
    Button btn_accept;

    @Override
    public void onERSuccess(GetEr GetEr, String dateAdmitted, String er_reason) {


        tv_admitted.setText("");
        et_er_reason.setText("");

// ER v2
        Intent gotoResult = new Intent(getContext(), ResultActivity.class);
        gotoResult.putExtra("REQUEST", "ER");
        System.out.println("REQUEST: ER");
        gotoResult.putExtra("ER_REASON", er_reason);
        gotoResult.putExtra("DATEADMITTED", DateConverter.convertDatetoyyyyMMdd(dateAdmitted));
        startActivity(gotoResult);

        loader.stopLoad();

//
//        final Dialog dialog = new Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.alertshow);
//        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        dialog.setCancelable(false);
//
//        ci_error_image = (CircleImageView) dialog.findViewById(R.id.ci_error_image);
//        tv_message = (TextView) dialog.findViewById(R.id.tv_message);
//        tv_title1 = (TextView) dialog.findViewById(R.id.tv_title);
//        btn_accept = (Button) dialog.findViewById(R.id.btn_accept);
//        btn_accept.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, MemberSearchActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        tv_message.setText("The member is admitted as ER");
//
//
//        dialog.show();
//
//
//        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.40);
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.width = width;
//        dialog.getWindow().setAttributes(lp);
//    }

    }

    @Override
    public void onDischargeErSuccess(String id, String username) {


    }

    @Override
    public void procedurePrimaryCollectedSuccess(String code, ProcedureFilter body, String ORIGIN) {
        Log.d("ORIGIN", ORIGIN);


        if (ORIGIN.equals(Constant.FROM_OTHER_PROC)) {
            isOtherAvailable = true;
            proceduresOtherList = body.getProcedures();
            implement.setLoadingPrimaryGone(tv_other_proc, pb_other_diag, true);
            otherProcedure.notifyDataSetChanged();
        } else if (ORIGIN.equals(Constant.FROM_PRIMARY_PROC)) {
            isPrimaryAvailable = true;
            proceduresPrimaryList = body.getProcedures();
            implement.setLoadingPrimaryGone(tv_primary, pb_primary_diag, true);
            primaryProcedures.notifyDataSetChanged();

        } else if (ORIGIN.equals(Constant.FROM_OTHER_PROC_2)) {
            isOtherAvailable = true;
            proceduresOtherList = body.getProcedures();
            implement.setLoadingPrimaryGone(tv_other_proc_second, pb_other_diag_second, true);
            otherProcedureSecond.notifyDataSetChanged();
        } else if (ORIGIN.equals(Constant.FROM_PRIMARY_PROC_2)) {
            isPrimary_2_Available = true;
            proceduresPrimary2List = body.getProcedures();
            implement.setLoadingPrimaryGone2(tv_primary_diag_2, pb_primary_diag_2, true);
            proceduresToBeSendAdapter.notifyDataSetChanged();
        }

        implement.setLoadingPrimaryGone(tv_other_proc, pb_other_diag, true);
        implement.setLoadingPrimaryGone(tv_primary, pb_primary_diag, true);
        implement.setLoadingPrimaryGone2(tv_primary_diag_2, pb_primary_diag_2, true);
    }

    @Override
    public void procedurePrimaryCollectedError(String tMessage, String ORIGIN) {
        Log.e("ORIGIN", ORIGIN);
        if (ORIGIN.equals(Constant.FROM_OTHER_PROC)) {
            implement.setLoadingPrimaryGone(tv_other_proc, pb_other_diag, false);
        } else if (ORIGIN.equals(Constant.FROM_PRIMARY_PROC)) {
            implement.setLoadingPrimaryGone(tv_primary, pb_primary_diag, false);
        }

    }

    // ERROR MESSAGES
    @Override
    public void onPrimaryDiagNotAvailable() {
        dialogCustom.showMe(getContext(), dialogCustom.unknown, getString(R.string.no_primary), 1);
    }

    @Override
    public void onAdmittedNotAvailable() {
        dialogCustom.showMe(getContext(), dialogCustom.unknown, getString(R.string.no_admitted), 1);
    }

    @Override
    public void onAdmittedTimeNotAvailable() {
        dialogCustom.showMe(getContext(), dialogCustom.unknown, getString(R.string.no_admitted_time), 1);
    }


    @Override
    public void onProcedureNotAvailable() {
        dialogCustom.showMe(getContext(), dialogCustom.unknown, getString(R.string.no_procedures), 1);
    }

    @Override
    public void onDiagnosisNotAvailable() {
        dialogCustom.showMe(getContext(), dialogCustom.unknown, getString(R.string.no_diagnosis), 1);
    }


    // EVENT BUS TRIGGER
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        implement.setEventTrigger(event, isFilledUpAndCannotbeEdited);
    }


    @Override
    public void onStart() {
        Log.d("START", "START");
        if (isPreventingDeleteFromOnStop) {
            isPreventingDeleteFromOnStop = false;
        } else {
            EventBus.getDefault().register(this);
        }
        super.onStart();
    }


    @Override
    public void onStop() {
        if (!isPreventingDeleteFromOnStop) {
            EventBus.getDefault().unregister(this);
            deleteData();
        }

        super.onStop();
    }


    public static class MessageEvent {


        public ArrayList<TestsAndProcedures> arrayProc = new ArrayList<>();
        public ArrayList<DiagnosisList> arrayDiag = new ArrayList<>();

        public ArrayList<DoctorModelInsertion> doctorModelInsertions = new ArrayList<>();
        public ArrayList<DiagnosisList> diagnosisModelInsertions = new ArrayList<>();
        public ArrayList<TestsAndProcedures> procedureModelInsertions = new ArrayList<>();

        private String Origin;
        private String message;
        private String MEMBER_ID;
        private String doctorCode;
        private String hospitalCode;
        private String doctorCreds;
        private String doctorName;
        private String hospitalName;
        private LoaList loa;
        private DiagnosisList diagnosisList;
        private Rooms rooms;
        private Hospital hospitalList;

        public MessageEvent(String message, ArrayList<DoctorModelInsertion> doctorModelMultipleSelections, String s, String s1) {
            this.message = message;
            this.doctorModelInsertions = doctorModelMultipleSelections;
        }

        public MessageEvent(String message, ArrayList<DiagnosisList> diagnosisModelInsertions, String s1, String s2, String s3) {
            this.message = message;
            this.diagnosisModelInsertions = diagnosisModelInsertions;
        }

        public MessageEvent(String message, ArrayList<TestsAndProcedures> procedureModelInsertions, String s1, String s2, String s3, String s4) {
            this.message = message;
            this.procedureModelInsertions = procedureModelInsertions;
        }


        public ArrayList<DoctorModelInsertion> getDoctorModelMultipleSelections() {
            return doctorModelInsertions;
        }

        public void setDoctorModelMultipleSelections(ArrayList<DoctorModelInsertion> doctorModelMultipleSelections) {
            this.doctorModelInsertions = doctorModelMultipleSelections;
        }

        public Hospital getHospitalList() {
            return hospitalList;
        }

        public void setHospitalList(Hospital hospitalList) {
            this.hospitalList = hospitalList;
        }

        public MessageEvent(String message, String Origin) {
            this.message = message;
            this.Origin = Origin;
        }

        public MessageEvent(String message, LoaList loa) {
            this.message = message;
            this.loa = loa;
        }

        public ArrayList<DoctorModelInsertion> getDoctorModelInsertions() {
            return doctorModelInsertions;
        }

        public void setDoctorModelInsertions(ArrayList<DoctorModelInsertion> doctorModelInsertions) {
            this.doctorModelInsertions = doctorModelInsertions;
        }

        public ArrayList<TestsAndProcedures> getProcedureModelInsertions() {
            return procedureModelInsertions;
        }

        public void setProcedureModelInsertions(ArrayList<TestsAndProcedures> procedureModelInsertions) {
            this.procedureModelInsertions = procedureModelInsertions;
        }

        public MessageEvent(String message, ArrayList<TestsAndProcedures> arrayList) {
            arrayProc = arrayList;
            this.message = message;
        }


        public MessageEvent(String message, ArrayList<DiagnosisList> arrayList, String msg1) {
            arrayDiag = arrayList;
            this.message = message;
        }

        public MessageEvent(String message, DiagnosisList diagnosisList) {
            this.message = message;
            this.diagnosisList = diagnosisList;
        }

        public MessageEvent(String message, String MEMBER_ID, String doctorCode, String hospitalCode, String doctorCreds, String doctorName, String hospitalName) {
            this.doctorCode = doctorCode;
            this.hospitalCode = hospitalCode;
            this.doctorCreds = doctorCreds;
            this.MEMBER_ID = MEMBER_ID;
            this.message = message;
            this.doctorName = doctorName;
            this.hospitalName = hospitalName;

        }

        public ArrayList<DiagnosisList> getDiagnosisModelInsertions() {
            return diagnosisModelInsertions;
        }

        public void setDiagnosisModelInsertions(ArrayList<DiagnosisList> diagnosisModelInsertions) {
            this.diagnosisModelInsertions = diagnosisModelInsertions;
        }

        public MessageEvent(String message, String MEMBER_ID, String doctorCode, String hospitalCode, String doctorCreds, String doctorName) {
            this.doctorCode = doctorCode;
            this.hospitalCode = hospitalCode;
            this.doctorCreds = doctorCreds;
            this.MEMBER_ID = MEMBER_ID;
            this.message = message;
            this.doctorName = doctorName;

        }

        public MessageEvent(String s, Hospital hospital) {
            this.message = s;
            this.hospitalList = hospital;

        }

        public MessageEvent(String message, Rooms roomPlan) {
            this.message = message;
            this.rooms = roomPlan;
        }

        public MessageEvent(String message, String hospitalName, String hospitalCode) {
            this.message = message;
            this.hospitalName = hospitalName;
            this.hospitalCode = hospitalCode;

        }

        public Rooms getRooms() {
            return rooms;
        }

        public void setRooms(Rooms rooms) {
            this.rooms = rooms;
        }

        public String getOrigin() {
            return Origin;
        }

        public void setOrigin(String origin) {
            Origin = origin;
        }

        public LoaList getLoa() {
            return loa;
        }

        public void setLoa(LoaList loa) {
            this.loa = loa;
        }

        public String getHospitalName() {
            return hospitalName;
        }

        public void setHospitalName(String hospitalName) {
            this.hospitalName = hospitalName;
        }


        public DiagnosisList getDiagnosisList() {
            return diagnosisList;
        }

        public void setDiagnosisList(DiagnosisList diagnosisList) {
            this.diagnosisList = diagnosisList;
        }


        public ArrayList<DiagnosisList> getArrayDiag() {
            return arrayDiag;
        }


        public String getMessage() {
            return message;
        }

        public String getDoctorCode() {
            return doctorCode;
        }

        public String getHospitalCode() {
            return hospitalCode;
        }

        public String getDoctorCreds() {
            return doctorCreds;
        }

        public String getMEMBER_ID() {
            return MEMBER_ID;
        }

        public String getDoctorName() {
            return doctorName;
        }

        public ArrayList<TestsAndProcedures> getArrayProc() {
            return arrayProc;
        }


    }
}
