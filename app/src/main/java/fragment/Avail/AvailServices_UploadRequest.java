package fragment.Avail;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.pdf.codec.Base64;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import adapter.AttachmentAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import coord.medicard.com.medicardcoordinator.R;
import model.ImageAttachment;
import model.Test.Attachment_uploadRequest;
import utilities.AlertDialogCustom;
import utilities.AttachmentSession;
import utilities.Constant;
import utilities.FileUtils;
import utilities.ImageConverters;
import utilities.Permission;
import utilities.pdfUtil.PermissionUtililities;

import static android.app.Activity.RESULT_OK;

/**
 * Created by IPC on 11/24/2017.
 */

public class AvailServices_UploadRequest extends Fragment implements  AttachmentAdapter.OnAttachmentClickListener, CompoundButton.OnCheckedChangeListener {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.btnAddAttachment)
    Button btnAddAttachment;
    @BindView(R.id.rvAttachments)
    RecyclerView rvAttachments;
    @BindView(R.id.cb_informed)
    CheckBox cb_informed;
    @BindView(R.id.btn_proceed)
    Button btn_proceed;
    @BindView(R.id.btn_cancel)
    Button btn_cancel;

    AlertDialogCustom dialogCustom;
    Context context;

    public String GALLERY = "Gallery";
    public String CAMERA = "Camera";
    public String CANCEL = "Cancel";

    private AttachmentAdapter attachmentAdapter;
    private List<ImageAttachment> imageAttachments;

    private Uri mCapturedImageURI = null;
    private List<Attachment_uploadRequest> attachments;

    AlertDialog.Builder builder;
    AlertDialog alert;

    public static final int SELECT_PICTURE = 100;
    public static final int SELECT_PICTURE_FROM_CAMERA = 101;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public int o = 0;
    public int count = 0;
    public File f;
    final int CAMERA_RQ = 290;

    int counter = 0;

    AttachmentSession attachmentSession;

    ProgressDialog pd;
    private Uri fileUri;

    Bitmap photo;
    private String encodedString;
    public AvailServices_UploadRequest() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_avail_service_uploadrequest, container, false);
        ButterKnife.bind(this, view);
        context = getContext();
        dialogCustom = new AlertDialogCustom();
        tv_title.setText(context.getString(R.string.uploadFormMenu));
        imageAttachments = new ArrayList<>();
        pd = new ProgressDialog(getContext());
        attachments = new ArrayList<>();
        attachmentAdapter = new AttachmentAdapter(attachments, this);

        rvAttachments.setLayoutManager(new LinearLayoutManager(context));
        rvAttachments.setAdapter(attachmentAdapter);

        if (!isDeviceSupportCamera()) {
            Toast.makeText(context,
                    "Sorry! Your device doesn't support camera",
                    Toast.LENGTH_LONG).show();
        }


        cb_informed.setOnCheckedChangeListener(this);

    return  view;
    }






    @OnClick(R.id.btnAddAttachment)
    public void onUpload() {
        if (attachments.size() != 3) {
            if (PermissionUtililities.hasPermissionToReadAndWriteStorage(getActivity())) {
                showMe(context);
            }
        } else {
            dialogCustom.showMe(context,dialogCustom.unknown,"Exceeded number of attachments!",1);
        }

    }

    public void showMe(Context contex) {

        final CharSequence[] items = {CAMERA, GALLERY, CANCEL};

        builder = new AlertDialog.Builder(contex);
        alert = builder.create();
        alert.show();
        builder.setTitle("Choose Option:");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                setDetails(items[item]);

            }
        }).show();

    }

    private void setDetails(CharSequence item) {


        if (item.equals(GALLERY)) {
            uploadAttachment();
            alert.dismiss();
        }

        if (item.equals(CAMERA)) {
            if (Permission.checkPermissionCamera(context)) {
                if (Permission.checkPermissionStorage(context)) {
                    takePhoto();
                }
                alert.dismiss();
            }
        }


        if (item.equals(CANCEL)) {
            alert.dismiss();
        }
    }

    @Override
    public void onRemoveAttachment(int position) {
//remove the current attachemt's position
        attachments.remove(position);
        //attachmentAdapter.update(imageAttachments);
        attachmentAdapter.update(attachments);
    }

    public void uploadAttachment() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK && requestCode == SELECT_PICTURE) {

                Uri selectImageUri = data.getData();
                Log.d("uri : %s", selectImageUri.toString());
                if (selectImageUri != null) {


                    try {

                        Bitmap picture = MediaStore.Images.Media.getBitmap(context.getContentResolver(), selectImageUri);
                        File file = new File(FileUtils.getPathFromURI(context, FileUtils.getImageUri(context, picture)));


                        System.out.println("File to save from gallery" + file);
                        attachmentSession.addAttachemnt(file);
                        counter++;
                        Log.d("counter %s", String.valueOf(counter));

                        try {
                            pd.setProgressStyle(R.style.MyTheme);
                            pd.setMessage("Loading");
                            pd.show();
                        /*imageAttachments.add(new ImageAttachment.Builder()
                                .image(picture)
                                .fileName(file.getName())
                                .build());
                        attachmentAdapter.update(imageAttachments);*/
                            attachments.add(
                                    new Attachment_uploadRequest.Builder()
                                            .fileName(file.getName())
                                            .uri(selectImageUri.toString())
                                            .build());
                            attachmentAdapter.update(attachments);

                            pd.dismiss();


                        } catch (Exception e) {
                            Log.e("error message %s", e.toString());

                        }
                    } catch (IOException e) {
                        Log.e("error message %s", e.toString());

                    }
                }
            } else if (requestCode == CAMERA_RQ) {

                if (resultCode != Activity.RESULT_CANCELED) {
                    if (data != null) {
                        ImageConverters imageConverters = new ImageConverters();

                        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

                        photo = (Bitmap) data.getExtras().get("data");
                        Bitmap bitmapOrg = photo;
                        ByteArrayOutputStream bao = new ByteArrayOutputStream();
                        bitmapOrg.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                        byte[] ba = bao.toByteArray();


                        try {
                            encodedString = Base64.encodeBytes(ba, 0);
                        } catch (Exception e1) {
                        }

                        if (android.os.Environment.getExternalStorageState()
                                .equals(android.os.Environment.MEDIA_MOUNTED)) {

                            String fileName = +count + ".jpg";
                            f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                                    "/IMG_" + fileName);
                            try {
                                f.createNewFile();
                                FileOutputStream fo = new FileOutputStream(f);
                                fo.write(ba);
                                fo.close();
                            } catch (IOException e) {

                            }


                        }
                        attachmentSession.addAttachemnt(f);
                        counter++;

                        try {
                        /*imageAttachments.add(new ImageAttachment.Builder()
                                .image(picture)
                                .fileName(file.getName())
                                .build());
                        attachmentAdapter.update(imageAttachments);*/
                            attachments.add(
                                    new Attachment_uploadRequest.Builder()
                                            .fileName(f.getName())
                                            .build());
                            attachmentAdapter.update(attachments);

                        } catch (Exception e) {
                            Log.e("error message %s", e.toString());

                        }


                    }
                } else {


                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PermissionUtililities.REQUESTCODE_STORAGE_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    uploadAttachment();

                } else {
                    Log.e("permission denied","permission denied");
                }
            }

            return;
        }
    }

    private void takePhoto() {

        //increment the number of image from the camera intent
        count++;
        Intent i = new Intent("android.media.action.IMAGE_CAPTURE");
        // start the image capture Intent
        startActivityForResult(i, CAMERA_RQ);
    }

    /**
     * Checking device has camera hardware or not
     */
    private boolean isDeviceSupportCamera() {
        if (context.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), Constant.IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {

                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");

        } else {
            return null;
        }

        return mediaFile;
    }


    public void setCheckBox(CheckBox cb_informed, Button btn_proceed) {

        if (cb_informed.isChecked()) {
            btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
            btn_proceed.setEnabled(true);
            btn_proceed.setClickable(true);
        } else {
            btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.gray));
            btn_proceed.setEnabled(false);
            btn_proceed.setClickable(false);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
            if(attachments.isEmpty())
                cb_informed.setChecked(false);
            setCheckBox(cb_informed, btn_proceed);
        }else {
            setCheckBox(cb_informed, btn_proceed);
        }
    }
}
