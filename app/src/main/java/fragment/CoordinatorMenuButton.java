package fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import coord.medicard.com.medicardcoordinator.R;
import fragment.Avail.AvailServices;
import fragment.Avail.AvailServices_UploadRequest;
import utilities.AlertDialogCustom;
import utilities.APIMultipleCalls.RequestBlockLogger;
import utilities.SharedPref;

public class CoordinatorMenuButton extends Fragment implements View.OnClickListener {

    int buttonCount = 6;
    LinearLayout[] background = new LinearLayout[buttonCount];
    TextView[] name = new TextView[buttonCount];
    ImageView[] image = new ImageView[buttonCount];
    Context context;

    Fragment fragment;
    FragmentTransaction fragmentTransaction;
    ProgressDialog pd;
    AlertDialogCustom alertDialogCustom = new AlertDialogCustom();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.menubutton, container, false);

        name[0] = (TextView) view.findViewById(R.id.tv_consult);
        name[1] = (TextView) view.findViewById(R.id.tv_maternity);
        name[2] = (TextView) view.findViewById(R.id.tv_basic);
        name[3] = (TextView) view.findViewById(R.id.tv_other);
        name[4] = (TextView) view.findViewById(R.id.tv_proc);
        name[5] = (TextView) view.findViewById(R.id.tv_upload);

        image[0] = (ImageView) view.findViewById(R.id.iv_consult);
        image[1] = (ImageView) view.findViewById(R.id.iv_maternity);
        image[2] = (ImageView) view.findViewById(R.id.iv_basic);
        image[3] = (ImageView) view.findViewById(R.id.iv_other);
        image[4] = (ImageView) view.findViewById(R.id.iv_proc);
        image[5] = (ImageView) view.findViewById(R.id.iv_upload);

        background[0] = (LinearLayout) view.findViewById(R.id.ll_consult);
        background[1] = (LinearLayout) view.findViewById(R.id.ll_maternity);
        background[2] = (LinearLayout) view.findViewById(R.id.ll_basic);
        background[3] = (LinearLayout) view.findViewById(R.id.ll_other);
        background[4] = (LinearLayout) view.findViewById(R.id.ll_proc);
        background[5] = (LinearLayout) view.findViewById(R.id.ll_upload);

        background[0].setOnClickListener(this);
        background[1].setOnClickListener(this);
        background[2].setOnClickListener(this);
        background[3].setOnClickListener(this);
        background[4].setOnClickListener(this);
        background[5].setOnClickListener(this);
        context = getContext();


        pd = new ProgressDialog(getContext(), R.style.MyTheme);
        pd.setCancelable(false);
        pd.setMessage("Authenticating...");
        pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

        return view;


    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_consult:

                if (SharedPref.getStringValue(SharedPref.USER, SharedPref.Consultation, context).equals("TRUE")) {
                    setDecor(0,false);
                } else {
                    RequestBlockLogger.logger(
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.USERNAME, context),
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context),
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context),
                            "Consultation is not available for the member.",
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, context));

                    alertDialogCustom.showMe(context, alertDialogCustom.unknown, "Consultation is not available for the member.", 1);
                }


                break;
            case R.id.ll_maternity:


                if (SharedPref.getStringValue(SharedPref.USER, SharedPref.GENDER, context).equals("MALE")) {

                    RequestBlockLogger.logger(
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.USERNAME, context),
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context),
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context),
                            "Maternity Consultation is not available for male.",
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, context));

                    alertDialogCustom.showMe(context, alertDialogCustom.unknown, "Maternity Consultation is not available for male.", 1);
                } else {

                    if (SharedPref.getStringValue(SharedPref.USER, SharedPref.Consultation, context).equals("TRUE")) {
                        if (SharedPref.getStringValue(SharedPref.USER, SharedPref.HAS_MATERNITY, context).equals("true")) {
                            setDecor(1,false);
                        } else {
                            alertDialogCustom.showMe(context, alertDialogCustom.unknown, getString(R.string.member_has_no_maternity), 1);
                        }
                    } else {

                        RequestBlockLogger.logger(
                                SharedPref.getStringValue(SharedPref.USER, SharedPref.USERNAME, context),
                                SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context),
                                SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context),
                                "Maternity is not available for the member.",
                                SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, context));


                        alertDialogCustom.showMe(context, alertDialogCustom.unknown, "Maternity Consultation is not available for the member.", 1);
                    }
                }


                break;
            case R.id.ll_basic:
                // showDialog();
                if (SharedPref.getStringValue(SharedPref.USER, SharedPref.Consultation, context).equals("TRUE")) {
                    setDecor(2,false);
                } else {

                    RequestBlockLogger.logger(
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.USERNAME, context),
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context),
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context),
                            "Basic Test is not available for the member.",
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, context));


                    alertDialogCustom.showMe(context, alertDialogCustom.unknown, "Basic Test is not available for the member.", 1);
                }
                break;
            case R.id.ll_other:

                if (SharedPref.getStringValue(SharedPref.USER, SharedPref.OtherTest, context).equals("TRUE")) {

                    setDecor(3,false);
                } else {


                    RequestBlockLogger.logger(
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.USERNAME, context),
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context),
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context),
                            "Other Test is not available for the member.",
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, context));

                    alertDialogCustom.showMe(context, alertDialogCustom.unknown, "Other Test is not available for the member.", 1);
                }

                break;

            case R.id.ll_proc:


                // if (SharedPref.getStringValue(SharedPref.USER, SharedPref.Procedure, context).equals("TRUE")) {

                setDecor(4,false);
//                } else {
//
//
//                    RequestBlockLogger.logger(
//                            SharedPref.getStringValue(SharedPref.USER, SharedPref.USERNAME, context),
//                            SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context),
//                            SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context),
//                            "Other Test is not available for the member.",
//                            SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, context));
//
//                    alertDialogCustom.showMe(context, alertDialogCustom.unknown, "Other Test is not available for the member.", 1);
//                }

                break;

            case R.id.ll_upload:


                // if (SharedPref.getStringValue(SharedPref.USER, SharedPref.Procedure, context).equals("TRUE")) {

                setDecor(5,true);
//                } else {
//
//
//                    RequestBlockLogger.logger(
//                            SharedPref.getStringValue(SharedPref.USER, SharedPref.USERNAME, context),
//                            SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context),
//                            SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context),
//                            "Other Test is not available for the member.",
//                            SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, context));
//
//                    alertDialogCustom.showMe(context, alertDialogCustom.unknown, "Other Test is not available for the member.", 1);
//                }

                break;
        }

    }

    private void switchPanel(int place,boolean isUploadRequest) {


        if(!isUploadRequest){
            fragment = new AvailServices();
            fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.left_to_right, R.anim.right_to_left);
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
            getActivity().getSupportFragmentManager().executePendingTransactions();
            /**
             * It will send number to AVAIL SERVICES
             * TO CATCH WHAT BUTTON IS TAPPED
             */

            EventBus.getDefault().post(new AvailServices.MessageEvent(getPlace(place) + "", "", "", "", "", ""));
            Log.d("FLAG_BUTTON", place + "");
        }else {
            // for upload requestbutton
            fragment = new AvailServices_UploadRequest();
            fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.left_to_right, R.anim.right_to_left);
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
            getActivity().getSupportFragmentManager().executePendingTransactions();
        }

    }

    private String getPlace(int place) {
        return place == 4 ? "12" : place + "";
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void setDecor(int place, boolean isUploadRequest) {


        image[0].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.lifestyle_icon));

        image[1].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.maternity_b));

        image[2].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.basic_test));

        image[3].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.other_proc));

        image[4].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.proc));

        image[5].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_upload));

        for (int x = 0; x < buttonCount; x++) {
            background[x].setBackgroundColor(ContextCompat.getColor(context, R.color.WHITE_));
            name[x].setTextColor(ContextCompat.getColor(context, R.color.BLACK));

        }


        background[place].setBackground(ContextCompat.getDrawable(context, R.drawable.background_selected));
        name[place].setTextColor(ContextCompat.getColor(context, R.color.WHITE_));


        if (place == 0) {
            image[0].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.lifestyle_icon_w));
        } else if (place == 1) {
            image[1].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.maternity_b_w));
        } else if (place == 2) {
            image[2].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.basic_text_w));
        } else if (place == 3) {
            image[3].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.other_proc_w));
        } else if (place == 4) {
            image[4].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.proc_selected));
        } else if (place == 5) {
            image[5].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_upload));
//            image[5].se(ContextCompat.getDrawable(context, R.drawable.ic_upload));

        }


        /**
         *
         * switch to AVAILFRAGMENT based on number/ id of button
         */
        switchPanel(place,isUploadRequest);
    }


}
