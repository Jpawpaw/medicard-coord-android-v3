package fragment.InPatient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import adapter.AttachmentsAdapter;
import adapter.DiagnosisToBeSendAdapter;
import adapter.DoctorMultipleToSendAdapter;
import adapter.OtherServicesAdapterToSend;
import adapter.ProceduresAdapterInPatient;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import coord.medicard.com.medicardcoordinator.MemberSearch.MemberSearchActivity;
import coord.medicard.com.medicardcoordinator.Navigator.NavigatorActivity;
import coord.medicard.com.medicardcoordinator.R;
import coord.medicard.com.medicardcoordinator.Result.ResultActivity;
import model.Attachments;
import model.DiagnosisList;
import model.DoctorModelInsertion;
import model.DoctorModelMultipleSelection;
import model.InPatientUpdate;
import model.ListingUpdateReturn;
import model.ProcedureFetchedData;
import model.ProcedureFilter;
import model.Rooms;
import model.Services;
import model.TestsAndProcedures;
import utilities.AlertDialogCustom;
import utilities.Constant;
import utilities.DataBaseHandler;
import utilities.DateConverter;
import utilities.DatepickerSet;
import utilities.APIMultipleCalls.FilteredProcedureCall;
import utilities.ErrorMessage;
import utilities.GetImageFileName;
import utilities.SharedPref;
import utilities.TimepickerSet;

public class InPatientAdmitted extends Fragment implements InPatientCallback, FilteredProcedureCall.FilterProcedureCallback, OtherServicesAdapterToSend.OtherServicesCallBack {


    int counterProcSuccess;
    int counterProcError;
    ArrayList<Attachments> arrayAttachments = new ArrayList<>();
    ArrayList<ProcedureFilter> procDiagnosisFetched = new ArrayList<>();
    ArrayList<ProcedureFetchedData> arrayProcedureFetched = new ArrayList<>();
    ArrayList<Services> arrayOtherServices = new ArrayList<>();
    InPatientImplement implement;
    InPatientCallback callback;
    FilteredProcedureCall.FilterProcedureCallback filterProcedureCallback;
    Context context;
    DataBaseHandler dataBaseHandler;

    DoctorMultipleToSendAdapter doctorMultipleToSendAdapter;
    ProceduresAdapterInPatient procedureAdapter;
    DiagnosisToBeSendAdapter otherDiagnosis;
    AttachmentsAdapter attachmentsAdapter;
    OtherServicesAdapterToSend otherServicesAdapterToSendadapter;
    AlertDialogCustom alertDialogCustom;

    String disChargeTime = "";
    String disChargeDate = "";
    String admittedDate = "";
    String admittedTime = "";

    OtherServicesAdapterToSend.OtherServicesCallBack otherServicesCallBack;
    ArrayList<TestsAndProcedures> arrayProcedure = new ArrayList<>();
    ArrayList<DoctorModelMultipleSelection> arrayDoctor = new ArrayList<>();
    ArrayList<DiagnosisList> arrayOtherDiagnosis = new ArrayList<>();
    private static final String in_patient_body = "in_patient_body";
    private static final String in_patient_doctor = "in_patient_doctor";
    private static final String in_patient_request_code = "in_patient_room_plan";
    private static final String in_patient_room_plan = "in_patient_request_code";
    private static final String in_patient_category = "in_patient_category";
    private static final String in_patient_room_no = "in_patient_room_no";
    private static final String in_patient_diagnosis = "in_patient_diagnosis";
    private static final String in_patient_admitted_on = "in_patient_admitted_on";
    private static final int SELECT_PICTURE = 150;
    private String OriginForUpdatingErrorFetch = "";
    String[] datain_Patient;
    DoctorModelInsertion preSelectedDoctor;
    String primaryDoc = "";
    Rooms preSelectedRoomPlan;
    DiagnosisList primaryDiagnosis;
    String pimaryDiag = "";
    String requestCode = "";
    String roomNo = "";
    String roomCategory = "";
    String admittedOn = "";
    private boolean isProceduresReady = false;
    private boolean isProcedureFetchError = false;
    private boolean isOtherTestReady = false;
    private String[] proceduresList;

    @BindView(R.id.rv_in_patient_procedure)
    RecyclerView rv_in_patient_procedure;
    @BindView(R.id.rl_in_patient_procedure)
    RelativeLayout rl_in_patient_procedure;
    @BindView(R.id.tv_in_patient_procedure)
    TextView tv_in_patient_procedure;
    @BindView(R.id.tv_in_patient_procedure_add)
    TextView tv_in_patient_procedure_add;
    @BindView(R.id.pb_in_patient_procedure)
    ProgressBar pb_in_patient_procedure;

    @BindView(R.id.rv_doctors)
    RecyclerView rv_doctors;
    @BindView(R.id.tv_doctor)
    TextView tv_doctor;
    @BindView(R.id.tv_doctor_add)
    TextView tv_doctor_add;

    @BindView(R.id.tv_other_services)
    TextView tv_other_services;
    @BindView(R.id.rv_other_services)
    RecyclerView rv_other_services;
    @BindView(R.id.pb_other_services)
    ProgressBar pb_other_services;
    @BindView(R.id.rl_other_services)
    RelativeLayout rl_other_services;


    @BindView(R.id.tv_other_diag)
    TextView tv_other_diag;
    @BindView(R.id.tv_other_diag_add)
    TextView tv_other_diag_add;
    @BindView(R.id.rv_other_diag)
    RecyclerView rv_other_diag;

    @BindView(R.id.rv_attachments)
    RecyclerView rv_attachments;
    @BindView(R.id.tv_attachments)
    TextView tv_attachments;

    @BindView(R.id.tv_dischargeDate)
    TextView tv_dischargeDate;
    @BindView(R.id.tv_dischargeTime)
    TextView tv_dischargeTime;
    @BindView(R.id.tv_attachments_add)
    TextView tv_attachments_add;

    @BindView(R.id.tv_in_other_services_add)
    TextView tv_in_other_services_add;

    @BindView(R.id.btn_cancel)
    Button btn_cancel;
    @BindView(R.id.btn_proceed)
    Button btn_proceed;

    @BindView(R.id.et_problem)
    EditText et_problem;

    @BindView(R.id.cb_informed)
    CheckBox cb_informed;

    @BindView(R.id.btn_clearDischargeDate)
    Button btn_clearDischargeDate;

    public InPatientAdmitted() {

    }

    public static InPatientAdmitted getData(String[] data,
                                            DoctorModelInsertion inPatientAdmittedDoctor,
                                            Rooms roomPlan,
                                            String requestCode,
                                            String roomNo,
                                            String category,
                                            DiagnosisList inPatientAdmittedDiagnosis,
                                            String admittedOn) {
        InPatientAdmitted fragment = new InPatientAdmitted();
        Bundle args = new Bundle();

        args.putStringArray(in_patient_body, data);
        args.putParcelable(in_patient_doctor, inPatientAdmittedDoctor);
        args.putParcelable(in_patient_room_plan, roomPlan);
        args.putString(in_patient_request_code, requestCode);
        args.putString(in_patient_room_no, roomNo);
        args.putString(in_patient_category, category);
        args.putParcelable(in_patient_diagnosis, inPatientAdmittedDiagnosis);
        args.putString(in_patient_admitted_on, admittedOn);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            datain_Patient = getArguments().getStringArray(in_patient_body);
            preSelectedDoctor = getArguments().getParcelable(in_patient_doctor);
            primaryDoc = preSelectedDoctor.getDoctorCode();
            preSelectedRoomPlan = getArguments().getParcelable(in_patient_room_plan);
            requestCode = getArguments().getString(in_patient_request_code);
            roomNo = getArguments().getString(in_patient_room_no);
            roomCategory = getArguments().getString(in_patient_category);
            primaryDiagnosis = getArguments().getParcelable(in_patient_diagnosis);
            pimaryDiag = primaryDiagnosis.getDiagCode();
            admittedOn = getArguments().getString(in_patient_admitted_on);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_in_patient_admitted, container, false);
        ButterKnife.bind(this, view);

        context = getActivity();
        callback = this;
        filterProcedureCallback = this;
        otherServicesCallBack = this;
        dataBaseHandler = new DataBaseHandler(context);
        implement = new InPatientImplement(context, callback);
        procedureAdapter = new ProceduresAdapterInPatient(callback, context, arrayProcedure);
        doctorMultipleToSendAdapter = new DoctorMultipleToSendAdapter(callback, context, arrayDoctor, dataBaseHandler);
        otherDiagnosis = new DiagnosisToBeSendAdapter(callback, dataBaseHandler, context, arrayOtherDiagnosis, tv_doctor);
        attachmentsAdapter = new AttachmentsAdapter(callback, context, arrayAttachments);
        otherServicesAdapterToSendadapter = new OtherServicesAdapterToSend(dataBaseHandler, context, arrayOtherServices, otherServicesCallBack);

        rv_in_patient_procedure.setLayoutManager(new LinearLayoutManager(context));
        rv_in_patient_procedure.setAdapter(procedureAdapter);
        rv_in_patient_procedure.setNestedScrollingEnabled(false);

        rv_doctors.setLayoutManager(new LinearLayoutManager(context));
        rv_doctors.setAdapter(doctorMultipleToSendAdapter);
        rv_doctors.setNestedScrollingEnabled(false);

        rv_other_diag.setLayoutManager(new LinearLayoutManager(context));
        rv_other_diag.setAdapter(otherDiagnosis);
        rv_other_diag.setNestedScrollingEnabled(false);

        rv_attachments.setLayoutManager(new LinearLayoutManager(context));
        rv_attachments.setAdapter(attachmentsAdapter);
        rv_attachments.setNestedScrollingEnabled(false);

        rv_other_services.setLayoutManager(new LinearLayoutManager(context));
        rv_other_services.setAdapter(otherServicesAdapterToSendadapter);
        rv_other_services.setNestedScrollingEnabled(false);


        dataBaseHandler.dropTableOtherServices();

        implement.setLoadingPrimaryGone(tv_other_services, pb_other_services, false);
        implement.getOtherServices(callback);
        Log.d("admittedOn", admittedOn);
        admittedTime = DateConverter.convertDate(new SimpleDateFormat("HH:mm"), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"), admittedOn);
        admittedDate = DateConverter.convertDate(new SimpleDateFormat("MMM dd, yyyy"), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"), admittedOn);


        dataBaseHandler.setALLDoctorsToFalse();
        dataBaseHandler.setALLProcedureToFalse();
        dataBaseHandler.setALLDiagnosisToFalse();
        try {
            dataBaseHandler.setInPatientMainDoctorDeSelected(preSelectedDoctor.getDoctorCode());
        } catch (Exception e) {

        }


        try {
            implement.loadProcedureBasedonDiagnosis(filterProcedureCallback,
                    Constant.ADAPTER_PROC_IN_PATIENT, datain_Patient, pb_in_patient_procedure,
                    tv_in_patient_procedure);

        } catch (Exception e) {

        }


        return view;
    }


    @OnClick({R.id.rl_in_patient_procedure, R.id.tv_other_diag, R.id.tv_attachments,
            R.id.tv_dischargeDate, R.id.tv_dischargeTime, R.id.tv_in_patient_procedure_add, R.id.tv_doctor_add,
            R.id.tv_doctor, R.id.tv_other_diag_add, R.id.btn_cancel, R.id.tv_attachments_add,
            R.id.tv_in_patient_procedure, R.id.rl_other_services, R.id.tv_in_other_services_add,
            R.id.btn_clearDischargeDate,
            R.id.btn_proceed, R.id.cb_informed})
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_proceed:


                    if (cb_informed.isChecked()) {
                        if (implement.getProcedureCodes(arrayProcedure) == null) {
                            alertDialogCustom.showMe(context, "", alertDialogCustom.fill_all_fields, 1);
                        }
                        String dateAdmitted = "";
                        String disChargeDate = "";

                        try {
                            Log.d("admittedTime", admittedTime);
                            Log.d("admittedDate", admittedDate);
                            Log.d("admittedDate", DateConverter.convertDatetoyyyy_MM_dd(
                                    admittedDate)
                                    + " " + admittedTime);
                            dateAdmitted = DateConverter.setDateInPatient(DateConverter.convertDatetoyyyy_MM_dd(
                                    admittedDate)
                                    + " " + admittedTime);

                        } catch (Exception e) {
                            dateAdmitted = "";
                        }


                        try {
                            Log.d("disChargeTime", disChargeTime);
                            Log.d("tv_dischargeDate", tv_dischargeDate.getText().toString().trim());
                            Log.d("disCharge", DateConverter.convertDatetoyyyy_MM_dd(
                                    tv_dischargeDate.getText().toString().trim())
                                    + " " + disChargeTime);
                            disChargeDate = DateConverter.setDateInPatient(DateConverter.convertDatetoyyyy_MM_dd(
                                    tv_dischargeDate.getText().toString().trim())
                                    + " " + disChargeTime);

                        } catch (Exception e) {
                            disChargeDate = "";
                        }



                        InPatientUpdate ipu = InPatientAdmittedAPICall.collectDataInPatient(
                                roomCategory,
                                disChargeDate,
                                SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, context),
                                implement.getDiagnosisData(arrayOtherDiagnosis, primaryDiagnosis),
                                pimaryDiag,
                                dateAdmitted,
                                implement.getDoctorCodes(arrayDoctor, preSelectedDoctor),
                                primaryDoc,
                                SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context),
                                requestCode,
                                SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, context),
                                implement.getProcedureCodes(arrayProcedure),
                                et_problem.getText().toString().trim(),
                                roomNo,
                                SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context),
                                implement.getServiceCodes(arrayOtherServices),
                                SharedPref.getStringValue(SharedPref.USER, SharedPref.SEARCHTYPE, context),
                                "roomType dont exist"
                        );

                        try {
                            InPatientAdmittedAPICall.sendInPatientUpdate(ipu, callback);
                        } catch (Exception e) {

                        }
                        Intent intent = new Intent(context, MemberSearchActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }

                //if unchecked checkbox
                btn_clearDischargeDate.setEnabled(true);


                break;

            case R.id.btn_clearDischargeDate: {
                //Setting the discharge date time to false
                tv_dischargeDate.setText("");
                tv_dischargeTime.setText("");
                btn_proceed.setText("Update");
                break;
            }


            case R.id.tv_in_other_services_add:
                if (isOtherTestReady) {
                    EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_PROC_OTHER_SERVICES));
                }

                break;
            case R.id.rl_other_services:

                if (isOtherTestReady) {
                    EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_PROC_OTHER_SERVICES));
                }


                break;
            case R.id.tv_in_patient_procedure:
//                if (isProcedureFetchError) {
//
//                    if (OriginForUpdatingErrorFetch.equals(Constant.ADAPTER_PROC_IN_PATIENT)) {
//                        implement.loadProcedureBasedonDiagnosis(filterProcedureCallback, Constant.ADAPTER_PROC_IN_PATIENT, datain_Patient, pb_in_patient_procedure, tv_in_patient_procedure);
//                    } else if (OriginForUpdatingErrorFetch.equals(Constant.FROM_ADMITTED_IN_PATIENT_CHANGE)) {
//                        implement.setProceduresAvailability(0, rl_in_patient_procedure, rv_in_patient_procedure, tv_other_diag_add);
//                        pb_in_patient_procedure.setVisibility(View.VISIBLE);
//                        tv_in_patient_procedure.setText(context.getString(R.string.loading_proc));
//                        FilteredProcedureCall.getProcedureFilter(filterProcedureCallback, datain_Patient[0], Constant.FROM_ADMITTED_IN_PATIENT_CHANGE);
//                    } else if (OriginForUpdatingErrorFetch.equals(Constant.FROM_ADMITTED_IN_PATIENT)) {
//                        pb_in_patient_procedure.setVisibility(View.VISIBLE);
//                        tv_in_patient_procedure.setText(context.getString(R.string.loading_proc));
//                        FilteredProcedureCall.getProcedureFilter(filterProcedureCallback, datain_Patient[0], Constant.FROM_ADMITTED_IN_PATIENT);
//                    }
//
//
//                }
                break;
            case R.id.tv_in_patient_procedure_add:

                SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_ORIGIN, Constant.ADAPTER_PROC_IN_PATIENT, context);
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_PROC_IN_PATIENT
                        , implement.getProcedures(proceduresList, arrayProcedureFetched)));


                break;

            case R.id.rl_in_patient_procedure:


                if (isProceduresReady) {
                    SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_ORIGIN, Constant.ADAPTER_PROC_IN_PATIENT, context);
                    EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_PROC_IN_PATIENT
                            , implement.getProcedures(proceduresList, arrayProcedureFetched)));
                }


                break;


            case R.id.tv_other_diag:
                SharedPref.setStringValue(SharedPref.USER, SharedPref.USER, Constant.DIAGNOSIS_OTHER_IN_PATIENT, context);
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.DIAGNOSIS_OTHER_IN_PATIENT));
                break;

            case R.id.tv_other_diag_add:
                SharedPref.setStringValue(SharedPref.USER, SharedPref.USER, Constant.DIAGNOSIS_OTHER_IN_PATIENT, context);
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.DIAGNOSIS_OTHER_IN_PATIENT));
                break;

            case R.id.tv_attachments:
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
                break;
            case R.id.tv_attachments_add:
                Intent getIntent = new Intent();
                getIntent.setType("image/*");
                getIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(getIntent, "Select Picture"), SELECT_PICTURE);
                break;

            case R.id.tv_dischargeDate:

                DatepickerSet.getDate(context, tv_dischargeDate, Constant.FROM_ADMITTED_IN_PATIENT);
                break;

            case R.id.tv_dischargeTime:

                TimepickerSet.getTime(context, tv_dischargeTime, Constant.FROM_AVIALED);
                btn_proceed.setText("Discharge");
                break;

            case R.id.tv_doctor_add:

                SharedPref.setStringValue(SharedPref.USER, SharedPref.USER, Constant.ADAPTER_DOCTOR_IN_PATIENT, context);
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_DOCTOR_IN_PATIENT));

            case R.id.tv_doctor:

                SharedPref.setStringValue(SharedPref.USER, SharedPref.USER, Constant.ADAPTER_DOCTOR_IN_PATIENT, context);
                EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_DOCTOR_IN_PATIENT));

                break;

            case R.id.btn_cancel:
                getActivity().finish();
                break;
            case R.id.cb_informed:
                implement.updateButton(btn_proceed, cb_informed.isChecked());
                break;


        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        switch (requestCode) {

            case SELECT_PICTURE:

                if (resultCode == Activity.RESULT_OK) {
                    Uri imageUri = data.getData();
                    Attachments attachments = new Attachments();
                    attachments.setData(imageUri);
                    attachments.setFileName(GetImageFileName.getFilename(data, context));
                    arrayAttachments.add(attachments);
                    attachmentsAdapter.notifyDataSetChanged();
                    Log.d("FILE_IMAGE", GetImageFileName.getFilename(data, context));
                    Log.d("FILE_IMAGE_URI", imageUri.toString());
                    implement.updateRecyclerLayout(tv_attachments, rv_attachments, arrayAttachments.size(), tv_attachments_add);

                } else {
                    Log.d("FILE_IMAGE_URI", resultCode + "");

                }

                break;

        }


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);

    }

    @Override
    public void onStop() {
        super.onStop();
        dataBaseHandler.setALLDoctorsToFalse();
        dataBaseHandler.setALLProcedureToFalse();
        dataBaseHandler.setALLDiagnosisToFalse();
        dataBaseHandler.setInPatientMainDoctorDeSelected(preSelectedDoctor.getDoctorCode());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dataBaseHandler.setALLDoctorsToFalse();
        dataBaseHandler.setALLProcedureToFalse();
        dataBaseHandler.setALLDiagnosisToFalse();
        dataBaseHandler.setInPatientMainDoctorDeSelected(preSelectedDoctor.getDoctorCode());
    }

    @Override
    public void procedurePrimaryCollectedError(String s, String ORIGIN) {
        counterProcError++;
        //FAKE DATA TO TAKE UP SPACE
        ProcedureFilter procFaker = new ProcedureFilter();
        procFaker.setResponseCode(Constant.NONE);
        procDiagnosisFetched.add(procFaker);

        //if all DIAG IS ERROR FETCHED
        if (counterProcError == datain_Patient.length) {
            isProcedureFetchError = true;
            implement.setLoadingPrimaryGone(tv_in_patient_procedure, pb_in_patient_procedure, false);
        } else if (counterProcError + counterProcSuccess == datain_Patient.length) {
            implement.setLoadingPrimaryGone(tv_in_patient_procedure, pb_in_patient_procedure, true);
            updateProcedureData();
            isProcedureFetchError = false;
            isProceduresReady = true;
        }

        if (ORIGIN.equals(Constant.FROM_ADMITTED_IN_PATIENT)) {
            implement.setLoadingPrimaryGone(tv_in_patient_procedure, pb_in_patient_procedure, false);
        }
    }


    @Override
    public void procedurePrimaryCollectedSuccess(String diagCode, ProcedureFilter body, String ORIGIN) {
        isProcedureFetchError = false;
        OriginForUpdatingErrorFetch = ORIGIN;
        if (ORIGIN.equals(Constant.FROM_ADMITTED_IN_PATIENT_CHANGE)) {

            procDiagnosisFetched.add(body);


            if (counterProcSuccess == datain_Patient.length) {
                implement.setLoadingPrimaryGone(tv_in_patient_procedure, pb_in_patient_procedure, true);
                updateProcedureData();
                isProceduresReady = true;
            } else if (counterProcError + counterProcSuccess == datain_Patient.length) {
                implement.setLoadingPrimaryGone(tv_in_patient_procedure, pb_in_patient_procedure, true);
                updateProcedureData();
                isProceduresReady = true;
            }

            implement.filterOtherDiagnosis(dataBaseHandler, arrayProcedure, implement.getProcedures(proceduresList, arrayProcedureFetched));

        } else if (ORIGIN.equals(Constant.FROM_ADMITTED_IN_PATIENT)) {
            implement.setLoadingPrimaryGone(tv_in_patient_procedure, pb_in_patient_procedure, true);
            isProceduresReady = true;
            implement.updateOtherDiagnosisProc(body, arrayProcedureFetched, diagCode);

            tv_in_patient_procedure.setText(context.getString(R.string.add_procedure));
            pb_in_patient_procedure.setVisibility(View.GONE);

            implement.setProceduresAvailability(arrayProcedure.size(), rl_in_patient_procedure, rv_in_patient_procedure, tv_in_patient_procedure_add);

        } else if (ORIGIN.equals(Constant.ADAPTER_PROC_IN_PATIENT)) {
            Log.d("ADAPTER_PROC_IN_PATIENT", "ADAPTER_PROC_IN_PATIENT");
            counterProcSuccess++;
            procDiagnosisFetched.clear();
            procDiagnosisFetched.add(body);


            if (counterProcSuccess == datain_Patient.length) {
                implement.setLoadingPrimaryGone(tv_in_patient_procedure, pb_in_patient_procedure, true);
                updateProcedureData();
                isProceduresReady = true;
            } else if (counterProcError + counterProcSuccess == datain_Patient.length) {
                implement.setLoadingPrimaryGone(tv_in_patient_procedure, pb_in_patient_procedure, true);
                updateProcedureData();
                isProceduresReady = true;
            }
        }


    }


    private void updateProcedureData() {
        proceduresList = implement.getDataProcedures(procDiagnosisFetched);
        procedureAdapter.notifyDataSetChanged();
    }

    @Override
    public void setProceduresData(ArrayList<TestsAndProcedures> arrayList) {

        arrayProcedure.clear();
        arrayProcedure.addAll(arrayList);
        Log.d("SIZE_PROC", arrayProcedure.size() + "");

        procedureAdapter.notifyDataSetChanged();
        implement.setProceduresAvailability(arrayProcedure.size(), rl_in_patient_procedure, rv_in_patient_procedure, tv_in_patient_procedure_add);

    }


    @Override
    public void removeSingleProcedure(int adapterPosition) {
        dataBaseHandler.setDataProcInPatienttoFalse(arrayProcedure.get(adapterPosition).getProcedureCode());
        arrayProcedure.remove(adapterPosition);
        implement.setProceduresAvailability(arrayProcedure.size(), rl_in_patient_procedure, rv_in_patient_procedure, tv_in_patient_procedure_add);
        procedureAdapter.notifyDataSetChanged();


    }

    @Override
    public void setDoctorsList(ArrayList<DoctorModelMultipleSelection> doctorModelMultipleSelection) {
        arrayDoctor.clear();
        arrayDoctor.addAll(doctorModelMultipleSelection);
        implement.updateRecyclerLayout(tv_doctor, rv_doctors, arrayDoctor.size(), tv_doctor_add);
        doctorMultipleToSendAdapter.notifyDataSetChanged();
    }

    @Override
    public void removeDoctorRow(int position) {
        dataBaseHandler.setDataDoctorToFalse(arrayDoctor.get(position).getDoctorCode());
        arrayDoctor.remove(position);
        implement.updateRecyclerLayout(tv_doctor, rv_doctors, arrayDoctor.size(), tv_doctor_add);
        doctorMultipleToSendAdapter.notifyDataSetChanged();
    }

    @Override
    public void removeDiagnosisRow(int position) {

        implement.removeDiagnosisFromListToSend(dataBaseHandler, arrayOtherDiagnosis.get(position), arrayProcedureFetched, arrayProcedure);
        Log.d("SAME_size", arrayProcedureFetched.size() + "");
        implement.filterOtherDiagnosis(dataBaseHandler, arrayProcedure, implement.getProcedures(proceduresList, arrayProcedureFetched));

        dataBaseHandler.setDataDiagnoseOtherToFALSE(arrayOtherDiagnosis.get(position).getDiagCode());
        implement.removeDiagnosisFromList(arrayOtherDiagnosis, arrayOtherDiagnosis.get(position));
        implement.setDiagnosisAvailability(arrayOtherDiagnosis.size(), tv_other_diag, rv_other_diag);
        otherDiagnosis.notifyDataSetChanged();
        implement.updateRecyclerLayout(tv_other_diag, rv_other_diag, arrayProcedureFetched.size(), tv_other_diag_add);

        procedureAdapter.notifyDataSetChanged();


    }


    @Override
    public void getTime(String date_time) {
        Log.d("DATE_TIME_time", date_time);
        disChargeTime = date_time;
    }

    @Override
    public void getDate(String date_time) {
        Log.d("DATE_TIME_date", date_time);
        disChargeDate = date_time;
    }

    @Override
    public void setAddOtherDiagnosis(DiagnosisList diagnosisList) {
        arrayProcedureFetched.add(implement.getDiagnosisOnly(diagnosisList));
        Log.d("SAME_size", arrayProcedureFetched.size() + "");
        arrayOtherDiagnosis.add(diagnosisList);
        implement.setDiagnosisAvailability(arrayOtherDiagnosis.size(), tv_other_diag, rv_other_diag);
        otherDiagnosis.notifyDataSetChanged();
        pb_in_patient_procedure.setVisibility(View.VISIBLE);
        tv_in_patient_procedure.setText(context.getString(R.string.loading_proc));
        isProceduresReady = false;
        isProcedureFetchError = false;
        pb_in_patient_procedure.setVisibility(View.VISIBLE);
        tv_in_patient_procedure.setText(context.getString(R.string.loading_proc));
        datain_Patient[0] = diagnosisList.getDiagCode();
        implement.updateRecyclerLayout(tv_other_diag, rv_other_diag, arrayProcedureFetched.size(), tv_other_diag_add);
        FilteredProcedureCall.getProcedureFilter(filterProcedureCallback, datain_Patient[0], Constant.FROM_ADMITTED_IN_PATIENT);
    }

    @Override
    public void setRemoveOtherDiagnosis(DiagnosisList diagnosisList) {
        implement.filterOtherDiagnosis(dataBaseHandler, arrayProcedure, implement.getProcedures(proceduresList, arrayProcedureFetched));

        implement.removeDiagnosisFromList(arrayOtherDiagnosis, diagnosisList);
        implement.removeDiagnosisFromListToSend(dataBaseHandler, diagnosisList, arrayProcedureFetched, arrayProcedure);
        Log.d("SAME_size", arrayProcedureFetched.size() + "");
        implement.setDiagnosisAvailability(arrayOtherDiagnosis.size(), tv_other_diag, rv_other_diag);
        otherDiagnosis.notifyDataSetChanged();
        implement.updateRecyclerLayout(tv_other_diag, rv_other_diag, arrayProcedureFetched.size(), tv_other_diag_add);


        procedureAdapter.notifyDataSetChanged();
    }

    @Override
    public void setOtherDiagnosisMainChanged(DiagnosisList otherDiagnosis) {
        primaryDiagnosis = otherDiagnosis;
        implement.setDiagnosisAvailability(arrayOtherDiagnosis.size(), tv_other_diag, rv_other_diag);
        pb_in_patient_procedure.setVisibility(View.VISIBLE);
        tv_in_patient_procedure.setText(context.getString(R.string.loading_proc));
        isProceduresReady = false;
        isProcedureFetchError = false;
        datain_Patient[0] = otherDiagnosis.getDiagCode();
        implement.setProceduresAvailability(0, rl_in_patient_procedure, rv_in_patient_procedure, tv_other_diag_add);
        FilteredProcedureCall.getProcedureFilter(filterProcedureCallback, datain_Patient[0], Constant.FROM_ADMITTED_IN_PATIENT_CHANGE);

    }

    @Override
    public void setChangedDoctorMain(DoctorModelInsertion doctorData) {

        preSelectedDoctor = doctorData;
        implement.removeDoctorAlreadSelectedFromMainPanel(dataBaseHandler, arrayDoctor, preSelectedDoctor);
        doctorMultipleToSendAdapter.notifyDataSetChanged();
        implement.updateRecyclerLayout(tv_doctor, rv_doctors, arrayDoctor.size(), tv_doctor_add);
    }

    @Override
    public void removeAttachmentRow(int position) {
        arrayAttachments.remove(position);
        attachmentsAdapter.notifyDataSetChanged();
        implement.updateRecyclerLayout(tv_attachments, rv_attachments, arrayAttachments.size(), tv_attachments_add);

    }

    @Override
    public void onSuccessOtherTest(ArrayList<Services> arrayServices) {
        implement.updateOtherServices(callback, dataBaseHandler, arrayServices);
    }



    @Override
    public void onErrorOtherTest(String s) {
        isOtherTestReady = false;
        implement.setLoadingPrimaryGone(tv_other_services, pb_other_services, false);
        implement.updateRecyclerLayout(tv_in_other_services_add, rl_other_services, rv_other_services, 0);

    }

    @Override
    public void onDoneUpdatingOtherServices() {
        Log.d("data", "done");
        isOtherTestReady = true;
        implement.setLoadingPrimaryGone(tv_other_services, pb_other_services, true);

    }

    @Override
    public void setOtherServicesData(ArrayList<Services> services) {


        arrayOtherServices.clear();
        arrayOtherServices.addAll(services);
        otherServicesAdapterToSendadapter.notifyDataSetChanged();
        implement.updateRecyclerLayout(tv_in_other_services_add, rl_other_services, rv_other_services, arrayOtherServices.size());
    }

    @Override
    public void onSuccessInpatientUpdate(InPatientUpdate inPatientUpdate) {

    }

    @Override
    public void onErrorInPatientUpdate() {
        alertDialogCustom.showMe(context, "", alertDialogCustom.fill_all_fields, 1);
    }

    @Override
    public void setAdmittedTime(String date_time) {
        Log.d("TIME_DIS", date_time);
        admittedTime = date_time;
    }

    @Override
    public void setAdmittedDate(String date_time) {
        Log.d("DATE_DIS", date_time);
        admittedDate = date_time;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        implement.setEventTrigger(event);
    }

    @Override
    public void otherServicesOnClick(int adapterPosition) {
        dataBaseHandler.setDataOtherServicesToFalse(arrayOtherServices.get(adapterPosition).getId());
        arrayOtherServices.remove(adapterPosition);
        otherServicesAdapterToSendadapter.notifyDataSetChanged();
        implement.updateRecyclerLayout(tv_in_other_services_add, rl_other_services, rv_other_services, arrayOtherServices.size());
    }


    public static class MessageEvent {


        private String data;
        private String date_time;
        private ArrayList<DoctorModelMultipleSelection> arrayDoctor;
        private ArrayList<TestsAndProcedures> arrayList;
        private ArrayList<DiagnosisList> otherDiagnosis;
        private ArrayList<Services> services;
        private DiagnosisList diagnosisList;
        private DoctorModelInsertion doctorData;

        public MessageEvent(String data, DoctorModelInsertion doctorModelInsertion) {
            this.data = data;
            doctorData = doctorModelInsertion;
        }

        public MessageEvent(String data, ArrayList<DiagnosisList> otherDiagnosis, String buggy) {
            this.data = data;
            this.otherDiagnosis = otherDiagnosis;

        }

        public MessageEvent(String data, String date_time) {
            this.data = data;
            this.date_time = date_time;

        }

        public MessageEvent(String data, DiagnosisList diagnosisList) {
            this.data = data;
            this.diagnosisList = diagnosisList;
        }

        public MessageEvent(String data, ArrayList<Services> services, String faker, String faker2) {
            this.services = services;
            this.data = data;
        }

        public DoctorModelInsertion getDoctorData() {
            return doctorData;
        }

        public ArrayList<Services> getServices() {
            return services;
        }

        public void setServices(ArrayList<Services> services) {
            this.services = services;
        }

        public void setDoctorData(DoctorModelInsertion doctorData) {
            this.doctorData = doctorData;
        }

        public DiagnosisList getDiagnosis() {
            return diagnosisList;
        }

        public void setDiagnosis(DiagnosisList diagnosisList) {
            this.diagnosisList = diagnosisList;
        }

        public ArrayList<DiagnosisList> getOtherDiagnosis() {
            return otherDiagnosis;
        }

        public void setOtherDiagnosis(ArrayList<DiagnosisList> otherDiagnosis) {
            this.otherDiagnosis = otherDiagnosis;
        }

        public ArrayList<TestsAndProcedures> getArrayList() {
            return arrayList;
        }

        public DoctorModelMultipleSelection getDoctorModelMultipleSelection() {
            return doctorModelMultipleSelection;
        }


        ArrayList<DoctorModelMultipleSelection> getArrayDoctor() {
            return arrayDoctor;
        }

        public void setArrayDoctor(ArrayList<DoctorModelMultipleSelection> arrayDoctor) {
            this.arrayDoctor = arrayDoctor;
        }

        public void setDoctorModelMultipleSelection(DoctorModelMultipleSelection doctorModelMultipleSelection) {
            this.doctorModelMultipleSelection = doctorModelMultipleSelection;
        }

        private DoctorModelMultipleSelection doctorModelMultipleSelection;

        public void setArrayList(ArrayList<TestsAndProcedures> arrayList) {
            this.arrayList = arrayList;
        }

        public String getDate_time() {
            return date_time;
        }

        public void setDate_time(String date_time) {
            this.date_time = date_time;
        }

        public MessageEvent(String data) {
            this.data = data;
        }

        public MessageEvent(ArrayList<TestsAndProcedures> arrayList, String data) {
            this.data = data;
            this.arrayList = arrayList;
        }

        public MessageEvent(String data, ArrayList<DoctorModelMultipleSelection> arrayDoctor) {
            this.data = data;
            this.arrayDoctor = arrayDoctor;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }
    }

}
