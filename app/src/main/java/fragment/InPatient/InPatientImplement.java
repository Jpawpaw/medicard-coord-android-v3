package fragment.InPatient;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import coord.medicard.com.medicardcoordinator.R;
import model.DiagnosisList;
import model.DoctorModelInsertion;
import model.DoctorModelMultipleSelection;
import model.OtherServices;
import model.ProcedureAndTestCodes;
import model.ProcedureFetchedData;
import model.ProcedureFilter;
import model.Services;
import model.TestsAndProcedures;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import services.AppInterface;
import services.AppService;
import utilities.Constant;
import utilities.DataBaseHandler;
import utilities.APIMultipleCalls.FilteredProcedureCall;

/**
 * Created by mpx-pawpaw on 6/1/17.
 */

public class InPatientImplement {
    Context context;
    InPatientCallback callback;


    public InPatientImplement(Context context, InPatientCallback callback) {
        this.context = context;
        this.callback = callback;
    }


    public void loadProcedureBasedonDiagnosis(FilteredProcedureCall.FilterProcedureCallback callback, String ORIGIN, String[] diagCode, ProgressBar pb_primary_diag, TextView tv_primary_diag) {


        pb_primary_diag.setVisibility(View.VISIBLE);
        tv_primary_diag.setText(context.getString(R.string.loading_proc));


        for (int x = 0; x < diagCode.length; x++) {

             FilteredProcedureCall.getProcedureFilter(callback, diagCode[x], ORIGIN);

        }

    }

    public void setLoadingPrimaryGone(TextView tv_primary_diag, ProgressBar pb_primary_diag, boolean b) {


        if (b) {
            tv_primary_diag.setText(context.getString(R.string.add_procedure));
            pb_primary_diag.setVisibility(View.GONE);
        } else {
            tv_primary_diag.setText(context.getString(R.string.there_is_a_problem_retrieving));
            pb_primary_diag.setVisibility(View.GONE);
        }

    }

    public void setEventTrigger(InPatientAdmitted.MessageEvent event) {


        if (event.getData().equals(Constant.IN_PATIENT_PROC)) {
            callback.setProceduresData(event.getArrayList());
        } else if (event.getData().equals(Constant.ADAPTER_DOCTORS_IN_PATIENT)) {
            callback.setDoctorsList(event.getArrayDoctor());
        } else if (event.getData().equals(Constant.DIAGNOSIS_OTHER_IN_PATIENT_CHANGE)) {
            callback.setOtherDiagnosisMainChanged(event.getDiagnosis());
        } else if (event.getData().equals(Constant.GETTIME)) {
            callback.getTime(event.getDate_time());
        } else if (event.getData().equals(Constant.GETDATE)) {
            callback.getDate(event.getDate_time());
        } else if (event.getData().equals(Constant.DIAGNOSIS_OTHER_IN_PATIENT_ADD)) {
            callback.setAddOtherDiagnosis(event.getDiagnosis());
        } else if (event.getData().equals(Constant.DIAGNOSIS_OTHER_IN_PATIENT_REMOVE)) {
            callback.setRemoveOtherDiagnosis(event.getDiagnosis());
        } else if (event.getData().equals(Constant.ADAPTER_DOCTORS_IN_PATIENT_MAIN)) {
            callback.setChangedDoctorMain(event.getDoctorData());
        } else if (event.getData().equals(Constant.ADAPTER_PROC_OTHER_SERVICES)) {
            callback.setOtherServicesData(event.getServices());
        } else if (event.getData().equals(Constant.TIME_ADMITTED)) {
            callback.setAdmittedTime(event.getDate_time());
        } else if (event.getData().equals(Constant.DATE_ADMITTED)) {
            callback.setAdmittedDate(event.getDate_time());
        }


    }

    public void setProceduresAvailability(int size, RelativeLayout rl_in_patient_procedure, RecyclerView rv_in_patient_procedure, TextView tv_in_patient_procedure_add) {


        if (size == 0) {
            rl_in_patient_procedure.setVisibility(View.VISIBLE);
            rv_in_patient_procedure.setVisibility(View.GONE);
            tv_in_patient_procedure_add.setVisibility(View.GONE);
        } else {
            rl_in_patient_procedure.setVisibility(View.GONE);
            rv_in_patient_procedure.setVisibility(View.VISIBLE);
            tv_in_patient_procedure_add.setVisibility(View.VISIBLE);
        }


    }

    public void updateRecyclerLayout(TextView tv_doctor, RecyclerView rv_doctors, int arrayDoctor, TextView tv_doctor_add) {

        if (arrayDoctor == 0) {
            tv_doctor.setVisibility(View.VISIBLE);
            rv_doctors.setVisibility(View.GONE);
            tv_doctor_add.setVisibility(View.GONE);

        } else {
            tv_doctor.setVisibility(View.GONE);
            rv_doctors.setVisibility(View.VISIBLE);
            tv_doctor_add.setVisibility(View.VISIBLE);
        }


    }

    public void setDiagnosisAvailability(int size, TextView tv_other_diag, RecyclerView rv_other_diag) {

        if (size <= 0) {
            tv_other_diag.setVisibility(View.VISIBLE);
            rv_other_diag.setVisibility(View.GONE);
        } else {
            tv_other_diag.setVisibility(View.GONE);
            rv_other_diag.setVisibility(View.VISIBLE);
        }
    }



    public String[] getDataProcedures(ArrayList<ProcedureFilter> procDiagnosisFetched) {
        String[] data = null;


        for (ProcedureFilter proc : procDiagnosisFetched) {
            if (!proc.getResponseCode().equals(Constant.NONE)) {
                data = proc.getProcedures();
            }
        }


        return data;
    }

    public ProcedureFetchedData getDiagnosisOnly(DiagnosisList arrayDiagnosis) {


        ProcedureFetchedData procedureFetchedData = new ProcedureFetchedData();
        procedureFetchedData.setDiagCode(arrayDiagnosis.getDiagCode());

        return procedureFetchedData;
    }

    public void removeDiagnosisFromList(ArrayList<DiagnosisList> arrayOtherDiagnosis, DiagnosisList diagnosisList) {

        if (arrayOtherDiagnosis.size() != 0) {
            for (int x = 0; x < arrayOtherDiagnosis.size(); x++) {
                if (arrayOtherDiagnosis.get(x).getDiagCode().equals(diagnosisList.getDiagCode())) {
                    arrayOtherDiagnosis.remove(x);
                    break;
                }
            }
        } else {
            arrayOtherDiagnosis.clear();
        }
    }

    public void updateOtherDiagnosisProc(ProcedureFilter body, ArrayList<ProcedureFetchedData> arrayProcedureFetched,
                                         String diagCode) {

        Log.d("DATA_PROC", "ADDED");

        ProcedureFetchedData setData = new ProcedureFetchedData();
        if (arrayProcedureFetched.size() != 0) {
            for (int x = 0; x < arrayProcedureFetched.size(); x++) {

                if (arrayProcedureFetched.get(x).getDiagCode() != null) {


                    try {
                        if (arrayProcedureFetched.get(x).getDiagCode().equals(diagCode)) {
//                            ProcedureFetchedData data = new ProcedureFetchedData();

                            Log.d("DATA_PROC", diagCode);
                            Log.d("DATA_PROC", arrayProcedureFetched.get(x).getDiagCode());
                            Log.d("DATA_PROC", body.getProcedures().length + "");
                            if (body.getProcedures() != null) {
                                arrayProcedureFetched.get(x).setProcedures(body.getProcedures());
                            }


                            setData = arrayProcedureFetched.get(x);
                            break;
                        }
                    } catch (Exception e) {
                        arrayProcedureFetched.remove(x);
                    }

                } else {
                    arrayProcedureFetched.remove(x);
                }

            }
        }

        Log.wtf("WTF_DATA", setData.toString());


    }

    public String[] getProcedures(final String[] proceduresList, final ArrayList<ProcedureFetchedData> arrayProcedureFetched) {
        String[] both = null;
        String[] procCreator = new String[0];


        Log.d("SIZE_OF_DIAG", arrayProcedureFetched.size() + "");
        if (arrayProcedureFetched.size() != 0) {
            for (int x = 0; x < arrayProcedureFetched.size(); x++) {

                List<String> strings = null;
                if (arrayProcedureFetched.get(x).getProcedures() != null) {
                    if (arrayProcedureFetched.get(x).getProcedures().length != 0)
                        strings = Arrays.asList(arrayProcedureFetched.get(x).getProcedures());
                    else
                        strings = Arrays.asList(proceduresList);
                } else {
                    strings = Arrays.asList(proceduresList);
                }

                Log.d("SIZE_OF_DIAG", arrayProcedureFetched.size() + "");
                final List<String> finalStrings = strings;
                final String[] finalProcCreator = procCreator;
                procCreator = new ArrayList<String>() {{
                    addAll(Arrays.asList(finalProcCreator));
                    addAll(finalStrings);
                }}.toArray(new String[0]);
            }


            final String[] finalProcCreator1 = procCreator;
            both = new ArrayList<String>() {{
                addAll(Arrays.asList(proceduresList));
                addAll(Arrays.asList(finalProcCreator1));
            }}.toArray(new String[0]);
        } else {

            both = new ArrayList<String>() {{
                addAll(Arrays.asList(proceduresList));
            }}.toArray(new String[0]);

        }

        for (int x = 0; x < both.length; x++) {
            Log.d("DATA_gETER", both[x]);
        }


        Log.d("BOTH_COUNT", both.length + "");

        return both;
    }

    public void removeDiagnosisFromListToSend(DataBaseHandler dataBaseHandler, DiagnosisList diagnosisList, ArrayList<ProcedureFetchedData> arrayProcedureFetched, ArrayList<TestsAndProcedures> arrayProcedure) {

        if (arrayProcedureFetched.size() != 0) {
            for (int x = 0; x < arrayProcedureFetched.size(); x++) {
                if (arrayProcedureFetched.get(x).getDiagCode() == null) {
                    arrayProcedureFetched.remove(x);
                } else {
                    if (arrayProcedureFetched.get(x).getDiagCode().equals(diagnosisList.getDiagCode())) {

                        // filterDiagnosis(dataBaseHandler, arrayProcedureFetched.get(x), arrayProcedure);
                        arrayProcedureFetched.remove(x);
                    }
                }
            }
        } else {
            arrayProcedureFetched.clear();
        }
    }

//    private void filterDiagnosis(DataBaseHandler dataBaseHandler, ProcedureFetchedData procedureFetchedData, ArrayList<ProceduresListLocal> arrayProcedure) {
//
//        int counterSize = arrayProcedure.size();
//
//        for (int procArray = 0; procArray < counterSize; procArray++) {
//            for (int procToDelete = 0; procToDelete < procedureFetchedData.getProcedures().length; procToDelete++) {
//                if (arrayProcedure.get(procArray) != null) {
//                    //                Log.d("data_same", procedureFetchedData.getProcedures()[procToDelete]);
////                Log.d("data_same", arrayProcedure.get(procArray).getProcedureCode());
//
//                    if (procedureFetchedData.getProcedures()[procToDelete].equals(arrayProcedure.get(procArray).getProcedureCode())) {
//                        dataBaseHandler.setDataProcInPatienttoFalse(arrayProcedure.get(procArray).getId());
//                        arrayProcedure.remove(procArray);
//
//                    }
//                }
//
//
//            }
//        }
//
//
//    }

    public void filterOtherDiagnosis(DataBaseHandler dataBaseHandler, ArrayList<TestsAndProcedures> arrayProcedure, String[] arrayProcedureFetched) {

        int counter = 0;
        Log.d("proc_size_wow", arrayProcedure.size() + "");
        Log.d("proc_size_wow", arrayProcedureFetched.length + "");
        if (arrayProcedure.size() != 0) {

            // for (int x = 0; x < arrayProcedure.size(); x++)
            while (counter < arrayProcedure.size()) {

                Log.d("proc_filter", arrayProcedure.get(counter).getProcedureCode() + "");


                if (!testProceduresListisListed(arrayProcedureFetched, arrayProcedure.get(counter))) {
                    dataBaseHandler.setDataProcInPatienttoFalse(arrayProcedure.get(counter).getProcedureCode());
                    arrayProcedure.remove(counter);
                    //TODO DELETE THIS
                    for (TestsAndProcedures proc : arrayProcedure) {
                        Log.d("REMOVED", proc.getProcedureCode());
                    }
                } else {
                    counter++;
                }


            }

        }

        Log.d("COUNTER", counter + "");

    }

    private boolean testProceduresListisListed(String[] procedureFetchedData,
                                               TestsAndProcedures arrayProcedure) {

        boolean isYES = false;
        int counter = 0;

        Log.d("proc_size_", procedureFetchedData.length + "");
        for (int ctrProcFetch = 0; ctrProcFetch < procedureFetchedData.length; ctrProcFetch++) {
            counter++;

            if (arrayProcedure.getProcedureCode().equals(procedureFetchedData[ctrProcFetch])) {
                Log.d("proc_size_filter", procedureFetchedData[ctrProcFetch] + "");
                Log.d("proc_size_filter", arrayProcedure.getProcedureCode() + "");
                isYES = true;
            } else {
                Log.d("proc_size_filter", procedureFetchedData[ctrProcFetch] + "&"
                        + arrayProcedure.getProcedureCode());

            }


        }


        Log.d("DATA_FILTER_PROC_bool", isYES + " ,PROCEDURE " + arrayProcedure.getProcedureCode());
        Log.d("COUNTER_INNER", counter + "");
        return isYES;
    }

    public void removeDoctorAlreadSelectedFromMainPanel(DataBaseHandler dataBaseHandler, ArrayList<DoctorModelMultipleSelection> arrayDoctor, DoctorModelInsertion preSelectedDoctor) {

        for (int x = 0; x < arrayDoctor.size(); x++) {
            if (arrayDoctor.get(x).getDoctorCode().equals(preSelectedDoctor.getDoctorCode())) {
                dataBaseHandler.setDataDoctorToFalse(arrayDoctor.get(x).getDoctorCode());
                arrayDoctor.remove(x);
                break;
            }
        }

    }

    public void getOtherServices(final InPatientCallback callback) {

        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.getOtherServices()
                .enqueue(new Callback<OtherServices>() {
                    @Override
                    public void onResponse(Call<OtherServices> call, Response<OtherServices> response) {
                        try {

                            callback.onSuccessOtherTest(response.body().getServices());
                        } catch (Exception e) {
                            callback.onErrorOtherTest("");
                        }
                    }

                    @Override
                    public void onFailure(Call<OtherServices> call, Throwable t) {
                        try {
                            callback.onErrorOtherTest(t.getMessage());
                        } catch (Exception e) {
                            callback.onErrorOtherTest("");
                        }
                    }
                });

    }

    public void updateRecyclerLayout(TextView tv_in_other_services_add, RelativeLayout tv_other_services, RecyclerView rv_other_services, int size) {

        if (size == 0) {
            tv_other_services.setVisibility(View.VISIBLE);
            rv_other_services.setVisibility(View.GONE);
            tv_in_other_services_add.setVisibility(View.GONE);
        } else {
            tv_other_services.setVisibility(View.GONE);
            rv_other_services.setVisibility(View.VISIBLE);
            tv_in_other_services_add.setVisibility(View.VISIBLE);
        }

    }

    public void updateOtherServices(final InPatientCallback callback, final DataBaseHandler dataBaseHandler, final ArrayList<Services> arrayServices) {

        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {


                for (Services services : arrayServices) {

                    dataBaseHandler.createOtherServices(services);
                }

                return null;
            }


            @Override
            protected void onPostExecute(Object o) {
                callback.onDoneUpdatingOtherServices();
            }
        };

        asyncTask.execute();
    }

    /**
     * @param arrayOtherDiagnosis
     * @param primaryDiagnosis
     * @return all diagnosis code
     */
    public String[] getDiagnosisData(ArrayList<DiagnosisList> arrayOtherDiagnosis, DiagnosisList primaryDiagnosis) {
        ArrayList<DiagnosisList> dataArray = new ArrayList<>();

        dataArray.addAll(arrayOtherDiagnosis);
        dataArray.add(primaryDiagnosis);
        String[] data = new String[dataArray.size()];


        for (int x = 0; x < dataArray.size(); x++) {
            DiagnosisList diagnosisList = dataArray.get(x);
            data[x] = diagnosisList.getDiagCode();
        }


        return data;
    }

    public String[] getDoctorCodes(ArrayList<DoctorModelMultipleSelection> arrayDoctor, DoctorModelInsertion preSelectedDoctor) {

        ArrayList<DoctorModelMultipleSelection> dataArray = new ArrayList<>();

        dataArray.addAll(arrayDoctor);
        dataArray.add(extractOnlyDoctorCode(preSelectedDoctor));
        String[] data = new String[dataArray.size()];


        for (int x = 0; x < dataArray.size(); x++) {
            DoctorModelMultipleSelection doctorModelMultipleSelection = dataArray.get(x);
            data[x] = doctorModelMultipleSelection.getDoctorCode();
        }

        return data;
    }

    private DoctorModelMultipleSelection extractOnlyDoctorCode(DoctorModelInsertion preSelectedDoctor) {

        DoctorModelMultipleSelection data = new DoctorModelMultipleSelection();
        data.setDoctorCode(preSelectedDoctor.getDoctorCode());

        return data;
    }

    public ArrayList<ProcedureAndTestCodes> getProcedureCodes(ArrayList<TestsAndProcedures> arrayProcedure) {
        ArrayList<ProcedureAndTestCodes> data = new ArrayList<>();

        Log.d("TestsAndProcedures", arrayProcedure.size() + "");
        for (int x = 0; x < arrayProcedure.size(); x++) {
            ProcedureAndTestCodes procedureAndTestCodes = new ProcedureAndTestCodes();
            procedureAndTestCodes.setAmount(arrayProcedure.get(x).getProcedureAmount());
            procedureAndTestCodes.setProcCode(arrayProcedure.get(x).getProcedureCode());
            data.add(procedureAndTestCodes);
        }

        return data;
    }

    public String[] getServiceCodes(ArrayList<Services> arrayOtherServices) {

        int x = 0;
        String[] data = new String[arrayOtherServices.size()];

        for (Services services : arrayOtherServices) {
            data[x] = services.getId();
            x++;
        }

        return data;
    }

    public void updateButton(Button btn_proceed, boolean checked) {


        if (checked) {
            btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        }else{
            btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.gray));

        }
    }
}
