package fragment.InPatient;

import java.util.ArrayList;

import model.DiagnosisList;
import model.DoctorModelInsertion;
import model.DoctorModelMultipleSelection;
import model.InPatientUpdate;
import model.Services;
import model.TestsAndProcedures;

/**
 * Created by mpx-pawpaw on 6/1/17.
 */

public interface InPatientCallback {

     void setProceduresData(ArrayList<TestsAndProcedures> arrayList);


    void removeSingleProcedure(int adapterPosition);

    void setDoctorsList(ArrayList<DoctorModelMultipleSelection> doctorModelMultipleSelection);

    void removeDoctorRow(int position);


    void removeDiagnosisRow(int adapterPosition);

    void getTime(String date_time);

    void getDate(String date_time);


    void setAddOtherDiagnosis(DiagnosisList diagnosisList);

    void setRemoveOtherDiagnosis(DiagnosisList diagnosisList);

    void setOtherDiagnosisMainChanged(DiagnosisList otherDiagnosis);

    void setChangedDoctorMain(DoctorModelInsertion doctorData);

    void removeAttachmentRow(int position);

    void onSuccessOtherTest(ArrayList<Services> body);

    void onErrorOtherTest(String s);

    void onDoneUpdatingOtherServices();

    void setOtherServicesData(ArrayList<Services> services);

    void onSuccessInpatientUpdate(InPatientUpdate inPatientUpdate);

    void onErrorInPatientUpdate();

    void setAdmittedTime(String date_time);

    void setAdmittedDate(String date_time);
}
