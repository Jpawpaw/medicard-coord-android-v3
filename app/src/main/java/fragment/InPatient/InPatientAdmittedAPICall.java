package fragment.InPatient;

import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;

import model.InPatientUpdate;
import model.ProcedureAndTestCodes;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import services.AppInterface;
import services.AppService;

/**
 * Created by mpx-pawpaw on 6/21/17.
 */

public class InPatientAdmittedAPICall {


    public static InPatientUpdate collectDataInPatient(String category,
                                                       String disChargeDate,
                                                       String phoneId,
                                                       String[] diagnosisData,
                                                       String primaryDiagnosis,
                                                       String dateAdmitted,
                                                       String[] doctorCodes,
                                                       String primaryDoctorCode,
                                                       String hospitalCode,
                                                       String maceRequestCode,
                                                       String memberID,
                                                       ArrayList<ProcedureAndTestCodes> procedureCodes,
                                                       String remarks,
                                                       String roomNo,
                                                       String userName,
                                                       String[] serviceCodes,
                                                       String searchType,
                                                       String roomType) {
        InPatientUpdate data = new InPatientUpdate();

        data.setCategory(category);
        data.setDateAdmitted(dateAdmitted);
        data.setDeviceId(phoneId);
        data.setDiagnosisCodes(diagnosisData);
        data.setPrimaryDiag(primaryDiagnosis);
        data.setDischargeDate(disChargeDate);
        data.setDoctorCodes(doctorCodes);
        data.setPrimaryDoctor(primaryDoctorCode);
        data.setHospitalCode(hospitalCode);
        data.setMaceRequestCode(maceRequestCode);
        data.setMemberCode(memberID);
        data.setProcedureAndTestCodes(procedureCodes);
        data.setRemarks(remarks);
        data.setRoomNumber(roomNo);
        data.setRoomPrice("");
        data.setUsername(userName);
        data.setServiceCodes(serviceCodes);
        data.setSearchType(searchType);
        //non existence data.setRoomType(roomType);


        Gson gson = new Gson();
        Log.d("DATA_UPDATE_IN_PATIENT", gson.toJson(data));
        return data;
    }

    public static void sendInPatientUpdate(InPatientUpdate data, final InPatientCallback callback) {

        AppInterface appService;
        appService = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appService.updateInPatient(data)
                .enqueue(new Callback<InPatientUpdate>() {
                    @Override
                    public void onResponse(Call<InPatientUpdate> call, Response<InPatientUpdate> response) {
                        callback.onSuccessInpatientUpdate(response.body());
                    }

                    @Override
                    public void onFailure(Call<InPatientUpdate> call, Throwable t) {
                        callback.onErrorInPatientUpdate();
                    }
                });

    }


}
