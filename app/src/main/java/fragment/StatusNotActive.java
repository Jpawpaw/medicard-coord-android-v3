package fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import coord.medicard.com.medicardcoordinator.R;
import utilities.SharedPref;

public class StatusNotActive extends Fragment {


    String status = " ";
    TextView tv_status;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_status_not_active, container, false);



        SharedPref.getStringValue(SharedPref.USER, SharedPref.STATUSREMARKS, getContext());
        tv_status = (TextView) view.findViewById(R.id.tv_status);
        tv_status.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.STATUSREMARKS, getContext()));
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(StatusNotActive.MessageEvent event) {

        status = event.getMessage();
        tv_status.setText("Disapproved because " + status);
        Log.d("FLAG_BUTTON", status);
    }

    public static class MessageEvent {

        String Message;


        public MessageEvent(String s) {
            this.Message = s;

        }


        public String getMessage() {
            return Message;
        }

        public void setMessage(String message) {
            Message = message;
        }
    }

}
