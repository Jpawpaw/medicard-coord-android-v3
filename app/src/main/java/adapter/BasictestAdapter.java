package adapter;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;
import org.greenrobot.eventbus.EventBus;
import java.text.DecimalFormat;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import coord.medicard.com.medicardcoordinator.Navigator.NavigatorRetrieve;
import coord.medicard.com.medicardcoordinator.R;
import de.hdodenhof.circleimageview.CircleImageView;
import fragment.Avail.AvailRetrieve;
import fragment.Avail.AvailServices;
import fragment.InPatient.InPatientAdmitted;
import model.BasicTests;
import model.TestsAndProcedures;
import services.ItemClickListener;
import utilities.BasicTestSession;
import utilities.Constant;
import utilities.DataBaseHandler;
import utilities.EditBasicTestPriceDialog;
import utilities.EditPriceDialog;
import utilities.OriginTester;
import utilities.SharedPref;

import static android.media.CamcorderProfile.get;


/**
 * Created by IPC_Server on 8/9/2017.
 */

public class BasictestAdapter extends RecyclerView.Adapter<BasictestAdapter.Holder> {

    private Context context;
    DataBaseHandler dataBaseHandler;
    private ItemClickListener clickListener;
    DecimalFormat formatter = new DecimalFormat("#,###.##");
    double amount = 0;
    private AvailRetrieve implement;
    private BasicTestSession basicTestSession;
    AvailServices availServices;
    ArrayList<TestsAndProcedures> basicTestList;

    public BasictestAdapter(Context context,ArrayList<TestsAndProcedures> basicTestList,DataBaseHandler dataBaseHandler) {
        this.context = context;
        this.dataBaseHandler = dataBaseHandler;
        this.basicTestList = basicTestList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.basic_test_row, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        double amount, current_amount;
        DecimalFormat formatter = new DecimalFormat("#,###.##");
        String formatString = "";

        try {
            holder.tv_name.setText(basicTestList.get(position).getProcedureDesc());
            current_amount = Double.parseDouble(String.valueOf(basicTestList.get(position).getProcedureAmount()));
            holder.tv_current_price.setText("P" + " " + formatter.format(current_amount));
        } catch (Exception e) {
            holder.tv_name.setText(basicTestList.get(position).getProcedureDesc());
        }

        holder.checkBox.setOnCheckedChangeListener(null);
        holder.checkBox.setChecked(basicTestList.get(position).isSelected());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    arrayList.get(holder.getAdapterPosition()).setSelected(isChecked);
                    basicTestList.get(position).setSelected(isChecked);
            }
        });
        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.checkBox.isChecked()){
                    holder.checkBox.setChecked(false);
                    basicTestList.get(position).setSelected(false);
                }else {
                    holder.checkBox.setChecked(true);
                    basicTestList.get(position).setSelected(true);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return basicTestList.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_current_price)
        TextView tv_current_price;
        @BindView(R.id.checkBox)
        CheckBox checkBox;
        @BindView(R.id.tv_name)
        TextView tv_name;
        @BindView(R.id.btn_edit_price)
        ImageButton btn_edit_price;
        EditBasicTestPriceDialog editBasicTestPriceDialog = new EditBasicTestPriceDialog();

        public Holder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
                btn_edit_price.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(basicTestList.get(getAdapterPosition()).isSelected()) {
                            editBasicTestPriceDialog.showMe(context, getAdapterPosition(),basicTestList, BasictestAdapter.this,dataBaseHandler);
                        }else {

                        }
                    }
                });
        }
    }
}

