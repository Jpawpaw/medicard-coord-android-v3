package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.R;
import fragment.Avail.AvailInterface;
import fragment.Avail.AvailRetrieve;
import model.ProceduresListLocal;
import model.TestsAndProcedures;
import utilities.Constant;
import utilities.DataBaseHandler;
import utilities.EditPriceDialog;

/**
 * Created by mpx-pawpaw on 11/22/16.
 */

public class ProceduresToBeSendAdapter extends RecyclerView.Adapter<ProceduresToBeSendAdapter.Holder> {

    private Context context;
    private ArrayList<TestsAndProcedures> arrayList = new ArrayList<>();
    private RecyclerView rv_diagnosis;
    private RelativeLayout tv_noProcAvailable;
    private TextView tv_price;
    private AvailRetrieve implement;
    private String ORIGIN;
    private AvailInterface callback;
    private TextView addMore ;
    private DataBaseHandler dataBaseHandler;

    public ProceduresToBeSendAdapter(TextView addMore  , AvailRetrieve implement, RecyclerView rv_diagnosis, RelativeLayout tv_noProcAvailable, Context context, ArrayList<TestsAndProcedures> arrayList, TextView tv_total_price, String ORIGIN, AvailInterface callback,DataBaseHandler dataBaseHandler) {
        this.context = context;
        this.arrayList = arrayList;
        this.tv_noProcAvailable = tv_noProcAvailable;
        this.rv_diagnosis = rv_diagnosis;
        tv_price = tv_total_price;
        this.implement = implement;
        this.ORIGIN = ORIGIN;
        this.callback = callback;
        this.addMore = addMore ;
        this.dataBaseHandler = dataBaseHandler;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_procdiagnosis, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        double amount, current_amount;
        DecimalFormat formatter = new DecimalFormat("#,###.##");
        String formatString = "";

        try {
            amount = Double.parseDouble(String.valueOf(arrayList.get(position).getOriginalPrice()));
            holder.tv_procName.setText(arrayList.get(position).getProcedureDesc() + " - " +
                    arrayList.get(position).getMaceServiceType() + "(P " +
                    "" + formatter.format(amount) + ")");

            current_amount = Double.parseDouble(String.valueOf(arrayList.get(position).getProcedureAmount()));
            holder.tv_current_price.setText("P" + " " + formatter.format(current_amount));
        } catch (Exception e) {
            holder.tv_procName.setText(arrayList.get(position).getProcedureDesc());

        }
        tv_price.setText(implement.setPrice(arrayList));
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class Holder extends RecyclerView.ViewHolder {

        TextView tv_procName, tv_current_price;
        ImageButton btn_edit_price;
        ImageButton ib_delete;
        EditPriceDialog editPriceDialog = new EditPriceDialog();

        public Holder(View itemView) {
            super(itemView);
            tv_procName = (TextView) itemView.findViewById(R.id.tv_procName);
            tv_current_price = (TextView) itemView.findViewById(R.id.tv_current_price);
            btn_edit_price = (ImageButton) itemView.findViewById(R.id.btn_edit_price);
            ib_delete = (ImageButton) itemView.findViewById(R.id.ib_delete);

            ib_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    DataBaseHandler dataBaseHandler = new DataBaseHandler(context);
                    if (ORIGIN.equals(Constant.ADAPTER_OTHER)) {
                        dataBaseHandler.setDataProcOthertoFALSE(arrayList.get(getAdapterPosition()).getProcedureCode());
                    } else if (ORIGIN.equals(Constant.ADAPTER_PRIMARY)) {
                        dataBaseHandler.setDataProcPrimarytoFALSE(arrayList.get(getAdapterPosition()).getProcedureCode());
                    } else if (ORIGIN.equals(Constant.ADAPTER_PROCEDURE)) {
                        dataBaseHandler.setDataProceduretoFALSE(arrayList.get(getAdapterPosition()).getProcedureCode());
                    }
                    arrayList.remove(getAdapterPosition());

                    if (arrayList.size() == 0) {
                        addMore.setVisibility(View.GONE);
                        tv_noProcAvailable.setVisibility(View.VISIBLE);
                        rv_diagnosis.setVisibility(View.GONE);
                    } else {
                        tv_noProcAvailable.setVisibility(View.GONE);
                        rv_diagnosis.setVisibility(View.VISIBLE);
                        addMore.setVisibility(View.VISIBLE);
                    }

                    tv_price.setText(implement.setPrice(arrayList));
                    ProceduresToBeSendAdapter.this.notifyDataSetChanged();
                }
            });


            btn_edit_price.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editPriceDialog.showMe(context, getAdapterPosition(), arrayList, ProceduresToBeSendAdapter.this,dataBaseHandler);
                }
            });



        }
    }


    public String setRawPrice(ArrayList<ProceduresListLocal> proceduresListLocals) {


        double price = 0;

        if (proceduresListLocals.size() == 0) {
            return "Please select procedure.";
        } else {

            for (int x = 0; x < proceduresListLocals.size(); x++) {

                price = price + Double.parseDouble(proceduresListLocals.get(x).getProcedureAmount());
            }


            Log.d("PRICE", price + "");
            return price + "";

        }


    }

}
