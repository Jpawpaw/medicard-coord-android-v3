package adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.Navigator.NavigatorRetrieve;
import coord.medicard.com.medicardcoordinator.R;
import fragment.Avail.AvailServices;
import model.Hospital;
import services.ItemClickListener;
import utilities.Constant;
import utilities.SharedPref;

/**
 * Created by IPC on 10/26/2017.
 */

public class UnfilteredHospitalAdapter extends RecyclerView.Adapter<UnfilteredHospitalAdapter.ViewHolder> {

    Context context;
    ArrayList<Hospital> arrayList = new ArrayList<>();
    private ItemClickListener clickListener;
    TextView tv_nav_notification;
    NavigatorRetrieve.NavigatorInterface callback;

    public UnfilteredHospitalAdapter(Context context, ArrayList<Hospital> arrayList, TextView tv_nav_notification,NavigatorRetrieve.NavigatorInterface callback) {
        this.context = context;
        this.arrayList = arrayList;
        this.tv_nav_notification = tv_nav_notification;
        this.callback = callback;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_doctors, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {


        holder.name.setText(arrayList.get(position).getHospitalName());
        holder.position.setVisibility(View.GONE);
//        holder.position.setText(arrayList.get(position).get());
        holder.cv_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSP_ORIGIN, context)
                        .equals(Constant.ADAPTER_UNFILTERED_HOSP)) {
                    EventBus.getDefault().post(new AvailServices.MessageEvent("19"
                            , arrayList.get(position)));
                    SharedPref.HOSPCODELIST.add(arrayList.get(position).getHospitalCode());
                    SharedPref.setStringValue(SharedPref.USER,SharedPref.UNFILTERED_HOSPITAL_CODE,arrayList.get(position).getHospitalCode(),context);
                    SharedPref.setStringValue(SharedPref.USER,SharedPref.UNFILTERED_HOSPITAL_NAME,arrayList.get(position).getHospitalName(),context);
                    SharedPref.setStringValue(SharedPref.USER,SharedPref.UNFILTERED_HOSPITAL_ADDR,arrayList.get(position).getStreetAddress(),context);

                }
                Log.d("TAPPED from adapter", arrayList.get(position).getHospitalName());

                callback.onUnfilteredHospSelected(arrayList.get(position));
            }
        });
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, position;
        CardView cv_item;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tv_name);
            position = (TextView) itemView.findViewById(R.id.tv_position);
            cv_item = (CardView) itemView.findViewById(R.id.cv_item);
//            cv_item.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (clickListener != null)
//                        clickListener.onClick(v, getAdapterPosition());
//                    Log.d("TAPPED from adapter", arrayList.get(poss).getHospitalName());
//                }
//            });

        }

    }


}

