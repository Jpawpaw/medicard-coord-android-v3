package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.R;
import model.Services;
import utilities.DataBaseHandler;

/**
 * Created by mpx-pawpaw on 6/16/17.
 */

public class OtherServicesAdapterToSend extends RecyclerView.Adapter<OtherServicesAdapterToSend.Holder> {

    private Context context;
    private ArrayList<Services> arrayList;
    private OtherServicesCallBack callback;
    private DataBaseHandler dataBaseHandler ;

    public OtherServicesAdapterToSend(DataBaseHandler dataBaseHandler , Context context, ArrayList<Services> arrayList, OtherServicesCallBack callback) {
        this.context = context;
        this.arrayList = arrayList;
        this.callback = callback;
        this.dataBaseHandler = dataBaseHandler  ;


    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_diagnosis_blue_bg, parent, false);

        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.tv_procName.setText(arrayList.get(position).getServiceDesc());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView tv_procName;
        ImageButton ib_delete;


        public Holder(View itemView) {
            super(itemView);
            tv_procName = (TextView) itemView.findViewById(R.id.tv_procName);
            ib_delete = (ImageButton) itemView.findViewById(R.id.ib_ddelete);

            ib_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.otherServicesOnClick(getAdapterPosition());


                }
            });


        }
    }


    public interface OtherServicesCallBack {
        void otherServicesOnClick(int adapterPosition);
    }
}


