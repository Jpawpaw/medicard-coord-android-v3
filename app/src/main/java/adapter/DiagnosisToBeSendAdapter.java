package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.Navigator.NavigatorActivity;
import coord.medicard.com.medicardcoordinator.R;
import fragment.InPatient.InPatientCallback;
import model.DiagnosisList;
import utilities.Constant;
import utilities.DataBaseHandler;

/**
 * Created by mpx-pawpaw on 11/22/16.
 */

public class DiagnosisToBeSendAdapter extends RecyclerView.Adapter<DiagnosisToBeSendAdapter.Holder> {

    private ArrayList<DiagnosisList> arrayList = new ArrayList<>();
    private Context context;
    private DataBaseHandler dataBaseHandler;
    private InPatientCallback callback;

    public DiagnosisToBeSendAdapter(InPatientCallback callback, DataBaseHandler dataBaseHandler, Context context, ArrayList<DiagnosisList> arrayList, TextView tv_doctor) {
        this.context = context;
        this.arrayList = arrayList;
        this.dataBaseHandler = dataBaseHandler;
        this.callback = callback;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_diagnosis_blue_bg, parent, false);

        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        holder.tv_procName.setText(arrayList.get(position).getIcd10Code() + " - " + arrayList.get(position).getDiagDesc());


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView tv_procName;
        ImageButton ib_delete;


        public Holder(View itemView) {
            super(itemView);
            tv_procName = (TextView) itemView.findViewById(R.id.tv_procName);
            ib_delete = (ImageButton) itemView.findViewById(R.id.ib_ddelete);

            ib_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.removeDiagnosisRow(getAdapterPosition());
                }
            });





        }
    }


}
