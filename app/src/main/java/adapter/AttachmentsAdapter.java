package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import coord.medicard.com.medicardcoordinator.R;
import fragment.InPatient.InPatientCallback;
import model.Attachments;

/**
 * Created by mpx-pawpaw on 6/7/17.
 */

public class AttachmentsAdapter extends RecyclerView.Adapter<AttachmentsAdapter.Holder> {


    private Context context;
    private ArrayList<Attachments> arrayList;
    private InPatientCallback callback;

    public AttachmentsAdapter(InPatientCallback callback, Context context, ArrayList<Attachments> arrayList) {
        this.arrayList = arrayList;
        this.context = context;
        this.callback = callback;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_diagnosis_blue_bg, parent, false);

        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        Attachments attachments = arrayList.get(position);

        holder.tv_procName.setText(attachments.getFileName());

        holder.ib_ddelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.removeAttachmentRow(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_procName)
        TextView tv_procName;
        @BindView(R.id.ib_ddelete)
        ImageButton ib_ddelete;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
