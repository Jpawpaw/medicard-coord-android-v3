package adapter;

import android.content.Context;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.R;
import model.Dependents;
import model.Utilization;

/**
 * Created by mpx-pawpaw on 11/18/16.
 */

public class DependentUtilizationAdapter extends RecyclerView.Adapter<DependentUtilizationAdapter.ViewHolder> {

        private int TYPE_HEADER = 0;
        private int TYPE_ITEM = 1;
        Context context;
        ArrayList<Dependents> dependentses = new ArrayList<>();
        ArrayList<Utilization> utilizations = new ArrayList<>();

        public DependentUtilizationAdapter(Context context, ArrayList<Dependents> dependentses, ArrayList<Utilization> utilizations) {
            this.context = context;
            this.dependentses = dependentses;
            this.utilizations = utilizations;


        }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_userlinks, parent, false);
        return new DependentUtilizationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_name.setText(dependentses.get(position).getFirstname() + ", " + dependentses.get(position).getLastname());
        holder.tv_memCode.setText(dependentses.get(position).getCode());

    }

    @Override
    public int getItemCount() {
        return dependentses.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_memCode, tv_name;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_memCode = (TextView) itemView.findViewById(R.id.tv_memberCode);

        }
    }


//    public class TitleHolder extends RecyclerView.ViewHolder {
//
//        TextView tv_title;
//
//        public TitleHolder(View itemView) {
//            super(itemView);
//
//
//            tv_title = (TextView) itemView.findViewById(R.id.tv_name);
//        }
//    }

}
