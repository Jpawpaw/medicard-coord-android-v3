package adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;
import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.Navigator.NavigatorRetrieve;
import coord.medicard.com.medicardcoordinator.R;
import fragment.Avail.AvailServices;
import fragment.InPatient.InPatientAdmitted;
import model.TestsAndProcedures;
import services.ItemClickListener;
import utilities.Constant;
import utilities.DataBaseHandler;
import utilities.OriginTester;

/**
 * Created by mpx-pawpaw on 11/21/16.
 */

public class ProceduresAdapter2 extends RecyclerView.Adapter<ProceduresAdapter2.Holder> {

    DataBaseHandler dataBaseHandler;
    private ItemClickListener clickListener;
    private static Context context;
    private static ArrayList<TestsAndProcedures> arrayList = new ArrayList<>();
    DecimalFormat formatter = new DecimalFormat("#,###.##");
    double amount = 0;
    TextView tv_nav_notification;
    Boolean isCheck = false;
    NavigatorRetrieve implement;
    private String ORIGIN;

    public ProceduresAdapter2(NavigatorRetrieve implement, Context context, ArrayList<TestsAndProcedures> arrayList, TextView tv_nav_notification, String ORIGIN_PROC) {
        this.implement = implement;
        this.context = context;
        this.arrayList = arrayList;
        this.tv_nav_notification = tv_nav_notification;
        this.ORIGIN = ORIGIN_PROC;

    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_other_proc, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        TestsAndProcedures procedure = arrayList.get(position);


        double amount;
        DecimalFormat formatter = new DecimalFormat("#,###.##");
        amount = Double.parseDouble(String.valueOf(arrayList.get(position).getProcedureAmount()));


        holder.name.setText(procedure.getProcedureDesc() + " - P " +
                "" + formatter.format(amount));


        holder.position.setText(arrayList.get(position).getMaceServiceType());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.name.getText().toString();
            }
        });
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.name.getText().toString();
                DataBaseHandler dataBaseHandler = new DataBaseHandler(context);

                if (OriginTester.isOriginFromPrimary(context)) {

                    Log.d("GET_FROM", "PRIMARY");
                    if (holder.checkBox.isChecked()) {
                        dataBaseHandler.setDataProcPrimarytoTRUE(arrayList.get(position).getProcedureCode());

                        EventBus.getDefault().post(new AvailServices.MessageEvent("8"
                                , dataBaseHandler.retrieveSelectedProcPrimary()));

                    } else {
                        dataBaseHandler.setDataProcPrimarytoFALSE(arrayList.get(position).getProcedureCode());
                        EventBus.getDefault().post(new AvailServices.MessageEvent("8"
                                , dataBaseHandler.retrieveSelectedProcPrimary()));
                    }

                } else if (OriginTester.isOriginFromOther(context)) {


                    if (holder.checkBox.isChecked()) {
                        dataBaseHandler.setDataProcOthertoTRUE(arrayList.get(position).getProcedureCode());

                        EventBus.getDefault().post(new AvailServices.MessageEvent("10"
                                , dataBaseHandler.retrieveSelectedProcOther()));


                    } else {
                        dataBaseHandler.setDataProcOthertoFALSE(arrayList.get(position).getProcedureCode());
                        EventBus.getDefault().post(new AvailServices.MessageEvent("10"
                                , dataBaseHandler.retrieveSelectedProcOther()));
                    }


                }

                else if (OriginTester.isOriginFromOtherSecond(context)) {


                    if (holder.checkBox.isChecked()) {
                        dataBaseHandler.setDataProcOthertoTRUE(arrayList.get(position).getProcedureCode());

                        EventBus.getDefault().post(new AvailServices.MessageEvent("18"
                                , dataBaseHandler.retrieveSelectedProcOther()));


                    } else {
                        dataBaseHandler.setDataProcOthertoFALSE(arrayList.get(position).getProcedureCode());
                        EventBus.getDefault().post(new AvailServices.MessageEvent("18"
                                , dataBaseHandler.retrieveSelectedProcOther()));
                    }


                }

                else if (OriginTester.isOriginFromProc(context)) {

                    if (holder.checkBox.isChecked()) {
                        dataBaseHandler.setDataProceduretoTRUE(arrayList.get(position).getProcedureCode());
                        EventBus.getDefault().post(new AvailServices.MessageEvent("5"
                                , dataBaseHandler.retrieveSelectedProcedures()));

                    } else {
                        dataBaseHandler.setDataProceduretoFALSE(arrayList.get(position).getProcedureCode());
                        EventBus.getDefault().post(new AvailServices.MessageEvent("5"
                                , dataBaseHandler.retrieveSelectedProcedures()));

                    }
                } else if (OriginTester.isFromOriginInPatientProc(context)) {
                    Log.d("GET_FROM", "PROC");
                    if (holder.checkBox.isChecked()) {
                        dataBaseHandler.setDataProcInPatienttoTRUE(arrayList.get(position).getProcedureCode());
                        EventBus.getDefault().post(new InPatientAdmitted.MessageEvent(dataBaseHandler.retrieveSelectedProceduresInPatient(), Constant.IN_PATIENT_PROC
                        ));


                    } else {
                        dataBaseHandler.setDataProcInPatienttoFalse(arrayList.get(position).getProcedureCode());
                        EventBus.getDefault().post(new InPatientAdmitted.MessageEvent(
                                dataBaseHandler.retrieveSelectedProceduresInPatient(), Constant.IN_PATIENT_PROC));

                    }
                }

            }
        });

        if (OriginTester.isOriginFromPrimary(context)) {
            dataBaseHandler = new DataBaseHandler(context);
            holder.checkBox.setChecked(dataBaseHandler.getDataProcPrimaryFromTrue(arrayList.get(position).getProcedureCode()));

        } else if (OriginTester.isOriginFromProc(context)) {
            dataBaseHandler = new DataBaseHandler(context);
            holder.checkBox.setChecked(dataBaseHandler.getDataProcedureFromTrue(arrayList.get(position).getProcedureCode()));

        } else if (OriginTester.isOriginFromOther(context)) {
            dataBaseHandler = new DataBaseHandler(context);
            holder.checkBox.setChecked(dataBaseHandler.getDataProcOtherFromTrue(arrayList.get(position).getProcedureCode()));
        } else if (OriginTester.isOriginFromOtherSecond(context)) {
            dataBaseHandler = new DataBaseHandler(context);
            holder.checkBox.setChecked(dataBaseHandler.getDataProcOtherFromTrue(arrayList.get(position).getProcedureCode()));
        } else if (OriginTester.isFromOriginInPatientProc(context)) {
            dataBaseHandler = new DataBaseHandler(context);
            holder.checkBox.setChecked(dataBaseHandler.getDataProcInPatientFromTrue(arrayList.get(position).getProcedureCode()));
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }


    static class Holder extends RecyclerView.ViewHolder {

        TextView name, position;
        CardView cv_item;
        CheckBox checkBox;

        public Holder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.tv_name);
            position = (TextView) itemView.findViewById(R.id.tv_position);
            cv_item = (CardView) itemView.findViewById(R.id.cv_item);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);


        }


    }
}
