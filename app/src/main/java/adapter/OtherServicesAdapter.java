package adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import coord.medicard.com.medicardcoordinator.R;
import fragment.InPatient.InPatientAdmitted;
import model.Services;
import utilities.Constant;
import utilities.DataBaseHandler;

/**
 * Created by mpx-pawpaw on 6/13/17.
 */

public class OtherServicesAdapter extends RecyclerView.Adapter<OtherServicesAdapter.Holder> {

    private ArrayList<Services> arrayList;
    private Context context;
    private DataBaseHandler dataBaseHandler;

    public OtherServicesAdapter(DataBaseHandler dataBaseHandler, Context context, ArrayList<Services> arrayList) {
        this.arrayList = arrayList;
        this.context = context;
        this.dataBaseHandler = dataBaseHandler;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_other_proc, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Services services = arrayList.get(position);

        holder.tv_name.setText(services.getServiceDesc());
        holder.tv_position.setText(services.getId());

        dataBaseHandler = new DataBaseHandler(context);
        holder.checkBox.setChecked(dataBaseHandler.getDataOtherServicesFromTrue(arrayList.get(position).getId()));

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextView tv_name;
        @BindView(R.id.tv_position)
        TextView tv_position;
        @BindView(R.id.cv_item)
        CardView cv_item;
        @BindView(R.id.checkBox)
        CheckBox checkBox;

        public Holder(View itemView) {
            super(itemView);


            ButterKnife.bind(this, itemView);

            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // TODO ADD METHOD TO RETRIEVE ALL SELECTED DOCTORS
                    //TODO ADD INPATIENTADMITTED RESPONSIVE LAYOUT FOR SELECTING DATA
                    if (checkBox.isChecked()) {

                        dataBaseHandler.setDataOtherServicesToTrue(arrayList.get(getAdapterPosition()).getId());

                        EventBus.getDefault().post(new InPatientAdmitted.MessageEvent(Constant.ADAPTER_PROC_OTHER_SERVICES,
                                dataBaseHandler.retrieveSelectedOtherServices(), "", ""));


                    } else {
                        dataBaseHandler.setDataOtherServicesToFalse(arrayList.get(getAdapterPosition()).getId());
                        EventBus.getDefault().post(new InPatientAdmitted.MessageEvent(Constant.ADAPTER_PROC_OTHER_SERVICES,
                                dataBaseHandler.retrieveSelectedOtherServices(), "", ""));
                    }

                }
            });

        }
    }
}
