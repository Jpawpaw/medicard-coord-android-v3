package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import coord.medicard.com.medicardcoordinator.R;
import fragment.InPatient.InPatientCallback;
import model.DoctorModelMultipleSelection;
import utilities.DataBaseHandler;

/**
 * Created by mpx-pawpaw on 6/2/17.
 */

public class DoctorMultipleToSendAdapter extends RecyclerView.Adapter<DoctorMultipleToSendAdapter.Holder> {

    private ArrayList<DoctorModelMultipleSelection> arrayList = new ArrayList<>();
    private Context context;
    private DataBaseHandler dataBaseHandler;
    private InPatientCallback callback;

    public DoctorMultipleToSendAdapter(InPatientCallback callback, Context context, ArrayList<DoctorModelMultipleSelection> arrayList,
                                       DataBaseHandler dataBaseHandler) {
        this.context = context;
        this.arrayList = arrayList;
        this.dataBaseHandler = dataBaseHandler;
        this.callback = callback ;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_doctormulti, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {


        holder.tv_Name.setText(arrayList.get(position).getDocLname() + ", " + arrayList.get(position).getDocFname());
        holder.tv_desc.setText(arrayList.get(position).getSpecDesc());


        holder.ib_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.removeDoctorRow(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.ib_delete)
        ImageButton ib_delete;
        @BindView(R.id.tv_Name)
        TextView tv_Name;
        @BindView(R.id.tv_desc)
        TextView tv_desc;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }
}
