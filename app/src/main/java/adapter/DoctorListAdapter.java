package adapter;

import android.content.Context;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.R;
import model.DoctorModelInsertion;
import services.ItemClickListener;

/**
 * Created by mpx-pawpaw on 11/16/16.
 */

public class DoctorListAdapter extends RecyclerView.Adapter<DoctorListAdapter.ViewHolder> {

    Context context;
    ArrayList<DoctorModelInsertion> arrayList = new ArrayList<>();
    private ItemClickListener clickListener;
    TextView tv_nav_notification;

    public DoctorListAdapter(Context context, ArrayList<DoctorModelInsertion> arrayList, TextView tv_nav_notification) {
        this.context = context;
        this.arrayList = arrayList;
        this.tv_nav_notification = tv_nav_notification;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_doctors, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.name.setText(arrayList.get(position).getDocLname() + ", " + arrayList.get(position).getDocFname());
        holder.position.setText(arrayList.get(position).getSpecDesc());

    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, position;
        CardView cv_item;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tv_name);
            position = (TextView) itemView.findViewById(R.id.tv_position);
            cv_item = (CardView) itemView.findViewById(R.id.cv_item);

            cv_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener != null)
                        clickListener.onClick(v, getAdapterPosition());
                }
            });

        }

    }


}
