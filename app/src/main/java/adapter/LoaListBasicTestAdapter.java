package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import coord.medicard.com.medicardcoordinator.R;
import model.LoaList;

/**
 * Created by IPC on 9/28/2017.
 */

public class LoaListBasicTestAdapter extends RecyclerView.Adapter<LoaListBasicTestAdapter.Holder> {
    private Context context;


    ArrayList<LoaList.GroupedByCostCenter> arrayListGroupedByCostCenter = new ArrayList<>();


    public LoaListBasicTestAdapter(Context context, ArrayList<LoaList.GroupedByCostCenter> arrayListGroupedByCostCenter) {
        this.context = context;
        this.arrayListGroupedByCostCenter = arrayListGroupedByCostCenter;
    }


    @Override
    public LoaListBasicTestAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.loa_procedure_row_nested, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(LoaListBasicTestAdapter.Holder holder, int position) {

        ArrayAdapter<String> adapterProcedures, adapterPrice, adapterStatus;
        ArrayList<String> arrayListProcedures = new ArrayList<String>();
        ArrayList<String> arrayListPrice = new ArrayList<String>();
//        ArrayList<String> arrayListStatus = new ArrayList<String>();


        LoaList.GroupedByCostCenter perGroupedByCostCenter = arrayListGroupedByCostCenter.get(position);
//        perGroupedByDiag.getApprovalNo();
//        perGroupedByDiag.getStatus();
//        perGroupedByDiag.getCostCenter();
//        perGroupedByDiag.getSubTotal();
//        for (LoaList.MappedTest mappedTest : perGroupedByDiag.getMappedTests()) {
//
//        }
        try {
            if (perGroupedByCostCenter.isFirstInstance()) {
//                holder.tv_procedure_name.setText(perGroupedByCostCenter.getDiagType() == 1 ? "PRIMARY DIAGNOSIS:" : "OTHER DIAGNOSIS:");
//                holder.tv_processName.setText(perGroupedByCostCenter.getDiagDesc());
                holder.tv_procedure_name.setVisibility(View.GONE);
                holder.tv_processName.setVisibility(View.GONE);


            } else {
                holder.tv_procedure_name.setVisibility(View.GONE);
                holder.tv_processName.setVisibility(View.GONE);
            }
            holder.tv_costcenter.setText(perGroupedByCostCenter.getStatus().equals("APPROVED") ? "Approval Code:": "");
            holder.tv_status.setText(perGroupedByCostCenter.getStatus().equals("APPROVED") ? perGroupedByCostCenter.getGroupedByDiag().get(position).getApprovalNo(): "PENDING");

            for (LoaList.MappedTest mappedTest : perGroupedByCostCenter.getGroupedByDiag().get(position).getMappedTests()) {
                arrayListProcedures.add(mappedTest.getProcDesc());
                arrayListPrice.add("P " + mappedTest.getAmount());
            }

//            arrayListProcedures.add(perDiag.getByCostCenterList().get(position).getMappedTests().get(position).getProcDesc());
            adapterProcedures = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, arrayListProcedures);
            holder.lv_procedure.setAdapter(adapterProcedures);
            holder.lv_procedure.setEnabled(false);
            setListViewHeightBasedOnItems(holder.lv_procedure);

//            arrayListPrice.add(perDiag.getByCostCenterList().get(position).getMappedTests().get(position).getAmount());
            adapterPrice = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, arrayListPrice);
            holder.lv_price.setAdapter(adapterPrice);
            holder.lv_price.setEnabled(false);
            setListViewHeightBasedOnItems(holder.lv_price);


            holder.tv_subTotal.setVisibility(View.GONE);
            holder.tv_price_subtotal.setVisibility(View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return arrayListGroupedByCostCenter.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_processName)
        TextView tv_processName;
        @BindView(R.id.tv_cost_center)
        TextView tv_cost_center;
        @BindView(R.id.tv_status)
        TextView tv_status;
        @BindView(R.id.tv_procedure_name)
        TextView tv_procedure_name;
        @BindView(R.id.tv_costcenter)
        TextView tv_costcenter;
        @BindView(R.id.tv_subTotal)
        TextView tv_subTotal;
        @BindView(R.id.tv_price_subtotal)
        TextView tv_price_subtotal;
        @BindView(R.id.lv_procedure)
        ListView lv_procedure;
        @BindView(R.id.lv_price)
        ListView lv_price;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                float px = 500 * (listView.getResources().getDisplayMetrics().density);
                item.measure(View.MeasureSpec.makeMeasureSpec((int) px, View.MeasureSpec.AT_MOST), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);
            // Get padding
            int totalPadding = listView.getPaddingTop() + listView.getPaddingBottom();

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight + totalPadding;
            listView.setLayoutParams(params);
            listView.requestLayout();
            return true;

        } else {
            return false;
        }

    }
}
