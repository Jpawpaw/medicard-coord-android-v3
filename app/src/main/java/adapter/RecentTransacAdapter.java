package adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import coord.medicard.com.medicardcoordinator.R;
import model.RecentTransactions;
import utilities.SharedPref;

/**
 * Created by IPC on 11/2/2017.
 */

public class RecentTransacAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> listDataCounter;
    private List<String> listDataHeader;
    // child data in format of header title, child title
    private HashMap<String, List<RecentTransactions>> listRecentTrans;
    // Test
    TextView tv_header_item;
    TextView tv_header_count;
    int pos;

    public RecentTransacAdapter(Context context, List<String> listDataHeader,List<String> listDataCounter,HashMap<String, List<RecentTransactions>> listRecentTrans) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        this.listRecentTrans = listRecentTrans;
        this.listDataCounter = listDataCounter;
        this.pos = 0;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.listRecentTrans.get(this.listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.exp_header_subitem_notif, null);
        }
//        final String childText = (String) getChild(groupPosition, childPosition);
        TextView tv_subitem_item = (TextView) convertView.findViewById(R.id.tv_subitem_item);
        TextView tv_subitem_outpatient = (TextView) convertView.findViewById(R.id.tv_subitem_outpatient);
        TextView tv_subitem_number = (TextView) convertView.findViewById(R.id.tv_subitem_number);


        int x = listRecentTrans.get(listDataHeader.get(groupPosition)).indexOf(listRecentTrans.get(listDataHeader.get(groupPosition)).get(childPosition));
        tv_subitem_number.setText(String.valueOf(x + 1));
        tv_subitem_item.setText(listRecentTrans.get(listDataHeader.get(groupPosition)).get(childPosition).getMemName());
        tv_subitem_outpatient.setText(listRecentTrans.get(listDataHeader.get(groupPosition)).get(childPosition).getServiceType());

        Log.d(".size()", String.valueOf(listRecentTrans.get(listDataHeader.get(groupPosition)).size()));
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.listRecentTrans.get(listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,View convertView, ViewGroup parent) {
        this.pos = groupPosition;

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.exp_header_notif, null);
        }

        tv_header_item = (TextView) convertView.findViewById(R.id.tv_header_item);
        tv_header_count = (TextView) convertView.findViewById(R.id.tv_header_count);

        tv_header_item.setText(listDataHeader.get(groupPosition) + " LIST");
        tv_header_count.setText(""+this.listRecentTrans.get(listDataHeader.get(groupPosition)).size());
        Log.d("this.listRecentTrans", this.listRecentTrans.get(listDataHeader.get(groupPosition)).toString());
        Log.d("this.listDataCounter", this.listDataCounter.toString());
//        setCount(tv_header_count,listDataHeader.get(groupPosition).toString());
        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}