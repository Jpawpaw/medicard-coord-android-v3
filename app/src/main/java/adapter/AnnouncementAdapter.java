package adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.R;
import model.AnnouncementDataJson;
import services.ItemClickListener;
import utilities.AlertDialogCustom;

/**
 * Created by IPC on 10/6/2017.
 */

public class AnnouncementAdapter extends RecyclerView.Adapter<AnnouncementAdapter.ViewHolder> {

    Context context;
    ArrayList<AnnouncementDataJson.AnnouncementDataModel> arrayList = new ArrayList<>();
    private ItemClickListener clickListener;
    AlertDialogCustom alertDialogCustom = new AlertDialogCustom();


    public AnnouncementAdapter(Context context, ArrayList<AnnouncementDataJson.AnnouncementDataModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;


    }


    @Override
    public AnnouncementAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_notif, parent, false);
        return new AnnouncementAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AnnouncementAdapter.ViewHolder holder, final int position) {
        holder.tv_time.setPaintFlags(holder.tv_time.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.tv_date.setText(arrayList.get(position).getMessageType()+" :");
        holder.tv_time.setText(arrayList.get(position).getDatePosted() +" "+arrayList.get(position).getMessageHeader());
        holder.tv_header.setVisibility(View.GONE);
        holder.tv_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogCustom.showNotif(context,arrayList.get(position).getMessageHeader(),arrayList.get(position).getMessageContent());
                arrayList.get(position).setIsClicked("true");
                if(arrayList.get(position).getIsClicked().equalsIgnoreCase("true")){
                    holder.tv_time.setTextColor(context.getResources().getColor(R.color.blue, null));
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_date, tv_time,tv_header;
        LinearLayout ll_notif;


        public ViewHolder(View itemView) {
            super(itemView);
            tv_date = (TextView) itemView.findViewById(R.id.tv_date);
            tv_time = (TextView) itemView.findViewById(R.id.tv_time);
            tv_header = (TextView) itemView.findViewById(R.id.tv_header);
            ll_notif = (LinearLayout) itemView.findViewById(R.id.ll_notif);
            ll_notif.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener != null)
                        clickListener.onClick(v, getAdapterPosition());
                }
            });

        }

    }


}



