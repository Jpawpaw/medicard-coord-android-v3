package adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;
import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.R;
import fragment.Avail.AvailServices;
import model.DoctorModelInsertion;
import services.ItemClickListener;
import utilities.DataBaseHandler;

/**
 * Created by IPC on 12/5/2017.
 */

public class InpatientDoctorSelectAdapter_Navigator extends RecyclerView.Adapter<InpatientDoctorSelectAdapter_Navigator.Holder> {

    DataBaseHandler dataBaseHandler;
    private ItemClickListener clickListener;
    private static Context context;
    private static ArrayList<DoctorModelInsertion> arrayList = new ArrayList<>();

    public InpatientDoctorSelectAdapter_Navigator(Context context, ArrayList<DoctorModelInsertion> arrayList, DataBaseHandler dataBaseHandler) {
        this.context = context;
        this.arrayList = arrayList;
        this.dataBaseHandler = dataBaseHandler;

    }



    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_inpatient_doc, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        DoctorModelInsertion doctor = arrayList.get(position);

        holder.name.setText(doctor.getDocLname() + ", " + doctor.getDocFname() +" " +doctor.getDocMname());
        holder.position.setText(doctor.getSpecDesc());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.name.getText().toString();
            }
        });

        holder.cv_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.name.getText().toString();
                DataBaseHandler dataBaseHandler = new DataBaseHandler(context);
                if (!holder.checkBox.isChecked()) {
                    holder.checkBox.setChecked(true);
                    dataBaseHandler.setDataInPatientDoctortoTRUE(arrayList.get(position).getDoctorCode());
                    EventBus.getDefault().post(new AvailServices.MessageEvent("20"
                            , dataBaseHandler.retrieveSelectedInpatientDoctor(),"",""));
                } else {
                    holder.checkBox.setChecked(false);
                    dataBaseHandler.setDataInPatientDoctortoFALSE(arrayList.get(position).getDoctorCode());
                    EventBus.getDefault().post(new AvailServices.MessageEvent("20"
                            , dataBaseHandler.retrieveSelectedInpatientDoctor(),"",""));

                }

            }
        });

        holder.checkBox.setClickable(false);
        holder.checkBox.setChecked(dataBaseHandler.getDataInpatientDoctorFromTrue(doctor.getDoctorCode()));

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }


    static class Holder extends RecyclerView.ViewHolder {

        TextView name, position;
        CardView cv_item;
        CheckBox checkBox;
        ImageButton ib_delete;

        public Holder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.tv_name);
            position = (TextView) itemView.findViewById(R.id.tv_position);
            cv_item = (CardView) itemView.findViewById(R.id.cv_item);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);

            ib_delete = (ImageButton) itemView.findViewById(R.id.ib_delete);
            ib_delete.setVisibility(View.GONE);

        }


    }
}
