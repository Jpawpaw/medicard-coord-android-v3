package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import coord.medicard.com.medicardcoordinator.R;
import fragment.InPatient.InPatientCallback;
import model.ProceduresListLocal;
import model.TestsAndProcedures;
import utilities.Constant;
import utilities.DataBaseHandler;
import utilities.EditPriceDialog;

/**
 * Created by mpx-pawpaw on 6/1/17.
 */

public class ProceduresAdapterInPatient extends RecyclerView.Adapter<ProceduresAdapterInPatient.Holder> {


    Context context;
    private ArrayList<TestsAndProcedures> arrayList = new ArrayList<>();
    InPatientCallback callback;

    public ProceduresAdapterInPatient(InPatientCallback callback, Context context, ArrayList<TestsAndProcedures> arrayList) {
        this.arrayList = arrayList;
        this.context = context;
        this.callback = callback;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_procdiagnosis_in_patitent, parent, false);


        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {


        double amount, current_amount;
        DecimalFormat formatter = new DecimalFormat("#,###.##");
        String formatString = "";

        try {
            amount = Double.parseDouble(String.valueOf(arrayList.get(position).getOriginalPrice()));
            holder.tv_procName.setText(arrayList.get(position).getProcedureDesc() + " - "
                    + arrayList.get(position).getMaceServiceType() + "(P " +
                    "" + formatter.format(amount) + ")");


            current_amount = Double.parseDouble(String.valueOf(arrayList.get(position).getProcedureAmount()));
            holder.tv_current_price.setText("P" + " " + formatter.format(current_amount));
        } catch (Exception e) {
            holder.tv_procName.setText(arrayList.get(position).getProcedureDesc());

        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_procName)
        TextView tv_procName;
        @BindView(R.id.tv_current_price)
        TextView tv_current_price;
        @BindView(R.id.btn_edit_price)
        ImageButton btn_edit_price;
        @BindView(R.id.ib_delete)
        ImageButton ib_delete;


        EditPriceDialog editPriceDialog = new EditPriceDialog();


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


            btn_edit_price.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editPriceDialog.showMeForUpdatePrice(context, getAdapterPosition(), arrayList, ProceduresAdapterInPatient.this);
                }
            });
            tv_procName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  callback.addProcedureFromSelectedProcedure();
                }
            });


            ib_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.removeSingleProcedure(getAdapterPosition());
                }
            });

        }
    }

}
