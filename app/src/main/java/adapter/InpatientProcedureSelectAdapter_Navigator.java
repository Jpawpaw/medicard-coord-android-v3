package adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;
import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.Navigator.NavigatorRetrieve;
import coord.medicard.com.medicardcoordinator.R;
import fragment.Avail.AvailServices;
import fragment.InPatient.InPatientAdmitted;
import model.DoctorModelInsertion;
import model.TestsAndProcedures;
import services.ItemClickListener;
import utilities.Constant;
import utilities.DataBaseHandler;
import utilities.OriginTester;

/**
 * Created by IPC on 12/6/2017.
 */

public class InpatientProcedureSelectAdapter_Navigator extends RecyclerView.Adapter<InpatientProcedureSelectAdapter_Navigator.Holder> {

    private ItemClickListener clickListener;
    private static Context context;
    private static ArrayList<TestsAndProcedures> arrayList = new ArrayList<>();
    DecimalFormat formatter = new DecimalFormat("#,###.##");
    double amount = 0;
    TextView tv_nav_notification;
    Boolean isCheck = false;
    DataBaseHandler dataBaseHandler;

    public InpatientProcedureSelectAdapter_Navigator(Context context, ArrayList<TestsAndProcedures> arrayList, DataBaseHandler dataBaseHandler) {

        this.context = context;
        this.arrayList = arrayList;
        this.dataBaseHandler = dataBaseHandler;


    }


    @Override
    public InpatientProcedureSelectAdapter_Navigator.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_other_proc, parent, false);
        return new InpatientProcedureSelectAdapter_Navigator.Holder(view);
    }

    @Override
    public void onBindViewHolder(final InpatientProcedureSelectAdapter_Navigator.Holder holder, final int position) {
        TestsAndProcedures procedure = arrayList.get(position);


        double amount;
        DecimalFormat formatter = new DecimalFormat("#,###.##");
        amount = Double.parseDouble(String.valueOf(arrayList.get(position).getProcedureAmount()));


        holder.name.setText(procedure.getProcedureDesc() + " - P " +
                "" + formatter.format(amount));


        holder.position.setText(arrayList.get(position).getMaceServiceType());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.name.getText().toString();
            }
        });
        holder.cv_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!holder.checkBox.isChecked()) {
                    holder.checkBox.setChecked(true);
                    dataBaseHandler.setDataInPatientProceduretoTRUE(arrayList.get(position).getProcedureCode());
                    EventBus.getDefault().post(new AvailServices.MessageEvent("22"
                            , dataBaseHandler.retrieveSelectedInpatientProcedure(),"","","",""));
                } else {
                    holder.checkBox.setChecked(false);
                    dataBaseHandler.setDataInPatientProceduretoFALSE(arrayList.get(position).getProcedureCode());
                    EventBus.getDefault().post(new AvailServices.MessageEvent("22"
                            , dataBaseHandler.retrieveSelectedInpatientProcedure(),"","","",""));
                }

            }
        });


        holder.checkBox.setClickable(false);
        holder.checkBox.setChecked(dataBaseHandler.getDataInpatientProcedureFromTrue(procedure.getProcedureCode()));


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }


    static class Holder extends RecyclerView.ViewHolder {

        TextView name, position;
        CardView cv_item;
        CheckBox checkBox;

        public Holder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.tv_name);
            position = (TextView) itemView.findViewById(R.id.tv_position);
            cv_item = (CardView) itemView.findViewById(R.id.cv_item);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);

        }


    }
}