package adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.Navigator.NavigatorRetrieve;
import coord.medicard.com.medicardcoordinator.R;
import fragment.Avail.AvailServices;
import utilities.Constant;

/**
 * Created by mpx-pawpaw on 6/27/17.
 */

public class RoomCategoryAdapter extends RecyclerView.Adapter<RoomCategoryAdapter.Holder> {
    private Context context;
    private ArrayList<String> arrayList;
    private NavigatorRetrieve.NavigatorInterface callback;

    public RoomCategoryAdapter(ArrayList<String> arrayList, Context context, NavigatorRetrieve.NavigatorInterface callback) {
        this.context = context;
        this.arrayList = arrayList;
        this.callback = callback ;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_doctors, parent, false);
        return new RoomCategoryAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        holder.name.setText(arrayList.get(position));
        holder.position.setVisibility(View.GONE);


        holder.cv_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.roomCategoryCallback(arrayList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {


        TextView name, position;
        CardView cv_item;

        public Holder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tv_name);
            position = (TextView) itemView.findViewById(R.id.tv_position);
            cv_item = (CardView) itemView.findViewById(R.id.cv_item);


        }

    }
}
