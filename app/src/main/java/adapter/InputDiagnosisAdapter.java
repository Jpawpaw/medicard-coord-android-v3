package adapter;

import android.content.Context;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.R;

/**
 * Created by mpx-pawpaw on 11/26/16.
 */

public class InputDiagnosisAdapter extends RecyclerView.Adapter<InputDiagnosisAdapter.Holder> {

    Context context;
   // private ArrayList<String> fields;
    public ArrayList<EditText> inputFields;

    public InputDiagnosisAdapter(Context context) {
        this.context = context;
      //  this.fields = new ArrayList<>();
        this.inputFields = new ArrayList<>();
    }

    public void addField(String field) {
       // this.fields.add(field);
        this.inputFields.add(new EditText(context));
        notifyDataSetChanged();

    }

    public ArrayList<String> getData() {
        ArrayList<String> data = new ArrayList<>();
        for (EditText input : inputFields) data.add(input.getText().toString());
        return data;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {


        return new Holder(LayoutInflater.from(context).inflate(R.layout.row_input_data, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
     //   this.inputFields.set(position,holder.et_input);


        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                inputFields.remove(position);
                notifyDataSetChanged();
            }
        });


    }

    @Override
    public int getItemCount() {
        return inputFields.size();
    }

    static class Holder extends RecyclerView.ViewHolder {

        EditText et_input;
        ImageView iv_delete;

        public Holder(View itemView) {
            super(itemView);

            et_input = (EditText) itemView.findViewById(R.id.et_input);
            iv_delete = (ImageView) itemView.findViewById(R.id.iv_delete);


        }
    }
}
