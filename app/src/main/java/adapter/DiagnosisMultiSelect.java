package adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.R;
import fragment.Avail.AvailServices;
import fragment.InPatient.InPatientAdmitted;
import model.DiagnosisList;
import utilities.Constant;
import utilities.DataBaseHandler;

/**
 * Created by mpx-pawpaw on 6/2/17.
 */

public class DiagnosisMultiSelect extends RecyclerView.Adapter<DiagnosisMultiSelect.Holder> {

    private ArrayList<DiagnosisList> arrayList;
    private Context context;
    private DataBaseHandler dataBaseHandler;

    public DiagnosisMultiSelect(Context context, ArrayList<DiagnosisList> arrayList,
                                DataBaseHandler dataBaseHandler) {
        this.context = context;
        this.arrayList = arrayList;
        this.dataBaseHandler = dataBaseHandler;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_other_proc, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        holder.name.setText(arrayList.get(position).getDiagDesc());
        holder.position.setText(arrayList.get(position).getIcd10Code());

        dataBaseHandler = new DataBaseHandler(context);
        holder.checkBox.setChecked(dataBaseHandler.getDataDiagnosisOtherFromTrue(arrayList.get(position).getDiagCode()));


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class Holder extends RecyclerView.ViewHolder {

        TextView name, position;
        CardView cv_item;
        CheckBox checkBox;

        public Holder(View itemView) {
            super(itemView);


            name = (TextView) itemView.findViewById(R.id.tv_name);
            position = (TextView) itemView.findViewById(R.id.tv_position);
            cv_item = (CardView) itemView.findViewById(R.id.cv_item);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);


            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (checkBox.isChecked()) {
                        dataBaseHandler.setDataDiagnoseOthertoTRUE(arrayList.get(getAdapterPosition()).getDiagCode());


                        EventBus.getDefault().post(new InPatientAdmitted.MessageEvent(Constant.DIAGNOSIS_OTHER_IN_PATIENT_ADD
                                , arrayList.get(getAdapterPosition()) ));


                    } else {
                        dataBaseHandler.setDataDiagnoseOtherToFALSE(arrayList.get(getAdapterPosition()).getDiagCode());


                        EventBus.getDefault().post(new InPatientAdmitted.MessageEvent(Constant.DIAGNOSIS_OTHER_IN_PATIENT_REMOVE
                                , arrayList.get(getAdapterPosition()) ));

                    }
                }
            });
        }
    }

}