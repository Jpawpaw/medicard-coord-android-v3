package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import coord.medicard.com.medicardcoordinator.R;
import model.LoaList;

/**
 * Created by IPC on 9/28/2017.
 */

public class LoaListProcedureAdapter extends RecyclerView.Adapter<LoaListProcedureAdapter.Holder>  {
    private Context context;
    ArrayList<LoaList.MappedTest> arrayList = new ArrayList<>();


    public LoaListProcedureAdapter(Context context, ArrayList<LoaList.MappedTest> arrayList) {
        this.context = context;
        this.arrayList = arrayList;

    }


    @Override
    public LoaListProcedureAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.loalist_procedure_row, parent, false);
        return new Holder(view);

    }

    @Override
    public void onBindViewHolder(LoaListProcedureAdapter.Holder holder, int position) {

        LoaList.MappedTest mappedTest = arrayList.get(position);

        holder.tv_procedure_name.setText(mappedTest.getProcDesc());
        holder.tv_price.setText(mappedTest.getAmount());

    }

    @Override
    public int getItemCount() { return arrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_cost_center)
        TextView tv_cost_center;
        @BindView(R.id.tv_procedure_name)
        TextView tv_procedure_name;
        @BindView(R.id.tv_price)
        TextView tv_price;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
