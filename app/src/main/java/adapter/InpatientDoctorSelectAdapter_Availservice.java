package adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;
import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.R;
import fragment.Avail.AvailServices;
import model.DoctorModelInsertion;
import services.ItemClickListener;
import utilities.Constant;
import utilities.DataBaseHandler;

/**
 * Created by IPC on 12/4/2017.
 */

public class InpatientDoctorSelectAdapter_Availservice extends RecyclerView.Adapter<InpatientDoctorSelectAdapter_Availservice.Holder> {

    DataBaseHandler dataBaseHandler;
    private ItemClickListener clickListener;
    private static Context context;
    private static ArrayList<DoctorModelInsertion> arrayList = new ArrayList<>();
    DecimalFormat formatter = new DecimalFormat("#,###.##");
    double amount = 0;
    TextView tv_nav_notification;
    Boolean isCheck = false;
    RecyclerView rv_inpatient_doctor;
    Button btn_inpatient_doctor;
    TextView tv_doctor_in_patient;

    public InpatientDoctorSelectAdapter_Availservice(DataBaseHandler dataBaseHandler,Context context, ArrayList<DoctorModelInsertion> arrayList, RecyclerView rv_inpatient_doctor, Button btn_inpatient_doctor,TextView tv_doctor_in_patient) {
        this.context = context;
        this.arrayList = arrayList;
        this.tv_nav_notification = tv_nav_notification;
        this.rv_inpatient_doctor = rv_inpatient_doctor;
        this.btn_inpatient_doctor = btn_inpatient_doctor;
        this.tv_doctor_in_patient = tv_doctor_in_patient;
        this.dataBaseHandler = dataBaseHandler;


    }



    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_inpatient_doc_nav, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        DoctorModelInsertion doctor = arrayList.get(position);

        holder.name.setText(doctor.getDocLname() + ", " + doctor.getDocFname() +" " +doctor.getDocMname());
        holder.position.setText(doctor.getSpecDesc());


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public class Holder extends RecyclerView.ViewHolder {

        TextView name, position;
        RelativeLayout cv_item;
        ImageButton ib_delete;
        CheckBox checkBox;

        public Holder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.tv_name);
            position = (TextView) itemView.findViewById(R.id.tv_position);
            cv_item = (RelativeLayout) itemView.findViewById(R.id.cv_item);
            ib_delete = (ImageButton) itemView.findViewById(R.id.ib_delete);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);
            checkBox.setVisibility(View.GONE);
            ib_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dataBaseHandler.setDataInPatientDoctortoFALSE(arrayList.get(getAdapterPosition()).getDoctorCode());
                    arrayList.remove(getAdapterPosition());

                    if (arrayList.size() == 0) {
                        btn_inpatient_doctor.setVisibility(View.GONE);
                        tv_doctor_in_patient.setVisibility(View.VISIBLE);
                        rv_inpatient_doctor.setVisibility(View.GONE);
                    } else {
                        tv_doctor_in_patient.setVisibility(View.GONE);
                        rv_inpatient_doctor.setVisibility(View.VISIBLE);
                        btn_inpatient_doctor.setVisibility(View.VISIBLE);
                    }

                    InpatientDoctorSelectAdapter_Availservice.this.notifyDataSetChanged();
                }
            });

        }


    }
}