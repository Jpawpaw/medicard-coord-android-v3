package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.R;
import fragment.Avail.AvailInterface;
import fragment.Avail.AvailRetrieve;
import model.ProceduresListLocal;
import model.TestsAndProcedures;
import utilities.Constant;
import utilities.DataBaseHandler;
import utilities.EditPriceDialog;

/**
 * Created by IPC on 12/6/2017.
 */

public class InpatientProcedureSelectAdapter_Availservice extends RecyclerView.Adapter<InpatientProcedureSelectAdapter_Availservice.Holder> {

    private Context context;
    private ArrayList<TestsAndProcedures> arrayList = new ArrayList<>();
    private RecyclerView rv_diagnosis;
    private RelativeLayout tv_noProcAvailable;
    private TextView tv_price;
    private AvailRetrieve implement;
    private String ORIGIN;
    private AvailInterface callback;
    private TextView addMore ;
    private DataBaseHandler dataBaseHandler;

    LinearLayout ll2_inpatient_procedure;
    private TextView tv_inpatient_procedure;
    private RecyclerView rv_inpatient_procedure;
    private Button btn_inpatient_procedure;

    public InpatientProcedureSelectAdapter_Availservice(DataBaseHandler dataBaseHandler, Context context, ArrayList<TestsAndProcedures> arrayList, LinearLayout ll2_inpatient_procedure, Button btn_inpatient_procedure, TextView tv_inpatient_procedure, AvailRetrieve implement) {
        this.context = context;
        this.arrayList = arrayList;
        this.tv_noProcAvailable = tv_noProcAvailable;
        this.rv_diagnosis = rv_diagnosis;
//        tv_price = tv_total_price;
        this.implement = implement;
        this.ORIGIN = ORIGIN;
        this.callback = callback;
        this.addMore = addMore ;

        this.dataBaseHandler = dataBaseHandler;
        this.tv_inpatient_procedure = tv_inpatient_procedure;
        this.ll2_inpatient_procedure = ll2_inpatient_procedure;
        this.btn_inpatient_procedure = btn_inpatient_procedure;
    }

    @Override
    public InpatientProcedureSelectAdapter_Availservice.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_procdiagnosis, parent, false);
        return new InpatientProcedureSelectAdapter_Availservice.Holder(view);
    }

    @Override
    public void onBindViewHolder(InpatientProcedureSelectAdapter_Availservice.Holder holder, int position) {

        double amount, current_amount;
        DecimalFormat formatter = new DecimalFormat("#,###.##");
        String formatString = "";

        try {
            amount = Double.parseDouble(String.valueOf(arrayList.get(position).getOriginalPrice()));
            holder.tv_procName.setText(arrayList.get(position).getProcedureDesc() + "\n" +
                    arrayList.get(position).getMaceServiceType());

            current_amount = Double.parseDouble(String.valueOf(arrayList.get(position).getProcedureAmount()));
            holder.tv_current_price.setText("P" + " " + formatter.format(current_amount));
        } catch (Exception e) {
            holder.tv_procName.setText(arrayList.get(position).getProcedureDesc());

        }
//        tv_price.setText(implement.setPrice(arrayList));
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class Holder extends RecyclerView.ViewHolder {

        TextView tv_procName, tv_current_price;
        ImageButton btn_edit_price;
        ImageButton ib_delete;
        EditPriceDialog editPriceDialog = new EditPriceDialog();

        public Holder(View itemView) {
            super(itemView);
            tv_procName = (TextView) itemView.findViewById(R.id.tv_procName);
            tv_current_price = (TextView) itemView.findViewById(R.id.tv_current_price);
            btn_edit_price = (ImageButton) itemView.findViewById(R.id.btn_edit_price);
            ib_delete = (ImageButton) itemView.findViewById(R.id.ib_delete);

            ib_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    dataBaseHandler.setDataInPatientProceduretoFALSE(arrayList.get(getAdapterPosition()).getProcedureCode());
                    arrayList.remove(getAdapterPosition());

                    if (arrayList.size() == 0) {
                        btn_inpatient_procedure.setVisibility(View.GONE);
                        tv_inpatient_procedure.setVisibility(View.VISIBLE);
                        ll2_inpatient_procedure.setVisibility(View.GONE);
                    } else {
                        tv_inpatient_procedure.setVisibility(View.GONE);
                        ll2_inpatient_procedure.setVisibility(View.VISIBLE);
                        btn_inpatient_procedure.setVisibility(View.VISIBLE);
                    }

//                    tv_price.setText(implement.setPrice(arrayList));
                    InpatientProcedureSelectAdapter_Availservice.this.notifyDataSetChanged();
                }
            });


            btn_edit_price.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editPriceDialog.showMe(context, getAdapterPosition(), arrayList, InpatientProcedureSelectAdapter_Availservice.this,dataBaseHandler,"");
                }
            });



        }
    }

}
