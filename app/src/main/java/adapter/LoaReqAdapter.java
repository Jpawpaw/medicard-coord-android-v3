package adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import coord.medicard.com.medicardcoordinator.LoaDisplay.LoaListCardRetrieve;
import coord.medicard.com.medicardcoordinator.R;
import model.LoaList;
import utilities.DataBaseHandler;
import utilities.DateConverter;
import utilities.StatusChecker;

/**
 * Created by mpx-pawpaw on 3/3/17.
 */

public class LoaReqAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //    private ArrayList<LoaList> loalist;
    private ArrayList<LoaListCardRetrieve> loaListCardRetrieves;
    private Context context;
    private DataBaseHandler database;
    private LoaCLickListener callback;

    private int WITH_DOCTOR = 0;
    private int WITHOUT_DOCTOR = 1;


    public LoaReqAdapter(Context context, ArrayList<LoaList> loaList, DataBaseHandler dataBaseHandler, LoaCLickListener callback, ArrayList<LoaListCardRetrieve> loaListCardRetrieves) {
        this.context = context;
//        this.loalist = loaList;
        database = dataBaseHandler;
        this.callback = callback;
        this.loaListCardRetrieves = loaListCardRetrieves;

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.row_loareq, parent, false);
        return new Holder(itemView);


    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder hold, int position) {

//        System.out.println("doctor " + loalist.get(position).getRequestTypeDetail01() + "doctor spec" + loalist.get(position).getRequestTypeDetail02() +
//                " servicetype" + loalist.get(position).getRequestType() + "date " + loalist.get(position).getRequestDatetime() + loalist.get(position).getRequestTypeDetail03());


//        if (hold instanceof HolderNonDoctor) {
//            final HolderNonDoctor holder = (LoaReqAdapter.HolderNonDoctor) hold;
//            setData(loalist.get(position).getRequestType(), holder.service_type);
//            setData(loalist.get(position).getRequestTypeDetail01(), holder.request);
//            if (loalist.get(position).getStatus().equals("PENDING") || (loalist.get(position).getStatus().equals("DISAPPROVED"))) {
//                holder.tv_approval_no.setVisibility(View.GONE);
//            } else {
//                setData(loalist.get(position).getApprovalNo() + "  ", holder.tv_approval_no);
//            }
//            try {
//
//                if (loalist.get(position).getRequestType().equals("BASIC TEST")) {
//                    holder.diagnosis.setVisibility(View.GONE);
//                    holder.request.setVisibility(View.GONE);
//
//                }
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//            if (loalist.get(position).getRequestDatetime() != null) {
//                setData(DateConverter.convertDatetoyyyyMMdd_(loalist.get(position).getRequestDatetime()), holder.date_test);
//            }
//            setData(StatusChecker.setStatusHeader(loalist.get(position).getRequestType(), loalist.get(position).getStatus(),
//                    loalist.get(position).getRequestDatetime()), holder.status);
//
//        } else
        if (hold instanceof Holder) {
            final Holder holder = (LoaReqAdapter.Holder) hold;
            if (loaListCardRetrieves.get(position).getServiceType().equals("BASIC TEST")) {
                basictestDisplay(position, holder);
            } else if (loaListCardRetrieves.get(position).getServiceType().equals("OTHER TEST")) {
                othertestDisplay(position, holder);
            } else if (loaListCardRetrieves.get(position).getServiceType().equalsIgnoreCase("CONSULTATION")
                    || loaListCardRetrieves.get(position).getServiceType().equalsIgnoreCase("MATERNITY CONSULTATION")) {
                consultationDisplay(position, holder);
            } else if (loaListCardRetrieves.get(position).getServiceType().equals("PROCEDURE")) {
                procedureDisplay(position, holder);
            }

//            {


//                setData(loalist.get(position).getRequestType().equalsIgnoreCase(("PROCEDURE")) ? "CLINIC PROCEDURE DONE IN DOCTOR'S CLINIC" : loalist.get(position).getRequestType(), holder.service_type);
//
//                holder.doc.setVisibility(View.GONE);
//
//                holder.spec.setVisibility(View.GONE);
//
//                if (loalist.get(position).getStatus().equals("PENDING") || (loalist.get(position).getStatus().equals("DISAPPROVED"))) {
//                    holder.tv_approval_no.setVisibility(View.GONE);
//                } else {
//                    holder.tv_approval_no.setVisibility(View.VISIBLE);
//                    setData(loalist.get(position).getApprovalNo() + "  ", holder.tv_approval_no);
//                }
//
//                if (loalist.get(position).getRequestType().equals("BASIC TEST")) {
//                    holder.diagnosis.setVisibility(View.GONE);
//                }
//            }
            if (loaListCardRetrieves.get(position).getRequestDateTime() != null) {
                setData(DateConverter.convertDatetoyyyyMMdd_(loaListCardRetrieves.get(position).getRequestDateTime()), holder.request);
            }
//
            setData(StatusChecker.setStatusHeader(loaListCardRetrieves.get(position).getServiceType(), loaListCardRetrieves.get(position).getLine4(),
                    loaListCardRetrieves.get(position).getRequestDateTime()), holder.status);
            setStatusColor(holder.ll_status, holder.cv_row_loa_req, holder.status, loaListCardRetrieves.get(position).getLine4());
        }
    }


    private void consultationDisplay(int position, LoaReqAdapter.Holder holder) {
        setData(loaListCardRetrieves.get(position).getServiceType(), holder.service_type);
        setData(loaListCardRetrieves.get(position).getLine1(), holder.doc);
        holder.doc.setVisibility(View.VISIBLE);
        setData(loaListCardRetrieves.get(position).getLine2(), holder.spec);
        holder.spec.setVisibility(View.VISIBLE);
        setData(loaListCardRetrieves.get(position).getApprovalNos() + "  ", holder.tv_approval_no);

        if (loaListCardRetrieves.get(position).getLine4().equals("PENDING")
                || (loaListCardRetrieves.get(position).getLine4().equals("DISAPPROVED"))) {
            holder.tv_approval_no.setVisibility(View.GONE);
        } else {
            holder.tv_approval_no.setVisibility(View.VISIBLE);
            setData(loaListCardRetrieves.get(position).getApprovalNos() + "  ", holder.tv_approval_no);
        }
    }

    //
    private void basictestDisplay(int position, LoaReqAdapter.Holder holder) {
        setData(loaListCardRetrieves.get(position).getServiceType(), holder.service_type);
        holder.diagnosis.setVisibility(View.GONE);

        setData(loaListCardRetrieves.get(position).getLine1(), holder.costCenter);
        holder.costCenter.setVisibility(View.VISIBLE);

        setData(loaListCardRetrieves.get(position).getLine2(), holder.test_basic_test);
        holder.test_basic_test.setVisibility(View.VISIBLE);

        if (loaListCardRetrieves.get(position).getLine4().equals("PENDING")
                || (loaListCardRetrieves.get(position).getLine4().equals("DISAPPROVED"))) {
            holder.tv_approval_no.setVisibility(View.GONE);
        } else {
            holder.tv_approval_no.setVisibility(View.VISIBLE);
            setData(loaListCardRetrieves.get(position).getApprovalNos() + "  ", holder.tv_approval_no);
        }
//
    }

    //
    private void othertestDisplay(int position, LoaReqAdapter.Holder holder) {
        setData(loaListCardRetrieves.get(position).getServiceType(), holder.service_type);
        setData(loaListCardRetrieves.get(position).getLine1(), holder.costCenter);
        holder.costCenter.setVisibility(View.VISIBLE);

        setData(loaListCardRetrieves.get(position).getLine2(), holder.test_basic_test);
        holder.test_basic_test.setVisibility(View.VISIBLE);
        holder.tv_approval_no.setVisibility(View.VISIBLE);
        setData(loaListCardRetrieves.get(position).getApprovalNos() + "  ", holder.tv_approval_no);

        if (loaListCardRetrieves.get(position).getLine4().equals("PENDING")
                || (loaListCardRetrieves.get(position).getLine4().equals("DISAPPROVED"))) {
            holder.tv_approval_no.setVisibility(View.GONE);
        } else {
            holder.tv_approval_no.setVisibility(View.VISIBLE);
            setData(loaListCardRetrieves.get(position).getApprovalNos() + "  ", holder.tv_approval_no);
        }
    }

    private void procedureDisplay(int position, LoaReqAdapter.Holder holder) {
        setData(loaListCardRetrieves.get(position).getServiceType(), holder.service_type);
        setData(loaListCardRetrieves.get(position).getLine1(), holder.diagnosis);
        holder.diagnosis.setVisibility(View.VISIBLE);
        setData(loaListCardRetrieves.get(position).getLine2(), holder.test_basic_test);
        holder.test_basic_test.setVisibility(View.VISIBLE);
        setData(loaListCardRetrieves.get(position).getApprovalNos(), holder.tv_approval_no);

        if (loaListCardRetrieves.get(position).getLine4().equals("PENDING")
                || (loaListCardRetrieves.get(position).getLine4().equals("DISAPPROVED"))) {
            holder.tv_approval_no.setVisibility(View.GONE);
        } else {
            holder.tv_approval_no.setVisibility(View.VISIBLE);
            setData(loaListCardRetrieves.get(position).getApprovalNos() + "  ", holder.tv_approval_no);
        }
    }

    private void setData(String data, TextView textView) {

        if (data != null && data.equals("")) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setText(data);
        }
    }

    public void setStatusColor(LinearLayout linearLayout, CardView cardView, TextView textView, String requestStatus) {

        if (requestStatus.equalsIgnoreCase("PENDING")) {
//            textView.setTextColor(context.getColor(R.color.status_pending));
//            cardView.setBackgroundResource(R.drawable.cv_status_pending);
            linearLayout.setBackgroundColor(context.getColor(R.color.status_pending));
        } else if (requestStatus.equalsIgnoreCase("APPROVED")) {
//            textView.setTextColor(context.getColor(R.color.status_approve));
//            cardView.setBackgroundResource(R.drawable.cv_status_approve);
            linearLayout.setBackgroundColor(context.getColor(R.color.status_approve));
        } else if (requestStatus.equalsIgnoreCase("DISAPPROVED")) {
//            textView.setTextColor(context.getColor(R.color.status_disapprove));
//            cardView.setBackgroundResource(R.drawable.cv_status_dispapprove);
            linearLayout.setBackgroundColor(context.getColor(R.color.status_disapprove));
        }
    }

    @Override
    public int getItemViewType(int position) {


        return 0;
//        if (null != loalist.get(position).getDoctorName())
//            return WITH_DOCTOR;
//        else {
//            return WITHOUT_DOCTOR;
//        }
    }


    @Override
    public int getItemCount() {
        return loaListCardRetrieves.size();
    }


    public class Holder extends RecyclerView.ViewHolder {


        //TODO Please Add the request details 1 2 3
        @BindView(R.id.service_type)
        TextView service_type;
        @BindView(R.id.doc)
        TextView doc;
        @BindView(R.id.spec)
        TextView spec;
        @BindView(R.id.request)
        TextView request;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.cv_row_loa_req)
        CardView cv_row_loa_req;
        @BindView(R.id.tv_approval_no)
        TextView tv_approval_no;
        @BindView(R.id.diagnosis)
        TextView diagnosis;
        @BindView(R.id.costCenter)
        TextView costCenter;
        @BindView(R.id.test_basic_test)
        TextView test_basic_test;
        @BindView(R.id.ll_status)
        LinearLayout ll_status;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            cv_row_loa_req.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onLoaRowSelected(loaListCardRetrieves.get(getAdapterPosition()).getLoaListIndex());
                }
            });
        }
    }


    public class HolderNonDoctor extends RecyclerView.ViewHolder {


        //TODO Please Add the request details 1 2 3
        @BindView(R.id.service_type)
        TextView service_type;
        @BindView(R.id.date_test)
        TextView date_test;
        @BindView(R.id.request)
        TextView request;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.ll_view)
        LinearLayout ll_view;
        @BindView(R.id.tv_approval_no)
        TextView tv_approval_no;
        @BindView(R.id.diagnosis)
        TextView diagnosis;


        public HolderNonDoctor(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


            ll_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onLoaRowSelected(getAdapterPosition());
                }
            });
        }
    }

    public interface LoaCLickListener {
        void onLoaRowSelected(int position);
    }
}
