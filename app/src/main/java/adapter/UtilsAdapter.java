package adapter;

import android.content.Context;
import  coord.medicard.com.medicardcoordinator.R;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import model.Dependents;
import model.Utilization;

/**
 * Created by mpx-pawpaw on 11/18/16.
 */

public class UtilsAdapter extends RecyclerView.Adapter<UtilsAdapter.ViewHolder> {


    private int TYPE_HEADER = 0;
    private int TYPE_ITEM = 1;
    Context context;

    ArrayList<Utilization> utilizations = new ArrayList<>();

    public UtilsAdapter(Context context, ArrayList<Utilization> utilizations) {
        this.context = context;
        this.utilizations = utilizations;
    }

    @Override
    public UtilsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_userlinks, parent, false);
        return new UtilsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UtilsAdapter.ViewHolder holder, int position) {
        holder.tv_name.setText(utilizations.get(position).getHospitalName());
        holder.tv_memCode.setText(utilizations.get(position).getControlCode());

    }

    @Override
    public int getItemCount() {
        return utilizations.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_memCode, tv_name;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_memCode = (TextView) itemView.findViewById(R.id.tv_memberCode);

        }
    }
}
