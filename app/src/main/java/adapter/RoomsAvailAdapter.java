package adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.Navigator.NavigatorActivity;
import coord.medicard.com.medicardcoordinator.Navigator.NavigatorRetrieve;
import coord.medicard.com.medicardcoordinator.R;
import fragment.Avail.AvailServices;
import model.Rooms;
import utilities.Constant;
import utilities.DataBaseHandler;
import utilities.SharedPref;

/**
 * Created by mpx-pawpaw on 6/14/17.
 */

public class RoomsAvailAdapter extends RecyclerView.Adapter<RoomsAvailAdapter.Holder> {

    private ArrayList<Rooms> arrayList;
    private Context context;
    private DataBaseHandler dataBaseHandler;
    private NavigatorRetrieve.NavigatorInterface callback;

    public RoomsAvailAdapter(Context context, ArrayList<Rooms> arrayList, DataBaseHandler databaseH, final NavigatorRetrieve.NavigatorInterface callback) {
        this.context = context;
        this.arrayList = arrayList;
        this.dataBaseHandler = databaseH;
        this.callback = callback;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_doctors, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        holder.name.setText(arrayList.get(position).getPlanDesc() +
                getNotNull(arrayList.get(position).getClassCategory()));
        holder.position.setVisibility(View.GONE);


        holder.cv_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SharedPref.getStringValue(SharedPref.USER, SharedPref.ROOM_ORIGIN, context).
                        equals(Constant.ADAPTER_PRE_ROOM_PLANS)) {
                    EventBus.getDefault().post(new NavigatorActivity.MessageEvent(Constant.ADAPTER_PRE_ROOM_PLANS,
                            dataBaseHandler.getRoomPlan(arrayList.get(position).getPlanCode())));
                                callback.roomPlanCallback(arrayList.get(position).getPlanCode());
                } else {
                    EventBus.getDefault().post(new AvailServices.MessageEvent(Constant.ADAPTER_ROOM_PLANS,
                            dataBaseHandler.getRoomPlan(arrayList.get(position).getPlanCode())));
                              callback.roomPlanCallback(arrayList.get(position).getPlanCode());

                }


            }
        });
    }

    private String getNotNull(String classCategory) {

        try {
            if (classCategory != null) {
                if (classCategory.equals("null")) {
                    return "";
                } else {
                    return "\n" + classCategory;
                }
            } else {
                return "";
            }
        } catch (Exception e) {
            return "";
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView name, position;
        CardView cv_item;

        public Holder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tv_name);
            position = (TextView) itemView.findViewById(R.id.tv_position);
            cv_item = (CardView) itemView.findViewById(R.id.cv_item);


        }
    }
}
