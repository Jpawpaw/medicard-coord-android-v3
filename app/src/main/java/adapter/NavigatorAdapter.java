package adapter;

import android.content.Context;

import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.R;

/**
 * Created by window on 11/6/2016.
 */

public class NavigatorAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    Context context;
    ArrayList<String> arrayList = new ArrayList<>();
    int selected;

    public NavigatorAdapter(ArrayList<String> arrayList, Context context, int selected) {
        this.arrayList = arrayList;
        this.context = context;
        this.selected = selected;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {


        ViewHolder holder;

        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = LayoutInflater.from(context).inflate(R.layout.row_navigator, viewGroup, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        holder.text.setText(arrayList.get(i));
        if (i == 0) {
            holder.image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.account));
        } else if (i == 1) {
            holder.image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.availed));
        } else if (i == 2) {
            holder.image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.availed));
        } else if (i == 3) {
            holder.image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.patient));
        } else if (i == 4) {
            holder.image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.claims));
        } else if (i == 5) {
            holder.image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.settings));
        }else if (i == 6) {
            holder.image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.logout));
        }

        Log.d("POSITION" , selected + "");



        return convertView;

    }

    static class ViewHolder {

        public TextView text;
        public ImageView image;

        public ViewHolder(View v) {
            text = (TextView) v.findViewById(R.id.textView);
            image = (ImageView) v.findViewById(R.id.imageView);
        }
    }

}
