package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.R;
import model.DiagnosisList;
import model.DoctorModelInsertion;
import services.ItemClickListener;
import utilities.DataBaseHandler;

/**
 * Created by IPC on 12/5/2017.
 */

public class InpatientDiagnosisSelectAdapter_Availservice extends RecyclerView.Adapter<InpatientDiagnosisSelectAdapter_Availservice.Holder> {

    DataBaseHandler dataBaseHandler;
    private ItemClickListener clickListener;
    private static Context context;
    private static ArrayList<DiagnosisList> arrayList = new ArrayList<>();
    DecimalFormat formatter = new DecimalFormat("#,###.##");
    double amount = 0;
    TextView tv_nav_notification;
    Boolean isCheck = false;
    RecyclerView rv_inpatient_diagnosis;
    Button btn_inpatient_diagnosis;
    TextView tv_inpatient_diagnosis;

    public InpatientDiagnosisSelectAdapter_Availservice(DataBaseHandler dataBaseHandler,Context context, ArrayList<DiagnosisList> arrayList, RecyclerView rv_inpatient_diagnosis, Button btn_inpatient_diagnosis,TextView tv_inpatient_diagnosis) {
        this.context = context;
        this.arrayList = arrayList;
        this.tv_nav_notification = tv_nav_notification;
        this.rv_inpatient_diagnosis = rv_inpatient_diagnosis;
        this.btn_inpatient_diagnosis = btn_inpatient_diagnosis;
        this.tv_inpatient_diagnosis = tv_inpatient_diagnosis;
        this.dataBaseHandler = dataBaseHandler;


    }



    @Override
    public InpatientDiagnosisSelectAdapter_Availservice.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_inpatient_doc_nav, parent, false);
        return new InpatientDiagnosisSelectAdapter_Availservice.Holder(view);
    }

    @Override
    public void onBindViewHolder(final InpatientDiagnosisSelectAdapter_Availservice.Holder holder, final int position) {
        DiagnosisList diagnosis = arrayList.get(position);

        holder.name.setText(diagnosis.getDiagDesc());
        holder.position.setText(diagnosis.getIcd10Code());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }


    public class Holder extends RecyclerView.ViewHolder {

        TextView name, position;
        RelativeLayout cv_item;
        ImageButton ib_delete;
        CheckBox checkBox;

        public Holder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.tv_name);
            position = (TextView) itemView.findViewById(R.id.tv_position);
            cv_item = (RelativeLayout) itemView.findViewById(R.id.cv_item);
            ib_delete = (ImageButton) itemView.findViewById(R.id.ib_delete);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);
            checkBox.setVisibility(View.GONE);
            ib_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dataBaseHandler.setDataInPatientDiagnosistoFALSE(arrayList.get(getAdapterPosition()).getDiagCode());
                    arrayList.remove(getAdapterPosition());

                    if (arrayList.size() == 0) {
                        btn_inpatient_diagnosis.setVisibility(View.GONE);
                        tv_inpatient_diagnosis.setVisibility(View.VISIBLE);
                        rv_inpatient_diagnosis.setVisibility(View.GONE);
                    } else {
                        tv_inpatient_diagnosis.setVisibility(View.GONE);
                        rv_inpatient_diagnosis.setVisibility(View.VISIBLE);
                        btn_inpatient_diagnosis.setVisibility(View.VISIBLE);
                    }

                    InpatientDiagnosisSelectAdapter_Availservice.this.notifyDataSetChanged();
                }
            });

        }


    }
}