package adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.R;
import fragment.Avail.AvailServices;
import fragment.InPatient.InPatientAdmitted;
import model.DoctorModelMultipleSelection;
import model.Test.Data;
import utilities.Constant;
import utilities.DataBaseHandler;

/**
 * Created by mpx-pawpaw on 6/1/17.
 */

public class DoctorMultipleSelectionAdapter extends RecyclerView.Adapter<DoctorMultipleSelectionAdapter.Holder> {


    private Context context;
    private ArrayList<DoctorModelMultipleSelection> arrayList;
    private DataBaseHandler dataBaseHandler;

    public DoctorMultipleSelectionAdapter(Context context, ArrayList<DoctorModelMultipleSelection> arrayList,
                                          DataBaseHandler dataBaseHandler) {
        this.context = context;
        this.arrayList = arrayList;
        this.dataBaseHandler = dataBaseHandler;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_other_proc, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {


        holder.name.setText(arrayList.get(position).getDocLname() + ", " + arrayList.get(position).getDocFname());
        holder.position.setText(arrayList.get(position).getSpecDesc());

        dataBaseHandler = new DataBaseHandler(context);
        holder.checkBox.setChecked(dataBaseHandler.getDataDoctorFromTrue(arrayList.get(position).getDoctorCode()));

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView name, position;
        CardView cv_item;
        CheckBox checkBox;

        public Holder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.tv_name);
            position = (TextView) itemView.findViewById(R.id.tv_position);
            cv_item = (CardView) itemView.findViewById(R.id.cv_item);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);


            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // TODO ADD METHOD TO RETRIEVE ALL SELECTED DOCTORS
                    //TODO ADD INPATIENTADMITTED RESPONSIVE LAYOUT FOR SELECTING DATA
                    if (checkBox.isChecked()) {

                        dataBaseHandler.setDataDoctorToTrue(arrayList.get(getAdapterPosition()).getDoctorCode());

                        EventBus.getDefault().post(new InPatientAdmitted.MessageEvent(Constant.ADAPTER_DOCTORS_IN_PATIENT,
                                dataBaseHandler.retrieveSelectedDoctors()));


                    } else {
                        dataBaseHandler.setDataDoctorToFalse(arrayList.get(getAdapterPosition()).getDoctorCode());
                        EventBus.getDefault().post(new InPatientAdmitted.MessageEvent(Constant.ADAPTER_DOCTORS_IN_PATIENT,
                                dataBaseHandler.retrieveSelectedDoctors()));
                    }

                }
            });

        }
    }
}
