package adapter;

import android.content.Context;
import  coord.medicard.com.medicardcoordinator.R;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import fragment.Avail.AvailServices;
import model.DiagnosisList;
import services.ItemClickListener;
import utilities.DataBaseHandler;

/**
 * Created by mpx-pawpaw on 11/22/16.
 */

public class DiagnosisAdapter extends RecyclerView.Adapter<DiagnosisAdapter.Holder> {
    private ItemClickListener clickListener;
    ArrayList<DiagnosisList> arrayList = new ArrayList<>();
    Context context;
    DataBaseHandler dataBaseHandler;
    TextView tv_nav_notification;

    public DiagnosisAdapter(Context context, ArrayList<DiagnosisList> arrayList, TextView tv_nav_notification) {
        this.arrayList = arrayList;
        this.context = context;
        this.tv_nav_notification = tv_nav_notification;

    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_other_proc, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        holder.name.setText(arrayList.get(position).getDiagDesc());
        holder.position.setText(arrayList.get(position).getIcd10Code());

        dataBaseHandler = new DataBaseHandler(context);
        holder.checkBox.setChecked(dataBaseHandler.getDataDiagnosisFromTrue(arrayList.get(position).getDiagCode()));



        if (getItemCount() == 0){
            tv_nav_notification.setText("PLEASE CALL");
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView name, position;
        CardView cv_item;
        CheckBox checkBox;

        public Holder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.tv_name);
            position = (TextView) itemView.findViewById(R.id.tv_position);
            cv_item = (CardView) itemView.findViewById(R.id.cv_item);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);


            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dataBaseHandler = new DataBaseHandler(context);

                    if (checkBox.isChecked()) {
                        dataBaseHandler.setDataDiagnosetoTRUE(arrayList.get(getAdapterPosition()).getDiagCode());


                        EventBus.getDefault().post(new AvailServices.MessageEvent("6"
                                , dataBaseHandler.retrieveSelectedDiagnosis(), ""));


                    } else {
                        dataBaseHandler.setDataDiagnosetoFALSE(arrayList.get(getAdapterPosition()).getDiagCode());


                        EventBus.getDefault().post(new AvailServices.MessageEvent("6"
                                , dataBaseHandler.retrieveSelectedDiagnosis(), ""));

                    }

                }
            });
        }
    }
}
