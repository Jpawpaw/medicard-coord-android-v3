package coord.medicard.com.medicardcoordinator.MemberSearch;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.logging.ErrorManager;

import coord.medicard.com.medicardcoordinator.CoordinatorMenu.CoordinatorMenuActivity;
import coord.medicard.com.medicardcoordinator.MemberVerificationAcitivty;
import model.Dependents;
import model.Utilization;
import model.VerifyMember;
import utilities.RemarksFilter;
import utilities.SearchToCoordinatorMenu;
import utilities.SetAvailableServices;
import utilities.SharedPref;

/**
 * Created by mpx-pawpaw on 6/20/17.
 */

public class NavigateSearchData {

    public static void gotoMemberVerification(Context context, VerifyMember memberInfo, NavigateToActivity callback, String DateofBirth) {

        boolean isEr = false;
        boolean isInPatient = false;

        try {
            isInPatient = memberInfo.isAdmittedIp();
        } catch (Exception e) {
            isInPatient = false;
        }

        try {
            isEr = memberInfo.isAdmittedEr();
        } catch (Exception e) {
            isEr = false;
        }


        String inPatientCode;
        try {
            if (isInPatient)
                inPatientCode = memberInfo.getInpatientRequestCode();
            else
                inPatientCode = "";
        } catch (Exception error) {
            inPatientCode = "";

        }

        String erCode;
        try {
            if (isEr)
                erCode = memberInfo.getErRequestCode();
            else
                erCode = "";
        } catch (Exception error) {
            erCode = "";

        }

        SharedPref.setStringValue(SharedPref.USER, SharedPref.FULL_NAME, memberInfo.getMemberInfo().getMEM_NAME(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.COMPANY_NAME, memberInfo.getMemberInfo().getACCOUNT_NAME(),context);
        String[] age;
        age = memberInfo.getMemberInfo().getAGE().split("\\.");
        SharedPref.setStringValue(SharedPref.USER, SharedPref.AGE, age[0], context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.GENDER, memberInfo.getMemberInfo().getMEM_SEX().equals("0")? "FEMALE": "MALE", context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.ID, memberInfo.getMemberInfo().getPRIN_CODE(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.MEMBER_STATUS, memberInfo.getMemberInfo().getMem_OStat_Code(),context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.USERNAME, memberInfo.getVerifiedMember().getAPP_USERNAME(),context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.ISKIPPED, memberInfo.getVerifiedMember().getIS_SKIPPED(),context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.MEMBERCODE, memberInfo.getMemberInfo().getPRIN_CODE(),context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.TYPE, memberInfo.getMemberInfo().getMEM_TYPE(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.EFFECTIVE, memberInfo.getMemberInfo().getEFF_DATE(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.VALID, memberInfo.getMemberInfo().getVAL_DATE(), context);
        SharedPref.setIntegerValue(SharedPref.USER, SharedPref.LIMITS, memberInfo.getMemberInfo().getDD_Reg_Limits(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.MEMBER_PLAN, memberInfo.getMemberInfo().getPlan_Desc(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.BIRTHDAY, memberInfo.getMemberInfo().getBDAY(),context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.HAS_MATERNITY, memberInfo.getHasMaternity(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.USERNAME, memberInfo.getVerifiedMember().getAPP_USERNAME(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.REQUEST_CODE_INPATIENT, memberInfo.getInpatientRequestCode(),context);
        SharedPref.setBooleanValue(SharedPref.USER, SharedPref.ISINPATIENT, memberInfo.isAdmittedIp(),context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.REQUEST_CODE_ER, memberInfo.getErRequestCode(),context);
        SharedPref.setBooleanValue(SharedPref.USER, SharedPref.ISINER, memberInfo.isAdmittedEr(),context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.REMARKS, RemarksFilter.filterRemarks(memberInfo.getMemberInfo().getID_REM(),
                memberInfo.getMemberInfo().getID_REM2(),
                memberInfo.getMemberInfo().getID_REM3(),
                memberInfo.getMemberInfo().getID_REM4(),
                memberInfo.getMemberInfo().getID_REM5(),
                memberInfo.getMemberInfo().getID_REM6(),
                memberInfo.getMemberInfo().getID_REM7()), context);
        SharedPref.util = memberInfo.getUtilization();
        SharedPref.dependents = memberInfo.getDependents();


        Intent gotoValidate = new Intent(context, MemberVerificationAcitivty.class);
//        gotoValidate.putExtra(SearchToCoordinatorMenu.NAME, memberInfo.getMemberInfo().getMEM_NAME());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.COMPANY, memberInfo.getMemberInfo().getACCOUNT_NAME());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.AGE, memberInfo.getMemberInfo().getAGE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.GENDER, memberInfo.getMemberInfo().getMEM_SEX());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.ID, memberInfo.getMemberInfo().getPRIN_CODE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.STATUS, memberInfo.getMemberInfo().getMem_OStat_Code());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.USERNAME, memberInfo.getVerifiedMember().getAPP_USERNAME());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.ISKIPPED, memberInfo.getVerifiedMember().getIS_SKIPPED());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.ID, memberInfo.getMemberInfo().getPRIN_CODE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.STATUS, memberInfo.getMemberInfo().getMem_OStat_Code());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.MEMCODE, memberInfo.getMemberInfo().getACCOUNT_CODE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.TYPE, memberInfo.getMemberInfo().getMEM_TYPE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.EFFECTIVE, memberInfo.getMemberInfo().getEFF_DATE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.VALID, memberInfo.getMemberInfo().getVAL_DATE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.DD_LIMIT, memberInfo.getMemberInfo().getDD_Reg_Limits());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.PLAN, memberInfo.getMemberInfo().getPlan_Desc());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.BDAY, memberInfo.getMemberInfo().getBDAY());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.BDAY_RAW, DateofBirth);
//        gotoValidate.putExtra(SearchToCoordinatorMenu.VALIDITYDATE, memberInfo.getMemberInfo().getVAL_DATE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.EFFECTIVITYDATE, memberInfo.getMemberInfo().getEFF_DATE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.REQUEST_CODE_IN_PATIENT, inPatientCode);
//        gotoValidate.putExtra(SearchToCoordinatorMenu.IS_IN_PATIENT, isInPatient);
//        gotoValidate.putExtra(SearchToCoordinatorMenu.REQUEST_CODE_ER, erCode);
//        gotoValidate.putExtra(SearchToCoordinatorMenu.IS_ER, isEr);
//        gotoValidate.putExtra(SearchToCoordinatorMenu.OTHER, RemarksFilter.filterRemarks(memberInfo.getMemberInfo().getID_REM(),
//                memberInfo.getMemberInfo().getID_REM2(),
//                memberInfo.getMemberInfo().getID_REM3(),
//                memberInfo.getMemberInfo().getID_REM4(),
//                memberInfo.getMemberInfo().getID_REM5(),
//                memberInfo.getMemberInfo().getID_REM6(),
//                memberInfo.getMemberInfo().getID_REM7()).trim());


//        ArrayList<Utilization> utils = new ArrayList<>();
//        utils.addAll(memberInfo.getUtilization());
//        gotoValidate.putParcelableArrayListExtra(SearchToCoordinatorMenu.UTILIZATION, utils);

//        ArrayList<Dependents> dependents = new ArrayList<>();
//        dependents.addAll(memberInfo.getDependents());
//        gotoValidate.putParcelableArrayListExtra(SearchToCoordinatorMenu.DEPENDENTS, dependents);


        new SetAvailableServices().setServiceType(memberInfo.getServiceType(), context);
        callback.onActivitySelect(gotoValidate);
        Log.d("IS_ADMITTED_IP", String.valueOf(isInPatient));
//        Log.d("IS_ADMITTED_IP", inPatientCode);
        Log.d("IS_ADMITTED_ER", String.valueOf(isEr));
//        Log.d("IS_ADMITTED_ER", erCode);

    }


    public static void gotoCoordinatorLayout(VerifyMember memberInfo, NavigateToActivity callback, String birthday, Context context) {

        boolean isInPatient = false;
        boolean isER = false;

        try {
            isInPatient = memberInfo.isAdmittedIp();
        } catch (Exception e) {
            isInPatient = false;
        }

        try{
            isER = memberInfo.isAdmittedEr();
        }catch (Exception e){
            e.printStackTrace();
        }


        String inPatientCode;
        try {
            inPatientCode = memberInfo.getInpatientRequestCode();
        } catch (Exception error) {
            inPatientCode = "";
        }

        String erCode;
        try {
            erCode = memberInfo.getErRequestCode();
        } catch (Exception error) {
            erCode = "";
        }


        //used to view loa in consultation
        SharedPref.setStringValue(SharedPref.USER, SharedPref.FULL_NAME, memberInfo.getMemberInfo().getMEM_NAME(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.GENDER, memberInfo.getMemberInfo().getMEM_SEX().equals("0")? "FEMALE": "MALE", context);
        String[] age;
        age = memberInfo.getMemberInfo().getAGE().split("\\.");
        SharedPref.setStringValue(SharedPref.USER, SharedPref.AGE, age[0], context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.ID, memberInfo.getMemberInfo().getPRIN_CODE(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.MEMBER_STATUS, memberInfo.getMemberInfo().getMem_OStat_Code(),context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.HAS_MATERNITY, memberInfo.getHasMaternity(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.EFFECTIVE, memberInfo.getMemberInfo().getEFF_DATE(), context);
        SharedPref.setIntegerValue(SharedPref.USER, SharedPref.LIMITS, memberInfo.getMemberInfo().getDD_Reg_Limits(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.MEMBER_PLAN, memberInfo.getMemberInfo().getPlan_Desc(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.TYPE, memberInfo.getMemberInfo().getMEM_TYPE(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.VALID, memberInfo.getMemberInfo().getVAL_DATE(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.REMARKS, RemarksFilter.filterRemarks(memberInfo.getMemberInfo().getID_REM(),
                memberInfo.getMemberInfo().getID_REM2(),
                memberInfo.getMemberInfo().getID_REM3(),
                memberInfo.getMemberInfo().getID_REM4(),
                memberInfo.getMemberInfo().getID_REM5(),
                memberInfo.getMemberInfo().getID_REM6(),
                memberInfo.getMemberInfo().getID_REM7()), context);
        SharedPref.util = memberInfo.getUtilization();
        SharedPref.dependents = memberInfo.getDependents();
        SharedPref.setBooleanValue(SharedPref.USER, SharedPref.ISINPATIENT, memberInfo.isAdmittedIp(),context);
        SharedPref.setBooleanValue(SharedPref.USER, SharedPref.ISINER, memberInfo.isAdmittedEr(),context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.REQUEST_CODE_ER, memberInfo.getErRequestCode(),context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.REQUEST_CODE_INPATIENT, memberInfo.getInpatientRequestCode(),context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.COMPANY_NAME, memberInfo.getMemberInfo().getACCOUNT_NAME(),context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.BIRTHDAY, memberInfo.getMemberInfo().getBDAY(),context);

        Intent gotoMenu = new Intent(context, CoordinatorMenuActivity.class);
//
//        gotoMenu.putExtra(SearchToCoordinatorMenu.NAME, memberInfo.getMemberInfo().getMEM_NAME());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.ID, memberInfo.getMemberInfo().getPRIN_CODE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.STATUS, memberInfo.getMemberInfo().getMem_OStat_Code());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.MEMCODE, memberInfo.getMemberInfo().getACCOUNT_CODE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.TYPE, memberInfo.getMemberInfo().getMEM_TYPE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.EFFECTIVE, memberInfo.getMemberInfo().getEFF_DATE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.VALID, memberInfo.getMemberInfo().getVAL_DATE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.DD_LIMIT, memberInfo.getMemberInfo().getDD_Reg_Limits());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.PLAN, memberInfo.getMemberInfo().getPlan_Desc());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.COMPANY, memberInfo.getMemberInfo().getACCOUNT_NAME());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.AGE, memberInfo.getMemberInfo().getAGE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.GENDER, memberInfo.getMemberInfo().getMEM_SEX());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.BDAY, memberInfo.getMemberInfo().getBDAY());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.VALIDITYDATE, memberInfo.getMemberInfo().getVAL_DATE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.EFFECTIVITYDATE, memberInfo.getMemberInfo().getEFF_DATE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.BDAY_RAW, birthday);
//        gotoMenu.putExtra(SearchToCoordinatorMenu.REQUEST_CODE_IN_PATIENT, inPatientCode);
//        gotoMenu.putExtra(SearchToCoordinatorMenu.IS_IN_PATIENT, isInPatient);
//        gotoMenu.putExtra(SearchToCoordinatorMenu.REQUEST_CODE_ER, erCode);
//        gotoMenu.putExtra(SearchToCoordinatorMenu.IS_ER, isER);
//
//        gotoMenu.putExtra(SearchToCoordinatorMenu.OTHER, RemarksFilter.filterRemarks
//                (memberInfo.getMemberInfo().getID_REM(),
//                        memberInfo.getMemberInfo().getID_REM2(),
//                        memberInfo.getMemberInfo().getID_REM3(),
//                        memberInfo.getMemberInfo().getID_REM4(),
//                        memberInfo.getMemberInfo().getID_REM5(),
//                        memberInfo.getMemberInfo().getID_REM6(),
//                        memberInfo.getMemberInfo().getID_REM7()).trim());

//        ArrayList<Utilization> utils = new ArrayList<>();
//        utils.addAll(memberInfo.getUtilization());
//        gotoMenu.putParcelableArrayListExtra(SearchToCoordinatorMenu.UTILIZATION, utils);
//
//
//        ArrayList<Dependents> dependents = new ArrayList<>();
//        dependents.addAll(memberInfo.getDependents());
//        gotoMenu.putParcelableArrayListExtra(SearchToCoordinatorMenu.DEPENDENTS, dependents);
//

        new SetAvailableServices().setServiceType(memberInfo.getServiceType(), context);
        callback.onActivitySelect(gotoMenu);

//        Log.d("IS_ADMITTED_INPATIENT", String.valueOf(isInPatient));
//       Log.d("IS_ADMITTED_INPATIENT", inPatientCode);
//        Log.d("IS_ADMITTED_ER", String.valueOf(isER));
//        Log.d("IS_ADMITTED_ER", erCode);
        System.out.println("IS_ADMITTED_ER" +  String.valueOf(isER));
    }


    public interface NavigateToActivity {
        void onActivitySelect(Intent gotoValidate);
    }


}
