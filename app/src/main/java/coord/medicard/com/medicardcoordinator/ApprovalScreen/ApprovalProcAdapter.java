package coord.medicard.com.medicardcoordinator.ApprovalScreen;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import coord.medicard.com.medicardcoordinator.R;
import model.DiagnosisClinicProcedures;
import model.Test.DiagnosisProcedures;
import utilities.DataBaseHandler;
import utilities.QrCodeCreator;

/**
 * Created by IPC_Server on 8/8/2017.
 */

public class ApprovalProcAdapter extends RecyclerView.Adapter<ApprovalProcAdapter.Holder> {
        private Context context;
        ArrayList<DiagnosisClinicProcedures> arrayListProcedure = new ArrayList<>();
        DataBaseHandler dataBaseHandler;



    public ApprovalProcAdapter(Context context, ArrayList<DiagnosisClinicProcedures> arrayList) {
            this.context = context;
            this.arrayListProcedure = arrayList;
            dataBaseHandler = new DataBaseHandler(context);

        }




        @Override
        public ApprovalProcAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(context).inflate(R.layout.row_approval, parent, false);
            return new ApprovalProcAdapter.Holder(view);
        }

        @Override
        public void onBindViewHolder(ApprovalProcAdapter.Holder holder, int position) {

                holder.img_approval.setImageBitmap(QrCodeCreator.getBitmapFromString(arrayListProcedure.get(position).getMaceRequestProcedure().getApprovalNo()));
                System.out.println("Q1" + arrayListProcedure.get(0).getProcedureCode());
                System.out.println("Q2" + arrayListProcedure.get(0).getAmount());
                System.out.println("Q3" + arrayListProcedure.get(0).getApprovalNo());
                System.out.println("Q4" + arrayListProcedure.get(0).getAmount());

                holder.tv_name_proc.setText(arrayListProcedure.get(position).getMaceRequestOpProcedure().getProcDesc());
                //List of TestProc(Get Object by ProcedureCode)
                String costcenter = dataBaseHandler.getCostCenter(arrayListProcedure.get(position).getMaceRequestOpProcedure().getProcCode());

                holder.tv_cost_center.setText(costcenter);
                holder.tv_total_num.setText("Price: " + arrayListProcedure.get(position).getMaceRequestProcedure().getTransamount());
                holder.tv_approval_no.setText("Approval Code: " + arrayListProcedure.get(position).getMaceRequestProcedure().getApprovalNo());


        }



        @Override
        public int getItemCount() {
            return arrayListProcedure.size();
        }

        public class Holder extends RecyclerView.ViewHolder {

            @BindView(R.id.img_approval)
            ImageView img_approval;
            @BindView(R.id.tv_cost_center)
            TextView tv_cost_center;
            @BindView(R.id.tv_name_proc)
            TextView tv_name_proc;
            @BindView(R.id.tv_approval_no)
            TextView tv_approval_no;
            @BindView(R.id.tv_total_num)
            TextView tv_total_num;

            public Holder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
}
