package coord.medicard.com.medicardcoordinator.Navigator;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;

import coord.medicard.com.medicardcoordinator.R;
import model.BasicTests;
import model.DiagnosisList;
import model.Hospital;
import model.InPatient;
import model.LoaList;
import model.ProceduresListLocal;
import utilities.APIMultipleCalls.NetworkTest;
import utilities.DateConverter;

/**
 * Created by mpx-pawpaw on 4/11/17.
 */

public class NavigatorRetrieve {
    private int Basic_Test = 1;
    private int OtherTest = 2;
    private int Procedure = 3;

    private Context context;
    private NavigatorInterface callback;

    public NavigatorRetrieve(Context context, NavigatorInterface callback) {
        this.callback = callback;
        this.context = context;
    }

//    public void setConsultVisible(final Button btn_consultation, final boolean b) {
//
//        btn_consultation.animate()
//                .alpha(setAlpha(b))
//                .setDuration(300)
//                .setListener(new AnimatorListenerAdapter() {
//                    @Override
//                    public void onAnimationEnd(Animator animation) {
//                        super.onAnimationEnd(animation);
//                        if (b)
//                            btn_consultation.setVisibility(View.VISIBLE);
//                        else
//                            btn_consultation.setVisibility(View.GONE);
//                    }
//                });
//
//
//    }

    private float setAlpha(boolean b) {
        return (float) ((b) ? 1.00 : 0.00);
    }

    /*
    set LIST ON CONSULTATATION AVAILABLE
    SET BUTTON VISIBLE
     */
    public void setConsultVisible(final LinearLayout btn_consultation, final boolean b) {
        btn_consultation.animate()
                .alpha(setAlpha(b))
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        if (b)
                            btn_consultation.setVisibility(View.VISIBLE);
                        else
                            btn_consultation.setVisibility(View.GONE);
                    }
                });


    }


    public void showConsultationListDialog(ArrayList<LoaList> data, ConsultationAdapter.ConsultationListInterface listcallback) {


        final Dialog dialog;
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_consultation);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);


        ConsultationAdapter adapter;
        adapter = new ConsultationAdapter(data, context, listcallback, dialog);

        RecyclerView rv_list;
        rv_list = (RecyclerView) dialog.findViewById(R.id.rv_list);
        Button btn_cancel;
        btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

        rv_list.setLayoutManager(new LinearLayoutManager(context));
        rv_list.setAdapter(adapter);

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        dialog.show();


        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.50);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = (int) (context.getResources().getDisplayMetrics().heightPixels * 0.80);
        lp.width = width;
        dialog.getWindow().setAttributes(lp);

    }

    // SET PROPERTY OF BUTTON IF LOADING
    public void setConsultVisibleList(LinearLayout btn_consultation_list, TextView tv_text_list, ProgressBar pb_loading_list, boolean b) {

        if (b) {
            btn_consultation_list.setClickable(true);
            tv_text_list.setText("Show Consultation");
            pb_loading_list.setVisibility(View.GONE);
            btn_consultation_list.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        } else {
            btn_consultation_list.setClickable(false);
            tv_text_list.setText("Loading Consultation");
            pb_loading_list.setVisibility(View.VISIBLE);
            btn_consultation_list.setBackgroundColor(ContextCompat.getColor(context, R.color.gray));
        }
    }


    //LOAD CONSULT LIST
    public void getDataList(String ORIGIN,
                            String memberid,
                            NavigatorInterface callback,
                            String serviceType,
                            String subServiceType,
                            String status,
                            String page,
                            String count) {


        NavigatorApiCall.getDataListForConsult(ORIGIN, memberid, callback,
                serviceType, subServiceType, status, page, count);

    }


    public Collection<? extends ProceduresListLocal> convertTestToProcedureListLocal(ArrayList<BasicTests> arrayListBasicTest) {

        ArrayList<ProceduresListLocal> data = new ArrayList<>();

        for (BasicTests proc : arrayListBasicTest) {


            ProceduresListLocal setProc = new ProceduresListLocal();
            setProc.setId(proc.getId());
            setProc.setServiceClassCode(proc.getServiceClassCode());
            setProc.setProcedureCode(proc.getProcedureCode());
            setProc.setProcedureAmount(proc.getProcedureAmount());
            setProc.setProcedureDesc(proc.getProcedureDesc());

            data.add(setProc);
        }


        return data;
    }

    public void setIsInPatientAvailable(LinearLayout ll_in_patient_available, boolean b) {
        if (b)
            ll_in_patient_available.setVisibility(View.VISIBLE);
        else
            ll_in_patient_available.setVisibility(View.GONE);
    }

    public void setTestApproved(LinearLayout ll_test_details, boolean b) {
        if (b) {
            ll_test_details.setVisibility(View.VISIBLE);
        } else
            ll_test_details.setVisibility(View.GONE);
    }


    public void setConsultError(LinearLayout btn_consultation_list, TextView tv_text_list, ProgressBar pb_loading_list) {

        btn_consultation_list.setVisibility(View.VISIBLE);
        tv_text_list.setText("Error Retrieving Data..");
        pb_loading_list.setVisibility(View.GONE);

    }

    public void navArgs(DrawerLayout drawer) {
        if (drawer.isDrawerOpen(Gravity.RIGHT)) {
            drawer.closeDrawer(Gravity.RIGHT);
        } else {
            drawer.openDrawer(Gravity.RIGHT);
        }

    }

    public void updateDisplayForNavigator(TextView tv_nav_notification, RecyclerView rv_doctors, String message) {

        tv_nav_notification.setText(message);
        tv_nav_notification.setVisibility(View.VISIBLE);
        rv_doctors.setVisibility(View.GONE);
    }


    public void showImageUpload(final File file, final String memberID) {


        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage("Are you sure you want to upload this photo?");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        if (NetworkTest.isOnline(context)) {
                            callback.updatePhoto(file, memberID);
                        } else {
                            callback.noInternetConnection();
                        }

                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        callback.cancelledData();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();


    }


    public void setAdmittedDate(TextView tv_admitted, TextView tv_admitted_time, String admittedOn) {

        //2017-06-23 05:57:01.950
        tv_admitted.setText(DateConverter.convertDate(new SimpleDateFormat("MMM dd , yyyy"), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"), admittedOn));
        //setting the txtView to false to disable editing
        tv_admitted.setEnabled(false);
        tv_admitted_time.setText(DateConverter.convertDate(new SimpleDateFormat("hh:mm aa"), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"), admittedOn));
        //setting the txtView to false to disable editing
        tv_admitted.setEnabled(false);


    }


    public interface NavigatorInterface {

        void onSingleItemSelected(DiagnosisList diagnosisList);

        void onUnfilteredHospSelected(Hospital hospital);

        void onSuccessConsultationList(ArrayList<LoaList> body);

        void onErrorConsultationList(String message);

        void updatePhoto(File file, String memberID);

        void noInternetConnection();

        void onSuccessUploadPhoto(File file);

        void onErrorUploadPhoto(String message);

        void cancelledData();

        void roomCategoryCallback(String s);

        void roomPlanCallback(String s);
    }
}
