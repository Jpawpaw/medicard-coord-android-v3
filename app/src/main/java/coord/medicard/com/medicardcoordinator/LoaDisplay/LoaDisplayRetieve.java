package coord.medicard.com.medicardcoordinator.LoaDisplay;

import android.content.Context;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import coord.medicard.com.medicardcoordinator.R;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import services.AppInterface;
import services.AppService;
import utilities.Constant;
import utilities.DataBaseHandler;
import utilities.DateConverter;
import utilities.pdfUtil.OutPatientConsultationForm;
import utilities.pdfUtil.PdfGenerator;

/**
 * Created by mpx-pawpaw on 3/23/17.
 */

public class LoaDisplayRetieve {

    private Context context;
    private LoaDisplayCallback callback;

    public LoaDisplayRetieve(Context context, LoaDisplayCallback callback) {
        this.callback = callback;
        this.context = context;
    }


    public String setStatusHeader(String requestType, String status, String requestDatetime) {

        String data = "";
        System.out.println("requestDatetime:"+requestDatetime);
        System.out.println("status:"+status);
        if (status.contains("AVAILED")) {
            data = "REQUEST AVAILED";
        } else if (null==requestDatetime  || "NULL".equalsIgnoreCase(requestDatetime) || "".equalsIgnoreCase(requestDatetime)) {
            data = "REQUEST PENDING";
        } else if (testExpiration(convertDate2(requestDatetime, new SimpleDateFormat("yyyy-MM-dd HH:mm")))) {
            if (status.contains("APPROVED")) {
                if (requestType.equals("CONSULTATION") || requestType.contains("MATERNITY")){
                    data = "REQUEST SUBMITTED";
                } else {
                    data = "REQUEST APPROVED";
                }
            } else {
                data = "REQUEST " + status.trim();
            }
        } else {
            data = "REQUEST EXPIRED";
        }


        return data;


    }


    public static String currentDate() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("MMM dd , yyyy");
        String formattedDate = df.format(c.getTime());

        return formattedDate;
    }


    public static boolean testExpiration(String requestDatetime) {

        boolean data = false;
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        String current = convertDateToMMddyyyy(currentDate());

        Date current_date = null;
        Date approval_date = null;

        try {
            current_date = dateFormat.parse(current);
            approval_date = dateFormat.parse(DateConverter.validityDatePLusDay(requestDatetime, 2));

            Log.d("CURRENT DATE", current_date + "");
            Log.d("APPROVAL DATE", approval_date + "");

            if (current_date.after(approval_date)){
                return false;
            } else if (current_date.before(approval_date)){
                return true;
            } else if (current_date.equals(approval_date)){
                return true;
            }

        } catch (ParseException e) {
            //expired when approval date cannot be parsed
            e.printStackTrace();
            return true;
        }
        //expired by default
        return true;
    }


    public static String convertDateToMMddyyyy(String dateAdmitted) {


        /**
         CURRENT FORMAT
         02 14, 1975
         to
         FEB 14 , 2017
         */


        String date_s = dateAdmitted;
        SimpleDateFormat dt = new SimpleDateFormat("MMM dd , yyyy");
        Date date = null;
        try {
            date = dt.parse(date_s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dt1 = new SimpleDateFormat("MM/dd/yyyy");

        return dt1.format(date);
    }


    public static String convertDate2(String requestDatetime, SimpleDateFormat currentFormat) {


        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd , yyyy");
        String dateToSend = "";
        try {
            Date currentDate = currentFormat.parse(requestDatetime);
            dateToSend = dateFormat.format(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateToSend;
    }

    public void generateLoaForm(final OutPatientConsultationForm build, final InputStream inputStream) {

        Observable.create(new Observable.OnSubscribe<Boolean>() {

            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                try {
                    PdfGenerator.generatePdfLoaConsultationForm(
                            build, inputStream);
                    subscriber.onNext(Boolean.TRUE);
                } catch (Exception e) {
                    // Timber.d("error %s", e.toString());
                    subscriber.onNext(Boolean.FALSE);
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onCompleted() {
                        // Timber.d("completed process");
                    }

                    @Override
                    public void onError(Throwable e) {
                        //  Timber.d("error message %s", e.toString());
                        callback.onGenerateLoaFormError();
                    }

                    @Override
                    public void onNext(Boolean success) {
                        //  Timber.d("onNext");
                        if (success) {
                            callback.onGenerateLoaFormSuccess();
                        } else {
                            callback.onGenerateLoaFormError();
                        }
                    }
                });

    }


    public void getProcedureNames(TextView tv_procedures, DataBaseHandler database, String[] proceduresList) {

        String data = "";


        if (proceduresList.length == 0) {
            tv_procedures.setText("Not Specified");
        } else {

            for (int x = 0; x < proceduresList.length; x++) {

                data = data + database.getProcedureDesc(proceduresList[x]) + " \n";

            }
            tv_procedures.setText(data);
        }


    }
    public void setDownloadButton(boolean isFileExist, Button btn_download, Button btn_download_test) {

        if (isFileExist){
            btn_download.setText(context.getString(R.string.view_loa));
        } else{
            btn_download.setText(context.getString(R.string.download_loa));
        }
        if (isFileExist){
            btn_download_test.setText(context.getString(R.string.view_loa));
        } else{
            btn_download_test.setText(context.getString(R.string.download_test));
        }

    }

    public String setInstructions(String requestType) {

        String data = "";

        if (requestType.equals(Constant.OTHER_TEST))
            data = context.getString(R.string.other_test_instruction);
        else if (requestType.equals(Constant.BASIC_TEST))
            data = context.getString(R.string.basic_test_instructions);
        else if (requestType.contains(Constant.MATERNITY))
            data = context.getString(R.string.maternity_instruction);
        else if (requestType.equals(Constant.CONSULTATION))
            data = context.getString(R.string.consultation_instruction);


        return data;
    }
}
