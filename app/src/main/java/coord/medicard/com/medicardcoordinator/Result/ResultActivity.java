package coord.medicard.com.medicardcoordinator.Result;

import android.content.Context;
import android.content.Intent;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import adapter.OtherTestLoaAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import coord.medicard.com.medicardcoordinator.CoordinatorMenu.CoordinatorMenuActivity;
import coord.medicard.com.medicardcoordinator.LoaDisplay.BasicTestLoaAdapter;
import coord.medicard.com.medicardcoordinator.MemberSearch.MemberSearchActivity;
import coord.medicard.com.medicardcoordinator.R;
import model.BasicTests;
import model.DiagnosisList;
import model.DoctorModelInsertion;
import model.LoaList;
import model.ProceduresListLocal;
import model.TestsAndProcedures;
import utilities.AlertDialogCustom;
import utilities.BasicTestSession;
import utilities.CheckFileExistence;
import utilities.Constant;
import utilities.DataBaseHandler;
import utilities.DateConverter;
import utilities.InpatientSession;
import utilities.OtherTestSession;
import utilities.SharedPref;
import utilities.ClinicProcedureSession;
import utilities.pdfUtil.GenerateForm;
import utilities.pdfUtil.OutPatientConsultationForm;
import utilities.pdfUtil.PermissionUtililities;

public class ResultActivity extends AppCompatActivity implements ResultCallback, GenerateForm.PdfCallback {


    private final String APPROVED = "Approved";
    private final String DISAPROVED = "Disapproved";
    private final String PLEASE_CALL = "Member is on hold";
    private final String NOT_SPECIFIED = "Not Specified";

    private final String consultation = "CONSULTATION";
    private final String basic_test = "BASIC_TEST";
    private final String maternity = "MATERNITY CONSULTATION";
    private final String other_test = "OTHER_TEST";
    private final String IN_PATIENT = "IN_PATIENT";
    private final String ER = "ER";
    private boolean isFileExisting = false;


    ArrayList<String> procCode = new ArrayList<>();
    ArrayList<String> clinicProcCode = new ArrayList<>(); //this arraylist is for clinicProcedures tests
    ArrayList<String> amount = new ArrayList<>();
    List<TextView> tvSelected = new ArrayList<>();
    List<TextView> tvSelectedData = new ArrayList<>();
    BasicTestSession basicTestSession;
    ClinicProcedureSession clinicProcedureSession;
    OtherTestSession otherTestSession;


//    ArrayList<RequestBasicTest.BasicTestsToSend> tests = new ArrayList<>();

    Context context;

    CardView ll_disclaimer_in_patient, box_header, box2, box3, box4, box5, box6, cv_please_Call;
    TextView tv_pdf_service, tv_provider, tv_doc_app, tv_problem_box6, tv_disclaimer_in_patient, tv_gender_header2, tv_gender2, tv_grandTotal, tv_priceGrandTotal;
    TextView tv_proceduresHeader_in_patient, tv_procedures_in_patient, tv_basic_test_note, tv_remarks_header, tv_disclaimer, tv_disclaimer_outpatient, tv_proceduresHeader, tv_procedures, tv_room_number, tv_room_price, tv_category, tv_diagnosis, tv_doctor_in_patient, tv_category_tag, tv_room_number_tag, tv_reason_er, tv_reason_er_tag, tv_room_plan_inPatient_tag, tv_room_plan_inPatient;
    TextView tv_titleHeader, tv_company, tv_companyHeader, tv_doctor, tv_doctorHeader, tv_approval, tv_approvalHeader, tv_companyGoneHeader, tv_companyGoneName;
    TextView tv_Doctor_box6, tv_date_admitted, tv_gender_header, btn_home_name, tv_toolbar_titleHeader, tv_remarks, tv_birthday, tv_age, tv_gender, tv_id, tv_name;
    LinearLayout btn_view_loa;
    String procedures = "", memberCode, fullName, companyName, birthday, age, gender, valid, effective, typeMember, tvOther, memberPlan, doctor, doctorSpec, diagnosis, remarks, approval_no = "", responceCode, responceDesc, status, clinicFilter, icd10Code;
    LinearLayout ll_remarks, btn_back, btn_home, ll_disclaimer, btn_download;
    String ref = "";
    int limits = 0;
    String problem = "";
    String ORIGIN = "", WITH_PROVIDER = "", batchCode = "";
    String requestStatus = "";
    ResultCallback callback;
    ResultRetrieve implement;
    GenerateForm.PdfCallback pdfCallback;
    private OutPatientConsultationForm.Builder loaFormBuilder; // builder for loa
    AlertDialogCustom alertyDialog = new AlertDialogCustom();

    String hospitalName, hospitalAddress, procedureDesc, totalTestAmount;
    String unfiltered_hospitalName, unfiltered_hospitalAddress;


    //linear layout for old layout and new layout
    LinearLayout content_loa_approval_linear_header, content_loa_approval_linear_footer;
    //header text
    TextView result_tv_remark, result_tv_status, result_tv_instructions, result_tv_disclaimer;
    //member details
    TextView result_tv_ref_no, result_tv_mem_code, result_tv_mem_name, result_tv_age, result_tv_gender, result_tv_birthday, result_tv_com_name, result_tv_date_req;
    TextView result_tv_ref_no_data, result_tv_mem_code_data, result_tv_mem_name_data, result_tv_age_data, result_tv_gender_data, result_tv_birthday_data, result_tv_com_name_data, result_tv_date_req_data;

    // box hospital
    CardView result_cv_box_hospital;
    TextView tv_box_hospital_title;
    TextView tv_box_hospital_name;
    TextView tv_box_hospital_desc;


    //first box
    CardView result_cv_first_box;
    TextView first_box_tv_physician_title;
    TextView first_box_tv_physician_name;
    TextView first_box_tv_physician_desc;
    //second box
    CardView result_cv_second_box;
    TextView second_box_tv_reasonforconsult_title;
    TextView second_box_tv_reasonforconsult_desc;
    //third box
    CardView result_cv_third;
    TextView third_box_tv_roomplanavailed_title;
    TextView third_box_tv_roomplanavailed_desc;
    TextView result_tv_subitem_third_second;
    //fourth box
    CardView result_cv_fourth;
    TextView fourth_box_tv_roomcategory_title;
    TextView fourth_box_tv_roomcategory_desc;
    //fifth box
    CardView result_cv_fifth_box;
    TextView result_tv_item_sixth;
    TextView result_tv_subitem_sixth_first;

    //sixth box
    CardView result_cv_sixth_box;
    TextView fifth_box_tv_roomno_title;
    TextView fifth_box_tv_roomno_desc;

    //sixth box
    CardView result_cv_seventh_box;
    TextView result_tv_item_seventh;
    TextView result_tv_subitem_seventh_first;
    TextView result_tv_subitem_seventh_second;
    //basic test cv
    CardView result_cv_basictest_selected_box;
    TextView tv_basictest_selected_first;
    TextView tv_basictest_selected_second;
    TextView tv_basictest_selected_third;
    TextView tv_basictest_selected_fourth;
    TextView tv_basictest_selected_fifth;
    TextView tv_basictest_selected_sixth;
    TextView tv_basictest_selected_first_data;
    TextView tv_basictest_selected_second_data;
    TextView tv_basictest_selected_third_data;
    TextView tv_basictest_selected_fourth_data;
    TextView tv_basictest_selected_fifth_data;
    TextView tv_basictest_selected_sixth_data;

    TextView tv_basictest_total;
    TextView tv_proc_title;


    InpatientSession inpatientSession;

    //inpatient listing
    @BindView(R.id.result_cv_inpatient_doctor_box)
    CardView result_cv_inpatient_doctor_box;
    @BindView(R.id.inpatient_doctor_title)
    TextView inpatient_doctor_title;
    @BindView(R.id.lv_inpatient_doctor_title)
    ListView lv_inpatient_doctor_title;
    @BindView(R.id.lv_inpatient_doctor_desc)
    ListView lv_inpatient_doctor_desc;
    ArrayAdapter<String> adapterDocList,adapterDocDesc;
    ArrayList<String> arraylistDocList, arraylistDocDesc;

    @BindView(R.id.result_cv_inpatient_diagnosis_box)
    CardView result_cv_inpatient_diagnosis_box;
    @BindView(R.id.inpatient_diagnosis_title)
    TextView inpatient_diagnosis_title;
    @BindView(R.id.lv_inpatient_diagnosis_title)
    ListView lv_inpatient_diagnosis_title;
    @BindView(R.id.lv_inpatient_diagnosis_desc)
    ListView lv_inpatient_diagnosis_desc;
    ArrayAdapter<String> adapterDiagList,adapterDiagDesc;
    ArrayList<String> arraylistDiagList, arraylistDiagDesc;

    @BindView(R.id.result_cv_inpatient_procedure_box)
    CardView result_cv_inpatient_procedure_box;
    @BindView(R.id.inpatient_procedure_title)
    TextView inpatient_procedure_title;
    @BindView(R.id.lv_inpatient_procedure_title)
    ListView lv_inpatient_procedure_title;
    @BindView(R.id.lv_inpatient_procedure_desc)
    ListView lv_inpatient_procedure_desc;
    ArrayAdapter<String> adapterProcList,adapterProcDesc;
    ArrayList<String> arraylistProcList, arraylistProcDesc;




    //other test
    ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> ARRAYLIST_GroupedByDiag = new ArrayList<>();
    OtherTestLoaAdapter otherTestLoaAdapter;
    LoaList loa;

    CardView cv_othertest;
    TextView tv_othertest_label;
    TextView tv_othertest_costcenter;
    RecyclerView rv_othertest_diagnosis;
    TextView tv_othertest_grandTotal;
    TextView tv_othertest_priceGrandTotal;


    //clinic procedures selected
    CardView result_cv_clinicproc_selected_box;
    ListView lv_clinicproc_selected_title, lv_clinicproc_selected_data;
    ArrayAdapter<String> adapterListDesc;
    ArrayAdapter<String> adapterListData;
    ArrayList<String> clinicListDesc, clinicListData;
    BasicTestLoaAdapter basicTestLoaAdapter;
    ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> arrayListGroupedByDiag = new ArrayList<>();


    //footer box
    LinearLayout result_lv_footer_first;
    TextView tv_footer_first;
    TextView tv_footer_second_valid_from;
    TextView tv_footer_third_val_date;
    Button result_lv_footer_first_btn_ok;
    Button result_lv_footer_first_btn_download;
    Button result_lv_footer_first_btn_download_test;
    LinearLayout result_lv_footer_second;
    LinearLayout result_lv_footer_second_btn_home;
    LinearLayout result_lv_footer_second_btn_downloads;
    LinearLayout result_lv_footer_second_btn_back;
    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


    String otherTestPrimaryDiag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        tv_toolbar_titleHeader = (TextView) toolbar.findViewById(R.id.tv_toolbar_titleHeader);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        context = this;
        callback = this;
        implement = new ResultRetrieve(context);
        DataBaseHandler db = new DataBaseHandler(context);
        db.setALLProcedureToFalse();
        db.setALLDiagnosisToFalse();
        pdfCallback = this;

        inpatientSession = new InpatientSession();

        tv_titleHeader = (TextView) findViewById(R.id.tv_titleHeader);
        tv_pdf_service = (TextView) findViewById(R.id.tv_pdf_service);

        content_loa_approval_linear_footer = (LinearLayout) findViewById(R.id.content_loa_approval_linear_footer);
        //header text
        result_tv_remark = (TextView) findViewById(R.id.result_tv_remark);
        result_tv_status = (TextView) findViewById(R.id.result_tv_status);
        result_tv_instructions = (TextView) findViewById(R.id.result_tv_instructions);
        result_tv_disclaimer = (TextView) findViewById(R.id.result_tv_disclaimer);
        //member details
        //item
        result_tv_ref_no = (TextView) findViewById(R.id.result_tv_ref_no);
        result_tv_mem_code = (TextView) findViewById(R.id.result_tv_mem_code);
        result_tv_mem_name = (TextView) findViewById(R.id.result_tv_mem_name);
        result_tv_age = (TextView) findViewById(R.id.result_tv_age);
        result_tv_gender = (TextView) findViewById(R.id.result_tv_gender);
        result_tv_birthday = (TextView) findViewById(R.id.result_tv_birthday);
        result_tv_com_name = (TextView) findViewById(R.id.result_tv_com_name);
        result_tv_date_req = (TextView) findViewById(R.id.result_tv_date_req);
        //subitem
        result_tv_ref_no_data = (TextView) findViewById(R.id.result_tv_ref_no_data);
        result_tv_mem_code_data = (TextView) findViewById(R.id.result_tv_mem_code_data);
        result_tv_mem_name_data = (TextView) findViewById(R.id.result_tv_mem_name_data);
        result_tv_age_data = (TextView) findViewById(R.id.result_tv_age_data);
        result_tv_gender_data = (TextView) findViewById(R.id.result_tv_gender_data);
        result_tv_birthday_data = (TextView) findViewById(R.id.result_tv_birthday_data);
        result_tv_com_name_data = (TextView) findViewById(R.id.result_tv_com_name_data);
        result_tv_date_req_data = (TextView) findViewById(R.id.result_tv_date_req_data);
        //box hospital
        result_cv_box_hospital = (CardView) findViewById(R.id.result_cv_box_hospital);
        tv_box_hospital_title = (TextView) findViewById(R.id.tv_box_hospital_title);
        tv_box_hospital_name = (TextView) findViewById(R.id.tv_box_hospital_name);
        tv_box_hospital_desc = (TextView) findViewById(R.id.tv_box_hospital_desc);


        //first box
        result_cv_first_box = (CardView) findViewById(R.id.result_cv_first_box);
        first_box_tv_physician_title = (TextView) findViewById(R.id.first_box_tv_physician_title);
        first_box_tv_physician_name = (TextView) findViewById(R.id.first_box_tv_physician_name);
        first_box_tv_physician_desc = (TextView) findViewById(R.id.first_box_tv_physician_desc);
        //second box
        result_cv_second_box = (CardView) findViewById(R.id.result_cv_second_box);
        second_box_tv_reasonforconsult_title = (TextView) findViewById(R.id.second_box_tv_reasonforconsult_title);
        second_box_tv_reasonforconsult_desc = (TextView) findViewById(R.id.second_box_tv_reasonforconsult_desc);
        //third box
        result_cv_third = (CardView) findViewById(R.id.result_cv_third_box);
        third_box_tv_roomplanavailed_title = (TextView) findViewById(R.id.third_box_tv_roomplanavailed_title);
        third_box_tv_roomplanavailed_desc = (TextView) findViewById(R.id.third_box_tv_roomplanavailed_desc);
        //fourth box
        result_cv_fourth = (CardView) findViewById(R.id.result_cv_fourth_box);
        fourth_box_tv_roomcategory_title = (TextView) findViewById(R.id.fourth_box_tv_roomcategory_title);
        fourth_box_tv_roomcategory_desc = (TextView) findViewById(R.id.fourth_box_tv_roomcategory_desc);
        //fifth box
        result_cv_fifth_box = (CardView) findViewById(R.id.result_cv_fifth_box);
        fifth_box_tv_roomno_title = (TextView) findViewById(R.id.fifth_box_tv_roomno_title);
        fifth_box_tv_roomno_desc = (TextView) findViewById(R.id.fifth_box_tv_roomno_desc);
        //sixth box
        result_cv_sixth_box = (CardView) findViewById(R.id.result_cv_sixth_box);
        result_tv_item_sixth = (TextView) findViewById(R.id.result_tv_item_sixth);
        result_tv_subitem_sixth_first = (TextView) findViewById(R.id.result_tv_subitem_sixth_first);

        //display for coord account
        result_cv_seventh_box = (CardView) findViewById(R.id.result_cv_seventh_box);
        result_tv_item_seventh = (TextView) findViewById(R.id.result_tv_item_seventh);
        result_tv_subitem_seventh_first = (TextView) findViewById(R.id.result_tv_subitem_seventh_first);
        result_tv_subitem_seventh_second = (TextView) findViewById(R.id.result_tv_subitem_seventh_second);

        //basic test selected item box
        result_cv_basictest_selected_box = (CardView) findViewById(R.id.result_cv_basictest_selected_box);
        tv_basictest_selected_first = (TextView) findViewById(R.id.tv_basictest_selected_first);
        tv_basictest_selected_second = (TextView) findViewById(R.id.tv_basictest_selected_second);
        tv_basictest_selected_third = (TextView) findViewById(R.id.tv_basictest_selected_third);
        tv_basictest_selected_fourth = (TextView) findViewById(R.id.tv_basictest_selected_fourth);
        tv_basictest_selected_fifth = (TextView) findViewById(R.id.tv_basictest_selected_fifth);
        tv_basictest_selected_sixth = (TextView) findViewById(R.id.tv_basictest_selected_sixth);
        tv_basictest_selected_first_data = (TextView) findViewById(R.id.tv_basictest_selected_first_data);
        tv_basictest_selected_second_data = (TextView) findViewById(R.id.tv_basictest_selected_second_data);
        tv_basictest_selected_third_data = (TextView) findViewById(R.id.tv_basictest_selected_third_data);
        tv_basictest_selected_fourth_data = (TextView) findViewById(R.id.tv_basictest_selected_fourth_data);
        tv_basictest_selected_fifth_data = (TextView) findViewById(R.id.tv_basictest_selected_fifth_data);
        tv_basictest_selected_sixth_data = (TextView) findViewById(R.id.tv_basictest_selected_sixth_data);
        tv_basictest_total = (TextView) findViewById(R.id.tv_basictest_total);


        //other test
        cv_othertest = (CardView) findViewById(R.id.cv_othertest);
        tv_othertest_label = (TextView) findViewById(R.id.tv_othertest_label);
        tv_othertest_costcenter = (TextView) findViewById(R.id.tv_othertest_costcenter);
        rv_othertest_diagnosis = (RecyclerView) findViewById(R.id.rv_othertest_diagnosis);
        tv_othertest_grandTotal = (TextView) findViewById(R.id.tv_othertest_grandTotal);
        tv_othertest_priceGrandTotal = (TextView) findViewById(R.id.tv_othertest_priceGrandTotal);

        //cliniic procedures
        result_cv_clinicproc_selected_box = (CardView) findViewById(R.id.result_cv_clinicproc_selected_box);
        lv_clinicproc_selected_title = (ListView) findViewById(R.id.lv_clinicproc_selected_title);
        lv_clinicproc_selected_data = (ListView) findViewById(R.id.lv_clinicproc_selected_data);
        tv_grandTotal = (TextView) findViewById(R.id.tv_grandTotal);
        tv_priceGrandTotal = (TextView) findViewById(R.id.tv_priceGrandTotal);
        tv_proc_title = (TextView) findViewById(R.id.tv_proc_title);

        tvSelected.add(tv_basictest_selected_first);
        tvSelected.add(tv_basictest_selected_second);
        tvSelected.add(tv_basictest_selected_third);
        tvSelected.add(tv_basictest_selected_fourth);
        tvSelected.add(tv_basictest_selected_fifth);
        tvSelected.add(tv_basictest_selected_sixth);
        //====================================================
        tvSelectedData.add(tv_basictest_selected_first_data);
        tvSelectedData.add(tv_basictest_selected_second_data);
        tvSelectedData.add(tv_basictest_selected_third_data);
        tvSelectedData.add(tv_basictest_selected_fourth_data);
        tvSelectedData.add(tv_basictest_selected_fifth_data);
        tvSelectedData.add(tv_basictest_selected_sixth_data);


        final TextView[] tvSelected = {tv_basictest_selected_first, tv_basictest_selected_second, tv_basictest_selected_third, tv_basictest_selected_fourth, tv_basictest_selected_fifth, tv_basictest_selected_sixth};
        TextView[] amountSelected = {tv_basictest_selected_first_data, tv_basictest_selected_second_data, tv_basictest_selected_third_data, tv_basictest_selected_fourth_data, tv_basictest_selected_fifth_data, tv_basictest_selected_sixth_data};

        //=================================================================

        //footer box
        result_lv_footer_first = (LinearLayout) findViewById(R.id.result_lv_footer_first);
        tv_footer_first = (TextView) findViewById(R.id.tv_footer_first);
        tv_footer_third_val_date = (TextView) findViewById(R.id.tv_footer_third_val_date);
        tv_footer_second_valid_from = (TextView) findViewById(R.id.tv_footer_second_valid_from);
        result_lv_footer_first_btn_ok = (Button) findViewById(R.id.result_lv_footer_first_btn_ok);
        result_lv_footer_first_btn_download = (Button) findViewById(R.id.result_lv_footer_first_btn_download);
        result_lv_footer_first_btn_download_test = (Button) findViewById(R.id.result_lv_footer_first_btn_download_test);
        result_lv_footer_second = (LinearLayout) findViewById(R.id.result_lv_footer_second);
        result_lv_footer_second_btn_home = (LinearLayout) findViewById(R.id.result_lv_footer_second_btn_home);
        result_lv_footer_second_btn_downloads = (LinearLayout) findViewById(R.id.result_lv_footer_second_btn_downloads);
        result_lv_footer_second_btn_back = (LinearLayout) findViewById(R.id.result_lv_footer_second_btn_back);

        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


        SharedPref sharedPref = new SharedPref();

        birthday = sharedPref.getStringValue(sharedPref.USER, sharedPref.BIRTHDAY, context);
        companyName = sharedPref.getStringValue(sharedPref.USER, sharedPref.COMPANY_NAME, context);
        fullName = sharedPref.getStringValue(sharedPref.USER, sharedPref.FULL_NAME, context);
        age = sharedPref.getStringValue(sharedPref.USER, sharedPref.AGE, context);
        limits = sharedPref.getIntegerValue(sharedPref.USER, sharedPref.LIMITS, context);
        valid = sharedPref.getStringValue(sharedPref.USER, sharedPref.VALID, context);
        effective = sharedPref.getStringValue(sharedPref.USER, sharedPref.EFFECTIVE, context);
        tvOther = sharedPref.getStringValue(sharedPref.USER, sharedPref.REMARKS, context);
        typeMember = sharedPref.getStringValue(sharedPref.USER, sharedPref.TYPE, context);
        memberPlan = sharedPref.getStringValue(sharedPref.USER, sharedPref.MEMBER_PLAN, context);
        gender = sharedPref.getStringValue(sharedPref.USER, sharedPref.GENDER, context);
        memberCode = sharedPref.getStringValue(sharedPref.USER, sharedPref.ID, context);
        responceCode = sharedPref.getStringValue(sharedPref.USER, sharedPref.CODESTATUS, context);
        status = sharedPref.getStringValue(sharedPref.USER, sharedPref.MEMBER_STATUS, context);
        hospitalName = sharedPref.getStringValue(sharedPref.USER, sharedPref.HOSPITALNAME, context);
        hospitalAddress = sharedPref.getStringValue(sharedPref.USER, sharedPref.HOSPITALADDRESS, context);
        unfiltered_hospitalName = sharedPref.getStringValue(sharedPref.USER, sharedPref.UNFILTERED_HOSPITAL_NAME, context);
        unfiltered_hospitalAddress = sharedPref.getStringValue(sharedPref.USER, sharedPref.UNFILTERED_HOSPITAL_ADDR, context);


//        procedureDesc = sharedPref.getStringValue(sharedPref.USER, sharedPref.PROCEDURE_DESC, context);

        ////**************************

//        ArrayList<BasicTests> arraylist = BasicTestSession.getAllProcedureCode();
//        System.out.println(arraylist.get(0));

//        System.out.println("SELECTED PROCEDURES"+tests.toString());
//
        try {
            ArrayList<BasicTests> basicTestses = BasicTestSession.getAllProcedureCode();
            for (BasicTests basicTests : basicTestses) {
                procCode.add(basicTests.getProcedureDesc());
                amount.add(basicTests.getProcedureAmount());
            }
            System.out.println("%%%%%%%%%%" + basicTestses.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*
         looping the session in clinicProcedure geting the desc of each test
         */
        try {
            ArrayList<TestsAndProcedures> testsAndProcedures = ClinicProcedureSession.getAllProcedureCode();
            for (TestsAndProcedures testsAndProcedure : testsAndProcedures) {
                clinicProcCode.add(testsAndProcedure.getProcedureDesc());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        GET_ARRAY =  (ArrayList<DiagnosisProcedures>)getIntent().getSerializableExtra("PRIMARY_ARRAY_LIST");
//        GET_ARRAY_PENDING =  (ArrayList<DiagnosisProcedures>)getIntent().getSerializableExtra("OTHER_ARRAY_LIST");

        otherTestPrimaryDiag = getIntent().getExtras().getString("PRIMARY_DIAGNOSIS");
        doctor = getIntent().getExtras().getString("DOCTOR");
        doctorSpec = getIntent().getExtras().getString("DOCTOR_SPEC");
        responceCode = getIntent().getExtras().getString("RESPONCE_CODE");
        approval_no = getIntent().getExtras().getString("APPROVAL_NO");
        responceDesc = getIntent().getExtras().getString("RESPONCE_DESC");
        remarks = getIntent().getExtras().getString("REMARKS");
        ORIGIN = getIntent().getExtras().getString("REQUEST");
        WITH_PROVIDER = getIntent().getExtras().getString("WITH_PROVIDER");
        requestStatus = getIntent().getExtras().getString("REQUEST_STATUS");
        //testing diag value for procedure
        diagnosis = getIntent().getExtras().getString("DIAGNOSIS");
        clinicFilter = getIntent().getExtras().getString("CLINIC_FILTER");
        loa = (LoaList) getIntent().getExtras().get("LOALIST");
        try {
            icd10Code = getIntent().getExtras().getString("ICD10CODE");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            totalTestAmount = getIntent().getExtras().getString("TOTALTESTAMOUNT");
        } catch (Exception e) {
            e.printStackTrace();
        }


        selectedDataForBasicTestAndProcedures();


//        System.out.println("PROCEDURE_LIST " + ar1);
        System.out.println("STATUS " + status);
        //     implement.withProviderVisiblity(WITH_PROVIDER , tv_doc_app);
        System.out.println("helooooo Diagnosis " + diagnosis);
        System.out.println("helooooo procedures" + procedures);


        Log.d("DATA_EXIST", CheckFileExistence.fileExistance(ORIGIN, approval_no) + "");

        isFileExisting = CheckFileExistence.fileExistance(ORIGIN, approval_no);
        implement.setDownloadButton(result_lv_footer_first_btn_download, result_lv_footer_first_btn_download_test, isFileExisting);





        //todo: COMMENTED FOR FIXING OF IN-PATIENT END SCREEN - JHAY
        if (responceCode == null) {
            showUserDetails();
            if (ORIGIN.equals(IN_PATIENT)) {
                tv_toolbar_titleHeader.setText("In Patient");
                tv_titleHeader.setText("SUBMITTED");
                inPatientDisplay();
            } else if (ORIGIN.equals(ER)) {
                tv_toolbar_titleHeader.setText("Emergency Room");
                emergencyRoomDisplay();
            }
        } else if (responceCode.equals("200")) {
            showUserDetails();
            if (ORIGIN.equals(Constant.BASIC_TEST) || ORIGIN.equals(Constant.OTHER_TEST) || ORIGIN.equals(Constant.PROCEDURES)) {
                if (requestStatus.contains("approved")) {
                    tv_titleHeader.setText("REQUEST APPROVED");
                    if (ORIGIN.equals(Constant.BASIC_TEST)) {
                        selectedDataForBasicTestAndProcedures();
                        tv_toolbar_titleHeader.setText("BASIC TEST");
                        result_tv_remark.setVisibility(View.GONE);
                        result_tv_status.setVisibility(View.GONE);
                        result_tv_instructions.setVisibility(View.VISIBLE);
                        result_tv_instructions.setText("NOTE: Please write the Approval Code and Type of Basic Test on the Request Form.");
                        result_tv_disclaimer.setVisibility(View.GONE);
                        result_cv_fifth_box.setVisibility(View.GONE);
                        result_tv_ref_no.setText("Approval Code:");
                        result_cv_first_box.setVisibility(View.GONE);
                        result_cv_second_box.setVisibility(View.GONE);
                        result_cv_third.setVisibility(View.GONE);
                        result_cv_fourth.setVisibility(View.GONE);
                        result_cv_fifth_box.setVisibility(View.GONE);
                        result_cv_sixth_box.setVisibility(View.GONE);
                        result_lv_footer_second.setVisibility(View.GONE);
                        result_lv_footer_first.setVisibility(View.VISIBLE);
                        tv_footer_first.setVisibility(View.VISIBLE);
                        result_lv_footer_first_btn_ok.setVisibility(View.VISIBLE);
                        result_lv_footer_first_btn_download_test.setVisibility(View.VISIBLE);
                        result_lv_footer_first_btn_download_test.setText("Download Form");
                        result_lv_footer_first_btn_download.setVisibility(View.GONE);
                        tv_footer_third_val_date.setText("This request is valid from " + DateConverter.convertDateToMMddyyyy(DateConverter.getDate()) + " to " + DateConverter.validityDatePLusDay(DateConverter.getDate(), 2));
                        basicTestDisplay();
                    } else if (ORIGIN.equals(Constant.OTHER_TEST)) {
                        tv_toolbar_titleHeader.setText("OTHER TEST");
                        tv_titleHeader.setText("REQUEST APPROVED");
                        result_tv_instructions.setText(R.string.out_patient_disclaimer_reference_approve);
                        result_tv_remark.setVisibility(View.GONE);
                        result_tv_status.setVisibility(View.GONE);
                        result_tv_disclaimer.setVisibility(View.GONE);
                        result_cv_second_box.setVisibility(View.GONE);
                        result_cv_third.setVisibility(View.GONE);
                        result_cv_fourth.setVisibility(View.GONE);
                        result_cv_fifth_box.setVisibility(View.GONE);
                        result_cv_sixth_box.setVisibility(View.GONE);
                        result_tv_ref_no.setVisibility(View.GONE);
                        result_tv_ref_no_data.setVisibility(View.GONE);
                        result_lv_footer_first.setVisibility(View.VISIBLE);
                        result_lv_footer_second.setVisibility(View.GONE);
                        tv_footer_first.setVisibility(View.VISIBLE);
                        tv_footer_third_val_date.setText("This request is valid from " + DateConverter.convertDateToMMddyyyy(DateConverter.getDate()) + " to " + DateConverter.validityDatePLusDay(DateConverter.getDate(), 2));
                        result_lv_footer_first_btn_ok.setVisibility(View.VISIBLE);
                        result_lv_footer_first_btn_download.setVisibility(View.GONE);
                        result_lv_footer_first_btn_download_test.setVisibility(View.GONE);
                        othertestDisplay();
                    } else if (ORIGIN.equals(Constant.PROCEDURES)) {
                        selectedDataForBasicTestAndProcedures();
                        tv_toolbar_titleHeader.setText("CLINIC PROCEDURES");
                        result_tv_instructions.setText("NOTE: Please write the Approval Code and Doctor’s Clinic Procedure on the Consultation Form");
                        tv_footer_third_val_date.setText("This request is valid from " + DateConverter.convertDateToMMddyyyy(DateConverter.getDate()) + " to " + DateConverter.validityDatePLusDay(DateConverter.getDate(), 2));
                        tv_titleHeader.setText("REQUEST APPROVED");
                        result_tv_status.setVisibility(View.GONE);
                        result_tv_remark.setVisibility(View.GONE);
                        result_tv_disclaimer.setVisibility(View.GONE);
                        result_lv_footer_first.setVisibility(View.VISIBLE);
                        result_tv_ref_no.setText("Approval Code:");
                        tv_footer_first.setVisibility(View.GONE);
                        result_lv_footer_first_btn_download_test.setVisibility(View.VISIBLE);
                        result_lv_footer_first_btn_download_test.setText("Download Form");
                        result_lv_footer_first_btn_download.setVisibility(View.GONE);
                        result_tv_item_sixth.setText("Clinic Procedure Remarks");
                        result_lv_footer_second.setVisibility(View.GONE);
                        tv_footer_first.setVisibility(View.VISIBLE);
                        procedureDisplay();
                    }
                    //pending request
                } else {
                    tv_titleHeader.setText("REQUEST PENDING");
                    result_tv_instructions.setVisibility(View.VISIBLE);
                    result_tv_instructions.setText(getString(R.string.out_patient_disclaimer_reference_pending));
                    tv_footer_second_valid_from.setVisibility(View.VISIBLE);
                    tv_footer_second_valid_from.setText(R.string.this_request_is_valid_for);
                    if (ORIGIN.equals(Constant.BASIC_TEST)) {
                        tv_toolbar_titleHeader.setText("BASIC TEST");
                        result_tv_remark.setVisibility(View.GONE);
                        result_tv_status.setVisibility(View.GONE);
                        result_tv_instructions.setVisibility(View.VISIBLE);
                        result_tv_disclaimer.setVisibility(View.GONE);
                        result_cv_first_box.setVisibility(View.GONE);
                        result_cv_second_box.setVisibility(View.GONE);
                        result_cv_third.setVisibility(View.GONE);
                        result_cv_fourth.setVisibility(View.GONE);
                        result_cv_fifth_box.setVisibility(View.GONE);
                        result_cv_sixth_box.setVisibility(View.GONE);
                        result_tv_ref_no.setVisibility(View.GONE);
                        result_tv_ref_no_data.setVisibility(View.GONE);
                        result_lv_footer_first.setVisibility(View.VISIBLE);
                        result_lv_footer_second.setVisibility(View.GONE);
                        tv_footer_first.setVisibility(View.GONE);
                        tv_footer_third_val_date.setVisibility(View.GONE);
                        result_lv_footer_first_btn_ok.setVisibility(View.VISIBLE);
                        result_lv_footer_first_btn_download.setVisibility(View.GONE);
                        result_lv_footer_first_btn_download_test.setVisibility(View.GONE);
                        basicTestDisplay();
                    } else if (ORIGIN.equals(Constant.OTHER_TEST)) {
                        tv_toolbar_titleHeader.setText("OTHER TEST");
                        result_tv_disclaimer.setVisibility(View.GONE);
                        result_cv_second_box.setVisibility(View.GONE);
                        result_cv_third.setVisibility(View.GONE);
                        result_cv_fourth.setVisibility(View.GONE);
                        result_cv_fifth_box.setVisibility(View.GONE);
                        result_cv_sixth_box.setVisibility(View.GONE);
                        result_tv_ref_no.setVisibility(View.GONE);
                        result_tv_ref_no_data.setVisibility(View.GONE);
                        result_lv_footer_first.setVisibility(View.VISIBLE);
                        result_lv_footer_second.setVisibility(View.GONE);
                        tv_footer_first.setVisibility(View.GONE);
                        tv_footer_third_val_date.setVisibility(View.GONE);
                        result_lv_footer_first_btn_ok.setVisibility(View.VISIBLE);
                        result_lv_footer_first_btn_download.setVisibility(View.GONE);
                        result_lv_footer_first_btn_download_test.setVisibility(View.GONE);
                        othertestDisplay();
                    } else if (ORIGIN.equals(Constant.PROCEDURES)) {
                        selectedDataForBasicTestAndProcedures();
                        tv_toolbar_titleHeader.setText("CLINIC PROCEDURES");
                        tv_titleHeader.setText("REQUEST PENDING");
                        result_tv_remark.setVisibility(View.GONE);
                        result_tv_status.setVisibility(View.GONE);
                        result_tv_instructions.setVisibility(View.VISIBLE);
                        result_tv_disclaimer.setVisibility(View.GONE);
                        result_tv_ref_no.setVisibility(View.GONE);
                        result_tv_ref_no_data.setVisibility(View.GONE);
                        result_lv_footer_first.setVisibility(View.VISIBLE);
                        result_lv_footer_second.setVisibility(View.GONE);
                        tv_footer_first.setVisibility(View.GONE);
                        tv_footer_third_val_date.setVisibility(View.GONE);
                        result_lv_footer_first_btn_ok.setVisibility(View.VISIBLE);
                        result_lv_footer_first_btn_download.setVisibility(View.GONE);
                        result_lv_footer_first_btn_download_test.setVisibility(View.GONE);
                        result_tv_item_sixth.setText("Clinic Procedure Remarks");
                        procedureDisplay();
                    }
                }
            }

            if (ORIGIN.equals(consultation) || ORIGIN.equals(maternity)) {
                if (ORIGIN.equals(consultation)) {
                    tv_toolbar_titleHeader.setText("Consultation");
                } else if (ORIGIN.equals(maternity)) {
                    tv_toolbar_titleHeader.setText("Maternity Consultation");
                }
                tv_titleHeader.setText("REQUEST SUBMITTED");
                result_tv_remark.setVisibility(View.GONE);
                result_tv_status.setVisibility(View.GONE);
                result_tv_instructions.setVisibility(View.VISIBLE);
                result_tv_instructions.setText(getString(R.string.submitted_instruction));
                result_tv_disclaimer.setText(getString(R.string.out_patient_disclaimer_reference));
                result_tv_ref_no.setVisibility(View.VISIBLE);
                result_tv_ref_no.setText("Reference Code:");
                result_tv_ref_no_data.setVisibility(View.VISIBLE);
                result_tv_ref_no_data.setText(approval_no);
                result_lv_footer_first.setVisibility(View.VISIBLE);
                result_lv_footer_second.setVisibility(View.GONE);
                tv_footer_first.setVisibility(View.VISIBLE);
                tv_footer_third_val_date.setText("This request is valid from " + DateConverter.convertDateToMMddyyyy(DateConverter.getDate()) + " to " + DateConverter.validityDatePLusDay(DateConverter.getDate(), 2));
                result_lv_footer_first_btn_ok.setVisibility(View.VISIBLE);
                result_lv_footer_first_btn_download.setVisibility(View.VISIBLE);
                result_lv_footer_first_btn_download.setText("Download Form");
                result_lv_footer_first_btn_download_test.setVisibility(View.GONE);
                consultationAndMaternityDisplay();
            } else if (ORIGIN.equals(IN_PATIENT)) {
                tv_toolbar_titleHeader.setText("In Patient");
                tv_titleHeader.setText("SUBMITTED");
                inPatientDisplay();
            } else if (ORIGIN.equals(ER)) {
                tv_toolbar_titleHeader.setText("Emergency Room");
                tv_titleHeader.setText("SUBMITTED");
                emergencyRoomDisplay();
            } else {


            }

        } else {
            showUserDetails();
            if (ORIGIN.equals(IN_PATIENT)) {
                tv_toolbar_titleHeader.setText("In Patient");
                tv_titleHeader.setText("SUBMITTED");
                inPatientDisplay();
            } else if (ORIGIN.equals(ER)) {
                tv_toolbar_titleHeader.setText("Emergency Room");
                emergencyRoomDisplay();
            }
        }

//        btn_back.setOnClickListener(new View.OnClickListener()
//
//                                    {
//                                        @Override
//                                        public void onClick(View v) {
//                                            finish();
//                                            procCode.clear();
//                                            amount.clear();
//                                        }
//                                    }
//
//        );
//
//
//        btn_home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(ResultActivity.this, MemberSearchActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
//                finish();
//            }
//        });
//
//
//        btn_download.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (isFileExisting) {
//                    alertyDialog.showLOA(context, ORIGIN, approval_no);
//                } else {
//                    generateLoaConsultationForm();
//                }
//            }
//        });


        //<----------------BUTTON FOR NEW LAYOUT--------------------->
        result_lv_footer_second_btn_back.setOnClickListener(new View.OnClickListener()

                                                            {
                                                                @Override
                                                                public void onClick(View v) {
                                                                    finish();
                                                                    procCode.clear();
                                                                    amount.clear();
                                                                    System.out.println("size of proc code: " + procCode.size() + "\n size of amount:" + amount.size());
                                                                }
                                                            }

        );
        result_lv_footer_second_btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultActivity.this, MemberSearchActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        result_lv_footer_second_btn_downloads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFileExisting) {
                    alertyDialog.showLOA(context, ORIGIN, approval_no);
                } else {
                    if (ORIGIN.equals(consultation) || ORIGIN.equals(maternity)) {
                        generateLoaConsultationForm();
                    } else {
                        generateTestForm();
                    }


                }
            }
        });

        result_lv_footer_first_btn_ok.setOnClickListener(new View.OnClickListener() {
                                                             @Override
                                                             public void onClick(View v) {

                                                                 disableText();
                                                                 clinicProcedureSession.releaseContent();
                                                                 procCode.clear();
                                                                 amount.clear();
                                                                 adapterListDesc.clear();
                                                                 adapterListData.clear();
                                                                 clinicListDesc.clear();
                                                                 clinicListData.clear();
                                                                 adapterListDesc.notifyDataSetChanged();
                                                                 adapterListData.notifyDataSetChanged();
                                                                 lv_clinicproc_selected_data.setAdapter(null);
                                                                 lv_clinicproc_selected_title.setAdapter(null);

                                                                 Intent goToCoordMenu = new Intent(getApplicationContext(), CoordinatorMenuActivity.class);
//                                                                 goToCoordMenu.putExtra(SearchToCoordinatorMenu.NAME,fullName);
//                                                                 goToCoordMenu.putExtra(SearchToCoordinatorMenu.AGE,age);
//                                                                 goToCoordMenu.putExtra(SearchToCoordinatorMenu.BDAY,birthday);
//                                                                 goToCoordMenu.putExtra(SearchToCoordinatorMenu.DD_LIMIT,limits);
//                                                                 goToCoordMenu.putExtra(SearchToCoordinatorMenu.GENDER,gender);
//                                                                 goToCoordMenu.putExtra(SearchToCoordinatorMenu.ID,memberCode);
//                                                                 goToCoordMenu.putExtra(SearchToCoordinatorMenu.COMPANY,companyName);
//                                                                 goToCoordMenu.putExtra(SearchToCoordinatorMenu.STATUS,status);
//                                                                 goToCoordMenu.putExtra(SearchToCoordinatorMenu.VALID,valid);
//                                                                 goToCoordMenu.putExtra(SearchToCoordinatorMenu.OTHER,tvOther);
//                                                                 goToCoordMenu.putExtra(SearchToCoordinatorMenu.EFFECTIVE,effective);
//                                                                 goToCoordMenu.putExtra(SearchToCoordinatorMenu.TYPE,typeMember);
//                                                                 goToCoordMenu.putExtra(SearchToCoordinatorMenu.PLAN,memberPlan);
                                                                 startActivity(goToCoordMenu);

                                                             }
                                                         }
        );
        result_lv_footer_first_btn_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFileExisting) {
                    alertyDialog.showLOA(context, ORIGIN, approval_no);
                } else {

                    generateLoaConsultationForm();
                }
            }
        });

        result_lv_footer_first_btn_download_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFileExisting) {
                    alertyDialog.showLOA(context, ORIGIN, approval_no);
                }
                if (ORIGIN.equals(Constant.PROCEDURES)) {
                    generateLoaConsultationForm();

                } else {
                    generateTestForm();
                }
            }
        });


    }

    private void selectedDataForBasicTestAndProcedures() {
        clinicListDesc = new ArrayList<>();
        clinicListData = new ArrayList<>();

        if (ORIGIN.equalsIgnoreCase(Constant.PROCEDURES)) {
            try {
                ArrayList<TestsAndProcedures> clinicProcedures = ClinicProcedureSession.getAllProcedureCode();
                for (TestsAndProcedures testsAndProcedures : clinicProcedures) {
                    clinicListDesc.add(testsAndProcedures.getProcedureDesc());
                    clinicListData.add(testsAndProcedures.getProcedureAmount());
                }
                adapterListDesc.notifyDataSetChanged();
                adapterListData.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (ORIGIN.equalsIgnoreCase(Constant.BASIC_TEST)) {
            try {
                ArrayList<TestsAndProcedures> clinicProcedures = basicTestSession.getAllTestAndProcedureCode();
                for (TestsAndProcedures testsAndProcedures : clinicProcedures) {
                    clinicListDesc.add(testsAndProcedures.getProcedureDesc());
                    clinicListData.add(testsAndProcedures.getProcedureAmount());
                }
                adapterListDesc.notifyDataSetChanged();
                adapterListData.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        adapterListDesc = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, clinicListDesc);
        lv_clinicproc_selected_title.setAdapter(adapterListDesc);
        lv_clinicproc_selected_title.setEnabled(false);
        setListViewHeightBasedOnItems(lv_clinicproc_selected_title);
        adapterListData = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, clinicListData);
        lv_clinicproc_selected_data.setAdapter(adapterListData);
        lv_clinicproc_selected_data.setEnabled(false);
        setListViewHeightBasedOnItems(lv_clinicproc_selected_data);
    }

    private void procedureDisplay() {

        String clinicProcedures = "";

        for (int i = 0; i < clinicProcCode.size(); i++) {
            if (i == 0)
                clinicProcedures += clinicProcCode.get(i);
            else
                clinicProcedures += ", " + clinicProcCode.get(i);
        }


        result_cv_basictest_selected_box.setVisibility(View.GONE);
        result_tv_subitem_sixth_first.setText(remarks);
        result_cv_second_box.setVisibility(View.VISIBLE);
        second_box_tv_reasonforconsult_title.setText("Primary Diagnosis:");
        second_box_tv_reasonforconsult_desc.setText(diagnosis);
        result_cv_third.setVisibility(View.GONE);
        result_cv_fourth.setVisibility(View.GONE);
        result_cv_fifth_box.setVisibility(View.GONE);
        result_cv_sixth_box.setVisibility(View.VISIBLE);
        tv_grandTotal.setVisibility(View.VISIBLE);
        tv_priceGrandTotal.setVisibility(View.VISIBLE);
        tv_priceGrandTotal.setText("P " + totalTestAmount);
        result_cv_clinicproc_selected_box.setVisibility(View.VISIBLE);


        insertToPdf(batchCode,
                ORIGIN,
                birthday,
                companyName,
                fullName,
                age,
                gender,
                memberCode,
                doctor,
                problem,
                remarks,
                diagnosis, //testing to see the value of primary diag in clinic procedures
                approval_no,
                procedures,
                approval_no,
                clinicProcedures,
                icd10Code); //clinicProcedure tests
    }

    private void emergencyRoomDisplay() {


        String dateAdmitted;
        String erReason;
        //display for ER
        //hiding the last layout
//        content_loa_approval_linear_header.setVisibility(LinearLayout.GONE);

        result_tv_remark.setVisibility(View.GONE);
        result_tv_status.setVisibility(View.GONE);
        result_tv_instructions.setText(getString(R.string.er_sent));
        result_tv_disclaimer.setVisibility(View.GONE);

        result_tv_ref_no.setVisibility(View.GONE);

        result_tv_mem_code.setText("Medicard Account/ID Number");


        result_tv_mem_name.setText("Member Name:");
        result_tv_age.setText("Company Name:");
        result_tv_gender.setVisibility(View.GONE);
        result_tv_birthday.setVisibility(View.GONE);
        result_tv_com_name.setVisibility(View.VISIBLE);
        result_tv_date_req.setVisibility(View.GONE);
        result_tv_date_req_data.setVisibility(View.GONE);


        if (getIntent().getExtras().getString("ER_REASON").equals(""))
            erReason = NOT_SPECIFIED;
        else
            erReason = getIntent().getExtras().getString("ER_REASON");


        if (getIntent().getExtras().getString("DATEADMITTED").equals(""))
            dateAdmitted = NOT_SPECIFIED;
        else
            dateAdmitted = getIntent().getExtras().getString("DATEADMITTED");

        result_tv_ref_no.setVisibility(View.GONE);
        result_tv_ref_no_data.setVisibility(View.GONE);
        result_tv_mem_code_data.setText(memberCode);
        result_tv_mem_name_data.setText(fullName);
        result_tv_age.setVisibility(View.GONE);
        result_tv_age_data.setVisibility(View.GONE);
        result_tv_gender.setVisibility(View.GONE);
        result_tv_gender_data.setVisibility(View.GONE);
        result_tv_birthday.setVisibility(View.GONE);
        result_tv_birthday_data.setVisibility(View.GONE);
        result_tv_com_name_data.setText(companyName);


        //title in first cardview
        second_box_tv_reasonforconsult_title.setText("Chief Complaint:");
        //sub item in first cardview
        second_box_tv_reasonforconsult_desc.setText(erReason);
        //sixth cardview
        result_tv_item_sixth.setText("Date Admitted:");
        result_tv_subitem_sixth_first.setText(dateAdmitted);


        //first cardview
        result_cv_first_box.setVisibility(View.GONE);
        //third cardview
        result_cv_third.setVisibility(View.GONE);
        //fourth cardview
        result_cv_fourth.setVisibility(View.GONE);
        //fifth cardview
        result_cv_fifth_box.setVisibility(View.GONE);

        tv_footer_first.setVisibility(View.GONE);
        tv_footer_third_val_date.setVisibility(View.GONE);
        result_lv_footer_first_btn_ok.setVisibility(View.VISIBLE);
        result_lv_footer_first_btn_download.setVisibility(View.GONE);
        result_lv_footer_first_btn_download_test.setVisibility(View.GONE);


        result_lv_footer_second.setVisibility(View.GONE);
    }

    private void inPatientDisplay() {
        String room_number;
        String room_price;
        String room_category;
        String procedures;
        String dateAdmitted;
        String room_plan;


//        if (getIntent().getExtras().getString("PROCEDURE").equals(""))
//            procedures = NOT_SPECIFIED;
//        else
//            procedures = getIntent().getExtras().getString("PROCEDURE");

        if (getIntent().getExtras().getString("ROOM_PLAN").equals(""))
            room_plan = NOT_SPECIFIED;
        else
            room_plan = getIntent().getExtras().getString("ROOM_PLAN");

        if (getIntent().getExtras().getString("ROOM_NUMBER").equals(""))
            room_number = NOT_SPECIFIED;
        else
            room_number = getIntent().getExtras().getString("ROOM_NUMBER");

        if (getIntent().getExtras().getString("ROOM_PRICE").equals(""))
            room_price = NOT_SPECIFIED;
        else
            room_price = getIntent().getExtras().getString("ROOM_PRICE");

        if (getIntent().getExtras().getString("CATEGORY").equals(""))
            room_category = NOT_SPECIFIED;
        else
            room_category = getIntent().getExtras().getString("CATEGORY");

        if (getIntent().getExtras().getString("DATEADMITTED").equals(""))
            dateAdmitted = NOT_SPECIFIED;
        else
            dateAdmitted = getIntent().getExtras().getString("DATEADMITTED");

        //for doctor
        arraylistDocList= new ArrayList<>();
        arraylistDocDesc = new ArrayList<>();
        arraylistDocList.clear();
        arraylistDocDesc.clear();
        ArrayList<DoctorModelInsertion> doctorModelInsertions = inpatientSession.getAllSelectedDoctor();
        for (DoctorModelInsertion doctorModelInsertion : doctorModelInsertions) {
            arraylistDocList.add(doctorModelInsertion.getDocLname() + ", " + doctorModelInsertion.getDocFname() + " "+doctorModelInsertion.getDocMname());
            arraylistDocDesc.add(doctorModelInsertion.getSpecDesc());
        }
        adapterDocList = new ArrayAdapter<String>(context, R.layout.result_list_item, arraylistDocList);
        lv_inpatient_doctor_title.setAdapter(adapterDocList);
        lv_inpatient_doctor_title.setEnabled(false);
        setListViewHeightBasedOnItems(lv_inpatient_doctor_title);
        adapterDocDesc = new ArrayAdapter<String>(context,  R.layout.result_list_item, arraylistDocDesc);
        lv_inpatient_doctor_desc.setAdapter(adapterDocDesc);
        lv_inpatient_doctor_desc.setEnabled(false);
        setListViewHeightBasedOnItems(lv_inpatient_doctor_desc);
        adapterDocList.notifyDataSetChanged();
        adapterDocDesc.notifyDataSetChanged();

        //for diagnosis
        arraylistDiagList= new ArrayList<>();
        arraylistDiagDesc = new ArrayList<>();
        arraylistDiagList.clear();
        arraylistDiagDesc.clear();
        ArrayList<DiagnosisList> diagnosisModelInsertion = inpatientSession.getAllSelectedDiagnosis();
        for (DiagnosisList diagnosisList : diagnosisModelInsertion) {
            arraylistDiagList.add(diagnosisList.getDiagDesc());
            arraylistDiagDesc.add(diagnosisList.getIcd10Code());
        }
        adapterDiagList = new ArrayAdapter<String>(context, R.layout.result_list_item, arraylistDiagList);
        lv_inpatient_diagnosis_title.setAdapter(adapterDiagList);
        lv_inpatient_diagnosis_title.setEnabled(false);
        setListViewHeightBasedOnItems(lv_inpatient_diagnosis_title);
        adapterDiagDesc = new ArrayAdapter<String>(context,  R.layout.result_list_item, arraylistDiagDesc);
        lv_inpatient_diagnosis_desc.setAdapter(adapterDiagDesc);
        lv_inpatient_diagnosis_desc.setEnabled(false);
        setListViewHeightBasedOnItems(lv_inpatient_diagnosis_desc);
        adapterDiagList.notifyDataSetChanged();
        adapterDiagDesc.notifyDataSetChanged();



        //for procedure
        arraylistProcList= new ArrayList<>();
        arraylistProcDesc = new ArrayList<>();
        arraylistProcList.clear();
        arraylistProcDesc.clear();
        ArrayList<TestsAndProcedures> procedureModelInsertion = inpatientSession.getAllSelectedProcedure();
        for (TestsAndProcedures testsAndProcedures : procedureModelInsertion) {
            arraylistProcList.add(testsAndProcedures.getProcedureDesc());
            arraylistProcDesc.add("P "+testsAndProcedures.getProcedureAmount());
        }
        adapterProcList = new ArrayAdapter<String>(context, R.layout.result_list_item, arraylistProcList);
        lv_inpatient_procedure_title.setAdapter(adapterProcList);
        lv_inpatient_procedure_title.setEnabled(false);
        setListViewHeightBasedOnItems(lv_inpatient_procedure_title);
        adapterProcDesc = new ArrayAdapter<String>(context,  R.layout.result_list_item, arraylistProcDesc);
        lv_inpatient_procedure_desc.setAdapter(adapterProcDesc);
        lv_inpatient_procedure_desc.setEnabled(false);
        setListViewHeightBasedOnItems(lv_inpatient_procedure_desc);
        adapterProcList.notifyDataSetChanged();
        adapterProcDesc.notifyDataSetChanged();


        //new layout for in patient
        result_tv_remark.setVisibility(View.GONE);
        result_tv_status.setVisibility(View.GONE);
        result_tv_instructions.setText(getString(R.string.in_patient_disclaimer));
        result_tv_disclaimer.setVisibility(View.GONE);


        result_tv_ref_no.setVisibility(View.GONE);
        result_tv_ref_no_data.setVisibility(View.GONE);
        result_tv_mem_code.setText("MediCard Account:");
        result_tv_mem_name.setText("Member Name:");
        result_tv_age.setText("Age:");
        result_tv_gender.setText("Gender:");
        result_tv_birthday.setVisibility(View.GONE);
        result_tv_birthday_data.setVisibility(View.GONE);
        result_tv_com_name.setText("Company Name:");
        result_tv_date_req.setVisibility(View.GONE);
        result_tv_date_req_data.setVisibility(View.GONE);


        result_cv_box_hospital.setVisibility(View.VISIBLE);
        tv_box_hospital_title.setText("Clinic/Hospital");
        tv_box_hospital_name.setText(hospitalName);
        tv_box_hospital_desc.setText(hospitalAddress);

        result_cv_first_box.setVisibility(View.GONE);
        result_cv_second_box.setVisibility(View.GONE);

        result_cv_inpatient_doctor_box.setVisibility(View.VISIBLE);
        inpatient_doctor_title.setText("Physician:");


        result_cv_inpatient_diagnosis_box.setVisibility(View.VISIBLE);
        inpatient_diagnosis_title.setText("Diagnosis:");

        result_cv_inpatient_procedure_box.setVisibility(View.VISIBLE);
        inpatient_procedure_title.setText("Procedure:");




        third_box_tv_roomplanavailed_title.setText("Room Rate:");
        third_box_tv_roomplanavailed_desc.setText("P "+ room_price);
        fourth_box_tv_roomcategory_title.setText("Room Category:");
        fourth_box_tv_roomcategory_desc.setText(room_category);
        fifth_box_tv_roomno_title.setText("Room Number:");
        fifth_box_tv_roomno_desc.setText(room_number);
        result_tv_item_sixth.setText("Date Admitted:");
        result_tv_subitem_sixth_first.setText(dateAdmitted);

        tv_footer_first.setVisibility(View.GONE);
        tv_footer_third_val_date.setVisibility(View.GONE);

        result_lv_footer_first_btn_ok.setVisibility(View.VISIBLE);
        result_lv_footer_first_btn_download.setVisibility(View.GONE);
        result_lv_footer_first_btn_download_test.setVisibility(View.GONE);


        result_lv_footer_second.setVisibility(View.GONE);
    }

    private void basicTestDisplay() {

        String basictest = "";

        for (int i = 0; i < procCode.size(); i++) {
            if (i == 0)
                basictest += procCode.get(i);
            else
                basictest += ", " + procCode.get(i);
        }


        tv_basictest_total.setText("P " + totalTestAmount);
        tv_priceGrandTotal.setText("P " + totalTestAmount);
        tv_proc_title.setText("Basic Tests:");
        result_cv_basictest_selected_box.setVisibility(View.GONE);
        result_cv_clinicproc_selected_box.setVisibility(View.VISIBLE);
        try {
            tv_basictest_selected_first.setText(procCode.get(0));
            tv_basictest_selected_first_data.setText(amount.get(0));
        } catch (Exception e) {
            e.printStackTrace();
            tv_basictest_selected_first.setVisibility(View.GONE);
            tv_basictest_selected_first_data.setVisibility(View.GONE);
        }
        try {
            tv_basictest_selected_second.setText(procCode.get(1));
            tv_basictest_selected_second_data.setText(amount.get(1));
        } catch (Exception e) {
            e.printStackTrace();
            tv_basictest_selected_second.setVisibility(View.GONE);
            tv_basictest_selected_second_data.setVisibility(View.GONE);
        }
        try {
            tv_basictest_selected_third.setText(procCode.get(2));
            tv_basictest_selected_third_data.setText(amount.get(2));
        } catch (Exception e) {
            e.printStackTrace();
            tv_basictest_selected_third.setVisibility(View.GONE);
            tv_basictest_selected_third_data.setVisibility(View.GONE);
        }
        try {
            tv_basictest_selected_fourth.setText(procCode.get(3));
            tv_basictest_selected_fourth_data.setText(amount.get(3));
        } catch (Exception e) {
            e.printStackTrace();
            tv_basictest_selected_fourth.setVisibility(View.GONE);
            tv_basictest_selected_fourth_data.setVisibility(View.GONE);
        }
        try {
            tv_basictest_selected_fifth.setText(procCode.get(4));
            tv_basictest_selected_fifth_data.setText(amount.get(4));
        } catch (Exception e) {
            e.printStackTrace();
            tv_basictest_selected_fifth.setVisibility(View.GONE);
            tv_basictest_selected_fifth_data.setVisibility(View.GONE);
        }
        try {
            tv_basictest_selected_sixth.setText(procCode.get(5));
            tv_basictest_selected_sixth_data.setText(amount.get(5));
        } catch (Exception e) {
            e.printStackTrace();
            tv_basictest_selected_sixth.setVisibility(View.GONE);
            tv_basictest_selected_sixth_data.setVisibility(View.GONE);
        }


        insertToPdf(batchCode,
                ORIGIN,
                birthday,
                companyName,
                fullName,
                age,
                gender,
                memberCode,
                doctor,
                problem,
                remarks,
                "",
                approval_no,
                basictest,
                "",
                "",
                "");
    }


    public void othertestDisplay() {
        result_tv_remark.setVisibility(View.GONE);
        result_tv_status.setVisibility(View.GONE);

        tv_box_hospital_title.setText("Clinic/Hospital where consultation was made:");
        tv_box_hospital_name.setText(unfiltered_hospitalName);
        tv_box_hospital_desc.setText(unfiltered_hospitalAddress);

        cv_othertest.setVisibility(View.VISIBLE);
        tv_othertest_label.setText("Other Test:");

//        loa = (LoaList) getIntent().getExtras().get("LOALIST");
        ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> unfilteredGrouping = new ArrayList<>();
        for (LoaList.GroupedByCostCenter groupedByCostCenter : loa.getGroupedByCostCenters()) {
            for (int i = 0; i < groupedByCostCenter.getGroupedByDiag().size(); i++) {
                LoaList.GroupedByCostCenter.GroupedByDiag groupedByDiag = groupedByCostCenter.getGroupedByDiag().get(i);
//                groupedByDiag.setBasicTestFlag(loa.getRequestType().contains("BASIC TEST") ? true : false);
                groupedByDiag.setCostCenter(groupedByCostCenter.getCostCenter());
                groupedByDiag.setStatus(groupedByCostCenter.getStatus());
                groupedByDiag.setSubTotal(groupedByCostCenter.getSubTotal());
                unfilteredGrouping.add(groupedByDiag);
            }
        }
        //Process and group by DiagType -> Tests + CostCenter
        ARRAYLIST_GroupedByDiag.addAll(processGrouping(unfilteredGrouping));
        otherTestLoaAdapter = new OtherTestLoaAdapter(context, ARRAYLIST_GroupedByDiag);
        rv_othertest_diagnosis.setLayoutManager(new LinearLayoutManager(this));
        rv_othertest_diagnosis.setAdapter(otherTestLoaAdapter);

        tv_othertest_priceGrandTotal.setText("P " + loa.getTotalAmount());
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> processGrouping(ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> gbdList) {
        ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> returnedList = new ArrayList<>();
        ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> primaryList = arrangeListByDiagType(gbdList, 1);
        ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> otherList = arrangeListByDiagType(gbdList, 2);

        returnedList.addAll(setSubTotalAndIndices(primaryList));
        returnedList.addAll(setSubTotalAndIndices(otherList));

        return returnedList;
    }

    private ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> arrangeListByDiagType(ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> gbdList, int diagType) {
        int a = 0;
        ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> primaryList = new ArrayList<>();
        for (LoaList.GroupedByCostCenter.GroupedByDiag groupedByDiag : gbdList) {
            if (groupedByDiag.getDiagType() == diagType) {
                groupedByDiag.setFirstInstance(a++ == 0 ? true : false);
                groupedByDiag.setLastInstance(false);
                groupedByDiag.setBasicTestFlag(false);
                primaryList.add(groupedByDiag);
            }
        }
        return primaryList;
    }

    private ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> setSubTotalAndIndices(ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> primaryList) {
        //Subtotal of CostCenter
        String costCenter = "";
        Double subTotal = 0.0;
        for (int i = 0; i < primaryList.size(); i++) {
            //Set initial CostCenter
            if (i == 0)
                costCenter = primaryList.get(i).getCostCenter();
            //Check if grouped CostCenter is equal to current CostCenter
            if (!primaryList.get(i).getCostCenter().equals(costCenter)) {
                costCenter = primaryList.get(i).getCostCenter();
                primaryList.get(i - 1).setSubTotal(String.valueOf(subTotal));
                primaryList.get(i - 1).setLastInstance(true);
                subTotal = 0.0;
            }
            for (LoaList.MappedTest mappedTest : primaryList.get(i).getMappedTests()) {
                subTotal += Double.valueOf(mappedTest.getAmount());
            }
            if (i == primaryList.size() - 1) {
                primaryList.get(i).setSubTotal(String.valueOf(subTotal));
                primaryList.get(i).setLastInstance(true);
            }
        }

        return primaryList;
    }

    private void showUserDetails() {

        //test  // display for approval no.
        result_tv_ref_no.setText("Reference Code:");
        result_tv_ref_no_data.setText(approval_no);
        //test  // display for membercode
        result_tv_mem_code.setText("MediCard Account:");
        result_tv_mem_code_data.setText(memberCode);
        //test // display for member name
        result_tv_mem_name.setText("Member Name:");
        result_tv_mem_name_data.setText(fullName);
        //test  // display for member age
        result_tv_age.setText("Age:");
        result_tv_age_data.setText(age.substring(0, 2));
        ////test  // display for member gender
        Log.e("gender", gender);
        result_tv_gender.setText("Gender:");
        result_tv_gender_data.setText(gender);

        //test  // display for member birthday
        result_tv_birthday.setText("Birthdate:");
        result_tv_birthday_data.setText(birthday);
        //test  // display for member company name
        result_tv_com_name.setText("Company Name:");
        result_tv_com_name_data.setText(companyName);

        result_tv_date_req.setVisibility(View.GONE);
        result_tv_date_req_data.setVisibility(View.GONE);

        //test  // display for member doctor
        first_box_tv_physician_title.setText("Physician:");
        first_box_tv_physician_name.setText(doctor);
        first_box_tv_physician_desc.setText(doctorSpec);

        result_cv_box_hospital.setVisibility(View.VISIBLE);
        tv_box_hospital_title.setText("Clinic/Hospital:");
        tv_box_hospital_name.setText(hospitalName);
        tv_box_hospital_desc.setText(hospitalAddress);

        //TODO: hide temporarily
        result_cv_seventh_box.setVisibility(View.VISIBLE);
        result_tv_item_seventh.setText("Coordinator:");
        result_tv_subitem_seventh_first.setText("Contact Number:\n" + SharedPref.getStringValue(SharedPref.USER, SharedPref.CONTACT_NO, context));
        result_tv_subitem_seventh_second.setText("Email:\n" + SharedPref.getStringValue(SharedPref.USER, SharedPref.EMAIL, context));

    }

    private void consultationAndMaternityDisplay() {
        problem = getIntent().getExtras().getString("PROBLEM");
        batchCode = getIntent().getExtras().getString("BATCHCODE");


        result_cv_second_box.setVisibility(View.VISIBLE);
        second_box_tv_reasonforconsult_title.setText("Reason for Consult:");
        second_box_tv_reasonforconsult_desc.setText(problem);

        insertToPdf(batchCode,
                ORIGIN,
                birthday,
                companyName,
                fullName,
                age,
                gender,
                memberCode,
                doctor,
                problem,
                remarks,
                "",
                approval_no,
                "",
                "",
                "",
                ""); //adding null values since no values to insert

        //hide unused cardviews
        result_cv_third.setVisibility(View.GONE);
        result_cv_fourth.setVisibility(View.GONE);
        result_cv_fifth_box.setVisibility(View.GONE);
        result_cv_sixth_box.setVisibility(View.GONE);
    }


    private void insertToPdf(String batchCode, String request, String birthday, String companyName, String fullName, String age, String gender,
                             String memberCode, String doctor, String problem, String remarks, String clinicProceduresPrimaryDiag, String approval_no, String basictest, String proc_approval, String clinicProceduresTests, String icd10CodeForClinicProcedures) {

        String rem = SharedPref.getStringValue(SharedPref.USER, SharedPref.REMARKS, this);

        try {
            loaFormBuilder = new OutPatientConsultationForm.Builder()
                    .date(DateConverter.convertDateToMMddyyyy(DateConverter.getDate()))
                    .validFrom(DateConverter.convertDateToMMddyyyy(DateConverter.getDate()))
                    .validUntil(DateConverter.validityDatePLusDay(DateConverter.getDate(), 2))
                    .referenceNumber(approval_no)
                    .doctor(doctor)
                    .procedure(basictest)
                    .hospital(SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALNAME, this))
                    .memberName(fullName)
                    .age(age)
                    .gender(gender)
                    .memberId(memberCode)
                    .company(companyName)
                    .remarks(rem)
                    .primaryDianosisWorkingImpression(clinicProceduresPrimaryDiag) //testing for clinicProcedures primary diag write to pdf
                    .chiefComplaint(problem)
                    .dateEffectivity(SharedPref.getStringValue(SharedPref.USER, SharedPref.EFFECTIVE, context))
                    .validityDate(SharedPref.getStringValue(SharedPref.USER, SharedPref.VALID, context))
                    .serviceType(request)
                    .batchCode(batchCode)
                    .procedureDoneInClinicApprovalNo(proc_approval)
                    .prescribedTestForPrimaryDiagnosis(clinicProceduresTests)
                    .icd10Code(icd10CodeForClinicProcedures);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void disableText() {
        BasicTestSession.releaseContent();
        tv_basictest_selected_first.setText("");
        tv_basictest_selected_first_data.setText("");
        tv_basictest_selected_second.setText("");
        tv_basictest_selected_second_data.setText("");
        tv_basictest_selected_third.setText("");
        tv_basictest_selected_third_data.setText("");
        tv_basictest_selected_fourth.setText("");
        tv_basictest_selected_fourth_data.setText("");
        tv_basictest_selected_fifth.setText("");
        tv_basictest_selected_fifth_data.setText("");
        tv_basictest_selected_sixth.setText("");
        tv_basictest_selected_sixth_data.setText("");
    }

    private void showApproved() {

        tv_disclaimer.setVisibility(View.VISIBLE);
        tv_id.setText(memberCode);
        //trial $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        tv_company.setText(companyName);
        tv_doctor.setText(doctor);
        tv_approval.setText(approval_no);
        tv_birthday.setText(birthday);
        tv_name.setText(fullName);
        tv_age.setText(age.substring(0, 2));
        tv_gender.setText(gender);
        tv_remarks.setText(remarks);
        tv_doctor.setText(doctor);
        tv_companyGoneName.setText(companyName);

        //<------------------testing for display--------------------------->
        tv_approval.setText(approval_no);
        //test  // display for approval no.
        result_tv_ref_no_data.setText(approval_no);
        //test  // display for membercode
        result_tv_mem_code_data.setText(memberCode);
        //test // display for member name
        result_tv_mem_name_data.setText(fullName);
        //test  // display for member age
        result_tv_age_data.setText(age.substring(0, 2));
        ////test  // display for member gender
        Log.e("gender", gender);
        result_tv_gender_data.setText(gender);
        //test  // display for member birthday
        result_tv_birthday_data.setText(birthday);
        //test  // display for member company name
        result_tv_com_name_data.setText(companyName);
        //test  // display for member doctor
        first_box_tv_physician_name.setText(doctor);


        result_cv_box_hospital.setVisibility(View.VISIBLE);
        tv_box_hospital_title.setText("Clinic/Hospital:");
        tv_box_hospital_name.setText(hospitalName);
        tv_box_hospital_desc.setText(hospitalAddress);

        //<------------------testing for display--------------------------->

    }

    public void showDisApprovedOrHold() {
        box2.setVisibility(View.GONE);
        tv_company.setVisibility(View.GONE);
        tv_companyHeader.setVisibility(View.INVISIBLE);
        tv_doctor.setVisibility(View.GONE);
        tv_doctorHeader.setVisibility(View.GONE);
        tv_approval.setVisibility(View.GONE);
        tv_approvalHeader.setVisibility(View.INVISIBLE);
        tv_companyGoneName.setVisibility(View.VISIBLE);
        tv_companyGoneHeader.setVisibility(View.VISIBLE);
        tv_disclaimer.setVisibility(View.GONE);
        showApproved();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {


        String ref = "";
        for (int x = 0; x < event.getApprovalNos().size(); x++) {
            ref = ref + event.getApprovalNos().get(x) + "\n";
            Log.d("GETres_NUm", ref);
        }


    }


    public static class MessageEvent {


        private String responseCode;

        private ArrayList<String> approvalNos;

        private String responseDesc;

        private String remarks;
        ArrayList<ProceduresListLocal> arrayProcedure = new ArrayList<>();


        public MessageEvent(String responseCode, String responseDesc, String remarks, ArrayList<String> approvalNos, ArrayList<ProceduresListLocal> arrayProcedure) {
            this.remarks = remarks;
            this.responseCode = responseCode;
            this.responseDesc = responseDesc;
            this.approvalNos = approvalNos;
            this.arrayProcedure = arrayProcedure;
        }

        public MessageEvent(ArrayList<String> approvalNos) {
            this.approvalNos = approvalNos;
        }


        public String getResponseCode() {
            return responseCode;
        }

        public void setResponseCode(String responseCode) {
            this.responseCode = responseCode;
        }

        public ArrayList<String> getApprovalNos() {
            return approvalNos;
        }

        public void setApprovalNos(ArrayList<String> approvalNos) {
            this.approvalNos = approvalNos;
        }

        public String getResponseDesc() {
            return responseDesc;
        }

        public void setResponseDesc(String responseDesc) {
            this.responseDesc = responseDesc;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }


    }


    @Override
    public void onBackPressed() {

    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    private void generateLoaConsultationForm() {
        if (PermissionUtililities.hasPermissionToReadAndWriteStorage(this)) {
            implement.generateLoaForm(loaFormBuilder.build(),
                    getResources().openRawResource(R.raw.loa_consultation_form), pdfCallback);
        }

    }

    private void generateTestForm() {
//        Log.d("builder", loaFormBuilder.toString());
        if (PermissionUtililities.hasPermissionToReadAndWriteStorage(this)) {
            GenerateForm.generateLoaForm(loaFormBuilder.build(),
                    getResources().openRawResource(R.raw.test_form), pdfCallback);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // Timber.d("request code for storate %s permission response %s", PermissionUtililities.READ_WRITE_PERMISSION, requestCode);
        switch (requestCode) {
            case PermissionUtililities.REQUESTCODE_STORAGE_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {


                    generateLoaConsultationForm();


                } else {
                    //  Timber.d("permission denied");
                }
            }
            System.out.println();
            return;
        }
    }


    @Override
    public void onGenerateLoaFormSuccess(String serviceType, String referenceNumber) {
        isFileExisting = CheckFileExistence.fileExistance(ORIGIN, approval_no);
        implement.setDownloadButton(result_lv_footer_first_btn_download, result_lv_footer_first_btn_download_test, isFileExisting);
        alertyDialog.showMePrintableLOA(context, "", getString(R.string.success_download), serviceType, referenceNumber);

    }

    @Override
    public void onGenerateLoaFormError() {

    }

    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                float px = 500 * (listView.getResources().getDisplayMetrics().density);
                item.measure(View.MeasureSpec.makeMeasureSpec((int) px, View.MeasureSpec.AT_MOST), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);
            // Get padding
            int totalPadding = listView.getPaddingTop() + listView.getPaddingBottom();

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight + totalPadding;
            listView.setLayoutParams(params);
            listView.requestLayout();
            return true;

        } else {
            return false;
        }

    }

}