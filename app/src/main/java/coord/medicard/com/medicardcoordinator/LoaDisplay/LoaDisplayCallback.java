package coord.medicard.com.medicardcoordinator.LoaDisplay;

/**
 * Created by mpx-pawpaw on 5/3/17.
 */

public interface LoaDisplayCallback {


    void onGenerateLoaFormSuccess();

    void onGenerateLoaFormError();
}
