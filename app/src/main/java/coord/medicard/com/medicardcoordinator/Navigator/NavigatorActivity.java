package coord.medicard.com.medicardcoordinator.Navigator;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import adapter.InpatientDiagnosisSelectAdapter_Navigator;
import adapter.InpatientDoctorSelectAdapter_Navigator;
import adapter.InpatientProcedureSelectAdapter_Navigator;
import adapter.ProceduresAdapter2;
import adapter.UnfilteredDoctorListAdapter;
import adapter.UnfilteredHospitalAdapter;
import coord.medicard.com.medicardcoordinator.CoordinatorMenu.CoordinatorMenuActivity;

import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.ArrayList;


import adapter.DiagnosisAdapter;
import adapter.DiagnosisMultiSelect;
import adapter.DoctorListAdapter;
import adapter.DoctorMultipleSelectionAdapter;
import adapter.OtherServicesAdapter;
import adapter.ProceduresAdapter;
import adapter.RoomCategoryAdapter;
import adapter.RoomsAvailAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import coord.medicard.com.medicardcoordinator.R;
import de.hdodenhof.circleimageview.CircleImageView;
import fragment.Avail.AvailServices;
import fragment.CoordinatorMenuButton;
import fragment.InPatient.InPatientAdmitted;
import fragment.StatusNotActive;
import model.BasicTests;
import model.DiagnosisList;
import model.DoctorModelInsertion;
import model.DoctorModelMultipleSelection;
import model.Hospital;
import model.InPatient;
import model.LoaList;
import model.Rooms;
import model.Services;
import model.TestsAndProcedures;
import services.AppInterface;
import services.ItemClickListener;
import utilities.AlertDialogCustom;
import utilities.Constant;
import utilities.DataBaseHandler;
import utilities.DatepickerSet;
import utilities.ImageConverters;
import utilities.Permission;
import utilities.SearchToCoordinatorMenu;
import utilities.SharedPref;
import utilities.Showcase;
import utilities.StatusSetter;
import utilities.TimepickerSet;

public class NavigatorActivity extends AppCompatActivity implements ItemClickListener,
        NavigatorRetrieve.NavigatorInterface, ConsultationAdapter.ConsultationListInterface,
        Showcase.ShowCaseCallBack {

    //CONSTANT
    boolean isProcedure = false;
    boolean isUnfilterdDoctor = false;
    boolean isUnfilterdHosp = false;
    boolean isDoctor = false;
    boolean isDoctor_in_Patient = false;
    boolean isDiagnosis = false;
    boolean isPrimary = false;
    boolean isPrimaryDiag = false;
    boolean isOther = false;
    boolean isOtherDiag = false;
    boolean isOtherDiagSecond = false;
    boolean isBasicTest = false;
    boolean isProcedure_in_Patient = false;
    boolean isProcedure_in_Patient_Doctor = false;
    boolean isDiagnosis_in_Patient = false;
    boolean isDiagnosis_in_Patient_doctor_main = false;
    boolean isOtherServices = false;
    boolean isRoomPlan = false;
    boolean isRoomCategory = false;

    boolean isInpatientDoctor = false;
    boolean isInpatientDiagnosis = false;
    boolean isInpatientProcedure = false;


    boolean isInPatientAlreadyAvailable = false;

    int Consultation_Origin;
    private int Basic_Test = 1;
    private int OtherTest = 2;
    private int Procedure = 3;
    private String DERMATOLOGY = "DERMATOLOGY";
    public String CAMERA = "Camera";
    final int CAMERA_RQ = 200;

    //VIEW
    @BindView(R.id.tv_inpatient_diagnosis)
    TextView tv_primary_diag_in_patient;
    @BindView(R.id.tv_inpatient_doctor)
    TextView tv_doctor_in_patient;
    @BindView(R.id.tv_admitted)
    TextView tv_admitted;
    @BindView(R.id.tv_admitted_time)
    TextView tv_admitted_time;
    @BindView(R.id.tv_input_room_availed)
    TextView tv_input_room_availed;
    //    @BindView(R.id.ll_request_details)
//    LinearLayout ll_request_details;
    @BindView(R.id.tv_primary_diagnosis)
    TextView tv_primary_diagnosis;
    @BindView(R.id.tv_doc_name)
    TextView tv_doc_name;
    @BindView(R.id.tv_doc_spec)
    TextView tv_doc_spec;
    @BindView(R.id.RL_layout)
    RelativeLayout RL_layout;

    @BindView(R.id.btn_consultation_list)
    LinearLayout btn_consultation_list;
    @BindView(R.id.ll_in_patient_available)
    LinearLayout ll_in_patient_available;
    @BindView(R.id.pb_loading_list)
    ProgressBar pb_loading_list;
    @BindView(R.id.tv_text_list)
    TextView tv_text_list;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tv_others)
    TextView tv_others;
    @BindView(R.id.tv_status)
    TextView tv_status;
    @BindView(R.id.tv_memberCode)
    TextView tv_memberCode;
    @BindView(R.id.tv_memberType)
    TextView tv_memberType;
    @BindView(R.id.tv_effectiveDate)
    TextView tv_effectiveDate;
    @BindView(R.id.tv_validityDate)
    TextView tv_validityDate;
    @BindView(R.id.tv_nav_notification)
    TextView tv_nav_notification;
    @BindView(R.id.tv_ddLimit)
    TextView tv_ddLimit;
    @BindView(R.id.tv_pec_non_dd)
    TextView tv_pec_non_dd;
    @BindView(R.id.tv_gender)
    TextView tv_gender;
    @BindView(R.id.tv_company)
    TextView tv_company;
    @BindView(R.id.tv_age)
    TextView tv_age;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_mappedTest)
    TextView tv_mappedTest;
    @BindView(R.id.tv_allTest)
    TextView tv_allTest;
    @BindView(R.id.tv_id)
    TextView tv_id;
    @BindView(R.id.btn_back)
    ImageButton btn_back;

    @BindView(R.id.ed_searchDoctor)
    EditText ed_searchDoctor;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ci_user)
    CircleImageView ci_user;
    @BindView(R.id.ll_out_patient_result)
    LinearLayout ll_out_patient_result;


    @BindView(R.id.content_navigator)
    LinearLayout content_navigator;
    File output = null;
    DrawerLayout drawer;
    FrameLayout container;
    Fragment fragment;
    FragmentTransaction fragmentTransaction;
    RecyclerView rv_doctors;
    RecyclerView rv_MappedTest;
    RecyclerView rv_req_hosp;
    LinearLayoutManager llm;
    ProgressDialog pd;


    //ADAPTER
    DoctorListAdapter doctorListAdapter;
    UnfilteredDoctorListAdapter unfilteredDoctorListAdapter;
    UnfilteredHospitalAdapter unfilteredHospitalAdapter;
    ProceduresAdapter proceduresAdapter;
    ProceduresAdapter2 proceduresAdapterMappedTest;
    DiagnosisAdapter diagnosisAdapter;
    DiagnosisAdapterSingleAdd diagnosisAdapterSingleAdd;
    DoctorMultipleSelectionAdapter doctorMultipleSelectionAdapter;
    OtherServicesAdapter otherServicesAdapter;
    DiagnosisMultiSelect diagnosisMultiSelectAdapter;
    RoomsAvailAdapter roomsAvailAdapter;
    RoomCategoryAdapter roomCategoryAdapter;
    InpatientDoctorSelectAdapter_Navigator inpatientDoctorSelectAdapterNavigator;
    InpatientDiagnosisSelectAdapter_Navigator inpatientDiagnosisSelectAdapterNavigator;
    InpatientProcedureSelectAdapter_Navigator inpatientProcedureSelectAdapterNavigator;


    //VARIABLE
    DiagnosisList inPatientAdmittedDiagnosis;
    DoctorModelInsertion inPatientAdmittedDoctor;
    String inPatientDateAdmitted;
    String inPatientTimeAdmitted;
    Rooms roomPlan;
    //USED TO DEFINE WHAT SPECIFIC DOCTOR WILL BE USED
    //MATERNITY MUST USE ONLY OBGY
    private String IsFromMaternityOrigin = "";


    ArrayList<LoaList> consultationList = new ArrayList<>();
    NavigatorRetrieve.NavigatorInterface callback;
    String ORIGIN_PROC;
    String PANEL = "";
    String[] procedurePrimaryCodes;
    String[] procedureOtherCodes;
    String[] procedurePrimaryCodes2;
    ArrayList<DoctorModelInsertion> arrayListDoctors = new ArrayList<>();
    ArrayList<Hospital> hospitalArrayList = new ArrayList<>();
    ArrayList<TestsAndProcedures> arrayListProcedure = new ArrayList<>();
    ArrayList<TestsAndProcedures> arrayListProcedureMappedTest = new ArrayList<>();
    ArrayList<DiagnosisList> arrayListDiagnosis = new ArrayList<>();

    ArrayList<DoctorModelInsertion> arrayListInpatientDoctor = new ArrayList<>();
    ArrayList<DiagnosisList> arrayListInpatientDiagnosis = new ArrayList<>();
    ArrayList<TestsAndProcedures> arrayListInpatientProcedure = new ArrayList<>();

    ArrayList<DoctorModelMultipleSelection> arrayListDoctorMultiSelect = new ArrayList<>();
    ArrayList<Services> arrayOtherServices = new ArrayList<>();
    ArrayList<Rooms> arrayRooms = new ArrayList<>();
    ArrayList<String> arrayRoomCategory = new ArrayList<>();
    ActionBarDrawerToggle toggle;
    Context context;
    DataBaseHandler databaseH;
    String MEMBERID = "";
    String diagToExclude = "";


    //PRESENTER
    ConsultationAdapter.ConsultationListInterface listcallback;
    AlertDialogCustom alertDialogCustom = new AlertDialogCustom();

    NavigatorRetrieve implement;
    Showcase.ShowCaseCallBack caseCallBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigator);
        context = this;
        listcallback = this;
        ButterKnife.bind(this);
        callback = this;
        caseCallBack = this;
        implement = new NavigatorRetrieve(context, callback);
        try {
            databaseH = new DataBaseHandler(context);
        } catch (Exception e) {
            Log.e("ERROR", e.getMessage());
        }

        //Trial for excluding the selected primary diag to the list
//        try{
//            diagToExclude = SharedPref.getStringValue(SharedPref.USER,SharedPref.DIAGTOEXCLUDE,context);
//        }catch (Exception e){
//            e.printStackTrace();
//        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        container = (FrameLayout) findViewById(R.id.container);
        ed_searchDoctor = (EditText) findViewById(R.id.ed_searchDoctor);
        rv_doctors = (RecyclerView) findViewById(R.id.rv_doctors);
        rv_MappedTest = (RecyclerView) findViewById(R.id.rv_MappedTest);
//        implement.setConsultVisible(btn_consultation_list, true);

        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rv_doctors.setLayoutManager(llm);
        rv_MappedTest.setLayoutManager(new LinearLayoutManager(this));


        doctorListAdapter = new DoctorListAdapter(context, arrayListDoctors, tv_nav_notification);
        unfilteredDoctorListAdapter = new UnfilteredDoctorListAdapter(context, arrayListDoctors, tv_nav_notification);
        unfilteredHospitalAdapter = new UnfilteredHospitalAdapter(context, hospitalArrayList, tv_nav_notification, callback);
        diagnosisAdapterSingleAdd = new DiagnosisAdapterSingleAdd(callback, databaseH, context, arrayListDiagnosis);
        proceduresAdapter = new ProceduresAdapter(implement, context, arrayListProcedure, tv_nav_notification, ORIGIN_PROC);


        proceduresAdapterMappedTest = new ProceduresAdapter2(implement, context, arrayListProcedureMappedTest, tv_nav_notification, ORIGIN_PROC);
        inpatientDoctorSelectAdapterNavigator = new InpatientDoctorSelectAdapter_Navigator(context, arrayListInpatientDoctor,databaseH);
        inpatientDiagnosisSelectAdapterNavigator = new InpatientDiagnosisSelectAdapter_Navigator(context, arrayListInpatientDiagnosis,databaseH);
        inpatientProcedureSelectAdapterNavigator = new InpatientProcedureSelectAdapter_Navigator(context, arrayListInpatientProcedure,databaseH);

        diagnosisAdapter = new DiagnosisAdapter(context, arrayListDiagnosis, tv_nav_notification);
        doctorMultipleSelectionAdapter = new DoctorMultipleSelectionAdapter(context, arrayListDoctorMultiSelect, databaseH);
        otherServicesAdapter = new OtherServicesAdapter(databaseH, context, arrayOtherServices);
        diagnosisMultiSelectAdapter = new DiagnosisMultiSelect(context, arrayListDiagnosis, databaseH);
        roomsAvailAdapter = new RoomsAvailAdapter(context, arrayRooms, databaseH, callback);
        roomCategoryAdapter = new RoomCategoryAdapter(arrayRoomCategory, context, callback);

        inpatientDoctorSelectAdapterNavigator.setClickListener(this);
        inpatientDiagnosisSelectAdapterNavigator.setClickListener(this);
        inpatientProcedureSelectAdapterNavigator.setClickListener(this);

        doctorListAdapter.setClickListener(this);
        unfilteredDoctorListAdapter.setClickListener(this);
        proceduresAdapter.setClickListener(this);
        diagnosisAdapter.setClickListener(this);

        /**
         * ADD DATA ON BOTH
         */
        arrayListDiagnosis.addAll(databaseH.retrieveDiagnose(""));
        diagnosisAdapter.notifyDataSetChanged();

        ed_searchDoctor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                ed_searchDoctor.setText("");


            }
        });
        ed_searchDoctor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                getCurrentListToRetrieve(s);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        init();


    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }


    @OnClick({R.id.tv_inpatient_diagnosis, R.id.tv_admitted,
            R.id.tv_admitted_time, R.id.tv_inpatient_doctor, R.id.tv_input_room_availed, R.id.btn_consultation_list,})
//    @OnClick({, R.id.tv_primary_diag_in_patient, R.id.tv_admitted,
//            R.id.tv_admitted_time, R.id.tv_doctor_in_patient, R.id.tv_input_room_availed})
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_consultation_list:
                implement.showConsultationListDialog(consultationList, listcallback);
                break;

            case R.id.tv_inpatient_diagnosis:
                setAdapter(Constant.ADAPTER_DIAG_ADMITTED);
                getCurrentListToRetrieve("");
                break;

            case R.id.tv_admitted:
                DatepickerSet.getDate(context, tv_admitted, Constant.FROM_AVIALED);
                break;

            case R.id.tv_admitted_time:
                TimepickerSet.getTime(context, tv_admitted_time, Constant.TIME_ADMITTED);
                break;

            case R.id.tv_inpatient_doctor:
                //todo change this function for update inpatient
                setAdapter(Constant.ADAPTER_INPATIENT_DOCTOR);
                getCurrentListToRetrieve("");
                break;

            case R.id.tv_input_room_availed:
                SharedPref.setStringValue(SharedPref.USER, SharedPref.ROOM_ORIGIN, Constant.ADAPTER_PRE_ROOM_PLANS, context);
                setAdapter(Constant.ADAPTER_ROOM_PLANS);
                getCurrentListToRetrieve("");
                break;

        }
    }


    private void getCurrentListToRetrieve(CharSequence s) {
        if (!isPrimary && !isOther) {
            tv_allTest.setVisibility(View.GONE);
            tv_mappedTest.setVisibility(View.GONE);
        }
        ///IF SEARCH ITEM DOES NOT EXIST
        if (isProcedure) {
            arrayListProcedure.clear();
            arrayListProcedureMappedTest.clear();
//            arrayListProcedure.addAll(databaseH.retrieveProcedure(String.valueOf(s), procedurePrimaryCodes2, 3));
            String forQuery = "";
            if (procedurePrimaryCodes2.length != 0) {
                System.out.println("Has Mapped Tests");
                arrayListProcedure.addAll(databaseH.retrieveProcedure(String.valueOf(s), procedurePrimaryCodes2, 3));
                arrayListProcedure.addAll(databaseH.retrieveProcedureAllWithExceptions(String.valueOf(s), procedurePrimaryCodes2, 3));
                proceduresAdapterMappedTest.notifyDataSetChanged();
            } else {
                System.out.println("No Mapped Tests");
                System.out.println("Query: " + forQuery);
                tv_allTest.setVisibility(View.VISIBLE);
                rv_MappedTest.setVisibility(View.GONE);
                arrayListProcedure.addAll(databaseH.retrieveProcedureAll(String.valueOf(s)));
            }

            proceduresAdapter.notifyDataSetChanged();
            tv_nav_notification.setVisibility(View.GONE);
            rv_doctors.setVisibility(View.VISIBLE);

        } else if (isUnfilterdDoctor) {
            arrayListDoctors.clear();
            arrayListDoctors.addAll(databaseH.retrieveUnfilteredDoctorList(String.valueOf(s), IsFromMaternityOrigin));
            if (arrayListDoctors.size() == 0) {
                tv_nav_notification.setText(R.string.doctor_not_found);
                tv_nav_notification.setVisibility(View.VISIBLE);
                rv_doctors.setVisibility(View.GONE);
            } else {
                unfilteredDoctorListAdapter.notifyDataSetChanged();
                tv_nav_notification.setVisibility(View.GONE);
                rv_doctors.setVisibility(View.VISIBLE);
            }

        } else if (isUnfilterdHosp) {
            hospitalArrayList.clear();
            hospitalArrayList.addAll(databaseH.retrieveUnfilterdHospMedicardOnly(String.valueOf(s)));
            hospitalArrayList.addAll(databaseH.retrieveUnfilterdHosp(String.valueOf(s)));
            if (hospitalArrayList.size() == 0) {
                implement.updateDisplayForNavigator(tv_nav_notification, rv_doctors, getString(R.string.hospital_not_found));
            } else {
                unfilteredHospitalAdapter.notifyDataSetChanged();
                tv_nav_notification.setVisibility(View.GONE);
                rv_doctors.setVisibility(View.VISIBLE);
            }

        } else if (isDoctor || isDoctor_in_Patient || isDiagnosis_in_Patient_doctor_main) {
            arrayListDoctors.clear();
            arrayListDoctors.addAll(databaseH.retrieveDoctorList(String.valueOf(s), IsFromMaternityOrigin));
            if (arrayListDoctors.size() == 0) {
                tv_nav_notification.setText(R.string.doctor_not_found);
                tv_nav_notification.setVisibility(View.VISIBLE);
                rv_doctors.setVisibility(View.GONE);
            } else {
                doctorListAdapter.notifyDataSetChanged();
                tv_nav_notification.setVisibility(View.GONE);
                rv_doctors.setVisibility(View.VISIBLE);
            }

        } else if (isInpatientDoctor) {
            arrayListInpatientDoctor.clear();
            arrayListInpatientDoctor.addAll(databaseH.retrieveDoctorList(String.valueOf(s), IsFromMaternityOrigin));
            if (arrayListInpatientDoctor.size() == 0) {
                tv_nav_notification.setText(R.string.doctor_not_found);
                tv_nav_notification.setVisibility(View.VISIBLE);
                rv_doctors.setVisibility(View.GONE);
            } else {
                inpatientDoctorSelectAdapterNavigator.notifyDataSetChanged();
                tv_nav_notification.setVisibility(View.GONE);
                rv_doctors.setVisibility(View.VISIBLE);
            }
            inpatientDoctorSelectAdapterNavigator.notifyDataSetChanged();
        } else if (isInpatientDiagnosis) {
            arrayListInpatientDiagnosis.clear();
            arrayListInpatientDiagnosis.addAll(databaseH.retrieveDiagnose(String.valueOf(s)));
            if (arrayListInpatientDiagnosis.size() == 0) {
                tv_nav_notification.setText(R.string.doctor_not_found);
                tv_nav_notification.setVisibility(View.VISIBLE);
                rv_doctors.setVisibility(View.GONE);
            } else {
                inpatientDiagnosisSelectAdapterNavigator.notifyDataSetChanged();
                tv_nav_notification.setVisibility(View.GONE);
                rv_doctors.setVisibility(View.VISIBLE);
            }
            inpatientDiagnosisSelectAdapterNavigator.notifyDataSetChanged();
        } else if (isInpatientProcedure) {
            arrayListInpatientProcedure.clear();
            arrayListInpatientProcedure.addAll(databaseH.retrieveProcedureAll(String.valueOf(s)));
            if (arrayListInpatientProcedure.size() == 0) {
                tv_nav_notification.setText(R.string.procedure_not_found);
                tv_nav_notification.setVisibility(View.VISIBLE);
                rv_doctors.setVisibility(View.GONE);
            } else {
                inpatientProcedureSelectAdapterNavigator.notifyDataSetChanged();
                tv_nav_notification.setVisibility(View.GONE);
                rv_doctors.setVisibility(View.VISIBLE);
            }
            inpatientProcedureSelectAdapterNavigator.notifyDataSetChanged();
        } else if (isDiagnosis) {
            arrayListDiagnosis.clear();
            arrayListDiagnosis.addAll(databaseH.retrieveDiagnose(String.valueOf(s)));
            if (arrayListDiagnosis.size() == 0) {
                implement.updateDisplayForNavigator(tv_nav_notification, rv_doctors, getString(R.string.diagnosis_not_found));
            } else {
                diagnosisAdapterSingleAdd.notifyDataSetChanged();
                tv_nav_notification.setVisibility(View.GONE);
                rv_doctors.setVisibility(View.VISIBLE);
            }

        } else if (isPrimary || isProcedure_in_Patient) {
            arrayListProcedure.clear();
            arrayListProcedureMappedTest.clear();
            //If mapped procedures in not empty, concat for query
            String forQuery = "";
            if (procedurePrimaryCodes.length != 0) {
                System.out.println("Has Mapped Tests");
                arrayListProcedure.addAll(databaseH.retrieveProcedure(String.valueOf(s), procedurePrimaryCodes, 2));
                arrayListProcedure.addAll(databaseH.retrieveProcedureAllWithExceptions(String.valueOf(s), procedurePrimaryCodes, 2));
                proceduresAdapterMappedTest.notifyDataSetChanged();
            } else {
                System.out.println("No Mapped Tests");
                System.out.println("Query: " + forQuery);
                tv_allTest.setVisibility(View.VISIBLE);
                rv_MappedTest.setVisibility(View.GONE);
                arrayListProcedure.addAll(databaseH.retrieveProcedureAll(String.valueOf(s)));
            }

            proceduresAdapter.notifyDataSetChanged();
            tv_nav_notification.setVisibility(View.GONE);
            rv_doctors.setVisibility(View.VISIBLE);
        } else if (isPrimaryDiag) {
            arrayListDiagnosis.clear();
            arrayListDiagnosis.addAll(databaseH.retrieveDiagnose(String.valueOf(s)));
            if (arrayListDiagnosis.size() == 0) {
                implement.updateDisplayForNavigator(tv_nav_notification, rv_doctors, getString(R.string.diagnosis_not_found));
            } else {
                diagnosisAdapterSingleAdd.notifyDataSetChanged();
                tv_nav_notification.setVisibility(View.GONE);
                rv_doctors.setVisibility(View.VISIBLE);
            }
        } else if (isOther) {
            arrayListProcedure.clear();
            arrayListProcedureMappedTest.clear();
            //If mapped procedures in not empty, concat for query
            String forQuery = "";
            if (procedureOtherCodes.length != 0) {
                System.out.println("Has Mapped Tests");
                arrayListProcedure.addAll(databaseH.retrieveProcedure(String.valueOf(s), procedureOtherCodes, 2));
                arrayListProcedure.addAll(databaseH.retrieveProcedureAllWithExceptions(String.valueOf(s), procedureOtherCodes, 2));
                proceduresAdapterMappedTest.notifyDataSetChanged();
            } else {
                System.out.println("No Mapped Tests");
                arrayListProcedure.addAll(databaseH.retrieveProcedureAll(String.valueOf(s)));
            }

            proceduresAdapter.notifyDataSetChanged();
            tv_nav_notification.setVisibility(View.GONE);
            rv_doctors.setVisibility(View.VISIBLE);
//            } else {
//               // arrayListProcedure.addAll(databaseH.retrieveProcedure(String.valueOf(s), procedureOtherCodes));
//                if (arrayListProcedure.size() == 0) {
//                    tv_nav_notification.setText(R.string.procedure_not_found);
//                    tv_nav_notification.setVisibility(View.VISIBLE);
//                    rv_doctors.setVisibility(View.GONE);
//                } else {
//                    proceduresAdapter.notifyDataSetChanged();
//                    tv_nav_notification.setVisibility(View.GONE);
//                    rv_doctors.setVisibility(View.VISIBLE);
//                }
//            }
        } else if (isOtherDiag) {
            arrayListDiagnosis.clear();
            for (int i = 0; i < SharedPref.DIAGCODELIST.size(); i++) {
                if (i == 0)
                    diagToExclude += "'" + SharedPref.DIAGCODELIST.get(i) + "'";
                else
                    diagToExclude += ", '" + SharedPref.DIAGCODELIST.get(i) + "'";
            }
            arrayListDiagnosis.addAll(databaseH.retrieveDiagnoseExcluded(String.valueOf(s), diagToExclude));

            if (arrayListDiagnosis.size() == 0) {
                implement.updateDisplayForNavigator(tv_nav_notification, rv_doctors, getString(R.string.diagnosis_not_found));
            } else {
                rv_MappedTest.setVisibility(View.GONE);
                tv_allTest.setVisibility(View.GONE);
                tv_mappedTest.setVisibility(View.GONE);
                diagnosisAdapterSingleAdd.notifyDataSetChanged();
                tv_nav_notification.setVisibility(View.GONE);
                rv_doctors.setVisibility(View.VISIBLE);
            }

        } else if (isOtherDiagSecond) {
            arrayListDiagnosis.clear();
            arrayListDiagnosis.addAll(databaseH.retrieveDiagnose(String.valueOf(s)));

            if (arrayListDiagnosis.size() == 0) {
                implement.updateDisplayForNavigator(tv_nav_notification, rv_doctors, getString(R.string.diagnosis_not_found));
            } else {
                diagnosisAdapterSingleAdd.notifyDataSetChanged();
                tv_nav_notification.setVisibility(View.GONE);
                rv_doctors.setVisibility(View.VISIBLE);
            }

        } else if (isProcedure_in_Patient_Doctor) {
            arrayListDoctorMultiSelect.clear();
            arrayListDoctorMultiSelect.addAll(databaseH.retrieveDoctorListMultiSelect(String.valueOf(s)));
            if (arrayListDoctorMultiSelect.size() == 0) {
                implement.updateDisplayForNavigator(tv_nav_notification, rv_doctors, getString(R.string.doctor_not_found));
            } else {
                doctorMultipleSelectionAdapter.notifyDataSetChanged();
                tv_nav_notification.setVisibility(View.GONE);
                rv_doctors.setVisibility(View.VISIBLE);
            }
        } else if (isDiagnosis_in_Patient) {
            arrayListDiagnosis.clear();
            arrayListDiagnosis.addAll(databaseH.retrieveDiagnose(String.valueOf(s)));
            if (arrayListDiagnosis.size() == 0) {
                implement.updateDisplayForNavigator(tv_nav_notification, rv_doctors, getString(R.string.diagnosis_not_found));
            } else {
                diagnosisMultiSelectAdapter.notifyDataSetChanged();
                tv_nav_notification.setVisibility(View.GONE);
                rv_doctors.setVisibility(View.VISIBLE);
            }
        } else if (isOtherServices) {
            arrayOtherServices.clear();
            arrayOtherServices.addAll(databaseH.retrieveOtherServices(String.valueOf(s)));
            if (arrayOtherServices.size() == 0) {
                implement.updateDisplayForNavigator(tv_nav_notification, rv_doctors, getString(R.string.other_not_found));
            } else {
                otherServicesAdapter.notifyDataSetChanged();
                tv_nav_notification.setVisibility(View.GONE);
                rv_doctors.setVisibility(View.VISIBLE);
            }
        } else if (isRoomPlan) {
            arrayRooms.clear();
            arrayRooms.addAll(databaseH.retrieveRooms(String.valueOf(s)));
            Log.d("DATA_SIZE", arrayRooms.size() + "");
            if (arrayRooms.size() == 0) {
                implement.updateDisplayForNavigator(tv_nav_notification, rv_doctors, getString(R.string.room_not_found));
            } else {
                roomsAvailAdapter.notifyDataSetChanged();
                tv_nav_notification.setVisibility(View.GONE);
                rv_doctors.setVisibility(View.VISIBLE);
            }
        } else if (isRoomCategory) {
            arrayRoomCategory.clear();
            arrayRoomCategory.addAll(databaseH.retrieveRoomCategory(String.valueOf(s)));
            if (arrayRoomCategory.size() == 0) {
                implement.updateDisplayForNavigator(tv_nav_notification, rv_doctors, getString(R.string.category_not_found));
            } else {

                roomCategoryAdapter.notifyDataSetChanged();
                tv_nav_notification.setVisibility(View.GONE);
                rv_doctors.setVisibility(View.VISIBLE);
            }
        }


    }


    private void init() {


        setSupportActionBar(toolbar);
        // MOVING BURGER MENU TO THE RIGHT`
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        MEMBERID = SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, context);
        PANEL = getIntent().getExtras().getString("PANEL");

        tv_others.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.REMARKS, context));
        StatusSetter.setStatus(SharedPref.getStringValue(SharedPref.USER, SharedPref.MEMBER_STATUS, context), tv_status);
        tv_memberCode.setText(MEMBERID);
        tv_memberType.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.TYPE, context));
        tv_effectiveDate.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.EFFECTIVE, context));
        tv_validityDate.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.VALID, context));


        try {
            double amount = Double.parseDouble(String.valueOf(SharedPref.getIntegerValue(SharedPref.USER, SharedPref.LIMITS, context)));
            DecimalFormat formatter = new DecimalFormat("#,###.##");
            tv_ddLimit.setText(formatter.format(amount));
        } catch (Exception e) {
            e.printStackTrace();
        }


        tv_pec_non_dd.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.MEMBER_PLAN, context));
        tv_name.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.FULL_NAME, context));
        tv_age.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.AGE, context));
        tv_company.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.COMPANY_NAME, context));
        try {
            tv_gender.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.GENDER, context));
        } catch (Exception e) {
            e.printStackTrace();
        }

        pd = new ProgressDialog(NavigatorActivity.this, R.style.MyTheme);
        pd.setCancelable(false);
        pd.setMessage("Authenticating...");
        pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
//        implement.setConsultVisibleList(btn_consultation_list, tv_text_list, pb_loading_list, false);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotToCoord = new Intent(NavigatorActivity.this, CoordinatorMenuActivity.class);
                startActivity(gotToCoord);
            }
        });
        tv_id.setText(MEMBERID);
        ci_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMe(context);

            }
        });

        NavigatorApiCall.getPhoto(ci_user, progressBar, AppInterface.PHOTOLINK + MEMBERID, context);

        /**
         * save for usage in RESULT ACTIVITY
         */
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.BIRTHDAY, getIntent().getExtras().getString(SearchToCoordinatorMenu.BDAY), context);
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.COMPANY_NAME, getIntent().getExtras().getString(SearchToCoordinatorMenu.COMPANY), context);
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.FULL_NAME, getIntent().getExtras().getString(SearchToCoordinatorMenu.NAME), context);
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.AGE, getIntent().getExtras().getString(SearchToCoordinatorMenu.AGE), context);
//        System.out.println("age dapat:" +getIntent().getExtras().getString(SearchToCoordinatorMenu.AGE));
//        try {
//            SharedPref.setStringValue(SharedPref.USER, SharedPref.GENDER, GenderPicker.setGender(Integer.parseInt(getIntent().getExtras().getString(SearchToCoordinatorMenu.GENDER))), context);
//        }catch (Exception e){
//            SharedPref.setStringValue(SharedPref.USER, SharedPref.GENDER, getIntent().getExtras().getString(SearchToCoordinatorMenu.GENDER), context);
//        }
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.MEMBERCODE, getIntent().getExtras().getString(SearchToCoordinatorMenu.ID), context);

        //TODO CHANGE STATUS TO PASS
        //status checker if blocked
        if (SharedPref.getStringValue(SharedPref.USER, SharedPref.MEMBER_STATUS, context) != null) {

            if (PANEL.equals("IN_PATIENT")) {
                isInPatientAlreadyAvailable = SharedPref.getBooleanValue(SharedPref.USER, SharedPref.ISINPATIENT, context);
                Log.d("IS_ADMITTED", isInPatientAlreadyAvailable + "");

                if (isInPatientAlreadyAvailable) {
                    Gson gson = new Gson();
                    InPatient ipu = gson.fromJson(getIntent().getStringExtra(SearchToCoordinatorMenu.REQUEST_DATA_IN_PATIENT), InPatient.class);
                    //InPatient inPatient = getIntent().getExtras().getSerializableE(SearchToCoordinatorMenu.REQUEST_DATA_IN_PATIENT);
//                    System.out.println(inPatient.getMaceReqIpDoctors().get(0).getDoctorCode());
                    // InPatient inPatient =  getIntent().getParcelableExtra("extra_name");

                    Showcase.showCase(new ViewTarget(R.id.tv_in_patient_tag, this), getString(R.string.pre_in_patient_title_),
                            getString(R.string.pre_in_patient_msg), this, caseCallBack, Constant.FROM_QR_SEARCH);


                    String getDiag = null;
                    try {
                        getDiag = ipu.getMaceReqIpDiags().get(0).getDiagCode();
                    } catch (Exception e) {

                    }
                    String getDoctor = null;
                    try {
                        getDoctor = ipu.getMaceReqIpDoctors().get(0).getDoctorCode();
                    } catch (Exception e) {

                    }

                    try {
                        roomPlan = databaseH.getRoomPlan(ipu.getMaceReqIpRoom().getRoomtype());
                        tv_input_room_availed.setText(roomPlan.getPlanDesc());
                        //setting the txtView to false to disable editing
                        tv_input_room_availed.setEnabled(false);
                    } catch (Exception e) {

                    }
                    String roomNo;
                    try {
                        roomNo = ipu.getMaceReqInpatient().getRoomNo();
                    } catch (Exception e) {
                        roomNo = "";
                    }

                    String category = "";
                    try {
                        inPatientAdmittedDiagnosis = databaseH.retrieveOtherDiagnosis(getDiag);
                    } catch (Exception e) {

                    }

                    try {
                        inPatientAdmittedDoctor = databaseH.retrieveDoctor(getDoctor);
                        databaseH.setInPatientMainDoctorSelected(getDoctor);
                    } catch (Exception e) {

                    }


                    String[] data = new String[1];
                    try {
                        data[0] = getDiag;
                        tv_primary_diag_in_patient.setText(inPatientAdmittedDiagnosis.getDiagDesc());
                        //setting the txtView to false to disable editing
                        tv_primary_diag_in_patient.setEnabled(false);

                        tv_doctor_in_patient.setText(inPatientAdmittedDoctor.getDocLname() + ", " + inPatientAdmittedDoctor.getDocFname());
                        //setting the txtView to false to disable editing
                        tv_doctor_in_patient.setEnabled(false);
                    } catch (Exception e) {

                    }

                    fragment = new InPatientAdmitted().getData(
                            data,
                            inPatientAdmittedDoctor,
                            roomPlan,
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.REQUEST_CODE_INPATIENT, context),
                            roomNo,
                            category,
                            inPatientAdmittedDiagnosis,
                            ipu.getMaceReqInpatient().getAdmittedOn());
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.left_to_right, R.anim.right_to_left);
                    fragmentTransaction.replace(R.id.container, fragment);
                    fragmentTransaction.commit();


                    implement.setIsInPatientAvailable(ll_in_patient_available, isInPatientAlreadyAvailable);
                    implement.setAdmittedDate(tv_admitted, tv_admitted_time, ipu.getMaceReqInpatient().getAdmittedOn());
                } else {

                    //TODO UPDATE DATA MUST COME FORM BACKEND

                    implement.setIsInPatientAvailable(ll_in_patient_available, isInPatientAlreadyAvailable);

                    fragment = new AvailServices();
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.left_to_right, R.anim.right_to_left);
                    fragmentTransaction.replace(R.id.container, fragment);
                    fragmentTransaction.commit();
                    getSupportFragmentManager().executePendingTransactions();

                }


                SharedPref.setStringValue(SharedPref.USER, SharedPref.INPATIENT, "IN_PATIENT", context);
                SharedPref.setStringValue(SharedPref.USER, SharedPref.INPATIENT_ADD_DATA, isInPatientAlreadyAvailable + "", context);
                Log.d("data", "in-patient");


            } else if (PANEL.equals("OUT_PATIENT")) {

                //TODO UPDATE DATA MUST COME FORM BACKEND

                implement.setIsInPatientAvailable(ll_in_patient_available, isInPatientAlreadyAvailable);
                //Needed LinearLayout

//                ll_out_patient_result.setVisibility(View.VISIBLE);

                fragment = new CoordinatorMenuButton();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
                SharedPref.setStringValue(SharedPref.USER, SharedPref.INPATIENT, "OUT_PATIENT", context);
            } else if (getIntent().getExtras().getString("PANEL").equals("ER")) {

                System.out.println("NavEr");
                //TODO UPDATE DATA MUST COME FORM BACKEND

                implement.setIsInPatientAvailable(ll_in_patient_available, isInPatientAlreadyAvailable);


                fragment = new AvailServices();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
                SharedPref.setStringValue(SharedPref.USER, SharedPref.INPATIENT, "ER", context);
            } else if (PANEL.equalsIgnoreCase("FROMER")) {
                implement.setIsInPatientAvailable(ll_in_patient_available, isInPatientAlreadyAvailable);

                fragment = new AvailServices();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.left_to_right, R.anim.right_to_left);
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
                getSupportFragmentManager().executePendingTransactions();
            }

        } else {


            // SharedPref.setStringValue(SharedPref.USER, SharedPref.STATUSREMARKS, StatusSetter.setRemarks(getIntent().getExtras().getString(SearchToCoordinatorMenu.STATUS)), context);
//            SharedPref.setStringValue(SharedPref.USER, SharedPref.INPATIENT, "FALSE", context);

            //DISAPPROVED
            fragment = new StatusNotActive();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
//            Log.d("FLAG_BUTTON", getIntent().getExtras().getString(SearchToCoordinatorMenu.STATUS) + "");
            getSupportFragmentManager().executePendingTransactions();


        }


    }

    public void showMe(final Context context) {

        final CharSequence[] items = {CAMERA};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select:");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (String.valueOf(items[item]).equals(CAMERA)) {


                    if (Permission.checkPermissionCamera(context)) {
                        if (Permission.checkPermissionStorage(context)) {

                            Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            File dir =
                                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);

                            output = new File(dir, "CameraMEDICARD.jpeg");
                            i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(output));

                            startActivityForResult(i, CAMERA_RQ);

                        }
                    }

                }


            }
        }).show();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {


            DataBaseHandler db = new DataBaseHandler(context);
            db.setALLProcedureToFalse();
            db.setALLDiagnosisToFalse();
            super.onBackPressed();


        }
    }


    private void setAdapterBasicTest(String message) {
        if (message.equals("Basic_Test")) {

            rv_doctors.setAdapter(proceduresAdapter);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            ORIGIN_PROC = "ADAPTER_PRIMARY";
            proceduresAdapter.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = true;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;

        }

    }


    private void setAdapter(String message) {
        System.out.println("=====" + message + "|");
        if (message.equals("ADAPTER_PROCEDURE")) {
            rv_doctors.setAdapter(proceduresAdapter);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            ORIGIN_PROC = "ADAPTER_PROCEDURE";
            proceduresAdapter.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = true;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_DOCTORS)) {
            rv_doctors.setAdapter(doctorListAdapter);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            doctorListAdapter.notifyDataSetChanged();
            isDoctor = true;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_UNFILTERED_DOCTORS)) {
            rv_doctors.setAdapter(unfilteredDoctorListAdapter);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            unfilteredDoctorListAdapter.notifyDataSetChanged();
            isUnfilterdDoctor = true;
            isUnfilterdHosp = false;
            isDoctor = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_DIAGNOSIS)
                || message.equals(Constant.ADAPTER_DIAG_IN_PATIENT)) {
            rv_doctors.setAdapter(diagnosisAdapterSingleAdd);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            diagnosisAdapterSingleAdd.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = true;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals("ADAPTER_PRIMARY")) {
            rv_doctors.setAdapter(proceduresAdapter);
            rv_doctors.setVerticalScrollBarEnabled(false);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            rv_MappedTest.setAdapter(proceduresAdapterMappedTest);
            rv_MappedTest.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            ORIGIN_PROC = "ADAPTER_PRIMARY";
            proceduresAdapter.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = true;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_PRIME_DIAG) ||
                message.equals(Constant.ADAPTER_DIAG_ADMITTED)) {
            rv_doctors.setAdapter(diagnosisAdapterSingleAdd);   //Frist instance of diagnosis
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            diagnosisAdapterSingleAdd.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = true;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_UNFILTERED_HOSP)) {
            rv_doctors.setAdapter(unfilteredHospitalAdapter);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            unfilteredHospitalAdapter.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = true;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_OTHER)) {
            rv_doctors.setAdapter(proceduresAdapter);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            ORIGIN_PROC = Constant.ADAPTER_OTHER;
            proceduresAdapter.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = true;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_OTHER_2)) {
            rv_doctors.setAdapter(proceduresAdapter);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            ORIGIN_PROC = Constant.ADAPTER_OTHER_2;
            proceduresAdapter.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = true;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_OTHER_DIAG)) {
            rv_doctors.setAdapter(diagnosisAdapterSingleAdd);  //Second Instance of diagnosis
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            diagnosisAdapterSingleAdd.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = true;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_OTHER_DIAG_SECOND)) {
            rv_doctors.setAdapter(diagnosisAdapterSingleAdd);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            diagnosisAdapterSingleAdd.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = true;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_OTHER_BASIC)) {
            rv_doctors.setAdapter(proceduresAdapter);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            ORIGIN_PROC = Constant.ADAPTER_OTHER;
            proceduresAdapter.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = true;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = true;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_DOCTORS_IN_PATIENT)) {
            rv_doctors.setAdapter(doctorListAdapter);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            doctorListAdapter.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = true;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_PROC_IN_PATIENT)) {
            rv_doctors.setAdapter(proceduresAdapter);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            proceduresAdapter.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = true;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_DOCTOR_IN_PATIENT)) {
            rv_doctors.setAdapter(doctorMultipleSelectionAdapter);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            doctorMultipleSelectionAdapter.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = true;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.DIAGNOSIS_OTHER_IN_PATIENT)) {
            rv_doctors.setAdapter(diagnosisMultiSelectAdapter);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            diagnosisMultiSelectAdapter.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = true;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_DOCTORS_IN_PATIENT_MAIN)) {
            rv_doctors.setAdapter(doctorListAdapter);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            doctorListAdapter.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = true;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_PROC_OTHER_SERVICES)) {
            rv_doctors.setAdapter(otherServicesAdapter);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            otherServicesAdapter.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = true;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_ROOM_PLANS)) {
            rv_doctors.setAdapter(roomsAvailAdapter);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            roomsAvailAdapter.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = true;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_ROOM_CATEGORY)) {
            rv_doctors.setAdapter(roomCategoryAdapter);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            roomCategoryAdapter.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = true;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_INPATIENT_DOCTOR)) {
            rv_doctors.setAdapter(inpatientDoctorSelectAdapterNavigator);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            inpatientDoctorSelectAdapterNavigator.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = true;
            isInpatientDiagnosis = false;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_INPATIENT_DIAGNOSIS)) {
            rv_doctors.setAdapter(inpatientDiagnosisSelectAdapterNavigator);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            inpatientDiagnosisSelectAdapterNavigator.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = true;
            isInpatientProcedure = false;
        } else if (message.equals(Constant.ADAPTER_INPATIENT_PROCEDURE)) {
            rv_doctors.setAdapter(inpatientProcedureSelectAdapterNavigator);
            rv_doctors.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return false;
                }
            });
            inpatientProcedureSelectAdapterNavigator.notifyDataSetChanged();
            isDoctor = false;
            isUnfilterdDoctor = false;
            isUnfilterdHosp = false;
            isProcedure = false;
            isDiagnosis = false;
            isPrimary = false;
            isPrimaryDiag = false;
            isOther = false;
            isOtherDiag = false;
            isOtherDiagSecond = false;
            isBasicTest = false;
            isDoctor_in_Patient = false;
            isProcedure_in_Patient = false;
            isProcedure_in_Patient_Doctor = false;
            isDiagnosis_in_Patient = false;
            isDiagnosis_in_Patient_doctor_main = false;
            isOtherServices = false;
            isRoomPlan = false;
            isRoomCategory = false;
            isInpatientDoctor = false;
            isInpatientDiagnosis = false;
            isInpatientProcedure = true;
        }

        implement.navArgs(drawer);
    }


    @Override
    public void onClick(View view, int position) {
        if (isDiagnosis_in_Patient_doctor_main) {
            implement.navArgs(drawer);
            databaseH.setInPatientMainDoctorDeSelected(inPatientAdmittedDoctor.getDoctorCode());
            inPatientAdmittedDoctor = arrayListDoctors.get(position);
            databaseH.setInPatientMainDoctorSelected(inPatientAdmittedDoctor.getDoctorCode());
            tv_doctor_in_patient.setText(arrayListDoctors.get(position).getDocLname() + ", " + arrayListDoctors.get(position).getDocFname());
            EventBus.getDefault().post(new InPatientAdmitted.MessageEvent(Constant.ADAPTER_DOCTORS_IN_PATIENT_MAIN, inPatientAdmittedDoctor));
        } else if (isDoctor) {
            Log.d("TAPPED", arrayListDoctors.get(position).getDocLname());
            implement.navArgs(drawer);
            if (arrayListDoctors.get(position).getSpecDesc().equals(DERMATOLOGY)) {
                alertDialogCustom.showMe(context, "", alertDialogCustom.invalid_doc, 1);
            } else {
                EventBus.getDefault().post(new AvailServices.MessageEvent(
                        "4",
                        MEMBERID,
                        arrayListDoctors.get(position).getDoctorCode(),
                        arrayListDoctors.get(position).getHospitalCode(),
                        arrayListDoctors.get(position).getSpecDesc(),
                        arrayListDoctors.get(position).getDocLname() + ", " + arrayListDoctors.get(position).getDocFname(),
                        arrayListDoctors.get(position).getHospitalName()));

            }

        } else if (isUnfilterdDoctor) {
            //TODO: change for unfiletered docs
            Log.d("TAPPED", arrayListDoctors.get(position).getDocLname());
            implement.navArgs(drawer);
            if (arrayListDoctors.get(position).getSpecDesc().equals(DERMATOLOGY)) {
                alertDialogCustom.showMe(context, "", alertDialogCustom.invalid_doc, 1);
            } else {
                EventBus.getDefault().post(new AvailServices.MessageEvent(
                        "4",
                        MEMBERID,
                        arrayListDoctors.get(position).getDoctorCode(),
                        arrayListDoctors.get(position).getHospitalCode(),
                        arrayListDoctors.get(position).getSpecDesc(),
                        arrayListDoctors.get(position).getDocLname() + ", " + arrayListDoctors.get(position).getDocFname(),
                        arrayListDoctors.get(position).getHospitalName()));

            }

        } else if (isUnfilterdHosp) {
            //TODO: change for unfiltered hosp
            Log.d("TAPPED", hospitalArrayList.get(position).getHospitalName());
            implement.navArgs(drawer);
            EventBus.getDefault().post(new AvailServices.MessageEvent(
                    Constant.ADAPTER_UNFILTERED_HOSP,
                    hospitalArrayList.get(position).getHospitalName(),
                    hospitalArrayList.get(position).getHospitalCode()));
        } else if (isDoctor_in_Patient) {
            Log.d("TAPPED", arrayListDoctors.get(position).getDocLname());
            implement.navArgs(drawer);
            if (arrayListDoctors.get(position).getSpecDesc().equals(DERMATOLOGY)) {
                alertDialogCustom.showMe(context, "", alertDialogCustom.invalid_doc, 1);
            } else {
                EventBus.getDefault().post(new AvailServices.MessageEvent(
                        "14",
                        MEMBERID,
                        arrayListDoctors.get(position).getDoctorCode(),
                        arrayListDoctors.get(position).getHospitalCode(),
                        arrayListDoctors.get(position).getSpecDesc(),
                        arrayListDoctors.get(position).getDocLname() + ", " + arrayListDoctors.get(position).getDocFname(),
                        arrayListDoctors.get(position).getHospitalName()));

            }

        }


        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {

        Log.d("MemberSearch", event.getMessage());
        if (event.getMessage().equals("OPEN")) {
            implement.navArgs(drawer);
        } else if (event.getMessage().equals(Constant.ADAPTER_DOCTORS)) {
            IsFromMaternityOrigin = event.getMsg1();
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals(Constant.ADAPTER_UNFILTERED_HOSP)) {
            //TODO: change this for hosp
            IsFromMaternityOrigin = event.getMsg1();
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals(Constant.ADAPTER_UNFILTERED_DOCTORS)) {
            IsFromMaternityOrigin = event.getMsg1();
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");

        } else if (event.getMessage().equals("ADAPTER_PROCEDURE")) {

            procedurePrimaryCodes2 = null;
            procedurePrimaryCodes2 = event.getProcedures();
            arrayListProcedure.clear();
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");

        } else if (event.getMessage().equals("ADAPTER_DIAGNOSIS") ||
                event.getMessage().equals(Constant.ADAPTER_DIAG_IN_PATIENT)) {
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals("Basic_Test")) {
            Consultation_Origin = Basic_Test;

            implement.getDataList(Constant.BASIC_TEST, MEMBERID, callback, "2", "3", "0", "1", "10");
            setAdapterBasicTest(event.getMessage());
            implement.setConsultVisible(btn_consultation_list, true);
            implement.setConsultVisibleList(btn_consultation_list, tv_text_list, pb_loading_list, true);
        } else if (event.getMessage().equals("Other_Test")) {
            Consultation_Origin = OtherTest;
            implement.getDataList(Constant.OTHER_TEST, MEMBERID, callback, "2", "4", "0", "1", "10");
            implement.setConsultVisible(btn_consultation_list, true);
            implement.setConsultVisibleList(btn_consultation_list, tv_text_list, pb_loading_list, true);
        } else if (event.getMessage().equals("Maternity")) {
//            implement.setConsultVisible(btn_consultation_list, false);
        } else if (event.getMessage().equals("Consultation")) {
//            implement.setConsultVisible(btn_consultation_list, false);
        } else if (event.getMessage().equals("Close_consult")) {
            implement.setConsultVisible(btn_consultation_list, false);
        } else if (event.getMessage().equals("ADAPTER_PRIMARY")) {
            procedurePrimaryCodes = null;
            procedurePrimaryCodes = event.getProcedures();
            arrayListProcedure.clear();

            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals("ADAPTER_PRIME_DIAG")) {
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals(Constant.ADAPTER_OTHER)) {

            procedureOtherCodes = event.getProcedures();
            arrayListProcedure.clear();
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals("ADAPTER_OTHER_DIAG")) {
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals("ADAPTER_OTHER_DIAG_SECOND")) {
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals(Constant.ADAPTER_OTHER_2)) {
            procedureOtherCodes = event.getProcedures();
            arrayListProcedure.clear();
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals("PROCEDURE")) {
            Consultation_Origin = Procedure;
            implement.getDataList(Constant.PROCEDURES, MEMBERID, callback, "3", "0", "0", "1", "10");
            implement.setConsultVisible(btn_consultation_list, true);
            implement.setConsultVisibleList(btn_consultation_list, tv_text_list, pb_loading_list, true);
        } else if (event.getMessage().equals(Constant.ADAPTER_OTHER_BASIC)) {
            arrayListProcedure.clear();
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals(Constant.ADAPTER_DOCTORS_IN_PATIENT)) {
            IsFromMaternityOrigin = event.getMsg1();
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals(Constant.ADAPTER_PROC_IN_PATIENT)) {
            procedurePrimaryCodes = null;
            procedurePrimaryCodes = event.getProcedures();
            arrayListProcedure.clear();
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals(Constant.ADAPTER_DOCTOR_IN_PATIENT)) {
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals(Constant.DIAGNOSIS_OTHER_IN_PATIENT)) {
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals(Constant.GETDATE)) {
            setDateInPatientData(event.getMsg1());
        } else if (event.getMessage().equals(Constant.GETTIME)) {
            setTimeInPatientData(event.getMsg1());
        } else if (event.getMessage().equals(Constant.ADAPTER_PROC_OTHER_SERVICES)) {
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals(Constant.ADAPTER_ROOM_PLANS)) {
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals(Constant.ADAPTER_PRE_ROOM_PLANS)) {
            displayRoomData(event.getRoomPlan());
        } else if (event.getMessage().equals(Constant.ADAPTER_ROOM_CATEGORY)) {
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals(Constant.ADAPTER_INPATIENT_DOCTOR)) {
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals(Constant.ADAPTER_INPATIENT_DIAGNOSIS)) {
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        } else if (event.getMessage().equals(Constant.ADAPTER_INPATIENT_PROCEDURE)) {
            setAdapter(event.getMessage());
            getCurrentListToRetrieve("");
        }
    }


    private void setTimeInPatientData(String msg1) {
        inPatientTimeAdmitted = msg1;
    }

    private void setDateInPatientData(String msg1) {
        inPatientDateAdmitted = msg1;
    }

    @Override
    public void onSingleItemSelected(DiagnosisList diagnosisList) {

        inPatientAdmittedDiagnosis = diagnosisList;
        implement.navArgs(drawer);
        //edit for er
        tv_primary_diag_in_patient.setText(diagnosisList.getDiagDesc());
//      tv_primary_diag_in_patient.setText("ABDOMINAL BLUNT INJURY");
        EventBus.getDefault().post(new InPatientAdmitted.MessageEvent(
                Constant.DIAGNOSIS_OTHER_IN_PATIENT_CHANGE, inPatientAdmittedDiagnosis));
    }


    @Override
    public void onUnfilteredHospSelected(Hospital hospital) {
        implement.navArgs(drawer);
    }


    @Override
    public void onSuccessConsultationList(ArrayList<LoaList> body) {
        consultationList.clear();
        implement.setConsultVisibleList(btn_consultation_list, tv_text_list, pb_loading_list, true);
        consultationList.addAll(body);
    }

    @Override
    public void onErrorConsultationList(String message) {
        Log.d("DATA_CONSULT", "onErrorConsultationList: " + message);
        implement.setConsultError(btn_consultation_list, tv_text_list, pb_loading_list);
    }

    @Override
    public void updatePhoto(File file, String memberID) {
        pd.show();
        NavigatorApiCall.uploadImage(callback, file, memberID, context);
    }

    @Override
    public void noInternetConnection() {
        NavigatorApiCall.getPhoto(ci_user, progressBar, AppInterface.PHOTOLINK + MEMBERID, context);
        alertDialogCustom.showMe(context, alertDialogCustom.NO_Internet_title, alertDialogCustom.NO_Internet, 1);
    }

    @Override
    public void onSuccessUploadPhoto(File file) {
        pd.dismiss();
        alertDialogCustom.showMe(context, alertDialogCustom.success, alertDialogCustom.success_msg, 2);
        Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
        ci_user.setImageBitmap(myBitmap);

    }

    @Override
    public void onErrorUploadPhoto(String message) {
        Log.e("Upload error:", message);
        pd.dismiss();
        alertDialogCustom.showMe(context, alertDialogCustom.unknown, alertDialogCustom.unknown_msg, 1);
        NavigatorApiCall.getPhoto(ci_user, progressBar, AppInterface.PHOTOLINK + MEMBERID, context);
    }

    @Override
    public void cancelledData() {
        NavigatorApiCall.getPhoto(ci_user, progressBar, AppInterface.PHOTOLINK + MEMBERID, context);
    }

    @Override
    public void roomCategoryCallback(String s) {
        EventBus.getDefault().post(new AvailServices.MessageEvent(Constant.ROOM_CATEGORY, s));
        implement.navArgs(drawer);
    }

    @Override
    public void roomPlanCallback(String s) {
        EventBus.getDefault().post(new AvailServices.MessageEvent(Constant.ADAPTER_ROOM_PLANS, s));
        implement.navArgs(drawer);
    }


    private void displayRoomData(Rooms getRoomPlan) {
        roomPlan = getRoomPlan;
        tv_input_room_availed.setText(roomPlan.getPlanDesc());
        implement.navArgs(drawer);
    }

    //DIALOG CONSULTATION SELECTED
    @Override
    public void onSelectedConsultation(LoaList s) {
        EventBus.getDefault().post(new AvailServices.MessageEvent("13", s));
    }

    @Override
    public void onClickedShowCaseClose(String ORIGIN) {

    }


    public static class MessageEvent {

        private ArrayList<BasicTests> arrayListBasicTest;
        private ArrayList<Services> arrayOtherServices;
        private ArrayList<DoctorModelInsertion> doctorModelInsertions;
        private ArrayList<DiagnosisList> diagnosisModelInsertions;
        ArrayList<TestsAndProcedures> procedureModelInsertions;

        private String message;
        private Rooms roomPlan;


        private String[] procedures;


        String msg1, msg2, msg3, msg4;

        public MessageEvent(String message, ArrayList<Services> arrayOtherServices, String msg2) {
            this.arrayOtherServices = arrayOtherServices;
            this.message = message;
        }


        public MessageEvent(String message, ArrayList<DoctorModelInsertion> doctorModelInsertions, String msg2, String msg3) {
            this.doctorModelInsertions = doctorModelInsertions;
            this.message = message;
        }

        public MessageEvent(String message, Rooms roomPlan) {
            this.message = message;
            this.roomPlan = roomPlan;
        }

        public MessageEvent(String message, ArrayList<DiagnosisList> diagnosisModelInsertions, String msg2, String msg3, String msg4) {
            this.diagnosisModelInsertions = diagnosisModelInsertions;
            this.message = message;
        }

        public MessageEvent(String message, ArrayList<TestsAndProcedures> procedureModelInsertions, String s, String s1, String s2, String s3) {
            this.procedureModelInsertions = procedureModelInsertions;
            this.message = message;
        }


        public String getMsg1() {
            return msg1;
        }

        public String getMsg2() {
            return msg2;
        }

        public String getMsg3() {
            return msg3;
        }

        public String getMsg4() {
            return msg4;
        }

        public MessageEvent(String s) {
            this.message = s;
        }

        public MessageEvent(String s, ArrayList<BasicTests> arrayListBasicTest) {
            this.message = s;
            this.arrayListBasicTest = arrayListBasicTest;
        }

        public MessageEvent(String s, String[] procedures) {
            this.message = s;
            this.procedures = procedures;
        }

        public MessageEvent(String message, String msg1) {
            this.message = message;
            this.msg1 = msg1;
        }

        public MessageEvent(String s, String msg1, String msg2, String msg3, String msg4) {
            this.message = s;
            this.msg1 = msg1;
            this.msg2 = msg2;
            this.msg3 = msg3;
            this.msg4 = msg4;

        }

        public Rooms getRoomPlan() {
            return roomPlan;
        }

        public void setRoomPlan(Rooms roomPlan) {
            this.roomPlan = roomPlan;
        }

        public ArrayList<BasicTests> getArrayListBasicTest() {
            return arrayListBasicTest;
        }

        public void setArrayListBasicTest(ArrayList<BasicTests> arrayListBasicTest) {
            this.arrayListBasicTest = arrayListBasicTest;
        }

        public String[] getProcedures() {
            return procedures;
        }

        public void setProcedures(String[] procedures) {
            this.procedures = procedures;
        }


        public String getMessage() {
            return message;
        }

        public ArrayList<Services> getArrayOtherServices() {
            return arrayOtherServices;
        }

        public void setArrayOtherServices(ArrayList<Services> arrayOtherServices) {
            this.arrayOtherServices = arrayOtherServices;
        }

        public ArrayList<DoctorModelInsertion> getDoctorModelInsertions() {
            return doctorModelInsertions;
        }

        public void setDoctorModelInsertions(ArrayList<DoctorModelInsertion> doctorModelInsertions) {
            this.doctorModelInsertions = doctorModelInsertions;
        }

        public ArrayList<DiagnosisList> getDiagnosisModelInsertions() {
            return diagnosisModelInsertions;
        }

        public void setDiagnosisModelInsertions(ArrayList<DiagnosisList> diagnosisModelInsertions) {
            this.diagnosisModelInsertions = diagnosisModelInsertions;
        }

        public ArrayList<TestsAndProcedures> getProcedureModelInsertions() {
            return procedureModelInsertions;
        }

        public void setProcedureModelInsertions(ArrayList<TestsAndProcedures> procedureModelInsertions) {
            this.procedureModelInsertions = procedureModelInsertions;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        try {


            if (requestCode == CAMERA_RQ) {

                if (resultCode != Activity.RESULT_CANCELED) {


                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;


                    File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                    File output = new File(dir + File.separator + "CameraMEDICARD.jpeg");
                    Log.d("FILE_PATH", output.getAbsolutePath());


                    //ROtate if need
                    Bitmap bitmap;
                    ImageConverters imageConverters = new ImageConverters();
                    bitmap = imageConverters.rotateBitmapOrientation(output.getAbsolutePath());

                    //Save bitmap
                    imageConverters.saveToInternalStorage(ci_user, bitmap, context, true);


                    try {
                        File f = new File(dir, "CameraMEDICARD.jpeg");
                        Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                        ci_user.setImageBitmap(b);
                        Log.v("Upload", f.toString());

                        implement.showImageUpload(f, MEMBERID);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }


                }


            }


        } catch (Exception e) {


        }
    }
}

