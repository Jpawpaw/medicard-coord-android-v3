package coord.medicard.com.medicardcoordinator.MemberSearch;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.CoordinatorMenu.CoordinatorMenuActivity;
import coord.medicard.com.medicardcoordinator.MemberVerificationAcitivty;
import coord.medicard.com.medicardcoordinator.R;
import model.Dependents;
import model.ExclusionModel;
import model.Utilization;
import model.VerifyMember;
import utilities.APIMultipleCalls.GetUserData;
import utilities.APIMultipleCalls.NetworkTest;
import utilities.AlertDialogCustom;
import utilities.ErrorMessage;
import utilities.QrCodeConverter;
import utilities.RemarksFilter;
import utilities.SearchToCoordinatorMenu;
import utilities.SetAvailableServices;
import utilities.SharedPref;

import static coord.medicard.com.medicardcoordinator.R.id.QRCodeReaderView;

public class ScanQrActivity extends AppCompatActivity implements QRCodeReaderView.OnQRCodeReadListener, View.OnClickListener,
        GetUserData.GetUserCallback, NavigateSearchData.NavigateToActivity {

    Toolbar toolbar;
    private QRCodeReaderView mydecoderview;
    ImageButton btn_back;
    Context context;
    AlertDialogCustom alertDialogCustom = new AlertDialogCustom();

    ProgressDialog pd;


    QrCodeConverter qrCodeConverter;
    GetUserData.GetUserCallback getUserCallback;
    NavigateSearchData.NavigateToActivity navigateToActivityCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qr);
        navigateToActivityCallback = this;

        init();

    }

    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        pd = new ProgressDialog(ScanQrActivity.this, R.style.MyTheme);
        pd.setCancelable(false);
        pd.setMessage("Authenticating...");
        pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

        qrCodeConverter = new QrCodeConverter();

        btn_back = (ImageButton) toolbar.findViewById(R.id.btn_back);
        mydecoderview = (QRCodeReaderView) toolbar.findViewById(QRCodeReaderView);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        setSupportActionBar(toolbar);
        context = this;
        getUserCallback = this;
        mydecoderview = (QRCodeReaderView) findViewById(QRCodeReaderView);
        mydecoderview.setOnQRCodeReadListener(this);
        mydecoderview.setQRDecodingEnabled(true);
        mydecoderview.setAutofocusInterval(2000L);
        mydecoderview.setTorchEnabled(true);
        mydecoderview.setFrontCamera();
        mydecoderview.setBackCamera();


    }


    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        mydecoderview.stopCamera();
        mydecoderview.onDetachedFromWindow();
        if (NetworkTest.isOnline(context)) {

            gotoMemberSearch(text);
//            pd.show();
//            pd.setMessage("Authenticating...");

//            GetUserData.getUserData(
//                    text,
//                    "19770308"
//                    ,
//                    SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context),
//                    SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, context),
//                    SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context),
//                    getUserCallback,
//                    GetUserData.QRCODE,pd);

        } else {

            alertDialogCustom.showMe(context, alertDialogCustom.NO_Internet_title, alertDialogCustom.NO_Internet, 1);

        }


    }

    private void gotoMemberSearch(String text) {
        Intent gotoMemberSearch = new Intent(ScanQrActivity.this, MemberSearchActivity.class);
        gotoMemberSearch.putExtra("MEMBER_CODE",text);
        startActivity(gotoMemberSearch);

    }


    private void testErrorData(String e) {
        if (e.contains("End of input")) {
            //  showError(alertDialogCustom.NO_Internet);

            AlertDialogCustom.showError(alertDialogCustom.not_message, context, new AlertDialogCustom.ConfirmationAttachment() {
                @Override
                public void onClickOk() {

                    finish();
                }

                @Override
                public void onClickCancel() {

                }
            });

        } else {
            // showError(alertDialogCustom.not_message);
            AlertDialogCustom.showError(ErrorMessage.setErrorMessage(""), context, new AlertDialogCustom.ConfirmationAttachment() {
                @Override
                public void onClickOk() {

                    finish();
                }

                @Override
                public void onClickCancel() {

                }
            });
        }

        pd.dismiss();

    }

    private void testSearchData(VerifyMember body) {
        try {

            if (body.getResponseCode().equals("200") || body.getResponseCode().equals("230")) {

                if (body.getIsLocked().equals("false")) {


                    new SetAvailableServices().setServiceType(body.getServiceType(), context);
                    String consultation, isInPatient, otherTest;

                    consultation = SharedPref.getStringValue(SharedPref.USER, SharedPref.Consultation, context);
                    isInPatient = SharedPref.getStringValue(SharedPref.USER, SharedPref.In_Patient, context);
                    otherTest = SharedPref.getStringValue(SharedPref.USER, SharedPref.OtherTest, context);


                    String VERIFIED = "Member is valid";
                    String NOT_VERIFY = "Member is not verified";
                    if (body.getResponseCode().equals("200")) {

                        if (isInPatient.equals("FALSE") && consultation.equals("FALSE") && otherTest.equals("FALSE")) {
                            //NO ACCESS TO HOSPITAL
                            onDataTrackingMessage(getString(R.string.no_access_to_hospital));

                        } else {


                            if (body.getResponseDesc().equals(VERIFIED)) {

                                // gotoCoordinatorMenu(body);
                                NavigateSearchData.gotoCoordinatorLayout(body, navigateToActivityCallback, body.getMemberInfo().getBDAY(), context);
                            } else if (body.getResponseDesc().equals(NOT_VERIFY)) {

                                //gotoVerificationMenu(body);
                                NavigateSearchData.gotoMemberVerification(context, body, navigateToActivityCallback, body.getMemberInfo().getBDAY());
                            }

                        }
                    } else if (body.getResponseCode().equals("230")) {

                        if (body.getMemberInfo().getMem_OStat_Code().equals("ACTIVE")) {
                            if (isInPatient.equals("FALSE") && consultation.equals("FALSE") && otherTest.equals("FALSE")) {
                                //NO ACCESS TO HOSPITAL
                                onDataTrackingMessage(getString(R.string.no_access_to_hospital));

                            } else {
                                if (body.getResponseDesc().equals(VERIFIED)) {

                                    // gotoCoordinatorMenu(body);
                                    NavigateSearchData.gotoCoordinatorLayout(body, navigateToActivityCallback, body.getMemberInfo().getBDAY(), context);

                                } else if (body.getResponseDesc().equals(NOT_VERIFY)) {

                                    // gotoVerificationMenu(body);
                                    NavigateSearchData.gotoMemberVerification(context, body, navigateToActivityCallback, body.getMemberInfo().getBDAY());

                                }
                            }
                        } else {
                            //gotoCoordinatorMenu(body);
                            NavigateSearchData.gotoCoordinatorLayout(body, navigateToActivityCallback, body.getMemberInfo().getBDAY(), context);

                        }

                    }


                } else {
                    //ACCOUNT IS LOCKED
                    onDataTrackingMessage(alertDialogCustom.not_eligible);

                }

            } else {
                onDataTrackingMessage(alertDialogCustom.not_message);

            }
        } catch (Exception e) {
            onDataTrackingMessage(ErrorMessage.setErrorMessage(""));
        }
    }

    private void onDataTrackingMessage(String message) {

        AlertDialogCustom.showError(message, context, new AlertDialogCustom.ConfirmationAttachment() {
            @Override
            public void onClickOk() {

                finish();
            }

            @Override
            public void onClickCancel() {

            }
        });
    }

//    private void gotoVerificationMenu(VerifyMember memberInfo) {
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.HAS_MATERNITY, memberInfo.getHasMaternity(), context);
//
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.ID, memberInfo.getMemberInfo().getPRIN_CODE(), context);
//        Intent gotoValidate = new Intent(ScanQrActivity.this, MemberVerificationAcitivty.class);
//        gotoValidate.putExtra(SearchToCoordinatorMenu.NAME, memberInfo.getMemberInfo().getMEM_NAME());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.COMPANY, memberInfo.getMemberInfo().getACCOUNT_NAME());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.AGE, memberInfo.getMemberInfo().getAGE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.GENDER, memberInfo.getMemberInfo().getMEM_SEX());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.ID, memberInfo.getMemberInfo().getPRIN_CODE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.STATUS, memberInfo.getMemberInfo().getMem_OStat_Code());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.USERNAME, memberInfo.getVerifiedMember().getAPP_USERNAME());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.ISKIPPED, memberInfo.getVerifiedMember().getIS_SKIPPED());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.NAME, memberInfo.getMemberInfo().getMEM_NAME());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.ID, memberInfo.getMemberInfo().getPRIN_CODE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.STATUS, memberInfo.getMemberInfo().getMem_OStat_Code());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.MEMCODE, memberInfo.getMemberInfo().getACCOUNT_CODE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.TYPE, memberInfo.getMemberInfo().getMEM_TYPE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.EFFECTIVE, memberInfo.getMemberInfo().getEFF_DATE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.VALID, memberInfo.getMemberInfo().getVAL_DATE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.DD_LIMIT, memberInfo.getMemberInfo().getDD_Reg_Limits());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.PLAN, memberInfo.getMemberInfo().getPlan_Desc());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.BDAY, memberInfo.getMemberInfo().getBDAY());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.BDAY_RAW, memberInfo.getMemberInfo().getBDAY());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.VALIDITYDATE, memberInfo.getMemberInfo().getVAL_DATE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.EFFECTIVITYDATE, memberInfo.getMemberInfo().getEFF_DATE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.UTILIZATION, memberInfo.getUtilization());
//
//        ArrayList<Utilization> utils = new ArrayList<>();
//        utils.addAll(memberInfo.getUtilization());
//        gotoValidate.putParcelableArrayListExtra(SearchToCoordinatorMenu.UTILIZATION, utils);
//
//        ArrayList<Dependents> dependents = new ArrayList<>();
//        dependents.addAll(memberInfo.getDependents());
//        gotoValidate.putParcelableArrayListExtra(SearchToCoordinatorMenu.DEPENDENTS, dependents);
//
//        gotoValidate.putExtra(SearchToCoordinatorMenu.OTHER, RemarksFilter.
//                filterRemarks(memberInfo.getMemberInfo().getID_REM(),
//                        memberInfo.getMemberInfo().getID_REM2(),
//                        memberInfo.getMemberInfo().getID_REM3(),
//                        memberInfo.getMemberInfo().getID_REM4(),
//                        memberInfo.getMemberInfo().getID_REM5(),
//                        memberInfo.getMemberInfo().getID_REM6(),
//                        memberInfo.getMemberInfo().getID_REM7()));
//
//        new SetAvailableServices().setServiceType(memberInfo.getServiceType(), context);
//        startActivity(gotoValidate);
//        finish();
//        pd.dismiss();
//
//    }
//
//    private void gotoCoordinatorMenu(VerifyMember memberInfo) {
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.HAS_MATERNITY, memberInfo.getHasMaternity(), context);
//
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.SEARCHTYPE, "qrcode", context);
//
//        Intent gotoMenu = new Intent(ScanQrActivity.this, CoordinatorMenuActivity.class);
//
//        gotoMenu.putExtra(SearchToCoordinatorMenu.NAME, memberInfo.getMemberInfo().getMEM_NAME());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.ID, memberInfo.getMemberInfo().getPRIN_CODE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.STATUS, memberInfo.getMemberInfo().getMem_OStat_Code());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.MEMCODE, memberInfo.getMemberInfo().getACCOUNT_CODE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.TYPE, memberInfo.getMemberInfo().getMEM_TYPE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.EFFECTIVE, memberInfo.getMemberInfo().getEFF_DATE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.VALID, memberInfo.getMemberInfo().getVAL_DATE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.DD_LIMIT, memberInfo.getMemberInfo().getDD_Reg_Limits());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.PLAN, memberInfo.getMemberInfo().getPlan_Desc());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.COMPANY, memberInfo.getMemberInfo().getACCOUNT_NAME());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.AGE, memberInfo.getMemberInfo().getAGE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.GENDER, memberInfo.getMemberInfo().getMEM_SEX());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.BDAY, memberInfo.getMemberInfo().getBDAY());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.VALIDITYDATE, memberInfo.getMemberInfo().getVAL_DATE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.EFFECTIVITYDATE, memberInfo.getMemberInfo().getEFF_DATE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.BDAY_RAW, memberInfo.getMemberInfo().getBDAY());
//
//        ArrayList<Utilization> utils = new ArrayList<>();
//        utils.addAll(memberInfo.getUtilization());
//        gotoMenu.putParcelableArrayListExtra(SearchToCoordinatorMenu.UTILIZATION, utils);
//
//        ArrayList<Dependents> dependents = new ArrayList<>();
//        dependents.addAll(memberInfo.getDependents());
//        gotoMenu.putParcelableArrayListExtra(SearchToCoordinatorMenu.DEPENDENTS, dependents);
//
//        gotoMenu.putExtra(SearchToCoordinatorMenu.OTHER, RemarksFilter.
//                filterRemarks(memberInfo.getMemberInfo().getID_REM(),
//                        memberInfo.getMemberInfo().getID_REM2(),
//                        memberInfo.getMemberInfo().getID_REM3(),
//                        memberInfo.getMemberInfo().getID_REM4(),
//                        memberInfo.getMemberInfo().getID_REM5(),
//                        memberInfo.getMemberInfo().getID_REM6(),
//                        memberInfo.getMemberInfo().getID_REM7()));
//
//
//        new SetAvailableServices().setServiceType(memberInfo.getServiceType(), context);
//        startActivity(gotoMenu);
//        finish();
//        pd.dismiss();
//    }

    @Override
    public void onClick(View view) {


        switch (view.getId()) {

            case R.id.btn_back:
                finish();
                break;

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        mydecoderview.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mydecoderview.stopCamera();
    }

//
//    public void showError(String isLocked) {
//        TextView tv_message, tv_title;
//        CircleImageView ci_error_image;
//        Button btn_accept;
//
//        dialog = new Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.alertshow);
//        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        dialog.setCancelable(false);
//
//        ci_error_image = (CircleImageView) dialog.findViewById(R.id.ci_error_image);
//        tv_message = (TextView) dialog.findViewById(R.id.tv_message);
//        tv_title = (TextView) dialog.findViewById(R.id.tv_title);
//        btn_accept = (Button) dialog.findViewById(R.id.btn_accept);
//        btn_accept.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                finish();
//            }
//        });
//
//        tv_title.setText(alertDialogCustom.hold_it);
//        tv_message.setText(isLocked);
//        dialog.show();
//
//
//        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.40);
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.width = width;
//        dialog.getWindow().setAttributes(lp);
//
//
//    }


    ///SPECIAL CASE
//    public void showMe(Context context, String title, String message, int errorImage) {
//
//        TextView tv_message, tv_title;
//        CircleImageView ci_error_image;
//        Button btn_accept;
//
//        final Dialog dialog = new Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.alertshow);
//        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        dialog.setCancelable(false);
//
//        ci_error_image = (CircleImageView) dialog.findViewById(R.id.ci_error_image);
//        tv_message = (TextView) dialog.findViewById(R.id.tv_message);
//        tv_title = (TextView) dialog.findViewById(R.id.tv_title);
//        btn_accept = (Button) dialog.findViewById(R.id.btn_accept);
//        btn_accept.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                finish();
//            }
//        });
//
//
//        ci_error_image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.warning));
//        btn_accept.setBackgroundColor(ContextCompat.getColor(context, R.color.BLACK));
//        tv_message.setText(message);
//        tv_title.setText(title);
//
//
//        dialog.show();
//
//
//        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.40);
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.width = width;
//        dialog.getWindow().setAttributes(lp);
//
//    }


    @Override
    public void onBackPressed() {

    }

    @Override
    public void onSuccessGetUser(VerifyMember body) {
        pd.dismiss();
        testSearchData(body);
    }

    @Override
    public void onSuccessGetExcludedList(ExclusionModel body, VerifyMember verifyMember) {

    }

    @Override
    public void onErrorGetUser(String message) {
        pd.dismiss();
        testErrorData(message);
    }

    @Override
    public void onActivitySelect(Intent gotoValidate) {
        finish();
        SharedPref.setStringValue(SharedPref.USER, SharedPref.SEARCHTYPE, "qrcode", context);
        startActivity(gotoValidate);
    }
}
