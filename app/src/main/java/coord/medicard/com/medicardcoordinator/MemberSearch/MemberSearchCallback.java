package coord.medicard.com.medicardcoordinator.MemberSearch;

import android.app.Dialog;
import android.content.Intent;

import java.util.HashMap;
import java.util.List;

import model.RecentTransactions;
import model.UpdatedInfo;
import model.VerifyMember;

/**
 * Created by mpx-pawpaw on 6/8/17.
 */

public interface MemberSearchCallback {


    void logOut();

//    void onSuccessSearchMember(VerifyMember body, String memberCode);
//
     void onErrorSearchMember(String message);

    void onErrorUpdateRecentTrans(String message);

    void onCloseDiagInfo();

    void onUpdateDiagInfo(String s, Dialog showCoordData);

    void onUpdateContactSuccess(UpdatedInfo body, String s, Dialog showCoordData);

    void onUpdateContactError(String message, Dialog showCoordData);

    void onSuccessLockAccount(String memberCode);

    void onUpdateRecentTrans(HashMap<String, List<RecentTransactions>> body);

    void onErrorLockAccount(String message);

    void navigateActivity(Intent gotoMenu);

    void testToLockAccount(String memberCode);

    void memberIsLocked();

    void memberNoAccessToHospital();
}
