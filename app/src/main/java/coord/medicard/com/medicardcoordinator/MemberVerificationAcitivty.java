package coord.medicard.com.medicardcoordinator;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;


import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import coord.medicard.com.medicardcoordinator.CoordinatorMenu.CoordinatorMenuActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import model.BasicTestOrOtherTestReturn;
import model.BasicTestResponse;
import model.SkipVerification;
import model.VerifyAccount;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import services.AppInterface;
import services.AppService;
import utilities.AlertDialogCustom;
import utilities.ErrorMessage;
import utilities.SharedPref;
import utilities.StatusSetter;

public class MemberVerificationAcitivty extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, AlertDialogCustom.ConfirmLoa {
    ImageButton btn_back;
    Spinner spinner;
    Context context;
    TextView name, age, company, memId, gender, tv_company_error, tv_status;
    Button btn_skip, btn_proceed;
    EditText et_idNumber;

    CircleImageView circleImageView;
    ProgressDialog pd;
    AlertDialogCustom alertDialogCustom = new AlertDialogCustom();
    ProgressBar progressBar;
    AlertDialogCustom.ConfirmLoa callback;
    String getIDTYPE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_verification_acitivty);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        callback = this;


        init();
    }

    private void init() {
        context = this;


        pd = new ProgressDialog(MemberVerificationAcitivty.this, R.style.MyTheme);
        pd.setCancelable(false);
        pd.setMessage("Updating...");
        pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);


        spinner = (Spinner) findViewById(R.id.dropStatus);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.id_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        name = (TextView) findViewById(R.id.tv_name);
        tv_status = (TextView) findViewById(R.id.tv_status);
        age = (TextView) findViewById(R.id.tv_age);
        company = (TextView) findViewById(R.id.tv_company);
        gender = (TextView) findViewById(R.id.tv_gender);
        memId = (TextView) findViewById(R.id.tv_id);
        tv_company_error = (TextView) findViewById(R.id.tv_company_error);
        btn_proceed = (Button) findViewById(R.id.btn_verify);
        btn_skip = (Button) findViewById(R.id.btn_skip);
        et_idNumber = (EditText) findViewById(R.id.et_idNumber);
        circleImageView = (CircleImageView) findViewById(R.id.circleImageView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);


        name.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.FULL_NAME, context));
        age.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.AGE, context));
        company.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.COMPANY_NAME, context));
        tv_company_error.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.COMPANY_NAME, context));
        gender.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.GENDER, context));
        memId.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, context));


        StatusSetter statusSetter = new StatusSetter();
        statusSetter.setStatus(SharedPref.getStringValue(SharedPref.USER, SharedPref.MEMBER_STATUS, context), tv_status);

        btn_back = (ImageButton) findViewById(R.id.btn_back);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (SharedPref.getStringValue(SharedPref.USER, SharedPref.ISKIPPED, context).equals("1")) {
            btn_skip.setVisibility(View.GONE);
        } else {
            btn_skip.setVisibility(View.VISIBLE);

        }


        btn_skip.setOnClickListener(this);
        btn_proceed.setOnClickListener(this);

        getPhoto();

        et_idNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (et_idNumber.getText().toString().trim().equals("")) {
                    skipIsEnabled(true);

                } else {

                    if (spinner.getSelectedItem().equals("Choose ID...")) {
                        skipIsEnabled(true);
                    } else {
                        skipIsEnabled(false);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.gray));
                    btn_proceed.setEnabled(false);
                } else {

                    if (et_idNumber.getText().toString().equals("")) {
                        btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.gray));
                        btn_proceed.setEnabled(false);
                    } else {

                        btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                        btn_proceed.setEnabled(true);

                        spinner.setSelection(position, false);
                        getIDTYPE = spinner.getSelectedItem().toString();
                    }


                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        skipIsEnabled(true);
    }


    private void getPhoto() {


        progressBar.setVisibility(View.VISIBLE);


        Picasso.with(context)

                .load(AppInterface.PHOTOLINK + SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, context))
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .placeholder(R.drawable.imageplaceholder)
                .error(R.drawable.imageplaceholder)
                .into(circleImageView, new Callback() {
                    @Override
                    public void onSuccess() {

                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        progressBar.setVisibility(View.GONE);
                    }
                });

    }


    private void skipIsEnabled(boolean b) {

        if (b) {
            btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.gray));
            btn_proceed.setEnabled(false);

            btn_skip.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
            btn_skip.setEnabled(true);


        } else {
            btn_skip.setBackgroundColor(ContextCompat.getColor(context, R.color.gray));
            btn_skip.setEnabled(false);

            btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
            btn_proceed.setEnabled(true);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        spinner.setSelection(position, false);
        getIDTYPE = spinner.getSelectedItem().toString();
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.btn_skip:
                showDialog();
                break;


            case R.id.btn_verify:
                alertDialogCustom.showWaiver(context, callback);
                break;
        }
    }


    private void showDialog() {

        final Dialog dialog = new Dialog(this);
        final Button btn_proceed;
        final CheckBox checkBox;

        dialog.setContentView(R.layout.dialog_verifyskip);
        btn_proceed = (Button) dialog.findViewById(R.id.btn_proceed);
        checkBox = (CheckBox) dialog.findViewById(R.id.cb_informed);
        btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.gray));
        btn_proceed.setEnabled(false);
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimaryLight));
                    btn_proceed.setEnabled(true);
                } else {
                    btn_proceed.setBackgroundColor(ContextCompat.getColor(context, R.color.gray));
                    btn_proceed.setEnabled(false);
                }
            }
        });

        btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSkip(SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, context));
                dialog.dismiss();
            }
        });


        dialog.show();

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        dialog.getWindow().setLayout((4 * width) / 7, Toolbar.LayoutParams.WRAP_CONTENT);
    }

    private void setSkip(String id) {

        pd.show();
        SharedPref s = new SharedPref();


        SkipVerification skipVerification = new SkipVerification();
        skipVerification.setAPP_USERNAME(s.getStringValue(s.USER, s.masterUSERNAME, context));
        skipVerification.setMEM_CODE(SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, context));
        skipVerification.setID_TYPE("");
        skipVerification.setID_NUMBER("");
        skipVerification.setIS_SKIPPED("1");
        skipVerification.setIS_VERIFIED("0");


        Log.e("VERIFY", s.getStringValue(s.USER, s.USERNAME, context));
        Log.e("VERIFY", SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, context));

        Gson gson = new Gson();
        Log.d("JSON", gson.toJson(skipVerification));


        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.verifier(skipVerification)
                .enqueue(new retrofit2.Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        pd.dismiss();
                        Log.d("VERIFY", response.body().toString());
                        moveData();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable e) {
                        alertDialogCustom.showMe(context, alertDialogCustom.not_title, ErrorMessage.setErrorMessage(e.getMessage()), 1);
                        pd.dismiss();
                        Log.e("VERIFY", e.getMessage());
                    }
                });
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Subscriber<ResponseBody>() {
//                    @Override
//                    public void onCompleted() {
//
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//
//                        alertDialogCustom.showMe(context, alertDialogCustom.not_title, ErrorMessage.setErrorMessage(e.getMessage()), 1);
//                        pd.dismiss();
//                        Log.e("VERIFY", e.getMessage());
//                    }
//
//                    @Override
//                    public void onNext(ResponseBody signInDetails) {
//                        pd.dismiss();
//                        Log.d("VERIFY", signInDetails.toString());
//                        moveData();
//                    }
//                });
    }


    public void moveData() {


        Intent gotoCoords = new Intent(MemberVerificationAcitivty.this, CoordinatorMenuActivity.class);
//        gotoCoords.putExtra("NAME", getIntent().getExtras().getString(SearchToCoordinatorMenu.NAME));
//        gotoCoords.putExtra("ID", getIntent().getExtras().getString(SearchToCoordinatorMenu.ID));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.STATUS, getIntent().getExtras().getString(SearchToCoordinatorMenu.STATUS));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.GENDER, getIntent().getExtras().getString(SearchToCoordinatorMenu.GENDER));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.AGE, getIntent().getExtras().getString(SearchToCoordinatorMenu.AGE));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.MEMCODE, getIntent().getExtras().getString(SearchToCoordinatorMenu.MEMCODE));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.TYPE, getIntent().getExtras().getString(SearchToCoordinatorMenu.TYPE));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.EFFECTIVE, getIntent().getExtras().getString(SearchToCoordinatorMenu.EFFECTIVE));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.VALID, getIntent().getExtras().getString(SearchToCoordinatorMenu.VALID));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.DD_LIMIT, getIntent().getExtras().getInt(SearchToCoordinatorMenu.DD_LIMIT));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.OTHER, getIntent().getExtras().getString(SearchToCoordinatorMenu.OTHER));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.PLAN, getIntent().getExtras().getString(SearchToCoordinatorMenu.PLAN));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.BDAY, getIntent().getExtras().getString(SearchToCoordinatorMenu.BDAY));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.COMPANY, getIntent().getExtras().getString(SearchToCoordinatorMenu.COMPANY));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.BDAY_RAW, getIntent().getExtras().getString(SearchToCoordinatorMenu.BDAY_RAW));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.VALIDITYDATE, getIntent().getExtras().getString(SearchToCoordinatorMenu.VALIDITYDATE));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.EFFECTIVITYDATE, getIntent().getExtras().getString(SearchToCoordinatorMenu.EFFECTIVITYDATE));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.REQUEST_CODE_IN_PATIENT, getIntent().getExtras().getString(SearchToCoordinatorMenu.REQUEST_CODE_IN_PATIENT));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.IS_IN_PATIENT, getIntent().getExtras().getString(SearchToCoordinatorMenu.IS_IN_PATIENT));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.IS_ER, getIntent().getExtras().getString(SearchToCoordinatorMenu.IS_ER));
//        ArrayList<Utilization> utils = getIntent().getExtras().getParcelableArrayList(SearchToCoordinatorMenu.UTILIZATION);
//        gotoCoords.putParcelableArrayListExtra(SearchToCoordinatorMenu.UTILIZATION, utils);
//
//        ArrayList<Dependents> dependents = getIntent().getExtras().getParcelableArrayList(SearchToCoordinatorMenu.DEPENDENTS);
//        gotoCoords.putParcelableArrayListExtra(SearchToCoordinatorMenu.DEPENDENTS, dependents);

        startActivity(gotoCoords);
        finish();


    }

    private void verifyUser(String id_number, String id_type) {


        pd.show();

        SharedPref s = new SharedPref();


        VerifyAccount verifyAccount = new VerifyAccount();
        verifyAccount.setAPP_USERNAME(s.getStringValue(s.USER, s.masterUSERNAME, context));
        verifyAccount.setMEM_CODE(SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, context));
        verifyAccount.setID_TYPE(spinner.getSelectedItem().toString());
        verifyAccount.setID_NUMBER(id_number);
        verifyAccount.setIS_SKIPPED("1");
        verifyAccount.setIS_VERIFIED("1");

        Log.e("VERIFY", s.getStringValue(s.USER, s.USERNAME, context));
        Log.e("VERIFY", SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, context));
        Log.e("VERIFY", id_type);
        Log.e("VERIFY", id_number);

        Gson gson = new Gson();
        Log.d("JSON", gson.toJson(verifyAccount));

        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.verifyAccount(verifyAccount)
                .enqueue(new retrofit2.Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        pd.dismiss();
                        Log.d("VERIFY", response.body().toString());
                        moveData();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        alertDialogCustom.showMe(context, alertDialogCustom.not_title, ErrorMessage.setErrorMessage(t.getMessage()), 1);
                        //
                        pd.dismiss();
                        Log.e("VERIFY", t.getMessage());
                    }
                });
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Subscriber<ResponseBody>() {
//                    @Override
//                    public void onCompleted() {
//
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        alertDialogCustom.showMe(context, alertDialogCustom.not_title, ErrorMessage.setErrorMessage(e.getMessage()), 1);
//
//                        pd.dismiss();
//                        Log.e("VERIFY", e.getMessage());
//                    }
//
//                    @Override
//                    public void onNext(ResponseBody signInDetails) {
//                        pd.dismiss();
//                        Log.d("VERIFY", signInDetails.toString());
//                        moveData();
//                    }
//                });

    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void loaDuplicateSendApproval(String ORIGIN, String batchCode) {

    }

    @Override
    public void onWaiverAcceptListener(String isTicked) {
        verifyUser(et_idNumber.getText().toString().trim(), getIDTYPE);
    }

    @Override
    public void loaDuplicateSendApprovalforBasicTestAndOtherTest(String origin, String batchCode, BasicTestOrOtherTestReturn loaReturn) {

    }

    @Override
    public void loaDuplicateSendApprovalforBasicTest(String origin, String batchCode, BasicTestResponse loaReturn){

    }
}
