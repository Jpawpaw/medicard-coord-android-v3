package coord.medicard.com.medicardcoordinator;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.ViewStub;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import utilities.SharedPref;
import utilities.StatusSetter;

public class MockUpActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {


    @BindView(R.id.tv_title)
    TextView tv_title;


    @BindView(R.id.switch1)
    ToggleButton switch1;
    @BindView(R.id.ll_account_info)
    LinearLayout ll_account_info;

    @BindView(R.id.tv_status)
    TextView tv_status;
    @BindView(R.id.tv_others)
    TextView tv_others;
    @BindView(R.id.tv_memberCode)
    TextView tv_memberCode;
    @BindView(R.id.tv_effectiveDate)
    TextView tv_effectiveDate;
    @BindView(R.id.tv_memberType)
    TextView tv_memberType;
    @BindView(R.id.tv_validityDate)
    TextView tv_validityDate;
    @BindView(R.id.tv_ddLimit)
    TextView tv_ddLimit;
    @BindView(R.id.tv_pec_non_dd)
    TextView tv_pec_non_dd;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_age)
    TextView tv_age;
    @BindView(R.id.tv_company)
    TextView tv_company;
    @BindView(R.id.tv_gender)
    TextView tv_gender;

//    @BindView(R.id.ll_position_one)
//    Incl ll_position_one;
//    @BindView(R.id.ll_position_two)
//    ScrollView ll_position_two;
    @BindView(R.id.ll_admission_details)
    LinearLayout ll_admission_details;

    Context  context;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mock_up);
        ButterKnife.bind(this);
        context = this;
        switch1.setOnCheckedChangeListener(this);
        switch1.setChecked(true);

        ViewStub stub = (ViewStub) findViewById(R.id.layout_stub_one);
        stub.setLayoutResource(R.layout.mock_up_one);
        View inflated = stub.inflate();

        ViewStub stub1 = (ViewStub) findViewById(R.id.layout_stub_two);
        stub1.setLayoutResource(R.layout.mock_up_two);
        View inflated1 = stub1.inflate();


        if(getIntent().getExtras().getString("POSITION").equals("ONE")){
            inflated.setVisibility(View.VISIBLE);
            inflated1.setVisibility(View.GONE);
            ll_admission_details.setVisibility(View.GONE);
            switch1.setChecked(true);
            tv_title.setText(context.getString(R.string.admission_det));
        }else {
            inflated.setVisibility(View.GONE);
            inflated1.setVisibility(View.VISIBLE);
//            ll_position_two.setVisibility(View.VISIBLE);
            ll_admission_details.setVisibility(View.VISIBLE);
            switch1.setChecked(false);
            tv_title.setText( "Submitted " +context.getString(R.string.admission_det));
        }




        tv_others.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.REMARKS, context));
        StatusSetter.setStatus(SharedPref.getStringValue(SharedPref.USER, SharedPref.MEMBER_STATUS, context), tv_status);
        tv_memberCode.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, context));
        tv_memberType.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.TYPE, context));
        tv_effectiveDate.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.EFFECTIVE, context));
        tv_validityDate.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.VALID, context));


        try {
            double amount = Double.parseDouble(String.valueOf(SharedPref.getIntegerValue(SharedPref.USER, SharedPref.LIMITS, context)));
            DecimalFormat formatter = new DecimalFormat("#,###.##");
            tv_ddLimit.setText(formatter.format(amount));
        } catch (Exception e) {
            e.printStackTrace();
        }


        tv_pec_non_dd.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.MEMBER_PLAN, context));
        tv_name.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.FULL_NAME, context));
        tv_age.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.AGE, context));
        tv_company.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.COMPANY_NAME, context));
        try {
            tv_gender.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.GENDER, context));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
            ll_account_info.setVisibility(View.VISIBLE);
        }else {
            ll_account_info.setVisibility(View.GONE);
        }
    }
}
