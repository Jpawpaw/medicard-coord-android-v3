package coord.medicard.com.medicardcoordinator.SignIn;

import java.util.ArrayList;

import model.ListingUpdateReturn;
import model.RoomCategory;

/**
 * Created by mpx-pawpaw on 6/19/17.
 */

public interface SignInCallback {
    void onSuccessDoctorFetch();

    void onErrorDoctorFetch(String message);

    void onErrorProcedureFetch(String message);

    void onSuccessProcedureFetch();

    void onErrorDiagnosisList(String s);

    void onSuccessDiagnosisFetch();

    void onErrorRoomPlanFetch(String message);

    void onSuccessRoomPlanFetch();

    void onErrorRoomCategory(String message);

    void onSuccessRoomCategory();

    void onSuccessListingUpdate();

    void onErrorListingUpdate(String s);
}
