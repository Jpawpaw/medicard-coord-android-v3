package coord.medicard.com.medicardcoordinator.SignIn;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import coord.medicard.com.medicardcoordinator.R;
import model.BasicTestReturn;
import model.DiagnosisModel;
import model.DoctorModel;
import model.GetDoctorsToHospital;
import model.Hospital;
import model.ListingUpdateReturn;
import model.ProcedureModel;
import model.RoomCategory;
import model.RoomsAvail;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import services.AppInterface;
import services.AppService;
import utilities.DataBaseHandler;
import utilities.ErrorMessage;
import utilities.SharedPref;

/**
 * Created by mpx-pawpaw on 6/19/17.
 */

public class SignInAPiCalls {

    Context context;

    public static void getListingUpdate(final SignInCallback callback) {

        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.getListingUpdate()
                .enqueue(new Callback<ListingUpdateReturn>() {
                    @Override
                    public void onResponse(Call<ListingUpdateReturn> call, Response<ListingUpdateReturn> response) {
                        if (response != null) {
                            callback.onSuccessListingUpdate();
                        } else {
                            callback.onErrorListingUpdate("");

                        }
                    }

                    @Override
                    public void onFailure(Call<ListingUpdateReturn> call, Throwable t) {
                        if (t != null) {
                            callback.onErrorListingUpdate(t.getMessage());
                        } else {
                            callback.onErrorListingUpdate("");

                        }
                    }
                });


    }


    public static void getDoctor(String hospitalCode, final SignInCallback callback, final SignInImplement implement, final ProgressDialog pd, final Context context, final String lastUpdate) {

        if (SharedPref.lastUpdateTables.get("DOCHOSP")) {
            callback.onSuccessDoctorFetch();
        } else {
            AppInterface appInterface;
            appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
            appInterface.getDoctorList(hospitalCode, SharedPref.getStringValue(SharedPref.USER, SharedPref.DOCTOR_LIST_LAST_UPDATE, context))
                    .enqueue(new Callback<DoctorModel>() {
                        @Override
                        public void onResponse(Call<DoctorModel> call, Response<DoctorModel> response) {
                            try {
                                if (SharedPref.getStringValue(SharedPref.USER, SharedPref.DOCTOR_LIST, context).equalsIgnoreCase("false")) {//First call
                                    pd.setMessage("Updating Doctor list...\n" + context.getString(R.string.update_prompt));
                                    pd.show();
                                    implement.upDateDoctor(response.body().getGetDoctorsToHospital(), callback);
                                    SharedPref.setStringValue(SharedPref.USER, SharedPref.DOCTOR_LIST, String.valueOf(true), context);
                                    SharedPref.setStringValue(SharedPref.USER, SharedPref.DOCTOR_LIST_LAST_UPDATE, lastUpdate, context);
                                } else if (!response.body().getGetDoctorsToHospital().isEmpty()) {//With update sa list
                                    pd.setMessage("Updating Doctor list...\n" + context.getString(R.string.update_prompt));
                                    pd.show();
                                    implement.upDateDoctor(response.body().getGetDoctorsToHospital(), callback
                                    );
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM", Locale.ENGLISH);
                                    String date = format.format(new Date());
                                    SharedPref.setStringValue(SharedPref.USER, SharedPref.DOCTOR_LIST_LAST_UPDATE, lastUpdate, context);
                                } else {//skip
                                    callback.onSuccessDoctorFetch();
                                }
                            } catch (Exception e) {
                                callback.onErrorDoctorFetch("");
                            }
                        }

                        @Override
                        public void onFailure(Call<DoctorModel> call, Throwable t) {
                            try {
                                //Delete table of Doctor
                                //Set Initialized to default
                                callback.onErrorDoctorFetch(t.getMessage());
                            } catch (Exception e) {
                                callback.onErrorDoctorFetch("");
                            }
                        }
                    });
        }

    }

    public static void getProcedureLIst(final SignInCallback callback, final SignInImplement implement,
                                        final ProgressDialog pd, final String hospCode, final Context context) {

        if (SharedPref.lastUpdateTables.get("TESTPROC")) {
            callback.onSuccessProcedureFetch();
        } else {
            AppInterface appInterface;
            appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
            appInterface.getProcList(hospCode, SharedPref.getStringValue(SharedPref.USER, SharedPref.PROCEDURE_LIST_LAST_UPDATE, context))
                    .enqueue(new Callback<ProcedureModel>() {
                        @Override
                        public void onResponse(Call<ProcedureModel> call, Response<ProcedureModel> response) {
                            try {
                                if (SharedPref.getStringValue(SharedPref.USER, SharedPref.PROCEDURE_LIST, context).equalsIgnoreCase("false")) {//First call
                                    pd.setMessage("Updating Procedure list...\n" + context.getString(R.string.update_prompt));
                                    implement.updateProcedure(callback, response.body().getProceduresListLocal());
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM", Locale.ENGLISH);
                                    String date = format.format(new Date());
                                    SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_LIST, String.valueOf(true), context);
                                    SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_LIST_LAST_UPDATE, date, context);
                                } else if (!response.body().getProceduresListLocal().isEmpty()) {//With update sa list
                                    pd.setMessage("Updating Procedure list...\n" + context.getString(R.string.update_prompt));
                                    implement.updateProcedure(callback, response.body().getProceduresListLocal());

                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM", Locale.ENGLISH);
                                    String date = format.format(new Date());
                                    SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_LIST_LAST_UPDATE, date, context);
                                } else {//skip
                                    callback.onSuccessProcedureFetch();
                                }
                            } catch (Exception e) {
                                callback.onErrorProcedureFetch("");
                            }
                        }

                        @Override
                        public void onFailure(Call<ProcedureModel> call, Throwable t) {
                            try {
                                callback.onErrorProcedureFetch(t.getMessage());
                            } catch (Exception e) {
                                callback.onErrorProcedureFetch("");

                            }
                        }
                    });

        }
    }


    public static void getDiagnosisList(final SignInCallback callback, final SignInImplement implement, final ProgressDialog pd, final Context context) {


        if (SharedPref.lastUpdateTables.get("DIAGNOSIS")) {
            callback.onSuccessDiagnosisFetch();
        } else {
            AppInterface appInterface;
            appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
            appInterface.getDiagnosisList(SharedPref.getStringValue(SharedPref.USER, SharedPref.DIAGNOSIS_LIST_LAST_UPDATE, context))
                    .enqueue(new Callback<DiagnosisModel>() {
                        @Override
                        public void onResponse(Call<DiagnosisModel> call, Response<DiagnosisModel> response) {
                            try {
                                if (SharedPref.getStringValue(SharedPref.USER, SharedPref.DIAGNOSIS_LIST, context).equalsIgnoreCase("false")) {//First call
                                    pd.setMessage("Updating Diagnosis list...\n" + context.getString(R.string.update_prompt));
                                    implement.upDateDiagnosis(callback, response.body().getDiagnosisList());
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM", Locale.ENGLISH);
                                    String date = format.format(new Date());
                                    SharedPref.setStringValue(SharedPref.USER, SharedPref.DIAGNOSIS_LIST, String.valueOf(true), context);
                                    SharedPref.setStringValue(SharedPref.USER, SharedPref.DIAGNOSIS_LIST_LAST_UPDATE, date, context);
                                } else if (!response.body().getDiagnosisList().isEmpty()) {//With update sa list
                                    pd.setMessage("Updating Diagnosis list...\n" + context.getString(R.string.update_prompt));
                                    implement.upDateDiagnosis(callback, response.body().getDiagnosisList());
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM", Locale.ENGLISH);
                                    String date = format.format(new Date());
                                    SharedPref.setStringValue(SharedPref.USER, SharedPref.DIAGNOSIS_LIST_LAST_UPDATE, date, context);
                                } else {//skip
                                    callback.onSuccessDiagnosisFetch();
                                }
                            } catch (Exception e) {
                                callback.onErrorDiagnosisList("");
                            }

                        }

                        @Override
                        public void onFailure(Call<DiagnosisModel> call, Throwable t) {
                            try {
                                callback.onErrorDiagnosisList(t.getMessage());
                            } catch (Exception e) {
                                callback.onErrorDiagnosisList("");
                            }
                        }
                    });
        }
    }

    public static void getRoomPlan(final SignInCallback callback, final SignInImplement implement,
                                   final ProgressDialog pd) {
        if (SharedPref.lastUpdateTables.get("ROOMPLAN")) {
            callback.onSuccessRoomPlanFetch();
        } else {
            AppInterface appService;
            appService = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
            appService.getRoomPlans()
                    .enqueue(new Callback<RoomsAvail>() {
                        @Override
                        public void onResponse(Call<RoomsAvail> call, Response<RoomsAvail> response) {
                            try {
                                implement.updateRoomsAvail(response.body(), pd, callback);
                            } catch (Exception e) {
                                callback.onErrorRoomPlanFetch("");
                            }
                        }

                        @Override
                        public void onFailure(Call<RoomsAvail> call, Throwable t) {


                            try {
                                callback.onErrorRoomPlanFetch(t.getMessage());
                            } catch (Exception e) {
                                callback.onErrorRoomPlanFetch("");

                            }
                        }
                    });
        }
    }


    public static void getRoomCategory(final SignInCallback callback, final SignInImplement implement,
                                       final ProgressDialog pd) {

        if (SharedPref.lastUpdateTables.get("ROOMCATEGORY")) {
            callback.onSuccessRoomCategory();
        } else {
            AppInterface appService;
            appService = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
            appService.getRoomCategory()
                    .enqueue(new Callback<RoomCategory>() {
                        @Override
                        public void onResponse(Call<RoomCategory> call, Response<RoomCategory> response) {
                            try {
                                implement.updateRoomCategory(response.body(), pd, callback);
                            } catch (Exception e) {
                                callback.onErrorRoomCategory(ErrorMessage.setErrorMessage(""));
                            }
                        }

                        @Override
                        public void onFailure(Call<RoomCategory> call, Throwable t) {
                            try {
                                callback.onErrorRoomCategory(ErrorMessage.setErrorMessage(t.getMessage()));
                            } catch (Exception e) {
                                callback.onErrorRoomCategory(ErrorMessage.setErrorMessage(""));
                            }
                        }
                    });
        }


    }
}
