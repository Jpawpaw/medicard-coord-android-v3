package coord.medicard.com.medicardcoordinator.Navigator;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import coord.medicard.com.medicardcoordinator.R;
import de.hdodenhof.circleimageview.CircleImageView;
import model.BasicTestReturn;
import model.InPatient;
import model.Loa;
import model.LoaList;
import model.LoaReq;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import services.AppInterface;
import services.AppService;
import utilities.SharedPref;
import utilities.SortLOAViaDate;

/**
 * Created by mpx-pawpaw on 4/11/17.
 */

public class NavigatorApiCall {


//    Consultation Type 1 Subtype 1
//    Maternity Type 1 Subtype 2
//    BasicTest Type 2 Subtype 3
//    OtherTest Type 2 Subtype 4
//    Procedures Type 3
//    IP Type 4
//    ER Type 5
//

    public static void getDataListForConsult(String origin,
                                             String memberid,
                                             final NavigatorRetrieve.NavigatorInterface callback,
                                             String serviceType,
                                             String subServiceType,
                                             String status,
                                             String page,
                                             String count) {


        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.getLoaReqFiltered(memberid, serviceType,subServiceType, status, page, count)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });

//                .enqueue(new Callback<ResponseBody>() {
//                    @Override
//                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
////                        try {
////                            if (response.body().getLoaList() != null) {
////                                ArrayList<LoaList> arrayList = new ArrayList<LoaList>();
////                                arrayList.addAll(response.body().getLoaList());
////
////                                //sort via date
////                                ArrayList<LoaList> temp = new ArrayList<LoaList>();
////                                temp.addAll(arrayList);
////                                arrayList.clear();
////                                arrayList.addAll(SortLOAViaDate.sortData(temp));
////                                Collections.reverse(arrayList);
////
////                                callback.onSuccessConsultationList(arrayList);
////                            } else {
////                                callback.onErrorConsultationList("");
////                            }
////                        } catch (Exception e) {
////                            callback.onErrorConsultationList("");
////                        }
//
//                    }
//
//                    @Override
//                    public void onFailure(Call<ResponseBody> call, Throwable t) {
//                        callback.onErrorConsultationList(t.getMessage());
//                    }
//                });

    }


    public static void uploadImage(final NavigatorRetrieve.NavigatorInterface callback, final File file, final String memberCode, Context context) {


        RequestBody fbody = RequestBody.create(MediaType.parse("image/jpeg"), file);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), memberCode);
        RequestBody username = RequestBody.create(MediaType.parse("text/plain"),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context));
        RequestBody type = RequestBody.create(MediaType.parse("text/plain"), "COORDINATOR");

        Log.v("Upload", file.toString());
        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.upload(
                fbody,
                name,
                username,
                type
        ).enqueue(new retrofit2.Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                callback.onSuccessUploadPhoto(file);

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                try {
                    callback.onErrorUploadPhoto(t.getMessage());
                } catch (Exception e) {
                    callback.onErrorUploadPhoto(t.getMessage());
                }

            }
        });

    }


    public static void getPhoto(CircleImageView ci_user, final ProgressBar progressBar, String linking_image, Context context) {


        progressBar.setVisibility(View.VISIBLE);


        Picasso.with(context)

                .load(linking_image)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .placeholder(R.drawable.imageplaceholder)
                .error(R.drawable.imageplaceholder)
                .into(ci_user, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {

                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        progressBar.setVisibility(View.GONE);
                    }
                });

    }


}
