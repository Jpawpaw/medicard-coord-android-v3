package coord.medicard.com.medicardcoordinator.ApprovalScreen;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import coord.medicard.com.medicardcoordinator.R;
import model.DiagnosisClinicProcedures;
import model.Test.DiagnosisProcedures;
import utilities.DataBaseHandler;
import utilities.QrCodeCreator;

/**
 * Created by IPC_Server on 8/18/2017.
 */



public class PendingAdapter extends RecyclerView.Adapter<PendingAdapter.Holder> {
    private Context context;
    private String check;
    ArrayList<DiagnosisProcedures> arrayList = new ArrayList<>();
    ArrayList<DiagnosisClinicProcedures> arrayListProcedure = new ArrayList<>();
    DataBaseHandler dataBaseHandler;


    //pending
//    public PendingAdapter(Context context, ArrayList<DiagnosisProcedures> arrayList, String check) {
//        this.context = context;
//        this.arrayList = arrayList;
//        this.check = check;
//        dataBaseHandler = new DataBaseHandler(context);
//
//    }

    public PendingAdapter(Context context, ArrayList<DiagnosisProcedures> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        this.check = check;
        dataBaseHandler = new DataBaseHandler(context);

    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_approval, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        System.out.println(check);
        holder.img_approval.setImageBitmap(QrCodeCreator.getBitmapFromString(arrayList.get(position).getMaceRequestTest().getApprovalNo()));
//        System.out.println("Q1" + arrayList.get(0).getProcedureCode());
//        System.out.println("Q2" + arrayList.get(0).getAmount());
//        System.out.println("Q3" + arrayList.get(0).getApprovalNo());
//        System.out.println("Q4" + arrayList.get(0).getAmount());


        holder.tv_name_proc.setText(arrayList.get(position).getMaceRequestOpTest().getProcDesc());
        //List of TestProc(Get Object by ProcedureCode)
        String costcenter = dataBaseHandler.getCostCenter(arrayList.get(position).getMaceRequestOpTest().getProcCode());

        if(arrayList.get(position).getMaceRequestOpDiag().getMaceDiagType().equals("1")) {
            holder.tv_cost_center.setText(costcenter);
            holder.tv_total_num.setText("Price: " + arrayList.get(position).getMaceRequestTest().getTransamount());
            holder.tv_approval_no.setVisibility(View.GONE);
            holder.tv_primaryDiagnosis.setText("Primary Diagnosis: " + arrayList.get(position).getMaceRequestTest().getPrimaryDiagnosisDesc());
        }else {
            holder.tv_cost_center.setText(costcenter);
            holder.tv_total_num.setText("Price: " + arrayList.get(position).getMaceRequestTest().getTransamount());
            holder.tv_approval_no.setVisibility(View.GONE);
            holder.tv_primaryDiagnosis.setText("Other Diagnosis: " + arrayList.get(position).getMaceRequestTest().getPrimaryDiagnosisDesc());
        }

    }



    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_approval)
        ImageView img_approval;
        @BindView(R.id.tv_cost_center)
        TextView tv_cost_center;
        @BindView(R.id.tv_name_proc)
        TextView tv_name_proc;
        @BindView(R.id.tv_approval_no)
        TextView tv_approval_no;
        @BindView(R.id.tv_total_num)
        TextView tv_total_num;
        @BindView(R.id.tv_primaryDiagnosis)
        TextView tv_primaryDiagnosis;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
