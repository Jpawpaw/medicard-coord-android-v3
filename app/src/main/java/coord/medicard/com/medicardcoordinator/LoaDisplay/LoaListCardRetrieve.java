package coord.medicard.com.medicardcoordinator.LoaDisplay;

import java.util.ArrayList;

import model.LoaList;

/**
 * Created by IPC_Server on 10/11/2017.
 */

public class LoaListCardRetrieve {

    private String requestCode;
    private String serviceType;
    private String line1;
    private String line2;
    private String line3;
    private String line4;
    private String requestDateTime;
    private String approvalNos;
    private Integer loaListIndex;

    public Integer getLoaListIndex() {
        return loaListIndex;
    }

    public void setLoaListIndex(Integer loaListIndex) {
        this.loaListIndex = loaListIndex;
    }

    public String getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(String requestCode) {
        this.requestCode = requestCode;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getLine1() {
        return line1;
    }

    public String getRequestDateTime() {
        return requestDateTime;
    }

    public void setRequestDateTime(String requestDateTime) {
        this.requestDateTime = requestDateTime;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getLine3() {
        return line3;
    }

    public void setLine3(String line3) {
        this.line3 = line3;
    }

    public String getLine4() {
        return line4;
    }

    public void setLine4(String line4) {
        this.line4 = line4;
    }

    public String getApprovalNos() {
        return approvalNos;
    }

    public void setApprovalNos(String approvalNos) {
        this.approvalNos = approvalNos;
    }
}
