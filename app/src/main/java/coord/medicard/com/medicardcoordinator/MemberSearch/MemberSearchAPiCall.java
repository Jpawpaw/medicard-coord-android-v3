package coord.medicard.com.medicardcoordinator.MemberSearch;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;

import java.util.HashMap;
import java.util.List;

import model.RecentTransactions;
import model.UpdatedInfo;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import services.AppInterface;
import services.AppService;
import utilities.SharedPref;

/**
 * Created by mpx-pawpaw on 6/8/17.
 */

public class MemberSearchAPiCall {

//    public static void verifyMember(final MemberSearchCallback callback,
//                                    final String memberCode,
//                                    final String bday,
//                                    String hospCode,
//                                    String phoneId,
//                                    String username) {
//        Log.d("MEMBERID", memberCode);
//        Log.d("bday", bday);
//        Log.d("hospCode", hospCode);
//        Log.d("phoneId", phoneId);
//        Log.d("username", username);
//
//
//        AppInterface appInterface;
//        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
//        appInterface.getData(memberCode, bday, phoneId, username, hospCode, "search")
//                .enqueue(new Callback<VerifyMember>() {
//                    @Override
//                    public void onResponse(Call<VerifyMember> call, Response<VerifyMember> response) {
//
//
//                        try {
//                            if (response.body() != null) {
//                                callback.onSuccessSearchMember(response.body(), memberCode);
//                            } else {
//                                callback.onErrorSearchMember("");
//                            }
//                        } catch (Exception e) {
//                            callback.onErrorSearchMember("");
//                        }
//
//
//                    }
//
//
//                    @Override
//                    public void onFailure(Call<VerifyMember> call, Throwable e) {
//
//                        try {
//                            callback.onErrorSearchMember(e.getMessage());
//                        } catch (Exception error) {
//                            callback.onErrorSearchMember("");
//
//                        }
//                    }
//                });
//
//
//    }


    public static void updateDataContact(final String s, final Context context, final MemberSearchCallback callback, final Dialog showCoordData) {

        Log.d("CONTACT", s);
        Log.d("appUsername", SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context));

        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.updateContact(SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context), s).
                enqueue(new Callback<UpdatedInfo>() {
                    @Override
                    public void onResponse(Call<UpdatedInfo> call, Response<UpdatedInfo> response) {
                        //  091838484
                        try {
                            callback.onUpdateContactSuccess(response.body(), s, showCoordData);
                        } catch (Exception e) {
                            callback.onUpdateContactError("", showCoordData);

                        }
                    }

                    @Override
                    public void onFailure(Call<UpdatedInfo> call, Throwable t) {
                        try {
                            if (t.getMessage() != null) {
                                callback.onUpdateContactError(t.getMessage(), showCoordData);
                            } else {
                                callback.onUpdateContactError("", showCoordData);
                            }
                        } catch (Exception error) {
                            callback.onUpdateContactError("", showCoordData);
                        }
                    }
                });

    }


    public static void sendLockAccount(final String memberCode, final MemberSearchCallback callback) {
        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.setLockAccount(memberCode)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                     try {
                         if (response.body() != null){
                             callback.onSuccessLockAccount(memberCode);
                         }else{
                             callback.onErrorLockAccount("");
                         }
                     }catch (Exception error){
                         callback.onErrorLockAccount("");
                     }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        try {
                            callback.onErrorSearchMember(t.getMessage());
                        } catch (Exception error) {
                            callback.onErrorSearchMember("");
                        }
                    }
                });

    }


    public static void updateRecentTrans(Context context,String hospitalCode,String memberCode,final MemberSearchCallback callback) {
        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.getRecentTransactionsForHosp(hospitalCode,memberCode)
                .enqueue(new Callback<HashMap<String, List<RecentTransactions>>>() {
                    @Override
                    public void onResponse(Call<HashMap<String, List<RecentTransactions>>> call, Response<HashMap<String, List<RecentTransactions>>> response) {
                        try {
                            if (response.body() != null){
                                callback.onUpdateRecentTrans(response.body());
                            }else{
                                callback.onErrorUpdateRecentTrans("");
                            }
                        }catch (Exception error){
                            callback.onErrorUpdateRecentTrans("");
                        }
                    }
                    @Override
                    public void onFailure(Call<HashMap<String, List<RecentTransactions>>> call, Throwable t) {
                        try {
                            callback.onErrorUpdateRecentTrans(t.getMessage());
                        } catch (Exception error) {
                            callback.onErrorUpdateRecentTrans("");
                        }
                    }
                });
    }
}
