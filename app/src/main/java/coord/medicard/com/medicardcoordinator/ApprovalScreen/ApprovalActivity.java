package coord.medicard.com.medicardcoordinator.ApprovalScreen;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.print.PrintHelper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import coord.medicard.com.medicardcoordinator.R;
import model.DiagnosisClinicProcedures;
import model.Test.DiagnosisProcedures;
import utilities.DateConverter;


public class ApprovalActivity extends Fragment {

    private static final String ARRAY = "ARRAY";
    private static final String ARRAY_PENDING = "ARRAY_PENDING";
    private static final String ARRAY_PROC = "ARRAY_PROC";

    private static final String ORIGIN = "TEST";
    private static final String HEADER = "HEADER";
    private static final String CHECK = "CHECK";
    boolean isTestApproved = false;

    private String GET_ORIGIN;
    private String GET_HEADER;
    private static  String primaryCheck;
    private ApprovalRetrieve implement;
    private PendingRetrieve implementPending;
    private ApprovalProcRetrieve implementProc;


    public ApprovalActivity() {
        // Required empty public constructor
    }

    ApprovalAdapter adapter;
    PendingAdapter adapterPending;
    ApprovalProcAdapter procAdapter;
    ArrayList<DiagnosisProcedures>  GET_ARRAY = new ArrayList<>();
    ArrayList<DiagnosisProcedures> GET_ARRAY_PENDING = new ArrayList<>();
    ArrayList<DiagnosisProcedures> procedures = new ArrayList<>();
    ArrayList<DiagnosisProcedures> proceduresPending = new ArrayList<>();
    ArrayList<DiagnosisClinicProcedures> GET_ARRAY_PROC = new ArrayList<>();
    ArrayList<DiagnosisClinicProcedures> clinicProcedures = new ArrayList<>();

//    ArrayList<DiagnosisClinicProcedures> GET_ARRAY = new ArrayList<>();
//    ArrayList<DiagnosisClinicProcedures> procedures = new ArrayList<>();

    public static ApprovalActivity newInstance(ArrayList<DiagnosisProcedures> arrayProc, ArrayList<DiagnosisProcedures> arrayPending, String Origin, String header, String primary) {
        ApprovalActivity fragment = new ApprovalActivity();
        Bundle args = new Bundle();
        System.out.println("Instance Initialized.");
        System.out.println("Size: " + arrayProc.size());
        args.putParcelableArrayList(ARRAY, arrayProc);
        args.putParcelableArrayList(ARRAY_PENDING, arrayPending);
        primaryCheck = primary;
        args.putString(ORIGIN, Origin);
        args.putString(HEADER, header);
        fragment.setArguments(args);

        return fragment;
    }

    public static ApprovalActivity newInstancePending(ArrayList<DiagnosisProcedures> arrayProc, String Origin, String header, String primary) {
        ApprovalActivity fragmentPending = new ApprovalActivity();
        Bundle argsPending = new Bundle();
        System.out.println("Instance Initialized.");
        System.out.println("Size: " + arrayProc.size());
        argsPending.putString(ORIGIN, Origin);
        argsPending.putString(HEADER, header);
        fragmentPending.setArguments(argsPending);
        return fragmentPending;
    }

    public static ApprovalActivity newInstance2(ArrayList<DiagnosisClinicProcedures> arrayProc, String Origin, String header) {
        ApprovalActivity fragment = new ApprovalActivity();
        Bundle args = new Bundle();
        System.out.println("Instance Initialized.");
        System.out.println("Size: " + arrayProc.size());
        args.putParcelableArrayList(ARRAY_PROC, arrayProc);
        args.putString(ORIGIN, Origin);
        args.putString(HEADER, header);
        fragment.setArguments(args);
        return fragment;
    }


    ArrayList<String> arrayList = new ArrayList<>();
    Context context;

    @BindView(R.id.fragment_approve)
    LinearLayout fragment_approve;
    @BindView(R.id.rv_approval)
    RecyclerView rv_approval;
    @BindView(R.id.tv_pending)
    TextView tv_pending;
    @BindView(R.id.tv_origin)
    TextView tv_origin;
    @BindView(R.id.tv_remarks)
    TextView tv_remarks;
    @BindView(R.id.tv_notes)
    TextView tv_notes;

    @BindView(R.id.tv_date)
    TextView tv_date;

    @BindView(R.id.fragment_pending)
    LinearLayout fragment_pending;
    @BindView(R.id.rv_approval_pending)
    RecyclerView rv_approval_pending;
    @BindView(R.id.tv_pending_pending)
    TextView tv_pending_pending;
    @BindView(R.id.tv_origin_pending)
    TextView tv_origin_pending;
    @BindView(R.id.tv_remarks_pending)
    TextView tv_remarks_pending;
    @BindView(R.id.tv_notes_pending)
    TextView tv_notes_pending;


    //    @BindView(R.id.ll_test_details)
//    LinearLayout ll_test_details;
    private String TAG = "APPROVAL_ACT";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            System.out.println("OnCreate init.");
            GET_ARRAY = getArguments().getParcelableArrayList(ARRAY);
            GET_ARRAY_PENDING = getArguments().getParcelableArrayList(ARRAY_PENDING);
            GET_ARRAY_PROC =getArguments().getParcelableArrayList(ARRAY_PROC);
            System.out.println("GET ARRAY"+GET_ARRAY);
            System.out.println("GET ARRAY PENDING"+GET_ARRAY_PENDING);
            GET_ORIGIN = getArguments().getString(ORIGIN);
            GET_HEADER = getArguments().getString(HEADER);
        }

//        Log.d(TAG, GET_ORIGIN);
//        Log.d(TAG, GET_ARRAY.get(0).getApprovalNo());



        if (GET_ORIGIN.equals("PROCEDURES")){
            implementProc = new ApprovalProcRetrieve(context);
            context = getActivity();
            clinicProcedures.addAll(GET_ARRAY_PROC);
            procAdapter = new ApprovalProcAdapter(context, GET_ARRAY_PROC);
        } else {
            implement = new ApprovalRetrieve(context);
            context = getActivity();
            procedures.addAll(GET_ARRAY);
//            adapter = new ApprovalAdapter(context, GET_ARRAY, primaryCheck);

            implementPending = new PendingRetrieve(context);
            context = getActivity();
            proceduresPending.addAll(GET_ARRAY_PENDING);
//            adapterPending = new PendingAdapter(context, GET_ARRAY_PENDING, primaryCheck);

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_approval, container, false);

        System.out.println("Origin: " + GET_ORIGIN);
        if (GET_ORIGIN.equals("PROCEDURES")){
            ButterKnife.bind(this, view);
            rv_approval.setLayoutManager(new LinearLayoutManager(context));
            rv_approval.setAdapter(procAdapter);
            implementProc.setApproveUiProc(clinicProcedures, tv_pending, rv_approval);
            tv_origin.setText(GET_HEADER);
            tv_remarks.setText(getString(R.string.out_patient_disclaimer_reference));
            tv_notes.setText(getString(R.string.out_patient_disclaimer_notes));
            tv_date.setText("This request is valid from "  + DateConverter.convertDateToMMddyyyy(DateConverter.getDate()) +" to "+ DateConverter.validityDatePLusDay(DateConverter.getDate(), 2));
        }else if (GET_HEADER.equalsIgnoreCase("REQUEST APPROVED")) {

            ButterKnife.bind(this, view);
            rv_approval.setLayoutManager(new LinearLayoutManager(context));
            rv_approval.setAdapter(adapter);
            implement.setApproveUi(procedures, tv_pending, rv_approval);
            tv_origin.setText("Request Approved");
            tv_remarks.setText(getString(R.string.out_patient_disclaimer_reference));
            tv_notes.setText(getString(R.string.out_patient_disclaimer_reference_approve));
            fragment_pending.setVisibility(View.GONE);
            tv_date.setText("This request is valid from "  + DateConverter.convertDateToMMddyyyy(DateConverter.getDate()) +" to "+ DateConverter.validityDatePLusDay(DateConverter.getDate(), 2));


        }else if (GET_HEADER.equalsIgnoreCase("PENDING")) {

            ButterKnife.bind(this, view);
            tv_origin_pending.setText("Request Pending");
            rv_approval_pending.setLayoutManager(new LinearLayoutManager(context));
            rv_approval_pending.setAdapter(adapterPending);
            implement.setPendingUi(proceduresPending, tv_pending_pending, rv_approval_pending);
            tv_remarks_pending.setText(getString(R.string.out_patient_disclaimer_reference_pending));
            tv_notes_pending.setVisibility(View.GONE);
            fragment_approve.setVisibility(View.GONE);

        }else {
            ButterKnife.bind(this, view);
            rv_approval.setLayoutManager(new LinearLayoutManager(context));
            rv_approval.setAdapter(adapter);
            implement.setApproveUi(procedures, tv_pending, rv_approval);
            tv_origin.setText("Request Approved");
            tv_remarks.setText(getString(R.string.out_patient_disclaimer_reference));
            tv_notes.setText(getString(R.string.out_patient_disclaimer_reference_approve));
            tv_date.setText("This request is valid from "  + DateConverter.convertDateToMMddyyyy(DateConverter.getDate()) +" to "+ DateConverter.validityDatePLusDay(DateConverter.getDate(), 2));


            ButterKnife.bind(this, view);
            tv_origin_pending.setText("Request Pending");
            rv_approval_pending.setLayoutManager(new LinearLayoutManager(context));
            rv_approval_pending.setAdapter(adapterPending);
            implement.setPendingUi(proceduresPending, tv_pending_pending, rv_approval_pending);
            tv_remarks_pending.setText(getString(R.string.out_patient_disclaimer_reference_pending));
            tv_notes_pending.setVisibility(View.GONE);


//            ButterKnife.bind(this, view);
//            rv_approval_pending.setLayoutManager(new LinearLayoutManager(context));
//            rv_approval_pending.setAdapter(adapterPending);
//            implement.setApproveUi(procedures, tv_pending_pending, rv_approval_pending);
////            implementPending.setUi(proceduresPending, tv_pending_pending, rv_approval_pending);
//            tv_origin_pending.setText(GET_HEADER);
//            tv_remarks_pending.setText(getString(R.string.out_patient_disclaimer_reference));
//            tv_notes_pending.setText(getString(R.string.out_patient_disclaimer_notes));


        }

        return view;
    }


}
