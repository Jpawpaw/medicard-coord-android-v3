package coord.medicard.com.medicardcoordinator.LoaDisplay;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;


import adapter.LoaListProcedureAdapter;
import adapter.LoaReqAdapter;
import adapter.OtherTestLoaAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import coord.medicard.com.medicardcoordinator.CoordinatorMenu.CoordinatorMenuActivity;
import coord.medicard.com.medicardcoordinator.R;
import model.LoaList;
import utilities.AlertDialogCustom;
import utilities.CheckFileExistence;
import utilities.Constant;
import utilities.DataBaseHandler;
import utilities.DateConverter;
import utilities.DownloadLOAButton;
import utilities.GenderPicker;
import utilities.QrCodeCreator;
import utilities.SharedPref;
import utilities.pdfUtil.GenerateForm;
import utilities.pdfUtil.OutPatientConsultationForm;
import utilities.pdfUtil.PermissionUtililities;

public class LoaDisplayActivity extends AppCompatActivity implements LoaDisplayCallback, GenerateForm.PdfCallback {


    //TODO RE MODEL POJO TO ACTUAL DATA TO ADD

    @BindView(R.id.tv_status)
    TextView tv_status;
    @BindView(R.id.tv_approval_code)
    TextView tv_approval_code;
    @BindView(R.id.tv_approval_code1)
    TextView tv_approval_code1;
    @BindView(R.id.tv_member_code)
    TextView tv_member_code;
    @BindView(R.id.tv_member_name)
    TextView tv_member_name;
    @BindView(R.id.tv_age)
    TextView tv_age;
    @BindView(R.id.tv_gender)
    TextView tv_gender;
    @BindView(R.id.tv_company)
    TextView tv_company;
    @BindView(R.id.tv_date_approved)
    TextView tv_date_approved;
    @BindView(R.id.tv_hospname)
    TextView tv_hospname;
    @BindView(R.id.tv_hospital_address)
    TextView tv_hospital_address;
    @BindView(R.id.tv_doc_name)
    TextView tv_doc_name;
    @BindView(R.id.tv_doc_spec)
    TextView tv_doc_spec;
    @BindView(R.id.tv_validity_date)
    TextView tv_validity_date;
    @BindView(R.id.tv_problem)
    TextView tv_problem;
    @BindView(R.id.remark)
    TextView remark;
    @BindView(R.id.tv_diangosis)
    TextView tv_diangosis;
    @BindView(R.id.tv_procedures)
    TextView tv_procedures;
    @BindView(R.id.tv_instructions)
    TextView tv_instructions;
    @BindView(R.id.tv_ref_tag)
    TextView tv_ref_tag;
    @BindView(R.id.tv_cost_center)
    TextView tv_cost_center;
    @BindView(R.id.tv_primary_diagnosis)
    TextView tv_primary_diagnosis;
    @BindView(R.id.tv_reason_consult)
    TextView tv_reason_consult;
    @BindView(R.id.tv_priceGrandTotal)
    TextView tv_priceGrandTotal;

    //bottom note

    @BindView(R.id.tv_bottom_note)
    TextView tv_bottom_note;
    @BindView(R.id.tv_validity_date_tag)
    TextView tv_validity_date_tag;


    @BindView(R.id.btn_ok)
    Button btn_ok;
    @BindView(R.id.btn_download)
    Button btn_download;
    //    @BindView(R.id.tv_test_name)
//    TextView tv_test_name;
    @BindView(R.id.tv_hosp)
    CardView cv_hosp;
    @BindView(R.id.cv_doc)
    CardView cv_doc;
    @BindView(R.id.tv_prob)
    CardView cv_prob;
    @BindView(R.id.cv_diagnosis)
    CardView cv_diagnosis;
    @BindView(R.id.cv_proc)
    CardView cv_proc;
    @BindView((R.id.tv_proc_notes))
    TextView tv_proc_notes;


    @BindView(R.id.rv_otherTestAndBasicTest)
    RecyclerView rv_otherTestAndBasicTest;


    @BindView(R.id.cv_procedures)
    CardView cv_procedures;

    @BindView(R.id.tv_reason_consult2)
    TextView tv_reason_consult2;

    @BindView(R.id.tv_diagnosis)
    TextView tv_diagnosis;
    @BindView(R.id.tv_procedure_label)
    TextView tv_procedure_label;
    @BindView(R.id.tv_instructions_pending)
    TextView tv_instructions_pending;
    //Relative layout for clinic Procedures
    @BindView(R.id.rl_clinic_procedure)
    RelativeLayout rl_clinic_procedure;
    //ListView for description
    @BindView(R.id.lv_clinicproc_selected_title)
    ListView lv_clinicproc_selected_title;
    //ListView for amount
    @BindView(R.id.lv_clinicproc_selected_data)
    ListView lv_clinicproc_selected_data;
    @BindView(R.id.txt_procedure)
    TextView txt_procedure;
    @BindView(R.id.txt_Price)
    TextView txt_Price;

    @BindView(R.id.tv_coor_contact_no)
    TextView tv_coor_contact_no;
    @BindView(R.id.tv_coor_contact_email)
    TextView tv_coor_contact_email;

    @BindView(R.id.cv_primary)
    CardView cv_primary;
    @BindView(R.id.cv_reason)
    CardView cv_reason;
    @BindView(R.id.img_approval)
    ImageView img_approval;
    @BindView(R.id.btn_download_test)
    Button btn_download_test;


    private boolean isFileExist = false;
    LoaReqAdapter loaReqAdapter;
    DataBaseHandler database;
    String convertedRequestDatetime;
    LoaDisplayRetieve implement;
    BasicTestLoaAdapter adapter;
    LoaListProcedureAdapter loaListProcedureAdapter;
    OtherTestLoaAdapter otherTestAdapter;
    ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> ARRAYLIST_GroupedByDiag = new ArrayList<>();
    ArrayList<LoaList.GroupedByCostCenter> ARRAYLIST_GroupedByCostCenter = new ArrayList<>();
    ArrayList<LoaList.MappedTest> ARRAYLIST_MappedTest = new ArrayList<>();
    ArrayList<LoaList.MappedTest> ARRAYLIST_MappedTestClinicProc = new ArrayList<>();
    ArrayList<String> clinicProcCodesMappedTest = new ArrayList<>();
    ArrayList<String> basicTestCodes = new ArrayList<>();
    ArrayList<String> clinicListData = new ArrayList<>();
    ArrayAdapter<String> adapterListDesc;
    ArrayAdapter<String> adapterListData;

    Context context;
    private OutPatientConsultationForm.Builder loaFormBuilder; // builder for loa
    private LoaDisplayCallback callback;
    AlertDialogCustom alertyDialog;
    GenerateForm.PdfCallback pdfCallback;
    LoaList loa;
    private String approvalNo = "";

    String notes = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loa_display);
        ButterKnife.bind(this);
        database = new DataBaseHandler(this);
        callback = this;
        pdfCallback = this;
        context = this;
        alertyDialog = new AlertDialogCustom();
        implement = new LoaDisplayRetieve(context, callback);
        loa = SharedPref.LOALIST;


        if ((Constant.CONSULTATION).equalsIgnoreCase(loa.getRequestType())) {
            if (loa.getStatus().contains("PENDING") || loa.getStatus().contains("DISAPPROVED")) {
                tv_ref_tag.setVisibility(View.GONE);
                tv_approval_code.setVisibility(View.GONE);
                tv_instructions.setText("Note: This request is subject for Approval.");
                tv_instructions_pending.setVisibility(View.VISIBLE);
                cv_doc.setVisibility(View.VISIBLE);
                cv_reason.setVisibility(View.VISIBLE);
                cv_primary.setVisibility(View.GONE);
                cv_prob.setVisibility(View.VISIBLE);
                btn_download_test.setVisibility(View.GONE);
                tv_bottom_note.setVisibility(View.GONE);
                tv_validity_date.setVisibility(View.GONE);
                tv_validity_date_tag.setText(R.string.this_request_is_valid_for);
                tv_validity_date_tag.setTextColor(Color.RED);
                cv_procedures.setVisibility(View.GONE);

            } else {
                tv_instructions.setText("Note: Please write the Reference Code on the Consultation form.");
                cv_doc.setVisibility(View.VISIBLE);
                cv_reason.setVisibility(View.VISIBLE);
                cv_primary.setVisibility(View.GONE);
                cv_prob.setVisibility(View.VISIBLE);
                btn_download_test.setVisibility(View.GONE);
                cv_procedures.setVisibility(View.GONE);
            }
        } else if ((Constant.MATERNITY).equalsIgnoreCase(loa.getRequestType())) {
            if (loa.getStatus().contains("PENDING") || loa.getStatus().contains("DISAPPROVED")) {
                tv_ref_tag.setVisibility(View.GONE);
                tv_approval_code.setVisibility(View.GONE);
                tv_instructions.setText("Note: This request is subject for Approval.");
                tv_instructions_pending.setVisibility(View.VISIBLE);
                cv_doc.setVisibility(View.VISIBLE);
                cv_reason.setVisibility(View.VISIBLE);
                cv_primary.setVisibility(View.GONE);
                cv_prob.setVisibility(View.VISIBLE);
                btn_download_test.setVisibility(View.GONE);
                tv_bottom_note.setVisibility(View.GONE);
                tv_validity_date.setVisibility(View.GONE);
                tv_validity_date_tag.setText(R.string.this_request_is_valid_for);
                tv_validity_date_tag.setTextColor(Color.RED);
                cv_procedures.setVisibility(View.GONE);
            } else {
                tv_instructions.setText("Note: Please write the Reference Code on the Maternity Consultation form.");
                cv_doc.setVisibility(View.VISIBLE);
                cv_reason.setVisibility(View.VISIBLE);
                cv_primary.setVisibility(View.GONE);
                cv_prob.setVisibility(View.VISIBLE);
                btn_download_test.setVisibility(View.GONE);
                cv_procedures.setVisibility(View.GONE);
            }
        } else if ((Constant.BASIC_TEST).equalsIgnoreCase(loa.getRequestType())) {
            basictestLoaDisplay();
            String s = loa.getStatus().trim();
            if (loa.getStatus().contains("PENDING") || loa.getStatus().contains("DISAPPROVED")) {
                tv_ref_tag.setVisibility(View.GONE);
                tv_status.setText(s);
                tv_approval_code.setVisibility(View.GONE);
                tv_instructions.setText("Note: This request is subject for Approval.");
                tv_instructions_pending.setVisibility(View.VISIBLE);
                cv_reason.setVisibility(View.GONE);
                cv_doc.setVisibility(View.GONE);
                cv_primary.setVisibility(View.GONE);
                cv_prob.setVisibility(View.GONE);
                btn_download.setVisibility(View.GONE);
                btn_download_test.setVisibility(View.VISIBLE);
                tv_procedure_label.setText("Basic Test");
                tv_cost_center.setVisibility(View.GONE);
                cv_procedures.setVisibility(View.VISIBLE);
//                adapter = new LoaListBasicAdapter(context, ARRAYLIST_GroupedByDiag);
                tv_bottom_note.setVisibility(View.GONE);
                tv_validity_date.setVisibility(View.GONE);
                tv_validity_date_tag.setText(R.string.this_request_is_valid_for);
                tv_validity_date_tag.setTextColor(Color.RED);
                tv_priceGrandTotal.setText("P" + loa.getTotalAmount());
                tv_ref_tag.setVisibility(View.GONE);
                tv_approval_code.setVisibility(View.GONE);
            } else {
                tv_ref_tag.setText("Approval Code");
                tv_instructions.setText("Note: Please write the Approval Code and Type of Basic Test on the Request Form.");
                cv_reason.setVisibility(View.GONE);
                cv_doc.setVisibility(View.GONE);
                cv_primary.setVisibility(View.GONE);
                cv_prob.setVisibility(View.GONE);
                btn_download.setVisibility(View.GONE);
                btn_download_test.setVisibility(View.VISIBLE);
                tv_procedure_label.setText("Basic Test");
                tv_cost_center.setVisibility(View.GONE);
                cv_procedures.setVisibility(View.VISIBLE);
                btn_download_test.setText("Download Basic Test Form");
                tv_priceGrandTotal.setText("P" + loa.getTotalAmount());
                tv_ref_tag.setVisibility(View.GONE);
                tv_approval_code.setVisibility(View.GONE);
            }
        } else if ((Constant.OTHER_TEST).equalsIgnoreCase(loa.getRequestType())) {
            otherTestLoaDisplay();
            if (loa.getStatus().contains("PENDING") || loa.getStatus().contains("DISAPPROVED")) {
                tv_ref_tag.setVisibility(View.GONE);
                tv_approval_code.setVisibility(View.GONE);
                tv_instructions.setText("Note: This request is subject for Approval.");
                tv_instructions_pending.setVisibility(View.VISIBLE);
                cv_reason.setVisibility(View.GONE);
                cv_doc.setVisibility(View.VISIBLE);
                cv_primary.setVisibility(View.GONE);
                cv_prob.setVisibility(View.GONE);
                btn_download.setVisibility(View.GONE);
                btn_download_test.setVisibility(View.VISIBLE);
                cv_procedures.setVisibility(View.VISIBLE);
                tv_procedure_label.setText("Other Test");
                tv_diagnosis.setVisibility(View.GONE);
                tv_cost_center.setText("hahahaha");
                tv_cost_center.setVisibility(View.GONE);
                btn_download.setVisibility(View.GONE);
                tv_bottom_note.setVisibility(View.GONE);
                tv_validity_date.setVisibility(View.GONE);
                tv_validity_date_tag.setText(R.string.this_request_is_valid_for);
                tv_validity_date_tag.setTextColor(Color.RED);
                tv_priceGrandTotal.setText("P" + loa.getTotalAmount());
                tv_ref_tag.setVisibility(View.GONE);
                tv_approval_code.setVisibility(View.GONE);
            } else {
                tv_instructions.setText("Note: Please write the Approval Code and Type of Diagnostic Test on the Request Form.");
                tv_ref_tag.setText("Approval Code");
                cv_reason.setVisibility(View.GONE);
                cv_doc.setVisibility(View.VISIBLE);
                cv_primary.setVisibility(View.GONE);
                cv_prob.setVisibility(View.GONE);
                btn_download.setVisibility(View.GONE);
                btn_download_test.setVisibility(View.VISIBLE);
                cv_procedures.setVisibility(View.VISIBLE);
                tv_procedure_label.setText("Other Test");
                tv_cost_center.setText("hahahaha");
                tv_diagnosis.setText("Other Diagnosis");
                tv_priceGrandTotal.setText("P" + loa.getTotalAmount());
                btn_download_test.setText("Download Other Test Form");
                tv_ref_tag.setVisibility(View.GONE);
                tv_approval_code.setVisibility(View.GONE);
            }
        } else if ((Constant.PROCEDURE).equalsIgnoreCase(loa.getRequestType())) {
            procedureLoaDisplay();
            if (loa.getStatus().contains("PENDING")) {
                System.out.println("PENDING PROCEDURE :");
                tv_ref_tag.setVisibility(View.GONE);
                remark.setText("Clinic Procedure");
                tv_approval_code.setVisibility(View.GONE);
                tv_instructions.setText("Note: This request is subject for Approval.");
                tv_instructions_pending.setVisibility(View.VISIBLE);
                cv_reason.setVisibility(View.VISIBLE);
                cv_doc.setVisibility(View.VISIBLE);
                cv_primary.setVisibility(View.VISIBLE);
                cv_prob.setVisibility(View.VISIBLE);
                tv_proc_notes.setVisibility(View.VISIBLE);
                btn_download.setVisibility(View.GONE);
                tv_reason_consult2.setText("Clinic Procedure Remarks:");
                btn_download_test.setVisibility(View.VISIBLE);
                cv_procedures.setVisibility(View.VISIBLE);
                tv_procedure_label.setText("Clinic Procedure");
                loaListProcedureAdapter = new LoaListProcedureAdapter(context, ARRAYLIST_MappedTestClinicProc);
                loaListProcedureAdapter.notifyDataSetChanged();
                System.out.println("listPROCEDUREAPDAPTER" + loaListProcedureAdapter + ARRAYLIST_MappedTestClinicProc);
                btn_download.setVisibility(View.GONE);
                tv_bottom_note.setVisibility(View.GONE);
                tv_validity_date.setVisibility(View.GONE);
                tv_validity_date_tag.setText(R.string.this_request_is_valid_for);
                tv_validity_date_tag.setTextColor(Color.RED);
                tv_cost_center.setVisibility(View.GONE);
                tv_priceGrandTotal.setText("P" + loa.getTotalAmount());
                tv_ref_tag.setVisibility(View.GONE);
                tv_approval_code.setVisibility(View.GONE);
            } else {
                System.out.println("APPROVED: ");
                remark.setText("Clinic Procedure");
                tv_instructions.setText("Note: Please write the Approval Code and Doctor’s Clinic Procedure on the Consultation Form");
                tv_ref_tag.setText("Approval Code");
                cv_reason.setVisibility(View.VISIBLE);
                cv_doc.setVisibility(View.VISIBLE);
                cv_primary.setVisibility(View.VISIBLE);
                cv_prob.setVisibility(View.VISIBLE);
                btn_download.setVisibility(View.VISIBLE);
                btn_download_test.setVisibility(View.GONE);
                cv_procedures.setVisibility(View.VISIBLE);
                tv_proc_notes.setVisibility(View.VISIBLE);
                tv_reason_consult2.setText("Clinic Procedure Remarks:");
                tv_procedure_label.setText("Clinic Procedure");
                loaListProcedureAdapter = new LoaListProcedureAdapter(context, ARRAYLIST_MappedTestClinicProc);
                loaListProcedureAdapter.notifyDataSetChanged();
                System.out.println("listPROCEDUREAPDAPTER" + loaListProcedureAdapter + ARRAYLIST_MappedTestClinicProc);
                btn_download_test.setText("Download Clinic Procedure Form");
                tv_cost_center.setVisibility(View.GONE);
                tv_priceGrandTotal.setText("P" + loa.getTotalAmount());
                tv_ref_tag.setVisibility(View.GONE);
                tv_approval_code.setVisibility(View.GONE);
            }
        }

        tv_coor_contact_no.setText("Coordinator Number:\n" + SharedPref.getStringValue(SharedPref.USER,SharedPref.CONTACT_NO,context));
        tv_coor_contact_email.setText("Email:\n" + SharedPref.getStringValue(SharedPref.USER,SharedPref.EMAIL,context));

        String clinicProceduresTest = "";

        for (int i = 0; i < clinicProcCodesMappedTest.size(); i++) {
            if (i == 0)
                clinicProceduresTest += clinicProcCodesMappedTest.get(i);
            else
                clinicProceduresTest += ", " + clinicProcCodesMappedTest.get(i);
        }

        approvalNo = loa.getApprovalNo();

        if (loa.getRequestDatetime() != null) {
            convertedRequestDatetime = DateConverter.convertDatetoyyyyMMdd_(loa.getRequestDatetime());
            setData(tv_date_approved, DateConverter.convertDateToMMddyyyy(convertedRequestDatetime));
            setData(tv_validity_date, DateConverter.convertDateToMMddyyyy(convertedRequestDatetime)
                    + " to " + DateConverter.validityDatePLusDay(convertedRequestDatetime, 2));
            setData(tv_status, implement.setStatusHeader(loa.getRequestType(), loa.getStatus(), loa.getRequestDatetime()));
            // setData(tv_approval_code, loa.getRequestDatetimeo());

            setDataFromEmpty(tv_approval_code, loa.getRequestDatetime() + " ", tv_ref_tag);
        } else {
            setData(tv_status, loa.getStatus());
        }


        setData(tv_approval_code, loa.getApprovalNo());
//            setData(tv_instructions, implement.setInstructions(loa.getRequestType()));
        setData(remark, loa.getRequestType().equalsIgnoreCase((Constant.PROCEDURE)) ? "CLINIC PROCEDURE DONE IN DOCTOR'S CLINIC" : loa.getRequestType());
        setData(tv_member_code, loa.getMemCode());
        setData(tv_member_name, loa.getMemFname() + " " + loa.getMemLname());
        setData(tv_age, SharedPref.getStringValue(SharedPref.USER, SharedPref.AGE, context));
        setData(tv_gender, loa.getMemGender());
        setData(tv_company, loa.getMemCompany());
        setData(tv_hospital_address, SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALADDRESS, this));
        setData(tv_hospname, SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALNAME, this));
        setData(tv_doc_name, loa.getDoctorName());
        setData(tv_doc_spec, loa.getDoctorSpec());
        setData(tv_procedures, loa.getRequestTypeDetail03());
        setData(tv_date_approved, DateConverter.convertDateToMMddyyyy(convertedRequestDatetime));
        setData(tv_reason_consult, loa.getReasonForConsult());
        setData(tv_problem, loa.getReasonForConsult());
        setData(tv_proc_notes, notes);
        setData(tv_cost_center, loa.getRequestTypeDetail01());
        setData(tv_primary_diagnosis, loa.getDiagnosis());
        setData(tv_approval_code1, loa.getApprovalNo());
        setData(tv_diangosis, loa.getPrimaryDiag());
        Bitmap bmp;
        if (null == loa.getApprovalNo() || "".equals(loa.getApprovalNo()))
            bmp = QrCodeCreator.getBitmapFromString("0");
        else
            bmp = QrCodeCreator.getBitmapFromString(loa.getApprovalNo());

        img_approval.setImageBitmap(bmp);


        if (loa.getRequestDatetime() != null) {
            DownloadLOAButton.updateDownloadButton(btn_download, btn_download_test, tv_status);
            isFileExist = CheckFileExistence.fileExistance(loa.getRequestType(), loa.getApprovalNo());
            implement.setDownloadButton(isFileExist, btn_download, btn_download_test);
        } else {
            btn_download.setVisibility(View.GONE);
            btn_download_test.setVisibility(View.GONE);
        }
//// 9/4/2017 change all download "" to download form
        if ((Constant.CONSULTATION).equalsIgnoreCase(loa.getRequestType()) || (Constant.MATERNITY).equalsIgnoreCase(loa.getRequestType()) || (Constant.PROCEDURE).equalsIgnoreCase(loa.getRequestType())) {
            btn_download_test.setVisibility(View.GONE);

        } else
            btn_download.setVisibility(View.GONE);

        if ((Constant.CONSULTATION).equalsIgnoreCase(loa.getRequestType())) {

            btn_download.setText("Download Form");

        } else if ((Constant.MATERNITY).equalsIgnoreCase(loa.getRequestType())) {

            btn_download.setText("Download Form");

        } else if ((Constant.BASIC_TEST).equalsIgnoreCase(loa.getRequestType())) {

            btn_download_test.setText("Download Form");

        } else if ((Constant.OTHER_TEST).equalsIgnoreCase(loa.getRequestType())) {

            btn_download_test.setText("Download Form");

        } else if ((Constant.PROCEDURE).equalsIgnoreCase(loa.getRequestType())) {

            btn_download.setText("Download Form");

        }

        String basicTestCode = "";

        for (int i = 0; i < basicTestCodes.size(); i++) {
            if (i == 0)
                basicTestCode += basicTestCodes.get(i);
            else
                basicTestCode += ", " + basicTestCodes.get(i);
        }


        String primaryComplaint =
                loa.getPrimaryComplaint() != null ? loa.getPrimaryComplaint() : "";

        setDataProblem(tv_reason_consult, primaryComplaint, cv_reason);

        System.out.println("aaaaaaaaaaaaaaaaaaa diagnosis: " + loa.getPrimaryDiag() + " procedures: " + loa.getRequestTypeDetail03() + "Doctor: " + loa.getDoctorName());

        String remarks = SharedPref.getStringValue(SharedPref.USER, SharedPref.REMARKS, context);
        remarks = remarks.replace("\n", ", ").replace("\r", "");

        String hospitalName = SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALNAME, this);
        System.out.println("Doctor :" + loa.getDoctorName());
        System.out.println("DoctorSpec :" + loa.getDoctorSpec());
        System.out.println("Hospital :" + hospitalName);
        System.out.println("PrimaryDiag :" + loa.getPrimaryDiag());

        if (loa.getRequestType().equalsIgnoreCase((Constant.PROCEDURE))) {
            loaFormBuilder = new OutPatientConsultationForm.Builder()
                    .validFrom(DateConverter.convertDateToMMddyyyy(convertedRequestDatetime))
                    .validUntil(DateConverter.validityDatePLusDay(convertedRequestDatetime, 2))
                    .date(DateConverter.convertDateToMMddyyyy(convertedRequestDatetime))
                    .referenceNumber(loa.getApprovalNo())
                    .doctor(loa.getDoctorName())
                    .hospital(hospitalName)
                    .memberName(loa.getMemFname() + " " + loa.getMemLname())
                    .age(SharedPref.getStringValue(SharedPref.USER, SharedPref.AGE, context))
                    .gender(loa.getMemGender())
                    .memberId(loa.getMemCode())
                    .company(loa.getMemCompany())
                    .chiefComplaint(primaryComplaint)
                    .dateEffectivity(SharedPref.getStringValue(SharedPref.USER, SharedPref.EFFECTIVE, context))
                    .validityDate(SharedPref.getStringValue(SharedPref.USER, SharedPref.VALID, context))
                    .serviceType(loa.getRequestType())
                    .batchCode(loa.getBatchCode())
                    .remarks(remarks)
                    .imgApproval(bmp)
                    .diagnosis(loa.getPrimaryDiag())
                    .primaryDianosisWorkingImpression(loa.getDiagnosis())
                    .prescribedTestForPrimaryDiagnosis(clinicProceduresTest)
                    .procedure(loa.getRequestTypeDetail03())
                    .procedureDoneInClinicApprovalNo(loa.getApprovalNo());
        } else {
            if (loa.getRequestDatetime() != null && loa.getRequestType() != (Constant.PROCEDURE)) {
                loaFormBuilder = new OutPatientConsultationForm.Builder()
                        .validFrom(DateConverter.convertDateToMMddyyyy(convertedRequestDatetime))
                        .validUntil(DateConverter.validityDatePLusDay(convertedRequestDatetime, 2))
                        .date(DateConverter.convertDateToMMddyyyy(convertedRequestDatetime))
                        .referenceNumber(loa.getApprovalNo())
                        .doctor(loa.getDoctorName())
                        .hospital(hospitalName)
                        .memberName(loa.getMemFname() + " " + loa.getMemLname())
                        .age(SharedPref.getStringValue(SharedPref.USER, SharedPref.AGE, context))
                        .gender(loa.getMemGender())
                        .memberId(loa.getMemCode())
                        .company(loa.getMemCompany())
                        .chiefComplaint(primaryComplaint)
                        .dateEffectivity(SharedPref.getStringValue(SharedPref.USER, SharedPref.EFFECTIVE, context))
                        .validityDate(SharedPref.getStringValue(SharedPref.USER, SharedPref.VALID, context))
                        .serviceType(loa.getRequestType())
                        .batchCode(loa.getBatchCode())
                        .remarks(remarks)
                        .imgApproval(bmp)
                        .diagnosis(loa.getPrimaryDiag())
                        .primaryDianosisWorkingImpression(loa.getDiagnosis())
                        .procedure(basicTestCode);
            } else {
                loaFormBuilder = new OutPatientConsultationForm.Builder()
                        .validFrom("NOT VALID")
                        .validUntil("NOT VALID")
                        .date("NOT VALID")
                        .referenceNumber("NOT APPROVED")
                        .doctor(loa.getDoctorName())
                        .hospital(SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALNAME, this))
//                       TODO: palitan
//                        .memberName(loa.getMemFname() + " " + loa.getMemLname())
                        .memberName(loa.getMemGender() + " " + loa.getMemFname())
                        .age(SharedPref.getStringValue(SharedPref.USER, SharedPref.AGE, context))
                        .gender(GenderPicker.setGender(Integer.parseInt(SharedPref.getStringValue(SharedPref.USER, SharedPref.GENDER, context))))
                        .memberId(loa.getMemberCode())
                        .company(loa.getMemCompany())
                        .chiefComplaint(primaryComplaint)
                        .dateEffectivity(SharedPref.getStringValue(SharedPref.USER, SharedPref.EFFECTIVE, context))
                        .validityDate(SharedPref.getStringValue(SharedPref.USER, SharedPref.VALID, context))
                        .serviceType(loa.getRequestType())
                        .batchCode(loa.getBatchCode())
                        .remarks(remarks)
                        .imgApproval(bmp);
            }


        }
    }

    private void procedureLoaDisplay() {
        if (null != loa.getMappedTest()) {
            ARRAYLIST_MappedTestClinicProc = loa.getMappedTest();
            System.out.println("ARRAYLIST_MappedTestClinicProc: " + ARRAYLIST_MappedTestClinicProc);
        }

        try {
            for (LoaList.MappedTest mappedTest : ARRAYLIST_MappedTestClinicProc) {
                clinicProcCodesMappedTest.add(mappedTest.getProcDesc());
                clinicListData.add(mappedTest.getAmount());
                notes = mappedTest.getNotes();

            }
            adapterListDesc.notifyDataSetChanged();
            adapterListData.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }

        adapterListDesc = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, clinicProcCodesMappedTest);
        lv_clinicproc_selected_title.setAdapter(adapterListDesc);
        lv_clinicproc_selected_title.setEnabled(false);
        setListViewHeightBasedOnItems(lv_clinicproc_selected_title);
        adapterListData = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, clinicListData);
        lv_clinicproc_selected_data.setAdapter(adapterListData);
        lv_clinicproc_selected_data.setEnabled(false);
        setListViewHeightBasedOnItems(lv_clinicproc_selected_data);
//        Log.d("procedures. ", procedures);

        loaListProcedureAdapter = new LoaListProcedureAdapter(context, ARRAYLIST_MappedTestClinicProc);
        System.out.println("LOALISTADAPTER FIRST: " + loaListProcedureAdapter + ARRAYLIST_MappedTestClinicProc);

    }

    private void basictestLoaDisplay() {
        if (!Constant.PROCEDURE.equalsIgnoreCase(loa.getRequestType())) {
            txt_procedure.setVisibility(View.GONE);
            txt_Price.setVisibility(View.GONE);
        }
        try {
            for (LoaList.GroupedByCostCenter groupedByCostCenter : loa.getGroupedByCostCenters()) {
                for (int i = 0; i < groupedByCostCenter.getGroupedByDiag().size(); i++) {
                    LoaList.GroupedByCostCenter.GroupedByDiag groupedByDiag = groupedByCostCenter.getGroupedByDiag().get(i);
                    groupedByDiag.setFirstInstance(i == 0 ? true : false);
                    groupedByDiag.setBasicTestFlag(loa.getRequestType().contains("BASIC TEST") ? true : false);
                    groupedByDiag.setCostCenter(groupedByCostCenter.getCostCenter());
                    groupedByDiag.setStatus(groupedByCostCenter.getStatus());
                    groupedByDiag.setSubTotal(groupedByCostCenter.getSubTotal());
                    ARRAYLIST_GroupedByDiag.add(groupedByDiag);
                }
            }
            //Process and group by DiagType -> Tests + CostCenter
            adapter = new BasicTestLoaAdapter(context, ARRAYLIST_GroupedByDiag);
            rv_otherTestAndBasicTest.setLayoutManager(new LinearLayoutManager(this));
            rv_otherTestAndBasicTest.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> processGrouping(ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> gbdList) {
        ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> returnedList = new ArrayList<>();
        ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> primaryList = arrangeListByDiagType(gbdList, 1);
        ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> otherList = arrangeListByDiagType(gbdList, 2);

        returnedList.addAll(setSubTotalAndIndices(primaryList));
        returnedList.addAll(setSubTotalAndIndices(otherList));

        return returnedList;
    }

    private ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> arrangeListByDiagType(ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> gbdList, int diagType){
        int a = 0;
        ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> primaryList = new ArrayList<>();
        for (LoaList.GroupedByCostCenter.GroupedByDiag groupedByDiag : gbdList) {
            if (groupedByDiag.getDiagType() == diagType) {
                groupedByDiag.setFirstInstance(a++ == 0 ? true : false);
                groupedByDiag.setLastInstance(false);
                groupedByDiag.setBasicTestFlag(false);
                primaryList.add(groupedByDiag);
            }
        }
        return primaryList;
    }

    private ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> setSubTotalAndIndices(ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> primaryList){
        //Subtotal of CostCenter
        String costCenter = "";
        Double subTotal = 0.0;
        for (int i = 0; i < primaryList.size(); i++) {
            //Set initial CostCenter
            if (i == 0)
                costCenter = primaryList.get(i).getCostCenter();
            //Check if grouped CostCenter is equal to current CostCenter
            if (!primaryList.get(i).getCostCenter().equals(costCenter)) {
                costCenter = primaryList.get(i).getCostCenter();
                primaryList.get(i - 1).setSubTotal(String.valueOf(subTotal));
                primaryList.get(i - 1).setLastInstance(true);
                subTotal = 0.0;
            }
            for (LoaList.MappedTest mappedTest : primaryList.get(i).getMappedTests()) {
                subTotal += Double.valueOf(mappedTest.getAmount());
            }
            if(i == primaryList.size() - 1){
                primaryList.get(i).setSubTotal(String.valueOf(subTotal));
                primaryList.get(i).setLastInstance(true);
            }
        }

        return primaryList;
    }

    private void otherTestLoaDisplay() {
        if (!Constant.PROCEDURE.equalsIgnoreCase(loa.getRequestType())) {
            txt_procedure.setVisibility(View.GONE);
            txt_Price.setVisibility(View.GONE);
        }
        try {
            ArrayList<LoaList.GroupedByCostCenter.GroupedByDiag> unfilteredGrouping = new ArrayList<>();
            for (LoaList.GroupedByCostCenter groupedByCostCenter : loa.getGroupedByCostCenters()) {
                for (int i = 0; i < groupedByCostCenter.getGroupedByDiag().size(); i++) {
                    LoaList.GroupedByCostCenter.GroupedByDiag groupedByDiag = groupedByCostCenter.getGroupedByDiag().get(i);
                    groupedByDiag.setBasicTestFlag(loa.getRequestType().contains("BASIC TEST") ? true : false);
                    groupedByDiag.setCostCenter(groupedByCostCenter.getCostCenter());
                    groupedByDiag.setStatus(groupedByCostCenter.getStatus());
                    groupedByDiag.setSubTotal(groupedByCostCenter.getSubTotal());
                    unfilteredGrouping.add(groupedByDiag);
                }
            }
            //Process and group by DiagType -> Tests + CostCenter
            ARRAYLIST_GroupedByDiag.addAll(processGrouping(unfilteredGrouping));
            otherTestAdapter = new OtherTestLoaAdapter(context, ARRAYLIST_GroupedByDiag);
            rv_otherTestAndBasicTest.setLayoutManager(new LinearLayoutManager(this));
            rv_otherTestAndBasicTest.setAdapter(otherTestAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setDataProblem(TextView tv_problem, String primaryComplaint, CardView cv_hosp) {
        String getComplaint = primaryComplaint;

        if (getComplaint.trim().equals("") || getComplaint.trim().equals("null")) {
            cv_hosp.setVisibility(View.GONE);
        } else {
            cv_hosp.setVisibility(View.VISIBLE);
            tv_problem.setText(primaryComplaint);
        }

    }


    private void setDataFromEmpty(TextView tv_approval_code, String requestDatetime, TextView tv_ref_tag) {

        if (null != requestDatetime && requestDatetime.length() == 0) {
            tv_approval_code.setVisibility(View.GONE);
            tv_ref_tag.setVisibility(View.GONE);
        } else {
            tv_approval_code.setText(requestDatetime);
        }
    }

    @OnClick({R.id.btn_ok, R.id.btn_download, R.id.btn_download_test})
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_ok:
                startActivity(new Intent(LoaDisplayActivity.this, CoordinatorMenuActivity.class));
                break;

            case R.id.btn_download:
                generateLoaConsultationForm();
                break;

            case R.id.btn_download_test:
                generateTestForm();
                break;
        }
    }

    private void generateLoaConsultationForm() {
        Log.d("builder", loaFormBuilder.toString());
        if (PermissionUtililities.hasPermissionToReadAndWriteStorage(this)) {
            GenerateForm.generateLoaForm(loaFormBuilder.build(),
                    getResources().openRawResource(R.raw.loa_consultation_form), pdfCallback);
        }

    }

    private void generateTestForm() {
        Log.d("builder", loaFormBuilder.toString());
        if (PermissionUtililities.hasPermissionToReadAndWriteStorage(this)) {
            GenerateForm.generateLoaForm(loaFormBuilder.build(),
                    getResources().openRawResource(R.raw.test_form), pdfCallback);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // Timber.d("request code for storate %s permission response %s", PermissionUtililities.READ_WRITE_PERMISSION, requestCode);
        switch (requestCode) {
            case PermissionUtililities.REQUESTCODE_STORAGE_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    generateLoaConsultationForm();

                } else {
                    //  Timber.d("permission denied");
                }
            }

            return;
        }
    }

    private void setData(TextView tv, String data) {
        tv.setText(data);
    }


    @Override
    public void onGenerateLoaFormSuccess(String serviceType, String referenceNumber) {
        alertyDialog.showMePrintableLOA(context, "", getString(R.string.success_download), loa.getRequestType(), loa.getApprovalNo());

        isFileExist = CheckFileExistence.fileExistance(loa.getRequestType(), loa.getApprovalNo());
        implement.setDownloadButton(isFileExist, btn_download, btn_download_test);

    }

    @Override
    public void onGenerateLoaFormSuccess() {

    }

    @Override
    public void onGenerateLoaFormError() {
        Log.d("ERROR", "ERROR");
    }

    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                float px = 500 * (listView.getResources().getDisplayMetrics().density);
                item.measure(View.MeasureSpec.makeMeasureSpec((int) px, View.MeasureSpec.AT_MOST), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);
            // Get padding
            int totalPadding = listView.getPaddingTop() + listView.getPaddingBottom();

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight + totalPadding;
            listView.setLayoutParams(params);
            listView.requestLayout();
            return true;

        } else {
            return false;
        }

    }

}
