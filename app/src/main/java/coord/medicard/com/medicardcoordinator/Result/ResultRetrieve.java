package coord.medicard.com.medicardcoordinator.Result;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.InputStream;

import coord.medicard.com.medicardcoordinator.R;
import utilities.pdfUtil.GenerateForm;
import utilities.pdfUtil.OutPatientConsultationForm;

/**
 * Created by mpx-pawpaw on 5/3/17.
 */

public class ResultRetrieve {

    private Context context;
    private final String withProvider = "withProvider";

    public ResultRetrieve(Context context) {
        this.context = context;

    }


    public void generateLoaForm(OutPatientConsultationForm build, InputStream inputStream, GenerateForm.PdfCallback pdfCallback) {

        GenerateForm.generateLoaForm(build, inputStream, pdfCallback);
    }

    public void withProviderVisiblity(String with_provider, TextView tv_doc_app) {
        // "NONE" MUST NOT VISIBLE
        // "withProvider" MUST SHOW


        if (with_provider.equalsIgnoreCase(withProvider))
            tv_doc_app.setVisibility(View.GONE);
        else
            tv_doc_app.setVisibility(View.VISIBLE);
    }


    public void setDownloadButton(Button btn_download, Button btn_download_test, boolean isFileExisting) {
        if (isFileExisting){
            btn_download.setText(context.getString(R.string.view_loa));
        }else{
            btn_download.setText(context.getString(R.string.download_loa));
        }
        if (isFileExisting){
            btn_download_test.setText(context.getString(R.string.view_loa));
        } else{
            btn_download_test.setText(context.getString(R.string.download_test));
        }
    }
}
