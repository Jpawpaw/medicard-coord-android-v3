package coord.medicard.com.medicardcoordinator.Navigator;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import coord.medicard.com.medicardcoordinator.R;
import model.DiagnosisList;
import model.Loa;
import model.LoaList;
import utilities.DataBaseHandler;
import utilities.DateConverter;
import utilities.StatusChecker;

/**
 * Created by mpx-pawpaw on 4/11/17.
 */

public class ConsultationAdapter extends RecyclerView.Adapter<ConsultationAdapter.Holder> {

    private ArrayList<LoaList> arrayList;
    private Context context;
    private ConsultationListInterface callback;
    private DataBaseHandler database;
    private Dialog dialog;

    public ConsultationAdapter(ArrayList<LoaList> arrayList, Context context, ConsultationListInterface callback, Dialog dialog) {
        this.arrayList = arrayList;
        this.context = context;
        this.callback = callback;
        database = new DataBaseHandler(context);
        this.dialog = dialog;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new Holder(LayoutInflater.from(context).inflate(R.layout.row_loareq, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        setData(arrayList.get(position).getRequestType(), holder.service_type);
        setData(database.getDoctorName(arrayList.get(position).getDoctorCode()), holder.doc);
        setData(database.getDoctorSpec(arrayList.get(position).getDoctorCode()), holder.spec);
        setData(DateConverter.convertDatetoyyyyMMdd_(arrayList.get(position).getRequestDatetime()), holder.request);
        setData(StatusChecker.setStatusHeader(arrayList.get(position).getRequestType(), arrayList.get(position).getStatus(),
                arrayList.get(position).getRequestDatetime()), holder.status);


    }

    private void setData(String data, TextView textView) {
        if (data.equals(""))
            textView.setVisibility(View.GONE);
        else
            textView.setText(data);

    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {


        @BindView(R.id.service_type)
        TextView service_type;
        @BindView(R.id.doc)
        TextView doc;
        @BindView(R.id.spec)
        TextView spec;
        @BindView(R.id.request)
        TextView request;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.ll_view)
        LinearLayout ll_view;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            ll_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onSelectedConsultation(arrayList.get(getAdapterPosition()));
                    dialog.dismiss();
                }
            });


        }


    }


    public interface ConsultationListInterface {
        void onSelectedConsultation(LoaList s);
    }
}
