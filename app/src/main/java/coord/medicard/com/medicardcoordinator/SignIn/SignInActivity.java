package coord.medicard.com.medicardcoordinator.SignIn;

/*
    Author Francisco Aquino III
    Start of Imports Libraries used for Login Activity

 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import adapter.AnnouncementAdapter;
import coord.medicard.com.medicardcoordinator.BuildConfig;
import coord.medicard.com.medicardcoordinator.MemberSearch.MemberSearchActivity;
import coord.medicard.com.medicardcoordinator.R;
import model.AnnouncementDataJson;
import model.SignInDetails;
import utilities.AccessGooglePlay;
import utilities.AlertDialogCustom;
import utilities.DataBaseHandler;
import utilities.DataReaderFromCsv;
import utilities.ErrorMessage;
import utilities.APIMultipleCalls.NetworkTest;
import utilities.Permission;
import utilities.PhoneInformations;
import utilities.SharedPref;
import utilities.APIMultipleCalls.SignInAPiCall;
import utilities.SnackBarSet;
import utilities.UpdateCaller;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener, UpdateCaller.DialogUpdateInterface,
        SignInAPiCall.SignInCallback, SignInCallback {
    /*
        Declarations of Global variables
     */
    SharedPref s = new SharedPref();
    EditText ed_userid, ed_password;
    Button btn_signIn;
    Context context;
    SnackBarSet snackBarSet;
    CoordinatorLayout coords;
    AlertDialogCustom alertDialogCustom = new AlertDialogCustom();
    ProgressDialog pd;
    DataBaseHandler databaseH;
    DataReaderFromCsv dataReaderFromCsv;
    String versionNumber = "";
    TextView tv_imei;
    TextView tv_build;
    TextView tv_serial;
    SignInDetails signInDetails;
    UpdateCaller.DialogUpdateInterface dialogUpdateCallback;
    SignInImplement implement;
    SignInAPiCall.SignInCallback signInCallback;
    SignInCallback callback;
    CardView cv_notif;
    RecyclerView rv_notif;
    AnnouncementAdapter announcementAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        context = this;
        dialogUpdateCallback = this;
        implement = new SignInImplement(context);
        signInCallback = this;
        callback = this;
        databaseH = new DataBaseHandler(this);
        dataReaderFromCsv = new DataReaderFromCsv(context, databaseH);
        init();


    }

    private void checkFirstRun() {
        //TODO changes in localization of data
        /**
         * first login - extract
         * check update
         * delete content after extraction
         * second and consecutive  login - check update
         * if has update
         * retrieve updated rows from service
         * and update the rows on local
         *
         *
         * to be done also on mem app v2 (for approval)
         * */


        if (null == SharedPref.getStringValue(SharedPref.USER, SharedPref.INIT_COMPLETE, context)) {

        } else if (!SharedPref.getStringValue(SharedPref.USER, SharedPref.INIT_COMPLETE, context).equals("NO INIT")) {

        } else {
            SharedPref.setStringValue(SharedPref.USER, SharedPref.DOCTOR_LIST, String.valueOf(false), context);
            SharedPref.setStringValue(SharedPref.USER, SharedPref.DOCTOR_LIST_LAST_UPDATE, "1900-01-01", context);

            SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_LIST, String.valueOf(false), context);
            SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_LIST_LAST_UPDATE, "1900-01-01", context);

            SharedPref.setStringValue(SharedPref.USER, SharedPref.DIAGNOSIS_LIST, String.valueOf(false), context);
            SharedPref.setStringValue(SharedPref.USER, SharedPref.DIAGNOSIS_LIST_LAST_UPDATE, "1900-01-01", context);

            SharedPref.setStringValue(SharedPref.USER, SharedPref.HOSPITAL_LIST, String.valueOf(false), context);
            SharedPref.setStringValue(SharedPref.USER, SharedPref.HOSPITAL_LIST_LAST_UPDATE, "1900-01-01", context);

            SharedPref.setStringValue(SharedPref.USER, SharedPref.ROOM_PLAN_LIST, String.valueOf(false), context);
            SharedPref.setStringValue(SharedPref.USER, SharedPref.ROOM_PLAN_LIST_LAST_UPDATE, "1900-01-01", context);

            SharedPref.setStringValue(SharedPref.USER, SharedPref.ROOM_CATEGORY_LIST, String.valueOf(false), context);
            SharedPref.setStringValue(SharedPref.USER, SharedPref.ROOM_CATEGORY_LIST_LAST_UPDATE, "1900-01-01", context);

            SharedPref.setStringValue(SharedPref.USER, SharedPref.INIT_COMPLETE, "INITIALIZED", context);
        }
        System.out.println("SharedPref.INIT_COMPLETE" + SharedPref.INIT_COMPLETE);

//        if (!SharedPref.INIT_COMPLETE.equals("NO INIT")) {
//            //on app consecutive run
//        } else {
//            //on app first run
//            SharedPref.setStringValue(SharedPref.USER, SharedPref.DOCTOR_LIST, String.valueOf(false), context);
//            SharedPref.setStringValue(SharedPref.USER, SharedPref.DOCTOR_LIST_LAST_UPDATE, "1900-01-01", context);
//
//            SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_LIST, String.valueOf(false), context);
//            SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_LIST_LAST_UPDATE, "1900-01-01", context);
//
//            SharedPref.setStringValue(SharedPref.USER, SharedPref.DIAGNOSIS_LIST, String.valueOf(false), context);
//            SharedPref.setStringValue(SharedPref.USER, SharedPref.DIAGNOSIS_LIST_LAST_UPDATE, "1900-01-01", context);
//
//            SharedPref.setStringValue(SharedPref.USER, SharedPref.HOSPITAL_LIST, String.valueOf(false), context);
//            SharedPref.setStringValue(SharedPref.USER, SharedPref.HOSPITAL_LIST_LAST_UPDATE, "1900-01-01", context);
//
//            SharedPref.setStringValue(SharedPref.USER, SharedPref.ROOM_PLAN_LIST, String.valueOf(false), context);
//            SharedPref.setStringValue(SharedPref.USER, SharedPref.ROOM_PLAN_LIST_LAST_UPDATE, "1900-01-01", context);
//
//            SharedPref.setStringValue(SharedPref.USER, SharedPref.ROOM_CATEGORY_LIST, String.valueOf(false), context);
//            SharedPref.setStringValue(SharedPref.USER, SharedPref.ROOM_CATEGORY_LIST_LAST_UPDATE, "1900-01-01", context);
//            SharedPref.INIT_COMPLETE = "INITIALIZED";
////            SharedPref.setStringValue(SharedPref.USER, SharedPref.INIT_COMPLETE, "INITIALIZED", context);
//        }

    }


    private void init() {

        cv_notif = (CardView) findViewById(R.id.cv_notif);
        rv_notif = (RecyclerView) findViewById(R.id.rv_notif);

        pd = new ProgressDialog(SignInActivity.this, R.style.MyTheme);
        pd.setCancelable(false);
        pd.setMessage("Authenticating...");
        pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);


        tv_imei = (TextView) findViewById(R.id.tv_imei);
        tv_serial = (TextView) findViewById(R.id.tv_serial);
        tv_build = (TextView) findViewById(R.id.tv_build);
        ed_userid = (EditText) findViewById(R.id.ed_userid);
        ed_password = (EditText) findViewById(R.id.ed_password);
        btn_signIn = (Button) findViewById(R.id.btn_signIn);
        coords = (CoordinatorLayout) findViewById(R.id.coords);
        btn_signIn.setOnClickListener(this);

        snackBarSet = new SnackBarSet();
        versionNumber = "v." + BuildConfig.VERSION_NAME;
        tv_build.setText("VERSION: " + versionNumber);


        if (Permission.checkPermissionPhone(context)) {
            tv_imei.setText("IMEI: " + PhoneInformations.getIMEI(context));
        } else
            tv_imei.setText("Allow permission to view IMEI");

        Log.d("SERIAL", Build.SERIAL);
        tv_serial.setText("HARDWARE SERIAL: " + Build.SERIAL);

        notification();


    }

    public void notification() {
        SignInAPiCall.getNotification(signInCallback);

    }

    @Override
    public void onNotificationSuccess(AnnouncementDataJson announcementDataJson) {
        cv_notif.setVisibility(View.VISIBLE);
        announcementAdapter = new AnnouncementAdapter(SignInActivity.this, announcementDataJson.getAnnouncements());
        rv_notif.setAdapter(announcementAdapter);
        announcementAdapter.notifyDataSetChanged();
        rv_notif.setLayoutManager(new LinearLayoutManager(this));
    }


    @Override
    public void onSuccessSignIn(SignInDetails getSignIn) {


        if (getSignIn.getResponseCode().equals("200")) {
            signInDetails = getSignIn;
            /**
             * If our table date is equal to app date: Set Value to False -> Meaning do not update list anymore.
             * */
            checkFirstRun();
            setSharedForLatestUpdate(getSignIn);
            //For Procedures Listing
            SharedPref.setStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, signInDetails.getHospital().getHospitalCode(), context);
            SharedPref.setStringValue(SharedPref.USER, SharedPref.RECENT_TRANS_MEMCODE, "", context);
            if (NetworkTest.isOnline(context)) {
                if (SharedPref.getStringValue(SharedPref.USER, SharedPref.DOCTOR_LIST, context).equalsIgnoreCase("false")) {
                    pd.setMessage("Loading all lists...\n" + context.getString(R.string.update_prompt));
                    implement.updateAllList(callback, dataReaderFromCsv);
                    setSharedForUpdate(getSignIn);
                } else {
                    pd.setMessage("Updating Doctor list...\n" + context.getString(R.string.update_prompt));
                    SignInAPiCalls.getDoctor(getSignIn.getHospital().getHospitalCode(), callback, implement, pd, context, getSignIn.getTables().get("DOCHOSP"));
                }
            } else {
                pd.dismiss();
                alertDialogCustom.showMe(context, alertDialogCustom.NO_Internet_title, alertDialogCustom.NO_Internet, 1);
            }
        } else if (getSignIn.getResponseCode().equals("270")) {
            pd.dismiss();
            alertDialogCustom.showMe(context, alertDialogCustom.INVALID_PASS_USERP_title, "Device is not assigned to AppUser", 1);
        } else if (getSignIn.getResponseCode().equals("280")) {

            signInDetails = getSignIn;
            UpdateCaller.showUpdateCall(context, "Optional Update Available", false, dialogUpdateCallback);
            pd.setMessage("Please wait...");

        } else if (getSignIn.getResponseCode().equals("290")) {
            pd.dismiss();
            UpdateCaller.showUpdateCall(context, "Update Required", true, dialogUpdateCallback);
        } else if (getSignIn.getResponseCode().equals("250")) {
            pd.dismiss();
            alertDialogCustom.showMe(context, alertDialogCustom.INVALID_PASS_USERP_title, alertDialogCustom.INVALID_PASS_USER, 1);
        } else if (getSignIn.getResponseCode().equals("210")) {
            pd.dismiss();
            alertDialogCustom.showMe(context, alertDialogCustom.INVALID_PASS_USERP_title, getSignIn.getResponseDesc(), 1);
        } else {
            pd.dismiss();
            alertDialogCustom.showMe(context, alertDialogCustom.not_title, ErrorMessage.setErrorMessage("no response to server"), 1);
        }
    }

    private void setSharedForLatestUpdate(SignInDetails getSignIn) {
        SharedPref.lastUpdateTables.put("DOCHOSP", getSignIn.getTables().get("DOCHOSP")
                .equals(SharedPref.getStringValue(SharedPref.USER, SharedPref.DOCTOR_LIST_LAST_UPDATE, context)));

        SharedPref.lastUpdateTables.put("DIAGNOSIS", getSignIn.getTables().get("DIAGNOSIS")
                .equals(SharedPref.getStringValue(SharedPref.USER, SharedPref.DIAGNOSIS_LIST_LAST_UPDATE, context)));

        SharedPref.lastUpdateTables.put("TESTPROC", getSignIn.getTables().get("TESTPROC")
                .equals(SharedPref.getStringValue(SharedPref.USER, SharedPref.PROCEDURE_LIST_LAST_UPDATE, context)));

        SharedPref.lastUpdateTables.put("HOSPITAL", getSignIn.getTables().get("HOSPITAL")
                .equals(SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITAL_LIST_LAST_UPDATE, context)));

        SharedPref.lastUpdateTables.put("ROOMCATEGORY", getSignIn.getTables().get("ROOMCATEGORY")
                .equals(SharedPref.getStringValue(SharedPref.USER, SharedPref.ROOM_CATEGORY_LIST_LAST_UPDATE, context)));

        SharedPref.lastUpdateTables.put("ROOMPLAN", getSignIn.getTables().get("ROOMPLAN")
                .equals(SharedPref.getStringValue(SharedPref.USER, SharedPref.ROOM_PLAN_LIST_LAST_UPDATE, context)));

    }

    private void setSharedForUpdate(SignInDetails getSignIn) {
        SharedPref.setStringValue(SharedPref.USER, SharedPref.DOCTOR_LIST, String.valueOf(true), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.DOCTOR_LIST_LAST_UPDATE, getSignIn.getTables().get("DOCHOSP"), context);

        SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_LIST, String.valueOf(true), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_LIST_LAST_UPDATE, getSignIn.getTables().get("TESTPROC"), context);

        SharedPref.setStringValue(SharedPref.USER, SharedPref.DIAGNOSIS_LIST, String.valueOf(true), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.DIAGNOSIS_LIST_LAST_UPDATE, getSignIn.getTables().get("DIAGNOSIS"), context);

        SharedPref.setStringValue(SharedPref.USER, SharedPref.HOSPITAL_LIST, String.valueOf(true), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.HOSPITAL_LIST_LAST_UPDATE, getSignIn.getTables().get("HOSPITAL"), context);

        SharedPref.setStringValue(SharedPref.USER, SharedPref.ROOM_PLAN_LIST, String.valueOf(true), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.ROOM_PLAN_LIST_LAST_UPDATE, getSignIn.getTables().get("ROOMPLAN"), context);

        SharedPref.setStringValue(SharedPref.USER, SharedPref.ROOM_CATEGORY_LIST, String.valueOf(true), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.ROOM_CATEGORY_LIST_LAST_UPDATE, getSignIn.getTables().get("ROOMCATEGORY"), context);
    }

    @Override
    public void onErrorSignIn(String message) {

        pd.dismiss();

        try {
            alertDialogCustom.showMe(context, alertDialogCustom.not_title, ErrorMessage.setErrorMessage(message), 1);
        } catch (Exception error) {
            alertDialogCustom.showMe(context, alertDialogCustom.not_title, ErrorMessage.setErrorMessage(message), 1);
        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_signIn:
                if (ed_userid.getText().toString().trim().equals("") ||
                        ed_password.getText().toString().equals("")) {
                    try {
                        snackBarSet.setSnackBar(coords, snackBarSet.INCOMPLETE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        if (Permission.checkPermissionPhone(context)) {
                            if (NetworkTest.isOnline(context)) {
                                pd.show();
                                implement.getSignInData(ed_userid.getText().toString().trim(), ed_password.getText().toString().trim(), signInCallback);
                            } else
                                alertDialogCustom.showMe(context, alertDialogCustom.NO_Internet_title, alertDialogCustom.NO_Internet, 1);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                break;
        }
    }


//    public void signIn(final String username, final String password) {
//
//        pd.show();
//        LogIn logIn = new LogIn();
//        logIn.setUsername(username);
//        logIn.setPassword(password);
//        logIn.setDeviceID(PhoneInformations.getIMEI(context));
//        logIn.setVersionNo(BuildConfig.VERSION_NAME);
//
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.PHONE_ID, PhoneInformations.getIMEI(context), context);
//        Gson gson = new Gson();
//        Log.d("JSON", gson.toJson(logIn));
//
//
//        AppInterface appInterface;
//        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
//        appInterface.logInUser(logIn)
//                .enqueue(new Callback<SignInDetails>() {
//                    @Override
//                    public void onResponse(Call<SignInDetails> call, Response<SignInDetails> response) {
//
//                        SignInDetails getSignIn = response.body();
//
//                        try {
//                            if (getSignIn.getResponseCode().equals("200")) {
//                                signInDetails = getSignIn;
//
//                                if (NetworkTest.isOnline(context)) {
//                                    getDoctor(response.body().getHospital().getHospitalCode());
//                                } else {
//                                    pd.dismiss();
//                                    alertDialogCustom.showMe(context, alertDialogCustom.NO_Internet_title, alertDialogCustom.NO_Internet, 1);
//                                }
//                            } else if (getSignIn.getResponseCode().equals("270")) {
//                                pd.dismiss();
//                                alertDialogCustom.showMe(context, alertDialogCustom.INVALID_PASS_USERP_title, "Device is not assigned to AppUser", 1);
//                            } else if (getSignIn.getResponseCode().equals("280")) {
//
//                                signInDetails = getSignIn;
//                                UpdateCaller.showUpdateCall(context, "Optional Update Available", false, callback);
//                                pd.setMessage("Please wait...");
//
//                            } else if (getSignIn.getResponseCode().equals("290")) {
//                                pd.dismiss();
//                                UpdateCaller.showUpdateCall(context, "Update Required", true, callback);
//                            } else if (getSignIn.getResponseCode().equals("250")) {
//                                pd.dismiss();
//                                alertDialogCustom.showMe(context, alertDialogCustom.INVALID_PASS_USERP_title, alertDialogCustom.INVALID_PASS_USER, 1);
//                            } else if (getSignIn.getResponseCode().equals("210")) {
//                                pd.dismiss();
//                                alertDialogCustom.showMe(context, alertDialogCustom.INVALID_PASS_USERP_title, getSignIn.getResponseDesc(), 1);
//                            }
//
//                        } catch (Exception e) {
//                            pd.dismiss();
//                            alertDialogCustom.showMe(context, alertDialogCustom.not_title, ErrorMessage.setErrorMessage(""), 1);
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<SignInDetails> call, Throwable e) {
//
//                        pd.dismiss();
//
//                        try {
//                            alertDialogCustom.showMe(context, alertDialogCustom.not_title, ErrorMessage.setErrorMessage(e.getMessage()), 1);
//                        } catch (Exception error) {
//                            alertDialogCustom.showMe(context, alertDialogCustom.not_title, ErrorMessage.setErrorMessage(""), 1);
//
//                        }
////                        if (e.getMessage().equals("failed to connect to"))
////                            alertDialogCustom.showMe(context, alertDialogCustom.INVALID_PASS_USERP_title, alertDialogCustom.NO_Internet, 1);
////                        else
////                            alertDialogCustom.showMe(context, alertDialogCustom.INVALID_PASS_USERP_title, alertDialogCustom.INVALID_PASS_USER, 1);
//
//
//                    }
//                });
//
//
//    }


//        public void getDoctor(String hospitalCode) {
//            pd.setMessage("Updating Doctor list...");
//            AppInterface appInterface;
//            appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
//            appInterface.getDoctorList(hospitalCode)
//                    .enqueue(new Callback<DoctorModel>() {
//                        @Override
//                        public void onResponse(Call<DoctorModel> call, Response<DoctorModel> response) {
//                            Gson gson = new Gson();
//                            Log.d("DOCTORS", gson.toJson(response.body()));
//                            Log.d("DOCTORS", response.body().getGetDoctorsToHospital().get(0).getDocLname());
//                            implement.upDateDoctor(response.body().getGetDoctorsToHospital(), callback);
//
//                        }
//
//                        @Override
//                        public void onFailure(Call<DoctorModel> call, Throwable t) {
//
//                        }
//                    });
//
//
//        }


//
//
//    private void upDateDiagnosis(final ArrayList<DiagnosisList> diagnosisList) {
//
//
//        AsyncTask asyncTask = new AsyncTask() {
//
//            @Override
//            protected void onPostExecute(Object o) {
//
//                getRoomPlan();
//
//                super.onPostExecute(o);
//            }
//
//            @Override
//            protected void onPreExecute() {
//
//                pd.setMessage("Updating Diagnosis list...");
//
//                super.onPreExecute();
//            }
//
//            @Override
//            protected Object doInBackground(Object[] params) {
//
//                setDatatoDatabase.setDiagnosisListToDatabase(diagnosisList, databaseH);
//
//
//                return null;
//            }
//        };
//
//
//        asyncTask.execute();
//
//    }


//
//    private void updateRoomsAvail(final RoomsAvail body) {
//
//
//        AsyncTask asyncTask = new AsyncTask() {
//            @Override
//            protected Object doInBackground(Object[] objects) {
//
//                for (Rooms rooms : body.getRooms()) {
//                    databaseH.createRoomAvail(rooms);
//                }
//
//
//                return null;
//            }
//
//
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//
//                pd.setMessage("Updating Rooms Plan list...");
//
//
//            }
//
//            @Override
//            protected void onPostExecute(Object o) {
//                super.onPostExecute(o);
//
//                gotoMemberSearch();
//            }
//        };
//
//        asyncTask.execute();
//
//    }

    private void gotoMemberSearch() {
        pd.dismiss();

        SharedPref.setStringValue(SharedPref.USER, SharedPref.masterUSERNAME, ed_userid.getText().toString().trim(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.masterPASSWORD, ed_password.getText().toString().trim(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, signInDetails.getHospital().getHospitalCode(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.HOSPITALNAME, signInDetails.getHospital().getHospitalName(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.HOSPITALADDRESS, signInDetails.getHospital().getStreetAddress().trim()
                        + ", " + signInDetails.getHospital().getCity().trim() + ", " + signInDetails.getHospital().getProvince().trim() + ", " +
                        signInDetails.getHospital().getRegion().trim()
                , context);

        SharedPref.setStringValue(SharedPref.USER, SharedPref.EMAIL, signInDetails.getUser().getCNTCT_EMAIL(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.CONTACT_NO, signInDetails.getUser().getCNTCT_PHONENO(), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.COORD_NAME,
                signInDetails.getUser().getCNTCT_LNAME() +
                        " ," +
                        signInDetails.getUser().getCNTCT_FNAME() + " " +
                        signInDetails.getUser().getCNTCT_MNAME()
                , context);

        startActivity(new Intent(SignInActivity.this, MemberSearchActivity.class));
        finish();


    }


    @Override
    public void onBackPressed() {

    }

    @Override
    public void updateRequired() {
        AccessGooglePlay.openAppInGooglePlay(context);
    }

    @Override
    public void updateNotRequired() {

        if (NetworkTest.isOnline(context)) {
            SignInAPiCalls.getDoctor(signInDetails.getHospital().getHospitalCode(), callback, implement, pd, context, "");
        } else {
            pd.dismiss();
            alertDialogCustom.showMe(context, alertDialogCustom.NO_Internet_title, alertDialogCustom.NO_Internet, 1);
        }
    }


    @Override
    public void onSuccessDoctorFetch() {
        pd.setMessage("Updating Procedure list...\n" + context.getString(R.string.update_prompt));
        SignInAPiCalls.getProcedureLIst(callback, implement, pd, SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context), context);
    }

    @Override
    public void onErrorDoctorFetch(String message) {
        pd.dismiss();
        alertDialogCustom.showMe(context, alertDialogCustom.NO_Internet_title, ErrorMessage.setErrorMessage(message), 1);
        databaseH.deletedoctorTableData();
        SharedPref.setStringValue(SharedPref.USER, SharedPref.DOCTOR_LIST, String.valueOf(false), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.DOCTOR_LIST_LAST_UPDATE, "1900-01-01", context);

    }

    @Override
    public void onErrorProcedureFetch(String message) {
        pd.dismiss();
        alertDialogCustom.showMe(context, alertDialogCustom.noDoc_title, alertDialogCustom.noProcList, 1);
        databaseH.deleteprocedureTableData();
        SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_LIST, String.valueOf(false), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.PROCEDURE_LIST_LAST_UPDATE, "1900-01-01", context);
    }

    @Override
    public void onSuccessProcedureFetch() {
        pd.setMessage("Updating Diagnosis list...\n" + getString(R.string.update_prompt));
        SignInAPiCalls.getDiagnosisList(callback, implement, pd, context);
    }

    @Override
    public void onErrorDiagnosisList(String s) {
        pd.dismiss();
        alertDialogCustom.showMe(context, alertDialogCustom.noDoc_title, alertDialogCustom.noDiagList, 1);
        databaseH.deletediagnosisTableData();
        SharedPref.setStringValue(SharedPref.USER, SharedPref.DIAGNOSIS_LIST, String.valueOf(false), context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.DIAGNOSIS_LIST_LAST_UPDATE, "1900-01-01", context);
    }


    @Override
    public void onSuccessListingUpdate() {

    }

    @Override
    public void onErrorListingUpdate(String s) {
        alertDialogCustom.showMe(context, alertDialogCustom.noDoc_title, alertDialogCustom.noDiagList, 1);

    }

    @Override
    public void onSuccessDiagnosisFetch() {
        pd.setMessage("Updating Rooms Plan list...\n" + context.getString(R.string.update_prompt));
        SignInAPiCalls.getRoomPlan(callback, implement, pd);
    }

    @Override
    public void onErrorRoomPlanFetch(String message) {
        alertDialogCustom.showMe(context, alertDialogCustom.noDoc_title, ErrorMessage.setErrorMessage(message), 1);
        pd.dismiss();
    }

    @Override
    public void onSuccessRoomPlanFetch() {
        pd.setMessage("Updating Rooms Category list...\n" + context.getString(R.string.update_prompt));
        SignInAPiCalls.getRoomCategory(callback, implement, pd);

    }

    @Override
    public void onErrorRoomCategory(String message) {
        alertDialogCustom.showMe(context, alertDialogCustom.noDoc_title, ErrorMessage.setErrorMessage(message), 1);
        pd.dismiss();
    }

    @Override
    public void onSuccessRoomCategory() {
        gotoMemberSearch();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (pd != null)
            pd.dismiss();
    }

}
