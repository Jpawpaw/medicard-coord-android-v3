package coord.medicard.com.medicardcoordinator.SignIn;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;

import coord.medicard.com.medicardcoordinator.BuildConfig;
import coord.medicard.com.medicardcoordinator.R;
import model.DiagnosisList;
import model.GetDoctorsToHospital;
import model.LogIn;
import model.RoomCategory;
import model.RoomsAvail;
import model.TestsAndProcedures;
import utilities.Constant;
import utilities.DataBaseHandler;
import utilities.DataReaderFromCsv;
import utilities.PhoneInformations;
import utilities.SetDatatoDatabase;
import utilities.APIMultipleCalls.SignInAPiCall;

/**
 * Created by mpx-pawpaw on 6/19/17.
 */

public class SignInImplement {
    private Context context;

    public SignInImplement(Context context) {
        this.context = context;
    }

    public void getSignInData(String username, String password, SignInAPiCall.SignInCallback signInCallback) {
        LogIn logIn = new LogIn();
        logIn.setUsername(username);
        logIn.setPassword(password);
        logIn.setDeviceID(PhoneInformations.getIMEI(context));
        logIn.setVersionNo(BuildConfig.VERSION_NAME);
        SignInAPiCall.signIn(logIn, signInCallback);
    }

    public void upDateDoctor(final ArrayList<GetDoctorsToHospital> getDoctorsToHospital, final SignInCallback callback) {
        final DataBaseHandler databaseH = new DataBaseHandler(context);
        AsyncTask updateDoc = new AsyncTask() {
            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                callback.onSuccessDoctorFetch();
            }

            @Override
            protected Object doInBackground(Object[] params) {
                SetDatatoDatabase.setDoctorsDataToDatabase(getDoctorsToHospital, databaseH);
                return null;
            }
        };
        updateDoc.execute();
    }

    public void updateAllList(final SignInCallback callback, final DataReaderFromCsv dataReaderFromCsv) {
        final DataBaseHandler databaseH = new DataBaseHandler(context);
        AsyncTask updateDoc = new AsyncTask() {
            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                callback.onSuccessRoomCategory();
            }

            @Override
            protected Object doInBackground(Object[] params) {
                databaseH.deleteTableData();
                dataReaderFromCsv.unZip(context, Constant.ZIP_NAME);
                return null;
            }
        };
        updateDoc.execute();
    }


    public void updateProcedure(final SignInCallback callback, final ArrayList<TestsAndProcedures> proceduresListLocal) {
        final DataBaseHandler databaseH = new DataBaseHandler(context);
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                callback.onSuccessProcedureFetch();
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Object doInBackground(Object[] params) {
                SetDatatoDatabase.setProcedureListToDatabase(proceduresListLocal, databaseH);
                return null;
            }
        };
        asyncTask.execute();
    }


    public void upDateDiagnosis(final SignInCallback callback, final ArrayList<DiagnosisList> diagnosisList) {
        final DataBaseHandler databaseH = new DataBaseHandler(context);
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected void onPostExecute(Object o) {
                callback.onSuccessDiagnosisFetch();
                super.onPostExecute(o);
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Object doInBackground(Object[] params) {
                SetDatatoDatabase.setDiagnosisListToDatabase(diagnosisList, databaseH);
                return null;
            }
        };
        asyncTask.execute();
    }


    public void updateRoomsAvail(final RoomsAvail body, final ProgressDialog pd, final SignInCallback callback) {
        final DataBaseHandler databaseH = new DataBaseHandler(context);
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                SetDatatoDatabase.setRoomPlanToDatabase(body, databaseH);
                return null;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                callback.onSuccessRoomPlanFetch();
            }
        };
        asyncTask.execute();
    }


    public void updateRoomCategory(final RoomCategory body, final ProgressDialog pd, final SignInCallback callback) {
        final DataBaseHandler databaseH = new DataBaseHandler(context);
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                SetDatatoDatabase.setRoomCategoryToDatabase(body, databaseH);
                return null;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                callback.onSuccessRoomCategory();
            }
        };
        asyncTask.execute();
    }
}
