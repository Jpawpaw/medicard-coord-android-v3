package coord.medicard.com.medicardcoordinator.MemberSearch;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import coord.medicard.com.medicardcoordinator.R;
import utilities.CreateList;
import utilities.DataBaseHandler;
import utilities.SharedPref;

/**
 * Created by mpx-pawpaw on 6/8/17.
 */

public class MemberSearchImplement {

    private Context context;


    public MemberSearchImplement(Context context) {
        this.context = context;
    }


    public void testDatabase(DataBaseHandler dataBaseHandler, MemberSearchCallback callback) {
        if (dataBaseHandler.isDatabaseEmpty()) {
            Log.d("DATABASE", "EMPTY");
            callback.logOut();
        } else
            Log.d("DATABASE", "FULL");
    }

    public String CollectBirthdate(int getYear, int getMonth, int getDay) {

        StringBuilder birthday = new StringBuilder();


        birthday
                .append(getYear)
                .append(getMonth <= 8 ? "0" + (getMonth + 1) : getMonth + 1)
                .append(getDay <= 8 ? "0" + getDay : getDay);
        return birthday.toString();


    }


    public void showCoordinatorDetails(final Context context, final MemberSearchCallback callback) {
        final Dialog showCoordData;

        showCoordData = new Dialog(context);
        showCoordData.requestWindowFeature(Window.FEATURE_NO_TITLE);
        showCoordData.setContentView(R.layout.dialog_coord_info);
        showCoordData.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        showCoordData.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        showCoordData.setCancelable(false);

        final Button btn_cancel, btn_update;
        //  ImageButton imgBtn_editContact;
        TextView tv_address, tv_email, tv_name;
        final EditText tv_contact;

        tv_address = (TextView) showCoordData.findViewById(R.id.tv_address);
        tv_contact = (EditText) showCoordData.findViewById(R.id.tv_contact);
        tv_email = (TextView) showCoordData.findViewById(R.id.tv_email);
        tv_name = (TextView) showCoordData.findViewById(R.id.tv_name);
        // tv_edit = (TextView) showCoordData.findViewById(R.id.tv_edit);
        btn_cancel = (Button) showCoordData.findViewById(R.id.btn_cancel);
        btn_update = (Button) showCoordData.findViewById(R.id.btn_update);


        tv_address.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALADDRESS, context));
        tv_contact.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.CONTACT_NO, context));
        tv_email.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.EMAIL, context));
        tv_name.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.COORD_NAME, context));


        btn_update.setClickable(false);
        btn_update.setBackgroundColor(ContextCompat.getColor(context, R.color.gray));


        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCoordData.dismiss();
                callback.onCloseDiagInfo();
            }
        });


        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!tv_contact.getText().toString().equals("") ||
                        SharedPref.getStringValue(SharedPref.USER, SharedPref.CONTACT_NO, context)
                                .equals(tv_contact.getText().toString())) {
                    callback.onUpdateDiagInfo(tv_contact.getText().toString(), showCoordData);

                }
            }
        });


        tv_contact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (tv_contact.getText().toString().trim().equals("") ||
                        SharedPref.getStringValue(SharedPref.USER, SharedPref.CONTACT_NO, context)
                                .equals(tv_contact.getText().toString())) {
                    btn_update.setClickable(false);
                    btn_update.setBackgroundColor(ContextCompat.getColor(context, R.color.gray));

                } else {
                    btn_update.setClickable(true);
                    btn_update.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimaryLight));

                }

            }
        });


        showCoordData.show();


        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.70);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(showCoordData.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = width;
        showCoordData.getWindow().setAttributes(lp);

    }


    public void testMonthForDaysCount(int getMonth, int getYear, ArrayAdapter<String> DateList) {

        int actualDays = 0;
        Calendar calMonth = new GregorianCalendar(getYear, getMonth, 1);
        actualDays = calMonth.getActualMaximum(Calendar.DAY_OF_MONTH);
        Log.d("YEAR", getYear + " ");
        Log.d("MONTh", getMonth + " ");
        Log.d("DAYS", actualDays + " ");

        DateList.clear();
        DateList.addAll(CreateList.getDayList(actualDays));
        DateList.notifyDataSetChanged();

    }


    private void setData(int position, View view) {


        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        if (position == 0) {
            imageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.account_w));
        } else if (position == 1) {
            imageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.pending_w));
        } else if (position == 2) {
            imageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.availed_w));
        } else if (position == 3) {
            imageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.patient_w));
        } else if (position == 4) {
            imageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.claims_w));
        } else if (position == 5) {
            imageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.settings_w));
        } else if (position == 6) {
            imageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.logout_w));
        }

    }


    public void setDecor(int i, List<ImageView> arrayImage, List<RelativeLayout> arrayR1,
                         List<TextView> arrayTextView) {

        arrayImage.get(0).setImageDrawable(ContextCompat.getDrawable(context, R.drawable.account));
        arrayImage.get(1).setImageDrawable(ContextCompat.getDrawable(context, R.drawable.pending));
        arrayImage.get(2).setImageDrawable(ContextCompat.getDrawable(context, R.drawable.availed));
        arrayImage.get(3).setImageDrawable(ContextCompat.getDrawable(context, R.drawable.patient));
        arrayImage.get(4).setImageDrawable(ContextCompat.getDrawable(context, R.drawable.claims));
        arrayImage.get(5).setImageDrawable(ContextCompat.getDrawable(context, R.drawable.settings));
        arrayImage.get(6).setImageDrawable(ContextCompat.getDrawable(context, R.drawable.logout_plain));

        for (int x = 0; x < 7; x++) {
            arrayR1.get(x).setBackgroundColor(ContextCompat.getColor(context, R.color.WHITE_));
            arrayTextView.get(x).setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));

        }


        arrayR1.get(i).setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimaryLight));
        arrayTextView.get(i).setTextColor(ContextCompat.getColor(context, R.color.WHITE_));
    }


    public void showLogOut(final MemberSearchCallback callback, final List<RelativeLayout> arrayR1, final List<ImageView> arrayImage, final List<TextView> arrayTextView) {


        AlertDialog logOut = new AlertDialog.Builder(context)
                .setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        callback.logOut();


                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        arrayImage.get(6).setImageDrawable(ContextCompat.getDrawable(context, R.drawable.logout_plain));
                        arrayR1.get(6).setBackgroundColor(ContextCompat.getColor(context, R.color.WHITE_));
                        arrayTextView.get(6).setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                    }
                })
                .show();

        logOut.setCancelable(false);
    }

//
//    public void gotoCoordinatorLayout(VerifyMember memberInfo, MemberSearchCallback callback, String birthday) {
//
//        boolean isInPatient = false;
//        try {
//            isInPatient = memberInfo.isAdmittedIp();
//        } catch (Exception e) {
//            isInPatient = false;
//        }
//
//
//        String inPatientCode;
//        try {
//            inPatientCode = memberInfo.getInpatientRequestCode();
//        } catch (Exception error) {
//            Log.e("SSEARCH", error.getMessage());
//            inPatientCode = "";
//        }
//
//        //used to view loa in consultation
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.GENDER, memberInfo.getMemberInfo().getMEM_SEX(), context);
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.AGE, memberInfo.getMemberInfo().getAGE(), context);
//
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.SEARCHTYPE, "search", context);
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.HAS_MATERNITY, memberInfo.getHasMaternity(), context);
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.EFFECTIVE, memberInfo.getMemberInfo().getEFF_DATE(), context);
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.VALID, memberInfo.getMemberInfo().getVAL_DATE(), context);
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.REMARKS, RemarksFilter.filterRemarks(memberInfo.getMemberInfo().getID_REM(),
//                memberInfo.getMemberInfo().getID_REM2(),
//                memberInfo.getMemberInfo().getID_REM3(),
//                memberInfo.getMemberInfo().getID_REM4(),
//                memberInfo.getMemberInfo().getID_REM5(),
//                memberInfo.getMemberInfo().getID_REM6(),
//                memberInfo.getMemberInfo().getID_REM7()), context);
//
//
//        Intent gotoMenu = new Intent(context, CoordinatorMenuActivity.class);
//
//        gotoMenu.putExtra(SearchToCoordinatorMenu.NAME, memberInfo.getMemberInfo().getMEM_NAME());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.ID, memberInfo.getMemberInfo().getPRIN_CODE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.STATUS, memberInfo.getMemberInfo().getMem_OStat_Code());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.MEMCODE, memberInfo.getMemberInfo().getACCOUNT_CODE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.TYPE, memberInfo.getMemberInfo().getMEM_TYPE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.EFFECTIVE, memberInfo.getMemberInfo().getEFF_DATE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.VALID, memberInfo.getMemberInfo().getVAL_DATE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.DD_LIMIT, memberInfo.getMemberInfo().getDD_Reg_Limits());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.PLAN, memberInfo.getMemberInfo().getPlan_Desc());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.COMPANY, memberInfo.getMemberInfo().getACCOUNT_NAME());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.AGE, memberInfo.getMemberInfo().getAGE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.GENDER, memberInfo.getMemberInfo().getMEM_SEX());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.BDAY, memberInfo.getMemberInfo().getBDAY());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.VALIDITYDATE, memberInfo.getMemberInfo().getVAL_DATE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.EFFECTIVITYDATE, memberInfo.getMemberInfo().getEFF_DATE());
//        gotoMenu.putExtra(SearchToCoordinatorMenu.BDAY_RAW, birthday);
//        gotoMenu.putExtra(SearchToCoordinatorMenu.REQUEST_CODE_IN_PATIENT, inPatientCode);
//        gotoMenu.putExtra(SearchToCoordinatorMenu.IS_IN_PATIENT, isInPatient);
//
//        gotoMenu.putExtra(SearchToCoordinatorMenu.OTHER, RemarksFilter.filterRemarks
//                (memberInfo.getMemberInfo().getID_REM(),
//                        memberInfo.getMemberInfo().getID_REM2(),
//                        memberInfo.getMemberInfo().getID_REM3(),
//                        memberInfo.getMemberInfo().getID_REM4(),
//                        memberInfo.getMemberInfo().getID_REM5(),
//                        memberInfo.getMemberInfo().getID_REM6(),
//                        memberInfo.getMemberInfo().getID_REM7()).trim());
//
//        ArrayList<Utilization> utils = new ArrayList<>();
//        utils.addAll(memberInfo.getUtilization());
//        gotoMenu.putParcelableArrayListExtra(SearchToCoordinatorMenu.UTILIZATION, utils);
//
//        ArrayList<Dependents> dependents = new ArrayList<>();
//        dependents.addAll(memberInfo.getDependents());
//        gotoMenu.putParcelableArrayListExtra(SearchToCoordinatorMenu.DEPENDENTS, dependents);
//
//        new SetAvailableServices().setServiceType(memberInfo.getServiceType(), context);
//        callback.navigateActivity(gotoMenu);
//    }

//
//    public void gotoMemberVerification(VerifyMember memberInfo, MemberSearchCallback callback, String DateofBirth) {
//
//        boolean isInPatient = false;
//        try {
//            isInPatient = memberInfo.isAdmittedIp();
//        } catch (Exception e) {
//            isInPatient = false;
//        }
//
//
//        String inPatientCode;
//        try {
//            inPatientCode = memberInfo.getInpatientRequestCode();
//        } catch (Exception error) {
//            inPatientCode = "";
//        }
//
//        //used to view loa in consultation
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.GENDER, memberInfo.getMemberInfo().getMEM_SEX(), context);
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.AGE, memberInfo.getMemberInfo().getAGE(), context);
//
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.SEARCHTYPE, "search", context);
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.ID, memberInfo.getMemberInfo().getPRIN_CODE(), context);
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.HAS_MATERNITY, memberInfo.getHasMaternity(), context);
//
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.EFFECTIVE, memberInfo.getMemberInfo().getEFF_DATE(), context);
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.VALID, memberInfo.getMemberInfo().getVAL_DATE(), context);
//        SharedPref.setStringValue(SharedPref.USER, SharedPref.REMARKS, RemarksFilter.filterRemarks(memberInfo.getMemberInfo().getID_REM(),
//                memberInfo.getMemberInfo().getID_REM2(),
//                memberInfo.getMemberInfo().getID_REM3(),
//                memberInfo.getMemberInfo().getID_REM4(),
//                memberInfo.getMemberInfo().getID_REM5(),
//                memberInfo.getMemberInfo().getID_REM6(),
//                memberInfo.getMemberInfo().getID_REM7()), context);
//
//
//        Intent gotoValidate = new Intent(context, MemberVerificationAcitivty.class);
//        gotoValidate.putExtra(SearchToCoordinatorMenu.NAME, memberInfo.getMemberInfo().getMEM_NAME());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.COMPANY, memberInfo.getMemberInfo().getACCOUNT_NAME());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.AGE, memberInfo.getMemberInfo().getAGE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.GENDER, memberInfo.getMemberInfo().getMEM_SEX());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.ID, memberInfo.getMemberInfo().getPRIN_CODE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.STATUS, memberInfo.getMemberInfo().getMem_OStat_Code());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.USERNAME, memberInfo.getVerifiedMember().getAPP_USERNAME());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.ISKIPPED, memberInfo.getVerifiedMember().getIS_SKIPPED());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.NAME, memberInfo.getMemberInfo().getMEM_NAME());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.ID, memberInfo.getMemberInfo().getPRIN_CODE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.STATUS, memberInfo.getMemberInfo().getMem_OStat_Code());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.MEMCODE, memberInfo.getMemberInfo().getACCOUNT_CODE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.TYPE, memberInfo.getMemberInfo().getMEM_TYPE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.EFFECTIVE, memberInfo.getMemberInfo().getEFF_DATE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.VALID, memberInfo.getMemberInfo().getVAL_DATE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.DD_LIMIT, memberInfo.getMemberInfo().getDD_Reg_Limits());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.PLAN, memberInfo.getMemberInfo().getPlan_Desc());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.BDAY, memberInfo.getMemberInfo().getBDAY());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.BDAY_RAW, DateofBirth);
//        gotoValidate.putExtra(SearchToCoordinatorMenu.VALIDITYDATE, memberInfo.getMemberInfo().getVAL_DATE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.EFFECTIVITYDATE, memberInfo.getMemberInfo().getEFF_DATE());
//        gotoValidate.putExtra(SearchToCoordinatorMenu.REQUEST_CODE_IN_PATIENT, inPatientCode);
//        gotoValidate.putExtra(SearchToCoordinatorMenu.IS_IN_PATIENT, isInPatient);
//        gotoValidate.putExtra(SearchToCoordinatorMenu.OTHER, RemarksFilter.filterRemarks(memberInfo.getMemberInfo().getID_REM(),
//                memberInfo.getMemberInfo().getID_REM2(),
//                memberInfo.getMemberInfo().getID_REM3(),
//                memberInfo.getMemberInfo().getID_REM4(),
//                memberInfo.getMemberInfo().getID_REM5(),
//                memberInfo.getMemberInfo().getID_REM6(),
//                memberInfo.getMemberInfo().getID_REM7()).trim());
//
//
//        ArrayList<Utilization> utils = new ArrayList<>();
//        utils.addAll(memberInfo.getUtilization());
//        gotoValidate.putParcelableArrayListExtra(SearchToCoordinatorMenu.UTILIZATION, utils);
//
//        ArrayList<Dependents> dependents = new ArrayList<>();
//        dependents.addAll(memberInfo.getDependents());
//        gotoValidate.putParcelableArrayListExtra(SearchToCoordinatorMenu.DEPENDENTS, dependents);
//
//
//        new SetAvailableServices().setServiceType(memberInfo.getServiceType(), context);
//        callback.navigateActivity(gotoValidate);
//
//
//    }

    public void updateSearchBarSearching(LinearLayout btn_search, TextView tv_search, ProgressBar pb_search, boolean b) {
        if (b) {
            btn_search.setBackgroundColor(ContextCompat.getColor(context, R.color.gray));
            tv_search.setText("Searching...");
            pb_search.setVisibility(View.VISIBLE);

        } else {

            btn_search.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
            tv_search.setText("Search");
            pb_search.setVisibility(View.GONE);
        }

    }
}
