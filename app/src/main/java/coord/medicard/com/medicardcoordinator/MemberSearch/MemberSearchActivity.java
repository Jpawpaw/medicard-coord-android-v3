package coord.medicard.com.medicardcoordinator.MemberSearch;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.targets.ViewTarget;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapter.RecentTransacAdapter;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import coord.medicard.com.medicardcoordinator.R;
import coord.medicard.com.medicardcoordinator.SignIn.SignInActivity;
import model.ExclusionModel;
import model.RecentTransactions;
import model.UpdatedInfo;
import model.VerifyMember;
import services.AppInterface;
import utilities.APIMultipleCalls.GetUserData;
import utilities.AlertDialogCustom;
import utilities.Constant;
import utilities.CreateList;
import utilities.DataBaseHandler;
import utilities.DatepickerSet;
import utilities.ErrorMessage;
import utilities.APIMultipleCalls.NetworkTest;
import utilities.Permission;
import utilities.SetAvailableServices;
import utilities.SharedPref;
import utilities.Showcase;
import utilities.SnackBarSet;

public class MemberSearchActivity extends AppCompatActivity implements View.OnClickListener, MemberSearchCallback
        , Showcase.ShowCaseCallBack, GetUserData.GetUserCallback, NavigateSearchData.NavigateToActivity {

    Spinner spn_year, spn_month, spn_day;
    private int getYear = 0;
    private int getDay = 1;
    private int getMonth = 0;
    private int actualDays = 31;
    boolean isSearching = false;
    ArrayAdapter<String> DateList, MonthList, YearList;
    Switch swicth_keyboard;

    //User for disabling special characters
    String specialChars = "@/*!##$%^&*()\"{}_[]|\\?/<>,.:-'';§£¥...€₩`•¤《《》》¡¿";


    @BindView(R.id.ed_medID)
    EditText ed_medID;
    @BindView(R.id.tv_hospitalName)
    TextView tv_hospitalName;
    @BindViews({R.id.imageView, R.id.imageView1, R.id.imageView2, R.id.imageView3,
            R.id.imageView4, R.id.imageView5, R.id.imageView6, R.id.imageView7})
    List<ImageView> arrayImage;
    @BindViews({R.id.r1, R.id.r2, R.id.r3, R.id.r4, R.id.r5, R.id.r6, R.id.r7})
    List<RelativeLayout> arrayR1;
    @BindViews({R.id.textView, R.id.textView1, R.id.textView2, R.id.textView3, R.id.textView4,
            R.id.textView5, R.id.textView6})
    List<TextView> arrayTextView;

    @BindView(R.id.btn_search)
    LinearLayout btn_search;
    @BindView(R.id.pb_search)
    ProgressBar pb_search;
    @BindView(R.id.tv_search)
    TextView tv_search;
    @BindView(R.id.sr_swipe)
    SwipeRefreshLayout sr_swipe;

    @BindView(R.id.cv_coord_recent)
    CardView cv_coord_recent;
    @BindView(R.id.connectedTo)
    TextView connectedTo;
    @BindView(R.id.el_list)
    ExpandableListView el_list;
    @BindView(R.id.ed_searchID)
    EditText ed_searchID;
    @BindView(R.id.btn_search_medID)
    CardView btn_search_medID;


    ImageView btn_scanQr;
    Context context;
    CoordinatorLayout coords;
    SnackBarSet snackBarSet;
    DatepickerSet datepickerSet;
    //    ArrayList<Dependents> arrayDependent = new ArrayList<>();


    AlertDialogCustom alertDialogCustom = new AlertDialogCustom();
    ProgressDialog pd;


    String DateofBirth = "";
    private final String VERIFIED = "Member is valid";
    private final String NOT_VERIFY = "Member is not verified";


    int searchMemberCtr = 0;
    String saveMemberIdInput = "";
    DataBaseHandler dataBaseHandler;
    MemberSearchImplement implement;
    MemberSearchCallback callback;
    Showcase.ShowCaseCallBack caseCallBack;
    GetUserData.GetUserCallback getUserCallback;
    NavigateSearchData.NavigateToActivity navigateToActivityCallback;
    RecentTransacAdapter recentTransacAdapter;
    List<String> listDataHeader;
    List<String> listDataCounter;
    HashMap<String, List<RecentTransactions>> listRecentTrans;
    List<RecentTransactions> approvedRecentTrans;
    List<RecentTransactions> pendingRecentTrans;
    List<RecentTransactions> disapprovedRecentTrans;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context = this;
        dataBaseHandler = new DataBaseHandler(context);
        implement = new MemberSearchImplement(context);
        listDataHeader = new ArrayList<String>();
        listDataCounter = new ArrayList<>();
        listRecentTrans = new HashMap<String, List<RecentTransactions>>();
        approvedRecentTrans = new ArrayList<>();
        pendingRecentTrans = new ArrayList<>();
        disapprovedRecentTrans = new ArrayList<>();
        callback = this;
        caseCallBack = this;
        getUserCallback = this;
        navigateToActivityCallback = this;
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        implement.testDatabase(dataBaseHandler, callback);
        pd = new ProgressDialog(MemberSearchActivity.this, R.style.MyTheme);
        pd.setCancelable(false);
        pd.setMessage("Authenticating...");
        pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

        spn_year = (Spinner) findViewById(R.id.spn_year);
        spn_month = (Spinner) findViewById(R.id.spn_month);
        spn_day = (Spinner) findViewById(R.id.spn_day);

        swicth_keyboard = (Switch) findViewById(R.id.switch_keyboard);


        btn_scanQr = (ImageView) findViewById(R.id.btn_scanQr);

        try {
            ed_medID.setText(getIntent().getExtras().getString("MEMBER_CODE"));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        coords = (CoordinatorLayout) findViewById(R.id.coords);

        tv_hospitalName.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALNAME, context));

        btn_scanQr.setOnClickListener(this);
        btn_search.setOnClickListener(this);
        swicth_keyboard.setOnClickListener(this);
        btn_search_medID.setOnClickListener(this);

        for (int x = 0; x < arrayR1.size(); x++) {
            arrayR1.get(x).setOnClickListener(this);
        }

        datepickerSet = new DatepickerSet();
        snackBarSet = new SnackBarSet();

        YearList = new ArrayAdapter<String>(this,
                R.layout.support_simple_spinner_dropdown_item, CreateList.getYearList());
        YearList.setDropDownViewResource(R.layout.row_spinner);

        MonthList = new ArrayAdapter<String>(this,
                R.layout.support_simple_spinner_dropdown_item, CreateList.getMonthList());
        MonthList.setDropDownViewResource(R.layout.row_spinner);

        DateList = new ArrayAdapter<String>(this,
                R.layout.support_simple_spinner_dropdown_item, CreateList.getDayList(actualDays));
        DateList.setDropDownViewResource(R.layout.row_spinner);

        spn_year.setAdapter(YearList);
        spn_month.setAdapter(MonthList);
        spn_day.setAdapter(DateList);


        spn_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedYear = spn_year.getSelectedItem().toString().trim();
                Log.d("DAYS", selectedYear + " ");
                getYear = Integer.parseInt(selectedYear);
                implement.testMonthForDaysCount(getMonth, getYear, DateList);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        spn_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getMonth = spn_month.getSelectedItemPosition();
                implement.testMonthForDaysCount(getMonth, getYear, DateList);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        spn_day.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getDay = Integer.parseInt(spn_day.getSelectedItem().toString().trim());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        swicth_keyboard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position

                if (isChecked) {
                    ed_medID.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                    ed_medID.setFilters(new InputFilter[]{filter});
                    ed_searchID.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                    ed_searchID.setFilters(new InputFilter[]{filter});
                } else
                    ed_medID.setInputType(InputType.TYPE_CLASS_NUMBER);
                ed_searchID.setInputType(InputType.TYPE_CLASS_NUMBER);
            }
        });


        isSearching = false;
        implement.updateSearchBarSearching(btn_search, tv_search, pb_search, isSearching);


        if (!SharedPref.getStringValue(SharedPref.USER, SharedPref.MEMBER_SEARCH_TUTS, context).equals(Constant.DONE)) {
            Showcase.showCase(new ViewTarget(R.id.btn_scanQr, this), getString(R.string.qrcode_scanner_title),
                    getString(R.string.qrcode_scanner_msg), this, caseCallBack, Constant.FROM_QR_SEARCH);
        }

        coordRecentTrans("");

        sr_swipe.setColorSchemeResources(R.color.colorPrimary);
        sr_swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ed_searchID.setText("");
                updateRecentTransactions();
            }
        });
    }

    public void updateRecentTransactions() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (NetworkTest.isOnline(context)) {
                    coordRecentTrans("");
                    sr_swipe.setRefreshing(false);
                } else {
                    alertDialogCustom.showMe(context, alertDialogCustom.NO_Internet_title, alertDialogCustom.NO_Internet, 1);
                    sr_swipe.setRefreshing(false);
                }
            }
        }, 3000);
    }


    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int index = start; index < end; index++) {

                int type = Character.getType(source.charAt(index));

                if (type == Character.SURROGATE || type == Character.OTHER_SYMBOL || type == Character.MATH_SYMBOL || specialChars.contains("" + source)) {
                    return "";
                }
            }
            return null;
        }
    };


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_scanQr:

                if (NetworkTest.isOnline(context)) {
                    if (Permission.checkPermissionCamera(context)) {
                        ed_medID.setText("");
                        startActivity(new Intent(MemberSearchActivity.this, ScanQrActivity.class));
                    }
                } else {
                    ed_medID.setText("");
                    alertDialogCustom.showMe(context, alertDialogCustom.NO_Internet_title, alertDialogCustom.NO_Internet, 1);
                }

                break;
            case R.id.btn_search:
                if (!isSearching) {
                    if (NetworkTest.isOnline(context)) {
                        if (ed_medID.getText().toString().trim().equals("")) {
                            alertDialogCustom.showMe(context, "", snackBarSet.INCOMPLETE, 1);
                        } else {
                            pd.show();
                            pd.setMessage("Searching...");

                            isSearching = true;
                            implement.updateSearchBarSearching(btn_search, tv_search, pb_search, isSearching);
                            Log.d("BIRTHDAY", implement.CollectBirthdate(getYear, getMonth, getDay));
//                            MemberSearchAPiCall.verifyMember(callback,
//                                    ed_medID.getText().toString().trim(),
//                                    implement.CollectBirthdate(getYear, getMonth, getDay),
//                                    SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context),
//                                    SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, context),
//                                    SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context));

                            GetUserData.getUserData(ed_medID.getText().toString().trim(),
                                    implement.CollectBirthdate(getYear, getMonth, getDay),
                                    SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context),
                                    SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, context),
                                    SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context),
                                    getUserCallback,
                                    GetUserData.SEARCH,
                                    pd
                            );

                            //isSearching set to false to enable btn_search
                            isSearching = false;

                            //implement update the searchBar
                            implement.updateSearchBarSearching(btn_search, tv_search, pb_search, isSearching);
                        }

                    } else {
                        ed_medID.setText("");
                        alertDialogCustom.showMe(context, alertDialogCustom.NO_Internet_title, alertDialogCustom.NO_Internet, 1);
                    }
                }

                break;
            case R.id.r1:
                implement.showCoordinatorDetails(context, callback);
                implement.setDecor(0, arrayImage, arrayR1, arrayTextView);
                arrayImage.get(0).setImageDrawable(ContextCompat.getDrawable(context, R.drawable.account_w));
                break;
            case R.id.r2:
                implement.setDecor(1, arrayImage, arrayR1, arrayTextView);
                arrayImage.get(1).setImageDrawable(ContextCompat.getDrawable(context, R.drawable.pending_w));
                break;
            case R.id.r3:
                implement.setDecor(2, arrayImage, arrayR1, arrayTextView);
                arrayImage.get(2).setImageDrawable(ContextCompat.getDrawable(context, R.drawable.availed_w));
                break;
            case R.id.r4:
                implement.setDecor(3, arrayImage, arrayR1, arrayTextView);
                arrayImage.get(3).setImageDrawable(ContextCompat.getDrawable(context, R.drawable.patient_w));

                break;
            case R.id.r5:
                implement.setDecor(4, arrayImage, arrayR1, arrayTextView);
                arrayImage.get(4).setImageDrawable(ContextCompat.getDrawable(context, R.drawable.claims_w));

                break;
            case R.id.r6:
                implement.setDecor(5, arrayImage, arrayR1, arrayTextView);
                arrayImage.get(5).setImageDrawable(ContextCompat.getDrawable(context, R.drawable.settings_w));

                break;
            case R.id.r7:
                implement.setDecor(6, arrayImage, arrayR1, arrayTextView);
                arrayImage.get(6).setImageDrawable(ContextCompat.getDrawable(context, R.drawable.logout_w));
                implement.showLogOut(callback, arrayR1, arrayImage, arrayTextView);
                break;
            case R.id.btn_search_medID:
                if (ed_searchID.getText().toString().isEmpty()) {
                    snackBarSet.setSnackBar(coords, snackBarSet.NO_MEMCODE);
                } else {
                    if (NetworkTest.isOnline(context)) {
                        coordRecentTrans(ed_searchID.getText().toString());
                    } else {
                        alertDialogCustom.showMe(context, alertDialogCustom.NO_Internet_title, alertDialogCustom.NO_Internet, 1);
                    }
                }
                break;

        }


    }

    @Override
    public void logOut() {

        SharedPref.setStringValue(SharedPref.USER, SharedPref.masterUSERNAME, "", context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.masterPASSWORD, "", context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, "", context);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.HOSPITALNAME, "", context);

        startActivity(new Intent(MemberSearchActivity.this, SignInActivity.class));
        finish();

    }

    @Override
    public void onErrorSearchMember(String message) {
        if (message != null) {
            alertDialogCustom.showMe(context, "", ErrorMessage.setErrorMessage(message), 1);
        } else {
            alertDialogCustom.showMe(context, alertDialogCustom.not_title, alertDialogCustom.NO_Internet, 1);
        }

        searchMemberCtr = 0;
        saveMemberIdInput = "";

        isSearching = false;
        implement.updateSearchBarSearching(btn_search, tv_search, pb_search, isSearching);

        //   pd.dismiss();
    }


    @Override
    public void onErrorUpdateRecentTrans(String message) {
        if (message != null) {
            alertDialogCustom.showMe(context, "", ErrorMessage.setErrorMessage(message), 1);
        } else {
            alertDialogCustom.showMe(context, alertDialogCustom.not_title, alertDialogCustom.NO_Internet, 1);
        }
    }

    @Override
    public void onCloseDiagInfo() {
        arrayImage.get(0).setImageDrawable(ContextCompat.getDrawable(context, R.drawable.account));
        arrayR1.get(0).setBackgroundColor(ContextCompat.getColor(context, R.color.WHITE_));
        arrayTextView.get(0).setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
    }

    @Override
    public void onUpdateDiagInfo(String contact, Dialog showCoordData) {
        pd.show();
        pd.setMessage("Updating...");
        MemberSearchAPiCall.updateDataContact(contact, context, callback, showCoordData);
    }

    @Override
    public void onUpdateContactSuccess(UpdatedInfo body, String s, Dialog showCoordData) {


        if (body.getResponseCode().equals("200")) {
            SharedPref.setStringValue(SharedPref.USER, SharedPref.CONTACT_NO, s, context);
            showCoordData.dismiss();
            implement.showCoordinatorDetails(context, callback);
        } else {
            alertDialogCustom.showMe(context, "", body.getResponseDesc(), 1);
        }
        pd.dismiss();
    }

    @Override
    public void onUpdateContactError(String message, Dialog showCoordData) {
        alertDialogCustom.showMe(context, "", ErrorMessage.setErrorMessage(message), 1);
        showCoordData.dismiss();
        implement.showCoordinatorDetails(context, callback);
        Log.d("CONTACT", message);
        pd.dismiss();
    }

    @Override
    public void onSuccessLockAccount(String memberCode) {

        Log.d("CTR_TO_LOCK", searchMemberCtr + "");


        Log.d("CTR_TO_LOCK", saveMemberIdInput + "");
        Log.d("CTR_TO_LOCK", memberCode + "");
        Log.d("CTR_TO_LOCK", "3");

        searchMemberCtr = 0;
        saveMemberIdInput = "";
        alertDialogCustom.showMe(context, alertDialogCustom.unknown, alertDialogCustom.member_is_locked, 1);

    }


    @Override
    public void onErrorLockAccount(String message) {
        alertDialogCustom.showMe(context, alertDialogCustom.unknown, ErrorMessage.setErrorMessage(message), 1);
    }

    @Override
    public void navigateActivity(Intent gotoMenu) {
        startActivity(gotoMenu);
    }

    @Override
    public void testToLockAccount(String memberCode) {

        //MEMBER ID DIDNT MATCH
        if (searchMemberCtr == 0) {
            saveMemberIdInput = memberCode;
            if (!saveMemberIdInput.equals(memberCode)) {
                alertDialogCustom.showMe(context, alertDialogCustom.unknown, alertDialogCustom.warning_on_date_and_member_ID_3, 1);
                searchMemberCtr = 0;
                saveMemberIdInput = "";
                Log.d("CTR_TO_LOCK", searchMemberCtr + "");
                Log.d("CTR_TO_LOCK", saveMemberIdInput + "");
                Log.d("CTR_TO_LOCK", memberCode + "");
            } else {
                searchMemberCtr += 1;
                alertDialogCustom.showMe(context, alertDialogCustom.unknown, alertDialogCustom.warning_on_date_and_member_ID_3, 1);
                Log.d("CTR_TO_LOCK", searchMemberCtr + "");
                Log.d("CTR_TO_LOCK", saveMemberIdInput + "");
                Log.d("CTR_TO_LOCK", memberCode + "");
            }

        } else if (searchMemberCtr == 1) {
            if (!saveMemberIdInput.equals(memberCode)) {
                alertDialogCustom.showMe(context, alertDialogCustom.unknown, alertDialogCustom.warning_on_date_and_member_ID_2, 1);
                searchMemberCtr = 0;
                saveMemberIdInput = "";
                Log.d("CTR_TO_LOCK", searchMemberCtr + "");
                Log.d("CTR_TO_LOCK", saveMemberIdInput + "");
                Log.d("CTR_TO_LOCK", memberCode + "");
            } else {
                searchMemberCtr += 1;
                alertDialogCustom.showMe(context, alertDialogCustom.unknown, alertDialogCustom.warning_on_date_and_member_ID_2, 1);
                Log.d("CTR_TO_LOCK", searchMemberCtr + "");
                Log.d("CTR_TO_LOCK", saveMemberIdInput + "");
                Log.d("CTR_TO_LOCK", memberCode + "");
            }

        } else if (searchMemberCtr == 2) {
            if (!saveMemberIdInput.equals(memberCode)) {
                alertDialogCustom.showMe(context, alertDialogCustom.unknown, alertDialogCustom.warning_on_date_and_member_ID_1, 1);
                searchMemberCtr = 0;
                saveMemberIdInput = "";


                Log.d("CTR_TO_LOCK", searchMemberCtr + "");


                Log.d("CTR_TO_LOCK", saveMemberIdInput + "");
                Log.d("CTR_TO_LOCK", memberCode + "");
            } else {
                searchMemberCtr += 1;
                alertDialogCustom.showMe(context, alertDialogCustom.unknown, alertDialogCustom.warning_on_date_and_member_ID_1, 1);
                Log.d("CTR_TO_LOCK", searchMemberCtr + "");


                Log.d("CTR_TO_LOCK", saveMemberIdInput + "");
                Log.d("CTR_TO_LOCK", memberCode + "");


            }
        } else {
            MemberSearchAPiCall.sendLockAccount(memberCode, callback);
        }


    }

    @Override
    public void memberIsLocked() {
        alertDialogCustom.showMe(context, alertDialogCustom.unknown, alertDialogCustom.not_eligible, 1);
    }

    @Override
    public void memberNoAccessToHospital() {
        alertDialogCustom.showMe(context, alertDialogCustom.hold_it, getString(R.string.no_access_to_hospital), 1);
    }


    @Override
    public void onBackPressed() {

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {

        Log.d("MemberSearch", event.getMessage());
        DateofBirth = event.getMessage();

    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);

    }

    @Override
    public void onClickedShowCaseClose(String ORIGIN) {
        switch (ORIGIN) {
            case Constant.FROM_QR_SEARCH:

                Showcase.showCase(new ViewTarget(R.id.btn_search, this), getString(R.string.search_title),
                        getString(R.string.search_msg), this, caseCallBack, Constant.FROM_MANUAL_SEARCH);

                break;
            case Constant.FROM_MANUAL_SEARCH:
                SharedPref.setStringValue(SharedPref.USER, SharedPref.MEMBER_SEARCH_TUTS, Constant.DONE, context);
                break;
        }
    }

    @Override
    public void onSuccessGetUser(VerifyMember body) {
        Log.d("SUCCESS", "SUCCESS");
        pd.dismiss();
        try {
            if (body.getIsLocked().equals("false")) {

                if (body.getResponseCode().equals("200") || body.getResponseCode().equals("230")) {

                    ed_medID.setText("");
                    searchMemberCtr = 0;
                    saveMemberIdInput = "";

                    new SetAvailableServices().setServiceType(body.getServiceType(), context);
                    String consultation, isInPatient, otherTest, isEr;

                    consultation = SharedPref.getStringValue(SharedPref.USER, SharedPref.Consultation, context);
                    isInPatient = SharedPref.getStringValue(SharedPref.USER, SharedPref.In_Patient, context);
                    otherTest = SharedPref.getStringValue(SharedPref.USER, SharedPref.OtherTest, context);
                    isEr = SharedPref.getStringValue(SharedPref.USER, SharedPref.Er, context);

                    System.out.println("consultation " + consultation);
                    System.out.println("isInPatient " + isInPatient);
                    System.out.println("otherTest " + otherTest);
                    System.out.println("isEr " + isEr);


                    if (body.getResponseCode().equals("200")) {
                        if (isInPatient.equals("FALSE") && consultation.equals("FALSE") && otherTest.equals("FALSE") && isEr.equals("FALSE")) {
                            callback.memberNoAccessToHospital();
                        } else {
                            if (body.getResponseDesc().equals(VERIFIED)) {
                                NavigateSearchData.gotoCoordinatorLayout(body, navigateToActivityCallback, implement.CollectBirthdate(getYear, getMonth, getDay), context);
                            } else if (body.getResponseDesc().equals(NOT_VERIFY)) {
                                NavigateSearchData.gotoMemberVerification(context, body, navigateToActivityCallback, implement.CollectBirthdate(getYear, getMonth, getDay));
                            }

                        }


                    } else if (body.getResponseCode().equals("230")) {

                        if (body.getMemberInfo().getMem_OStat_Code().equals("ACTIVE")) {
                            if (isInPatient.equals("FALSE") && consultation.equals("FALSE") && otherTest.equals("FALSE") && isEr.equals("FALSE")) {
                                //NO ACCESS TO HOSPITAL
                                callback.memberNoAccessToHospital();
                            } else {
                                if (body.getResponseDesc().equals(VERIFIED)) {
                                    NavigateSearchData.gotoCoordinatorLayout(body, navigateToActivityCallback, implement.CollectBirthdate(getYear, getMonth, getDay), context);
                                } else if (body.getResponseDesc().equals(NOT_VERIFY)) {
                                    NavigateSearchData.gotoMemberVerification(context, body, navigateToActivityCallback, implement.CollectBirthdate(getYear, getMonth, getDay));
                                }
                            }
                        } else {
                            NavigateSearchData.gotoCoordinatorLayout(body, navigateToActivityCallback, implement.CollectBirthdate(getYear, getMonth, getDay), context);
                        }
                    }

                } else if (body.getResponseCode().equals("210")) {
                    callback.testToLockAccount(ed_medID.getText().toString().trim());
                } else {
                    alertDialogCustom.showMe(context, "", alertDialogCustom.cannot_connect_message, 1);
                }
//edited for unblocking
            } else {
                callback.memberIsLocked();
            }


            isSearching = false;
            implement.updateSearchBarSearching(btn_search, tv_search, pb_search, isSearching);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorGetUser(String message) {
        Log.d("not_SUCCESS", message);

        pd.dismiss();
        if (message != null) {
            System.out.println("In if");
            if (message.contains("End of input")) {
                alertDialogCustom.showMe(context, "", alertDialogCustom.not_message, 1);
            } else {
                alertDialogCustom.showMe(context, "", ErrorMessage.setErrorMessage(message), 1);
            }
        } else {
            System.out.println("In else");
            alertDialogCustom.showMe(context, "", ErrorMessage.setErrorMessage(message), 1);
        }

        searchMemberCtr = 0;
        saveMemberIdInput = "";

        isSearching = false;
        implement.updateSearchBarSearching(btn_search, tv_search, pb_search, isSearching);

    }

    @Override
    public void onSuccessGetExcludedList(ExclusionModel body, VerifyMember verifyMember) {

        GetUserData.updateAllExcludedHospToFalse(body,verifyMember,dataBaseHandler,getUserCallback);
//        dataBaseHandler.updateAllExcludedHospToFalse();
//        dataBaseHandler.updateHospListExcludedList(body);
//        onSuccessGetUser(verifyMember);

    }

    @Override
    public void onActivitySelect(Intent gotoValidate) {
        startActivity(gotoValidate);
        SharedPref.setStringValue(SharedPref.USER, SharedPref.SEARCHTYPE, "search", context);
    }

    public static class MessageEvent {

        private String message;

        public MessageEvent(String s) {
            this.message = s;
        }

        public String getMessage() {
            return message;
        }

    }

    private void coordRecentTrans(String memberCode) {
        prepareListData();
        MemberSearchAPiCall.updateRecentTrans(context, SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context), memberCode, callback);
        recentTransacAdapter = new RecentTransacAdapter(this, listDataHeader, listDataCounter, listRecentTrans);
        recentTransacAdapter.notifyDataSetChanged();
        el_list.setAdapter(recentTransacAdapter);

        if (AppInterface.ENDPOINT.contains("macetestsvr01")) {
            connectedTo.setText("Connected to Test Server 2");
        } else {
            connectedTo.setText("Connected to Stagging");
        }
    }

    private void prepareListData() {
        listDataHeader.clear();
        approvedRecentTrans.clear();
        pendingRecentTrans.clear();
        disapprovedRecentTrans.clear();
        listDataCounter.clear();
        listDataHeader.add("APPROVED");
        listDataHeader.add("DISAPPROVED");
        listDataHeader.add("PENDING");
        listRecentTrans.put(listDataHeader.get(0), approvedRecentTrans); // Header, Child data
        listRecentTrans.put(listDataHeader.get(1), disapprovedRecentTrans);
        listRecentTrans.put(listDataHeader.get(2), pendingRecentTrans);
    }

    @Override
    public void onUpdateRecentTrans(HashMap<String, List<RecentTransactions>> body) {
        Map<String, List<RecentTransactions>> map = body;
        for (Map.Entry<String, List<RecentTransactions>> perItem : map.entrySet()) {
            Log.e("Entry", perItem.getKey().toString() + " " + perItem.getValue().size());
            if (perItem.getKey().equalsIgnoreCase("APPROVED")) {
                approvedRecentTrans.addAll(perItem.getValue());
            } else if (perItem.getKey().equalsIgnoreCase("DISAPPROVED")) {
                disapprovedRecentTrans.addAll(perItem.getValue());
            } else if (perItem.getKey().equalsIgnoreCase("PENDING")) {
                pendingRecentTrans.addAll(perItem.getValue());
            }
            recentTransacAdapter.notifyDataSetChanged();
//            recentTransacAdapter.updateHeaderLabel();
        }

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
}
