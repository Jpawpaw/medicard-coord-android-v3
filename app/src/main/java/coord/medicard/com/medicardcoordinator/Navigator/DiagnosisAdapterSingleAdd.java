package coord.medicard.com.medicardcoordinator.Navigator;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import coord.medicard.com.medicardcoordinator.R;
import fragment.Avail.AvailServices;
import model.DiagnosisList;
import utilities.Constant;
import utilities.DataBaseHandler;
import utilities.SharedPref;

/**
 * Created by mpx-pawpaw on 4/19/17.
 */

public class DiagnosisAdapterSingleAdd extends RecyclerView.Adapter<DiagnosisAdapterSingleAdd.Holder> {

    private Context context;
    private ArrayList<DiagnosisList> arrayList;
    private DataBaseHandler databaseH;
    private NavigatorRetrieve.NavigatorInterface callback;

    public DiagnosisAdapterSingleAdd(NavigatorRetrieve.NavigatorInterface callback, DataBaseHandler databaseH, Context context, ArrayList<DiagnosisList> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        this.databaseH = databaseH;
        this.callback = callback;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(context).inflate(R.layout.row_diag_single, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        holder.name.setText(arrayList.get(position).getDiagDesc());
        holder.position.setText(arrayList.get(position).getIcd10Code());
        holder.cv_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SharedPref.getStringValue(SharedPref.USER, SharedPref.DIAG_ORIGIN, context)
                        .equals(Constant.ADAPTER_PRIME_DIAG)) {
                    EventBus.getDefault().post(new AvailServices.MessageEvent("9"
                            , arrayList.get(position)));
                    SharedPref.DIAGCODELIST.add(arrayList.get(position).getDiagCode());
                } else if (SharedPref.getStringValue(SharedPref.USER, SharedPref.DIAG_ORIGIN, context)
                        .equals(Constant.ADAPTER_OTHER_DIAG)) {
                    EventBus.getDefault().post(new AvailServices.MessageEvent("11"
                            , arrayList.get(position)));
                } else if (SharedPref.getStringValue(SharedPref.USER, SharedPref.DIAG_ORIGIN, context)
                        .equals(Constant.ADAPTER_OTHER_DIAG_SECOND)) {
                    EventBus.getDefault().post(new AvailServices.MessageEvent("15"
                            , arrayList.get(position)));
                } else if (SharedPref.getStringValue(SharedPref.USER, SharedPref.DIAG_ORIGIN, context)
                        .equals(Constant.ADAPTER_DIAGNOSIS)) {
                    EventBus.getDefault().post(new AvailServices.MessageEvent("6"
                            , arrayList.get(position)));
                } else if (SharedPref.getStringValue(SharedPref.USER, SharedPref.DIAG_ORIGIN, context)
                        .equals(Constant.ADAPTER_DIAG_IN_PATIENT)) {
                    EventBus.getDefault().post(new AvailServices.MessageEvent("9"
                            , arrayList.get(position)));
                }

                callback.onSingleItemSelected(arrayList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextView name;
        @BindView(R.id.cv_item)
        CardView cv_item;
        @BindView(R.id.tv_position)
        TextView position;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
