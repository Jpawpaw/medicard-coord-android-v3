package coord.medicard.com.medicardcoordinator.ApprovalScreen;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import model.DiagnosisClinicProcedures;
import model.Test.DiagnosisProcedures;

/**
 * Created by mpx-pawpaw on 5/24/17.
 */

public class ApprovalRetrieve {


    private Context context;

    public ApprovalRetrieve(Context context) {
        this.context = context;
    }


    /**
     * @param ORIGIN
     * @return DATA WITH SORTED AND JOINED AMOUNTS BASED ON PROCEDURES
     */
    public Collection<? extends DiagnosisProcedures> getProceduresOrder(ArrayList<DiagnosisProcedures> ORIGIN) {

        Collection<? extends DiagnosisProcedures> data = new ArrayList<>();

        String[] getCount = getProcCount(ORIGIN);


        for (int x = 0; x < getCount.length; x++) {

        }

        return data;
    }

    private String[] getProcCount(ArrayList<DiagnosisProcedures> origin_array) {
//
//
             String[] temp = null;
//
//
//        for (int x = 0; x < origin_array.size(); x++) {
//            temp[x] = origin_array.get(x).getApprovalNo();
//        }
//
//
//        Set<String> uniqueWords = new HashSet<String>(Arrays.asList(temp));
//        String[] temp1 = uniqueWords.toArray(new String[uniqueWords.size()]);

        return temp;
    }

    public void setApproveUi(ArrayList<DiagnosisProcedures> procedures, TextView tv_pending
            , RecyclerView rv_approval) {


        if (procedures.size() == 0) {
            tv_pending.setVisibility(View.VISIBLE);
            rv_approval.setVisibility(View.GONE);
        } else {
            tv_pending.setVisibility(View.GONE);
            rv_approval.setVisibility(View.VISIBLE);

        }
    }

    public void setPendingUi(ArrayList<DiagnosisProcedures> procedures, TextView tv_pending_pending
            , RecyclerView rv_approval_pending) {


        if (procedures.size() == 0) {
            tv_pending_pending.setVisibility(View.VISIBLE);
            rv_approval_pending.setVisibility(View.GONE);
        } else {
            tv_pending_pending.setVisibility(View.GONE);
            rv_approval_pending.setVisibility(View.VISIBLE);

        }
    }



    public void setApproveUiProc(ArrayList<DiagnosisClinicProcedures> procedures, TextView tv_pending
            , RecyclerView rv_approval) {


        if (procedures.size() == 0) {
            tv_pending.setVisibility(View.VISIBLE);
            rv_approval.setVisibility(View.GONE);
        } else {
            tv_pending.setVisibility(View.GONE);
            rv_approval.setVisibility(View.VISIBLE);

        }
    }
}
