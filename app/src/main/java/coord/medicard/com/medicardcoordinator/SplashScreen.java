package coord.medicard.com.medicardcoordinator;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import coord.medicard.com.medicardcoordinator.MemberSearch.MemberSearchActivity;
import coord.medicard.com.medicardcoordinator.SignIn.SignInActivity;
import model.AnnouncementDataJson;
import model.LogIn;
import model.SignInDetails;
import utilities.AccessGooglePlay;
import utilities.AlertDialogCustom;
import utilities.ErrorMessage;
import utilities.APIMultipleCalls.NetworkTest;
import utilities.Permission;
import utilities.PhoneInformations;
import utilities.SharedPref;
import utilities.APIMultipleCalls.SignInAPiCall;
import utilities.UpdateCaller;

public class SplashScreen extends AppCompatActivity implements UpdateCaller.DialogUpdateInterface,
        SignInAPiCall.SignInCallback {

    Context context;
    AlertDialogCustom alertDialogCustom;
    UpdateCaller.DialogUpdateInterface callback;
    SignInAPiCall.SignInCallback signInCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        context = this;
        callback = this;
        signInCallback = this;
        alertDialogCustom = new AlertDialogCustom();
        if (SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, this).equals("") ||
                SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, this).equals("null")) {
            startActivity(new Intent(this, SignInActivity.class));
            finish();
        } else {

            if (Permission.checkPermissionPhone(context)) {
                if (NetworkTest.isOnline(context)) {

                    LogIn logIn = new LogIn();
                    logIn.setUsername(SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, this));
                    logIn.setPassword(SharedPref.getStringValue(SharedPref.USER, SharedPref.masterPASSWORD, this));
                    logIn.setDeviceID(PhoneInformations.getIMEI(context));
                    logIn.setVersionNo(BuildConfig.VERSION_NAME);

                    SignInAPiCall.signIn(logIn, signInCallback);


                } else{
                    AlertDialogCustom.showError(alertDialogCustom.NO_Internet_title, context,
                            new AlertDialogCustom.ConfirmationAttachment() {

                                @Override
                                public void onClickOk() {
                                    Intent intent = getIntent();
                                    finish();
                                    startActivity(intent);
                                }

                                @Override
                                public void onClickCancel() {

                                }
                            });
                }
//                    alertDialogCustom.showMe(context, alertDialogCustom.NO_Internet_title, alertDialogCustom.NO_Internet, 1);

            }

        }

    }

        @Override
        public void updateRequired() {
            AccessGooglePlay.openAppInGooglePlay(context);
        }

    @Override
    public void updateNotRequired() {
        startActivity(new Intent(SplashScreen.this, MemberSearchActivity.class));
        finish();
    }

    @Override
    public void onSuccessSignIn(SignInDetails body) {
        SignInDetails test = body;

        /**
         *
         Added
         Code: 270
         Desc: Device is not assigned to AppUser

         Code: 280
         Desc: Optional Update Available

         Code: 290
         Desc: Update Required
         */
        if (test != null) {
            if (test.getResponseCode().equals("200")) {
                startActivity(new Intent(SplashScreen.this, MemberSearchActivity.class));
                finish();
            } else if (test.getResponseCode().equals("270")) {
                AlertDialogCustom.showError("Device is not assigned to AppUser", context,
                        new AlertDialogCustom.ConfirmationAttachment() {

                            @Override
                            public void onClickOk() {
                                Intent intent = getIntent();
                                finish();
                                startActivity(intent);
                            }

                            @Override
                            public void onClickCancel() {

                            }
                        });
            } else if (test.getResponseCode().equals("280")) {
                AlertDialogCustom.showError("Optional Update Available", context,
                        new AlertDialogCustom.ConfirmationAttachment() {

                            @Override
                            public void onClickOk() {
                                Intent intent = getIntent();
                                finish();
                                startActivity(intent);
                            }

                            @Override
                            public void onClickCancel() {

                            }
                        });

              //  UpdateCaller.showUpdateCall(context, "Optional Update Available", false, callback);
            } else if (test.getResponseCode().equals("290")) {
                AlertDialogCustom.showError("Update Required", context,
                        new AlertDialogCustom.ConfirmationAttachment() {

                            @Override
                            public void onClickOk() {
                                Intent intent = getIntent();
                                finish();
                                startActivity(intent);
                            }

                            @Override
                            public void onClickCancel() {

                            }
                        });
               // UpdateCaller.showUpdateCall(context, "Update Required", true, callback);
            } else if (test.getResponseCode().equals("250")) {
                AlertDialogCustom.showError(alertDialogCustom.INVALID_PASS_USERP_title, context,
                        new AlertDialogCustom.ConfirmationAttachment() {

                            @Override
                            public void onClickOk() {
                                Intent intent = getIntent();
                                finish();
                                startActivity(intent);
                            }

                            @Override
                            public void onClickCancel() {

                            }
                        });
                alertDialogCustom.showMe(context, alertDialogCustom.INVALID_PASS_USERP_title, alertDialogCustom.INVALID_PASS_USER, 1);
            } else if (test.getResponseCode().equals("210")) {
                AlertDialogCustom.showError(alertDialogCustom.INVALID_PASS_USERP_title, context,
                        new AlertDialogCustom.ConfirmationAttachment() {

                            @Override
                            public void onClickOk() {
                                Intent intent = getIntent();
                                finish();
                                startActivity(intent);
                            }

                            @Override
                            public void onClickCancel() {

                            }
                        });

               // alertDialogCustom.showMe(context, alertDialogCustom.INVALID_PASS_USERP_title, alertDialogCustom.no_PASS_USER, 1);
            }
        } else {
          //  alertDialogCustom.showMe(context, alertDialogCustom.not_title, ErrorMessage.setErrorMessage(""), 1);
            AlertDialogCustom.showError(ErrorMessage.setErrorMessage(
                    ""), context,
                    new AlertDialogCustom.ConfirmationAttachment() {

                        @Override
                        public void onClickOk() {
                            Intent intent = getIntent();
                            finish();
                            startActivity(intent);
                        }

                        @Override
                        public void onClickCancel() {

                        }
                    });
        }
    }

    @Override
    public void onErrorSignIn(String message) {
        alertDialogCustom.showMe(context, alertDialogCustom.not_title, ErrorMessage.setErrorMessage(message), 1);
    }

    @Override
    public void onNotificationSuccess(AnnouncementDataJson body) {

    }


}
