package coord.medicard.com.medicardcoordinator.CoordinatorMenu;



import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import model.DischargeER;
import model.GetEr;
import model.InPatient;
import model.LoaReq;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import services.AppInterface;
import services.AppService;
import utilities.SharedPref;

/**
 * Created by mpx-pawpaw on 3/3/17.
 */

public class CoordMenuImplement {
    Context context;

    public CoordMenuImplement(Context context) {
        this.context = context;
    }


    public void getLoaReq(final LoaReqListInterface callback, String MEMBERID) {

        Log.d("LOA_HOSP_CODE", SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context));
        Log.d("LOA_MEMID", MEMBERID);

        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.getLoaReq(SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context),
                MEMBERID)
                .enqueue(new Callback<LoaReq>() {
                    @Override
                    public void onResponse(Call<LoaReq> call, Response<LoaReq> response) {
                        System.out.println("onResponse");
                        if (response.body() != null) {
                            callback.onSuccessLoaList(response.body());
                        } else {
                            callback.onErrorLoaList("");
                        }
                    }

                    @Override
                    public void onFailure(Call<LoaReq> call, Throwable t) {
                        System.out.println("onFailure");
                        if (t.getMessage() != null) {
                            callback.onErrorLoaList(t.getMessage());
                        } else {
                            callback.onErrorLoaList("");
                        }
                    }
                });

    }

    public boolean setApprovedStatus(String data) {

        boolean setData = false;
        if (data.equals("VERIFY MEMBERSHIP"))
            setData = true;
        else if (data.equals("VERIFY PAYMENT WITH RMD"))
            setData = true;
        else if (data.equals("VERIFY RENEWAL FROM MARKETING / SALES"))
            setData = true;
        else if (data.equals("ACTIVE"))
            setData = true;
        else if (data.equals("ON HOLD"))
            setData = true;


        return setData;
    }

    public void getInPatientDetails(final String requestCode, final LoaReqListInterface callback) {

        Log.d("DATA_INPATIENT", requestCode);
        AppInterface appService;
        appService = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appService.getInpatientInfo(requestCode)
                .enqueue(new Callback<InPatient>() {
                    @Override
                    public void onResponse(Call<InPatient> call, Response<InPatient> response) {
                        try {

                            InPatient ipu = response.body();
                            callback.onSuccessInPatientFetch(ipu);
                        } catch (Exception e) {
                            callback.onErrorInPatientFetch("");
                        }
                    }

                    @Override
                    public void onFailure(Call<InPatient> call, Throwable t) {
                        try {
                            callback.onErrorInPatientFetch(t.getMessage());
                        } catch (Exception e) {
                            callback.onErrorInPatientFetch("");
                        }
                    }
                });


    }

    public void DischargeER(final String requestCode, final String username, final LoaReqListInterface callback) {

        Log.d("DATA_ER", requestCode);
        Log.d("USER_ER", username);

        DischargeER dischargeER = new DischargeER();
        dischargeER.setId(requestCode);
        dischargeER.setUsername(username);

        Gson gson = new Gson();
        Log.d("DISCHARGE ER", gson.toJson(dischargeER));
        AppInterface appService;
        appService = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appService.sendDischargeEr(requestCode, username)
                .enqueue(new Callback<GetEr>() {
                    @Override
                    public void onResponse(Call<GetEr> call, Response<GetEr> response) {

                        try {
                            if (response.isSuccessful()) {

                                if (response != null) {
                                    callback.dischargeERSuccess();
                                    //
                                } else {
                                    callback.onErrorInPatientFetch("");
                                }
                            }
                        }catch(Exception e){
                            callback.onErrorInPatientFetch("");
                        }

                    }

                    @Override
                    public void onFailure(Call<GetEr> call, Throwable t) {
                        try {
                            if (t != null) {
                                callback.onErrorInPatientFetch(t.getMessage());
                            } else {
                                callback.onErrorInPatientFetch("");

                            }
                        } catch (Exception e) {
                            callback.onErrorInPatientFetch("");
                        }
                    }
                });


    }


    public void DischargeERToInpatient(final String requestCode, final String username, final LoaReqListInterface callback) {

        Log.d("DATA_ER", requestCode);
        Log.d("USER_ER", username);

        DischargeER dischargeER = new DischargeER();
        dischargeER.setId(requestCode);
        dischargeER.setUsername(username);

        Gson gson = new Gson();
        Log.d("DISCHARGE ER", gson.toJson(dischargeER));
        AppInterface appService;
        appService = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appService.sendDischargeErToInpatient(requestCode, username)
                .enqueue(new Callback<GetEr>() {
                    @Override
                    public void onResponse(Call<GetEr> call, Response<GetEr> response) {

                        try {
                            if (response.isSuccessful()) {

                                if (response != null) {
                                    callback.dischargeERToInpatient();
                                    //
                                } else {
                                    callback.onErrorInPatientFetch("");
                                }
                            }
                        }catch(Exception e){
                            callback.onErrorInPatientFetch("");
                        }

                    }

                    @Override
                    public void onFailure(Call<GetEr> call, Throwable t) {
                        try {
                            if (t != null) {
                                callback.onErrorInPatientFetch(t.getMessage());
                            } else {
                                callback.onErrorInPatientFetch("");

                            }
                        } catch (Exception e) {
                            callback.onErrorInPatientFetch("");
                        }
                    }
                });


    }

    public void DischargeERToOutpatient(final String requestCode, final String username, final LoaReqListInterface callback) {

        Log.d("DATA_ER", requestCode);
        Log.d("USER_ER", username);

        DischargeER dischargeER = new DischargeER();
        dischargeER.setId(requestCode);
        dischargeER.setUsername(username);

        Gson gson = new Gson();
        Log.d("DISCHARGE ER", gson.toJson(dischargeER));
        AppInterface appService;
        appService = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appService.sendDischargeErToOutpatient(requestCode, username)
                .enqueue(new Callback<GetEr>() {
                    @Override
                    public void onResponse(Call<GetEr> call, Response<GetEr> response) {

                        try {
                            if (response.isSuccessful()) {

                                if (response != null) {
                                    callback.dischargeERToOutpatient();
                                    //
                                } else {
                                    callback.onErrorInPatientFetch("");
                                }
                            }
                        }catch(Exception e){
                            callback.onErrorInPatientFetch("");
                        }

                    }

                    @Override
                    public void onFailure(Call<GetEr> call, Throwable t) {
                        try {
                            if (t != null) {
                                callback.onErrorInPatientFetch(t.getMessage());
                            } else {
                                callback.onErrorInPatientFetch("");

                            }
                        } catch (Exception e) {
                            callback.onErrorInPatientFetch("");
                        }
                    }
                });


    }



    public void upDateUILoadingInPatient(LinearLayout btn_in_patient, ProgressBar pd_in_patient, boolean b) {
        if (b) {
            pd_in_patient.setVisibility(View.VISIBLE);
        } else {
            pd_in_patient.setVisibility(View.GONE);
        }
    }


    public interface LoaReqListInterface {

        void onSuccessLoaList(LoaReq body);

        void onErrorLoaList(String message);

        void onErrorInPatientFetch(String message);

        void onSuccessInPatientFetch(InPatient inPatient);

        void dischargeERSuccess();

        void dischargeERToInpatient();

        void dischargeERToOutpatient();
    }

}
