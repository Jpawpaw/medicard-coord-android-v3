package coord.medicard.com.medicardcoordinator.CoordinatorMenu;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import adapter.DependentUtilizationAdapter;
import adapter.LoaReqAdapter;
import adapter.UtilsAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import coord.medicard.com.medicardcoordinator.LoaDisplay.LoaDisplayActivity;
import coord.medicard.com.medicardcoordinator.LoaDisplay.LoaListCardRetrieve;
import coord.medicard.com.medicardcoordinator.MemberSearch.MemberSearchActivity;
import coord.medicard.com.medicardcoordinator.MockUpActivity;
import coord.medicard.com.medicardcoordinator.MockUpEndScreenActivity;
import coord.medicard.com.medicardcoordinator.Navigator.NavigatorActivity;
import coord.medicard.com.medicardcoordinator.R;
import de.hdodenhof.circleimageview.CircleImageView;
import fragment.Avail.AvailServices;
import model.Dependents;
import model.GetEr;
import model.InPatient;
import model.LoaList;
import model.LoaReq;
import model.SendEr;
import model.Utilization;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import services.AppInterface;
import services.AppService;
import services.AppServicewithBasicAuth;
import utilities.AlertDialogCustom;
import utilities.Constant;
import utilities.DataBaseHandler;
import utilities.EffectivityAndValidityDateTest;
import utilities.ErrorMessage;
import utilities.ImageConverters;
import utilities.APIMultipleCalls.NetworkTest;
import utilities.Permission;
import utilities.APIMultipleCalls.RequestBlockLogger;
import utilities.SearchToCoordinatorMenu;
import utilities.SharedPref;
import utilities.Showcase;
import utilities.StatusSetter;


public class CoordinatorMenuActivity extends AppCompatActivity implements
        LoaReqAdapter.LoaCLickListener, CoordMenuImplement.LoaReqListInterface, Showcase.ShowCaseCallBack {

    CoordMenuImplement implement;
    CardView cv_btn_container, cv_block;
    AlertDialogCustom alertDialogCustom = new AlertDialogCustom();
    StatusSetter statusSetter = new StatusSetter();

    @BindView(R.id.tv_loa_req_tag)
    TextView tv_loa_req_tag;
    @BindView(R.id.txtInpatient)
    TextView txtInpatient;
    @BindView(R.id.txtER)
    TextView txtER;

    SwipeRefreshLayout sr_swipe;

    ProgressBar progressBar3, pd_in_patient;
    RecyclerView rv_loa;
    LinearLayout ll_loa_req_list;
    LinearLayout ll_dependents_container;
    LinearLayout ll_utilization_container;
    TextView tv_no_loareq, tv_noDependents, tv_noUtils, tv_depedents, tv_utilization, tv_status;
    Button btn_loa;
    LinearLayout btn_in_patient, btn_Er;
    CircleImageView ci_user;
    TextView tv_name, tv_id, tv_company, tv_status_set;
    Context context;
    ImageButton btn_back;
    TextView tv_age, tv_gender, tvInPatientStatus, tvErStatus, tvErStatusTag, tvInPatientStatusStatusTag;
    ProgressDialog pd;
    File output = null;
    ProgressBar progressBar;
    RecyclerView rv_dependents;
    RecyclerView rv_utils;
    final int CAMERA_RQ = 200;
    String MEMBERID = "";
    public String CAMERA = "Camera";
    public String REMOVE_IMAGE = "Remove Image";
    public String Validity_message = "Please Call 841-8080 to verify validity.";
    public String Effectivty_message = "Member Account is not yet in effect.";
    boolean dependentIsOpen = true;
    boolean utilizationIsOpen = true;
    boolean isInPatientLoading = false;

    ArrayList<Dependents> dependentsArrayList = new ArrayList<>();
    ArrayList<LoaListCardRetrieve> loaListCardRetrieve = new ArrayList<>();
    ArrayList<Utilization> utilizationArrayList = new ArrayList<>();
    DependentUtilizationAdapter adapter;
    UtilsAdapter utilsAdapter;

    String consultation, isInPatient, otherTest, isER;
    boolean isinER = false;
    boolean isinIP = false;
    ArrayList<LoaList> loaList = new ArrayList<>();
    LoaReqAdapter loaReqAdapter;
    DataBaseHandler dataBaseHandler;
    LoaReqAdapter.LoaCLickListener callback;
    CoordMenuImplement.LoaReqListInterface listCallback;
    Showcase.ShowCaseCallBack caseCallBack;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coordinator_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        caseCallBack = this;
        ButterKnife.bind(this);


        init();

        if (!SharedPref.getStringValue(SharedPref.USER, SharedPref.COORD_MENU_TUTS, context).equals(Constant.DONE)) {
            Showcase.showCase(new ViewTarget(R.id.ci_edit, this), getString(R.string.edit_image_title)
                    , getString(R.string.edit_image_msg), this, caseCallBack, Constant.FROM_EDIT_IMAGE_ICON);
        }
    }

    private void init() {

        loaList.clear();
        loaListCardRetrieve.clear();
        context = this;
        callback = this;
        listCallback = this;
        implement = new CoordMenuImplement(context);
        dataBaseHandler = new DataBaseHandler(context);
        MEMBERID = SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, context);

        consultation = SharedPref.getStringValue(SharedPref.USER, SharedPref.Consultation, context);
        isInPatient = SharedPref.getStringValue(SharedPref.USER, SharedPref.In_Patient, context);
        otherTest = SharedPref.getStringValue(SharedPref.USER, SharedPref.OtherTest, context);
        isER = SharedPref.getStringValue(SharedPref.USER, SharedPref.Er, context);

        isinIP = SharedPref.getBooleanValue(SharedPref.USER, SharedPref.ISINPATIENT, context);
        isinER = SharedPref.getBooleanValue(SharedPref.USER, SharedPref.ISINER, context);
        System.out.println("HAAHHAHHAHAHHAHAHAHA =================" + isinER);


        adapter = new DependentUtilizationAdapter(context, dependentsArrayList, utilizationArrayList);
        utilsAdapter = new UtilsAdapter(context, utilizationArrayList);
        loaReqAdapter = new LoaReqAdapter(context, loaList, dataBaseHandler, callback, loaListCardRetrieve);


        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        ci_user = (CircleImageView) findViewById(R.id.ci_user);
        tv_gender = (TextView) findViewById(R.id.tv_gender);
        tvInPatientStatus = (TextView) findViewById(R.id.tvInPatientStatus);
        tvInPatientStatusStatusTag = (TextView) findViewById(R.id.tvInPatientStatusStatusTag);
        tvErStatusTag = (TextView) findViewById(R.id.tvErStatusTag);
        tvErStatus = (TextView) findViewById(R.id.tvErStatus);
        tv_company = (TextView) findViewById(R.id.tv_company);
        tv_status = (TextView) findViewById(R.id.tv_status);
        tv_age = (TextView) findViewById(R.id.tv_age);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_no_loareq = (TextView) findViewById(R.id.tv_no_loareq);
        tv_id = (TextView) findViewById(R.id.tv_id);
        tv_status_set = (TextView) findViewById(R.id.tv_status_set);
        tv_noDependents = (TextView) findViewById(R.id.tv_noDependents);
        tv_noUtils = (TextView) findViewById(R.id.tv_noUtils);
        tv_depedents = (TextView) findViewById(R.id.tv_depedents);
        tv_utilization = (TextView) findViewById(R.id.tv_utilization);
        btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_loa = (Button) findViewById(R.id.btn_loa);
        btn_Er = (LinearLayout) findViewById(R.id.btn_Er);
        btn_in_patient = (LinearLayout) findViewById(R.id.btn_in_patient);
        ll_dependents_container = (LinearLayout) findViewById(R.id.ll_dependents_container);
        ll_utilization_container = (LinearLayout) findViewById(R.id.ll_utilization_container);
        ll_loa_req_list = (LinearLayout) findViewById(R.id.ll_loa_req_list);
        rv_loa = (RecyclerView) findViewById(R.id.rv_loa);
        progressBar3 = (ProgressBar) findViewById(R.id.progressBar3);
        pd_in_patient = (ProgressBar) findViewById(R.id.pd_in_patient);
        cv_btn_container = (CardView) findViewById(R.id.cv_btn_container);
        cv_block = (CardView) findViewById(R.id.cv_block);
        sr_swipe = (SwipeRefreshLayout) findViewById(R.id.sr_swipe);
        sr_swipe.setColorSchemeResources(R.color.colorPrimary);
        sr_swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateLoaReq();
            }
        });


        tv_noDependents.setVisibility(View.GONE);
        tv_noUtils.setVisibility(View.GONE);
        btn_loa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //OUT PATIENT AVAILABILITY
                if (SharedPref.getBooleanValue(SharedPref.USER, SharedPref.ISINER, context)) {

                    implement.DischargeERToOutpatient(SharedPref.getStringValue(SharedPref.USER, SharedPref.REQUEST_CODE_ER, context), SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context), listCallback);

                } else if (consultation.equals("TRUE") || otherTest.equals("TRUE")) {
                    Intent gotoCoords = new Intent(CoordinatorMenuActivity.this, NavigatorActivity.class);

                    //      gotoCoords.putStringArrayListExtra(SearchToCoordinatorMenu.APPROVAL_NO, getIntent().getStringArrayListExtra("APPROVAL_NO"));

                    gotoCoords.putExtra("PANEL", "OUT_PATIENT");

                    /*
                      commented this blocks of codes because of the SharedPref Class that handles the
                      data that can be use in all Activities onCreate
                     */
//                    gotoCoords.putExtra("NAME", getIntent().getExtras().getString(SearchToCoordinatorMenu.NAME));
//                    gotoCoords.putExtra("ID", getIntent().getExtras().getString(SearchToCoordinatorMenu.ID));
//                    gotoCoords.putExtra(SearchToCoordinatorMenu.STATUS, getIntent().getExtras().getString(SearchToCoordinatorMenu.STATUS));
//                    gotoCoords.putExtra(SearchToCoordinatorMenu.GENDER, getIntent().getExtras().getString(SearchToCoordinatorMenu.GENDER));
//                    gotoCoords.putExtra(SearchToCoordinatorMenu.AGE, getIntent().getExtras().getString(SearchToCoordinatorMenu.AGE));
//                    gotoCoords.putExtra(SearchToCoordinatorMenu.MEMCODE, getIntent().getExtras().getString(SearchToCoordinatorMenu.MEMCODE));
//                    gotoCoords.putExtra(SearchToCoordinatorMenu.TYPE, getIntent().getExtras().getString(SearchToCoordinatorMenu.TYPE));
//                    gotoCoords.putExtra(SearchToCoordinatorMenu.EFFECTIVE, getIntent().getExtras().getString(SearchToCoordinatorMenu.EFFECTIVE));
//                    gotoCoords.putExtra(SearchToCoordinatorMenu.VALID, getIntent().getExtras().getString(SearchToCoordinatorMenu.VALID));
//                    gotoCoords.putExtra(SearchToCoordinatorMenu.DD_LIMIT, getIntent().getExtras().getInt(SearchToCoordinatorMenu.DD_LIMIT));
//                    gotoCoords.putExtra(SearchToCoordinatorMenu.OTHER, getIntent().getExtras().getString(SearchToCoordinatorMenu.OTHER));
//                    gotoCoords.putExtra(SearchToCoordinatorMenu.PLAN, getIntent().getExtras().getString(SearchToCoordinatorMenu.PLAN));
//                    gotoCoords.putExtra(SearchToCoordinatorMenu.BDAY, getIntent().getExtras().getString(SearchToCoordinatorMenu.BDAY));
//                    gotoCoords.putExtra(SearchToCoordinatorMenu.COMPANY, getIntent().getExtras().getString(SearchToCoordinatorMenu.COMPANY));
                    startActivity(gotoCoords);

                } else {

                    RequestBlockLogger.logger(
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.USERNAME, context),
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context),
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context),
                            getString(R.string.out_patient_not_available),
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, context)

                    );
                    alertDialogCustom.showMe(context, alertDialogCustom.unknown, getString(R.string.out_patient_not_available), 1);
                }

            }
        });


        rv_dependents = (RecyclerView) findViewById(R.id.rv_dependent);
        rv_utils = (RecyclerView) findViewById(R.id.rv_utils);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(context);
        rv_dependents.setLayoutManager(mLayoutManager);
        rv_utils.setLayoutManager(mLayoutManager2);

        rv_dependents.setAdapter(adapter);
        rv_utils.setAdapter(utilsAdapter);

        rv_dependents.setNestedScrollingEnabled(false);
        rv_utils.setNestedScrollingEnabled(false);

        rv_loa.setLayoutManager(new LinearLayoutManager(context));
        rv_loa.setAdapter(loaReqAdapter);


        tv_name.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.FULL_NAME, context));
        tv_age.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.AGE, context));
        tv_id.setText(MEMBERID);
        tv_company.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.COMPANY_NAME, context));
        ci_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMe(context);

            }
        });
//            if(getIntent().getExtras().getString(SearchToCoordinatorMenu.AGE).length() > 1){
//                tv_age.setText(getIntent().getExtras().getString(SearchToCoordinatorMenu.AGE).substring(0,2));
//            }else {
//                tv_age.setText(getIntent().getExtras().getString(SearchToCoordinatorMenu.AGE));
//            }
//            tv_gender.setText(GenderPicker.setGender(Integer.parseInt(sharedPref.getStringValue(sharedPref.USER, sharedPref.GENDER, context))));
//            tv_age.setText(AgeCorrector.age(sharedPref.getStringValue(sharedPref.USER, sharedPref.AGE, context)));
        try {
            tv_gender.setText(SharedPref.getStringValue(SharedPref.USER, SharedPref.GENDER, context));
        } catch (Exception e) {
            e.printStackTrace();
        }


        System.out.println("value of IP  " + isInPatient);
        System.out.println("value of ER " + isER);
        System.out.println("value of is_er " + String.valueOf(isinER));

        tvInPatientStatus.setText(StatusSetter.inPatientStatus(context, txtInpatient, (SharedPref.getBooleanValue(SharedPref.USER, SharedPref.ISINPATIENT, context))));


        if (isinIP) {

//            btn_loa.setEnabled(false);
//            btn_Er.setEnabled(false);
//            btn_loa.setBackgroundColor(getResources().getColor(R.color.gray));
//            btn_Er.setBackgroundColor(getResources().getColor(R.color.gray));

            /**
             * IF MEMBER IS ADMITTED, BUTTON FOR INPATIENT
             * IS DISABLED BUT OUT PATIENT AND ER ARE STILL ACCESSIBLE - JHAY
             * */
            btn_in_patient.setEnabled(false);
            btn_in_patient.setBackgroundColor(getResources().getColor(R.color.gray));
        }


        if (isinER) {
            tvInPatientStatusStatusTag.setVisibility(View.GONE);
            tvInPatientStatus.setVisibility(View.GONE);
            //Setting the text of the inpatient button to "Discharge To  In-Patient"
            //Aquino Francisco III August 2, 2017 2 am
            txtInpatient.setText(SearchToCoordinatorMenu.DISCHARGE_TO_INPATIENT);
            btn_loa.setText(SearchToCoordinatorMenu.DISCHARGE_TO_OUTPATIENT);
            tvErStatus.setVisibility(View.VISIBLE);
            tvErStatusTag.setVisibility(View.VISIBLE);
            tvErStatus.setText(StatusSetter.erStatus(context, txtER, (SharedPref.getBooleanValue(SharedPref.USER, SharedPref.ISINER, context))));
        }


        StatusSetter.setStatus(SharedPref.getStringValue(SharedPref.USER, SharedPref.MEMBER_STATUS, context), tv_status);
        pd = new ProgressDialog(CoordinatorMenuActivity.this, R.style.MyTheme);
        pd.setCancelable(false);
        pd.setMessage("Requesting...");
        pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);


        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CoordinatorMenuActivity.this, MemberSearchActivity.class));

            }
        });
        btn_in_patient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isInPatientLoading) {
                    if (isInPatient.equals("TRUE")) {
                        if (SharedPref.getBooleanValue(SharedPref.USER, SharedPref.ISINPATIENT, context)) {
                            isInPatientLoading = true;
                            implement.upDateUILoadingInPatient(btn_in_patient, pd_in_patient, true);
                            implement.getInPatientDetails(SharedPref.getStringValue(SharedPref.USER, SharedPref.REQUEST_CODE_INPATIENT, context), listCallback);
                        } else if (SharedPref.getBooleanValue(SharedPref.USER, SharedPref.ISINER, context)) {

                            implement.DischargeERToInpatient(SharedPref.getStringValue(SharedPref.USER, SharedPref.REQUEST_CODE_ER, context), SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context), listCallback);
                        } else {
                            //todo ADD IS_IN_PATIENT THE REQUIRED DATA
                            Intent gotoCoords = new Intent(CoordinatorMenuActivity.this, NavigatorActivity.class);
                            gotoCoords.putExtra("PANEL", "IN_PATIENT");
//                            gotoCoords.putExtra("NAME", getIntent().getExtras().getString(SearchToCoordinatorMenu.NAME));
//                            gotoCoords.putExtra("ID", getIntent().getExtras().getString(SearchToCoordinatorMenu.ID));
//                            gotoCoords.putExtra(SearchToCoordinatorMenu.STATUS, getIntent().getExtras().getString(SearchToCoordinatorMenu.STATUS));
//                            gotoCoords.putExtra(SearchToCoordinatorMenu.GENDER, getIntent().getExtras().getString(SearchToCoordinatorMenu.GENDER));
//                            gotoCoords.putExtra(SearchToCoordinatorMenu.AGE, getIntent().getExtras().getString(SearchToCoordinatorMenu.AGE));
//                            gotoCoords.putExtra(SearchToCoordinatorMenu.MEMCODE, getIntent().getExtras().getString(SearchToCoordinatorMenu.MEMCODE));
//                            gotoCoords.putExtra(SearchToCoordinatorMenu.TYPE, getIntent().getExtras().getString(SearchToCoordinatorMenu.TYPE));
//                            gotoCoords.putExtra(SearchToCoordinatorMenu.EFFECTIVE, getIntent().getExtras().getString(SearchToCoordinatorMenu.EFFECTIVE));
//                            gotoCoords.putExtra(SearchToCoordinatorMenu.VALID, getIntent().getExtras().getString(SearchToCoordinatorMenu.VALID));
//                            gotoCoords.putExtra(SearchToCoordinatorMenu.DD_LIMIT, getIntent().getExtras().getInt(SearchToCoordinatorMenu.DD_LIMIT));
//                            gotoCoords.putExtra(SearchToCoordinatorMenu.OTHER, getIntent().getExtras().getString(SearchToCoordinatorMenu.OTHER));
//                            gotoCoords.putExtra(SearchToCoordinatorMenu.PLAN, getIntent().getExtras().getString(SearchToCoordinatorMenu.PLAN));
//                            gotoCoords.putExtra(SearchToCoordinatorMenu.BDAY, getIntent().getExtras().getString(SearchToCoordinatorMenu.BDAY));
//                            gotoCoords.putExtra(SearchToCoordinatorMenu.COMPANY, getIntent().getExtras().getString(SearchToCoordinatorMenu.COMPANY));
//                            gotoCoords.putExtra(SearchToCoordinatorMenu.IS_IN_PATIENT, getIntent().getExtras().getBoolean(SearchToCoordinatorMenu.IS_IN_PATIENT));
//                            gotoCoords.putExtra(SearchToCoordinatorMenu.REQUEST_CODE_IN_PATIENT, getIntent().getExtras().getString(SearchToCoordinatorMenu.REQUEST_CODE_IN_PATIENT));
                            startActivity(gotoCoords);
                            EventBus.getDefault().post(new AvailServices.MessageEvent(7 + "", "", "", "", "", ""));
                        }
                    }

                } else {
                    RequestBlockLogger.logger(
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.USERNAME, context),
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context),
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context),
                            getString(R.string.in_patient_not_available),
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.PHONE_ID, context)

                    );

                    alertDialogCustom.showMe(context, alertDialogCustom.unknown, getString(R.string.in_patient_not_available), 1);
                }
            }


        });


        btn_Er.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {

                if (isER.equalsIgnoreCase("TRUE")) {
                    Log.d("appUsername", SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context));
                    if (SharedPref.getBooleanValue(SharedPref.USER, SharedPref.ISINER, context)) {

                        implement.DischargeER(SharedPref.getStringValue(SharedPref.USER, SharedPref.REQUEST_CODE_ER, context), SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context), listCallback);
                    } else {
                        Intent gotoCoords = new Intent(CoordinatorMenuActivity.this, NavigatorActivity.class);
                        gotoCoords.putExtra("PANEL", "ER");
//                        gotoCoords.putExtra("NAME", getIntent().getExtras().getString(SearchToCoordinatorMenu.NAME));
//                        gotoCoords.putExtra("ID", getIntent().getExtras().getString(SearchToCoordinatorMenu.ID));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.STATUS, getIntent().getExtras().getString(SearchToCoordinatorMenu.STATUS));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.GENDER, getIntent().getExtras().getString(SearchToCoordinatorMenu.GENDER));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.AGE, getIntent().getExtras().getString(SearchToCoordinatorMenu.AGE));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.MEMCODE, getIntent().getExtras().getString(SearchToCoordinatorMenu.MEMCODE));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.TYPE, getIntent().getExtras().getString(SearchToCoordinatorMenu.TYPE));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.EFFECTIVE, getIntent().getExtras().getString(SearchToCoordinatorMenu.EFFECTIVE));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.VALID, getIntent().getExtras().getString(SearchToCoordinatorMenu.VALID));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.DD_LIMIT, getIntent().getExtras().getInt(SearchToCoordinatorMenu.DD_LIMIT));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.OTHER, getIntent().getExtras().getString(SearchToCoordinatorMenu.OTHER));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.PLAN, getIntent().getExtras().getString(SearchToCoordinatorMenu.PLAN));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.BDAY, getIntent().getExtras().getString(SearchToCoordinatorMenu.BDAY));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.COMPANY, getIntent().getExtras().getString(SearchToCoordinatorMenu.COMPANY));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.IS_ER, getIntent().getExtras().getBoolean(SearchToCoordinatorMenu.IS_ER));
//
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.REQUEST_CODE_ER, getIntent().getExtras().getString(SearchToCoordinatorMenu.REQUEST_CODE_ER));
                        startActivity(gotoCoords);
//                        Log.d("IS_ADMITTED", getIntent().getExtras().getBoolean(SearchToCoordinatorMenu.IS_ER) + "");

                        EventBus.getDefault().post(new AvailServices.MessageEvent(7 + "", "", "", "", "", ""));
                    }
                } else {
                    sendEr(SharedPref.getStringValue(SharedPref.USER, SharedPref.ID, context),
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context),
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.HOSPITALCODE, context),
                            SharedPref.getStringValue(SharedPref.USER, SharedPref.FULL_NAME, context));

                }
            }
        });

        ll_dependents_container.setVisibility(View.GONE);
        ll_utilization_container.setVisibility(View.GONE);
        tv_depedents.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {

                if (dependentIsOpen) {
                    ll_dependents_container.setVisibility(View.VISIBLE);
                    dependentIsOpen = false;
                } else {
                    ll_dependents_container.setVisibility(View.GONE);
                    dependentIsOpen = true;
                }

            }
        });


        tv_utilization.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                if (dependentIsOpen) {
                    ll_utilization_container.setVisibility(View.VISIBLE);
                    dependentIsOpen = false;
                } else {
                    ll_utilization_container.setVisibility(View.GONE);
                    dependentIsOpen = true;
                }

            }
        });

        getPhoto();


        ArrayList<Dependents> dependents = SharedPref.dependents;
        if (dependents != null)

        {
            if (dependents.size() > 0) {
                dependentsArrayList.addAll(dependents);
                adapter.notifyDataSetChanged();

                rv_dependents.setVisibility(View.VISIBLE);
                tv_noDependents.setVisibility(View.GONE);

            } else {

                rv_dependents.setVisibility(View.GONE);
                tv_noDependents.setVisibility(View.VISIBLE);

            }

        } else

        {
            rv_dependents.setVisibility(View.GONE);
            tv_noDependents.setVisibility(View.VISIBLE);

        }

        ArrayList<Utilization> utils = SharedPref.util;
        if (utils != null)

        {
            if (utils.size() > 0) {
                utilizationArrayList.addAll(utils);
                utilsAdapter.notifyDataSetChanged();

                tv_noUtils.setVisibility(View.GONE);
                rv_utils.setVisibility(View.VISIBLE);

            } else {
                tv_noUtils.setVisibility(View.VISIBLE);
                rv_utils.setVisibility(View.GONE);

            }
        } else

        {
            tv_noUtils.setVisibility(View.VISIBLE);
            rv_utils.setVisibility(View.GONE);
        }

//    setAvailableButtonStatus();

        if (!sr_swipe.isRefreshing())
            sr_swipe.setRefreshing(true);
        implement.getLoaReq(listCallback, MEMBERID);

    }


    public void updateLoaReq() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (NetworkTest.isOnline(context)) {
                    loaList.clear();
                    loaListCardRetrieve.clear();
                    loaReqAdapter.notifyDataSetChanged();
                    sr_swipe.setRefreshing(false);
//          ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayList);
//          mListView.setAdapter(adapter);
                    implement.getLoaReq(listCallback, MEMBERID);
                    loaReqAdapter = new LoaReqAdapter(context, loaList, dataBaseHandler, callback, loaListCardRetrieve);
                    loaReqAdapter.notifyDataSetChanged();
                    rv_loa.setAdapter(loaReqAdapter);
                } else {

                }
            }
        }, 3000);
    }

    private void sendEr(String memberCode, String username, String hospCode, String name) {

        pd.show();
        SendEr sendEr = new SendEr();
        sendEr.setUsername(username);
        sendEr.setHospitalCode(hospCode);
        sendEr.setMemberName(name);
        sendEr.setMemberCode(memberCode);

        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.inquireER(sendEr)
                .enqueue(new retrofit2.Callback<GetEr>() {
                    @Override
                    public void onResponse(Call<GetEr> call, Response<GetEr> response) {
                        Log.e("ERROR", response.body().toString());

                        Intent gotoCoords = new Intent(CoordinatorMenuActivity.this, NavigatorActivity.class);
                        gotoCoords.putExtra("PANEL", "ER");
//                        gotoCoords.putExtra("NAME", getIntent().getExtras().getString(SearchToCoordinatorMenu.NAME));
//                        gotoCoords.putExtra("ID", getIntent().getExtras().getString(SearchToCoordinatorMenu.ID));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.STATUS, getIntent().getExtras().getString(SearchToCoordinatorMenu.STATUS));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.GENDER, getIntent().getExtras().getString(SearchToCoordinatorMenu.GENDER));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.AGE, getIntent().getExtras().getString(SearchToCoordinatorMenu.AGE));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.MEMCODE, getIntent().getExtras().getString(SearchToCoordinatorMenu.MEMCODE));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.TYPE, getIntent().getExtras().getString(SearchToCoordinatorMenu.TYPE));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.EFFECTIVE, getIntent().getExtras().getString(SearchToCoordinatorMenu.EFFECTIVE));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.VALID, getIntent().getExtras().getString(SearchToCoordinatorMenu.VALID));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.DD_LIMIT, getIntent().getExtras().getInt(SearchToCoordinatorMenu.DD_LIMIT));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.OTHER, getIntent().getExtras().getString(SearchToCoordinatorMenu.OTHER));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.PLAN, getIntent().getExtras().getString(SearchToCoordinatorMenu.PLAN));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.BDAY, getIntent().getExtras().getString(SearchToCoordinatorMenu.BDAY));
//                        gotoCoords.putExtra(SearchToCoordinatorMenu.COMPANY, getIntent().getExtras().getString(SearchToCoordinatorMenu.COMPANY));
                        startActivity(gotoCoords);
                        pd.dismiss();
                    }

                    @Override
                    public void onFailure(Call<GetEr> call, Throwable e) {

                        pd.dismiss();

                        try {
                            Log.e("ERROR", e.getMessage());
                        } catch (Exception e1) {
                            e.printStackTrace();
                        }

                    }
                });

    }

    private void setAvailableButtonStatus() {

        Log.d("STATUS", getIntent().getExtras().getString(SearchToCoordinatorMenu.STATUS));
        //TEST USER STATUS


        if (implement.setApprovedStatus(getIntent().getExtras().getString(SearchToCoordinatorMenu.STATUS))) {
            btn_loa.setVisibility(View.VISIBLE);
            btn_in_patient.setVisibility(View.VISIBLE);
            cv_block.setVisibility(View.GONE);

            EffectivityAndValidityDateTest dateTest = new EffectivityAndValidityDateTest();


            //TEST VALIDITY
            if (dateTest.isValid(getIntent().getExtras().getString(SearchToCoordinatorMenu.VALIDITYDATE))) {
                btn_loa.setVisibility(View.GONE);
                btn_in_patient.setVisibility(View.GONE);
                cv_block.setVisibility(View.VISIBLE);
                tv_status_set.setText(Validity_message);


            } else {
                //TEST EFFECTIVITY
                if (dateTest.isEffective(getIntent().getExtras().getString(SearchToCoordinatorMenu.EFFECTIVITYDATE))) {
                    btn_loa.setVisibility(View.GONE);
                    btn_in_patient.setVisibility(View.GONE);
                    cv_block.setVisibility(View.VISIBLE);
                    tv_status_set.setText(Effectivty_message);


                } else {


                    if (isInPatient.equals("FALSE") && consultation.equals("FALSE") && otherTest.equals("FALSE")) {
                        btn_loa.setVisibility(View.GONE);
                        btn_in_patient.setVisibility(View.GONE);
                        cv_block.setVisibility(View.VISIBLE);
                        tv_status_set.setText(R.string.no_access_to_hospital);

                    }

                    Log.d("CONSULT", consultation);
                    Log.d("In-PATIENT", isInPatient);
                    Log.d("Other_test", otherTest);
                }
            }


        } else {

            cv_btn_container.setVisibility(View.GONE);
            btn_loa.setVisibility(View.GONE);
            btn_Er.setVisibility(View.GONE);
            btn_in_patient.setVisibility(View.GONE);

            cv_block.setVisibility(View.VISIBLE);
            tv_status_set.setText(statusSetter.setRemarks(getIntent().getExtras().getString(SearchToCoordinatorMenu.STATUS)));


        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        try {


            if (requestCode == CAMERA_RQ) {

                if (resultCode != Activity.RESULT_CANCELED) {


                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;


                    File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                    File output = new File(dir + File.separator + "CameraMEDICARD.jpeg");
                    Log.d("FILE_PATH", output.getAbsolutePath());


                    //ROtate if need
                    Bitmap bitmap;
                    ImageConverters imageConverters = new ImageConverters();
                    bitmap = imageConverters.rotateBitmapOrientation(output.getAbsolutePath());

                    //Save bitmap
                    imageConverters.saveToInternalStorage(ci_user, bitmap, context, true);


                    try {
                        File f = new File(dir, "CameraMEDICARD.jpeg");
                        Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
                        ci_user.setImageBitmap(b);
                        Log.v("Upload", f.toString());

                        showImageUpload(f, MEMBERID);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }


                }


            }


        } catch (Exception e) {


        }
    }


    public void showMe(Context contex) {

        final CharSequence[] items = {CAMERA};

        AlertDialog.Builder builder = new AlertDialog.Builder(contex);
        builder.setTitle("Select:");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                if (String.valueOf(items[item]).equals(CAMERA)) {


                    if (Permission.checkPermissionCamera(context)) {
                        if (Permission.checkPermissionStorage(context)) {

                            Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            File dir =
                                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);

                            output = new File(dir, "CameraMEDICARD.jpeg");
                            i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(output));
                            startActivityForResult(i, CAMERA_RQ);

                        }
                    }

                }


            }
        }).show();
    }

    private void showDeletePhoto(final String ID) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage("Photo will be updated. Continue?");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        if (NetworkTest.isOnline(context)) {
                            deleteImage(ID);
                        } else {
                            getPhoto();
                            alertDialogCustom.showMe(context, alertDialogCustom.NO_Internet_title, alertDialogCustom.NO_Internet, 1);
                        }

                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        getPhoto();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();


    }

    private void deleteImage(final String member_id) {


        pd.show();
        AppInterface appInterface;
        appInterface = AppServicewithBasicAuth.createApiService(AppInterface.class, AppInterface.ENDPOINT, SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context), SharedPref.getStringValue(SharedPref.USER, SharedPref.masterPASSWORD, context));
        appInterface.deletePhoto(member_id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Upload error:", e.getMessage());
                        pd.dismiss();
                        alertDialogCustom.showMe(context, alertDialogCustom.unknown, alertDialogCustom.unknown_msg, 1);
                        getPhoto();
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        alertDialogCustom.showMe(context, alertDialogCustom.success, alertDialogCustom.delete_msg, 2);
                        pd.dismiss();
                        getPhoto();
                    }
                });
    }


    public void showImageUpload(final File file, final String memberID) {


        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage("Are you sure you want to upload this photo?");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        if (NetworkTest.isOnline(context)) {
                            uploadImage(file, memberID);
                        } else {
                            getPhoto();
                            alertDialogCustom.showMe(context, alertDialogCustom.NO_Internet_title, alertDialogCustom.NO_Internet, 1);

                        }

                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        getPhoto();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();


    }


    private void getPhoto() {


        progressBar.setVisibility(View.VISIBLE);


        Picasso.with(context)

                .load(AppInterface.PHOTOLINK + MEMBERID)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .placeholder(R.drawable.imageplaceholder)
                .error(R.drawable.imageplaceholder)
                .into(ci_user, new Callback() {
                    @Override
                    public void onSuccess() {

                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        progressBar.setVisibility(View.GONE);
                    }
                });

    }

    @Override
    protected void onResume() {
        super.onResume();
//        implement.getLoaReq(listCallback, MEMBERID);
    }


    private void uploadImage(final File file, final String memberCode) {

        pd.show();

        RequestBody fbody = RequestBody.create(MediaType.parse("image/jpeg"), file);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), memberCode);
        RequestBody username = RequestBody.create(MediaType.parse("text/plain"),
                SharedPref.getStringValue(SharedPref.USER, SharedPref.masterUSERNAME, context));
        RequestBody type = RequestBody.create(MediaType.parse("text/plain"), "COORDINATOR");
        Log.v("Upload", file.toString());
        AppInterface appInterface;
        appInterface = AppService.createApiService(AppInterface.class, AppInterface.ENDPOINT);
        appInterface.upload(
                fbody,
                name,
                username,
                type
        ).enqueue(new retrofit2.Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.v("Upload", "success");
                pd.dismiss();
                alertDialogCustom.showMe(context, alertDialogCustom.success, alertDialogCustom.success_msg, 2);
                Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                ci_user.setImageBitmap(myBitmap);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
                pd.dismiss();
                alertDialogCustom.showMe(context, alertDialogCustom.unknown, alertDialogCustom.unknown_msg, 1);
                getPhoto();
            }
        });

    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onLoaRowSelected(int position) {
        SharedPref.LOALIST = loaList.get(position);
        Intent gotoReq = new Intent(CoordinatorMenuActivity.this, LoaDisplayActivity.class);
//        gotoReq.putExtra("LOALIST", loaList.get(position));
//        gotoReq.putExtra(SearchToCoordinatorMenu.GENDER, getIntent().getExtras().getString(SearchToCoordinatorMenu.GENDER));
//        gotoReq.putExtra(SearchToCoordinatorMenu.AGE, getIntent().getExtras().getString(SearchToCoordinatorMenu.AGE));
        startActivity(gotoReq);
    }

    @Override
    public void onSuccessLoaList(LoaReq loaReq) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (sr_swipe.isRefreshing())
                    sr_swipe.setRefreshing(false);
            }
        }, 3000);


        progressBar3.setVisibility(View.GONE);
        Log.d("LOA_", loaReq.toString());
        if (loaReq.getLoaList().size() == 0) {
            System.out.println("MACEMACE EMPTY");
            rv_loa.setVisibility(View.GONE);
            tv_no_loareq.setVisibility(View.VISIBLE);
        } else {
            System.out.println("MACEMACE NOT EMPTY");
            loaList.clear();
            loaList.addAll(loaReq.getLoaList());
            if (loaList.size() != 0) {
                int y = 0;
                for (LoaList list : loaList) {
                    LoaListCardRetrieve loaListCard;
                    //todo: remove comment after debugging -jhay
                    try {
                        switch (list.getRequestType()) {
                            case "CONSULTATION":
                            case "MATERNITY CONSULTATION":
                                loaListCard = new LoaListCardRetrieve();
                                loaListCard.setServiceType(list.getRequestType());
                                loaListCard.setRequestCode(list.getRequestCode());
                                loaListCard.setRequestDateTime(list.getRequestDatetime());
                                loaListCard.setLine3(list.getRequestDatetime());
                                loaListCard.setLine4(list.getStatus());
                                loaListCard.setLine1(list.getDoctorName());
                                loaListCard.setLine2(list.getDoctorSpec());
                                loaListCard.setApprovalNos(list.getApprovalNo());
                                loaListCard.setLoaListIndex(y++);
                                loaListCardRetrieve.add(loaListCard);
                                System.out.println("LOA LIST CARD SIZE: " + loaListCardRetrieve.size());
                                break;
                            case "BASIC TEST":
                            case "OTHER TEST":
                                for (LoaList.GroupedByCostCenter groupedByCostCenter : list.getGroupedByCostCenters()) {
                                    loaListCard = new LoaListCardRetrieve();
                                    String approvalNos = "";
                                    int x = 0;
                                    for (LoaList.GroupedByCostCenter.GroupedByDiag groupedByDiag : groupedByCostCenter.getGroupedByDiag()) {
                                        approvalNos += (x++) == 0 ? groupedByDiag.getApprovalNo() : "\n" + groupedByDiag.getApprovalNo();
                                        loaListCard.setServiceType(list.getRequestType());
                                        loaListCard.setRequestCode(list.getRequestCode());
                                        loaListCard.setRequestDateTime(list.getRequestDatetime());
                                        loaListCard.setLine3(list.getRequestDatetime());
                                        loaListCard.setLine4(groupedByCostCenter.getStatus());
                                        loaListCard.setLine1(groupedByCostCenter.getCostCenter());
                                        String allProcDesc = "";
                                        for (int i = 0; i < groupedByDiag.getMappedTests().size(); i++) {
                                            if (i == 0) {
                                                allProcDesc += groupedByDiag.getMappedTests().get(i).getProcDesc();
                                            } else {

                                                allProcDesc += ", " + groupedByDiag.getMappedTests().get(i).getProcDesc();
                                            }
                                        }
                                        loaListCard.setLine2(allProcDesc);
                                        System.out.println("LOA LIST CARD SIZE: " + loaListCardRetrieve.size());
                                    }
                                    loaListCard.setLoaListIndex(y);
                                    loaListCard.setApprovalNos(approvalNos);
                                    loaListCardRetrieve.add(loaListCard);
                                }
                                y++;
                                break;
                            case "PROCEDURE":
                                for (LoaList.MappedTest mappedTest : list.getMappedTest()) {
                                    loaListCard = new LoaListCardRetrieve();
                                    loaListCard.setServiceType(list.getRequestType());
                                    loaListCard.setRequestCode(list.getRequestCode());
                                    loaListCard.setRequestDateTime(list.getRequestDatetime());
                                    loaListCard.setLine3(list.getRequestDatetime());
                                    loaListCard.setLine4(list.getStatus());
                                    loaListCard.setLine1(list.getDiagnosis());
                                    loaListCard.setLine2(mappedTest.getProcDesc());
                                    loaListCard.setApprovalNos(list.getApprovalNo());
                                    loaListCard.setLoaListIndex(y);
                                    loaListCardRetrieve.add(loaListCard);
                                    System.out.println("LOA LIST CARD SIZE: " + loaListCardRetrieve.size());
                                }
                                y++;
                                break;
                            default:
                                break;
                        }

                    } catch (Exception e) {

                    }


                }
                //sort via date
//                ArrayList<LoaList.LoaListCardRetrieve> temp = new ArrayList<>();
//                temp.addAll(loaListCardRetrieve);
//                loaListCardRetrieve.clear();
//                loaListCardRetrieve.addAll(SortLOAViaDate.sortData(temp));
            }
//            Collections.reverse(loaList);
            loaReqAdapter.notifyDataSetChanged();
            tv_no_loareq.setVisibility(View.GONE);
            rv_loa.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public void onErrorLoaList(String message) {
        if (sr_swipe.isRefreshing())
            sr_swipe.setRefreshing(false);
        progressBar3.setVisibility(View.GONE);
        tv_no_loareq.setVisibility(View.VISIBLE);
        tv_no_loareq.setText("Error Fetching LOA List. \n" + ErrorMessage.setErrorMessage(message));
        rv_loa.setVisibility(View.GONE);
    }

    @Override
    public void onErrorInPatientFetch(String message) {
        isInPatientLoading = false;
        implement.upDateUILoadingInPatient(btn_in_patient, pd_in_patient, false);
    }

    @Override
    public void onSuccessInPatientFetch(InPatient inPatient1) {
        isInPatientLoading = false;
        implement.upDateUILoadingInPatient(btn_in_patient, pd_in_patient, false);
        //todo ADD IS_IN_PATIENT THE REQUIRED DATA
        Intent gotoCoords = new Intent(CoordinatorMenuActivity.this, NavigatorActivity.class);
        gotoCoords.putExtra("PANEL", "IN_PATIENT");
//        gotoCoords.putExtra("NAME", getIntent().getExtras().getString(SearchToCoordinatorMenu.NAME));
//        gotoCoords.putExtra("ID", getIntent().getExtras().getString(SearchToCoordinatorMenu.ID));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.STATUS, getIntent().getExtras().getString(SearchToCoordinatorMenu.STATUS));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.GENDER, getIntent().getExtras().getString(SearchToCoordinatorMenu.GENDER));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.AGE, getIntent().getExtras().getString(SearchToCoordinatorMenu.AGE));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.MEMCODE, getIntent().getExtras().getString(SearchToCoordinatorMenu.MEMCODE));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.TYPE, getIntent().getExtras().getString(SearchToCoordinatorMenu.TYPE));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.EFFECTIVE, getIntent().getExtras().getString(SearchToCoordinatorMenu.EFFECTIVE));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.VALID, getIntent().getExtras().getString(SearchToCoordinatorMenu.VALID));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.DD_LIMIT, getIntent().getExtras().getInt(SearchToCoordinatorMenu.DD_LIMIT));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.OTHER, getIntent().getExtras().getString(SearchToCoordinatorMenu.OTHER));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.PLAN, getIntent().getExtras().getString(SearchToCoordinatorMenu.PLAN));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.BDAY, getIntent().getExtras().getString(SearchToCoordinatorMenu.BDAY));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.COMPANY, getIntent().getExtras().getString(SearchToCoordinatorMenu.COMPANY));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.IS_IN_PATIENT, getIntent().getExtras().getBoolean(SearchToCoordinatorMenu.IS_IN_PATIENT));
//        gotoCoords.putExtra(SearchToCoordinatorMenu.REQUEST_CODE_IN_PATIENT, getIntent().getExtras().getString(SearchToCoordinatorMenu.REQUEST_CODE_IN_PATIENT));
        Gson gson = new Gson();
        String myJson = gson.toJson(inPatient1);
        gotoCoords.putExtra(SearchToCoordinatorMenu.REQUEST_DATA_IN_PATIENT, myJson);

        Log.d("INPATIENT_", inPatient1.toString());
        startActivity(gotoCoords);

        EventBus.getDefault().post(new AvailServices.MessageEvent(7 + "", "", "", "", "", ""));

    }

    CircleImageView ci_error_image;
    TextView tv_message;
    TextView tv_title1;
    Button btn_accept;

    private void setDetails(Context context, String message, String title, int errorImage, Button button) {

        tv_message.setText(message);
        tv_title1.setText(title);


        switch (errorImage) {

            case 1:
//error
                ci_error_image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon));
                button.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimaryLight));
                break;


            case 2:
//congrats
                ci_error_image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.success));
                button.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));

                break;


            case 3:
//Log out
                ci_error_image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.logout));
                button.setBackgroundColor(ContextCompat.getColor(context, R.color.BLACK));

                break;


        }
    }


    @Override
    public void dischargeERSuccess() {

        System.out.println("PUMASOK DITO SA ER");

//        Intent gotoResult = new Intent(getContext(), ResultActivity.class);
//        gotoResult.putExtra("REQUEST", "ER");;
//        gotoResult.putExtra("ER_REASON", er_reason );
//        gotoResult.putExtra("DATEADMITTED", DateConverter.convertDatetoyyyyMMdd(dateAdmitted));
//
//        startActivity(gotoResult);
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alertshow);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        ci_error_image = (CircleImageView) dialog.findViewById(R.id.ci_error_image);
        tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        tv_title1 = (TextView) dialog.findViewById(R.id.tv_title);
        btn_accept = (Button) dialog.findViewById(R.id.btn_accept);
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MemberSearchActivity.class);
                startActivity(intent);
            }
        });

        tv_message.setText("The member is Discharge");


        dialog.show();


        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.40);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = width;
        dialog.getWindow().setAttributes(lp);


    }


    @Override
    public void dischargeERToInpatient() {

        System.out.println("PUMASOK DITO SA ER");

//        Intent gotoResult = new Intent(getContext(), ResultActivity.class);
//        gotoResult.putExtra("REQUEST", "ER");;
//        gotoResult.putExtra("ER_REASON", er_reason );
//        gotoResult.putExtra("DATEADMITTED", DateConverter.convertDatetoyyyyMMdd(dateAdmitted));
//
//        startActivity(gotoResult);
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alertshow);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        ci_error_image = (CircleImageView) dialog.findViewById(R.id.ci_error_image);
        tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        tv_title1 = (TextView) dialog.findViewById(R.id.tv_title);
        btn_accept = (Button) dialog.findViewById(R.id.btn_accept);
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoCoords = new Intent(CoordinatorMenuActivity.this, NavigatorActivity.class);
                gotoCoords.putExtra("PANEL", "IN_PATIENT");
//                gotoCoords.putExtra("NAME", getIntent().getExtras().getString(SearchToCoordinatorMenu.NAME));
//                gotoCoords.putExtra("ID", getIntent().getExtras().getString(SearchToCoordinatorMenu.ID));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.STATUS, getIntent().getExtras().getString(SearchToCoordinatorMenu.STATUS));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.GENDER, getIntent().getExtras().getString(SearchToCoordinatorMenu.GENDER));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.AGE, getIntent().getExtras().getString(SearchToCoordinatorMenu.AGE));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.MEMCODE, getIntent().getExtras().getString(SearchToCoordinatorMenu.MEMCODE));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.TYPE, getIntent().getExtras().getString(SearchToCoordinatorMenu.TYPE));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.EFFECTIVE, getIntent().getExtras().getString(SearchToCoordinatorMenu.EFFECTIVE));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.VALID, getIntent().getExtras().getString(SearchToCoordinatorMenu.VALID));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.DD_LIMIT, getIntent().getExtras().getInt(SearchToCoordinatorMenu.DD_LIMIT));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.OTHER, getIntent().getExtras().getString(SearchToCoordinatorMenu.OTHER));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.PLAN, getIntent().getExtras().getString(SearchToCoordinatorMenu.PLAN));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.BDAY, getIntent().getExtras().getString(SearchToCoordinatorMenu.BDAY));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.COMPANY, getIntent().getExtras().getString(SearchToCoordinatorMenu.COMPANY));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.IS_IN_PATIENT, getIntent().getExtras().getBoolean(SearchToCoordinatorMenu.IS_IN_PATIENT));
//
//                gotoCoords.putExtra(SearchToCoordinatorMenu.REQUEST_CODE_IN_PATIENT, getIntent().getExtras().getString(SearchToCoordinatorMenu.REQUEST_CODE_IN_PATIENT));
                startActivity(gotoCoords);
                Log.d("IS_ADMITTED", getIntent().getExtras().getBoolean(SearchToCoordinatorMenu.IS_IN_PATIENT) + "");

                EventBus.getDefault().post(new AvailServices.MessageEvent(7 + "", "", "", "", "", ""));

            }
        });

        tv_message.setText("The member is Discharge Inpatient");


        dialog.show();


        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.40);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = width;
        dialog.getWindow().setAttributes(lp);


    }


    @Override
    public void dischargeERToOutpatient() {

        System.out.println("PUMASOK DITO SA ER");

//        Intent gotoResult = new Intent(getContext(), ResultActivity.class);
//        gotoResult.putExtra("REQUEST", "ER");;
//        gotoResult.putExtra("ER_REASON", er_reason );
//        gotoResult.putExtra("DATEADMITTED", DateConverter.convertDatetoyyyyMMdd(dateAdmitted));
//
//        startActivity(gotoResult);
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alertshow);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        ci_error_image = (CircleImageView) dialog.findViewById(R.id.ci_error_image);
        tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        tv_title1 = (TextView) dialog.findViewById(R.id.tv_title);
        btn_accept = (Button) dialog.findViewById(R.id.btn_accept);
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoCoords = new Intent(CoordinatorMenuActivity.this, NavigatorActivity.class);

                //      gotoCoords.putStringArrayListExtra(SearchToCoordinatorMenu.APPROVAL_NO, getIntent().getStringArrayListExtra("APPROVAL_NO"));

                gotoCoords.putExtra("PANEL", "OUT_PATIENT");
//                gotoCoords.putExtra("NAME", getIntent().getExtras().getString(SearchToCoordinatorMenu.NAME));
//                gotoCoords.putExtra("ID", getIntent().getExtras().getString(SearchToCoordinatorMenu.ID));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.STATUS, getIntent().getExtras().getString(SearchToCoordinatorMenu.STATUS));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.GENDER, getIntent().getExtras().getString(SearchToCoordinatorMenu.GENDER));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.AGE, getIntent().getExtras().getString(SearchToCoordinatorMenu.AGE));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.MEMCODE, getIntent().getExtras().getString(SearchToCoordinatorMenu.MEMCODE));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.TYPE, getIntent().getExtras().getString(SearchToCoordinatorMenu.TYPE));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.EFFECTIVE, getIntent().getExtras().getString(SearchToCoordinatorMenu.EFFECTIVE));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.VALID, getIntent().getExtras().getString(SearchToCoordinatorMenu.VALID));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.DD_LIMIT, getIntent().getExtras().getInt(SearchToCoordinatorMenu.DD_LIMIT));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.OTHER, getIntent().getExtras().getString(SearchToCoordinatorMenu.OTHER));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.PLAN, getIntent().getExtras().getString(SearchToCoordinatorMenu.PLAN));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.BDAY, getIntent().getExtras().getString(SearchToCoordinatorMenu.BDAY));
//                gotoCoords.putExtra(SearchToCoordinatorMenu.COMPANY, getIntent().getExtras().getString(SearchToCoordinatorMenu.COMPANY));
                startActivity(gotoCoords);
            }
        });

        tv_message.setText("The member is Discharge Outpatient");


        dialog.show();


        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.40);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = width;
        dialog.getWindow().setAttributes(lp);


    }


    @Override
    public void onClickedShowCaseClose(String ORIGIN) {
        switch (ORIGIN) {
            case Constant.FROM_EDIT_IMAGE_ICON:

                Showcase.showCase(new ViewTarget(R.id.tv_loa_req_tag, this), getString(R.string.list_loa_title)
                        , getString(R.string.list_loa_msg), this, caseCallBack, Constant.FROM_VIEW_LOA_LIST);

                break;

            case Constant.FROM_VIEW_LOA_LIST:

                Showcase.showCase(new ViewTarget(R.id.btn_loa, this), getString(R.string.out_patient_title_)
                        , getString(R.string.out_patient_msg), this, caseCallBack, Constant.FROM_IN_PATIENT);


                break;

            case Constant.FROM_IN_PATIENT:

                Showcase.showCase(new ViewTarget(R.id.btn_in_patient, this), getString(R.string.in_patient_title_)
                        , getString(R.string.in_patient_msg), this, caseCallBack, Constant.FROM_ER);

                break;

            case Constant.FROM_ER:

                Showcase.showCase(new ViewTarget(R.id.btn_Er, this), getString(R.string.er_title_)
                        , getString(R.string.er_msg), this, caseCallBack, "NONE");

                break;

            case "NONE":
                SharedPref.setStringValue(SharedPref.USER, SharedPref.COORD_MENU_TUTS, Constant.DONE, context);
                break;


        }
    }
}
