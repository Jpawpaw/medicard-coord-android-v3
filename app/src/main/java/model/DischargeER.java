package model;

/**
 * Created by IPC_Server on 7/28/2017.
 */

public class DischargeER {

    private String id;

    private String username;


    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

}
