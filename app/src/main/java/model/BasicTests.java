package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mpx-pawpaw on 5/9/17.
 */

public class BasicTests implements Parcelable {
    private String id;

    private String procedureCode;

    private String serviceClassCode;

    private String procedureAmount;

    private String procedureDesc;

    private boolean isSelected;

    protected BasicTests(Parcel in) {
        id = in.readString();
        procedureCode = in.readString();
        serviceClassCode = in.readString();
        procedureAmount = in.readString();
        procedureDesc = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(procedureCode);
        dest.writeString(serviceClassCode);
        dest.writeString(procedureAmount);
        dest.writeString(procedureDesc);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BasicTests> CREATOR = new Creator<BasicTests>() {
        @Override
        public BasicTests createFromParcel(Parcel in) {
            return new BasicTests(in);
        }

        @Override
        public BasicTests[] newArray(int size) {
            return new BasicTests[size];
        }
    };

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getProcedureCode ()
    {
        return procedureCode;
    }

    public void setProcedureCode (String procedureCode)
    {
        this.procedureCode = procedureCode;
    }

    public String getServiceClassCode ()
    {
        return serviceClassCode;
    }

    public void setServiceClassCode (String serviceClassCode)
    {
        this.serviceClassCode = serviceClassCode;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getProcedureAmount ()
    {
        return procedureAmount;
    }

    public void setProcedureAmount (String procedureAmount)
    {
        this.procedureAmount = procedureAmount;
    }

    public String getProcedureDesc ()
    {
        return procedureDesc;
    }

    public void setProcedureDesc (String procedureDesc)
    {
        this.procedureDesc = procedureDesc;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", procedureCode = "+procedureCode+", serviceClassCode = "+serviceClassCode+", procedureAmount = "+procedureAmount+", procedureDesc = "+procedureDesc+"]";
    }
}