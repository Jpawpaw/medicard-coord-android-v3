package model;

import android.net.Uri;

/**
 * Created by mpx-pawpaw on 6/7/17.
 */

public class Attachments {

    private Uri data  ;
    private String fileName ;

    public Uri getData() {
        return data;
    }

    public void setData(Uri data) {
        this.data = data;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
