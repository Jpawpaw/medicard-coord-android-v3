package model;

import java.util.ArrayList;

/**
 * Created by IPC on 1/18/2018.
 */

public class ExclusionModel {
    private ArrayList<String> exclusions;

    public ArrayList<String> getExclusions() {
        return exclusions;
    }

    public void setExclusions(ArrayList<String> exclusions) {
        this.exclusions = exclusions;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [exclusions = "+ exclusions+"]";
    }
}
