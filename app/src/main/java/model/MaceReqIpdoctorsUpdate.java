package model;

/**
 * Created by macbookpro on 7/11/17.
 */

public class MaceReqIpdoctorsUpdate {

    private String accredited;

    private String docFname;

    private String transactionId;

    private String doctorCode;

    private String docMname;

    private String addedOn;

    private String docHospId;

    private String maceDoctype;

    private String refDiagprocId;

    private String isaccredited;

    private String maceRequestId;

    private String docLname;

    private String hospitalCode;

    private String hospProfFee;

    private String defProfFee;

    private String actualProfFee;

    private String ipReqdocId;

    private String addedBy;

    public MaceReqIpdoctorsUpdate() {

    }

    public String getAccredited() {
        return accredited;
    }

    public void setAccredited(String accredited) {
        this.accredited = accredited;
    }

    public String getDocFname() {
        return docFname;
    }

    public void setDocFname(String docFname) {
        this.docFname = docFname;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(String doctorCode) {
        this.doctorCode = doctorCode;
    }

    public String getDocMname() {
        return docMname;
    }

    public void setDocMname(String docMname) {
        this.docMname = docMname;
    }

    public String getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(String addedOn) {
        this.addedOn = addedOn;
    }

    public String getDocHospId() {
        return docHospId;
    }

    public void setDocHospId(String docHospId) {
        this.docHospId = docHospId;
    }

    public String getMaceDoctype() {
        return maceDoctype;
    }

    public void setMaceDoctype(String maceDoctype) {
        this.maceDoctype = maceDoctype;
    }

    public String getRefDiagprocId() {
        return refDiagprocId;
    }

    public void setRefDiagprocId(String refDiagprocId) {
        this.refDiagprocId = refDiagprocId;
    }

    public String getIsaccredited() {
        return isaccredited;
    }

    public void setIsaccredited(String isaccredited) {
        this.isaccredited = isaccredited;
    }

    public String getMaceRequestId() {
        return maceRequestId;
    }

    public void setMaceRequestId(String maceRequestId) {
        this.maceRequestId = maceRequestId;
    }

    public String getDocLname() {
        return docLname;
    }

    public void setDocLname(String docLname) {
        this.docLname = docLname;
    }

    public String getHospitalCode() {
        return hospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
    }

    public String getHospProfFee() {
        return hospProfFee;
    }

    public void setHospProfFee(String hospProfFee) {
        this.hospProfFee = hospProfFee;
    }

    public String getDefProfFee() {
        return defProfFee;
    }

    public void setDefProfFee(String defProfFee) {
        this.defProfFee = defProfFee;
    }

    public String getActualProfFee() {
        return actualProfFee;
    }

    public void setActualProfFee(String actualProfFee) {
        this.actualProfFee = actualProfFee;
    }

    public String getIpReqdocId() {
        return ipReqdocId;
    }

    public void setIpReqdocId(String ipReqdocId) {
        this.ipReqdocId = ipReqdocId;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }
}
