package model;

import java.util.ArrayList;

/**
 * Created by mpx-pawpaw on 11/22/16.
 */

public class DiagnosisModel {

    private ArrayList<DiagnosisList> diagnosisList;

    public ArrayList<DiagnosisList> getDiagnosisList ()
    {
        return diagnosisList;
    }

    public void setDiagnosisList (ArrayList<DiagnosisList> diagnosisList)
    {
        this.diagnosisList = diagnosisList;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [diagnosisList = "+diagnosisList+"]";
    }
}
