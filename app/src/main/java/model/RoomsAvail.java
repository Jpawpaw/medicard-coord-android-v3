package model;

import java.util.ArrayList;

/**
 * Created by mpx-pawpaw on 6/14/17.
 */

public class RoomsAvail {

    private ArrayList<Rooms> rooms;

    public ArrayList<Rooms> getRooms() {
        return rooms;
    }

    public void setRooms(ArrayList<Rooms> rooms) {
        this.rooms = rooms;
    }

    @Override
    public String toString() {
        return "ClassPojo [rooms = " + rooms + "]";
    }
}
