package model;

/**
 * Created by mpx-pawpaw on 6/27/17.
 */

public class RoomCategory {
    private String responseCode;

    private String[] categories;

    public String getResponseCode ()
    {
        return responseCode;
    }

    public void setResponseCode (String responseCode)
    {
        this.responseCode = responseCode;
    }

    public String[] getCategories ()
    {
        return categories;
    }

    public void setCategories (String[] categories)
    {
        this.categories = categories;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [responseCode = "+responseCode+", categories = "+categories+"]";
    }
}

