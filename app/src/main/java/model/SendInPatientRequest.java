package model;

import java.util.List;

/**
 * Created by mpx-pawpaw on 11/28/16.
 */

public class SendInPatientRequest {

    private String appUsername;
    private String memberCode;
    private String hospitalCode;
    private List<String> doctorCodes;
    private List<String> diagCodes;
    private List<String> procCodes;
    private String roomRate;
    private String roomNo;
    private String dateTimeAdmitted;
    private String coorContact;
    private String coorEmail;
    private String searchType;
    private String category;
    private String roomType;
    private String deviceId;
    private String requestOrigin;



    public String getAppUsername() {
        return appUsername;
    }

    public void setAppUsername(String appUsername) {
        this.appUsername = appUsername;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getHospitalCode() {
        return hospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
    }

    public List<String> getDoctorCodes() {
        return doctorCodes;
    }

    public void setDoctorCodes(List<String> doctorCodes) {
        this.doctorCodes = doctorCodes;
    }

    public List<String> getDiagCodes() {
        return diagCodes;
    }

    public void setDiagCodes(List<String> diagCodes) {
        this.diagCodes = diagCodes;
    }

    public List<String> getProcCodes() {
        return procCodes;
    }

    public void setProcCodes(List<String> procCodes) {
        this.procCodes = procCodes;
    }

    public String getRoomRate() {
        return roomRate;
    }

    public void setRoomRate(String roomRate) {
        this.roomRate = roomRate;
    }

    public String getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(String roomNo) {
        this.roomNo = roomNo;
    }

    public String getDateTimeAdmitted() {
        return dateTimeAdmitted;
    }

    public void setDateTimeAdmitted(String dateTimeAdmitted) {
        this.dateTimeAdmitted = dateTimeAdmitted;
    }

    public String getCoorContact() {
        return coorContact;
    }

    public void setCoorContact(String coorContact) {
        this.coorContact = coorContact;
    }

    public String getCoorEmail() {
        return coorEmail;
    }

    public void setCoorEmail(String coorEmail) {
        this.coorEmail = coorEmail;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getRequestOrigin() {
        return requestOrigin;
    }

    public void setRequestOrigin(String requestOrigin) {
        this.requestOrigin = requestOrigin;
    }

    @Override
    public String toString() {
        return "ClassPojo [appUsername = " + appUsername
                + ", memberCode = " + memberCode
                + ", hospitalCode = " + hospitalCode
                + ", doctorCodes = " + doctorCodes
                + ", diagCodes = " + diagCodes
                + ", procCodes = " + procCodes
                + ", roomRate = " + roomRate
                + ", roomNo = " + roomNo
                + ", dateTimeAdmitted = " + dateTimeAdmitted
                + ", coorContact = " + coorContact
                + ", coorEmail = " + coorEmail
                + ", searchType = " + searchType
                + ", category = " + category
                + ", roomType = " + roomType
                + ", deviceId = " + deviceId
                + "]";
    }
}

