package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by mpx-pawpaw on 5/7/17.
 */

public class Procedure {
    private String username;

    private ArrayList<ProcedureList> procedureList;

    private String doctorCode;

    private String diagnosisDesc;

    private String deviceID;

    private String hospitalCode;

    private String searchType;

    private String diagnosisCode;

    private String memberCode;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ArrayList<ProcedureList> getProcedureList() {
        return procedureList;
    }

    public void setProcedureList(ArrayList<ProcedureList> procedureList) {
        this.procedureList = procedureList;
    }

    public String getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(String doctorCode) {
        this.doctorCode = doctorCode;
    }

    public String getDiagnosisDesc() {
        return diagnosisDesc;
    }

    public void setDiagnosisDesc(String diagnosisDesc) {
        this.diagnosisDesc = diagnosisDesc;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getHospitalCode() {
        return hospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public String getDiagnosisCode() {
        return diagnosisCode;
    }

    public void setDiagnosisCode(String diagnosisCode) {
        this.diagnosisCode = diagnosisCode;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    @Override
    public String toString() {
        return "ClassPojo [username = " + username + ", procedureList = " + procedureList + ", doctorCode = " + doctorCode + ", diagnosisDesc = " + diagnosisDesc + ", deviceID = " + deviceID + ", hospitalCode = " + hospitalCode + ", searchType = " + searchType + ", diagnosisCode = " + diagnosisCode + ", memberCode = " + memberCode + "]";
    }
}

