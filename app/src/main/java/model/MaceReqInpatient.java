package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mpx-pawpaw on 6/19/17.
 */

public class MaceReqInpatient implements Parcelable {

    private String disapprovedOn;

    private String lastupdateBy;

    private String dxRemarks;

    private String ddRemlimit;

    private String transactionId;

    private String ipReasonRemarks;

    private String primaryDiagnosisIcd10;

    private String diseaseType;

    private String docHospId;

    private String estimatedCost;

    private String approvedBy;

    private String dischargeupdateBy;

    private String maceRequest;

    private String statusRemarks;

    private String maceRequestId;

    private String pecLimit;

    private String roomratePerday;

    private String disapprovedBy;

    private String approvalRemarks;

    private String lastupdateOn;

    private String recordSubmittedBy;

    private String disposition;

    private String admittedOn;

    private String primaryDiagnosisCode;

    private String ddLimit;

    private String doctorCode;

    private String status;

    private String pecRemlimit;

    private String roomNo;

    private String dispRemarks;

    private String primaryDiagnosisDesc;

    private String disapprovalReason;

    private String dischargedOn;

    private String approvedOn;

    private String requestFrom;

    private String loaNo;

    private String validto;

    private String disapprovalRemarks;

    private String recordSubmittedOn;

    private String hospitalCode;

    private String roomboard;

    private String validfrom;

    private String transCode;

    protected MaceReqInpatient(Parcel in) {
        disapprovedOn = in.readString();
        lastupdateBy = in.readString();
        dxRemarks = in.readString();
        ddRemlimit = in.readString();
        transactionId = in.readString();
        ipReasonRemarks = in.readString();
        primaryDiagnosisIcd10 = in.readString();
        diseaseType = in.readString();
        docHospId = in.readString();
        estimatedCost = in.readString();
        approvedBy = in.readString();
        dischargeupdateBy = in.readString();
        maceRequest = in.readString();
        statusRemarks = in.readString();
        maceRequestId = in.readString();
        pecLimit = in.readString();
        roomratePerday = in.readString();
        disapprovedBy = in.readString();
        approvalRemarks = in.readString();
        lastupdateOn = in.readString();
        recordSubmittedBy = in.readString();
        disposition = in.readString();
        admittedOn = in.readString();
        primaryDiagnosisCode = in.readString();
        ddLimit = in.readString();
        doctorCode = in.readString();
        status = in.readString();
        pecRemlimit = in.readString();
        roomNo = in.readString();
        dispRemarks = in.readString();
        primaryDiagnosisDesc = in.readString();
        disapprovalReason = in.readString();
        dischargedOn = in.readString();
        approvedOn = in.readString();
        requestFrom = in.readString();
        loaNo = in.readString();
        validto = in.readString();
        disapprovalRemarks = in.readString();
        recordSubmittedOn = in.readString();
        hospitalCode = in.readString();
        roomboard = in.readString();
        validfrom = in.readString();
        transCode = in.readString();
    }

    public static final Creator<MaceReqInpatient> CREATOR = new Creator<MaceReqInpatient>() {
        @Override
        public MaceReqInpatient createFromParcel(Parcel in) {
            return new MaceReqInpatient(in);
        }

        @Override
        public MaceReqInpatient[] newArray(int size) {
            return new MaceReqInpatient[size];
        }
    };

    public String getDisapprovedOn() {
        return disapprovedOn;
    }

    public void setDisapprovedOn(String disapprovedOn) {
        this.disapprovedOn = disapprovedOn;
    }

    public String getLastupdateBy() {
        return lastupdateBy;
    }

    public void setLastupdateBy(String lastupdateBy) {
        this.lastupdateBy = lastupdateBy;
    }

    public String

    getDxRemarks() {
        return dxRemarks;
    }

    public void setDxRemarks(String dxRemarks) {
        this.dxRemarks = dxRemarks;
    }

    public String

    getDdRemlimit() {
        return ddRemlimit;
    }

    public void setDdRemlimit(String ddRemlimit) {
        this.ddRemlimit = ddRemlimit;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String

    getIpReasonRemarks() {
        return ipReasonRemarks;
    }

    public void setIpReasonRemarks(String ipReasonRemarks) {
        this.ipReasonRemarks = ipReasonRemarks;
    }

    public String

    getPrimaryDiagnosisIcd10() {
        return primaryDiagnosisIcd10;
    }

    public void setPrimaryDiagnosisIcd10(String primaryDiagnosisIcd10) {
        this.primaryDiagnosisIcd10 = primaryDiagnosisIcd10;
    }

    public String

    getDiseaseType() {
        return diseaseType;
    }

    public void setDiseaseType(String diseaseType) {
        this.diseaseType = diseaseType;
    }

    public String getDocHospId() {
        return docHospId;
    }

    public void setDocHospId(String docHospId) {
        this.docHospId = docHospId;
    }

    public String

    getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(String estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String

    getDischargeupdateBy() {
        return dischargeupdateBy;
    }

    public void setDischargeupdateBy(String dischargeupdateBy) {
        this.dischargeupdateBy = dischargeupdateBy;
    }

    public String

    getMaceRequest() {
        return maceRequest;
    }

    public void setMaceRequest(String maceRequest) {
        this.maceRequest = maceRequest;
    }

    public String getStatusRemarks() {
        return statusRemarks;
    }

    public void setStatusRemarks(String statusRemarks) {
        this.statusRemarks = statusRemarks;
    }

    public String getMaceRequestId() {
        return maceRequestId;
    }

    public void setMaceRequestId(String maceRequestId) {
        this.maceRequestId = maceRequestId;
    }

    public String

    getPecLimit() {
        return pecLimit;
    }

    public void setPecLimit(String pecLimit) {
        this.pecLimit = pecLimit;
    }

    public String getRoomratePerday() {
        return roomratePerday;
    }

    public void setRoomratePerday(String roomratePerday) {
        this.roomratePerday = roomratePerday;
    }

    public String

    getDisapprovedBy() {
        return disapprovedBy;
    }

    public void setDisapprovedBy(String disapprovedBy) {
        this.disapprovedBy = disapprovedBy;
    }

    public String getApprovalRemarks() {
        return approvalRemarks;
    }

    public void setApprovalRemarks(String approvalRemarks) {
        this.approvalRemarks = approvalRemarks;
    }

    public String

    getLastupdateOn() {
        return lastupdateOn;
    }

    public void setLastupdateOn(String lastupdateOn) {
        this.lastupdateOn = lastupdateOn;
    }

    public String

    getRecordSubmittedBy() {
        return recordSubmittedBy;
    }

    public void setRecordSubmittedBy(String recordSubmittedBy) {
        this.recordSubmittedBy = recordSubmittedBy;
    }

    public String

    getDisposition() {
        return disposition;
    }

    public void setDisposition(String disposition) {
        this.disposition = disposition;
    }

    public String getAdmittedOn() {
        return admittedOn;
    }

    public void setAdmittedOn(String admittedOn) {
        this.admittedOn = admittedOn;
    }

    public String getPrimaryDiagnosisCode() {
        return primaryDiagnosisCode;
    }

    public void setPrimaryDiagnosisCode(String primaryDiagnosisCode) {
        this.primaryDiagnosisCode = primaryDiagnosisCode;
    }

    public String

    getDdLimit() {
        return ddLimit;
    }

    public void setDdLimit(String ddLimit) {
        this.ddLimit = ddLimit;
    }

    public String getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(String doctorCode) {
        this.doctorCode = doctorCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String

    getPecRemlimit() {
        return pecRemlimit;
    }

    public void setPecRemlimit(String pecRemlimit) {
        this.pecRemlimit = pecRemlimit;
    }

    public String getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(String roomNo) {
        this.roomNo = roomNo;
    }

    public String

    getDispRemarks() {
        return dispRemarks;
    }

    public void setDispRemarks(String dispRemarks) {
        this.dispRemarks = dispRemarks;
    }

    public String getPrimaryDiagnosisDesc() {
        return primaryDiagnosisDesc;
    }

    public void setPrimaryDiagnosisDesc(String primaryDiagnosisDesc) {
        this.primaryDiagnosisDesc = primaryDiagnosisDesc;
    }

    public String

    getDisapprovalReason() {
        return disapprovalReason;
    }

    public void setDisapprovalReason(String disapprovalReason) {
        this.disapprovalReason = disapprovalReason;
    }

    public String

    getDischargedOn() {
        return dischargedOn;
    }

    public void setDischargedOn(String dischargedOn) {
        this.dischargedOn = dischargedOn;
    }

    public String getApprovedOn() {
        return approvedOn;
    }

    public void setApprovedOn(String approvedOn) {
        this.approvedOn = approvedOn;
    }

    public String getRequestFrom() {
        return requestFrom;
    }

    public void setRequestFrom(String requestFrom) {
        this.requestFrom = requestFrom;
    }

    public String

    getLoaNo() {
        return loaNo;
    }

    public void setLoaNo(String loaNo) {
        this.loaNo = loaNo;
    }

    public String getValidto() {
        return validto;
    }

    public void setValidto(String validto) {
        this.validto = validto;
    }

    public String

    getDisapprovalRemarks() {
        return disapprovalRemarks;
    }

    public void setDisapprovalRemarks(String disapprovalRemarks) {
        this.disapprovalRemarks = disapprovalRemarks;
    }

    public String

    getRecordSubmittedOn() {
        return recordSubmittedOn;
    }

    public void setRecordSubmittedOn(String recordSubmittedOn) {
        this.recordSubmittedOn = recordSubmittedOn;
    }

    public String getHospitalCode() {
        return hospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
    }

    public String getRoomboard() {
        return roomboard;
    }

    public void setRoomboard(String roomboard) {
        this.roomboard = roomboard;
    }

    public String getValidfrom() {
        return validfrom;
    }

    public void setValidfrom(String validfrom) {
        this.validfrom = validfrom;
    }

    public String

    getTransCode() {
        return transCode;
    }

    public void setTransCode(String transCode) {
        this.transCode = transCode;
    }

    @Override
    public String toString() {
        return "ClassPojo [disapprovedOn = " + disapprovedOn + ", lastupdateBy = " + lastupdateBy + ", dxRemarks = " + dxRemarks + ", ddRemlimit = " + ddRemlimit + ", transactionId = " + transactionId + ", ipReasonRemarks = " + ipReasonRemarks + ", primaryDiagnosisIcd10 = " + primaryDiagnosisIcd10 + ", diseaseType = " + diseaseType + ", docHospId = " + docHospId + ", estimatedCost = " + estimatedCost + ", approvedBy = " + approvedBy + ", dischargeupdateBy = " + dischargeupdateBy + ", maceRequest = " + maceRequest + ", statusRemarks = " + statusRemarks + ", maceRequestId = " + maceRequestId + ", pecLimit = " + pecLimit + ", roomratePerday = " + roomratePerday + ", disapprovedBy = " + disapprovedBy + ", approvalRemarks = " + approvalRemarks + ", lastupdateOn = " + lastupdateOn + ", recordSubmittedBy = " + recordSubmittedBy + ", disposition = " + disposition + ", admittedOn = " + admittedOn + ", primaryDiagnosisCode = " + primaryDiagnosisCode + ", ddLimit = " + ddLimit + ", doctorCode = " + doctorCode + ", status = " + status + ", pecRemlimit = " + pecRemlimit + ", roomNo = " + roomNo + ", dispRemarks = " + dispRemarks + ", primaryDiagnosisDesc = " + primaryDiagnosisDesc + ", disapprovalReason = " + disapprovalReason + ", dischargedOn = " + dischargedOn + ", approvedOn = " + approvedOn + ", requestFrom = " + requestFrom + ", loaNo = " + loaNo + ", validto = " + validto + ", disapprovalRemarks = " + disapprovalRemarks + ", recordSubmittedOn = " + recordSubmittedOn + ", hospitalCode = " + hospitalCode + ", roomboard = " + roomboard + ", validfrom = " + validfrom + ", transCode = " + transCode + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(disapprovedOn);
        parcel.writeString(lastupdateBy);
        parcel.writeString(dxRemarks);
        parcel.writeString(ddRemlimit);
        parcel.writeString(transactionId);
        parcel.writeString(ipReasonRemarks);
        parcel.writeString(primaryDiagnosisIcd10);
        parcel.writeString(diseaseType);
        parcel.writeString(docHospId);
        parcel.writeString(estimatedCost);
        parcel.writeString(approvedBy);
        parcel.writeString(dischargeupdateBy);
        parcel.writeString(maceRequest);
        parcel.writeString(statusRemarks);
        parcel.writeString(maceRequestId);
        parcel.writeString(pecLimit);
        parcel.writeString(roomratePerday);
        parcel.writeString(disapprovedBy);
        parcel.writeString(approvalRemarks);
        parcel.writeString(lastupdateOn);
        parcel.writeString(recordSubmittedBy);
        parcel.writeString(disposition);
        parcel.writeString(admittedOn);
        parcel.writeString(primaryDiagnosisCode);
        parcel.writeString(ddLimit);
        parcel.writeString(doctorCode);
        parcel.writeString(status);
        parcel.writeString(pecRemlimit);
        parcel.writeString(roomNo);
        parcel.writeString(dispRemarks);
        parcel.writeString(primaryDiagnosisDesc);
        parcel.writeString(disapprovalReason);
        parcel.writeString(dischargedOn);
        parcel.writeString(approvedOn);
        parcel.writeString(requestFrom);
        parcel.writeString(loaNo);
        parcel.writeString(validto);
        parcel.writeString(disapprovalRemarks);
        parcel.writeString(recordSubmittedOn);
        parcel.writeString(hospitalCode);
        parcel.writeString(roomboard);
        parcel.writeString(validfrom);
        parcel.writeString(transCode);
    }
}


