package model;

import java.util.ArrayList;

/**
 * Created by IPC on 10/6/2017.
 */

public class AnnouncementDataJson {

    private String responseCode;
    private ArrayList<AnnouncementDataModel> announcements;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public ArrayList<AnnouncementDataModel> getAnnouncements() {
        return announcements;
    }

    public void setAnnouncements(ArrayList<AnnouncementDataModel> announcements) {
        this.announcements = announcements;
    }

    public class AnnouncementDataModel {
        private String messageHeader;
        private String messageContent;
        private String datePosted;
        private String postedBy;
        private String updatedOn;
        private String updatedBy;
        private String messageType;
        private String isClicked;

        public AnnouncementDataModel(String messageHeader, String messageContent, String datePosted, String postedBy, String updatedOn, String updatedBy, String messageType, String isClicked) {
            this.messageHeader = messageHeader;
            this.messageContent = messageContent;
            this.datePosted = datePosted;
            this.postedBy = postedBy;
            this.updatedOn = updatedOn;
            this.updatedBy = updatedBy;
            this.messageType = messageType;
            this.isClicked = isClicked;
        }

        public AnnouncementDataModel() {
        }

        public String getIsClicked() {
            return isClicked;
        }

        public void setIsClicked(String isClicked) {
            this.isClicked = isClicked;
        }

        public String getMessageHeader() {
            return messageHeader;
        }

        public void setMessageHeader(String messageHeader) {
            this.messageHeader = messageHeader;
        }

        public String getMessageContent() {
            return messageContent;
        }

        public void setMessageContent(String messageContent) {
            this.messageContent = messageContent;
        }

        public String getDatePosted() {
            return datePosted;
        }

        public void setDatePosted(String datePosted) {
            this.datePosted = datePosted;
        }

        public String getPostedBy() {
            return postedBy;
        }

        public void setPostedBy(String postedBy) {
            this.postedBy = postedBy;
        }

        public String getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
        }

        public String getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(String updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getMessageType() {
            return messageType;
        }

        public void setMessageType(String messageType) {
            this.messageType = messageType;
        }

        @Override
        public String toString() {
            return "ClassPojo [messageHeader = " + messageHeader + ", messageContent = " + messageContent + ", datePosted = " + datePosted + ", postedBy = " + postedBy + ", updatedOn = " + updatedOn + ", updatedBy = " + updatedBy +", messageType = " + messageType + "]";
        }
    }
}
