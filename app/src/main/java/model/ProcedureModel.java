package model;

import java.util.ArrayList;

/**
 * Created by mpx-pawpaw on 11/25/16.
 */

public class ProcedureModel {


    private ArrayList<TestsAndProcedures> testsAndProcedures;

    public ArrayList<TestsAndProcedures> getProceduresListLocal() {
        return testsAndProcedures;
    }

    public void setProceduresListLocal(ArrayList<TestsAndProcedures> proceduresListLocal) {
        this.testsAndProcedures = proceduresListLocal;
    }

    @Override
    public String toString() {
        return "ClassPojo [proceduresList = " + testsAndProcedures + "]";
    }

}
