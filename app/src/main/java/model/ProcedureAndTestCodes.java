package model;

/**
 * Created by mpx-pawpaw on 6/22/17.
 */

public class ProcedureAndTestCodes {

    private String amount;

    private String procCode;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getProcCode() {
        return procCode;
    }

    public void setProcCode(String procCode) {
        this.procCode = procCode;
    }

    @Override
    public String toString() {
        return "ClassPojo [amount = " + amount + ", procCode = " + procCode + "]";
    }
}
