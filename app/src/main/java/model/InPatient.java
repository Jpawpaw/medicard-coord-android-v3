package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by mpx-pawpaw on 6/19/17.
 */

public class InPatient implements Serializable {

    private MaceReqInpatient maceReqInpatient;

    private String responseDesc;

    private MaceReqIpRoom maceReqIpRoom;

    private MaceReq maceReq;

    private ArrayList<MaceReqIpDoctors> maceReqIpDoctors;

    private ArrayList<MaceReqIpDiags> maceReqIpDiags;

    protected InPatient(Parcel in) {
        maceReqInpatient = in.readParcelable(MaceReqInpatient.class.getClassLoader());
        responseDesc = in.readString();
        maceReqIpRoom = in.readParcelable(MaceReqIpRoom.class.getClassLoader());
        maceReq = in.readParcelable(MaceReq.class.getClassLoader());
        maceReqIpDoctors = in.createTypedArrayList(MaceReqIpDoctors.CREATOR);
        maceReqIpDiags = in.createTypedArrayList(MaceReqIpDiags.CREATOR);
    }

    /*public static final Creator<InPatient> CREATOR = new Creator<InPatient>() {
        @Override
        public InPatient createFromParcel(Parcel in) {
            return new InPatient(in);
        }

        @Override
        public InPatient[] newArray(int size) {
            return new InPatient[size];
        }
    };*/

    public MaceReqInpatient getMaceReqInpatient() {
        return maceReqInpatient;
    }

    public void setMaceReqInpatient(MaceReqInpatient maceReqInpatient) {
        this.maceReqInpatient = maceReqInpatient;
    }

    public String getResponseDesc() {
        return responseDesc;
    }

    public void setResponseDesc(String responseDesc) {
        this.responseDesc = responseDesc;
    }

    public MaceReqIpRoom getMaceReqIpRoom() {
        return maceReqIpRoom;
    }

    public void setMaceReqIpRoom(MaceReqIpRoom maceReqIpRoom) {
        this.maceReqIpRoom = maceReqIpRoom;
    }

    public MaceReq getMaceReq() {
        return maceReq;
    }

    public void setMaceReq(MaceReq maceReq) {
        this.maceReq = maceReq;
    }

    public ArrayList<MaceReqIpDoctors> getMaceReqIpDoctors() {
        return maceReqIpDoctors;
    }

    public void setMaceReqIpDoctors(ArrayList<MaceReqIpDoctors> maceReqIpDoctors) {
        this.maceReqIpDoctors = maceReqIpDoctors;
    }

    public ArrayList<MaceReqIpDiags> getMaceReqIpDiags() {
        return maceReqIpDiags;
    }

    public void setMaceReqIpDiags(ArrayList<MaceReqIpDiags> maceReqIpDiags) {
        this.maceReqIpDiags = maceReqIpDiags;
    }

    @Override
    public String toString() {
        return "ClassPojo [maceReqInpatient = " + maceReqInpatient + ", responseDesc = " + responseDesc + ", maceReqIpRoom = " + maceReqIpRoom + ", maceReq = " + maceReq + ", maceReqIpDoctors = " + maceReqIpDoctors + ", maceReqIpDiags = " + maceReqIpDiags + "]";
    }

    /*@Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(maceReqInpatient, i);
        parcel.writeString(responseDesc);
        parcel.writeParcelable(maceReqIpRoom, i);
        parcel.writeParcelable(maceReq, i);
        parcel.writeTypedList(maceReqIpDoctors);
        parcel.writeTypedList(maceReqIpDiags);
    }
}*/
}

