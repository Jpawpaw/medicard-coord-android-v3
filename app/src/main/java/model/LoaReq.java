package model;

import java.util.ArrayList;

/**
 * Created by mpx-pawpaw on 3/3/17.
 */

public class LoaReq {
    private ArrayList<LoaList> loaList;

    public ArrayList<LoaList> getLoaList ()
    {
        return loaList;
    }

    public void setLoaList (ArrayList<LoaList> loaList)
    {
        this.loaList = loaList;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [loaList = "+loaList+"]";
    }
}
