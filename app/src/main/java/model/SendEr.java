package model;

/**
 * Created by mpx-pawpaw on 12/23/16.
 */

public class SendEr {

    private String dateAdmitted;

    private String erReason;

    private String hospitalCode;

    private String id;

    private String memberCode;

    private String memberName;

    private String username;
    private String coorContact;

    private String coorEmail;

    public String getCoorContact() {
        return coorContact;
    }

    public void setCoorContact(String coorContact) {
        this.coorContact = coorContact;
    }

    public String getCoorEmail() {
        return coorEmail;
    }

    public void setCoorEmail(String coorEmail) {
        this.coorEmail = coorEmail;
    }
//not needed for Sprint 6

//    private String diagnosisCode;
//
//    private String deviceId;
//
//    private String procedureCode;
//
//    private String hospitalName;
//
//    private String doctorCode;

    public String getUsername ()
    {
        return username;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }

    public String getHospitalCode ()
    {
        return hospitalCode;
    }

    public void setHospitalCode (String hospitalCode)
    {
        this.hospitalCode = hospitalCode;
    }

    public String getMemberName ()
    {
        return memberName;
    }

    public void setMemberName (String memberName)
    {
        this.memberName = memberName;
    }

    public String getMemberCode ()
    {
        return memberCode;
    }

    public void setMemberCode (String memberCode)
    {
        this.memberCode = memberCode;
    }

    public String getErReason() { return erReason; }

    public void setErReason(String erReason) { this.erReason = erReason; }

    public String getDateAdmitted() { return dateAdmitted; }

    public void setDateAdmitted(String dateAdmitted) { this.dateAdmitted = dateAdmitted; }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    //not needed for Sprint 6
//
//    public String getDiagnosisCode ()
//    {
//        return diagnosisCode;
//    }
//
//    public void setDiagnosisCode (String diagnosisCode)
//    {
//        this.diagnosisCode = diagnosisCode;
//    }
//
//    public String getDeviceId ()
//    {
//        return deviceId;
//    }
//
//    public void setDeviceId (String deviceId)
//    {
//        this.deviceId = deviceId;
//    }

//    public String getProcedureCode ()
//    {
//        return procedureCode;
//    }
//
//    public void setProcedureCode (String procedureCode)
//    {
//        this.procedureCode = procedureCode;
//    }
//
//    public String getDoctorCode ()
//    {
//        return doctorCode;
//    }
//
//    public void setDoctorCode (String doctorCode)
//    {
//        this.doctorCode = doctorCode;
//    }
//
//    public String getHospitalName ()
//    {
//        return hospitalName;
//    }
//
//    public void setHospitalName (String hospitalName)
//    {
//        this.hospitalName = hospitalName;
//    }

    @Override
    public String toString()
    {
        return "ClassPojo [username = "+username+
//                ", procedureCode = "+procedureCode+
//                ", doctorCode = "+doctorCode+
//                ", hospitalName = "+hospitalName+
                ", hospitalCode = "+hospitalCode+
                ", memberName = "+memberName+
//                ", diagnosisCode = "+diagnosisCode+
//                ", deviceId = "+deviceId+
                ", memberCode = "+ memberCode +
//                ", reasonER = "+ reasonER+
                ", dateAdmitted = "+ dateAdmitted +
                "]";
    }
}
