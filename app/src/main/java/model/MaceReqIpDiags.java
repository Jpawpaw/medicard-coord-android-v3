package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mpx-pawpaw on 6/19/17.
 */

public class MaceReqIpDiags implements Parcelable
{
    private String procGroupDesc;

    private String transactionId;

    private String diagClass;

    private String diagTypeOld;

    private String maceDiagType;

    private String diseaseType;

    private String ipReqdiagId;

    private String typeDesc;

    private String icd10Code;

    private String maceRequestId;

    private String icd104C;

    private String diagCode;

    private String icd10Class;

    private String diagType;

    private String diagDesc;

    private String diagRemarks;

    protected MaceReqIpDiags(Parcel in) {
        procGroupDesc = in.readString();
        transactionId = in.readString();
        diagClass = in.readString();
        diagTypeOld = in.readString();
        maceDiagType = in.readString();
        diseaseType = in.readString();
        ipReqdiagId = in.readString();
        typeDesc = in.readString();
        icd10Code = in.readString();
        maceRequestId = in.readString();
        icd104C = in.readString();
        diagCode = in.readString();
        icd10Class = in.readString();
        diagType = in.readString();
        diagDesc = in.readString();
        diagRemarks = in.readString();
    }

    public static final Creator<MaceReqIpDiags> CREATOR = new Creator<MaceReqIpDiags>() {
        @Override
        public MaceReqIpDiags createFromParcel(Parcel in) {
            return new MaceReqIpDiags(in);
        }

        @Override
        public MaceReqIpDiags[] newArray(int size) {
            return new MaceReqIpDiags[size];
        }
    };

    public String getProcGroupDesc ()
{
    return procGroupDesc;
}

    public void setProcGroupDesc (String procGroupDesc)
    {
        this.procGroupDesc = procGroupDesc;
    }

    public String getTransactionId ()
    {
        return transactionId;
    }

    public void setTransactionId (String transactionId)
    {
        this.transactionId = transactionId;
    }

    public String getDiagClass ()
    {
        return diagClass;
    }

    public void setDiagClass (String diagClass)
    {
        this.diagClass = diagClass;
    }

    public String getDiagTypeOld ()
{
    return diagTypeOld;
}

    public void setDiagTypeOld (String diagTypeOld)
    {
        this.diagTypeOld = diagTypeOld;
    }

    public String getMaceDiagType ()
{
    return maceDiagType;
}

    public void setMaceDiagType (String maceDiagType)
    {
        this.maceDiagType = maceDiagType;
    }

    public String getDiseaseType ()
    {
        return diseaseType;
    }

    public void setDiseaseType (String diseaseType)
    {
        this.diseaseType = diseaseType;
    }

    public String getIpReqdiagId ()
    {
        return ipReqdiagId;
    }

    public void setIpReqdiagId (String ipReqdiagId)
    {
        this.ipReqdiagId = ipReqdiagId;
    }

    public String getTypeDesc ()
    {
        return typeDesc;
    }

    public void setTypeDesc (String typeDesc)
    {
        this.typeDesc = typeDesc;
    }

    public String getIcd10Code ()
    {
        return icd10Code;
    }

    public void setIcd10Code (String icd10Code)
    {
        this.icd10Code = icd10Code;
    }

    public String getMaceRequestId ()
    {
        return maceRequestId;
    }

    public void setMaceRequestId (String maceRequestId)
    {
        this.maceRequestId = maceRequestId;
    }

    public String getIcd104C ()
    {
        return icd104C;
    }

    public void setIcd104C (String icd104C)
    {
        this.icd104C = icd104C;
    }

    public String getDiagCode ()
    {
        return diagCode;
    }

    public void setDiagCode (String diagCode)
    {
        this.diagCode = diagCode;
    }

    public String getIcd10Class ()
    {
        return icd10Class;
    }

    public void setIcd10Class (String icd10Class)
    {
        this.icd10Class = icd10Class;
    }

    public String getDiagType ()
    {
        return diagType;
    }

    public void setDiagType (String diagType)
    {
        this.diagType = diagType;
    }

    public String getDiagDesc ()
    {
        return diagDesc;
    }

    public void setDiagDesc (String diagDesc)
    {
        this.diagDesc = diagDesc;
    }

    public String getDiagRemarks ()
    {
        return diagRemarks;
    }

    public void setDiagRemarks (String diagRemarks)
    {
        this.diagRemarks = diagRemarks;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [procGroupDesc = "+procGroupDesc+", transactionId = "+transactionId+", diagClass = "+diagClass+", diagTypeOld = "+diagTypeOld+", maceDiagType = "+maceDiagType+", diseaseType = "+diseaseType+", ipReqdiagId = "+ipReqdiagId+", typeDesc = "+typeDesc+", icd10Code = "+icd10Code+", maceRequestId = "+maceRequestId+", icd104C = "+icd104C+", diagCode = "+diagCode+", icd10Class = "+icd10Class+", diagType = "+diagType+", diagDesc = "+diagDesc+", diagRemarks = "+diagRemarks+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(procGroupDesc);
        parcel.writeString(transactionId);
        parcel.writeString(diagClass);
        parcel.writeString(diagTypeOld);
        parcel.writeString(maceDiagType);
        parcel.writeString(diseaseType);
        parcel.writeString(ipReqdiagId);
        parcel.writeString(typeDesc);
        parcel.writeString(icd10Code);
        parcel.writeString(maceRequestId);
        parcel.writeString(icd104C);
        parcel.writeString(diagCode);
        parcel.writeString(icd10Class);
        parcel.writeString(diagType);
        parcel.writeString(diagDesc);
        parcel.writeString(diagRemarks);
    }
}




