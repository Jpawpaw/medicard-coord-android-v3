package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IPC on 11/8/2017.
 */

public class RecentTransactions {
        private String memCode;
        private String memName;
        private String maceReqId;
        private String status;
        private String serviceType;
        private ArrayList<String> approvalNo;
        private String requestDateTime;

    public String getMemCode() {
        return memCode;
    }

    public void setMemCode(String memCode) {
        this.memCode = memCode;
    }

    public String getMemName() {
        return memName;
    }

    public void setMemName(String memName) {
        this.memName = memName;
    }

    public String getMaceReqId() {
        return maceReqId;
    }

    public void setMaceReqId(String maceReqId) {
        this.maceReqId = maceReqId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public ArrayList<String> getApprovalNo() {
        return approvalNo;
    }

    public void setApprovalNo(ArrayList<String> approvalNo) {
        this.approvalNo = approvalNo;
    }

    public String getRequestDateTime() {
        return requestDateTime;
    }

    public void setRequestDateTime(String requestDateTime) {
        this.requestDateTime = requestDateTime;
    }
}
