package model;

/**
 * Created by mpx-pawpaw on 11/11/16.
 */

public class VerifiedMember {
    private String ID_NUMBER;

    private String VERIFY_DATE;

    private String MEM_CODE;

    private String IS_VERIFIED;

    private String IS_SKIPPED;

    private String APP_USERNAME;

    private String ID;

    private String ID_TYPE;

    private String STATUS;

    public String getID_NUMBER ()
    {
        return ID_NUMBER;
    }

    public void setID_NUMBER (String ID_NUMBER)
    {
        this.ID_NUMBER = ID_NUMBER;
    }

    public String getVERIFY_DATE ()
    {
        return VERIFY_DATE;
    }

    public void setVERIFY_DATE (String VERIFY_DATE)
    {
        this.VERIFY_DATE = VERIFY_DATE;
    }

    public String getMEM_CODE ()
    {
        return MEM_CODE;
    }

    public void setMEM_CODE (String MEM_CODE)
    {
        this.MEM_CODE = MEM_CODE;
    }

    public String getIS_VERIFIED ()
    {
        return IS_VERIFIED;
    }

    public void setIS_VERIFIED (String IS_VERIFIED)
    {
        this.IS_VERIFIED = IS_VERIFIED;
    }

    public String getIS_SKIPPED ()
    {
        return IS_SKIPPED;
    }

    public void setIS_SKIPPED (String IS_SKIPPED)
    {
        this.IS_SKIPPED = IS_SKIPPED;
    }

    public String getAPP_USERNAME ()
    {
        return APP_USERNAME;
    }

    public void setAPP_USERNAME (String APP_USERNAME)
    {
        this.APP_USERNAME = APP_USERNAME;
    }

    public String getID ()
    {
        return ID;
    }

    public void setID (String ID)
    {
        this.ID = ID;
    }

    public String getID_TYPE ()
    {
        return ID_TYPE;
    }

    public void setID_TYPE (String ID_TYPE)
    {
        this.ID_TYPE = ID_TYPE;
    }

    public String getSTATUS ()
    {
        return STATUS;
    }

    public void setSTATUS (String STATUS)
    {
        this.STATUS = STATUS;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ID_NUMBER = "+ID_NUMBER+", VERIFY_DATE = "+VERIFY_DATE+", MEM_CODE = "+MEM_CODE+", IS_VERIFIED = "+IS_VERIFIED+", IS_SKIPPED = "+IS_SKIPPED+", APP_USERNAME = "+APP_USERNAME+", ID = "+ID+", ID_TYPE = "+ID_TYPE+", STATUS = "+STATUS+"]";
    }
}
