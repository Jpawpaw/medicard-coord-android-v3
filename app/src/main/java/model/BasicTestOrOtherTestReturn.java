package model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import model.Test.Data;

/**
 * Created by mpx-pawpaw on 5/23/17.
 */

public class BasicTestOrOtherTestReturn {
    private Data data;
    private String icd10Code;
    private String totalAmount;
    private String responseCode;
    private String responseDesc;
    private String approvalNo;
    private String status;
    @SerializedName("loaList")
    @Expose
    private LoaList loaList;

    private String batchCode;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDesc() {
        return responseDesc;
    }

    public void setResponseDesc(String responseDesc) {
        this.responseDesc = responseDesc;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Data getData() {
        return data;
    }

    public String getIcd10Code() {
        return icd10Code;
    }

    public void setIcd10Code(String icd10Code) {
        this.icd10Code = icd10Code;
    }

    public void setData(Data data) {
        this.data = data;
    }
    public String getApprovalNo() { return approvalNo;}

    public void setApprovalNo(String approvalNo) { this.approvalNo = approvalNo; }

    public LoaList getLoaList() {
        return loaList;
    }

    public void setLoaList(LoaList loaList) {
        this.loaList = loaList;
    }

    @Override
    public String toString() {
        return "ClassPojo [data = " + data + responseCode +  ", loaList = " + loaList + "]";
    }
}
