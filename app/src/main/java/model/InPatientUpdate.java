package model;

import java.util.ArrayList;

import fragment.InPatient.InPatientAdmittedAPICall;

/**
 * Created by mpx-pawpaw on 6/21/17.
 */

public class InPatientUpdate {
    private String[] diagnosisCodes;

    private String remarks;

    private String searchType;

    private String maceRequestCode;

    private String username;

    private String category;

    private String roomNumber;

    private ArrayList<ProcedureAndTestCodes> procedureAndTestCodes;

    private String roomType;

    private String roomPrice;

    private String[] doctorCodes;

    private String hospitalCode;

    private String dischargeDate;

    private String[] serviceCodes;

    private String dateAdmitted;

    private String deviceId;

    private String primaryDiag;

    private String primaryDoctor;

    private String memberCode;


    public String[] getDiagnosisCodes ()
    {
        return diagnosisCodes;
    }

    public String getPrimaryDoctor() {
        return primaryDoctor;
    }

    public void setPrimaryDoctor(String primaryDoctor) {
        this.primaryDoctor = primaryDoctor;
    }

    public String getPrimaryDiag() {
        return primaryDiag;
    }

    public void setPrimaryDiag(String primaryDiag) {
        this.primaryDiag = primaryDiag;
    }

    public void setDiagnosisCodes (String[] diagnosisCodes)
    {
        this.diagnosisCodes = diagnosisCodes;
    }

    public String getRemarks ()
    {
        return remarks;
    }

    public void setRemarks (String remarks)
    {
        this.remarks = remarks;
    }

    public String getSearchType ()
    {
        return searchType;
    }

    public void setSearchType (String searchType)
    {
        this.searchType = searchType;
    }

    public String getMaceRequestCode ()
    {
        return maceRequestCode;
    }

    public void setMaceRequestCode (String maceRequestCode)
    {
        this.maceRequestCode = maceRequestCode;
    }

    public String getUsername ()
    {
        return username;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }

    public String getCategory ()
    {
        return category;
    }

    public void setCategory (String category)
    {
        this.category = category;
    }

    public String getRoomNumber ()
    {
        return roomNumber;
    }

    public void setRoomNumber (String roomNumber)
    {
        this.roomNumber = roomNumber;
    }

    public ArrayList<ProcedureAndTestCodes> getProcedureAndTestCodes ()
    {
        return procedureAndTestCodes;
    }

    public void setProcedureAndTestCodes (ArrayList<ProcedureAndTestCodes> procedureAndTestCodes)
    {
        this.procedureAndTestCodes = procedureAndTestCodes;
    }

    public String getRoomType ()
    {
        return roomType;
    }

    public void setRoomType (String roomType)
    {
        this.roomType = roomType;
    }

    public String getRoomPrice ()
    {
        return roomPrice;
    }

    public void setRoomPrice (String roomPrice)
    {
        this.roomPrice = roomPrice;
    }

    public String[] getDoctorCodes ()
    {
        return doctorCodes;
    }

    public void setDoctorCodes (String[] doctorCodes)
    {
        this.doctorCodes = doctorCodes;
    }

    public String getHospitalCode ()
    {
        return hospitalCode;
    }

    public void setHospitalCode (String hospitalCode)
    {
        this.hospitalCode = hospitalCode;
    }

    public String getDischargeDate ()
    {
        return dischargeDate;
    }

    public void setDischargeDate (String dischargeDate)
    {
        this.dischargeDate = dischargeDate;
    }

    public String[] getServiceCodes ()
    {
        return serviceCodes;
    }

    public void setServiceCodes (String[] serviceCodes)
    {
        this.serviceCodes = serviceCodes;
    }

    public String getDateAdmitted ()
    {
        return dateAdmitted;
    }

    public void setDateAdmitted (String dateAdmitted)
    {
        this.dateAdmitted = dateAdmitted;
    }

    public String getDeviceId ()
    {
        return deviceId;
    }

    public void setDeviceId (String deviceId)
    {
        this.deviceId = deviceId;
    }

    public String getMemberCode ()
    {
        return memberCode;
    }

    public void setMemberCode (String memberCode)
    {
        this.memberCode = memberCode;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [diagnosisCodes = "+diagnosisCodes+", remarks = "+remarks+", searchType = "+searchType+", maceRequestCode = "+maceRequestCode+", username = "+username+", category = "+category+", roomNumber = "+roomNumber+", procedureAndTestCodes = "+procedureAndTestCodes+", roomType = "+roomType+", roomPrice = "+roomPrice+", doctorCodes = "+doctorCodes+", hospitalCode = "+hospitalCode+", dischargeDate = "+dischargeDate+", serviceCodes = "+serviceCodes+", dateAdmitted = "+dateAdmitted+", deviceId = "+deviceId+", memberCode = "+memberCode+"]";
    }
}

