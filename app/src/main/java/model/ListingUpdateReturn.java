package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IPC on 10/11/2017.
 */

public class ListingUpdateReturn {

    private String responseCode;
    private ArrayList<ListingUpdateModel> tables;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public ArrayList<ListingUpdateModel> getTables() {
        return tables;
    }

    public void setTables(ArrayList<ListingUpdateModel> tables) {
        this.tables = tables;
    }

    private class ListingUpdateModel {
        private String listType;
        private String lastUpdateDate;

        public String getListType() {
            return listType;
        }

        public void setListType(String listType) {
            this.listType = listType;
        }

        public String getLastUpdateDate() {
            return lastUpdateDate;
        }

        public void setLastUpdateDate(String lastUpdateDate) {
            this.lastUpdateDate = lastUpdateDate;
        }
    }
}
