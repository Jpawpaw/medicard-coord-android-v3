package model;

/**
 * Created by mpx-pawpaw on 5/24/17.
 */

public class MaceRequestTest {
    private String disapprovedOn;

    private String availHospId;

    private String dxRemarks;

    private String transactionId;

    private String primaryDiagnosisICD10;

    private String docHospId;

    private String approvedBy;

    private String validTo;

    private String statusRemarks;

    private String maceRequestId;

    private String refRequestId;

    private String disapprovedBy;

    private String transamount;

    private String approvalRemarks;

    private String disapprvalReason;

    private String primaryDiagnosisCode;

    private String doctorCode;

    private String status;

    private String approvalNo;

    private String primaryDiagnosisDesc;

    private String planOfManagement;

    private String approvedOn;

    private String expiredOn;

    private String requestFrom;

    private String validFrom;

    private String disapprovalRemarks;

    private String hospitalCode;

    private String maceTestGroup;

    private String testSubtype;

    private String notes;

    private String refRefNo;

    private String consultReason;

    public String getDisapprovedOn() {
        return disapprovedOn;
    }

    public void setDisapprovedOn(String disapprovedOn) {
        this.disapprovedOn = disapprovedOn;
    }

    public String getAvailHospId() {
        return availHospId;
    }

    public void setAvailHospId(String availHospId) {
        this.availHospId = availHospId;
    }

    public String getDxRemarks() {
        return dxRemarks;
    }

    public void setDxRemarks(String dxRemarks) {
        this.dxRemarks = dxRemarks;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getPrimaryDiagnosisICD10() {
        return primaryDiagnosisICD10;
    }

    public void setPrimaryDiagnosisICD10(String primaryDiagnosisICD10) {
        this.primaryDiagnosisICD10 = primaryDiagnosisICD10;
    }

    public String getDocHospId() {
        return docHospId;
    }

    public void setDocHospId(String docHospId) {
        this.docHospId = docHospId;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getStatusRemarks() {
        return statusRemarks;
    }

    public void setStatusRemarks(String statusRemarks) {
        this.statusRemarks = statusRemarks;
    }

    public String getMaceRequestId() {
        return maceRequestId;
    }

    public void setMaceRequestId(String maceRequestId) {
        this.maceRequestId = maceRequestId;
    }

    public String getRefRequestId() {
        return refRequestId;
    }

    public void setRefRequestId(String refRequestId) {
        this.refRequestId = refRequestId;
    }

    public String getDisapprovedBy() {
        return disapprovedBy;
    }

    public void setDisapprovedBy(String disapprovedBy) {
        this.disapprovedBy = disapprovedBy;
    }

    public String getTransamount() {
        return transamount;
    }

    public void setTransamount(String transamount) {
        this.transamount = transamount;
    }

    public String getApprovalRemarks() {
        return approvalRemarks;
    }

    public void setApprovalRemarks(String approvalRemarks) {
        this.approvalRemarks = approvalRemarks;
    }

    public String getDisapprvalReason() {
        return disapprvalReason;
    }

    public void setDisapprvalReason(String disapprvalReason) {
        this.disapprvalReason = disapprvalReason;
    }

    public String getPrimaryDiagnosisCode() {
        return primaryDiagnosisCode;
    }

    public void setPrimaryDiagnosisCode(String primaryDiagnosisCode) {
        this.primaryDiagnosisCode = primaryDiagnosisCode;
    }

    public String getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(String doctorCode) {
        this.doctorCode = doctorCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApprovalNo() {
        return approvalNo;
    }

    public void setApprovalNo(String approvalNo) {
        this.approvalNo = approvalNo;
    }

    public String getPrimaryDiagnosisDesc() {
        return primaryDiagnosisDesc;
    }

    public void setPrimaryDiagnosisDesc(String primaryDiagnosisDesc) {
        this.primaryDiagnosisDesc = primaryDiagnosisDesc;
    }

    public String getPlanOfManagement() {
        return planOfManagement;
    }

    public void setPlanOfManagement(String planOfManagement) {
        this.planOfManagement = planOfManagement;
    }

    public String getApprovedOn() {
        return approvedOn;
    }

    public void setApprovedOn(String approvedOn) {
        this.approvedOn = approvedOn;
    }

    public String getExpiredOn() {
        return expiredOn;
    }

    public void setExpiredOn(String expiredOn) {
        this.expiredOn = expiredOn;
    }

    public String getRequestFrom() {
        return requestFrom;
    }

    public void setRequestFrom(String requestFrom) {
        this.requestFrom = requestFrom;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getDisapprovalRemarks() {
        return disapprovalRemarks;
    }

    public void setDisapprovalRemarks(String disapprovalRemarks) {
        this.disapprovalRemarks = disapprovalRemarks;
    }

    public String getHospitalCode() {
        return hospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
    }

    public String getMaceTestGroup() {
        return maceTestGroup;
    }

    public void setMaceTestGroup(String maceTestGroup) {
        this.maceTestGroup = maceTestGroup;
    }

    public String getTestSubtype() {
        return testSubtype;
    }

    public void setTestSubtype(String testSubtype) {
        this.testSubtype = testSubtype;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getRefRefNo() {
        return refRefNo;
    }

    public void setRefRefNo(String refRefNo) {
        this.refRefNo = refRefNo;
    }

    public String getConsultReason() {
        return consultReason;
    }

    public void setConsultReason(String consultReason) {
        this.consultReason = consultReason;
    }

    @Override
    public String toString() {
        return "ClassPojo [disapprovedOn = " + disapprovedOn + ", availHospId = " + availHospId + ", dxRemarks = " + dxRemarks + ", transactionId = " + transactionId + ", primaryDiagnosisICD10 = " + primaryDiagnosisICD10 + ", docHospId = " + docHospId + ", approvedBy = " + approvedBy + ", validTo = " + validTo + ", statusRemarks = " + statusRemarks + ", maceRequestId = " + maceRequestId + ", refRequestId = " + refRequestId + ", disapprovedBy = " + disapprovedBy + ", transamount = " + transamount + ", approvalRemarks = " + approvalRemarks + ", disapprvalReason = " + disapprvalReason + ", primaryDiagnosisCode = " + primaryDiagnosisCode + ", doctorCode = " + doctorCode + ", status = " + status + ", approvalNo = " + approvalNo + ", primaryDiagnosisDesc = " + primaryDiagnosisDesc + ", planOfManagement = " + planOfManagement + ", approvedOn = " + approvedOn + ", expiredOn = " + expiredOn + ", requestFrom = " + requestFrom + ", validFrom = " + validFrom + ", disapprovalRemarks = " + disapprovalRemarks + ", hospitalCode = " + hospitalCode + ", maceTestGroup = " + maceTestGroup + ", testSubtype = " + testSubtype + ", notes = " + notes + ", refRefNo = " + refRefNo + ", consultReason = " + consultReason + "]";
    }
}

