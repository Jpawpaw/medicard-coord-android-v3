package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mpx-pawpaw on 11/22/16.
 */

public class DiagnosisList implements Parcelable {


    public DiagnosisList(){

    }

    public DiagnosisList(String status, String icd10Desc, String diagCode, String typeDesc, String type, String diagDesc, String icd10Code, String diagRemarks) {
        this.status = status;
        this.icd10Desc = icd10Desc;
        this.diagCode = diagCode;
        this.typeDesc = typeDesc;
        this.type = type;
        this.diagDesc = diagDesc;
        this.icd10Code = icd10Code;
        this.diagRemarks = diagRemarks;

    }

    private String status;

    private String icd10Desc;

    private String diagCode;

    private String typeDesc;

    private String type;

    private String diagDesc;

    private String icd10Code;

    private String diagRemarks;

    private String isSelected;

    protected DiagnosisList(Parcel in) {
        status = in.readString();
        icd10Desc = in.readString();
        diagCode = in.readString();
        typeDesc = in.readString();
        type = in.readString();
        diagDesc = in.readString();
        icd10Code = in.readString();
        diagRemarks = in.readString();
    }

    public static final Creator<DiagnosisList> CREATOR = new Creator<DiagnosisList>() {
        @Override
        public DiagnosisList createFromParcel(Parcel in) {
            return new DiagnosisList(in);
        }

        @Override
        public DiagnosisList[] newArray(int size) {
            return new DiagnosisList[size];
        }
    };

    public String getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(String isSelected) {
        this.isSelected = isSelected;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIcd10Desc() {
        return icd10Desc;
    }

    public void setIcd10Desc(String icd10Desc) {
        this.icd10Desc = icd10Desc;
    }

    public String getDiagCode() {
        return diagCode;
    }

    public void setDiagCode(String diagCode) {
        this.diagCode = diagCode;
    }

    public String getTypeDesc() {
        return typeDesc;
    }

    public void setTypeDesc(String typeDesc) {
        this.typeDesc = typeDesc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDiagDesc() {
        return diagDesc;
    }

    public void setDiagDesc(String diagDesc) {
        this.diagDesc = diagDesc;
    }

    public String getIcd10Code() {
        return icd10Code;
    }

    public void setIcd10Code(String icd10Code) {
        this.icd10Code = icd10Code;
    }

    public String getDiagRemarks() {
        return diagRemarks;
    }

    public void setDiagRemarks(String diagRemarks) {
        this.diagRemarks = diagRemarks;
    }

    @Override
    public String toString() {
        return "ClassPojo [status = " + status + ", icd10Desc = " + icd10Desc + ", diagCode = " + diagCode + ", typeDesc = " + typeDesc + ", type = " + type + ", diagDesc = " + diagDesc + ", icd10Code = " + icd10Code + ", diagRemarks = " + diagRemarks + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(status);
        parcel.writeString(icd10Desc);
        parcel.writeString(diagCode);
        parcel.writeString(typeDesc);
        parcel.writeString(type);
        parcel.writeString(diagDesc);
        parcel.writeString(icd10Code);
        parcel.writeString(diagRemarks);
    }

    public boolean isSelected() {
        if(getIsSelected().toString().equals("true")){
            return true;
        }else {
            return false;
        }
    }
}
