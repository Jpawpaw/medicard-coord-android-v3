package model;

import java.util.ArrayList;

/**
 * Created by mpx-pawpaw on 6/13/17.
 */

public class OtherServices {

    private ArrayList<Services> services;

    private String responseDesc;

    public ArrayList<Services> getServices() {
        return services;
    }

    public void setServices(ArrayList<Services> services) {
        this.services = services;
    }

    public String getResponseDesc() {
        return responseDesc;
    }

    public void setResponseDesc(String responseDesc) {
        this.responseDesc = responseDesc;
    }

    @Override
    public String toString() {
        return "ClassPojo [services = " + services + ", responseDesc = " + responseDesc + "]";
    }
}
