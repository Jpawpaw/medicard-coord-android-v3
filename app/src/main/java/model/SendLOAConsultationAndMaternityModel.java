package model;

/**
 * Created by mpx-pawpaw on 11/18/16.
 */

public class SendLOAConsultationAndMaternityModel {

    private String locationCode;

    private String username;

    private String procedureCode;

    private String doctorCode;

    private String diagnosisDesc;

    private String deviceID;

    private int procedureAmount;

    private String primaryComplaint;

    private String procedureDesc;

    private String hospitalCode;

    private String diagnosisCode;

    private String memberCode;

    private String duplicateTag;

    private int consultSubtype;

    private String coorContact;

    private String coorEmail;
    private String appUsername;

    public String getAppUsername() {
        return appUsername;
    }

    public void setAppUsername(String appUsername) {
        this.appUsername = appUsername;
    }

    public SendLOAConsultationAndMaternityModel(){}

    public SendLOAConsultationAndMaternityModel(String locationCode, String username, String procedureCode, String doctorCode, String diagnosisDesc, String deviceID, int procedureAmount, String primaryComplaint, String procedureDesc, String hospitalCode, String diagnosisCode, String memberCode, String duplicateTag, int consultSubtype, String coorContact, String coorEmail) {
        this.locationCode = locationCode;
        this.username = username;
        this.procedureCode = procedureCode;
        this.doctorCode = doctorCode;
        this.diagnosisDesc = diagnosisDesc;
        this.deviceID = deviceID;
        this.procedureAmount = procedureAmount;
        this.primaryComplaint = primaryComplaint;
        this.procedureDesc = procedureDesc;
        this.hospitalCode = hospitalCode;
        this.diagnosisCode = diagnosisCode;
        this.memberCode = memberCode;
        this.duplicateTag = duplicateTag;
        this.consultSubtype = consultSubtype;
        this.coorContact = coorContact;
        this.coorEmail = coorEmail;
    }

    public String getCoorContact() {
        return coorContact;
    }

    public void setCoorContact(String coorContact) {
        this.coorContact = coorContact;
    }

    public String getCoorEmail() {
        return coorEmail;
    }

    public void setCoorEmail(String coorEmail) {
        this.coorEmail = coorEmail;
    }

    public int getConsultSubtype() {
        return consultSubtype;
    }

    public void setConsultSubtype(int consultSubtype) {
        this.consultSubtype = consultSubtype;
    }

    public String getDuplicateTag() {
        return duplicateTag;
    }

    public void setDuplicateTag(String duplicateTag) {
        this.duplicateTag = duplicateTag;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProcedureCode() {
        return procedureCode;
    }

    public void setProcedureCode(String procedureCode) {
        this.procedureCode = procedureCode;
    }

    public String getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(String doctorCode) {
        this.doctorCode = doctorCode;
    }

    public String getDiagnosisDesc() {
        return diagnosisDesc;
    }

    public void setDiagnosisDesc(String diagnosisDesc) {
        this.diagnosisDesc = diagnosisDesc;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public int getProcedureAmount() {
        return procedureAmount;
    }

    public void setProcedureAmount(int procedureAmount) {
        this.procedureAmount = procedureAmount;
    }

    public String getPrimaryComplaint() {
        return primaryComplaint;
    }

    public void setPrimaryComplaint(String primaryComplaint) {
        this.primaryComplaint = primaryComplaint;
    }

    public String getProcedureDesc() {
        return procedureDesc;
    }

    public void setProcedureDesc(String procedureDesc) {
        this.procedureDesc = procedureDesc;
    }

    public String getHospitalCode() {
        return hospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
    }

    public String getDiagnosisCode() {
        return diagnosisCode;
    }

    public void setDiagnosisCode(String diagnosisCode) {
        this.diagnosisCode = diagnosisCode;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    @Override
    public String toString() {
        return "ClassPojo [locationCode = " + locationCode + ", username = " + username + ", procedureCode = " + procedureCode + ", doctorCode = " + doctorCode + ", diagnosisDesc = " + diagnosisDesc + ", deviceID = " + deviceID + ", procedureAmount = " + procedureAmount + ", primaryComplaint = " + primaryComplaint + ", procedureDesc = " + procedureDesc + ", hospitalCode = " + hospitalCode + ", diagnosisCode = " + diagnosisCode + ", memberCode = " + memberCode + ", duplicateTag = " + duplicateTag + "]";
    }
}
