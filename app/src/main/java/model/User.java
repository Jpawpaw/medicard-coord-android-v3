package model;

/**
 * Created by mpx-pawpaw on 10/28/16.
 */

public class User {
    private String HOSPITAL;

    private String ROLE;

    private String APP_PASSWORD;

    private String STATUS;

    private String CNTCT_PHONENO;

    private String LOCATION;

    private String COSTCENTER;

    private String CNTCT_EMAIL;

    private String APP_USERNAME;

    private String CNTCT_LNAME;

    private String APPUSER_ID;

    private String CNTCT_FNAME;

    private String CNTCT_MNAME;

    public String getHOSPITAL ()
    {
        return HOSPITAL;
    }

    public void setHOSPITAL (String HOSPITAL)
    {
        this.HOSPITAL = HOSPITAL;
    }

    public String getROLE ()
    {
        return ROLE;
    }

    public void setROLE (String ROLE)
    {
        this.ROLE = ROLE;
    }

    public String getAPP_PASSWORD ()
    {
        return APP_PASSWORD;
    }

    public void setAPP_PASSWORD (String APP_PASSWORD)
    {
        this.APP_PASSWORD = APP_PASSWORD;
    }

    public String getSTATUS ()
    {
        return STATUS;
    }

    public void setSTATUS (String STATUS)
    {
        this.STATUS = STATUS;
    }

    public String getCNTCT_PHONENO ()
    {
        return CNTCT_PHONENO;
    }

    public void setCNTCT_PHONENO (String CNTCT_PHONENO)
    {
        this.CNTCT_PHONENO = CNTCT_PHONENO;
    }

    public String getLOCATION ()
    {
        return LOCATION;
    }

    public void setLOCATION (String LOCATION)
    {
        this.LOCATION = LOCATION;
    }

    public String getCOSTCENTER ()
    {
        return COSTCENTER;
    }

    public void setCOSTCENTER (String COSTCENTER)
    {
        this.COSTCENTER = COSTCENTER;
    }

    public String getCNTCT_EMAIL ()
    {
        return CNTCT_EMAIL;
    }

    public void setCNTCT_EMAIL (String CNTCT_EMAIL)
    {
        this.CNTCT_EMAIL = CNTCT_EMAIL;
    }

    public String getAPP_USERNAME ()
    {
        return APP_USERNAME;
    }

    public void setAPP_USERNAME (String APP_USERNAME)
    {
        this.APP_USERNAME = APP_USERNAME;
    }

    public String getCNTCT_LNAME ()
    {
        return CNTCT_LNAME;
    }

    public void setCNTCT_LNAME (String CNTCT_LNAME)
    {
        this.CNTCT_LNAME = CNTCT_LNAME;
    }

    public String getAPPUSER_ID ()
    {
        return APPUSER_ID;
    }

    public void setAPPUSER_ID (String APPUSER_ID)
    {
        this.APPUSER_ID = APPUSER_ID;
    }

    public String getCNTCT_FNAME ()
    {
        return CNTCT_FNAME;
    }

    public void setCNTCT_FNAME (String CNTCT_FNAME)
    {
        this.CNTCT_FNAME = CNTCT_FNAME;
    }

    public String getCNTCT_MNAME ()
    {
        return CNTCT_MNAME;
    }

    public void setCNTCT_MNAME (String CNTCT_MNAME)
    {
        this.CNTCT_MNAME = CNTCT_MNAME;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [HOSPITAL = "+HOSPITAL+", ROLE = "+ROLE+", APP_PASSWORD = "+APP_PASSWORD+", STATUS = "+STATUS+", CNTCT_PHONENO = "+CNTCT_PHONENO+", LOCATION = "+LOCATION+", COSTCENTER = "+COSTCENTER+", CNTCT_EMAIL = "+CNTCT_EMAIL+", APP_USERNAME = "+APP_USERNAME+", CNTCT_LNAME = "+CNTCT_LNAME+", APPUSER_ID = "+APPUSER_ID+", CNTCT_FNAME = "+CNTCT_FNAME+", CNTCT_MNAME = "+CNTCT_MNAME+"]";
    }
}