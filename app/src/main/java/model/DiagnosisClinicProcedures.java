package model;

import android.os.Parcel;
import android.os.Parcelable;

import model.Test.*;

/**
 * Created by IPC_Server on 8/2/2017.
 */

public class DiagnosisClinicProcedures implements Parcelable  {
    private String amount;

    private MaceRequestOpTest maceRequestOpProcedure;

    private String procedureCode;

    private MaceRequestOpDiag maceRequestOpDiag;

    private String status;

    private String approvalNo;

    private String remarks;

    private String diagnosisCode;

    private MaceRequestTest maceRequestProcedure;


    public MaceRequestOpTest getMaceRequestOpProcedure() {
        return maceRequestOpProcedure;
    }

    public void setMaceRequestOpProcedure(MaceRequestOpTest maceRequestOpProcedure) {
        this.maceRequestOpProcedure = maceRequestOpProcedure;
    }

    public String getProcedureCode() {
        return procedureCode;
    }

    public void setProcedureCode(String procedureCode) {
        this.procedureCode = procedureCode;
    }

    public MaceRequestOpDiag getMaceRequestOpDiag() {
        return maceRequestOpDiag;
    }

    public void setMaceRequestOpDiag(MaceRequestOpDiag maceRequestOpDiag) {
        this.maceRequestOpDiag = maceRequestOpDiag;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApprovalNo() {
        return approvalNo;
    }

    public void setApprovalNo(String approvalNo) {
        this.approvalNo = approvalNo;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDiagnosisCode() {
        return diagnosisCode;
    }

    public void setDiagnosisCode(String diagnosisCode) {
        this.diagnosisCode = diagnosisCode;
    }

    public MaceRequestTest getMaceRequestProcedure() {
        return maceRequestProcedure;
    }

    public void setMaceRequestProcedure(MaceRequestTest maceRequestProcedure) {
        this.maceRequestProcedure = maceRequestProcedure;
    }

    public String getAmount() {

        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public static Creator<DiagnosisClinicProcedures> getCREATOR() {
        return CREATOR;
    }

    protected DiagnosisClinicProcedures(Parcel in) {
        amount = in.readString();
        procedureCode = in.readString();
        status = in.readString();
        approvalNo = in.readString();
        remarks = in.readString();
        diagnosisCode = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(amount);
        dest.writeString(procedureCode);
        dest.writeString(status);
        dest.writeString(approvalNo);
        dest.writeString(remarks);
        dest.writeString(diagnosisCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<model.DiagnosisClinicProcedures> CREATOR = new Parcelable.Creator<model.DiagnosisClinicProcedures>() {
        @Override
        public model.DiagnosisClinicProcedures createFromParcel(Parcel in) {
            return new model.DiagnosisClinicProcedures(in);
        }

        @Override
        public model.DiagnosisClinicProcedures[] newArray(int size) {
            return new model.DiagnosisClinicProcedures[size];
        }
    };
}
