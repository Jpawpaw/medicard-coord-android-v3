package model;

/**
 * Created by mpx-pawpaw on 11/17/16.
 */

public class Hospital
{
    private String phoneNo;

    private String region;

    private String regionDesc;

    private String entryType;

    private String isAccredited;

    private String mpiCnilicCategory;

    private String isMpiClinic;

    private String updateDate;

    private String streetAddress;

    private String category;

    private String faxno;

    private String alias;

    private String keyword;

    private String province;

    private String hospitalName;

    private String hospitalCode;

    private String coordinator;

    private String contactPerson;

    private String city;

    private String cclass;

    private String hospitalAbbre;

    public String getCclass() {
        return cclass;
    }

    public void setCclass(String cclass) {
        this.cclass = cclass;
    }

    public String getRegionDesc() {
        return regionDesc;
    }

    public void setRegionDesc(String regionDesc) {
        this.regionDesc = regionDesc;
    }

    public String getEntryType() {
        return entryType;
    }

    public void setEntryType(String entryType) {
        this.entryType = entryType;
    }

    public String getHospitalAbbre() {
        return hospitalAbbre;
    }

    public void setHospitalAbbre(String hospitalAbbre) {
        this.hospitalAbbre = hospitalAbbre;
    }

    public String getIsAccredited() {
        return isAccredited;
    }

    public void setIsAccredited(String isAccredited) {
        this.isAccredited = isAccredited;
    }

    public String getMpiCnilicCategory() {
        return mpiCnilicCategory;
    }

    public void setMpiCnilicCategory(String mpiCnilicCategory) {
        this.mpiCnilicCategory = mpiCnilicCategory;
    }

    public String getIsMpiClinic() {
        return isMpiClinic;
    }

    public void setIsMpiClinic(String isMpiClinic) {
        this.isMpiClinic = isMpiClinic;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }


    public String getPhoneNo ()
    {
        return phoneNo;
    }

    public void setPhoneNo (String phoneNo)
    {
        this.phoneNo = phoneNo;
    }

    public String getRegion ()
    {
        return region;
    }

    public void setRegion (String region)
    {
        this.region = region;
    }

    public String getStreetAddress ()
    {
        return streetAddress;
    }

    public void setStreetAddress (String streetAddress)
    {
        this.streetAddress = streetAddress;
    }

    public String getCategory ()
    {
        return category;
    }

    public void setCategory (String category)
    {
        this.category = category;
    }

    public String getFaxno ()
    {
        return faxno;
    }

    public void setFaxno (String faxno)
    {
        this.faxno = faxno;
    }

    public String getAlias ()
    {
        return alias;
    }

    public void setAlias (String alias)
    {
        this.alias = alias;
    }

    public String getKeyword ()
    {
        return keyword;
    }

    public void setKeyword (String keyword)
    {
        this.keyword = keyword;
    }

    public String getProvince ()
    {
        return province;
    }

    public void setProvince (String province)
    {
        this.province = province;
    }

    public String getHospitalName ()
    {
        return hospitalName;
    }

    public void setHospitalName (String hospitalName)
    {
        this.hospitalName = hospitalName;
    }

    public String getHospitalCode ()
    {
        return hospitalCode;
    }

    public void setHospitalCode (String hospitalCode)
    {
        this.hospitalCode = hospitalCode;
    }

    public String getCoordinator ()
    {
        return coordinator;
    }

    public void setCoordinator (String coordinator)
    {
        this.coordinator = coordinator;
    }

    public String getContactPerson ()
    {
        return contactPerson;
    }

    public void setContactPerson (String contactPerson)
    {
        this.contactPerson = contactPerson;
    }

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [phoneNo = "+phoneNo+", region = "+region+", streetAddress = "+streetAddress+", category = "+category+", faxno = "+faxno+", alias = "+alias+", keyword = "+keyword+", province = "+province+", hospitalName = "+hospitalName+", hospitalCode = "+hospitalCode+", coordinator = "+coordinator+", contactPerson = "+contactPerson+", city = "+city+"]";
    }
}

