package model;

/**
 * Created by mpx-pawpaw on 11/8/16.
 */

public class SkipVerification {
    String ID_NUMBER;

    String MEM_CODE;

    String IS_VERIFIED;

    String IS_SKIPPED;

    String APP_USERNAME;

    String ID_TYPE;

    public String getID_NUMBER() {
        return ID_NUMBER;
    }

    public void setID_NUMBER(String ID_NUMBER) {
        this.ID_NUMBER = ID_NUMBER;
    }

    public String getMEM_CODE() {
        return MEM_CODE;
    }

    public void setMEM_CODE(String MEM_CODE) {
        this.MEM_CODE = MEM_CODE;
    }

    public String getIS_VERIFIED() {
        return IS_VERIFIED;
    }

    public void setIS_VERIFIED(String IS_VERIFIED) {
        this.IS_VERIFIED = IS_VERIFIED;
    }

    public String getIS_SKIPPED() {
        return IS_SKIPPED;
    }

    public void setIS_SKIPPED(String IS_SKIPPED) {
        this.IS_SKIPPED = IS_SKIPPED;
    }

    public String getAPP_USERNAME() {
        return APP_USERNAME;
    }

    public void setAPP_USERNAME(String APP_USERNAME) {
        this.APP_USERNAME = APP_USERNAME;
    }

    public String getID_TYPE() {
        return ID_TYPE;
    }

    public void setID_TYPE(String ID_TYPE) {
        this.ID_TYPE = ID_TYPE;
    }

    @Override
    public String toString() {
        return "ClassPojo [ID_NUMBER = " + ID_NUMBER + ", MEM_CODE = " + MEM_CODE + ", IS_VERIFIED = " + IS_VERIFIED + ", IS_SKIPPED = " + IS_SKIPPED + ", APP_USERNAME = " + APP_USERNAME + ", ID_TYPE = " + ID_TYPE + "]";
    }
}
