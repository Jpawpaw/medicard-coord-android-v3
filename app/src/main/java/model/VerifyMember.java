package model;

import java.util.ArrayList;

/**
 * Created by mpx-pawpaw on 10/25/16.
 */

public class VerifyMember {

    private ArrayList<String> ServiceType;

    private String responseCode;

    private String responseDesc;

    private VerifiedMember VerifiedMember;

    private MemberInfo MemberInfo;

    private ArrayList<Dependents> Dependents;

    private ArrayList<Utilization> Utilization;

    private String isLocked;

    private String hasMaternity;

    private boolean isAdmittedIp;

    private String inpatientRequestCode;

    private boolean isAdmittedEr;

    private String erRequestCode;

    public boolean isAdmittedIp() { return isAdmittedIp; }

    public void setAdmittedIp(boolean admittedIp) {
        isAdmittedIp = admittedIp;
    }

    public String getInpatientRequestCode() { return inpatientRequestCode; }

    public void setInpatientRequestCode(String inpatientRequestCode) { this.inpatientRequestCode = inpatientRequestCode; }

    public boolean isAdmittedEr() {
        return isAdmittedEr;
    }

    public void setAdmittedEr(boolean admittedEr) {
        isAdmittedEr = admittedEr;
    }

    public String getErRequestCode() {
        return erRequestCode;
    }

    public void setErRequestCode(String erRequestCode) {
        this.erRequestCode = erRequestCode;
    }

    public String getHasMaternity() {
        return hasMaternity;
    }

    public void setHasMaternity(String hasMaternity) {
        this.hasMaternity = hasMaternity;
    }


    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDesc() {
        return responseDesc;
    }

    public void setResponseDesc(String responseDesc) {
        this.responseDesc = responseDesc;
    }

    public VerifiedMember getVerifiedMember() {
        return VerifiedMember;
    }

    public void setVerifiedMember(VerifiedMember VerifiedMember) {
        this.VerifiedMember = VerifiedMember;
    }

    public MemberInfo getMemberInfo() {
        return MemberInfo;
    }

    public void setMemberInfo(MemberInfo MemberInfo) {
        this.MemberInfo = MemberInfo;
    }

    public ArrayList<Dependents> getDependents() {
        return Dependents;
    }

    public void setDependents(ArrayList<Dependents> Dependents) {
        this.Dependents = Dependents;
    }

    public ArrayList<Utilization> getUtilization() {
        return Utilization;
    }

    public void setUtilization(ArrayList<Utilization> Utilization) {
        this.Utilization = Utilization;
    }

    public String getIsLocked() {
        return isLocked;
    }

    public void setIsLocked(String isLocked) {
        this.isLocked = isLocked;
    }

    public ArrayList<String> getServiceType() {
        return ServiceType;
    }

    public void setServiceType(ArrayList<String> ServiceType) {
        this.ServiceType = ServiceType;
    }

    @Override
    public String toString() {
        return "ClassPojo [responseCode = " + responseCode +
                ", responseDesc = " + responseDesc +
                ", VerifiedMember = " + VerifiedMember +
                ", MemberInfo = " + MemberInfo +
                ", Dependents = " + Dependents +
                ", Utilization = " + Utilization +
                ", ServiceType = " + ServiceType +
                ", isLocked = " + isLocked +
                ", isAdmittedIp = " + isAdmittedIp +
                ", isAdmittedEr = " + isAdmittedEr+"]";
    }
}

