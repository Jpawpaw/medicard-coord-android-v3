package model;

/**
 * Created by mpx-pawpaw on 6/13/17.
 */

public class Services {

    private String createdOn;

    private String id;

    private String serviceDesc;

    private String createdBy;

    private String lastUpdateOn;

    private String lastUpdateBy;

    private String serviceCode;

    private String isSelected;

    public String getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(String isSelected) {
        this.isSelected = isSelected;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServiceDesc() {
        return serviceDesc;
    }

    public void setServiceDesc(String serviceDesc) {
        this.serviceDesc = serviceDesc;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastUpdateOn() {
        return lastUpdateOn;
    }

    public void setLastUpdateOn(String lastUpdateOn) {
        this.lastUpdateOn = lastUpdateOn;
    }

    public String getLastUpdateBy() {
        return lastUpdateBy;
    }

    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    @Override
    public String toString() {
        return "ClassPojo [createdOn = " + createdOn + ", id = " + id + ", serviceDesc = " + serviceDesc + ", createdBy = " + createdBy + ", lastUpdateOn = " + lastUpdateOn + ", lastUpdateBy = " + lastUpdateBy + ", serviceCode = " + serviceCode + "]";
    }
}

