package model;

import java.util.Date;

/**
 * Created by Jabito on 04/08/2017.
 */

public class LockMember {

    private String memberCode;
    private String hospitalCode;
    private Date dateTimeBlocked;
    private String hospitalName;
    private Date bdayActual;
    private Date bdayEntered;
}
