package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by mpx-pawpaw on 5/8/17.
 */

public class Loa  implements Parcelable  {
    private String responseCode;

    private String responseDesc;

    private  ArrayList<LoaList> loaList;


    protected Loa(Parcel in) {
        responseCode = in.readString();
        responseDesc = in.readString();
        loaList = in.createTypedArrayList(LoaList.CREATOR);
    }

    public static final Creator<Loa> CREATOR = new Creator<Loa>() {
        @Override
        public Loa createFromParcel(Parcel in) {
            return new Loa(in);
        }

        @Override
        public Loa[] newArray(int size) {
            return new Loa[size];
        }
    };

    public String getResponseCode ()
    {
        return responseCode;
    }

    public void setResponseCode (String responseCode)
    {
        this.responseCode = responseCode;
    }

    public String getResponseDesc ()
    {
        return responseDesc;
    }

    public void setResponseDesc (String responseDesc)
    {
        this.responseDesc = responseDesc;
    }

    public  ArrayList<LoaList> getLoaList ()
    {
        return loaList;
    }

    public void setLoaList ( ArrayList<LoaList> loaList)
    {
        this.loaList = loaList;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [responseCode = "+responseCode+", responseDesc = "+responseDesc+", loaList = "+loaList+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(responseCode);
        parcel.writeString(responseDesc);
        parcel.writeTypedList(loaList);
    }
}