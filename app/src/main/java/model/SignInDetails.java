package model;




import java.util.ArrayList;
import java.util.HashMap;

public class SignInDetails {

    private String responseCode;

    private String responseDesc;

    private User user;

    private Hospital hospital;

    private HashMap<String, String> tables;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDesc() {
        return responseDesc;
    }

    public void setResponseDesc(String responseDesc) {
        this.responseDesc = responseDesc;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public HashMap<String, String> getTables() {
        return tables;
    }

    public void setTables(HashMap<String, String> tables) {
        this.tables = tables;
    }

    public static class ListingUpdateModel {
        private String listType;
        private String lastUpdateDate;

        public String getListType() {
            return listType;
        }

        public void setListType(String listType) {
            this.listType = listType;
        }

        public String getLastUpdateDate() {
            return lastUpdateDate;
        }

        public void setLastUpdateDate(String lastUpdateDate) {
            this.lastUpdateDate = lastUpdateDate;
        }
    }

    @Override
    public String toString() {
        return "ClassPojo [responseCode = " + responseCode + ", responseDesc = " + responseDesc + ", user = " + user + ", hospital = " + hospital + "]";
    }
}