package model;

import java.util.ArrayList;

/**
 * Created by mpx-pawpaw on 5/23/17.
 */

public class  BasicTestOrOtherTest {
    private String primaryDiagnosisCode;
    private String doctorCode;
    private String isDisclaimerTicked;
    private String requestBy;
    private String otherDiagnosisCode;
    private Integer serviceType;
    private String requestOrigin;
    private String hospitalCode;
    private String requestDevice;
    private String memberCode;
    private String remarks;
    private String disclaimerTicked;
    private String duplicateTag;
    private String serviceSubtype;
    private String coorContact;
    private String coorEmail;
    private String requestingHospCode;
    private String appUsername;
    private ArrayList<DiagnosisProcedures> diagnosisProcedures;
    private ArrayList<ClinicProceduresToSend> clinictests;

    public String getAppUsername() {
        return appUsername;
    }

    public void setAppUsername(String appUsername) {
        this.appUsername = appUsername;
    }

    public String getRequestingHospCode() {
        return requestingHospCode;
    }

    public void setRequestingHospCode(String requestingHospCode) {
        this.requestingHospCode = requestingHospCode;
    }

    public String getCoorContact() {
        return coorContact;
    }

    public void setCoorContact(String coorContact) {
        this.coorContact = coorContact;
    }

    public String getCoorEmail() {
        return coorEmail;
    }

    public void setCoorEmail(String coorEmail) {
        this.coorEmail = coorEmail;
    }

    public String getDisclaimerTicked() {
        return disclaimerTicked;
    }

    public void setDisclaimerTicked(String disclaimerTicked) {
        this.disclaimerTicked = disclaimerTicked;
    }

    public String getDuplicateTag() {
        return duplicateTag;
    }

    public void setDuplicateTag(String duplicateTag) {
        this.duplicateTag = duplicateTag;
    }

    public String getServiceSubtype() {
        return serviceSubtype;
    }

    public void setServiceSubtype(String serviceSubtype) {
        this.serviceSubtype = serviceSubtype;
    }


    public Integer getServiceType() {
        return serviceType;
    }

    public void setServiceType(Integer serviceType) {
        this.serviceType = serviceType;
    }

    public String getRemarks() { return remarks; }

    public void setRemarks(String remarks) { this.remarks = remarks; }

    public String getPrimaryDiagnosisCode() {
        return primaryDiagnosisCode;
    }

    public void setPrimaryDiagnosisCode(String primaryDiagnosisCode) { this.primaryDiagnosisCode = primaryDiagnosisCode; }

    public String getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(String doctorCode) {
        this.doctorCode = doctorCode;
    }

    public String getIsDisclaimerTicked() {
        return isDisclaimerTicked;
    }

    public void setIsDisclaimerTicked(String isDisclaimerTicked) {
        this.isDisclaimerTicked = isDisclaimerTicked;
    }

    public String getRequestBy() {
        return requestBy;
    }

    public void setRequestBy(String requestBy) {
        this.requestBy = requestBy;
    }

    public String getOtherDiagnosisCode() {
        return otherDiagnosisCode;
    }

    public void setOtherDiagnosisCode(String otherDiagnosisCode) {
        this.otherDiagnosisCode = otherDiagnosisCode;
    }

    public String getRequestOrigin() {
        return requestOrigin;
    }

    public void setRequestOrigin(String requestOrigin) {
        this.requestOrigin = requestOrigin;
    }

    public String getHospitalCode() {
        return hospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
    }

    public String getRequestDevice() {
        return requestDevice;
    }

    public void setRequestDevice(String requestDevice) {
        this.requestDevice = requestDevice;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public ArrayList<DiagnosisProcedures> getDiagnosisProcedures() {
        return diagnosisProcedures;
    }

    public void setDiagnosisProcedures(ArrayList<DiagnosisProcedures> diagnosisProcedures) {
        this.diagnosisProcedures = diagnosisProcedures;
    }

    public ArrayList<ClinicProceduresToSend> getClinicTests() {
        return clinictests;
    }

    public void setClinicTests(ArrayList<ClinicProceduresToSend> clinictests) {
        this.clinictests = clinictests;
    }

    public static class ClinicProceduresToSend {

        private double amount;

        private String procCode;

        private String procDesc;

        public double getAmount() {
            return amount;
        }

        public void setAmount(double amount) {
            this.amount = amount;
        }

        public String getProcCode() {
            return procCode;
        }

        public void setProcCode(String procCode) {
            this.procCode = procCode;
        }

        public String getProcDesc() {
            return procDesc;
        }

        public void setProcDesc(String procDesc) {
            this.procDesc = procDesc;
        }
    }


    @Override
    public String toString() {
        return "ClassPojo [primaryDiagnosisCode = " + primaryDiagnosisCode + ", doctorCode = " + doctorCode + ", isDisclaimerTicked = " + isDisclaimerTicked + ", requestBy = " + requestBy + ", otherDiagnosisCode = " + otherDiagnosisCode + ", serviceSubtype = " + serviceType + ", requestOrigin = " + requestOrigin + ", hospitalCode = " + hospitalCode + ", requestDevice = " + requestDevice + ", memberCode = " + memberCode + ", diagnosisProcedures = " + diagnosisProcedures + "]";
    }

    public class TestObj {
        private String procCode;
        private String amount;
        private String costCenter;

        public String getProcCode() {
            return procCode;
        }

        public void setProcCode(String procCode) {
            this.procCode = procCode;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getCostCenter() {
            return costCenter;
        }

        public void setCostCenter(String costCenter) {
            this.costCenter = costCenter;
        }
    }
}