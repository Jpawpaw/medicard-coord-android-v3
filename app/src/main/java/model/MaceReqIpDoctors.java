package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mpx-pawpaw on 6/19/17.
 */

public class MaceReqIpDoctors implements Parcelable {

    private String accredited;

    private String docFname;

    private String transactionId;

    private String doctorCode;

    private String docMname;

    private String addedOn;

    private String docHospId;

    private String maceDoctype;

    private String refDiagprocId;

    private String isaccredited;

    private String maceRequestId;

    private String docLname;

    private String hospitalCode;

    private String hospProfFee;

    private String defProfFee;

    private String actualProfFee;

    private String ipReqdocId;

    private String addedBy;

    protected MaceReqIpDoctors(Parcel in) {
        accredited = in.readString();
        docFname = in.readString();
        transactionId = in.readString();
        doctorCode = in.readString();
        docMname = in.readString();
        addedOn = in.readString();
        docHospId = in.readString();
        maceDoctype = in.readString();
        refDiagprocId = in.readString();
        isaccredited = in.readString();
        maceRequestId = in.readString();
        docLname = in.readString();
        hospitalCode = in.readString();
        hospProfFee = in.readString();
        defProfFee = in.readString();
        actualProfFee = in.readString();
        ipReqdocId = in.readString();
        addedBy = in.readString();
    }

    public static final Creator<MaceReqIpDoctors> CREATOR = new Creator<MaceReqIpDoctors>() {
        @Override
        public MaceReqIpDoctors createFromParcel(Parcel in) {
            return new MaceReqIpDoctors(in);
        }

        @Override
        public MaceReqIpDoctors[] newArray(int size) {
            return new MaceReqIpDoctors[size];
        }
    };

    public String getAccredited ()
    {
        return accredited;
    }

    public void setAccredited (String accredited)
    {
        this.accredited = accredited;
    }

    public String getDocFname ()
    {
        return docFname;
    }

    public void setDocFname (String docFname)
    {
        this.docFname = docFname;
    }

    public String getTransactionId ()
    {
        return transactionId;
    }

    public void setTransactionId (String transactionId)
    {
        this.transactionId = transactionId;
    }

    public String getDoctorCode ()
    {
        return doctorCode;
    }

    public void setDoctorCode (String doctorCode)
    {
        this.doctorCode = doctorCode;
    }

    public String getDocMname ()
    {
        return docMname;
    }

    public void setDocMname (String docMname)
    {
        this.docMname = docMname;
    }

    public String getAddedOn ()
    {
        return addedOn;
    }

    public void setAddedOn (String addedOn)
    {
        this.addedOn = addedOn;
    }

    public String getDocHospId ()
    {
        return docHospId;
    }

    public void setDocHospId (String docHospId)
    {
        this.docHospId = docHospId;
    }

    public String getMaceDoctype ()
    {
        return maceDoctype;
    }

    public void setMaceDoctype (String maceDoctype)
    {
        this.maceDoctype = maceDoctype;
    }

    public String getRefDiagprocId ()
    {
        return refDiagprocId;
    }

    public void setRefDiagprocId (String refDiagprocId)
    {
        this.refDiagprocId = refDiagprocId;
    }

    public String getIsaccredited ()
    {
        return isaccredited;
    }

    public void setIsaccredited (String isaccredited)
    {
        this.isaccredited = isaccredited;
    }

    public String getMaceRequestId ()
    {
        return maceRequestId;
    }

    public void setMaceRequestId (String maceRequestId)
    {
        this.maceRequestId = maceRequestId;
    }

    public String getDocLname ()
    {
        return docLname;
    }

    public void setDocLname (String docLname)
    {
        this.docLname = docLname;
    }

    public String getHospitalCode ()
    {
        return hospitalCode;
    }

    public void setHospitalCode (String hospitalCode)
    {
        this.hospitalCode = hospitalCode;
    }

    public String getHospProfFee ()
    {
        return hospProfFee;
    }

    public void setHospProfFee (String hospProfFee)
    {
        this.hospProfFee = hospProfFee;
    }

    public String getDefProfFee ()
    {
        return defProfFee;
    }

    public void setDefProfFee (String  defProfFee)
    {
        this.defProfFee = defProfFee;
    }

    public String getActualProfFee ()
    {
        return actualProfFee;
    }

    public void setActualProfFee (String actualProfFee)
    {
        this.actualProfFee = actualProfFee;
    }

    public String getIpReqdocId ()
    {
        return ipReqdocId;
    }

    public void setIpReqdocId (String ipReqdocId)
    {
        this.ipReqdocId = ipReqdocId;
    }

    public String getAddedBy ()
    {
        return addedBy;
    }

    public void setAddedBy (String addedBy)
    {
        this.addedBy = addedBy;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [accredited = "+accredited+", docFname = "+docFname+", transactionId = "+transactionId+", doctorCode = "+doctorCode+", docMname = "+docMname+", addedOn = "+addedOn+", docHospId = "+docHospId+", maceDoctype = "+maceDoctype+", refDiagprocId = "+refDiagprocId+", isaccredited = "+isaccredited+", maceRequestId = "+maceRequestId+", docLname = "+docLname+", hospitalCode = "+hospitalCode+", hospProfFee = "+hospProfFee+", defProfFee = "+defProfFee+", actualProfFee = "+actualProfFee+", ipReqdocId = "+ipReqdocId+", addedBy = "+addedBy+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(accredited);
        parcel.writeString(docFname);
        parcel.writeString(transactionId);
        parcel.writeString(doctorCode);
        parcel.writeString(docMname);
        parcel.writeString(addedOn);
        parcel.writeString(docHospId);
        parcel.writeString(maceDoctype);
        parcel.writeString(refDiagprocId);
        parcel.writeString(isaccredited);
        parcel.writeString(maceRequestId);
        parcel.writeString(docLname);
        parcel.writeString(hospitalCode);
        parcel.writeString(hospProfFee);
        parcel.writeString(defProfFee);
        parcel.writeString(actualProfFee);
        parcel.writeString(ipReqdocId);
        parcel.writeString(addedBy);
    }
}

