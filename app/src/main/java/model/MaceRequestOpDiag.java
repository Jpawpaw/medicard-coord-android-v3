package model;

/**
 * Created by mpx-pawpaw on 5/24/17.
 */


public class MaceRequestOpDiag {
    private String transactionId;

    private String diagTypeOld;

    private String diagClass;

    private String groupDesc;

    private String maceDiagType;

    private String diseaseType;

    private String typeDesc;

    private String reqDiagId;

    private String icd10Code;

    private String icd104C;

    private String maceRequestId;

    private String diagCode;


    private String icd10Class;

    private String diagType;

    private String diagDesc;

    private String diagRemarks;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getDiagTypeOld() {
        return diagTypeOld;
    }

    public void setDiagTypeOld(String diagTypeOld) {
        this.diagTypeOld = diagTypeOld;
    }

    public String getDiagClass() {
        return diagClass;
    }

    public void setDiagClass(String diagClass) {
        this.diagClass = diagClass;
    }

    public String getGroupDesc() {
        return groupDesc;
    }

    public void setGroupDesc(String groupDesc) {
        this.groupDesc = groupDesc;
    }

    public String getMaceDiagType() {
        return maceDiagType;
    }

    public void setMaceDiagType(String maceDiagType) {
        this.maceDiagType = maceDiagType;
    }

    public String getDiseaseType() {
        return diseaseType;
    }

    public void setDiseaseType(String diseaseType) {
        this.diseaseType = diseaseType;
    }

    public String getTypeDesc() {
        return typeDesc;
    }

    public void setTypeDesc(String typeDesc) {
        this.typeDesc = typeDesc;
    }

    public String getReqDiagId() {
        return reqDiagId;
    }

    public void setReqDiagId(String reqDiagId) {
        this.reqDiagId = reqDiagId;
    }

    public String getIcd10Code() {
        return icd10Code;
    }

    public void setIcd10Code(String icd10Code) {
        this.icd10Code = icd10Code;
    }

    public String getIcd104C() {
        return icd104C;
    }

    public String getDiagDesc() {
        return diagDesc;
    }

    public void setDiagDesc(String diagDesc) {
        this.diagDesc = diagDesc;
    }

}
