package model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mpx-pawpaw on 3/3/17.
 */


public class LoaList implements Parcelable {

    private String procedureCode;

    private String otherDiagnosisDesc;

    private String batchCode;

    private String reason;

    private String memCompany;

    private String diagnosis;

    private String remarks;

    private String diagnosisCode;

    private String type;

    private String[] proceduresList;

    private String updatedBy;

    private String callTypeId;

    private String id;

    private String otherDiagnosisCode;

    private String runningBill;

    private String primaryComplaint;

    private String memMi;

    private String memberCode;

    private String memCode;

    private String memFname;

    private String memLname;

    private String memGender;

    private String doctorCode;

    private String[] diagnosisList;

    private String actionTaken;

    private String status;

    private String updatedDate;

    private String terminalNo;

    private String requestOrigin;

    private String approvalNo;

    private String procedureAmount;

    private String callDate;

    private String companyCode;

    private String disclaimerTicked;

    private String category;

    private String diagnosisTest;

    private String callerId;

    private ArrayList<OtherDiagnosisTest> otherDiagnosisTest;

    private String approvalDate;

    private String hospitalCode;

    private String procedureDesc;

    private String notes;

    private String dateAdmitted;

    private String room;

    private String requestType;

    private String serviceType;

    private String requestTypeDetail01;

    private String requestTypeDetail02;

    private String requestTypeDetail03;

    private String requestCode;

    private String requestedDate;

    private String requestDatetime;


    @SerializedName("doctorName")
    @Expose

    private String doctorName;
    @SerializedName("doctorSpec")
    @Expose

    private String doctorSpec;

    private String primaryDiag;

    private String reasonForConsult;

    private String diagType;

    private String totalAmount;

    private ArrayList<GroupedByCostCenter> groupedByCostCenters;

    public ArrayList<GroupedByCostCenter> getGroupedByCostCenters() {
        return groupedByCostCenters;
    }

    public void setGroupedByCostCenters(ArrayList<GroupedByCostCenter> groupedByCostCenters) {
        this.groupedByCostCenters = groupedByCostCenters;
    }

    public static class GroupedByCostCenter implements Parcelable {

        private String costCenter;

        private String subTotal;

        private String status;

        private boolean firstInstance;

        public boolean basicTestFlag;

        private int diagType;

        private String diagDesc;

        private ArrayList<GroupedByDiag> groupedByDiag;

        public GroupedByCostCenter() {
        }

        protected GroupedByCostCenter(Parcel in) {
            costCenter = in.readString();
            subTotal = in.readString();
            status = in.readString();
            diagType = in.readInt();
            diagDesc = in.readString();
            groupedByDiag = in.createTypedArrayList(GroupedByDiag.CREATOR);
        }

        public static final Creator<GroupedByCostCenter> CREATOR = new Creator<GroupedByCostCenter>() {
            @Override
            public GroupedByCostCenter createFromParcel(Parcel in) {
                return new GroupedByCostCenter(in);
            }

            @Override
            public GroupedByCostCenter[] newArray(int size) {
                return new GroupedByCostCenter[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(costCenter);
            dest.writeString(subTotal);
            dest.writeString(status);
            dest.writeInt(diagType);
            dest.writeString(diagDesc);
            dest.writeTypedList(groupedByDiag);
        }

        public static class GroupedByDiag implements Parcelable {

            /**
             * For App-side only
             */
            private String costCenter;

            private String subTotal;

            private String status;

            private boolean firstInstance;

            private boolean lastInstance;

            public boolean basicTestFlag;

            private Integer diagType;

            private String diagDesc;

            private String approvalNo;

            private ArrayList<MappedTest> mappedTests;

            //<editor-fold default=collapsed desc="Sub GetterSetters">
            public String getCostCenter() {
                return costCenter;
            }

            public void setCostCenter(String costCenter) {
                this.costCenter = costCenter;
            }

            public String getSubTotal() {
                return subTotal;
            }

            public void setSubTotal(String subTotal) {
                this.subTotal = subTotal;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public boolean isFirstInstance() {
                return firstInstance;
            }

            public void setFirstInstance(boolean firstInstance) {
                this.firstInstance = firstInstance;
            }

            public boolean isLastInstance() {
                return lastInstance;
            }

            public void setLastInstance(boolean lastInstance) {
                this.lastInstance = lastInstance;
            }

            public boolean isBasicTestFlag() {
                return basicTestFlag;
            }

            public void setBasicTestFlag(boolean basicTestFlag) {
                this.basicTestFlag = basicTestFlag;
            }

            //</editor-fold>

            protected GroupedByDiag(Parcel in) {
                costCenter = in.readString();
                subTotal = in.readString();
                status = in.readString();
                diagType = in.readInt();
                diagDesc = in.readString();
                approvalNo = in.readString();
                mappedTests = in.createTypedArrayList(MappedTest.CREATOR);
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(costCenter);
                dest.writeString(subTotal);
                dest.writeString(status);
                dest.writeInt(diagType);
                dest.writeString(diagDesc);
                dest.writeString(approvalNo);
                dest.writeTypedList(mappedTests);
            }

            @Override
            public int describeContents() {
                return 0;
            }

            public static final Creator<GroupedByDiag> CREATOR = new Creator<GroupedByDiag>() {
                @Override
                public GroupedByDiag createFromParcel(Parcel in) {
                    return new GroupedByDiag(in);
                }

                @Override
                public GroupedByDiag[] newArray(int size) {
                    return new GroupedByDiag[size];
                }
            };

            //<editor-fold desc="GetterSettersGroupByDiag" default="collapsed">
            public Integer getDiagType() {
                return diagType;
            }

            public void setDiagType(Integer diagType) {
                this.diagType = diagType;
            }

            public String getDiagDesc() {
                return diagDesc;
            }

            public void setDiagDesc(String diagDesc) {
                this.diagDesc = diagDesc;
            }

            public String getApprovalNo() {
                return approvalNo;
            }

            public void setApprovalNo(String approvalNo) {
                this.approvalNo = approvalNo;
            }

            public ArrayList<MappedTest> getMappedTests() {
                return mappedTests;
            }

            public void setMappedTests(ArrayList<MappedTest> mappedTests) {
                this.mappedTests = mappedTests;
            }

        }

        //<editor-fold desc="GetterSettersGroupByCC" default="collapsed">
        public String getCostCenter() {
            return costCenter;
        }

        public void setCostCenter(String costCenter) {
            this.costCenter = costCenter;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getSubTotal() {
            return subTotal;
        }

        public void setSubTotal(String subTotal) {
            this.subTotal = subTotal;
        }

        public ArrayList<GroupedByDiag> getGroupedByDiag() {
            return groupedByDiag;
        }

        public void setGroupedByDiag(ArrayList<GroupedByDiag> groupedByDiag) {
            this.groupedByDiag = groupedByDiag;
        }

        public boolean isFirstInstance() {
            return firstInstance;
        }

        public void setFirstInstance(boolean firstInstance) {
            this.firstInstance = firstInstance;
        }

        public boolean isBasicTestFlag() {
            return basicTestFlag;
        }

        public void setBasicTestFlag(boolean basicTestFlag) {
            this.basicTestFlag = basicTestFlag;
        }

        public int getDiagType() {
            return diagType;
        }

        public void setDiagType(int diagType) {
            this.diagType = diagType;
        }

        public String getDiagDesc() {
            return diagDesc;
        }

        public void setDiagDesc(String diagDesc) {
            this.diagDesc = diagDesc;
        }
        //</editor-fold>

    }

    private ArrayList<MappedTest> mappedTest;

    public static class MappedTest implements Parcelable {

        private String procCode;

        private Integer diagType;

        private String procDesc;

        private String costCenter;

        private String amount;

        private String notes;

        public String getProcCode() {
            return procCode;
        }

        public void setProcCode(String procCode) {
            this.procCode = procCode;
        }

        public String getProcDesc() {
            return procDesc;
        }

        public void setProcDesc(String procDesc) {
            this.procDesc = procDesc;
        }

        public String getCostCenter() {
            return costCenter;
        }

        public void setCostCenter(String costCenter) {
            this.costCenter = costCenter;
        }

        public String getAmount() {
            return amount;
        }

        public Integer getDiagType() {
            return diagType;
        }

        public void setDiagType(Integer diagType) {
            this.diagType = diagType;
        }

        public String getNotes() {
            return notes;
        }

        public void setNotes(String notes) {
            this.notes = notes;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        protected MappedTest(Parcel in) {
            diagType = in.readInt();

            procCode = in.readString();

            procDesc = in.readString();

            costCenter = in.readString();

            amount = in.readString();

            notes = in.readString();
        }

        @Override
        public String toString() {
            return "ClassPojo [procCode = " + procCode + ", procDesc = " + procDesc + " costCenter = " + costCenter + ", amount = " + amount + ", notes = " + notes + "]";
        }

        public static Creator<MappedTest> getCREATOR() {
            return CREATOR;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {

            parcel.writeInt(diagType);
            parcel.writeString(procCode);
            parcel.writeString(procDesc);
            parcel.writeString(costCenter);
            parcel.writeString(amount);
            parcel.writeString(notes);
        }

        public static final Parcelable.Creator<MappedTest> CREATOR = new Parcelable.Creator<MappedTest>() {
            @Override
            public MappedTest createFromParcel(Parcel in) {
                return new MappedTest(in);
            }

            @Override
            public MappedTest[] newArray(int size) {
                return new MappedTest[size];
            }
        };


    }

    protected LoaList(Parcel in) {
        procedureCode = in.readString();
        otherDiagnosisDesc = in.readString();
        batchCode = in.readString();
        reason = in.readString();
        memCompany = in.readString();
        diagnosis = in.readString();
        remarks = in.readString();
        diagnosisCode = in.readString();
        type = in.readString();
        proceduresList = in.createStringArray();
        updatedBy = in.readString();
        callTypeId = in.readString();
        id = in.readString();
        otherDiagnosisCode = in.readString();
        runningBill = in.readString();
        primaryComplaint = in.readString();
        memMi = in.readString();
        memberCode = in.readString();
        memGender = in.readString();
        memFname = in.readString();
        memLname = in.readString();
        doctorCode = in.readString();
        actionTaken = in.readString();
        status = in.readString();
        updatedDate = in.readString();
        terminalNo = in.readString();
        requestOrigin = in.readString();
        approvalNo = in.readString();
        procedureAmount = in.readString();
        callDate = in.readString();
        companyCode = in.readString();
        disclaimerTicked = in.readString();
        category = in.readString();
        diagnosisTest = in.readString();
        callerId = in.readString();
        approvalDate = in.readString();
        hospitalCode = in.readString();
        procedureDesc = in.readString();
        notes = in.readString();
        dateAdmitted = in.readString();
        room = in.readString();
        requestType = in.readString();
        serviceType = in.readString();
        requestTypeDetail01 = in.readString();
        requestTypeDetail02 = in.readString();
        requestTypeDetail03 = in.readString();
        requestCode = in.readString();
        requestedDate = in.readString();
        memCode = in.readString();
        requestDatetime = in.readString();
        doctorName = in.readString();
        doctorSpec = in.readString();
        primaryDiag = in.readString();
        reasonForConsult = in.readString();
        diagType = in.readString();
        mappedTest = in.createTypedArrayList(LoaList.MappedTest.CREATOR);
        groupedByCostCenters = in.createTypedArrayList(GroupedByCostCenter.CREATOR);
        totalAmount = in.readString();
    }

    public static final Creator<LoaList> CREATOR = new Creator<LoaList>() {
        @Override
        public LoaList createFromParcel(Parcel in) {
            return new LoaList(in);
        }

        @Override
        public LoaList[] newArray(int size) {
            return new LoaList[size];
        }
    };

    public String getProcedureCode() {
        return procedureCode;
    }

    public void setProcedureCode(String procedureCode) {
        this.procedureCode = procedureCode;
    }

    public String getOtherDiagnosisDesc() {
        return otherDiagnosisDesc;
    }

    public void setOtherDiagnosisDesc(String otherDiagnosisDesc) {
        this.otherDiagnosisDesc = otherDiagnosisDesc;
    }

    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMemCompany() {
        return memCompany;
    }

    public void setMemCompany(String memCompany) {
        this.memCompany = memCompany;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDiagnosisCode() {
        return diagnosisCode;
    }

    public void setDiagnosisCode(String diagnosisCode) {
        this.diagnosisCode = diagnosisCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String[] getProceduresList() {
        return proceduresList;
    }

    public void setProceduresList(String[] proceduresList) {
        this.proceduresList = proceduresList;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCallTypeId() {
        return callTypeId;
    }

    public void setCallTypeId(String callTypeId) {
        this.callTypeId = callTypeId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOtherDiagnosisCode() {
        return otherDiagnosisCode;
    }

    public void setOtherDiagnosisCode(String otherDiagnosisCode) {
        this.otherDiagnosisCode = otherDiagnosisCode;
    }

    public String getRunningBill() {
        return runningBill;
    }

    public void setRunningBill(String runningBill) {
        this.runningBill = runningBill;
    }

    public String getPrimaryComplaint() {
        return primaryComplaint;
    }

    public void setPrimaryComplaint(String primaryComplaint) {
        this.primaryComplaint = primaryComplaint;
    }

    public String getMemMi() {
        return memMi;
    }

    public void setMemMi(String memMi) {
        this.memMi = memMi;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getMemFname() {
        return memFname;
    }

    public void setMemFname(String memFname) {
        this.memFname = memFname;
    }

    public String getMemLname() {
        return memLname;
    }

    public void setMemLname(String memLname) {
        this.memLname = memLname;
    }

    public String getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(String doctorCode) {
        this.doctorCode = doctorCode;
    }

    public String[] getDiagnosisList() {
        return diagnosisList;
    }

    public void setDiagnosisList(String[] diagnosisList) {
        this.diagnosisList = diagnosisList;
    }

    public String getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getTerminalNo() {
        return terminalNo;
    }

    public void setTerminalNo(String terminalNo) {
        this.terminalNo = terminalNo;
    }

    public String getRequestOrigin() {
        return requestOrigin;
    }

    public void setRequestOrigin(String requestOrigin) {
        this.requestOrigin = requestOrigin;
    }

    public String getApprovalNo() {
        return approvalNo;
    }

    public void setApprovalNo(String approvalNo) {
        this.approvalNo = approvalNo;
    }

    public String getProcedureAmount() {
        return procedureAmount;
    }

    public void setProcedureAmount(String procedureAmount) {
        this.procedureAmount = procedureAmount;
    }

    public String getCallDate() {
        return callDate;
    }

    public void setCallDate(String callDate) {
        this.callDate = callDate;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getDisclaimerTicked() {
        return disclaimerTicked;
    }

    public void setDisclaimerTicked(String disclaimerTicked) {
        this.disclaimerTicked = disclaimerTicked;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDiagnosisTest() {
        return diagnosisTest;
    }

    public void setDiagnosisTest(String diagnosisTest) {
        this.diagnosisTest = diagnosisTest;
    }

    public String getCallerId() {
        return callerId;
    }

    public void setCallerId(String callerId) {
        this.callerId = callerId;
    }

    public ArrayList<OtherDiagnosisTest> getOtherDiagnosisTest() {
        return otherDiagnosisTest;
    }

    public void setOtherDiagnosisTest(ArrayList<OtherDiagnosisTest> otherDiagnosisTest) {
        this.otherDiagnosisTest = otherDiagnosisTest;
    }

    public String getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(String approvalDate) {
        this.approvalDate = approvalDate;
    }

    public String getHospitalCode() {
        return hospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
    }

    public String getProcedureDesc() {
        return procedureDesc;
    }

    public void setProcedureDesc(String procedureDesc) {
        this.procedureDesc = procedureDesc;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDateAdmitted() {
        return dateAdmitted;
    }

    public void setDateAdmitted(String dateAdmitted) {
        this.dateAdmitted = dateAdmitted;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getRequestTypeDetail01() {
        return requestTypeDetail01;
    }

    public void setRequestTypeDetail01(String requestTypeDetail01) {
        this.requestTypeDetail01 = requestTypeDetail01;
    }

    public String getRequestTypeDetail02() {
        return requestTypeDetail02;
    }

    public void setRequestTypeDetail02(String requestTypeDetail02) {
        this.requestTypeDetail02 = requestTypeDetail02;
    }

    public String getRequestTypeDetail03() {
        return requestTypeDetail03;
    }

    public void setRequestTypeDetail03(String requestTypeDetail03) {
        this.requestTypeDetail03 = requestTypeDetail03;
    }

    public String getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(String requestCode) {
        this.requestCode = requestCode;
    }

    public String getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(String requestedDate) {
        this.requestedDate = requestedDate;
    }

    public String getMemCode() {
        return memCode;
    }

    public void setMemCode(String memCode) {
        this.memCode = memCode;
    }

    public String getRequestDatetime() {
        return requestDatetime;
    }

    public void setRequestDatetime(String requestDatetime) {
        this.requestDatetime = requestDatetime;
    }

    public String getDoctorSpec() {
        return doctorSpec;
    }

    public void setDoctorSpec(String doctorSpec) {
        this.doctorSpec = doctorSpec;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }


    public String getReasonForConsult() {
        return reasonForConsult;
    }

    public void setReasonForConsult(String reasonForConsult) {
        this.reasonForConsult = reasonForConsult;
    }

    public String getPrimaryDiag() {
        return primaryDiag;
    }

    public void setPrimaryDiag(String primaryDiag) {
        this.primaryDiag = primaryDiag;
    }

    public String getMemGender() {
        return memGender;
    }

    public void setMemGender(String memGender) {
        this.memGender = memGender;
    }

    public String getDiagType() {
        return diagType;
    }

    public void setDiagType(String diagType) {
        this.diagType = diagType;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public ArrayList<MappedTest> getMappedTest() {
        return mappedTest;
    }

    public void setMappedTest(ArrayList<MappedTest> mappedTest) {
        this.mappedTest = mappedTest;
    }

    public static Creator<LoaList> getCREATOR() {
        return CREATOR;
    }


    @Override
    public String toString() {
        return "ClassPojo [procedureCode = " + procedureCode + ", otherDiagnosisDesc = " + otherDiagnosisDesc + ", batchCode = " + batchCode + ", reason = " + reason + ", memCompany = " + memCompany + ", diagnosis = " + diagnosis + ", remarks = " + remarks + ", diagnosisCode = " + diagnosisCode + ", type = " + type + ", proceduresList = " + proceduresList + ", updatedBy = " + updatedBy + ", callTypeId = " + callTypeId + ", id = " + id + ", otherDiagnosisCode = " + otherDiagnosisCode + ", runningBill = " + runningBill + ", primaryComplaint = " + primaryComplaint + ", memMi = " + memMi + ", memCode = " + memCode + ", memFname = " + memFname + ", memLname = " + memLname + ", memGender = " + memGender + ", doctorCode = " + doctorCode + ", diagnosisList = " + diagnosisList + ", actionTaken = " + actionTaken + ", status = " + status + ", updatedDate = " + updatedDate + ", terminalNo = " + terminalNo + ", requestOrigin = " + requestOrigin + ", approvalNo = " + approvalNo + ", procedureAmount = " + procedureAmount + ", callDate = " + callDate + ", companyCode = " + companyCode + ", disclaimerTicked = " + disclaimerTicked + ", category = " + category + ", diagnosisTest = " + diagnosisTest + ", callerId = " + callerId + ", otherDiagnosisTest = " + otherDiagnosisTest + ", approvalDate = " + approvalDate + ", hospitalCode = " + hospitalCode + ", procedureDesc = " + procedureDesc + ", notes = " + notes + ", dateAdmitted = " + dateAdmitted + ", room = " + room + ", requestType = " + requestType + ", serviceType = " + serviceType + ", requestDatetime = " + requestDatetime + ", requestTypeDetail01 = " + requestTypeDetail01 + ", requestTypeDetail02 = " + requestTypeDetail02 + ", requestTypeDetail03 = " + requestTypeDetail03 + ", doctorName = " + doctorName + ", doctorSpec = " + doctorSpec + ", primaryDiag = " + primaryDiag + ", reasonForConsult = " + reasonForConsult + ", mappedTest = " + mappedTest + ", diagType = " + diagType + ", groupedByDiag = " + groupedByCostCenters + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(procedureCode);
        parcel.writeString(otherDiagnosisDesc);
        parcel.writeString(batchCode);
        parcel.writeString(reason);
        parcel.writeString(memCompany);
        parcel.writeString(diagnosis);
        parcel.writeString(remarks);
        parcel.writeString(diagnosisCode);
        parcel.writeString(type);
        parcel.writeStringArray(proceduresList);
        parcel.writeString(updatedBy);
        parcel.writeString(callTypeId);
        parcel.writeString(id);
        parcel.writeString(otherDiagnosisCode);
        parcel.writeString(runningBill);
        parcel.writeString(primaryComplaint);
        parcel.writeString(memMi);
        parcel.writeString(memberCode);
        parcel.writeString(memGender);
        parcel.writeString(memLname);
        parcel.writeString(memFname);
        parcel.writeString(doctorCode);
        parcel.writeString(actionTaken);
        parcel.writeString(status);
        parcel.writeString(updatedDate);
        parcel.writeString(terminalNo);
        parcel.writeString(requestOrigin);
        parcel.writeString(approvalNo);
        parcel.writeString(procedureAmount);
        parcel.writeString(callDate);
        parcel.writeString(companyCode);
        parcel.writeString(disclaimerTicked);
        parcel.writeString(category);
        parcel.writeString(diagnosisTest);
        parcel.writeString(callerId);
        parcel.writeString(approvalDate);
        parcel.writeString(hospitalCode);
        parcel.writeString(procedureDesc);
        parcel.writeString(notes);
        parcel.writeString(dateAdmitted);
        parcel.writeString(room);
        parcel.writeString(requestType);
        parcel.writeString(serviceType);
        parcel.writeString(requestTypeDetail01);
        parcel.writeString(requestTypeDetail02);
        parcel.writeString(requestTypeDetail03);
        parcel.writeString(requestCode);
        parcel.writeString(requestedDate);
        parcel.writeString(memCode);
        parcel.writeString(requestDatetime);
        parcel.writeString(doctorName);
        parcel.writeString(doctorSpec);
        parcel.writeString(primaryDiag);
        parcel.writeString(reasonForConsult);
        parcel.writeString(diagType);
        parcel.writeTypedList(mappedTest);
        parcel.writeTypedList(groupedByCostCenters);
        parcel.writeString(totalAmount);


    }
}

