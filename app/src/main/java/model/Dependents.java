package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by mpx-pawpaw on 11/18/16.
 */
public class Dependents implements Parcelable {

    private String otherRemarks;

    private String isPrincipal;

    private String memberType;

    private String lastname;

    private String statRemarks;

    private String gender;

    private String birthDate;

    private String mi;

    private String remarks1;

    private String remarks2;

    private String remarks6;

    private String remarks5;

    private String remarks4;

    private String remarks3;

    private String effectDate;

    private String remarks7;

    private String roomRateId;

    private String firstname;

    private String code;

    private String updateDate;

    private String companyCode;

    private String planCode;

    private String validDate;

    private String planDesc;

    private String companyDesc;

    public String getOtherRemarks ()
    {
        return otherRemarks;
    }

    public void setOtherRemarks (String otherRemarks)
    {
        this.otherRemarks = otherRemarks;
    }

    public String getIsPrincipal ()
    {
        return isPrincipal;
    }

    public void setIsPrincipal (String isPrincipal)
    {
        this.isPrincipal = isPrincipal;
    }

    public String getMemberType ()
    {
        return memberType;
    }

    public void setMemberType (String memberType)
    {
        this.memberType = memberType;
    }

    public String getLastname ()
    {
        return lastname;
    }

    public void setLastname (String lastname)
    {
        this.lastname = lastname;
    }

    public String getStatRemarks ()
    {
        return statRemarks;
    }

    public void setStatRemarks (String statRemarks)
    {
        this.statRemarks = statRemarks;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getBirthDate ()
    {
        return birthDate;
    }

    public void setBirthDate (String birthDate)
    {
        this.birthDate = birthDate;
    }

    public String getMi ()
    {
        return mi;
    }

    public void setMi (String mi)
    {
        this.mi = mi;
    }

    public String getRemarks1 ()
    {
        return remarks1;
    }

    public void setRemarks1 (String remarks1)
    {
        this.remarks1 = remarks1;
    }

    public String getRemarks2 ()
    {
        return remarks2;
    }

    public void setRemarks2 (String remarks2)
    {
        this.remarks2 = remarks2;
    }

    public String getRemarks6 ()
    {
        return remarks6;
    }

    public void setRemarks6 (String remarks6)
    {
        this.remarks6 = remarks6;
    }

    public String getRemarks5 ()
    {
        return remarks5;
    }

    public void setRemarks5 (String remarks5)
    {
        this.remarks5 = remarks5;
    }

    public String getRemarks4 ()
    {
        return remarks4;
    }

    public void setRemarks4 (String remarks4)
    {
        this.remarks4 = remarks4;
    }

    public String getRemarks3 ()
    {
        return remarks3;
    }

    public void setRemarks3 (String remarks3)
    {
        this.remarks3 = remarks3;
    }

    public String getEffectDate ()
    {
        return effectDate;
    }

    public void setEffectDate (String effectDate)
    {
        this.effectDate = effectDate;
    }

    public String getRemarks7 ()
    {
        return remarks7;
    }

    public void setRemarks7 (String remarks7)
    {
        this.remarks7 = remarks7;
    }

    public String getRoomRateId ()
    {
        return roomRateId;
    }

    public void setRoomRateId (String roomRateId)
    {
        this.roomRateId = roomRateId;
    }

    public String getFirstname ()
    {
        return firstname;
    }

    public void setFirstname (String firstname)
    {
        this.firstname = firstname;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getUpdateDate ()
    {
        return updateDate;
    }

    public void setUpdateDate (String updateDate)
    {
        this.updateDate = updateDate;
    }

    public String getCompanyCode ()
    {
        return companyCode;
    }

    public void setCompanyCode (String companyCode)
    {
        this.companyCode = companyCode;
    }

    public String getPlanCode ()
    {
        return planCode;
    }

    public void setPlanCode (String planCode)
    {
        this.planCode = planCode;
    }

    public String getValidDate ()
    {
        return validDate;
    }

    public void setValidDate (String validDate)
    {
        this.validDate = validDate;
    }

    public String getPlanDesc ()
    {
        return planDesc;
    }

    public void setPlanDesc (String planDesc)
    {
        this.planDesc = planDesc;
    }

    public String getCompanyDesc ()
    {
        return companyDesc;
    }

    public void setCompanyDesc (String companyDesc)
    {
        this.companyDesc = companyDesc;
    }

    protected Dependents(Parcel in) {
        otherRemarks = in.readString();
        isPrincipal = in.readString();
        memberType = in.readString();
        lastname = in.readString();
        statRemarks = in.readString();
        gender = in.readString();
        birthDate = in.readString();
        mi = in.readString();
        remarks1 = in.readString();
        remarks2 = in.readString();
        remarks6 = in.readString();
        remarks5 = in.readString();
        remarks4 = in.readString();
        remarks3 = in.readString();
        effectDate = in.readString();
        remarks7 = in.readString();
        roomRateId = in.readString();
        firstname = in.readString();
        code = in.readString();
        updateDate = in.readString();
        companyCode = in.readString();
        planCode = in.readString();
        validDate = in.readString();
        planDesc = in.readString();
        companyDesc = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(otherRemarks);
        dest.writeString(isPrincipal);
        dest.writeString(memberType);
        dest.writeString(lastname);
        dest.writeString(statRemarks);
        dest.writeString(gender);
        dest.writeString(birthDate);
        dest.writeString(mi);
        dest.writeString(remarks1);
        dest.writeString(remarks2);
        dest.writeString(remarks6);
        dest.writeString(remarks5);
        dest.writeString(remarks4);
        dest.writeString(remarks3);
        dest.writeString(effectDate);
        dest.writeString(remarks7);
        dest.writeString(roomRateId);
        dest.writeString(firstname);
        dest.writeString(code);
        dest.writeString(updateDate);
        dest.writeString(companyCode);
        dest.writeString(planCode);
        dest.writeString(validDate);
        dest.writeString(planDesc);
        dest.writeString(companyDesc);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Dependents> CREATOR = new Parcelable.Creator<Dependents>() {
        @Override
        public Dependents createFromParcel(Parcel in) {
            return new Dependents(in);
        }

        @Override
        public Dependents[] newArray(int size) {
            return new Dependents[size];
        }
    };

    @Override
    public String toString()
    {
        return "ClassPojo [otherRemarks = "+otherRemarks+", isPrincipal = "+isPrincipal+", memberType = "+memberType+", lastname = "+lastname+", statRemarks = "+statRemarks+", gender = "+gender+", birthDate = "+birthDate+", mi = "+mi+", remarks1 = "+remarks1+", remarks2 = "+remarks2+", remarks6 = "+remarks6+", remarks5 = "+remarks5+", remarks4 = "+remarks4+", remarks3 = "+remarks3+", effectDate = "+effectDate+", remarks7 = "+remarks7+", roomRateId = "+roomRateId+", firstname = "+firstname+", code = "+code+", updateDate = "+updateDate+", companyCode = "+companyCode+", planCode = "+planCode+", validDate = "+validDate+", planDesc = "+planDesc+", companyDesc = "+companyDesc+"]";
    }
}
