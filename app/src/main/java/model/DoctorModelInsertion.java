package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mpx-pawpaw on 11/17/16.
 */

public class DoctorModelInsertion implements Parcelable {


    private String specialRem;

    private String docFname;

    private String specDesc;

    private String hospRemarks;

    private String doctorCode;

    private String docMname;

    private String vat;

    private String wtax;

    private String remarks;

    private String gracePeriod;

    private String specCode;

    private String schedule;

    private String hospitalName;

    private String docLname;

    private String hospitalCode;

    private String roomBoard;

    private String remarks2;

    private String room;

    private String isSelected;




    public DoctorModelInsertion(String docMname, String docLname, String docFname, String specDesc, String doctorCode) {
        this.docFname = docFname;
        this.docMname = docMname ;
        this.docLname = docLname ;
        this.doctorCode = doctorCode ;
        this.specDesc = specDesc ;

    }

    public DoctorModelInsertion(){

    }
    public DoctorModelInsertion(String specialRem,
                                String docFname,
                                String specDesc,
                                String hospRemarks,
                                String doctorCode,
                                String docMname,
                                String vat,
                                String wtax,
                                String remarks,
                                String gracePeriod,
                                String specCode,
                                String schedule,
                                String room,
                                String hospitalName,
                                String docLname,
                                String hospitalCode,
                                String remarks2,
                                String roomBoard) {
        this.specialRem = specialRem;
        this.docFname = docFname;
        this.specDesc = specDesc;
        this.hospRemarks = hospRemarks;
        this.doctorCode = doctorCode;
        this.docMname = docMname;
        this.vat = vat;
        this.wtax = wtax;
        this.remarks = remarks;
        this.gracePeriod = gracePeriod;
        this.specCode = specCode;
        this.schedule = schedule;
        this.hospitalName = hospitalName;
        this.docLname = docLname;
        this.hospitalCode = hospitalCode;
        this.roomBoard = roomBoard;
        this.remarks2 = remarks2;
        this.room = room;
    }

    protected DoctorModelInsertion(Parcel in) {
        specialRem = in.readString();
        docFname = in.readString();
        specDesc = in.readString();
        hospRemarks = in.readString();
        doctorCode = in.readString();
        docMname = in.readString();
        vat = in.readString();
        wtax = in.readString();
        remarks = in.readString();
        gracePeriod = in.readString();
        specCode = in.readString();
        schedule = in.readString();
        hospitalName = in.readString();
        docLname = in.readString();
        hospitalCode = in.readString();
        roomBoard = in.readString();
        remarks2 = in.readString();
        room = in.readString();
    }

    public static final Creator<DoctorModelInsertion> CREATOR = new Creator<DoctorModelInsertion>() {
        @Override
        public DoctorModelInsertion createFromParcel(Parcel in) {
            return new DoctorModelInsertion(in);
        }

        @Override
        public DoctorModelInsertion[] newArray(int size) {
            return new DoctorModelInsertion[size];
        }
    };

    public String getSpecialRem() {
        return specialRem;
    }

    public void setSpecialRem(String specialRem) {
        this.specialRem = specialRem;
    }

    public String getDocFname() {
        return docFname;
    }

    public void setDocFname(String docFname) {
        this.docFname = docFname;
    }

    public String getSpecDesc() {
        return specDesc;
    }

    public void setSpecDesc(String specDesc) {
        this.specDesc = specDesc;
    }

    public String getHospRemarks() {
        return hospRemarks;
    }

    public void setHospRemarks(String hospRemarks) {
        this.hospRemarks = hospRemarks;
    }

    public String getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(String doctorCode) {
        this.doctorCode = doctorCode;
    }

    public String getDocMname() {
        return docMname;
    }

    public void setDocMname(String docMname) {
        this.docMname = docMname;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getWtax() {
        return wtax;
    }

    public void setWtax(String wtax) {
        this.wtax = wtax;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getGracePeriod() {
        return gracePeriod;
    }

    public void setGracePeriod(String gracePeriod) {
        this.gracePeriod = gracePeriod;
    }

    public String getSpecCode() {
        return specCode;
    }

    public void setSpecCode(String specCode) {
        this.specCode = specCode;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getDocLname() {
        return docLname;
    }

    public void setDocLname(String docLname) {
        this.docLname = docLname;
    }

    public String getHospitalCode() {
        return hospitalCode;
    }

    public void setHospitalCode(String hospitalCode) {
        this.hospitalCode = hospitalCode;
    }

    public String getRoomBoard() {
        return roomBoard;
    }

    public void setRoomBoard(String roomBoard) {
        this.roomBoard = roomBoard;
    }

    public String getRemarks2() {
        return remarks2;
    }

    public void setRemarks2(String remarks2) {
        this.remarks2 = remarks2;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }


    public String getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(String isSelected) {
        this.isSelected = isSelected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(specialRem);
        parcel.writeString(docFname);
        parcel.writeString(specDesc);
        parcel.writeString(hospRemarks);
        parcel.writeString(doctorCode);
        parcel.writeString(docMname);
        parcel.writeString(vat);
        parcel.writeString(wtax);
        parcel.writeString(remarks);
        parcel.writeString(gracePeriod);
        parcel.writeString(specCode);
        parcel.writeString(schedule);
        parcel.writeString(hospitalName);
        parcel.writeString(docLname);
        parcel.writeString(hospitalCode);
        parcel.writeString(roomBoard);
        parcel.writeString(remarks2);
        parcel.writeString(room);
    }

    public boolean isSelected() {
        if(getIsSelected().toString().equals("true")){
            return true;
        }else {
            return false;
        }
    }
}
