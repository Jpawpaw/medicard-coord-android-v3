package model;

/**
 * Created by mpx-pawpaw on 11/18/16.
 */

public class LOAReturn {
    private String responseCode;

    private String batchCode;

    private String responseDesc;

    private String withProvider;

    private String approvalNo;

    private String remarks;

    public String getResponseCode ()
    {
        return responseCode;
    }

    public void setResponseCode (String responseCode)
    {
        this.responseCode = responseCode;
    }

    public String getBatchCode ()
    {
        return batchCode;
    }

    public void setBatchCode (String batchCode)
    {
        this.batchCode = batchCode;
    }

    public String getResponseDesc ()
    {
        return responseDesc;
    }

    public void setResponseDesc (String responseDesc)
    {
        this.responseDesc = responseDesc;
    }

    public String getWithProvider ()
    {
        return withProvider;
    }

    public void setWithProvider (String withProvider)
    {
        this.withProvider = withProvider;
    }

    public String getApprovalNo ()
    {
        return approvalNo;
    }

    public void setApprovalNo (String approvalNo)
    {
        this.approvalNo = approvalNo;
    }

    public String getRemarks ()
    {
        return remarks;
    }

    public void setRemarks (String remarks)
    {
        this.remarks = remarks;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [responseCode = "+responseCode+", batchCode = "+batchCode+", responseDesc = "+responseDesc+", withProvider = "+withProvider+", approvalNo = "+approvalNo+", remarks = "+remarks+"]";
    }
}
