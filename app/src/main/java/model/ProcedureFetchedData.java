package model;

/**
 * Created by mpx-pawpaw on 6/3/17.
 */

public class ProcedureFetchedData {

    private String diagCode;
    private String[] procedures;


    public String getDiagCode() {
        return diagCode;
    }

    public void setDiagCode(String diagCode) {
        this.diagCode = diagCode;
    }

    public String[] getProcedures() {
        return procedures;
    }

    public void setProcedures(String[] procedures) {
        this.procedures = procedures;
    }

    public String setDataSet() {
        String data = "";

       if (procedures != null){
           for (int x = 0; x < procedures.length; x++) {
               data = " \n" + procedures[x] + data;
           }
       }

        return data;
    }

    @Override
    public String toString() {
        return "diagCode" + diagCode + " \n" +
                "procedures" + setDataSet();
    }
}
