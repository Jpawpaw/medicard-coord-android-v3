package model;

import java.io.Serializable;

/**
 * Created by mpx-pawpaw on 6/14/17.
 */

public class TestsAndProcedures  {
    private String procedureCode;

    private String procedureAmount;

    private String procedureDesc;

    private String costCenter;

    private String maceServiceType;

    private String originalPrice ;

    private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public TestsAndProcedures(){

    }

    public TestsAndProcedures(String procedureCode,
                              String procedureAmount,
                              String procedureDesc,
                              String costCenter,
                              String maceServiceType) {

        this.procedureCode = procedureCode;
        this.procedureAmount = procedureAmount;
        this.procedureDesc = procedureDesc;
        this.costCenter = costCenter;
        this.maceServiceType = maceServiceType;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getProcedureCode() {
        return procedureCode;
    }

    public void setProcedureCode(String procedureCode) {
        this.procedureCode = procedureCode;
    }

    public String getProcedureAmount() {
        return procedureAmount;
    }

    public void setProcedureAmount(String procedureAmount) {
        this.procedureAmount = procedureAmount;
    }

    public String getProcedureDesc() {
        return procedureDesc;
    }

    public void setProcedureDesc(String procedureDesc) {
        this.procedureDesc = procedureDesc;
    }

    public String getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    public String getMaceServiceType() {
        return maceServiceType;
    }

    public void setMaceServiceType(String maceServiceType) {
        this.maceServiceType = maceServiceType;
    }

    @Override
    public String toString() {
        return "ClassPojo [procedureCode = " + procedureCode + ", procedureAmount = " + procedureAmount + ", procedureDesc = " + procedureDesc + ", costCenter = " + costCenter + ", maceServiceType = " + maceServiceType + "]";
    }
}

