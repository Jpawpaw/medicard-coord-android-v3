package model;

import java.util.ArrayList;

/**
 * Created by mpx-pawpaw on 11/28/16.
 */

public class GetOtherTestReturnModel {
    private String responseCode;

    private String approvalNo;

    private String responseDesc;

    private String remarks;

    private String batchCode;

    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public String getResponseCode ()
    {
        return responseCode;
    }

    public void setResponseCode (String responseCode)
    {
        this.responseCode = responseCode;
    }

    public String getApprovalNo ()
    {
        return approvalNo;
    }

    public void setApprovalNo (String approvalNos)
    {
        this.approvalNo = approvalNos;
    }

    public String getResponseDesc ()
    {
        return responseDesc;
    }

    public void setResponseDesc (String responseDesc)
    {
        this.responseDesc = responseDesc;
    }

    public String getRemarks ()
    {
        return remarks;
    }

    public void setRemarks (String remarks)
    {
        this.remarks = remarks;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [responseCode = "+responseCode+", approvalNos = "+approvalNo+", responseDesc = "+responseDesc+", remarks = "+remarks+"]";
    }



}
