package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mpx-pawpaw on 6/14/17.
 */

public class Rooms implements Parcelable {

    private String classCategory;

    private String monthlyPremium;

    private String updatedDate;

    private String planCode;

    private String annualPremium;

    private String grpType;

    private String ddl;

    private String planDesc;

    private String semiAnnualPremium;

    private String wHosp;

    private String quarterlyPremium;

    private String updatedBy;

    public Rooms() {
    }

    protected  Rooms(Parcel in) {
        classCategory = in.readString();
        monthlyPremium = in.readString();
        updatedDate = in.readString();
        planCode = in.readString();
        annualPremium = in.readString();
        grpType = in.readString();
        ddl = in.readString();
        planDesc = in.readString();
        semiAnnualPremium = in.readString();
        wHosp = in.readString();
        quarterlyPremium = in.readString();
        updatedBy = in.readString();
    }

    public static final Creator<Rooms> CREATOR = new Creator<Rooms>() {
        @Override
        public Rooms createFromParcel(Parcel in) {
            return new Rooms(in);
        }

        @Override
        public Rooms[] newArray(int size) {
            return new Rooms[size];
        }
    };

    public String getClassCategory ()
    {
        return classCategory;
    }

    public void setClassCategory (String classCategory)
    {
        this.classCategory = classCategory;
    }

    public String getMonthlyPremium ()
    {
        return monthlyPremium;
    }

    public void setMonthlyPremium (String monthlyPremium)
    {
        this.monthlyPremium = monthlyPremium;
    }

    public String getUpdatedDate ()
    {
        return updatedDate;
    }

    public void setUpdatedDate (String updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public String getPlanCode ()
    {
        return planCode;
    }

    public void setPlanCode (String planCode)
    {
        this.planCode = planCode;
    }

    public String getAnnualPremium ()
    {
        return annualPremium;
    }

    public void setAnnualPremium (String annualPremium)
    {
        this.annualPremium = annualPremium;
    }

    public String getGrpType ()
    {
        return grpType;
    }

    public void setGrpType (String grpType)
    {
        this.grpType = grpType;
    }

    public String getDdl ()
    {
        return ddl;
    }

    public void setDdl (String ddl)
    {
        this.ddl = ddl;
    }

    public String getPlanDesc ()
    {
        return planDesc;
    }

    public void setPlanDesc (String planDesc)
    {
        this.planDesc = planDesc;
    }

    public String getSemiAnnualPremium ()
    {
        return semiAnnualPremium;
    }

    public void setSemiAnnualPremium (String semiAnnualPremium)
    {
        this.semiAnnualPremium = semiAnnualPremium;
    }

    public String getWHosp ()
    {
        return wHosp;
    }

    public void setWHosp (String wHosp)
    {
        this.wHosp = wHosp;
    }

    public String getQuarterlyPremium ()
    {
        return quarterlyPremium;
    }

    public void setQuarterlyPremium (String quarterlyPremium)
    {
        this.quarterlyPremium = quarterlyPremium;
    }

    public String getUpdatedBy ()
    {
        return updatedBy;
    }

    public void setUpdatedBy (String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [classCategory = "+classCategory+", monthlyPremium = "+monthlyPremium+", updatedDate = "+updatedDate+", planCode = "+planCode+", annualPremium = "+annualPremium+", grpType = "+grpType+", ddl = "+ddl+", planDesc = "+planDesc+", semiAnnualPremium = "+semiAnnualPremium+", wHosp = "+wHosp+", quarterlyPremium = "+quarterlyPremium+", updatedBy = "+updatedBy+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(classCategory);
        parcel.writeString(monthlyPremium);
        parcel.writeString(updatedDate);
        parcel.writeString(planCode);
        parcel.writeString(annualPremium);
        parcel.writeString(grpType);
        parcel.writeString(ddl);
        parcel.writeString(planDesc);
        parcel.writeString(semiAnnualPremium);
        parcel.writeString(wHosp);
        parcel.writeString(quarterlyPremium);
        parcel.writeString(updatedBy);
    }
}
