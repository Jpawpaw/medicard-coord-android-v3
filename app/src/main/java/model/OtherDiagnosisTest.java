package model;

/**
 * Created by mpx-pawpaw on 5/7/17.
 */

public class OtherDiagnosisTest {
    private String GROUP_REM;

    private String REMARKS;

    private String PROCEDURE_DESC;

    private String PROCLASS_CODE;

    private String PROCEDURE_ID;

    private String PROCEDURE_RATE;

    private String UPDATED_DATE;

    private String STATUS;

    private String ORGAN_CODE;

    private String PROCEDURE_CODE;

    private String UPDATED_BY;

    private String ORGSYS_CODE;

    private String EDIT_DATE;

    private String EDIT_BY;

    public String getGROUP_REM ()
    {
        return GROUP_REM;
    }

    public void setGROUP_REM (String GROUP_REM)
    {
        this.GROUP_REM = GROUP_REM;
    }

    public String getREMARKS ()
    {
        return REMARKS;
    }

    public void setREMARKS (String REMARKS)
    {
        this.REMARKS = REMARKS;
    }

    public String getPROCEDURE_DESC ()
    {
        return PROCEDURE_DESC;
    }

    public void setPROCEDURE_DESC (String PROCEDURE_DESC)
    {
        this.PROCEDURE_DESC = PROCEDURE_DESC;
    }

    public String getPROCLASS_CODE ()
    {
        return PROCLASS_CODE;
    }

    public void setPROCLASS_CODE (String PROCLASS_CODE)
    {
        this.PROCLASS_CODE = PROCLASS_CODE;
    }

    public String getPROCEDURE_ID ()
    {
        return PROCEDURE_ID;
    }

    public void setPROCEDURE_ID (String PROCEDURE_ID)
    {
        this.PROCEDURE_ID = PROCEDURE_ID;
    }

    public String getPROCEDURE_RATE ()
    {
        return PROCEDURE_RATE;
    }

    public void setPROCEDURE_RATE (String PROCEDURE_RATE)
    {
        this.PROCEDURE_RATE = PROCEDURE_RATE;
    }

    public String getUPDATED_DATE ()
    {
        return UPDATED_DATE;
    }

    public void setUPDATED_DATE (String UPDATED_DATE)
    {
        this.UPDATED_DATE = UPDATED_DATE;
    }

    public String getSTATUS ()
    {
        return STATUS;
    }

    public void setSTATUS (String STATUS)
    {
        this.STATUS = STATUS;
    }

    public String getORGAN_CODE ()
    {
        return ORGAN_CODE;
    }

    public void setORGAN_CODE (String ORGAN_CODE)
    {
        this.ORGAN_CODE = ORGAN_CODE;
    }

    public String getPROCEDURE_CODE ()
    {
        return PROCEDURE_CODE;
    }

    public void setPROCEDURE_CODE (String PROCEDURE_CODE)
    {
        this.PROCEDURE_CODE = PROCEDURE_CODE;
    }

    public String getUPDATED_BY ()
    {
        return UPDATED_BY;
    }

    public void setUPDATED_BY (String UPDATED_BY)
    {
        this.UPDATED_BY = UPDATED_BY;
    }

    public String getORGSYS_CODE ()
    {
        return ORGSYS_CODE;
    }

    public void setORGSYS_CODE (String ORGSYS_CODE)
    {
        this.ORGSYS_CODE = ORGSYS_CODE;
    }

    public String getEDIT_DATE ()
    {
        return EDIT_DATE;
    }

    public void setEDIT_DATE (String EDIT_DATE)
    {
        this.EDIT_DATE = EDIT_DATE;
    }

    public String getEDIT_BY ()
    {
        return EDIT_BY;
    }

    public void setEDIT_BY (String EDIT_BY)
    {
        this.EDIT_BY = EDIT_BY;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [GROUP_REM = "+GROUP_REM+", REMARKS = "+REMARKS+", PROCEDURE_DESC = "+PROCEDURE_DESC+", PROCLASS_CODE = "+PROCLASS_CODE+", PROCEDURE_ID = "+PROCEDURE_ID+", PROCEDURE_RATE = "+PROCEDURE_RATE+", UPDATED_DATE = "+UPDATED_DATE+", STATUS = "+STATUS+", ORGAN_CODE = "+ORGAN_CODE+", PROCEDURE_CODE = "+PROCEDURE_CODE+", UPDATED_BY = "+UPDATED_BY+", ORGSYS_CODE = "+ORGSYS_CODE+", EDIT_DATE = "+EDIT_DATE+", EDIT_BY = "+EDIT_BY+"]";
    }
}

