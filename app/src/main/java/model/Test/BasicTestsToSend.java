package model.Test;

/**
 * Created by IPC_Server on 8/14/2017.
 */

public class BasicTestsToSend {

    private String amount;

    private String procCode;


    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getProcCode() {
        return procCode;
    }

    public void setProcCode(String procCode) {
        this.procCode = procCode;
    }
}
