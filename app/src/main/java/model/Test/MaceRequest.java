package model.Test;

/**
 * Created by mpx-pawpaw on 5/29/17.
 */

public class MaceRequest {

        private String lastupdateBy;

        private String requestDatetime;

        private String serviceTypeId;

        private String memCompany;

        private String mbasCode;

        private String memType;

        private String requestDevice;

        private String requestBy;

        private String memAge;

        private String maceReqTest;

        private String memBdate;

        private String acctEffectivity;

        private String lastupdateOn;

        private String memMi;

        private String memFname;

        private String memStat;

        private String memLname;

        private String idremarks;

        private String status;

        private String requestId;

        private String requestOrigin;

        private String acctValidity;

        private String requestFromhosp;

        private String disclaimerTicked;

        private String memGender;

        private String memCode;

        private String mbasApprover;

        private String override;

        private String mbasupdateOn;

        private String parRequestId;

        private String memAcct;

        private String requestFrommem;

        private String maceReqAudit;

        private String requestCode;

    public String getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(String requestCode) {
        this.requestCode = requestCode;
    }

    public String getLastupdateBy ()
        {
            return lastupdateBy;
        }

        public void setLastupdateBy (String lastupdateBy)
        {
            this.lastupdateBy = lastupdateBy;
        }

        public String getRequestDatetime ()
        {
            return requestDatetime;
        }

        public void setRequestDatetime (String requestDatetime)
        {
            this.requestDatetime = requestDatetime;
        }

        public String getServiceTypeId ()
        {
            return serviceTypeId;
        }

        public void setServiceTypeId (String serviceTypeId)
        {
            this.serviceTypeId = serviceTypeId;
        }

        public String getMemCompany ()
        {
            return memCompany;
        }

        public void setMemCompany (String memCompany)
        {
            this.memCompany = memCompany;
        }

        public String getMbasCode ()
    {
        return mbasCode;
    }

        public void setMbasCode (String mbasCode)
        {
            this.mbasCode = mbasCode;
        }

        public String getMemType ()
        {
            return memType;
        }

        public void setMemType (String memType)
        {
            this.memType = memType;
        }

        public String getRequestDevice ()
        {
            return requestDevice;
        }

        public void setRequestDevice (String requestDevice)
        {
            this.requestDevice = requestDevice;
        }

        public String getRequestBy ()
        {
            return requestBy;
        }

        public void setRequestBy (String requestBy)
        {
            this.requestBy = requestBy;
        }

        public String getMemAge ()
        {
            return memAge;
        }

        public void setMemAge (String memAge)
        {
            this.memAge = memAge;
        }

        public String getMaceReqTest ()
    {
        return maceReqTest;
    }

        public void setMaceReqTest (String maceReqTest)
        {
            this.maceReqTest = maceReqTest;
        }

        public String getMemBdate ()
        {
            return memBdate;
        }

        public void setMemBdate (String memBdate)
        {
            this.memBdate = memBdate;
        }

        public String getAcctEffectivity ()
        {
            return acctEffectivity;
        }

        public void setAcctEffectivity (String acctEffectivity)
        {
            this.acctEffectivity = acctEffectivity;
        }

        public String getLastupdateOn ()
        {
            return lastupdateOn;
        }

        public void setLastupdateOn (String lastupdateOn)
        {
            this.lastupdateOn = lastupdateOn;
        }

        public String getMemMi ()
        {
            return memMi;
        }

        public void setMemMi (String memMi)
        {
            this.memMi = memMi;
        }

        public String getMemFname ()
        {
            return memFname;
        }

        public void setMemFname (String memFname)
        {
            this.memFname = memFname;
        }

        public String getMemStat ()
        {
            return memStat;
        }

        public void setMemStat (String memStat)
        {
            this.memStat = memStat;
        }

        public String getMemLname ()
        {
            return memLname;
        }

        public void setMemLname (String memLname)
        {
            this.memLname = memLname;
        }

        public String getIdremarks ()
        {
            return idremarks;
        }

        public void setIdremarks (String idremarks)
        {
            this.idremarks = idremarks;
        }

        public String getStatus ()
        {
            return status;
        }

        public void setStatus (String status)
        {
            this.status = status;
        }

        public String getRequestId ()
        {
            return requestId;
        }

        public void setRequestId (String requestId)
        {
            this.requestId = requestId;
        }

        public String getRequestOrigin ()
        {
            return requestOrigin;
        }

        public void setRequestOrigin (String requestOrigin)
        {
            this.requestOrigin = requestOrigin;
        }

        public String getAcctValidity ()
        {
            return acctValidity;
        }

        public void setAcctValidity (String acctValidity)
        {
            this.acctValidity = acctValidity;
        }

        public String getRequestFromhosp ()
        {
            return requestFromhosp;
        }

        public void setRequestFromhosp (String requestFromhosp)
        {
            this.requestFromhosp = requestFromhosp;
        }

        public String getDisclaimerTicked ()
    {
        return disclaimerTicked;
    }

        public void setDisclaimerTicked (String disclaimerTicked)
        {
            this.disclaimerTicked = disclaimerTicked;
        }

        public String getMemGender ()
        {
            return memGender;
        }

        public void setMemGender (String memGender)
        {
            this.memGender = memGender;
        }

        public String getMemCode ()
        {
            return memCode;
        }

        public void setMemCode (String memCode)
        {
            this.memCode = memCode;
        }

        public String getMbasApprover ()
    {
        return mbasApprover;
    }

        public void setMbasApprover (String mbasApprover)
        {
            this.mbasApprover = mbasApprover;
        }

        public String getOverride ()
        {
            return override;
        }

        public void setOverride (String override)
        {
            this.override = override;
        }

        public String getMbasupdateOn ()
    {
        return mbasupdateOn;
    }

        public void setMbasupdateOn (String mbasupdateOn)
        {
            this.mbasupdateOn = mbasupdateOn;
        }

        public String getParRequestId ()
        {
            return parRequestId;
        }

        public void setParRequestId (String parRequestId)
        {
            this.parRequestId = parRequestId;
        }

        public String getMemAcct ()
        {
            return memAcct;
        }

        public void setMemAcct (String memAcct)
        {
            this.memAcct = memAcct;
        }

        public String getRequestFrommem ()
        {
            return requestFrommem;
        }

        public void setRequestFrommem (String requestFrommem)
        {
            this.requestFrommem = requestFrommem;
        }

        public String getMaceReqAudit ()
    {
        return maceReqAudit;
    }

        public void setMaceReqAudit (String maceReqAudit)
        {
            this.maceReqAudit = maceReqAudit;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [lastupdateBy = "+lastupdateBy+", requestDatetime = "+requestDatetime+", serviceTypeId = "+serviceTypeId+", memCompany = "+memCompany+", mbasCode = "+mbasCode+", memType = "+memType+", requestDevice = "+requestDevice+", requestBy = "+requestBy+", memAge = "+memAge+", maceReqTest = "+maceReqTest+", memBdate = "+memBdate+", acctEffectivity = "+acctEffectivity+", lastupdateOn = "+lastupdateOn+", memMi = "+memMi+", memFname = "+memFname+", memStat = "+memStat+", memLname = "+memLname+", idremarks = "+idremarks+", status = "+status+", requestId = "+requestId+", requestOrigin = "+requestOrigin+", acctValidity = "+acctValidity+", requestFromhosp = "+requestFromhosp+", disclaimerTicked = "+disclaimerTicked+", memGender = "+memGender+", memCode = "+memCode+", mbasApprover = "+mbasApprover+", override = "+override+", mbasupdateOn = "+mbasupdateOn+", parRequestId = "+parRequestId+", memAcct = "+memAcct+", requestFrommem = "+requestFrommem+", maceReqAudit = "+maceReqAudit+"]";
        }
    }


