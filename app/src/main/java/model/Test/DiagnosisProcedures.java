package model.Test;

import android.os.Parcel;
import android.os.Parcelable;

import model.MaceRequestOpDiag;
import model.MaceRequestOpTest;
import model.MaceRequestTest;

/**
 * Created by mpx-pawpaw on 5/29/17.
 */

public class DiagnosisProcedures implements Parcelable {

    private String amount;

    private MaceRequestOpTest maceRequestOpTest;

    private String procedureCode;

    private MaceRequestOpDiag maceRequestOpDiag;

    private String status;

    private String approvalNo;

    private String remarks;

    private String diagnosisCode;

    private MaceRequestTest maceRequestTest;


    protected DiagnosisProcedures(Parcel in) {
        amount = in.readString();
        procedureCode = in.readString();
        status = in.readString();
        approvalNo = in.readString();
        remarks = in.readString();
        diagnosisCode = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(amount);
        dest.writeString(procedureCode);
        dest.writeString(status);
        dest.writeString(approvalNo);
        dest.writeString(remarks);
        dest.writeString(diagnosisCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiagnosisProcedures> CREATOR = new Creator<DiagnosisProcedures>() {
        @Override
        public DiagnosisProcedures createFromParcel(Parcel in) {
            return new DiagnosisProcedures(in);
        }

        @Override
        public DiagnosisProcedures[] newArray(int size) {
            return new DiagnosisProcedures[size];
        }
    };

    public String getAmount ()
    {
        return amount;
    }

    public void setAmount (String amount)
    {
        this.amount = amount;
    }

    public MaceRequestOpTest getMaceRequestOpTest ()
    {
        return maceRequestOpTest;
    }

    public void setMaceRequestOpTest (MaceRequestOpTest maceRequestOpTest)
    {
        this.maceRequestOpTest = maceRequestOpTest;
    }

    public String getProcedureCode ()
    {
        return procedureCode;
    }

    public void setProcedureCode (String procedureCode)
    {
        this.procedureCode = procedureCode;
    }

    public MaceRequestOpDiag getMaceRequestOpDiag ()
    {
        return maceRequestOpDiag;
    }

    public void setMaceRequestOpDiag (MaceRequestOpDiag maceRequestOpDiag)
    {
        this.maceRequestOpDiag = maceRequestOpDiag;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getApprovalNo ()
    {
        return approvalNo;
    }

    public void setApprovalNo (String approvalNo)
    {
        this.approvalNo = approvalNo;
    }

    public String getRemarks ()
    {
        return remarks;
    }

    public void setRemarks (String remarks)
    {
        this.remarks = remarks;
    }

    public String getDiagnosisCode ()
    {
        return diagnosisCode;
    }

    public void setDiagnosisCode (String diagnosisCode)
    {
        this.diagnosisCode = diagnosisCode;
    }

    public MaceRequestTest getMaceRequestTest ()
    {
        return maceRequestTest;
    }

    public void setMaceRequestTest (MaceRequestTest maceRequestTest)
    {
        this.maceRequestTest = maceRequestTest;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [amount = "+amount+", maceRequestOpTest = "+maceRequestOpTest+", procedureCode = "+procedureCode+", maceRequestOpDiag = "+maceRequestOpDiag+", status = "+status+", approvalNo = "+approvalNo+", remarks = "+remarks+", diagnosisCode = "+diagnosisCode+", maceRequestTest = "+maceRequestTest+"]";
    }
}


