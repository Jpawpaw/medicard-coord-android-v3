package model.Test;

import java.util.ArrayList;

import model.BasicTests;
import model.DiagnosisClinicProcedures;
import model.LoaList;
import model.RequestBasicTest;


/**
 * Created by mpx-pawpaw on 5/29/17.
 */

public class Data {
    private MaceRequest maceRequest;

    private ArrayList<DiagnosisProcedures> diagnosisProcedures;

    private ArrayList<DiagnosisClinicProcedures> diagnosisClinicProcedures;

    private ArrayList<BasicTests> basicTests;

    private ArrayList<RequestBasicTest> requestBasicTest;


    public MaceRequest getMaceRequest ()
    {
        return maceRequest;
    }

    public void setMaceRequest (MaceRequest maceRequest)
    {
        this.maceRequest = maceRequest;
    }

    public ArrayList<DiagnosisProcedures> getDiagnosisProcedures ()
    {
        return diagnosisProcedures;
    }

    public void setDiagnosisProcedures (ArrayList<DiagnosisProcedures> diagnosisProcedures) { this.diagnosisProcedures = diagnosisProcedures; }

    public ArrayList<DiagnosisClinicProcedures> getDiagnosisClinicProcedures() { return diagnosisClinicProcedures; }

    public void setDiagnosisClinicProcedures(ArrayList<DiagnosisClinicProcedures> diagnosisClinicProcedures) { this.diagnosisClinicProcedures = diagnosisClinicProcedures; }

    public ArrayList<BasicTests> getBasicTests() { return basicTests; }

    public void setBasicTests(ArrayList<BasicTests> basicTests) { this.basicTests = basicTests; }

    public ArrayList<RequestBasicTest> getRequestBasicTest() {
        return requestBasicTest;
    }

    public void setRequestBasicTest(ArrayList<RequestBasicTest> requestBasicTest) {
        this.requestBasicTest = requestBasicTest;
    }


    @Override
    public String toString()
    {
        return "ClassPojo [maceRequest = "+maceRequest+", diagnosisProcedures = "+diagnosisProcedures+"]";
    }
}
