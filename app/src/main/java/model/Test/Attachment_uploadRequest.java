package model.Test;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by IPC on 11/24/2017.
 */

public class Attachment_uploadRequest implements Parcelable {

    private String fileName;
    private String uri;

    public Attachment_uploadRequest() {
    }

    private Attachment_uploadRequest(Builder builder) {
        setFileName(builder.fileName);
        setUri(builder.uri);
    }

    protected Attachment_uploadRequest(Parcel in) {
        fileName = in.readString();
        uri = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fileName);
        dest.writeString(uri);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Attachment_uploadRequest> CREATOR = new Creator<Attachment_uploadRequest>() {
        @Override
        public Attachment_uploadRequest createFromParcel(Parcel in) {
            return new Attachment_uploadRequest(in);
        }

        @Override
        public Attachment_uploadRequest[] newArray(int size) {
            return new Attachment_uploadRequest[size];
        }
    };

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }


    public static final class Builder {
        private String fileName;
        private String uri;

        public Builder() {
        }

        public Builder fileName(String val) {
            fileName = val;
            return this;
        }

        public Builder uri(String val) {
            uri = val;
            return this;
        }

        public Attachment_uploadRequest build() {
            return new Attachment_uploadRequest(this);
        }
    }

}
