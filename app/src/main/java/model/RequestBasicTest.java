package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Lawrence on 8/11/2017.
 */

public class RequestBasicTest {

    private String memberCode;
    private String hospitalCode;
    private String appUsername;
    private String requestDevice;
    private String duplicateTag;
    private String coorContact;
    private String coorEmail;

    public String getCoorContact() {
        return coorContact;
    }

    public void setCoorContact(String coorContact) {
        this.coorContact = coorContact;
    }

    public String getCoorEmail() {
        return coorEmail;
    }

    public void setCoorEmail(String coorEmail) {
        this.coorEmail = coorEmail;
    }

    public String getDuplicateTag() {
        return duplicateTag;
    }

    public RequestBasicTest (){}

    public RequestBasicTest(BasicTestOrOtherTest json){
        this.memberCode = json.getMemberCode();
        this.hospitalCode = json.getHospitalCode();
    }

    public RequestBasicTest(RequestBasicTest req) {
        this.memberCode = req.getMemberCode();
        this.hospitalCode = req.getHospitalCode();
        this.appUsername = req.getAppUsername();
        this.requestDevice = req.getRequestDevice();
        this.duplicateTag = req.getDuplicateTag();
        this.tests = req.getTests();
    }


    public void setDuplicateTag(String duplicateTag) {
        this.duplicateTag = duplicateTag;
    }

    private ArrayList<BasicTestsToSend> tests;

    //-----------Getter Setter

    public String getMemberCode() { return memberCode; }

    public void setMemberCode(String memberCode) { this.memberCode = memberCode; }

    public String getHospitalCode() { return hospitalCode; }

    public void setHospitalCode(String hospitalCode) { this.hospitalCode = hospitalCode; }

    public String getAppUsername() { return appUsername; }

    public void setAppUsername(String appUsername) { this.appUsername = appUsername; }

    public String getRequestDevice() { return requestDevice; }

    public void setRequestDevice(String requestDevice) { this.requestDevice = requestDevice; }

    public ArrayList<BasicTestsToSend> getTests() {
        return tests;
    }

    public void setTests(ArrayList<BasicTestsToSend> tests) {
        this.tests = tests;
    }

    public static class BasicTestsToSend {

        private double amount;

        private String procCode;

        private String procDesc;

        public double getAmount() {
            return amount;
        }

        public void setAmount(double amount) {
            this.amount = amount;
        }

        public String getProcCode() {
            return procCode;
        }

        public void setProcCode(String procCode) {
            this.procCode = procCode;
        }

        public String getProcDesc() {
            return procDesc;
        }

        public void setProcDesc(String procDesc) {
            this.procDesc = procDesc;
        }
    }


    public static final Parcelable.Creator<model.RequestBasicTest> CREATOR = new Parcelable.Creator<model.RequestBasicTest>() {


        @Override
        public RequestBasicTest createFromParcel(Parcel source) {
            return null;
        }

        @Override
        public RequestBasicTest[] newArray(int size) {
            return new RequestBasicTest[0];
        }
    };
}
