package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mpx-pawpaw on 5/7/17.
 */
public class ProcedureList
{
    private String id;

    private String procedureCode;

    private String status;

    private String serviceClassCode;

    private String procedureAmount;

    private String procedureDesc;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getProcedureCode ()
    {
        return procedureCode;
    }

    public void setProcedureCode (String procedureCode)
    {
        this.procedureCode = procedureCode;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getServiceClassCode ()
    {
        return serviceClassCode;
    }

    public void setServiceClassCode (String serviceClassCode)
    {
        this.serviceClassCode = serviceClassCode;
    }

    public String getProcedureAmount ()
    {
        return procedureAmount;
    }

    public void setProcedureAmount (String procedureAmount)
    {
        this.procedureAmount = procedureAmount;
    }

    public String getProcedureDesc ()
    {
        return procedureDesc;
    }

    public void setProcedureDesc (String procedureDesc)
    {
        this.procedureDesc = procedureDesc;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", procedureCode = "+procedureCode+", status = "+status+", serviceClassCode = "+serviceClassCode+", procedureAmount = "+procedureAmount+", procedureDesc = "+procedureDesc+"]";
    }
}

