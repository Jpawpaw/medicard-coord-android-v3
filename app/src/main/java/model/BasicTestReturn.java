package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by mpx-pawpaw on 5/9/17.
 */

public class BasicTestReturn implements Parcelable {

    private String responseCode;

    private ArrayList<BasicTests> basicTests;

    private String responseDesc;

    protected BasicTestReturn(Parcel in) {
        responseCode = in.readString();
        responseDesc = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(responseCode);
        dest.writeString(responseDesc);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BasicTestReturn> CREATOR = new Creator<BasicTestReturn>() {
        @Override
        public BasicTestReturn createFromParcel(Parcel in) {
            return new BasicTestReturn(in);
        }

        @Override
        public BasicTestReturn[] newArray(int size) {
            return new BasicTestReturn[size];
        }
    };

    public String getResponseCode ()
    {
        return responseCode;
    }

    public void setResponseCode (String responseCode)
    {
        this.responseCode = responseCode;
    }

    public ArrayList<BasicTests> getBasicTests ()
    {
        return basicTests;
    }

    public void setBasicTests (ArrayList<BasicTests> basicTests)
    {
        this.basicTests = basicTests;
    }

    public String getResponseDesc ()
    {
        return responseDesc;
    }

    public void setResponseDesc (String responseDesc)
    {
        this.responseDesc = responseDesc;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [responseCode = "+responseCode+", basicTests = "+basicTests+", responseDesc = "+responseDesc+"]";
    }

}
