package model;

/**
 * Created by mpx-pawpaw on 5/3/17.
 */

public class ProcedureFilter {

    private String responseCode;

    private String responseDesc;

    private String[] procedures;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDesc() {
        return responseDesc;
    }

    public void setResponseDesc(String responseDesc) {
        this.responseDesc = responseDesc;
    }

    public String[] getProcedures() {
        return procedures;
    }

    public void setProcedures(String[] procedures) {
        this.procedures = procedures;
    }

    @Override
    public String toString() {
        return "ClassPojo [responseCode = " + responseCode + ", responseDesc = " + responseDesc + ", procedures = " + procedures + "]";
    }
}
