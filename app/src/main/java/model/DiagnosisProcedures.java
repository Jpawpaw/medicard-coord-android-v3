package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mpx-pawpaw on 5/23/17.
 */
public class DiagnosisProcedures implements Parcelable
{
    private String amount;

    private String procedureCode;

    private String diagnosisCode;

    private String serviceType;

    public DiagnosisProcedures() {
    }

    protected DiagnosisProcedures(Parcel in) {
        amount = in.readString();
        procedureCode = in.readString();
        diagnosisCode = in.readString();
        serviceType = in.readString();
    }

    public static final Creator<DiagnosisProcedures> CREATOR = new Creator<DiagnosisProcedures>() {
        @Override
        public DiagnosisProcedures createFromParcel(Parcel in) {
            return new DiagnosisProcedures(in);
        }

        @Override
        public DiagnosisProcedures[] newArray(int size) {
            return new DiagnosisProcedures[size];
        }
    };

    public String getAmount ()
    {
        return amount;
    }

    public void setAmount (String amount)
    {
        this.amount = amount;
    }

    public String getProcedureCode ()
    {
        return procedureCode;
    }

    public void setProcedureCode (String procedureCode)
    {
        this.procedureCode = procedureCode;
    }

    public String getDiagnosisCode ()
    {
        return diagnosisCode;
    }

    public void setDiagnosisCode (String diagnosisCode)
    {
        this.diagnosisCode = diagnosisCode;
    }

    public String getServiceType ()
    {
        return serviceType;
    }

    public void setServiceType (String serviceType)
    {
        this.serviceType = serviceType;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [amount = "+amount+", procedureCode = "+procedureCode+", diagnosisCode = "+diagnosisCode+", serviceType = "+serviceType+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(amount);
        parcel.writeString(procedureCode);
        parcel.writeString(diagnosisCode);
        parcel.writeString(serviceType);
    }
}

