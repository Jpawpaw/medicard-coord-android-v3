package model;

/**
 * Created by mpx-pawpaw on 11/25/16.
 */

public class ProceduresListLocal {


    private String id;

    private String procedureCode;

    private String serviceClassCode;

    private String procedureAmount;

    private String procedureDesc;

    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    private String originalPrice;


    public ProceduresListLocal() {

    }


    public ProceduresListLocal(String procedureCode, String procedureDesc, String serviceClassCode, String procedureAmount, String id) {
        this.id = id;
        this.procedureCode = procedureCode;
        this.serviceClassCode = serviceClassCode;
        this.procedureDesc = procedureDesc;
        this.procedureAmount = procedureAmount;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProcedureCode() {
        return procedureCode;
    }

    public void setProcedureCode(String procedureCode) {
        this.procedureCode = procedureCode;
    }

    public String getServiceClassCode() {
        return serviceClassCode;
    }

    public void setServiceClassCode(String serviceClassCode) {
        this.serviceClassCode = serviceClassCode;
    }

    public String getProcedureAmount() {
        return procedureAmount;
    }

    public void setProcedureAmount(String procedureAmount) {
        this.procedureAmount = procedureAmount;
    }

    public String getProcedureDesc() {
        return procedureDesc;
    }

    public void setProcedureDesc(String procedureDesc) {
        this.procedureDesc = procedureDesc;
    }

    @Override
    public String toString() {
        return "ClassPojo [id = " + id + ", procedureCode = " + procedureCode + ", serviceClassCode = " + serviceClassCode + ", procedureAmount = " + procedureAmount + ", procedureDesc = " + procedureDesc + "]";
    }
}
