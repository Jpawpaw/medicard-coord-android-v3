package model;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

/**
 * Created by IPC_Server on 10/12/2017.
 */

public class CustomLinearLayoutManager extends LinearLayoutManager {

    public CustomLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);

    }

    @Override
    public boolean canScrollVertically() {
        return false;
    }
}
