package model;

/**
 * Created by mpx-pawpaw on 5/24/17.
 */

public class MaceRequestOpTest {
    private String procTypeDesc;

    private String procActualAmount;

    private String transactionId;

    private String procClass;

    private String status;

    private String refDiagProcId;

    private String procClassDesc;

    private String diagTestId;

    private String reqDiagId;

    private String procDesc;

    private String procHospAmount;

    private String maceRequestId;

    private String ruv;

    private String diagCode;

    private String procCode;

    private String maceSubtype;

    private String group;

    private String prescribedTestId;

    private String procDefAmount;

    private String procType;

    public String getProcTypeDesc ()
    {
        return procTypeDesc;
    }

    public void setProcTypeDesc (String procTypeDesc)
    {
        this.procTypeDesc = procTypeDesc;
    }

    public String getProcActualAmount ()
    {
        return procActualAmount;
    }

    public void setProcActualAmount (String procActualAmount)
    {
        this.procActualAmount = procActualAmount;
    }

    public String getTransactionId ()
    {
        return transactionId;
    }

    public void setTransactionId (String transactionId)
    {
        this.transactionId = transactionId;
    }

    public String getProcClass ()
    {
        return procClass;
    }

    public void setProcClass (String procClass)
    {
        this.procClass = procClass;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getRefDiagProcId ()
    {
        return refDiagProcId;
    }

    public void setRefDiagProcId (String refDiagProcId)
    {
        this.refDiagProcId = refDiagProcId;
    }

    public String getProcClassDesc ()
    {
        return procClassDesc;
    }

    public void setProcClassDesc (String procClassDesc)
    {
        this.procClassDesc = procClassDesc;
    }

    public String getDiagTestId ()
    {
        return diagTestId;
    }

    public void setDiagTestId (String diagTestId)
    {
        this.diagTestId = diagTestId;
    }

    public String getReqDiagId ()
    {
        return reqDiagId;
    }

    public void setReqDiagId (String reqDiagId)
    {
        this.reqDiagId = reqDiagId;
    }

    public String getProcDesc ()
    {
        return procDesc;
    }

    public void setProcDesc (String procDesc)
    {
        this.procDesc = procDesc;
    }

    public String getProcHospAmount ()
    {
        return procHospAmount;
    }

    public void setProcHospAmount (String procHospAmount)
    {
        this.procHospAmount = procHospAmount;
    }

    public String getMaceRequestId ()
    {
        return maceRequestId;
    }

    public void setMaceRequestId (String maceRequestId)
    {
        this.maceRequestId = maceRequestId;
    }

    public String getRuv ()
    {
        return ruv;
    }

    public void setRuv (String ruv)
    {
        this.ruv = ruv;
    }

    public String getDiagCode ()
    {
        return diagCode;
    }

    public void setDiagCode (String diagCode)
    {
        this.diagCode = diagCode;
    }

    public String getProcCode ()
    {
        return procCode;
    }

    public void setProcCode (String procCode)
    {
        this.procCode = procCode;
    }

    public String getMaceSubtype ()
    {
        return maceSubtype;
    }

    public void setMaceSubtype (String maceSubtype)
    {
        this.maceSubtype = maceSubtype;
    }

    public String getGroup ()
    {
        return group;
    }

    public void setGroup (String group)
    {
        this.group = group;
    }

    public String getPrescribedTestId ()
    {
        return prescribedTestId;
    }

    public void setPrescribedTestId (String prescribedTestId)
    {
        this.prescribedTestId = prescribedTestId;
    }

    public String getProcDefAmount ()
    {
        return procDefAmount;
    }

    public void setProcDefAmount (String procDefAmount)
    {
        this.procDefAmount = procDefAmount;
    }

    public String getProcType ()
    {
        return procType;
    }

    public void setProcType (String procType)
    {
        this.procType = procType;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [procTypeDesc = "+procTypeDesc+", procActualAmount = "+procActualAmount+", transactionId = "+transactionId+", procClass = "+procClass+", status = "+status+", refDiagProcId = "+refDiagProcId+", procClassDesc = "+procClassDesc+", diagTestId = "+diagTestId+", reqDiagId = "+reqDiagId+", procDesc = "+procDesc+", procHospAmount = "+procHospAmount+", maceRequestId = "+maceRequestId+", ruv = "+ruv+", diagCode = "+diagCode+", procCode = "+procCode+", maceSubtype = "+maceSubtype+", group = "+group+", prescribedTestId = "+prescribedTestId+", procDefAmount = "+procDefAmount+", procType = "+procType+"]";
    }
}

