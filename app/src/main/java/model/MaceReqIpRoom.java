package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mpx-pawpaw on 6/19/17.
 */

public class MaceReqIpRoom implements Parcelable
{
    private String rate;

    private String transactionId;

    private String maceRequestId;

    private String lastUpdateOn;

    private String dateTo;

    private String roomtype;

    private String lastUpdateBy;

    private String addedOn;

    private String hospitalCode;

    private String dateFrom;

    private String roomplan;

    private String ipRoomId;

    private String addedBy;

    protected MaceReqIpRoom(Parcel in) {
        rate = in.readString();
        transactionId = in.readString();
        maceRequestId = in.readString();
        lastUpdateOn = in.readString();
        dateTo = in.readString();
        roomtype = in.readString();
        lastUpdateBy = in.readString();
        addedOn = in.readString();
        hospitalCode = in.readString();
        dateFrom = in.readString();
        roomplan = in.readString();
        ipRoomId = in.readString();
        addedBy = in.readString();
    }

    public static final Creator<MaceReqIpRoom> CREATOR = new Creator<MaceReqIpRoom>() {
        @Override
        public MaceReqIpRoom createFromParcel(Parcel in) {
            return new MaceReqIpRoom(in);
        }

        @Override
        public MaceReqIpRoom[] newArray(int size) {
            return new MaceReqIpRoom[size];
        }
    };

    public String getRate ()
    {
        return rate;
    }

    public void setRate (String rate)
    {
        this.rate = rate;
    }

    public String getTransactionId ()
    {
        return transactionId;
    }

    public void setTransactionId (String transactionId)
    {
        this.transactionId = transactionId;
    }

    public String getMaceRequestId ()
    {
        return maceRequestId;
    }

    public void setMaceRequestId (String maceRequestId)
    {
        this.maceRequestId = maceRequestId;
    }

    public String getLastUpdateOn ()
{
    return lastUpdateOn;
}

    public void setLastUpdateOn (String  lastUpdateOn)
    {
        this.lastUpdateOn = lastUpdateOn;
    }

    public String getDateTo ()
{
    return dateTo;
}

    public void setDateTo (String dateTo)
    {
        this.dateTo = dateTo;
    }

    public String getRoomtype ()
    {
        return roomtype;
    }

    public void setRoomtype (String roomtype)
    {
        this.roomtype = roomtype;
    }

    public String getLastUpdateBy ()
{
    return lastUpdateBy;
}

    public void setLastUpdateBy (String lastUpdateBy)
    {
        this.lastUpdateBy = lastUpdateBy;
    }

    public String getAddedOn ()
{
    return addedOn;
}

    public void setAddedOn (String addedOn)
    {
        this.addedOn = addedOn;
    }

    public String getHospitalCode ()
    {
        return hospitalCode;
    }

    public void setHospitalCode (String hospitalCode)
    {
        this.hospitalCode = hospitalCode;
    }

    public String getDateFrom ()
{
    return dateFrom;
}

    public void setDateFrom (String dateFrom)
    {
        this.dateFrom = dateFrom;
    }

    public String getRoomplan ()
    {
        return roomplan;
    }

    public void setRoomplan (String roomplan)
    {
        this.roomplan = roomplan;
    }

    public String getIpRoomId ()
    {
        return ipRoomId;
    }

    public void setIpRoomId (String ipRoomId)
    {
        this.ipRoomId = ipRoomId;
    }

    public String getAddedBy ()
{
    return addedBy;
}

    public void setAddedBy (String addedBy)
    {
        this.addedBy = addedBy;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [rate = "+rate+", transactionId = "+transactionId+", maceRequestId = "+maceRequestId+", lastUpdateOn = "+lastUpdateOn+", dateTo = "+dateTo+", roomtype = "+roomtype+", lastUpdateBy = "+lastUpdateBy+", addedOn = "+addedOn+", hospitalCode = "+hospitalCode+", dateFrom = "+dateFrom+", roomplan = "+roomplan+", ipRoomId = "+ipRoomId+", addedBy = "+addedBy+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(rate);
        parcel.writeString(transactionId);
        parcel.writeString(maceRequestId);
        parcel.writeString(lastUpdateOn);
        parcel.writeString(dateTo);
        parcel.writeString(roomtype);
        parcel.writeString(lastUpdateBy);
        parcel.writeString(addedOn);
        parcel.writeString(hospitalCode);
        parcel.writeString(dateFrom);
        parcel.writeString(roomplan);
        parcel.writeString(ipRoomId);
        parcel.writeString(addedBy);
    }
}

